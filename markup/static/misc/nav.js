$(document).ready(function() {
	var menuLinks = `
	<a style="color: #fff; padding: 5px;" href="index.html">Главная</a>
	<a style="color: #fff; padding: 5px;" href="login.html">Авторизация</a>
	<a style="color: #fff; padding: 5px;" href="register.html">Регистрация</a>
	<a style="color: #fff; padding: 5px;" href="unicef-kz.html">Unicef KZ</a>
	<a style="color: #fff; padding: 5px;" href="plastic.html">Plastic</a>
	<a style="color: #fff; padding: 5px;" href="genu.html">GENU</a>
	<a style="color: #fff; padding: 5px;" href="be-kind.html">Be Kind</a>
	<a style="color: #fff; padding: 5px;" href="admin-lessons.html">Администратор:уроки</a>
	<a style="color: #fff; padding: 5px;" href="admin-volont.html">Администратор:волонтеры</a>
	<a style="color: #fff; padding: 5px;" href="admin-student.html">Администратор:ученики</a>
	<a style="color: #fff; padding: 5px;" href="admin-school.html">Администратор:школы</a>
	<a style="color: #fff; padding: 5px;" href="admin-timetable.html">Администратор:расписание</a>
	<a style="color: #fff; padding: 5px;" href="admin-users.html">Администратор:пользователи</a>
	<a style="color: #fff; padding: 5px;" href="admin-pages.html">Администратор:страницы</a>
	<a style="color: #fff; padding: 5px;" href="admin-pages-add.html">Администратор:страницы добавить</a>
	<a style="color: #fff; padding: 5px;" href="volont-slider.html">Волонтеры:главная слайдер</a>
	<a style="color: #fff; padding: 5px;" href="volont-main.html">Волонтеры:главная</a>
	<a style="color: #fff; padding: 5px;" href="volont-settings.html">Волонтеры:настройки</a>
	<a style="color: #fff; padding: 5px;" href="volont-timetable.html">Волонтеры:расписание</a>
	<a style="color: #fff; padding: 5px;" href="volont-timetable-empty.html">Волонтеры:расписание пусто</a>
	<a style="color: #fff; padding: 5px;" href="#" data-open="modal-request">Модальное окно:подать заявку</a>
	<a style="color: #fff; padding: 5px;" href="#" data-open="modal-anketa">Модальное окно:анкета</a>
	<a style="color: #fff; padding: 5px;" href="#" data-open="modal-write">Модальное окно:оставить отзыв</a>
	<a style="color: #fff; padding: 5px;" href="#" data-open="modal-delete">Модальное окно:удалить</a>
	<a style="color: #fff; padding: 5px;" href="kurator-main.html">Куратор:главная</a>
	<a style="color: #fff; padding: 5px;" href="kurator-settings.html">Куратор:настройки</a>
	<a style="color: #fff; padding: 5px;" href="kurator-timetable.html">Куратор расписание занятий: таблица</a>
	<a style="color: #fff; padding: 5px;" href="kurator-timetable-calendar.html">Куратор расписание занятий: календарь</a>
	<a style="color: #fff; padding: 5px;" href="kurator-timetable-empty.html">Куратор расписание занятий: пусто</a>
	<a style="color: #fff; padding: 5px;" href="kurator-add.html">Куратор:добавить урок</a>
	<a style="color: #fff; padding: 5px;" href="kurator-class-empty.html">Куратор:класс пусто</a>
	<a style="color: #fff; padding: 5px;" href="kurator-class.html">Куратор:класс</a>
	<a style="color: #fff; padding: 5px;" href="kurator-class-add.html">Куратор:класс добавить</a>
	<a style="color: #fff; padding: 5px;" href="contacts.html">Контакты</a>
	<a style="color: #fff; padding: 5px;" href="error.html">404</a>
	<a style="color: #fff; padding: 5px;" href="unavailable.html">Портал недоступен</a>
	<a style="color: #fff; padding: 5px;" href="thanks.html">Спасибо!</a>
	<a style="color: #fff; padding: 5px;" href="volunteers.html">Волонтеры</a>
	<a style="color: #fff; padding: 5px;" href="schools.html">Школы</a>
	<a style="color: #fff; padding: 5px;" href="calendar.html">Календарь лекций</a>
	<a style="color: #fff; padding: 5px;" href="gallery.html">Галерея</a>
	<a style="color: #fff; padding: 5px;" href="gallery-full.html">Галерея внутренняя</a>
	<a style="color: #fff; padding: 5px;" href="volont-data.html">Волонтеры База знаний</a>
	<a style="color: #fff; padding: 5px;" href="volont-data-article.html">Волонтеры База знаний Статья</a>
	`;


	var styleNavdd = 'width: 40px;height: 40px;border-radius: 50%;right: 5px;bottom: 5px;background-color: rgba(133,143,159, 0.6);position: fixed;color: #fff;display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-align: center;-ms-flex-align: center;align-items: center;-webkit-box-pack: center;-ms-flex-pack: center;justify-content: center;-webkit-transition: 0.4s;-o-transition: 0.4s;transition: 0.4s;z-index: 300;';
	var styleNavddActive = 'width: 100%;height: 100%;border-radius: 0px;right: 0px;bottom: 0px;background-color: rgba(133,143,159, 0.95);position: fixed;color: #fff;display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-align: center;-ms-flex-align: center; align-items: center;-webkit-box-pack: center;-ms-flex-pack: center;justify-content: center;-webkit-transition: 0.4s;-o-transition: 0.4s;transition: 0.4s;z-index: 300; overflow-y: scroll;';
	var styleNavdd_span = 'display: block;';
	var styleNavdd_spanActive = 'display: none;';
	var styleNavdd_div = 'pointer-events: none;opacity: 0;display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-orient: vertical;-webkit-box-direction: normal;-ms-flex-direction: column;flex-direction: column;position: absolute;overflow-x: hidden;overflow-y: auto;';
	var styleNavdd_divActive = 'pointer-events: auto;opacity: 1;width: 1000px;height: 100%;position: static;display: -webkit-box;display: -ms-flexbox;display: flex;-webkit-box-orient: vertical;-webkit-box-direction: normal;-ms-flex-direction: column;flex-direction: column;overflow-x: hidden;overflow-y: auto;';
	$('body').append('<div class="nav-d-d"><span>nav</span><div>'+menuLinks+'</div></div>');
	$('.nav-d-d').attr('style', styleNavdd).find('span').attr('style', styleNavdd_span).siblings('div').attr('style', styleNavdd_div);
	$('.nav-d-d').on('click',function(){
		if($(this).is('.active')){
			$(this).removeClass('active');
			$('.nav-d-d').attr('style', styleNavdd).find('span').attr('style', styleNavdd_span).siblings('div').attr('style', styleNavdd_div);
		}else{
			$(this).addClass('active');
			$('.nav-d-d').attr('style', styleNavddActive).find('span').attr('style', styleNavdd_spanActive).siblings('div').attr('style', styleNavdd_divActive);
		}
	})
});
