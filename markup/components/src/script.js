$(document).ready(function() {
	initSelects();
	initSliders();
	openLangSwitcher();
	tabs();
	openProfileSettings();
	selectAllCheckbox();
	actionsAfterCheck();
	openActions();
	loadFile();
	getDatesToGraph();
	filterGraph();
	modals();
	openTableActions();
	openMobileMenu();
	initMaps();
	scrollToNumbers();
	addSchool();

	// const defaultDatepicker = $('.datepicker-here').datepicker({
	// 	autoClose: true,
	// });

	$('.datepicker-here').each(function() {
		const defaultDatepicker = $(this).datepicker({
			autoClose: true,
		})

		$(this).on('input', function () {
			defaultDatepicker.setViewDate($(this).val())
		});
	})

	$('.monthpicker').datepicker({
		autoClose: true,
		view: 'months',
		minView: 'months',
		dateFormat: 'MM yyyy'
	});

	$('#not-patronymic').change(function() {
		let checked = $(this).prop('checked');
		$(this).parent('.radio').siblings('.input').find('input').attr('disabled', checked)
	});

	$('.article-play').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		helpers : {
			media : {}
		}
	});
});

function openLangSwitcher() {
	$('body').on('click','.lang-current',function(){
		$(this).addClass('active');
		$(this).siblings('.lang-container').slideToggle();
	});
}

function initSliders() {
	const mainSlider = new Swiper('.main-container', {
		slidesPerView: 1,
		spaceBetween: 20,
		navigation: {
			prevEl: '.main-left',
			nextEl: '.main-right'
		}
	});

	const totalSlide = $('.main-container .swiper-slide').length;
	$('.main-container .swiper-slide').each(function(index){
		$(this).find('.slider-current').text(index + 1);
		$(this).find('.slider-total').text(totalSlide);
	});

	const directionSlider = new Swiper('.direction .swiper-container', {
		slidesPerView: 'auto'
	});

	const partnersSlider = new Swiper('.partners .swiper-container', {
		slidesPerView: 'auto',
		navigation: {
			prevEl: '.partners-left',
			nextEl: '.partners-right'
		}
	});

	const volontSlider = new Swiper('.volont-slider .swiper-container', {
		slidesPerView: 'auto',
		pagination: {
			el: '.volont-pagination'
		},
		navigation: {
			nextEl: '.volont-next'
		}
	});

	volontSlider.on('slideChange', function () {
		$('.volont-next').removeClass(`slide-${volontSlider.activeIndex}`)
		$('.volont-next').removeClass(`slide-${volontSlider.activeIndex + 2}`)
		$('.volont-next').addClass(`slide-${volontSlider.activeIndex + 1}`)
	});

};

function initSelects() {
	$('.nice-select').each(function() {
		$(this).niceSelect()
	})

	$('.sl2').each(function() {
		$(this).select2({
			minimumResultsForSearch: Infinity,
			placeholder: $(this).data('placeholder')
		});
	});

	$('.school-select').each(function() {
		$(this).select2({
			placeholder: $(this).data('placeholder'),
			dropdownCssClass: "school-dropdown"
		});
	});

};

function tabs() {
	$('.auth-list').on('click', 'li:not(.active)', function(e) {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.auth').find('.auth-body').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('.admin-list').on('click', 'li:not(.active)', function(e) {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.admin').find('.admin-tab').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('.contacts-list').on('click', 'li:not(.active)', function(e) {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.contacts').find('.contacts-tab').removeClass('active').eq($(this).index()).addClass('active');
	});

	$('.data-list').on('click', 'li:not(.active)', function(e) {
		$(this)
		.addClass('active').siblings().removeClass('active')
		.closest('.admin').find('.data-tab').removeClass('active').eq($(this).index()).addClass('active');
	});
}

function openProfileSettings() {
	$('.profile').on('click', function() {
		$('.profile-settings').slideToggle(400);
		$(this).toggleClass('active');
	})
}

function selectAllCheckbox() {
	$('.table th .check input').change(function() {
		let checked = $(this).prop('checked');
		$('.table td .check input').prop('checked', checked);
		$('.table td .check input').trigger('change')
	})
}

function actionsAfterCheck() {
	$('.table td .check input').change(function() {
		let allInputLength = $('.table td .check input').length;
		let checkedLength = $('.table td .check input:checked').length;

		$(this).parents('.table').find('.actions').addClass('active');

		$('#current').text(checkedLength);
		$('#all').text(allInputLength);

		if (checkedLength === 0) {
			$(this).parents('.table').find('.actions').removeClass('active');
		}

		if (checkedLength === allInputLength) {
			$('.table th .check input').prop('checked', true);
		} else {
			$('.table th .check input').prop('checked', false);
		}
	})
}

function openActions() {
	$('.actions-btn').on('click', function() {
		$(this).toggleClass('active')
		$('.actions-drop').toggleClass('active')
	})
}

function loadFile() {
	$('#file').change(function(e) {
		let file = e.target.files[0]
		$(this).next('.btn-text').text(file.name)
	})
}

function getDatesToGraph(lng = 14) {
	const startDate = new Date();
	const endDate = startDate * 1 + lng * 24 * 3600 * 1000
	let template = '';

	const days = [
	'Вc',
	'Пн',
	'Вт',
	'Ср',
	'Чт',
	'Пт',
	'Сб'
	];

	const months = [
	'января',
	'февраля',
	'марта',
	'апреля',
	'мая',
	'июня',
	'июля',
	'августа',
	'сентября',
	'октября',
	'ноября',
	'декабря',
	]

	const values = [
	'4',
	'2',
	'5'
	]

	function getDates(d1, d2) {
		var oneDay = 24 * 3600 * 1000;
		for (let ms = d1 * 1, last = d2 * 1; ms < last; ms += oneDay){
			template += `
			<div class="graph-day" style="z-index: ${new Date(ms).getDate()}">
			<div class="graph-wrap" style="height: ${values[new Date(ms).getDay()] * 45}px">
			<div class="graph-value"></div>
			<div class="help">
			<div class="help-text">
			<div class="help-date">${new Date(ms).getDate()} ${months[new Date(ms).getMonth()]}</div>
			<ul class="help-list">
			<li>10:00 / 1-4 класс</li>
			<li>12:00 / 9 Б класс</li>
			<li>14:00 / 5-11 класс</li>
			<li>16:00 / 1-4 класс</li>
			</ul>
			</div>
			</div>
			</div>
			<div class="graph-weekday">${days[new Date(ms).getDay()]}</div>
			<div class="graph-date">${new Date(ms).getDate()}</div>
			</div>
			`
		}
	}

	getDates( startDate, endDate )

	$('.graph-bottom').html(template)
}

function filterGraph() {
	$('.graph-setting').each(function() {
		$(this).on('click', function() {
			$('.graph-setting').removeClass('active');
			$(this).addClass('active');
			getDatesToGraph($(this).data('lng'))
		})
	})
}


let modal = $('.modal');
let overlay = $('.overlay');
let close = $('.modal-close, .modal-cancel');
let width = $(window).width();

function modals() {
	$(document).on('click', '.modal-close, .modal-cancel', function (event) {
		event.preventDefault();
		modalClose();
	});
	// close.on('click', function(event){
	// 	event.preventDefault();
	// 	modalClose();
	// });
	overlay.on('mousedown', function(event){
		if (event.target !== modal && modal.has(event.target).length === 0) {
			modalClose();
		}
	});
	$('body').on('click','[data-open]',function(e){
		e.preventDefault();
		var modalName = $(this).data('open');
		modalOpen(modalName);
	});
}

function modalOpen(modalName){
	$('body').css('width', width);
	$('body').addClass('no-scroll');
	overlay.addClass('active');
	modal.removeClass('active');
	$('.'+modalName).addClass('active');
}

function modalClose(){
	modal.removeClass('active');
	overlay.removeClass('active');
	$('body').removeClass('no-scroll');
};

function openTableActions() {
	$('.table-more').each(function() {
		$(this).on('click', function(){
			if ($(this).next('.table-drop').hasClass('active')) {
				$('.table-drop').removeClass('active')
			} else {
				$('.table-drop').removeClass('active')
				$(this).next('.table-drop').addClass('active')
			}
		})
	})

	// $('body').on('click', function(e) {
	// 	if (e.target !== $('.table-more') && $('.table-more').has(e.target).length === 0) {
	// 		$('.table-drop').removeClass('active')
	// 	}
	// })
}

function openMobileMenu() {
	$('.burger').on('click', function() {
		$('.mobile-menu').addClass('active')
	});

	$('.mobile-close').on('click', function() {
		$('.mobile-menu').removeClass('active')
	})
}

function initMaps() {
	if ($('#map-1').is('#map-1')) {
		ymaps.ready(init);
		const lat = $('#map-1').data('lat');
		const lng = $('#map-1').data('lng');
		const zoom = $('#map-1').data('zoom');

		function init() {
			const map = new ymaps.Map('map-1', {
				center: [lng, lat],
				zoom: zoom,
				controls: [],
				behaviors: ['drag']
			});

			const placemark = new ymaps.Placemark([lng, lat], {
				hintContent: ''
			},
			{
				iconLayout: 'default#image'
			});

			map.geoObjects.add(placemark)
		}
	}
	if ($('#map-2').is('#map-2')) {
		ymaps.ready(init);
		const lat = $('#map-2').data('lat');
		const lng = $('#map-2').data('lng');
		const zoom = $('#map-2').data('zoom');

		function init() {
			const map = new ymaps.Map('map-2', {
				center: [lng, lat],
				zoom: zoom,
				controls: [],
				behaviors: ['drag']
			});

			const placemark = new ymaps.Placemark([lng, lat], {
				hintContent: ''
			},
			{
				iconLayout: 'default#image'
			});

			map.geoObjects.add(placemark)
		}
	}
}

function animateNumbers () {
	$('.num').each(function() {
		const num = +$(this).data('num');

		$(this).animateNumber(
		{
			number: num,
			numberStep: function(now, tween) {
				var floored_number = Math.floor(now),
				target = $(tween.elem);

				target.find('span').text(floored_number);
			}
		},
		{
			easing: 'swing',
			duration: 1000
		});
	})
}

function scrollToNumbers() {
	var scrollDone = false;

	$(window).on('scroll load resize', function(e) {
		let h = $(window).height();
		let top = $(window).scrollTop()
		let numbers = $('.numbers'); 
		if (numbers.is('.numbers')) {
			if (top > numbers.offset().top - h) {
				if (!scrollDone) {
					animateNumbers();
					scrollDone = true;
				}
			}
		}
	})
}

function addSchool() {
	const btn = $('#add-school');
	const template = $('.school-item').html();

	btn.on('click', function() {
		$(this).parents('.admin-tab').append(
			`<form class="school-item">
				${template}
			</form>`
		)
	})
}