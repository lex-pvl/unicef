"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*! jQuery v3.4.1 | (c) JS Foundation and other contributors | jquery.org/license */
!function (e, t) {
  "use strict";

  "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && "object" == _typeof(module.exports) ? module.exports = e.document ? t(e, !0) : function (e) {
    if (!e.document) throw new Error("jQuery requires a window with a document");
    return t(e);
  } : t(e);
}("undefined" != typeof window ? window : void 0, function (C, e) {
  "use strict";

  var t = [],
      E = C.document,
      r = Object.getPrototypeOf,
      s = t.slice,
      g = t.concat,
      u = t.push,
      i = t.indexOf,
      n = {},
      o = n.toString,
      v = n.hasOwnProperty,
      a = v.toString,
      l = a.call(Object),
      y = {},
      m = function m(e) {
    return "function" == typeof e && "number" != typeof e.nodeType;
  },
      x = function x(e) {
    return null != e && e === e.window;
  },
      c = {
    type: !0,
    src: !0,
    nonce: !0,
    noModule: !0
  };

  function b(e, t, n) {
    var r,
        i,
        o = (n = n || E).createElement("script");
    if (o.text = e, t) for (r in c) {
      (i = t[r] || t.getAttribute && t.getAttribute(r)) && o.setAttribute(r, i);
    }
    n.head.appendChild(o).parentNode.removeChild(o);
  }

  function w(e) {
    return null == e ? e + "" : "object" == _typeof(e) || "function" == typeof e ? n[o.call(e)] || "object" : _typeof(e);
  }

  var f = "3.4.1",
      k = function k(e, t) {
    return new k.fn.init(e, t);
  },
      p = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

  function d(e) {
    var t = !!e && "length" in e && e.length,
        n = w(e);
    return !m(e) && !x(e) && ("array" === n || 0 === t || "number" == typeof t && 0 < t && t - 1 in e);
  }

  k.fn = k.prototype = {
    jquery: f,
    constructor: k,
    length: 0,
    toArray: function toArray() {
      return s.call(this);
    },
    get: function get(e) {
      return null == e ? s.call(this) : e < 0 ? this[e + this.length] : this[e];
    },
    pushStack: function pushStack(e) {
      var t = k.merge(this.constructor(), e);
      return t.prevObject = this, t;
    },
    each: function each(e) {
      return k.each(this, e);
    },
    map: function map(n) {
      return this.pushStack(k.map(this, function (e, t) {
        return n.call(e, t, e);
      }));
    },
    slice: function slice() {
      return this.pushStack(s.apply(this, arguments));
    },
    first: function first() {
      return this.eq(0);
    },
    last: function last() {
      return this.eq(-1);
    },
    eq: function eq(e) {
      var t = this.length,
          n = +e + (e < 0 ? t : 0);
      return this.pushStack(0 <= n && n < t ? [this[n]] : []);
    },
    end: function end() {
      return this.prevObject || this.constructor();
    },
    push: u,
    sort: t.sort,
    splice: t.splice
  }, k.extend = k.fn.extend = function () {
    var e,
        t,
        n,
        r,
        i,
        o,
        a = arguments[0] || {},
        s = 1,
        u = arguments.length,
        l = !1;

    for ("boolean" == typeof a && (l = a, a = arguments[s] || {}, s++), "object" == _typeof(a) || m(a) || (a = {}), s === u && (a = this, s--); s < u; s++) {
      if (null != (e = arguments[s])) for (t in e) {
        r = e[t], "__proto__" !== t && a !== r && (l && r && (k.isPlainObject(r) || (i = Array.isArray(r))) ? (n = a[t], o = i && !Array.isArray(n) ? [] : i || k.isPlainObject(n) ? n : {}, i = !1, a[t] = k.extend(l, o, r)) : void 0 !== r && (a[t] = r));
      }
    }

    return a;
  }, k.extend({
    expando: "jQuery" + (f + Math.random()).replace(/\D/g, ""),
    isReady: !0,
    error: function error(e) {
      throw new Error(e);
    },
    noop: function noop() {},
    isPlainObject: function isPlainObject(e) {
      var t, n;
      return !(!e || "[object Object]" !== o.call(e)) && (!(t = r(e)) || "function" == typeof (n = v.call(t, "constructor") && t.constructor) && a.call(n) === l);
    },
    isEmptyObject: function isEmptyObject(e) {
      var t;

      for (t in e) {
        return !1;
      }

      return !0;
    },
    globalEval: function globalEval(e, t) {
      b(e, {
        nonce: t && t.nonce
      });
    },
    each: function each(e, t) {
      var n,
          r = 0;

      if (d(e)) {
        for (n = e.length; r < n; r++) {
          if (!1 === t.call(e[r], r, e[r])) break;
        }
      } else for (r in e) {
        if (!1 === t.call(e[r], r, e[r])) break;
      }

      return e;
    },
    trim: function trim(e) {
      return null == e ? "" : (e + "").replace(p, "");
    },
    makeArray: function makeArray(e, t) {
      var n = t || [];
      return null != e && (d(Object(e)) ? k.merge(n, "string" == typeof e ? [e] : e) : u.call(n, e)), n;
    },
    inArray: function inArray(e, t, n) {
      return null == t ? -1 : i.call(t, e, n);
    },
    merge: function merge(e, t) {
      for (var n = +t.length, r = 0, i = e.length; r < n; r++) {
        e[i++] = t[r];
      }

      return e.length = i, e;
    },
    grep: function grep(e, t, n) {
      for (var r = [], i = 0, o = e.length, a = !n; i < o; i++) {
        !t(e[i], i) !== a && r.push(e[i]);
      }

      return r;
    },
    map: function map(e, t, n) {
      var r,
          i,
          o = 0,
          a = [];
      if (d(e)) for (r = e.length; o < r; o++) {
        null != (i = t(e[o], o, n)) && a.push(i);
      } else for (o in e) {
        null != (i = t(e[o], o, n)) && a.push(i);
      }
      return g.apply([], a);
    },
    guid: 1,
    support: y
  }), "function" == typeof Symbol && (k.fn[Symbol.iterator] = t[Symbol.iterator]), k.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function (e, t) {
    n["[object " + t + "]"] = t.toLowerCase();
  });

  var h = function (n) {
    var e,
        d,
        b,
        o,
        i,
        h,
        f,
        g,
        w,
        u,
        l,
        T,
        C,
        a,
        E,
        v,
        s,
        c,
        y,
        k = "sizzle" + 1 * new Date(),
        m = n.document,
        S = 0,
        r = 0,
        p = ue(),
        x = ue(),
        N = ue(),
        A = ue(),
        D = function D(e, t) {
      return e === t && (l = !0), 0;
    },
        j = {}.hasOwnProperty,
        t = [],
        q = t.pop,
        L = t.push,
        H = t.push,
        O = t.slice,
        P = function P(e, t) {
      for (var n = 0, r = e.length; n < r; n++) {
        if (e[n] === t) return n;
      }

      return -1;
    },
        R = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
        M = "[\\x20\\t\\r\\n\\f]",
        I = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",
        W = "\\[" + M + "*(" + I + ")(?:" + M + "*([*^$|!~]?=)" + M + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + I + "))|)" + M + "*\\]",
        $ = ":(" + I + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + W + ")*)|.*)\\)|)",
        F = new RegExp(M + "+", "g"),
        B = new RegExp("^" + M + "+|((?:^|[^\\\\])(?:\\\\.)*)" + M + "+$", "g"),
        _ = new RegExp("^" + M + "*," + M + "*"),
        z = new RegExp("^" + M + "*([>+~]|" + M + ")" + M + "*"),
        U = new RegExp(M + "|>"),
        X = new RegExp($),
        V = new RegExp("^" + I + "$"),
        G = {
      ID: new RegExp("^#(" + I + ")"),
      CLASS: new RegExp("^\\.(" + I + ")"),
      TAG: new RegExp("^(" + I + "|[*])"),
      ATTR: new RegExp("^" + W),
      PSEUDO: new RegExp("^" + $),
      CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + M + "*(even|odd|(([+-]|)(\\d*)n|)" + M + "*(?:([+-]|)" + M + "*(\\d+)|))" + M + "*\\)|)", "i"),
      bool: new RegExp("^(?:" + R + ")$", "i"),
      needsContext: new RegExp("^" + M + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + M + "*((?:-\\d)?\\d*)" + M + "*\\)|)(?=[^-]|$)", "i")
    },
        Y = /HTML$/i,
        Q = /^(?:input|select|textarea|button)$/i,
        J = /^h\d$/i,
        K = /^[^{]+\{\s*\[native \w/,
        Z = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
        ee = /[+~]/,
        te = new RegExp("\\\\([\\da-f]{1,6}" + M + "?|(" + M + ")|.)", "ig"),
        ne = function ne(e, t, n) {
      var r = "0x" + t - 65536;
      return r != r || n ? t : r < 0 ? String.fromCharCode(r + 65536) : String.fromCharCode(r >> 10 | 55296, 1023 & r | 56320);
    },
        re = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
        ie = function ie(e, t) {
      return t ? "\0" === e ? "\uFFFD" : e.slice(0, -1) + "\\" + e.charCodeAt(e.length - 1).toString(16) + " " : "\\" + e;
    },
        oe = function oe() {
      T();
    },
        ae = be(function (e) {
      return !0 === e.disabled && "fieldset" === e.nodeName.toLowerCase();
    }, {
      dir: "parentNode",
      next: "legend"
    });

    try {
      H.apply(t = O.call(m.childNodes), m.childNodes), t[m.childNodes.length].nodeType;
    } catch (e) {
      H = {
        apply: t.length ? function (e, t) {
          L.apply(e, O.call(t));
        } : function (e, t) {
          var n = e.length,
              r = 0;

          while (e[n++] = t[r++]) {
            ;
          }

          e.length = n - 1;
        }
      };
    }

    function se(t, e, n, r) {
      var i,
          o,
          a,
          s,
          u,
          l,
          c,
          f = e && e.ownerDocument,
          p = e ? e.nodeType : 9;
      if (n = n || [], "string" != typeof t || !t || 1 !== p && 9 !== p && 11 !== p) return n;

      if (!r && ((e ? e.ownerDocument || e : m) !== C && T(e), e = e || C, E)) {
        if (11 !== p && (u = Z.exec(t))) if (i = u[1]) {
          if (9 === p) {
            if (!(a = e.getElementById(i))) return n;
            if (a.id === i) return n.push(a), n;
          } else if (f && (a = f.getElementById(i)) && y(e, a) && a.id === i) return n.push(a), n;
        } else {
          if (u[2]) return H.apply(n, e.getElementsByTagName(t)), n;
          if ((i = u[3]) && d.getElementsByClassName && e.getElementsByClassName) return H.apply(n, e.getElementsByClassName(i)), n;
        }

        if (d.qsa && !A[t + " "] && (!v || !v.test(t)) && (1 !== p || "object" !== e.nodeName.toLowerCase())) {
          if (c = t, f = e, 1 === p && U.test(t)) {
            (s = e.getAttribute("id")) ? s = s.replace(re, ie) : e.setAttribute("id", s = k), o = (l = h(t)).length;

            while (o--) {
              l[o] = "#" + s + " " + xe(l[o]);
            }

            c = l.join(","), f = ee.test(t) && ye(e.parentNode) || e;
          }

          try {
            return H.apply(n, f.querySelectorAll(c)), n;
          } catch (e) {
            A(t, !0);
          } finally {
            s === k && e.removeAttribute("id");
          }
        }
      }

      return g(t.replace(B, "$1"), e, n, r);
    }

    function ue() {
      var r = [];
      return function e(t, n) {
        return r.push(t + " ") > b.cacheLength && delete e[r.shift()], e[t + " "] = n;
      };
    }

    function le(e) {
      return e[k] = !0, e;
    }

    function ce(e) {
      var t = C.createElement("fieldset");

      try {
        return !!e(t);
      } catch (e) {
        return !1;
      } finally {
        t.parentNode && t.parentNode.removeChild(t), t = null;
      }
    }

    function fe(e, t) {
      var n = e.split("|"),
          r = n.length;

      while (r--) {
        b.attrHandle[n[r]] = t;
      }
    }

    function pe(e, t) {
      var n = t && e,
          r = n && 1 === e.nodeType && 1 === t.nodeType && e.sourceIndex - t.sourceIndex;
      if (r) return r;
      if (n) while (n = n.nextSibling) {
        if (n === t) return -1;
      }
      return e ? 1 : -1;
    }

    function de(t) {
      return function (e) {
        return "input" === e.nodeName.toLowerCase() && e.type === t;
      };
    }

    function he(n) {
      return function (e) {
        var t = e.nodeName.toLowerCase();
        return ("input" === t || "button" === t) && e.type === n;
      };
    }

    function ge(t) {
      return function (e) {
        return "form" in e ? e.parentNode && !1 === e.disabled ? "label" in e ? "label" in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && ae(e) === t : e.disabled === t : "label" in e && e.disabled === t;
      };
    }

    function ve(a) {
      return le(function (o) {
        return o = +o, le(function (e, t) {
          var n,
              r = a([], e.length, o),
              i = r.length;

          while (i--) {
            e[n = r[i]] && (e[n] = !(t[n] = e[n]));
          }
        });
      });
    }

    function ye(e) {
      return e && "undefined" != typeof e.getElementsByTagName && e;
    }

    for (e in d = se.support = {}, i = se.isXML = function (e) {
      var t = e.namespaceURI,
          n = (e.ownerDocument || e).documentElement;
      return !Y.test(t || n && n.nodeName || "HTML");
    }, T = se.setDocument = function (e) {
      var t,
          n,
          r = e ? e.ownerDocument || e : m;
      return r !== C && 9 === r.nodeType && r.documentElement && (a = (C = r).documentElement, E = !i(C), m !== C && (n = C.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", oe, !1) : n.attachEvent && n.attachEvent("onunload", oe)), d.attributes = ce(function (e) {
        return e.className = "i", !e.getAttribute("className");
      }), d.getElementsByTagName = ce(function (e) {
        return e.appendChild(C.createComment("")), !e.getElementsByTagName("*").length;
      }), d.getElementsByClassName = K.test(C.getElementsByClassName), d.getById = ce(function (e) {
        return a.appendChild(e).id = k, !C.getElementsByName || !C.getElementsByName(k).length;
      }), d.getById ? (b.filter.ID = function (e) {
        var t = e.replace(te, ne);
        return function (e) {
          return e.getAttribute("id") === t;
        };
      }, b.find.ID = function (e, t) {
        if ("undefined" != typeof t.getElementById && E) {
          var n = t.getElementById(e);
          return n ? [n] : [];
        }
      }) : (b.filter.ID = function (e) {
        var n = e.replace(te, ne);
        return function (e) {
          var t = "undefined" != typeof e.getAttributeNode && e.getAttributeNode("id");
          return t && t.value === n;
        };
      }, b.find.ID = function (e, t) {
        if ("undefined" != typeof t.getElementById && E) {
          var n,
              r,
              i,
              o = t.getElementById(e);

          if (o) {
            if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
            i = t.getElementsByName(e), r = 0;

            while (o = i[r++]) {
              if ((n = o.getAttributeNode("id")) && n.value === e) return [o];
            }
          }

          return [];
        }
      }), b.find.TAG = d.getElementsByTagName ? function (e, t) {
        return "undefined" != typeof t.getElementsByTagName ? t.getElementsByTagName(e) : d.qsa ? t.querySelectorAll(e) : void 0;
      } : function (e, t) {
        var n,
            r = [],
            i = 0,
            o = t.getElementsByTagName(e);

        if ("*" === e) {
          while (n = o[i++]) {
            1 === n.nodeType && r.push(n);
          }

          return r;
        }

        return o;
      }, b.find.CLASS = d.getElementsByClassName && function (e, t) {
        if ("undefined" != typeof t.getElementsByClassName && E) return t.getElementsByClassName(e);
      }, s = [], v = [], (d.qsa = K.test(C.querySelectorAll)) && (ce(function (e) {
        a.appendChild(e).innerHTML = "<a id='" + k + "'></a><select id='" + k + "-\r\\' msallowcapture=''><option selected=''></option></select>", e.querySelectorAll("[msallowcapture^='']").length && v.push("[*^$]=" + M + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || v.push("\\[" + M + "*(?:value|" + R + ")"), e.querySelectorAll("[id~=" + k + "-]").length || v.push("~="), e.querySelectorAll(":checked").length || v.push(":checked"), e.querySelectorAll("a#" + k + "+*").length || v.push(".#.+[+~]");
      }), ce(function (e) {
        e.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
        var t = C.createElement("input");
        t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && v.push("name" + M + "*[*^$|!~]?="), 2 !== e.querySelectorAll(":enabled").length && v.push(":enabled", ":disabled"), a.appendChild(e).disabled = !0, 2 !== e.querySelectorAll(":disabled").length && v.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), v.push(",.*:");
      })), (d.matchesSelector = K.test(c = a.matches || a.webkitMatchesSelector || a.mozMatchesSelector || a.oMatchesSelector || a.msMatchesSelector)) && ce(function (e) {
        d.disconnectedMatch = c.call(e, "*"), c.call(e, "[s!='']:x"), s.push("!=", $);
      }), v = v.length && new RegExp(v.join("|")), s = s.length && new RegExp(s.join("|")), t = K.test(a.compareDocumentPosition), y = t || K.test(a.contains) ? function (e, t) {
        var n = 9 === e.nodeType ? e.documentElement : e,
            r = t && t.parentNode;
        return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)));
      } : function (e, t) {
        if (t) while (t = t.parentNode) {
          if (t === e) return !0;
        }
        return !1;
      }, D = t ? function (e, t) {
        if (e === t) return l = !0, 0;
        var n = !e.compareDocumentPosition - !t.compareDocumentPosition;
        return n || (1 & (n = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1) || !d.sortDetached && t.compareDocumentPosition(e) === n ? e === C || e.ownerDocument === m && y(m, e) ? -1 : t === C || t.ownerDocument === m && y(m, t) ? 1 : u ? P(u, e) - P(u, t) : 0 : 4 & n ? -1 : 1);
      } : function (e, t) {
        if (e === t) return l = !0, 0;
        var n,
            r = 0,
            i = e.parentNode,
            o = t.parentNode,
            a = [e],
            s = [t];
        if (!i || !o) return e === C ? -1 : t === C ? 1 : i ? -1 : o ? 1 : u ? P(u, e) - P(u, t) : 0;
        if (i === o) return pe(e, t);
        n = e;

        while (n = n.parentNode) {
          a.unshift(n);
        }

        n = t;

        while (n = n.parentNode) {
          s.unshift(n);
        }

        while (a[r] === s[r]) {
          r++;
        }

        return r ? pe(a[r], s[r]) : a[r] === m ? -1 : s[r] === m ? 1 : 0;
      }), C;
    }, se.matches = function (e, t) {
      return se(e, null, null, t);
    }, se.matchesSelector = function (e, t) {
      if ((e.ownerDocument || e) !== C && T(e), d.matchesSelector && E && !A[t + " "] && (!s || !s.test(t)) && (!v || !v.test(t))) try {
        var n = c.call(e, t);
        if (n || d.disconnectedMatch || e.document && 11 !== e.document.nodeType) return n;
      } catch (e) {
        A(t, !0);
      }
      return 0 < se(t, C, null, [e]).length;
    }, se.contains = function (e, t) {
      return (e.ownerDocument || e) !== C && T(e), y(e, t);
    }, se.attr = function (e, t) {
      (e.ownerDocument || e) !== C && T(e);
      var n = b.attrHandle[t.toLowerCase()],
          r = n && j.call(b.attrHandle, t.toLowerCase()) ? n(e, t, !E) : void 0;
      return void 0 !== r ? r : d.attributes || !E ? e.getAttribute(t) : (r = e.getAttributeNode(t)) && r.specified ? r.value : null;
    }, se.escape = function (e) {
      return (e + "").replace(re, ie);
    }, se.error = function (e) {
      throw new Error("Syntax error, unrecognized expression: " + e);
    }, se.uniqueSort = function (e) {
      var t,
          n = [],
          r = 0,
          i = 0;

      if (l = !d.detectDuplicates, u = !d.sortStable && e.slice(0), e.sort(D), l) {
        while (t = e[i++]) {
          t === e[i] && (r = n.push(i));
        }

        while (r--) {
          e.splice(n[r], 1);
        }
      }

      return u = null, e;
    }, o = se.getText = function (e) {
      var t,
          n = "",
          r = 0,
          i = e.nodeType;

      if (i) {
        if (1 === i || 9 === i || 11 === i) {
          if ("string" == typeof e.textContent) return e.textContent;

          for (e = e.firstChild; e; e = e.nextSibling) {
            n += o(e);
          }
        } else if (3 === i || 4 === i) return e.nodeValue;
      } else while (t = e[r++]) {
        n += o(t);
      }

      return n;
    }, (b = se.selectors = {
      cacheLength: 50,
      createPseudo: le,
      match: G,
      attrHandle: {},
      find: {},
      relative: {
        ">": {
          dir: "parentNode",
          first: !0
        },
        " ": {
          dir: "parentNode"
        },
        "+": {
          dir: "previousSibling",
          first: !0
        },
        "~": {
          dir: "previousSibling"
        }
      },
      preFilter: {
        ATTR: function ATTR(e) {
          return e[1] = e[1].replace(te, ne), e[3] = (e[3] || e[4] || e[5] || "").replace(te, ne), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4);
        },
        CHILD: function CHILD(e) {
          return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || se.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && se.error(e[0]), e;
        },
        PSEUDO: function PSEUDO(e) {
          var t,
              n = !e[6] && e[2];
          return G.CHILD.test(e[0]) ? null : (e[3] ? e[2] = e[4] || e[5] || "" : n && X.test(n) && (t = h(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3));
        }
      },
      filter: {
        TAG: function TAG(e) {
          var t = e.replace(te, ne).toLowerCase();
          return "*" === e ? function () {
            return !0;
          } : function (e) {
            return e.nodeName && e.nodeName.toLowerCase() === t;
          };
        },
        CLASS: function CLASS(e) {
          var t = p[e + " "];
          return t || (t = new RegExp("(^|" + M + ")" + e + "(" + M + "|$)")) && p(e, function (e) {
            return t.test("string" == typeof e.className && e.className || "undefined" != typeof e.getAttribute && e.getAttribute("class") || "");
          });
        },
        ATTR: function ATTR(n, r, i) {
          return function (e) {
            var t = se.attr(e, n);
            return null == t ? "!=" === r : !r || (t += "", "=" === r ? t === i : "!=" === r ? t !== i : "^=" === r ? i && 0 === t.indexOf(i) : "*=" === r ? i && -1 < t.indexOf(i) : "$=" === r ? i && t.slice(-i.length) === i : "~=" === r ? -1 < (" " + t.replace(F, " ") + " ").indexOf(i) : "|=" === r && (t === i || t.slice(0, i.length + 1) === i + "-"));
          };
        },
        CHILD: function CHILD(h, e, t, g, v) {
          var y = "nth" !== h.slice(0, 3),
              m = "last" !== h.slice(-4),
              x = "of-type" === e;
          return 1 === g && 0 === v ? function (e) {
            return !!e.parentNode;
          } : function (e, t, n) {
            var r,
                i,
                o,
                a,
                s,
                u,
                l = y !== m ? "nextSibling" : "previousSibling",
                c = e.parentNode,
                f = x && e.nodeName.toLowerCase(),
                p = !n && !x,
                d = !1;

            if (c) {
              if (y) {
                while (l) {
                  a = e;

                  while (a = a[l]) {
                    if (x ? a.nodeName.toLowerCase() === f : 1 === a.nodeType) return !1;
                  }

                  u = l = "only" === h && !u && "nextSibling";
                }

                return !0;
              }

              if (u = [m ? c.firstChild : c.lastChild], m && p) {
                d = (s = (r = (i = (o = (a = c)[k] || (a[k] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] || [])[0] === S && r[1]) && r[2], a = s && c.childNodes[s];

                while (a = ++s && a && a[l] || (d = s = 0) || u.pop()) {
                  if (1 === a.nodeType && ++d && a === e) {
                    i[h] = [S, s, d];
                    break;
                  }
                }
              } else if (p && (d = s = (r = (i = (o = (a = e)[k] || (a[k] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] || [])[0] === S && r[1]), !1 === d) while (a = ++s && a && a[l] || (d = s = 0) || u.pop()) {
                if ((x ? a.nodeName.toLowerCase() === f : 1 === a.nodeType) && ++d && (p && ((i = (o = a[k] || (a[k] = {}))[a.uniqueID] || (o[a.uniqueID] = {}))[h] = [S, d]), a === e)) break;
              }

              return (d -= v) === g || d % g == 0 && 0 <= d / g;
            }
          };
        },
        PSEUDO: function PSEUDO(e, o) {
          var t,
              a = b.pseudos[e] || b.setFilters[e.toLowerCase()] || se.error("unsupported pseudo: " + e);
          return a[k] ? a(o) : 1 < a.length ? (t = [e, e, "", o], b.setFilters.hasOwnProperty(e.toLowerCase()) ? le(function (e, t) {
            var n,
                r = a(e, o),
                i = r.length;

            while (i--) {
              e[n = P(e, r[i])] = !(t[n] = r[i]);
            }
          }) : function (e) {
            return a(e, 0, t);
          }) : a;
        }
      },
      pseudos: {
        not: le(function (e) {
          var r = [],
              i = [],
              s = f(e.replace(B, "$1"));
          return s[k] ? le(function (e, t, n, r) {
            var i,
                o = s(e, null, r, []),
                a = e.length;

            while (a--) {
              (i = o[a]) && (e[a] = !(t[a] = i));
            }
          }) : function (e, t, n) {
            return r[0] = e, s(r, null, n, i), r[0] = null, !i.pop();
          };
        }),
        has: le(function (t) {
          return function (e) {
            return 0 < se(t, e).length;
          };
        }),
        contains: le(function (t) {
          return t = t.replace(te, ne), function (e) {
            return -1 < (e.textContent || o(e)).indexOf(t);
          };
        }),
        lang: le(function (n) {
          return V.test(n || "") || se.error("unsupported lang: " + n), n = n.replace(te, ne).toLowerCase(), function (e) {
            var t;

            do {
              if (t = E ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang")) return (t = t.toLowerCase()) === n || 0 === t.indexOf(n + "-");
            } while ((e = e.parentNode) && 1 === e.nodeType);

            return !1;
          };
        }),
        target: function target(e) {
          var t = n.location && n.location.hash;
          return t && t.slice(1) === e.id;
        },
        root: function root(e) {
          return e === a;
        },
        focus: function focus(e) {
          return e === C.activeElement && (!C.hasFocus || C.hasFocus()) && !!(e.type || e.href || ~e.tabIndex);
        },
        enabled: ge(!1),
        disabled: ge(!0),
        checked: function checked(e) {
          var t = e.nodeName.toLowerCase();
          return "input" === t && !!e.checked || "option" === t && !!e.selected;
        },
        selected: function selected(e) {
          return e.parentNode && e.parentNode.selectedIndex, !0 === e.selected;
        },
        empty: function empty(e) {
          for (e = e.firstChild; e; e = e.nextSibling) {
            if (e.nodeType < 6) return !1;
          }

          return !0;
        },
        parent: function parent(e) {
          return !b.pseudos.empty(e);
        },
        header: function header(e) {
          return J.test(e.nodeName);
        },
        input: function input(e) {
          return Q.test(e.nodeName);
        },
        button: function button(e) {
          var t = e.nodeName.toLowerCase();
          return "input" === t && "button" === e.type || "button" === t;
        },
        text: function text(e) {
          var t;
          return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase());
        },
        first: ve(function () {
          return [0];
        }),
        last: ve(function (e, t) {
          return [t - 1];
        }),
        eq: ve(function (e, t, n) {
          return [n < 0 ? n + t : n];
        }),
        even: ve(function (e, t) {
          for (var n = 0; n < t; n += 2) {
            e.push(n);
          }

          return e;
        }),
        odd: ve(function (e, t) {
          for (var n = 1; n < t; n += 2) {
            e.push(n);
          }

          return e;
        }),
        lt: ve(function (e, t, n) {
          for (var r = n < 0 ? n + t : t < n ? t : n; 0 <= --r;) {
            e.push(r);
          }

          return e;
        }),
        gt: ve(function (e, t, n) {
          for (var r = n < 0 ? n + t : n; ++r < t;) {
            e.push(r);
          }

          return e;
        })
      }
    }).pseudos.nth = b.pseudos.eq, {
      radio: !0,
      checkbox: !0,
      file: !0,
      password: !0,
      image: !0
    }) {
      b.pseudos[e] = de(e);
    }

    for (e in {
      submit: !0,
      reset: !0
    }) {
      b.pseudos[e] = he(e);
    }

    function me() {}

    function xe(e) {
      for (var t = 0, n = e.length, r = ""; t < n; t++) {
        r += e[t].value;
      }

      return r;
    }

    function be(s, e, t) {
      var u = e.dir,
          l = e.next,
          c = l || u,
          f = t && "parentNode" === c,
          p = r++;
      return e.first ? function (e, t, n) {
        while (e = e[u]) {
          if (1 === e.nodeType || f) return s(e, t, n);
        }

        return !1;
      } : function (e, t, n) {
        var r,
            i,
            o,
            a = [S, p];

        if (n) {
          while (e = e[u]) {
            if ((1 === e.nodeType || f) && s(e, t, n)) return !0;
          }
        } else while (e = e[u]) {
          if (1 === e.nodeType || f) if (i = (o = e[k] || (e[k] = {}))[e.uniqueID] || (o[e.uniqueID] = {}), l && l === e.nodeName.toLowerCase()) e = e[u] || e;else {
            if ((r = i[c]) && r[0] === S && r[1] === p) return a[2] = r[2];
            if ((i[c] = a)[2] = s(e, t, n)) return !0;
          }
        }

        return !1;
      };
    }

    function we(i) {
      return 1 < i.length ? function (e, t, n) {
        var r = i.length;

        while (r--) {
          if (!i[r](e, t, n)) return !1;
        }

        return !0;
      } : i[0];
    }

    function Te(e, t, n, r, i) {
      for (var o, a = [], s = 0, u = e.length, l = null != t; s < u; s++) {
        (o = e[s]) && (n && !n(o, r, i) || (a.push(o), l && t.push(s)));
      }

      return a;
    }

    function Ce(d, h, g, v, y, e) {
      return v && !v[k] && (v = Ce(v)), y && !y[k] && (y = Ce(y, e)), le(function (e, t, n, r) {
        var i,
            o,
            a,
            s = [],
            u = [],
            l = t.length,
            c = e || function (e, t, n) {
          for (var r = 0, i = t.length; r < i; r++) {
            se(e, t[r], n);
          }

          return n;
        }(h || "*", n.nodeType ? [n] : n, []),
            f = !d || !e && h ? c : Te(c, s, d, n, r),
            p = g ? y || (e ? d : l || v) ? [] : t : f;

        if (g && g(f, p, n, r), v) {
          i = Te(p, u), v(i, [], n, r), o = i.length;

          while (o--) {
            (a = i[o]) && (p[u[o]] = !(f[u[o]] = a));
          }
        }

        if (e) {
          if (y || d) {
            if (y) {
              i = [], o = p.length;

              while (o--) {
                (a = p[o]) && i.push(f[o] = a);
              }

              y(null, p = [], i, r);
            }

            o = p.length;

            while (o--) {
              (a = p[o]) && -1 < (i = y ? P(e, a) : s[o]) && (e[i] = !(t[i] = a));
            }
          }
        } else p = Te(p === t ? p.splice(l, p.length) : p), y ? y(null, t, p, r) : H.apply(t, p);
      });
    }

    function Ee(e) {
      for (var i, t, n, r = e.length, o = b.relative[e[0].type], a = o || b.relative[" "], s = o ? 1 : 0, u = be(function (e) {
        return e === i;
      }, a, !0), l = be(function (e) {
        return -1 < P(i, e);
      }, a, !0), c = [function (e, t, n) {
        var r = !o && (n || t !== w) || ((i = t).nodeType ? u(e, t, n) : l(e, t, n));
        return i = null, r;
      }]; s < r; s++) {
        if (t = b.relative[e[s].type]) c = [be(we(c), t)];else {
          if ((t = b.filter[e[s].type].apply(null, e[s].matches))[k]) {
            for (n = ++s; n < r; n++) {
              if (b.relative[e[n].type]) break;
            }

            return Ce(1 < s && we(c), 1 < s && xe(e.slice(0, s - 1).concat({
              value: " " === e[s - 2].type ? "*" : ""
            })).replace(B, "$1"), t, s < n && Ee(e.slice(s, n)), n < r && Ee(e = e.slice(n)), n < r && xe(e));
          }

          c.push(t);
        }
      }

      return we(c);
    }

    return me.prototype = b.filters = b.pseudos, b.setFilters = new me(), h = se.tokenize = function (e, t) {
      var n,
          r,
          i,
          o,
          a,
          s,
          u,
          l = x[e + " "];
      if (l) return t ? 0 : l.slice(0);
      a = e, s = [], u = b.preFilter;

      while (a) {
        for (o in n && !(r = _.exec(a)) || (r && (a = a.slice(r[0].length) || a), s.push(i = [])), n = !1, (r = z.exec(a)) && (n = r.shift(), i.push({
          value: n,
          type: r[0].replace(B, " ")
        }), a = a.slice(n.length)), b.filter) {
          !(r = G[o].exec(a)) || u[o] && !(r = u[o](r)) || (n = r.shift(), i.push({
            value: n,
            type: o,
            matches: r
          }), a = a.slice(n.length));
        }

        if (!n) break;
      }

      return t ? a.length : a ? se.error(e) : x(e, s).slice(0);
    }, f = se.compile = function (e, t) {
      var n,
          v,
          y,
          m,
          x,
          r,
          i = [],
          o = [],
          a = N[e + " "];

      if (!a) {
        t || (t = h(e)), n = t.length;

        while (n--) {
          (a = Ee(t[n]))[k] ? i.push(a) : o.push(a);
        }

        (a = N(e, (v = o, m = 0 < (y = i).length, x = 0 < v.length, r = function r(e, t, n, _r, i) {
          var o,
              a,
              s,
              u = 0,
              l = "0",
              c = e && [],
              f = [],
              p = w,
              d = e || x && b.find.TAG("*", i),
              h = S += null == p ? 1 : Math.random() || .1,
              g = d.length;

          for (i && (w = t === C || t || i); l !== g && null != (o = d[l]); l++) {
            if (x && o) {
              a = 0, t || o.ownerDocument === C || (T(o), n = !E);

              while (s = v[a++]) {
                if (s(o, t || C, n)) {
                  _r.push(o);

                  break;
                }
              }

              i && (S = h);
            }

            m && ((o = !s && o) && u--, e && c.push(o));
          }

          if (u += l, m && l !== u) {
            a = 0;

            while (s = y[a++]) {
              s(c, f, t, n);
            }

            if (e) {
              if (0 < u) while (l--) {
                c[l] || f[l] || (f[l] = q.call(_r));
              }
              f = Te(f);
            }

            H.apply(_r, f), i && !e && 0 < f.length && 1 < u + y.length && se.uniqueSort(_r);
          }

          return i && (S = h, w = p), c;
        }, m ? le(r) : r))).selector = e;
      }

      return a;
    }, g = se.select = function (e, t, n, r) {
      var i,
          o,
          a,
          s,
          u,
          l = "function" == typeof e && e,
          c = !r && h(e = l.selector || e);

      if (n = n || [], 1 === c.length) {
        if (2 < (o = c[0] = c[0].slice(0)).length && "ID" === (a = o[0]).type && 9 === t.nodeType && E && b.relative[o[1].type]) {
          if (!(t = (b.find.ID(a.matches[0].replace(te, ne), t) || [])[0])) return n;
          l && (t = t.parentNode), e = e.slice(o.shift().value.length);
        }

        i = G.needsContext.test(e) ? 0 : o.length;

        while (i--) {
          if (a = o[i], b.relative[s = a.type]) break;

          if ((u = b.find[s]) && (r = u(a.matches[0].replace(te, ne), ee.test(o[0].type) && ye(t.parentNode) || t))) {
            if (o.splice(i, 1), !(e = r.length && xe(o))) return H.apply(n, r), n;
            break;
          }
        }
      }

      return (l || f(e, c))(r, t, !E, n, !t || ee.test(e) && ye(t.parentNode) || t), n;
    }, d.sortStable = k.split("").sort(D).join("") === k, d.detectDuplicates = !!l, T(), d.sortDetached = ce(function (e) {
      return 1 & e.compareDocumentPosition(C.createElement("fieldset"));
    }), ce(function (e) {
      return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href");
    }) || fe("type|href|height|width", function (e, t, n) {
      if (!n) return e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2);
    }), d.attributes && ce(function (e) {
      return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value");
    }) || fe("value", function (e, t, n) {
      if (!n && "input" === e.nodeName.toLowerCase()) return e.defaultValue;
    }), ce(function (e) {
      return null == e.getAttribute("disabled");
    }) || fe(R, function (e, t, n) {
      var r;
      if (!n) return !0 === e[t] ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null;
    }), se;
  }(C);

  k.find = h, k.expr = h.selectors, k.expr[":"] = k.expr.pseudos, k.uniqueSort = k.unique = h.uniqueSort, k.text = h.getText, k.isXMLDoc = h.isXML, k.contains = h.contains, k.escapeSelector = h.escape;

  var T = function T(e, t, n) {
    var r = [],
        i = void 0 !== n;

    while ((e = e[t]) && 9 !== e.nodeType) {
      if (1 === e.nodeType) {
        if (i && k(e).is(n)) break;
        r.push(e);
      }
    }

    return r;
  },
      S = function S(e, t) {
    for (var n = []; e; e = e.nextSibling) {
      1 === e.nodeType && e !== t && n.push(e);
    }

    return n;
  },
      N = k.expr.match.needsContext;

  function A(e, t) {
    return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase();
  }

  var D = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i;

  function j(e, n, r) {
    return m(n) ? k.grep(e, function (e, t) {
      return !!n.call(e, t, e) !== r;
    }) : n.nodeType ? k.grep(e, function (e) {
      return e === n !== r;
    }) : "string" != typeof n ? k.grep(e, function (e) {
      return -1 < i.call(n, e) !== r;
    }) : k.filter(n, e, r);
  }

  k.filter = function (e, t, n) {
    var r = t[0];
    return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === r.nodeType ? k.find.matchesSelector(r, e) ? [r] : [] : k.find.matches(e, k.grep(t, function (e) {
      return 1 === e.nodeType;
    }));
  }, k.fn.extend({
    find: function find(e) {
      var t,
          n,
          r = this.length,
          i = this;
      if ("string" != typeof e) return this.pushStack(k(e).filter(function () {
        for (t = 0; t < r; t++) {
          if (k.contains(i[t], this)) return !0;
        }
      }));

      for (n = this.pushStack([]), t = 0; t < r; t++) {
        k.find(e, i[t], n);
      }

      return 1 < r ? k.uniqueSort(n) : n;
    },
    filter: function filter(e) {
      return this.pushStack(j(this, e || [], !1));
    },
    not: function not(e) {
      return this.pushStack(j(this, e || [], !0));
    },
    is: function is(e) {
      return !!j(this, "string" == typeof e && N.test(e) ? k(e) : e || [], !1).length;
    }
  });
  var q,
      L = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
  (k.fn.init = function (e, t, n) {
    var r, i;
    if (!e) return this;

    if (n = n || q, "string" == typeof e) {
      if (!(r = "<" === e[0] && ">" === e[e.length - 1] && 3 <= e.length ? [null, e, null] : L.exec(e)) || !r[1] && t) return !t || t.jquery ? (t || n).find(e) : this.constructor(t).find(e);

      if (r[1]) {
        if (t = t instanceof k ? t[0] : t, k.merge(this, k.parseHTML(r[1], t && t.nodeType ? t.ownerDocument || t : E, !0)), D.test(r[1]) && k.isPlainObject(t)) for (r in t) {
          m(this[r]) ? this[r](t[r]) : this.attr(r, t[r]);
        }
        return this;
      }

      return (i = E.getElementById(r[2])) && (this[0] = i, this.length = 1), this;
    }

    return e.nodeType ? (this[0] = e, this.length = 1, this) : m(e) ? void 0 !== n.ready ? n.ready(e) : e(k) : k.makeArray(e, this);
  }).prototype = k.fn, q = k(E);
  var H = /^(?:parents|prev(?:Until|All))/,
      O = {
    children: !0,
    contents: !0,
    next: !0,
    prev: !0
  };

  function P(e, t) {
    while ((e = e[t]) && 1 !== e.nodeType) {
      ;
    }

    return e;
  }

  k.fn.extend({
    has: function has(e) {
      var t = k(e, this),
          n = t.length;
      return this.filter(function () {
        for (var e = 0; e < n; e++) {
          if (k.contains(this, t[e])) return !0;
        }
      });
    },
    closest: function closest(e, t) {
      var n,
          r = 0,
          i = this.length,
          o = [],
          a = "string" != typeof e && k(e);
      if (!N.test(e)) for (; r < i; r++) {
        for (n = this[r]; n && n !== t; n = n.parentNode) {
          if (n.nodeType < 11 && (a ? -1 < a.index(n) : 1 === n.nodeType && k.find.matchesSelector(n, e))) {
            o.push(n);
            break;
          }
        }
      }
      return this.pushStack(1 < o.length ? k.uniqueSort(o) : o);
    },
    index: function index(e) {
      return e ? "string" == typeof e ? i.call(k(e), this[0]) : i.call(this, e.jquery ? e[0] : e) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
    },
    add: function add(e, t) {
      return this.pushStack(k.uniqueSort(k.merge(this.get(), k(e, t))));
    },
    addBack: function addBack(e) {
      return this.add(null == e ? this.prevObject : this.prevObject.filter(e));
    }
  }), k.each({
    parent: function parent(e) {
      var t = e.parentNode;
      return t && 11 !== t.nodeType ? t : null;
    },
    parents: function parents(e) {
      return T(e, "parentNode");
    },
    parentsUntil: function parentsUntil(e, t, n) {
      return T(e, "parentNode", n);
    },
    next: function next(e) {
      return P(e, "nextSibling");
    },
    prev: function prev(e) {
      return P(e, "previousSibling");
    },
    nextAll: function nextAll(e) {
      return T(e, "nextSibling");
    },
    prevAll: function prevAll(e) {
      return T(e, "previousSibling");
    },
    nextUntil: function nextUntil(e, t, n) {
      return T(e, "nextSibling", n);
    },
    prevUntil: function prevUntil(e, t, n) {
      return T(e, "previousSibling", n);
    },
    siblings: function siblings(e) {
      return S((e.parentNode || {}).firstChild, e);
    },
    children: function children(e) {
      return S(e.firstChild);
    },
    contents: function contents(e) {
      return "undefined" != typeof e.contentDocument ? e.contentDocument : (A(e, "template") && (e = e.content || e), k.merge([], e.childNodes));
    }
  }, function (r, i) {
    k.fn[r] = function (e, t) {
      var n = k.map(this, i, e);
      return "Until" !== r.slice(-5) && (t = e), t && "string" == typeof t && (n = k.filter(t, n)), 1 < this.length && (O[r] || k.uniqueSort(n), H.test(r) && n.reverse()), this.pushStack(n);
    };
  });
  var R = /[^\x20\t\r\n\f]+/g;

  function M(e) {
    return e;
  }

  function I(e) {
    throw e;
  }

  function W(e, t, n, r) {
    var i;

    try {
      e && m(i = e.promise) ? i.call(e).done(t).fail(n) : e && m(i = e.then) ? i.call(e, t, n) : t.apply(void 0, [e].slice(r));
    } catch (e) {
      n.apply(void 0, [e]);
    }
  }

  k.Callbacks = function (r) {
    var e, n;
    r = "string" == typeof r ? (e = r, n = {}, k.each(e.match(R) || [], function (e, t) {
      n[t] = !0;
    }), n) : k.extend({}, r);

    var i,
        t,
        o,
        a,
        s = [],
        u = [],
        l = -1,
        c = function c() {
      for (a = a || r.once, o = i = !0; u.length; l = -1) {
        t = u.shift();

        while (++l < s.length) {
          !1 === s[l].apply(t[0], t[1]) && r.stopOnFalse && (l = s.length, t = !1);
        }
      }

      r.memory || (t = !1), i = !1, a && (s = t ? [] : "");
    },
        f = {
      add: function add() {
        return s && (t && !i && (l = s.length - 1, u.push(t)), function n(e) {
          k.each(e, function (e, t) {
            m(t) ? r.unique && f.has(t) || s.push(t) : t && t.length && "string" !== w(t) && n(t);
          });
        }(arguments), t && !i && c()), this;
      },
      remove: function remove() {
        return k.each(arguments, function (e, t) {
          var n;

          while (-1 < (n = k.inArray(t, s, n))) {
            s.splice(n, 1), n <= l && l--;
          }
        }), this;
      },
      has: function has(e) {
        return e ? -1 < k.inArray(e, s) : 0 < s.length;
      },
      empty: function empty() {
        return s && (s = []), this;
      },
      disable: function disable() {
        return a = u = [], s = t = "", this;
      },
      disabled: function disabled() {
        return !s;
      },
      lock: function lock() {
        return a = u = [], t || i || (s = t = ""), this;
      },
      locked: function locked() {
        return !!a;
      },
      fireWith: function fireWith(e, t) {
        return a || (t = [e, (t = t || []).slice ? t.slice() : t], u.push(t), i || c()), this;
      },
      fire: function fire() {
        return f.fireWith(this, arguments), this;
      },
      fired: function fired() {
        return !!o;
      }
    };

    return f;
  }, k.extend({
    Deferred: function Deferred(e) {
      var o = [["notify", "progress", k.Callbacks("memory"), k.Callbacks("memory"), 2], ["resolve", "done", k.Callbacks("once memory"), k.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", k.Callbacks("once memory"), k.Callbacks("once memory"), 1, "rejected"]],
          i = "pending",
          a = {
        state: function state() {
          return i;
        },
        always: function always() {
          return s.done(arguments).fail(arguments), this;
        },
        "catch": function _catch(e) {
          return a.then(null, e);
        },
        pipe: function pipe() {
          var i = arguments;
          return k.Deferred(function (r) {
            k.each(o, function (e, t) {
              var n = m(i[t[4]]) && i[t[4]];
              s[t[1]](function () {
                var e = n && n.apply(this, arguments);
                e && m(e.promise) ? e.promise().progress(r.notify).done(r.resolve).fail(r.reject) : r[t[0] + "With"](this, n ? [e] : arguments);
              });
            }), i = null;
          }).promise();
        },
        then: function then(t, n, r) {
          var u = 0;

          function l(i, o, a, s) {
            return function () {
              var n = this,
                  r = arguments,
                  e = function e() {
                var e, t;

                if (!(i < u)) {
                  if ((e = a.apply(n, r)) === o.promise()) throw new TypeError("Thenable self-resolution");
                  t = e && ("object" == _typeof(e) || "function" == typeof e) && e.then, m(t) ? s ? t.call(e, l(u, o, M, s), l(u, o, I, s)) : (u++, t.call(e, l(u, o, M, s), l(u, o, I, s), l(u, o, M, o.notifyWith))) : (a !== M && (n = void 0, r = [e]), (s || o.resolveWith)(n, r));
                }
              },
                  t = s ? e : function () {
                try {
                  e();
                } catch (e) {
                  k.Deferred.exceptionHook && k.Deferred.exceptionHook(e, t.stackTrace), u <= i + 1 && (a !== I && (n = void 0, r = [e]), o.rejectWith(n, r));
                }
              };

              i ? t() : (k.Deferred.getStackHook && (t.stackTrace = k.Deferred.getStackHook()), C.setTimeout(t));
            };
          }

          return k.Deferred(function (e) {
            o[0][3].add(l(0, e, m(r) ? r : M, e.notifyWith)), o[1][3].add(l(0, e, m(t) ? t : M)), o[2][3].add(l(0, e, m(n) ? n : I));
          }).promise();
        },
        promise: function promise(e) {
          return null != e ? k.extend(e, a) : a;
        }
      },
          s = {};
      return k.each(o, function (e, t) {
        var n = t[2],
            r = t[5];
        a[t[1]] = n.add, r && n.add(function () {
          i = r;
        }, o[3 - e][2].disable, o[3 - e][3].disable, o[0][2].lock, o[0][3].lock), n.add(t[3].fire), s[t[0]] = function () {
          return s[t[0] + "With"](this === s ? void 0 : this, arguments), this;
        }, s[t[0] + "With"] = n.fireWith;
      }), a.promise(s), e && e.call(s, s), s;
    },
    when: function when(e) {
      var n = arguments.length,
          t = n,
          r = Array(t),
          i = s.call(arguments),
          o = k.Deferred(),
          a = function a(t) {
        return function (e) {
          r[t] = this, i[t] = 1 < arguments.length ? s.call(arguments) : e, --n || o.resolveWith(r, i);
        };
      };

      if (n <= 1 && (W(e, o.done(a(t)).resolve, o.reject, !n), "pending" === o.state() || m(i[t] && i[t].then))) return o.then();

      while (t--) {
        W(i[t], a(t), o.reject);
      }

      return o.promise();
    }
  });
  var $ = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
  k.Deferred.exceptionHook = function (e, t) {
    C.console && C.console.warn && e && $.test(e.name) && C.console.warn("jQuery.Deferred exception: " + e.message, e.stack, t);
  }, k.readyException = function (e) {
    C.setTimeout(function () {
      throw e;
    });
  };
  var F = k.Deferred();

  function B() {
    E.removeEventListener("DOMContentLoaded", B), C.removeEventListener("load", B), k.ready();
  }

  k.fn.ready = function (e) {
    return F.then(e)["catch"](function (e) {
      k.readyException(e);
    }), this;
  }, k.extend({
    isReady: !1,
    readyWait: 1,
    ready: function ready(e) {
      (!0 === e ? --k.readyWait : k.isReady) || (k.isReady = !0) !== e && 0 < --k.readyWait || F.resolveWith(E, [k]);
    }
  }), k.ready.then = F.then, "complete" === E.readyState || "loading" !== E.readyState && !E.documentElement.doScroll ? C.setTimeout(k.ready) : (E.addEventListener("DOMContentLoaded", B), C.addEventListener("load", B));

  var _ = function _(e, t, n, r, i, o, a) {
    var s = 0,
        u = e.length,
        l = null == n;
    if ("object" === w(n)) for (s in i = !0, n) {
      _(e, t, s, n[s], !0, o, a);
    } else if (void 0 !== r && (i = !0, m(r) || (a = !0), l && (a ? (t.call(e, r), t = null) : (l = t, t = function t(e, _t2, n) {
      return l.call(k(e), n);
    })), t)) for (; s < u; s++) {
      t(e[s], n, a ? r : r.call(e[s], s, t(e[s], n)));
    }
    return i ? e : l ? t.call(e) : u ? t(e[0], n) : o;
  },
      z = /^-ms-/,
      U = /-([a-z])/g;

  function X(e, t) {
    return t.toUpperCase();
  }

  function V(e) {
    return e.replace(z, "ms-").replace(U, X);
  }

  var G = function G(e) {
    return 1 === e.nodeType || 9 === e.nodeType || !+e.nodeType;
  };

  function Y() {
    this.expando = k.expando + Y.uid++;
  }

  Y.uid = 1, Y.prototype = {
    cache: function cache(e) {
      var t = e[this.expando];
      return t || (t = {}, G(e) && (e.nodeType ? e[this.expando] = t : Object.defineProperty(e, this.expando, {
        value: t,
        configurable: !0
      }))), t;
    },
    set: function set(e, t, n) {
      var r,
          i = this.cache(e);
      if ("string" == typeof t) i[V(t)] = n;else for (r in t) {
        i[V(r)] = t[r];
      }
      return i;
    },
    get: function get(e, t) {
      return void 0 === t ? this.cache(e) : e[this.expando] && e[this.expando][V(t)];
    },
    access: function access(e, t, n) {
      return void 0 === t || t && "string" == typeof t && void 0 === n ? this.get(e, t) : (this.set(e, t, n), void 0 !== n ? n : t);
    },
    remove: function remove(e, t) {
      var n,
          r = e[this.expando];

      if (void 0 !== r) {
        if (void 0 !== t) {
          n = (t = Array.isArray(t) ? t.map(V) : (t = V(t)) in r ? [t] : t.match(R) || []).length;

          while (n--) {
            delete r[t[n]];
          }
        }

        (void 0 === t || k.isEmptyObject(r)) && (e.nodeType ? e[this.expando] = void 0 : delete e[this.expando]);
      }
    },
    hasData: function hasData(e) {
      var t = e[this.expando];
      return void 0 !== t && !k.isEmptyObject(t);
    }
  };
  var Q = new Y(),
      J = new Y(),
      K = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
      Z = /[A-Z]/g;

  function ee(e, t, n) {
    var r, i;
    if (void 0 === n && 1 === e.nodeType) if (r = "data-" + t.replace(Z, "-$&").toLowerCase(), "string" == typeof (n = e.getAttribute(r))) {
      try {
        n = "true" === (i = n) || "false" !== i && ("null" === i ? null : i === +i + "" ? +i : K.test(i) ? JSON.parse(i) : i);
      } catch (e) {}

      J.set(e, t, n);
    } else n = void 0;
    return n;
  }

  k.extend({
    hasData: function hasData(e) {
      return J.hasData(e) || Q.hasData(e);
    },
    data: function data(e, t, n) {
      return J.access(e, t, n);
    },
    removeData: function removeData(e, t) {
      J.remove(e, t);
    },
    _data: function _data(e, t, n) {
      return Q.access(e, t, n);
    },
    _removeData: function _removeData(e, t) {
      Q.remove(e, t);
    }
  }), k.fn.extend({
    data: function data(n, e) {
      var t,
          r,
          i,
          o = this[0],
          a = o && o.attributes;

      if (void 0 === n) {
        if (this.length && (i = J.get(o), 1 === o.nodeType && !Q.get(o, "hasDataAttrs"))) {
          t = a.length;

          while (t--) {
            a[t] && 0 === (r = a[t].name).indexOf("data-") && (r = V(r.slice(5)), ee(o, r, i[r]));
          }

          Q.set(o, "hasDataAttrs", !0);
        }

        return i;
      }

      return "object" == _typeof(n) ? this.each(function () {
        J.set(this, n);
      }) : _(this, function (e) {
        var t;
        if (o && void 0 === e) return void 0 !== (t = J.get(o, n)) ? t : void 0 !== (t = ee(o, n)) ? t : void 0;
        this.each(function () {
          J.set(this, n, e);
        });
      }, null, e, 1 < arguments.length, null, !0);
    },
    removeData: function removeData(e) {
      return this.each(function () {
        J.remove(this, e);
      });
    }
  }), k.extend({
    queue: function queue(e, t, n) {
      var r;
      if (e) return t = (t || "fx") + "queue", r = Q.get(e, t), n && (!r || Array.isArray(n) ? r = Q.access(e, t, k.makeArray(n)) : r.push(n)), r || [];
    },
    dequeue: function dequeue(e, t) {
      t = t || "fx";

      var n = k.queue(e, t),
          r = n.length,
          i = n.shift(),
          o = k._queueHooks(e, t);

      "inprogress" === i && (i = n.shift(), r--), i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, function () {
        k.dequeue(e, t);
      }, o)), !r && o && o.empty.fire();
    },
    _queueHooks: function _queueHooks(e, t) {
      var n = t + "queueHooks";
      return Q.get(e, n) || Q.access(e, n, {
        empty: k.Callbacks("once memory").add(function () {
          Q.remove(e, [t + "queue", n]);
        })
      });
    }
  }), k.fn.extend({
    queue: function queue(t, n) {
      var e = 2;
      return "string" != typeof t && (n = t, t = "fx", e--), arguments.length < e ? k.queue(this[0], t) : void 0 === n ? this : this.each(function () {
        var e = k.queue(this, t, n);
        k._queueHooks(this, t), "fx" === t && "inprogress" !== e[0] && k.dequeue(this, t);
      });
    },
    dequeue: function dequeue(e) {
      return this.each(function () {
        k.dequeue(this, e);
      });
    },
    clearQueue: function clearQueue(e) {
      return this.queue(e || "fx", []);
    },
    promise: function promise(e, t) {
      var n,
          r = 1,
          i = k.Deferred(),
          o = this,
          a = this.length,
          s = function s() {
        --r || i.resolveWith(o, [o]);
      };

      "string" != typeof e && (t = e, e = void 0), e = e || "fx";

      while (a--) {
        (n = Q.get(o[a], e + "queueHooks")) && n.empty && (r++, n.empty.add(s));
      }

      return s(), i.promise(t);
    }
  });

  var te = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
      ne = new RegExp("^(?:([+-])=|)(" + te + ")([a-z%]*)$", "i"),
      re = ["Top", "Right", "Bottom", "Left"],
      ie = E.documentElement,
      oe = function oe(e) {
    return k.contains(e.ownerDocument, e);
  },
      ae = {
    composed: !0
  };

  ie.getRootNode && (oe = function oe(e) {
    return k.contains(e.ownerDocument, e) || e.getRootNode(ae) === e.ownerDocument;
  });

  var se = function se(e, t) {
    return "none" === (e = t || e).style.display || "" === e.style.display && oe(e) && "none" === k.css(e, "display");
  },
      ue = function ue(e, t, n, r) {
    var i,
        o,
        a = {};

    for (o in t) {
      a[o] = e.style[o], e.style[o] = t[o];
    }

    for (o in i = n.apply(e, r || []), t) {
      e.style[o] = a[o];
    }

    return i;
  };

  function le(e, t, n, r) {
    var i,
        o,
        a = 20,
        s = r ? function () {
      return r.cur();
    } : function () {
      return k.css(e, t, "");
    },
        u = s(),
        l = n && n[3] || (k.cssNumber[t] ? "" : "px"),
        c = e.nodeType && (k.cssNumber[t] || "px" !== l && +u) && ne.exec(k.css(e, t));

    if (c && c[3] !== l) {
      u /= 2, l = l || c[3], c = +u || 1;

      while (a--) {
        k.style(e, t, c + l), (1 - o) * (1 - (o = s() / u || .5)) <= 0 && (a = 0), c /= o;
      }

      c *= 2, k.style(e, t, c + l), n = n || [];
    }

    return n && (c = +c || +u || 0, i = n[1] ? c + (n[1] + 1) * n[2] : +n[2], r && (r.unit = l, r.start = c, r.end = i)), i;
  }

  var ce = {};

  function fe(e, t) {
    for (var n, r, i, o, a, s, u, l = [], c = 0, f = e.length; c < f; c++) {
      (r = e[c]).style && (n = r.style.display, t ? ("none" === n && (l[c] = Q.get(r, "display") || null, l[c] || (r.style.display = "")), "" === r.style.display && se(r) && (l[c] = (u = a = o = void 0, a = (i = r).ownerDocument, s = i.nodeName, (u = ce[s]) || (o = a.body.appendChild(a.createElement(s)), u = k.css(o, "display"), o.parentNode.removeChild(o), "none" === u && (u = "block"), ce[s] = u)))) : "none" !== n && (l[c] = "none", Q.set(r, "display", n)));
    }

    for (c = 0; c < f; c++) {
      null != l[c] && (e[c].style.display = l[c]);
    }

    return e;
  }

  k.fn.extend({
    show: function show() {
      return fe(this, !0);
    },
    hide: function hide() {
      return fe(this);
    },
    toggle: function toggle(e) {
      return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function () {
        se(this) ? k(this).show() : k(this).hide();
      });
    }
  });
  var pe = /^(?:checkbox|radio)$/i,
      de = /<([a-z][^\/\0>\x20\t\r\n\f]*)/i,
      he = /^$|^module$|\/(?:java|ecma)script/i,
      ge = {
    option: [1, "<select multiple='multiple'>", "</select>"],
    thead: [1, "<table>", "</table>"],
    col: [2, "<table><colgroup>", "</colgroup></table>"],
    tr: [2, "<table><tbody>", "</tbody></table>"],
    td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
    _default: [0, "", ""]
  };

  function ve(e, t) {
    var n;
    return n = "undefined" != typeof e.getElementsByTagName ? e.getElementsByTagName(t || "*") : "undefined" != typeof e.querySelectorAll ? e.querySelectorAll(t || "*") : [], void 0 === t || t && A(e, t) ? k.merge([e], n) : n;
  }

  function ye(e, t) {
    for (var n = 0, r = e.length; n < r; n++) {
      Q.set(e[n], "globalEval", !t || Q.get(t[n], "globalEval"));
    }
  }

  ge.optgroup = ge.option, ge.tbody = ge.tfoot = ge.colgroup = ge.caption = ge.thead, ge.th = ge.td;
  var me,
      xe,
      be = /<|&#?\w+;/;

  function we(e, t, n, r, i) {
    for (var o, a, s, u, l, c, f = t.createDocumentFragment(), p = [], d = 0, h = e.length; d < h; d++) {
      if ((o = e[d]) || 0 === o) if ("object" === w(o)) k.merge(p, o.nodeType ? [o] : o);else if (be.test(o)) {
        a = a || f.appendChild(t.createElement("div")), s = (de.exec(o) || ["", ""])[1].toLowerCase(), u = ge[s] || ge._default, a.innerHTML = u[1] + k.htmlPrefilter(o) + u[2], c = u[0];

        while (c--) {
          a = a.lastChild;
        }

        k.merge(p, a.childNodes), (a = f.firstChild).textContent = "";
      } else p.push(t.createTextNode(o));
    }

    f.textContent = "", d = 0;

    while (o = p[d++]) {
      if (r && -1 < k.inArray(o, r)) i && i.push(o);else if (l = oe(o), a = ve(f.appendChild(o), "script"), l && ye(a), n) {
        c = 0;

        while (o = a[c++]) {
          he.test(o.type || "") && n.push(o);
        }
      }
    }

    return f;
  }

  me = E.createDocumentFragment().appendChild(E.createElement("div")), (xe = E.createElement("input")).setAttribute("type", "radio"), xe.setAttribute("checked", "checked"), xe.setAttribute("name", "t"), me.appendChild(xe), y.checkClone = me.cloneNode(!0).cloneNode(!0).lastChild.checked, me.innerHTML = "<textarea>x</textarea>", y.noCloneChecked = !!me.cloneNode(!0).lastChild.defaultValue;
  var Te = /^key/,
      Ce = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
      Ee = /^([^.]*)(?:\.(.+)|)/;

  function ke() {
    return !0;
  }

  function Se() {
    return !1;
  }

  function Ne(e, t) {
    return e === function () {
      try {
        return E.activeElement;
      } catch (e) {}
    }() == ("focus" === t);
  }

  function Ae(e, t, n, r, i, o) {
    var a, s;

    if ("object" == _typeof(t)) {
      for (s in "string" != typeof n && (r = r || n, n = void 0), t) {
        Ae(e, s, n, r, t[s], o);
      }

      return e;
    }

    if (null == r && null == i ? (i = n, r = n = void 0) : null == i && ("string" == typeof n ? (i = r, r = void 0) : (i = r, r = n, n = void 0)), !1 === i) i = Se;else if (!i) return e;
    return 1 === o && (a = i, (i = function i(e) {
      return k().off(e), a.apply(this, arguments);
    }).guid = a.guid || (a.guid = k.guid++)), e.each(function () {
      k.event.add(this, t, i, r, n);
    });
  }

  function De(e, i, o) {
    o ? (Q.set(e, i, !1), k.event.add(e, i, {
      namespace: !1,
      handler: function handler(e) {
        var t,
            n,
            r = Q.get(this, i);

        if (1 & e.isTrigger && this[i]) {
          if (r.length) (k.event.special[i] || {}).delegateType && e.stopPropagation();else if (r = s.call(arguments), Q.set(this, i, r), t = o(this, i), this[i](), r !== (n = Q.get(this, i)) || t ? Q.set(this, i, !1) : n = {}, r !== n) return e.stopImmediatePropagation(), e.preventDefault(), n.value;
        } else r.length && (Q.set(this, i, {
          value: k.event.trigger(k.extend(r[0], k.Event.prototype), r.slice(1), this)
        }), e.stopImmediatePropagation());
      }
    })) : void 0 === Q.get(e, i) && k.event.add(e, i, ke);
  }

  k.event = {
    global: {},
    add: function add(t, e, n, r, i) {
      var o,
          a,
          s,
          u,
          l,
          c,
          f,
          p,
          d,
          h,
          g,
          v = Q.get(t);

      if (v) {
        n.handler && (n = (o = n).handler, i = o.selector), i && k.find.matchesSelector(ie, i), n.guid || (n.guid = k.guid++), (u = v.events) || (u = v.events = {}), (a = v.handle) || (a = v.handle = function (e) {
          return "undefined" != typeof k && k.event.triggered !== e.type ? k.event.dispatch.apply(t, arguments) : void 0;
        }), l = (e = (e || "").match(R) || [""]).length;

        while (l--) {
          d = g = (s = Ee.exec(e[l]) || [])[1], h = (s[2] || "").split(".").sort(), d && (f = k.event.special[d] || {}, d = (i ? f.delegateType : f.bindType) || d, f = k.event.special[d] || {}, c = k.extend({
            type: d,
            origType: g,
            data: r,
            handler: n,
            guid: n.guid,
            selector: i,
            needsContext: i && k.expr.match.needsContext.test(i),
            namespace: h.join(".")
          }, o), (p = u[d]) || ((p = u[d] = []).delegateCount = 0, f.setup && !1 !== f.setup.call(t, r, h, a) || t.addEventListener && t.addEventListener(d, a)), f.add && (f.add.call(t, c), c.handler.guid || (c.handler.guid = n.guid)), i ? p.splice(p.delegateCount++, 0, c) : p.push(c), k.event.global[d] = !0);
        }
      }
    },
    remove: function remove(e, t, n, r, i) {
      var o,
          a,
          s,
          u,
          l,
          c,
          f,
          p,
          d,
          h,
          g,
          v = Q.hasData(e) && Q.get(e);

      if (v && (u = v.events)) {
        l = (t = (t || "").match(R) || [""]).length;

        while (l--) {
          if (d = g = (s = Ee.exec(t[l]) || [])[1], h = (s[2] || "").split(".").sort(), d) {
            f = k.event.special[d] || {}, p = u[d = (r ? f.delegateType : f.bindType) || d] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), a = o = p.length;

            while (o--) {
              c = p[o], !i && g !== c.origType || n && n.guid !== c.guid || s && !s.test(c.namespace) || r && r !== c.selector && ("**" !== r || !c.selector) || (p.splice(o, 1), c.selector && p.delegateCount--, f.remove && f.remove.call(e, c));
            }

            a && !p.length && (f.teardown && !1 !== f.teardown.call(e, h, v.handle) || k.removeEvent(e, d, v.handle), delete u[d]);
          } else for (d in u) {
            k.event.remove(e, d + t[l], n, r, !0);
          }
        }

        k.isEmptyObject(u) && Q.remove(e, "handle events");
      }
    },
    dispatch: function dispatch(e) {
      var t,
          n,
          r,
          i,
          o,
          a,
          s = k.event.fix(e),
          u = new Array(arguments.length),
          l = (Q.get(this, "events") || {})[s.type] || [],
          c = k.event.special[s.type] || {};

      for (u[0] = s, t = 1; t < arguments.length; t++) {
        u[t] = arguments[t];
      }

      if (s.delegateTarget = this, !c.preDispatch || !1 !== c.preDispatch.call(this, s)) {
        a = k.event.handlers.call(this, s, l), t = 0;

        while ((i = a[t++]) && !s.isPropagationStopped()) {
          s.currentTarget = i.elem, n = 0;

          while ((o = i.handlers[n++]) && !s.isImmediatePropagationStopped()) {
            s.rnamespace && !1 !== o.namespace && !s.rnamespace.test(o.namespace) || (s.handleObj = o, s.data = o.data, void 0 !== (r = ((k.event.special[o.origType] || {}).handle || o.handler).apply(i.elem, u)) && !1 === (s.result = r) && (s.preventDefault(), s.stopPropagation()));
          }
        }

        return c.postDispatch && c.postDispatch.call(this, s), s.result;
      }
    },
    handlers: function handlers(e, t) {
      var n,
          r,
          i,
          o,
          a,
          s = [],
          u = t.delegateCount,
          l = e.target;
      if (u && l.nodeType && !("click" === e.type && 1 <= e.button)) for (; l !== this; l = l.parentNode || this) {
        if (1 === l.nodeType && ("click" !== e.type || !0 !== l.disabled)) {
          for (o = [], a = {}, n = 0; n < u; n++) {
            void 0 === a[i = (r = t[n]).selector + " "] && (a[i] = r.needsContext ? -1 < k(i, this).index(l) : k.find(i, this, null, [l]).length), a[i] && o.push(r);
          }

          o.length && s.push({
            elem: l,
            handlers: o
          });
        }
      }
      return l = this, u < t.length && s.push({
        elem: l,
        handlers: t.slice(u)
      }), s;
    },
    addProp: function addProp(t, e) {
      Object.defineProperty(k.Event.prototype, t, {
        enumerable: !0,
        configurable: !0,
        get: m(e) ? function () {
          if (this.originalEvent) return e(this.originalEvent);
        } : function () {
          if (this.originalEvent) return this.originalEvent[t];
        },
        set: function set(e) {
          Object.defineProperty(this, t, {
            enumerable: !0,
            configurable: !0,
            writable: !0,
            value: e
          });
        }
      });
    },
    fix: function fix(e) {
      return e[k.expando] ? e : new k.Event(e);
    },
    special: {
      load: {
        noBubble: !0
      },
      click: {
        setup: function setup(e) {
          var t = this || e;
          return pe.test(t.type) && t.click && A(t, "input") && De(t, "click", ke), !1;
        },
        trigger: function trigger(e) {
          var t = this || e;
          return pe.test(t.type) && t.click && A(t, "input") && De(t, "click"), !0;
        },
        _default: function _default(e) {
          var t = e.target;
          return pe.test(t.type) && t.click && A(t, "input") && Q.get(t, "click") || A(t, "a");
        }
      },
      beforeunload: {
        postDispatch: function postDispatch(e) {
          void 0 !== e.result && e.originalEvent && (e.originalEvent.returnValue = e.result);
        }
      }
    }
  }, k.removeEvent = function (e, t, n) {
    e.removeEventListener && e.removeEventListener(t, n);
  }, k.Event = function (e, t) {
    if (!(this instanceof k.Event)) return new k.Event(e, t);
    e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && !1 === e.returnValue ? ke : Se, this.target = e.target && 3 === e.target.nodeType ? e.target.parentNode : e.target, this.currentTarget = e.currentTarget, this.relatedTarget = e.relatedTarget) : this.type = e, t && k.extend(this, t), this.timeStamp = e && e.timeStamp || Date.now(), this[k.expando] = !0;
  }, k.Event.prototype = {
    constructor: k.Event,
    isDefaultPrevented: Se,
    isPropagationStopped: Se,
    isImmediatePropagationStopped: Se,
    isSimulated: !1,
    preventDefault: function preventDefault() {
      var e = this.originalEvent;
      this.isDefaultPrevented = ke, e && !this.isSimulated && e.preventDefault();
    },
    stopPropagation: function stopPropagation() {
      var e = this.originalEvent;
      this.isPropagationStopped = ke, e && !this.isSimulated && e.stopPropagation();
    },
    stopImmediatePropagation: function stopImmediatePropagation() {
      var e = this.originalEvent;
      this.isImmediatePropagationStopped = ke, e && !this.isSimulated && e.stopImmediatePropagation(), this.stopPropagation();
    }
  }, k.each({
    altKey: !0,
    bubbles: !0,
    cancelable: !0,
    changedTouches: !0,
    ctrlKey: !0,
    detail: !0,
    eventPhase: !0,
    metaKey: !0,
    pageX: !0,
    pageY: !0,
    shiftKey: !0,
    view: !0,
    "char": !0,
    code: !0,
    charCode: !0,
    key: !0,
    keyCode: !0,
    button: !0,
    buttons: !0,
    clientX: !0,
    clientY: !0,
    offsetX: !0,
    offsetY: !0,
    pointerId: !0,
    pointerType: !0,
    screenX: !0,
    screenY: !0,
    targetTouches: !0,
    toElement: !0,
    touches: !0,
    which: function which(e) {
      var t = e.button;
      return null == e.which && Te.test(e.type) ? null != e.charCode ? e.charCode : e.keyCode : !e.which && void 0 !== t && Ce.test(e.type) ? 1 & t ? 1 : 2 & t ? 3 : 4 & t ? 2 : 0 : e.which;
    }
  }, k.event.addProp), k.each({
    focus: "focusin",
    blur: "focusout"
  }, function (e, t) {
    k.event.special[e] = {
      setup: function setup() {
        return De(this, e, Ne), !1;
      },
      trigger: function trigger() {
        return De(this, e), !0;
      },
      delegateType: t
    };
  }), k.each({
    mouseenter: "mouseover",
    mouseleave: "mouseout",
    pointerenter: "pointerover",
    pointerleave: "pointerout"
  }, function (e, i) {
    k.event.special[e] = {
      delegateType: i,
      bindType: i,
      handle: function handle(e) {
        var t,
            n = e.relatedTarget,
            r = e.handleObj;
        return n && (n === this || k.contains(this, n)) || (e.type = r.origType, t = r.handler.apply(this, arguments), e.type = i), t;
      }
    };
  }), k.fn.extend({
    on: function on(e, t, n, r) {
      return Ae(this, e, t, n, r);
    },
    one: function one(e, t, n, r) {
      return Ae(this, e, t, n, r, 1);
    },
    off: function off(e, t, n) {
      var r, i;
      if (e && e.preventDefault && e.handleObj) return r = e.handleObj, k(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this;

      if ("object" == _typeof(e)) {
        for (i in e) {
          this.off(i, t, e[i]);
        }

        return this;
      }

      return !1 !== t && "function" != typeof t || (n = t, t = void 0), !1 === n && (n = Se), this.each(function () {
        k.event.remove(this, e, n, t);
      });
    }
  });
  var je = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,
      qe = /<script|<style|<link/i,
      Le = /checked\s*(?:[^=]|=\s*.checked.)/i,
      He = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

  function Oe(e, t) {
    return A(e, "table") && A(11 !== t.nodeType ? t : t.firstChild, "tr") && k(e).children("tbody")[0] || e;
  }

  function Pe(e) {
    return e.type = (null !== e.getAttribute("type")) + "/" + e.type, e;
  }

  function Re(e) {
    return "true/" === (e.type || "").slice(0, 5) ? e.type = e.type.slice(5) : e.removeAttribute("type"), e;
  }

  function Me(e, t) {
    var n, r, i, o, a, s, u, l;

    if (1 === t.nodeType) {
      if (Q.hasData(e) && (o = Q.access(e), a = Q.set(t, o), l = o.events)) for (i in delete a.handle, a.events = {}, l) {
        for (n = 0, r = l[i].length; n < r; n++) {
          k.event.add(t, i, l[i][n]);
        }
      }
      J.hasData(e) && (s = J.access(e), u = k.extend({}, s), J.set(t, u));
    }
  }

  function Ie(n, r, i, o) {
    r = g.apply([], r);
    var e,
        t,
        a,
        s,
        u,
        l,
        c = 0,
        f = n.length,
        p = f - 1,
        d = r[0],
        h = m(d);
    if (h || 1 < f && "string" == typeof d && !y.checkClone && Le.test(d)) return n.each(function (e) {
      var t = n.eq(e);
      h && (r[0] = d.call(this, e, t.html())), Ie(t, r, i, o);
    });

    if (f && (t = (e = we(r, n[0].ownerDocument, !1, n, o)).firstChild, 1 === e.childNodes.length && (e = t), t || o)) {
      for (s = (a = k.map(ve(e, "script"), Pe)).length; c < f; c++) {
        u = e, c !== p && (u = k.clone(u, !0, !0), s && k.merge(a, ve(u, "script"))), i.call(n[c], u, c);
      }

      if (s) for (l = a[a.length - 1].ownerDocument, k.map(a, Re), c = 0; c < s; c++) {
        u = a[c], he.test(u.type || "") && !Q.access(u, "globalEval") && k.contains(l, u) && (u.src && "module" !== (u.type || "").toLowerCase() ? k._evalUrl && !u.noModule && k._evalUrl(u.src, {
          nonce: u.nonce || u.getAttribute("nonce")
        }) : b(u.textContent.replace(He, ""), u, l));
      }
    }

    return n;
  }

  function We(e, t, n) {
    for (var r, i = t ? k.filter(t, e) : e, o = 0; null != (r = i[o]); o++) {
      n || 1 !== r.nodeType || k.cleanData(ve(r)), r.parentNode && (n && oe(r) && ye(ve(r, "script")), r.parentNode.removeChild(r));
    }

    return e;
  }

  k.extend({
    htmlPrefilter: function htmlPrefilter(e) {
      return e.replace(je, "<$1></$2>");
    },
    clone: function clone(e, t, n) {
      var r,
          i,
          o,
          a,
          s,
          u,
          l,
          c = e.cloneNode(!0),
          f = oe(e);
      if (!(y.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || k.isXMLDoc(e))) for (a = ve(c), r = 0, i = (o = ve(e)).length; r < i; r++) {
        s = o[r], u = a[r], void 0, "input" === (l = u.nodeName.toLowerCase()) && pe.test(s.type) ? u.checked = s.checked : "input" !== l && "textarea" !== l || (u.defaultValue = s.defaultValue);
      }
      if (t) if (n) for (o = o || ve(e), a = a || ve(c), r = 0, i = o.length; r < i; r++) {
        Me(o[r], a[r]);
      } else Me(e, c);
      return 0 < (a = ve(c, "script")).length && ye(a, !f && ve(e, "script")), c;
    },
    cleanData: function cleanData(e) {
      for (var t, n, r, i = k.event.special, o = 0; void 0 !== (n = e[o]); o++) {
        if (G(n)) {
          if (t = n[Q.expando]) {
            if (t.events) for (r in t.events) {
              i[r] ? k.event.remove(n, r) : k.removeEvent(n, r, t.handle);
            }
            n[Q.expando] = void 0;
          }

          n[J.expando] && (n[J.expando] = void 0);
        }
      }
    }
  }), k.fn.extend({
    detach: function detach(e) {
      return We(this, e, !0);
    },
    remove: function remove(e) {
      return We(this, e);
    },
    text: function text(e) {
      return _(this, function (e) {
        return void 0 === e ? k.text(this) : this.empty().each(function () {
          1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = e);
        });
      }, null, e, arguments.length);
    },
    append: function append() {
      return Ie(this, arguments, function (e) {
        1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || Oe(this, e).appendChild(e);
      });
    },
    prepend: function prepend() {
      return Ie(this, arguments, function (e) {
        if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
          var t = Oe(this, e);
          t.insertBefore(e, t.firstChild);
        }
      });
    },
    before: function before() {
      return Ie(this, arguments, function (e) {
        this.parentNode && this.parentNode.insertBefore(e, this);
      });
    },
    after: function after() {
      return Ie(this, arguments, function (e) {
        this.parentNode && this.parentNode.insertBefore(e, this.nextSibling);
      });
    },
    empty: function empty() {
      for (var e, t = 0; null != (e = this[t]); t++) {
        1 === e.nodeType && (k.cleanData(ve(e, !1)), e.textContent = "");
      }

      return this;
    },
    clone: function clone(e, t) {
      return e = null != e && e, t = null == t ? e : t, this.map(function () {
        return k.clone(this, e, t);
      });
    },
    html: function html(e) {
      return _(this, function (e) {
        var t = this[0] || {},
            n = 0,
            r = this.length;
        if (void 0 === e && 1 === t.nodeType) return t.innerHTML;

        if ("string" == typeof e && !qe.test(e) && !ge[(de.exec(e) || ["", ""])[1].toLowerCase()]) {
          e = k.htmlPrefilter(e);

          try {
            for (; n < r; n++) {
              1 === (t = this[n] || {}).nodeType && (k.cleanData(ve(t, !1)), t.innerHTML = e);
            }

            t = 0;
          } catch (e) {}
        }

        t && this.empty().append(e);
      }, null, e, arguments.length);
    },
    replaceWith: function replaceWith() {
      var n = [];
      return Ie(this, arguments, function (e) {
        var t = this.parentNode;
        k.inArray(this, n) < 0 && (k.cleanData(ve(this)), t && t.replaceChild(e, this));
      }, n);
    }
  }), k.each({
    appendTo: "append",
    prependTo: "prepend",
    insertBefore: "before",
    insertAfter: "after",
    replaceAll: "replaceWith"
  }, function (e, a) {
    k.fn[e] = function (e) {
      for (var t, n = [], r = k(e), i = r.length - 1, o = 0; o <= i; o++) {
        t = o === i ? this : this.clone(!0), k(r[o])[a](t), u.apply(n, t.get());
      }

      return this.pushStack(n);
    };
  });

  var $e = new RegExp("^(" + te + ")(?!px)[a-z%]+$", "i"),
      Fe = function Fe(e) {
    var t = e.ownerDocument.defaultView;
    return t && t.opener || (t = C), t.getComputedStyle(e);
  },
      Be = new RegExp(re.join("|"), "i");

  function _e(e, t, n) {
    var r,
        i,
        o,
        a,
        s = e.style;
    return (n = n || Fe(e)) && ("" !== (a = n.getPropertyValue(t) || n[t]) || oe(e) || (a = k.style(e, t)), !y.pixelBoxStyles() && $e.test(a) && Be.test(t) && (r = s.width, i = s.minWidth, o = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = r, s.minWidth = i, s.maxWidth = o)), void 0 !== a ? a + "" : a;
  }

  function ze(e, t) {
    return {
      get: function get() {
        if (!e()) return (this.get = t).apply(this, arguments);
        delete this.get;
      }
    };
  }

  !function () {
    function e() {
      if (u) {
        s.style.cssText = "position:absolute;left:-11111px;width:60px;margin-top:1px;padding:0;border:0", u.style.cssText = "position:relative;display:block;box-sizing:border-box;overflow:scroll;margin:auto;border:1px;padding:1px;width:60%;top:1%", ie.appendChild(s).appendChild(u);
        var e = C.getComputedStyle(u);
        n = "1%" !== e.top, a = 12 === t(e.marginLeft), u.style.right = "60%", o = 36 === t(e.right), r = 36 === t(e.width), u.style.position = "absolute", i = 12 === t(u.offsetWidth / 3), ie.removeChild(s), u = null;
      }
    }

    function t(e) {
      return Math.round(parseFloat(e));
    }

    var n,
        r,
        i,
        o,
        a,
        s = E.createElement("div"),
        u = E.createElement("div");
    u.style && (u.style.backgroundClip = "content-box", u.cloneNode(!0).style.backgroundClip = "", y.clearCloneStyle = "content-box" === u.style.backgroundClip, k.extend(y, {
      boxSizingReliable: function boxSizingReliable() {
        return e(), r;
      },
      pixelBoxStyles: function pixelBoxStyles() {
        return e(), o;
      },
      pixelPosition: function pixelPosition() {
        return e(), n;
      },
      reliableMarginLeft: function reliableMarginLeft() {
        return e(), a;
      },
      scrollboxSize: function scrollboxSize() {
        return e(), i;
      }
    }));
  }();
  var Ue = ["Webkit", "Moz", "ms"],
      Xe = E.createElement("div").style,
      Ve = {};

  function Ge(e) {
    var t = k.cssProps[e] || Ve[e];
    return t || (e in Xe ? e : Ve[e] = function (e) {
      var t = e[0].toUpperCase() + e.slice(1),
          n = Ue.length;

      while (n--) {
        if ((e = Ue[n] + t) in Xe) return e;
      }
    }(e) || e);
  }

  var Ye = /^(none|table(?!-c[ea]).+)/,
      Qe = /^--/,
      Je = {
    position: "absolute",
    visibility: "hidden",
    display: "block"
  },
      Ke = {
    letterSpacing: "0",
    fontWeight: "400"
  };

  function Ze(e, t, n) {
    var r = ne.exec(t);
    return r ? Math.max(0, r[2] - (n || 0)) + (r[3] || "px") : t;
  }

  function et(e, t, n, r, i, o) {
    var a = "width" === t ? 1 : 0,
        s = 0,
        u = 0;
    if (n === (r ? "border" : "content")) return 0;

    for (; a < 4; a += 2) {
      "margin" === n && (u += k.css(e, n + re[a], !0, i)), r ? ("content" === n && (u -= k.css(e, "padding" + re[a], !0, i)), "margin" !== n && (u -= k.css(e, "border" + re[a] + "Width", !0, i))) : (u += k.css(e, "padding" + re[a], !0, i), "padding" !== n ? u += k.css(e, "border" + re[a] + "Width", !0, i) : s += k.css(e, "border" + re[a] + "Width", !0, i));
    }

    return !r && 0 <= o && (u += Math.max(0, Math.ceil(e["offset" + t[0].toUpperCase() + t.slice(1)] - o - u - s - .5)) || 0), u;
  }

  function tt(e, t, n) {
    var r = Fe(e),
        i = (!y.boxSizingReliable() || n) && "border-box" === k.css(e, "boxSizing", !1, r),
        o = i,
        a = _e(e, t, r),
        s = "offset" + t[0].toUpperCase() + t.slice(1);

    if ($e.test(a)) {
      if (!n) return a;
      a = "auto";
    }

    return (!y.boxSizingReliable() && i || "auto" === a || !parseFloat(a) && "inline" === k.css(e, "display", !1, r)) && e.getClientRects().length && (i = "border-box" === k.css(e, "boxSizing", !1, r), (o = s in e) && (a = e[s])), (a = parseFloat(a) || 0) + et(e, t, n || (i ? "border" : "content"), o, r, a) + "px";
  }

  function nt(e, t, n, r, i) {
    return new nt.prototype.init(e, t, n, r, i);
  }

  k.extend({
    cssHooks: {
      opacity: {
        get: function get(e, t) {
          if (t) {
            var n = _e(e, "opacity");

            return "" === n ? "1" : n;
          }
        }
      }
    },
    cssNumber: {
      animationIterationCount: !0,
      columnCount: !0,
      fillOpacity: !0,
      flexGrow: !0,
      flexShrink: !0,
      fontWeight: !0,
      gridArea: !0,
      gridColumn: !0,
      gridColumnEnd: !0,
      gridColumnStart: !0,
      gridRow: !0,
      gridRowEnd: !0,
      gridRowStart: !0,
      lineHeight: !0,
      opacity: !0,
      order: !0,
      orphans: !0,
      widows: !0,
      zIndex: !0,
      zoom: !0
    },
    cssProps: {},
    style: function style(e, t, n, r) {
      if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
        var i,
            o,
            a,
            s = V(t),
            u = Qe.test(t),
            l = e.style;
        if (u || (t = Ge(s)), a = k.cssHooks[t] || k.cssHooks[s], void 0 === n) return a && "get" in a && void 0 !== (i = a.get(e, !1, r)) ? i : l[t];
        "string" === (o = _typeof(n)) && (i = ne.exec(n)) && i[1] && (n = le(e, t, i), o = "number"), null != n && n == n && ("number" !== o || u || (n += i && i[3] || (k.cssNumber[s] ? "" : "px")), y.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), a && "set" in a && void 0 === (n = a.set(e, n, r)) || (u ? l.setProperty(t, n) : l[t] = n));
      }
    },
    css: function css(e, t, n, r) {
      var i,
          o,
          a,
          s = V(t);
      return Qe.test(t) || (t = Ge(s)), (a = k.cssHooks[t] || k.cssHooks[s]) && "get" in a && (i = a.get(e, !0, n)), void 0 === i && (i = _e(e, t, r)), "normal" === i && t in Ke && (i = Ke[t]), "" === n || n ? (o = parseFloat(i), !0 === n || isFinite(o) ? o || 0 : i) : i;
    }
  }), k.each(["height", "width"], function (e, u) {
    k.cssHooks[u] = {
      get: function get(e, t, n) {
        if (t) return !Ye.test(k.css(e, "display")) || e.getClientRects().length && e.getBoundingClientRect().width ? tt(e, u, n) : ue(e, Je, function () {
          return tt(e, u, n);
        });
      },
      set: function set(e, t, n) {
        var r,
            i = Fe(e),
            o = !y.scrollboxSize() && "absolute" === i.position,
            a = (o || n) && "border-box" === k.css(e, "boxSizing", !1, i),
            s = n ? et(e, u, n, a, i) : 0;
        return a && o && (s -= Math.ceil(e["offset" + u[0].toUpperCase() + u.slice(1)] - parseFloat(i[u]) - et(e, u, "border", !1, i) - .5)), s && (r = ne.exec(t)) && "px" !== (r[3] || "px") && (e.style[u] = t, t = k.css(e, u)), Ze(0, t, s);
      }
    };
  }), k.cssHooks.marginLeft = ze(y.reliableMarginLeft, function (e, t) {
    if (t) return (parseFloat(_e(e, "marginLeft")) || e.getBoundingClientRect().left - ue(e, {
      marginLeft: 0
    }, function () {
      return e.getBoundingClientRect().left;
    })) + "px";
  }), k.each({
    margin: "",
    padding: "",
    border: "Width"
  }, function (i, o) {
    k.cssHooks[i + o] = {
      expand: function expand(e) {
        for (var t = 0, n = {}, r = "string" == typeof e ? e.split(" ") : [e]; t < 4; t++) {
          n[i + re[t] + o] = r[t] || r[t - 2] || r[0];
        }

        return n;
      }
    }, "margin" !== i && (k.cssHooks[i + o].set = Ze);
  }), k.fn.extend({
    css: function css(e, t) {
      return _(this, function (e, t, n) {
        var r,
            i,
            o = {},
            a = 0;

        if (Array.isArray(t)) {
          for (r = Fe(e), i = t.length; a < i; a++) {
            o[t[a]] = k.css(e, t[a], !1, r);
          }

          return o;
        }

        return void 0 !== n ? k.style(e, t, n) : k.css(e, t);
      }, e, t, 1 < arguments.length);
    }
  }), ((k.Tween = nt).prototype = {
    constructor: nt,
    init: function init(e, t, n, r, i, o) {
      this.elem = e, this.prop = n, this.easing = i || k.easing._default, this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (k.cssNumber[n] ? "" : "px");
    },
    cur: function cur() {
      var e = nt.propHooks[this.prop];
      return e && e.get ? e.get(this) : nt.propHooks._default.get(this);
    },
    run: function run(e) {
      var t,
          n = nt.propHooks[this.prop];
      return this.options.duration ? this.pos = t = k.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : this.pos = t = e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : nt.propHooks._default.set(this), this;
    }
  }).init.prototype = nt.prototype, (nt.propHooks = {
    _default: {
      get: function get(e) {
        var t;
        return 1 !== e.elem.nodeType || null != e.elem[e.prop] && null == e.elem.style[e.prop] ? e.elem[e.prop] : (t = k.css(e.elem, e.prop, "")) && "auto" !== t ? t : 0;
      },
      set: function set(e) {
        k.fx.step[e.prop] ? k.fx.step[e.prop](e) : 1 !== e.elem.nodeType || !k.cssHooks[e.prop] && null == e.elem.style[Ge(e.prop)] ? e.elem[e.prop] = e.now : k.style(e.elem, e.prop, e.now + e.unit);
      }
    }
  }).scrollTop = nt.propHooks.scrollLeft = {
    set: function set(e) {
      e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now);
    }
  }, k.easing = {
    linear: function linear(e) {
      return e;
    },
    swing: function swing(e) {
      return .5 - Math.cos(e * Math.PI) / 2;
    },
    _default: "swing"
  }, k.fx = nt.prototype.init, k.fx.step = {};
  var rt,
      it,
      ot,
      at,
      st = /^(?:toggle|show|hide)$/,
      ut = /queueHooks$/;

  function lt() {
    it && (!1 === E.hidden && C.requestAnimationFrame ? C.requestAnimationFrame(lt) : C.setTimeout(lt, k.fx.interval), k.fx.tick());
  }

  function ct() {
    return C.setTimeout(function () {
      rt = void 0;
    }), rt = Date.now();
  }

  function ft(e, t) {
    var n,
        r = 0,
        i = {
      height: e
    };

    for (t = t ? 1 : 0; r < 4; r += 2 - t) {
      i["margin" + (n = re[r])] = i["padding" + n] = e;
    }

    return t && (i.opacity = i.width = e), i;
  }

  function pt(e, t, n) {
    for (var r, i = (dt.tweeners[t] || []).concat(dt.tweeners["*"]), o = 0, a = i.length; o < a; o++) {
      if (r = i[o].call(n, t, e)) return r;
    }
  }

  function dt(o, e, t) {
    var n,
        a,
        r = 0,
        i = dt.prefilters.length,
        s = k.Deferred().always(function () {
      delete u.elem;
    }),
        u = function u() {
      if (a) return !1;

      for (var e = rt || ct(), t = Math.max(0, l.startTime + l.duration - e), n = 1 - (t / l.duration || 0), r = 0, i = l.tweens.length; r < i; r++) {
        l.tweens[r].run(n);
      }

      return s.notifyWith(o, [l, n, t]), n < 1 && i ? t : (i || s.notifyWith(o, [l, 1, 0]), s.resolveWith(o, [l]), !1);
    },
        l = s.promise({
      elem: o,
      props: k.extend({}, e),
      opts: k.extend(!0, {
        specialEasing: {},
        easing: k.easing._default
      }, t),
      originalProperties: e,
      originalOptions: t,
      startTime: rt || ct(),
      duration: t.duration,
      tweens: [],
      createTween: function createTween(e, t) {
        var n = k.Tween(o, l.opts, e, t, l.opts.specialEasing[e] || l.opts.easing);
        return l.tweens.push(n), n;
      },
      stop: function stop(e) {
        var t = 0,
            n = e ? l.tweens.length : 0;
        if (a) return this;

        for (a = !0; t < n; t++) {
          l.tweens[t].run(1);
        }

        return e ? (s.notifyWith(o, [l, 1, 0]), s.resolveWith(o, [l, e])) : s.rejectWith(o, [l, e]), this;
      }
    }),
        c = l.props;

    for (!function (e, t) {
      var n, r, i, o, a;

      for (n in e) {
        if (i = t[r = V(n)], o = e[n], Array.isArray(o) && (i = o[1], o = e[n] = o[0]), n !== r && (e[r] = o, delete e[n]), (a = k.cssHooks[r]) && ("expand" in a)) for (n in o = a.expand(o), delete e[r], o) {
          (n in e) || (e[n] = o[n], t[n] = i);
        } else t[r] = i;
      }
    }(c, l.opts.specialEasing); r < i; r++) {
      if (n = dt.prefilters[r].call(l, o, c, l.opts)) return m(n.stop) && (k._queueHooks(l.elem, l.opts.queue).stop = n.stop.bind(n)), n;
    }

    return k.map(c, pt, l), m(l.opts.start) && l.opts.start.call(o, l), l.progress(l.opts.progress).done(l.opts.done, l.opts.complete).fail(l.opts.fail).always(l.opts.always), k.fx.timer(k.extend(u, {
      elem: o,
      anim: l,
      queue: l.opts.queue
    })), l;
  }

  k.Animation = k.extend(dt, {
    tweeners: {
      "*": [function (e, t) {
        var n = this.createTween(e, t);
        return le(n.elem, e, ne.exec(t), n), n;
      }]
    },
    tweener: function tweener(e, t) {
      m(e) ? (t = e, e = ["*"]) : e = e.match(R);

      for (var n, r = 0, i = e.length; r < i; r++) {
        n = e[r], dt.tweeners[n] = dt.tweeners[n] || [], dt.tweeners[n].unshift(t);
      }
    },
    prefilters: [function (e, t, n) {
      var r,
          i,
          o,
          a,
          s,
          u,
          l,
          c,
          f = "width" in t || "height" in t,
          p = this,
          d = {},
          h = e.style,
          g = e.nodeType && se(e),
          v = Q.get(e, "fxshow");

      for (r in n.queue || (null == (a = k._queueHooks(e, "fx")).unqueued && (a.unqueued = 0, s = a.empty.fire, a.empty.fire = function () {
        a.unqueued || s();
      }), a.unqueued++, p.always(function () {
        p.always(function () {
          a.unqueued--, k.queue(e, "fx").length || a.empty.fire();
        });
      })), t) {
        if (i = t[r], st.test(i)) {
          if (delete t[r], o = o || "toggle" === i, i === (g ? "hide" : "show")) {
            if ("show" !== i || !v || void 0 === v[r]) continue;
            g = !0;
          }

          d[r] = v && v[r] || k.style(e, r);
        }
      }

      if ((u = !k.isEmptyObject(t)) || !k.isEmptyObject(d)) for (r in f && 1 === e.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY], null == (l = v && v.display) && (l = Q.get(e, "display")), "none" === (c = k.css(e, "display")) && (l ? c = l : (fe([e], !0), l = e.style.display || l, c = k.css(e, "display"), fe([e]))), ("inline" === c || "inline-block" === c && null != l) && "none" === k.css(e, "float") && (u || (p.done(function () {
        h.display = l;
      }), null == l && (c = h.display, l = "none" === c ? "" : c)), h.display = "inline-block")), n.overflow && (h.overflow = "hidden", p.always(function () {
        h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2];
      })), u = !1, d) {
        u || (v ? "hidden" in v && (g = v.hidden) : v = Q.access(e, "fxshow", {
          display: l
        }), o && (v.hidden = !g), g && fe([e], !0), p.done(function () {
          for (r in g || fe([e]), Q.remove(e, "fxshow"), d) {
            k.style(e, r, d[r]);
          }
        })), u = pt(g ? v[r] : 0, r, p), r in v || (v[r] = u.start, g && (u.end = u.start, u.start = 0));
      }
    }],
    prefilter: function prefilter(e, t) {
      t ? dt.prefilters.unshift(e) : dt.prefilters.push(e);
    }
  }), k.speed = function (e, t, n) {
    var r = e && "object" == _typeof(e) ? k.extend({}, e) : {
      complete: n || !n && t || m(e) && e,
      duration: e,
      easing: n && t || t && !m(t) && t
    };
    return k.fx.off ? r.duration = 0 : "number" != typeof r.duration && (r.duration in k.fx.speeds ? r.duration = k.fx.speeds[r.duration] : r.duration = k.fx.speeds._default), null != r.queue && !0 !== r.queue || (r.queue = "fx"), r.old = r.complete, r.complete = function () {
      m(r.old) && r.old.call(this), r.queue && k.dequeue(this, r.queue);
    }, r;
  }, k.fn.extend({
    fadeTo: function fadeTo(e, t, n, r) {
      return this.filter(se).css("opacity", 0).show().end().animate({
        opacity: t
      }, e, n, r);
    },
    animate: function animate(t, e, n, r) {
      var i = k.isEmptyObject(t),
          o = k.speed(e, n, r),
          a = function a() {
        var e = dt(this, k.extend({}, t), o);
        (i || Q.get(this, "finish")) && e.stop(!0);
      };

      return a.finish = a, i || !1 === o.queue ? this.each(a) : this.queue(o.queue, a);
    },
    stop: function stop(i, e, o) {
      var a = function a(e) {
        var t = e.stop;
        delete e.stop, t(o);
      };

      return "string" != typeof i && (o = e, e = i, i = void 0), e && !1 !== i && this.queue(i || "fx", []), this.each(function () {
        var e = !0,
            t = null != i && i + "queueHooks",
            n = k.timers,
            r = Q.get(this);
        if (t) r[t] && r[t].stop && a(r[t]);else for (t in r) {
          r[t] && r[t].stop && ut.test(t) && a(r[t]);
        }

        for (t = n.length; t--;) {
          n[t].elem !== this || null != i && n[t].queue !== i || (n[t].anim.stop(o), e = !1, n.splice(t, 1));
        }

        !e && o || k.dequeue(this, i);
      });
    },
    finish: function finish(a) {
      return !1 !== a && (a = a || "fx"), this.each(function () {
        var e,
            t = Q.get(this),
            n = t[a + "queue"],
            r = t[a + "queueHooks"],
            i = k.timers,
            o = n ? n.length : 0;

        for (t.finish = !0, k.queue(this, a, []), r && r.stop && r.stop.call(this, !0), e = i.length; e--;) {
          i[e].elem === this && i[e].queue === a && (i[e].anim.stop(!0), i.splice(e, 1));
        }

        for (e = 0; e < o; e++) {
          n[e] && n[e].finish && n[e].finish.call(this);
        }

        delete t.finish;
      });
    }
  }), k.each(["toggle", "show", "hide"], function (e, r) {
    var i = k.fn[r];

    k.fn[r] = function (e, t, n) {
      return null == e || "boolean" == typeof e ? i.apply(this, arguments) : this.animate(ft(r, !0), e, t, n);
    };
  }), k.each({
    slideDown: ft("show"),
    slideUp: ft("hide"),
    slideToggle: ft("toggle"),
    fadeIn: {
      opacity: "show"
    },
    fadeOut: {
      opacity: "hide"
    },
    fadeToggle: {
      opacity: "toggle"
    }
  }, function (e, r) {
    k.fn[e] = function (e, t, n) {
      return this.animate(r, e, t, n);
    };
  }), k.timers = [], k.fx.tick = function () {
    var e,
        t = 0,
        n = k.timers;

    for (rt = Date.now(); t < n.length; t++) {
      (e = n[t])() || n[t] !== e || n.splice(t--, 1);
    }

    n.length || k.fx.stop(), rt = void 0;
  }, k.fx.timer = function (e) {
    k.timers.push(e), k.fx.start();
  }, k.fx.interval = 13, k.fx.start = function () {
    it || (it = !0, lt());
  }, k.fx.stop = function () {
    it = null;
  }, k.fx.speeds = {
    slow: 600,
    fast: 200,
    _default: 400
  }, k.fn.delay = function (r, e) {
    return r = k.fx && k.fx.speeds[r] || r, e = e || "fx", this.queue(e, function (e, t) {
      var n = C.setTimeout(e, r);

      t.stop = function () {
        C.clearTimeout(n);
      };
    });
  }, ot = E.createElement("input"), at = E.createElement("select").appendChild(E.createElement("option")), ot.type = "checkbox", y.checkOn = "" !== ot.value, y.optSelected = at.selected, (ot = E.createElement("input")).value = "t", ot.type = "radio", y.radioValue = "t" === ot.value;
  var ht,
      gt = k.expr.attrHandle;
  k.fn.extend({
    attr: function attr(e, t) {
      return _(this, k.attr, e, t, 1 < arguments.length);
    },
    removeAttr: function removeAttr(e) {
      return this.each(function () {
        k.removeAttr(this, e);
      });
    }
  }), k.extend({
    attr: function attr(e, t, n) {
      var r,
          i,
          o = e.nodeType;
      if (3 !== o && 8 !== o && 2 !== o) return "undefined" == typeof e.getAttribute ? k.prop(e, t, n) : (1 === o && k.isXMLDoc(e) || (i = k.attrHooks[t.toLowerCase()] || (k.expr.match.bool.test(t) ? ht : void 0)), void 0 !== n ? null === n ? void k.removeAttr(e, t) : i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : (e.setAttribute(t, n + ""), n) : i && "get" in i && null !== (r = i.get(e, t)) ? r : null == (r = k.find.attr(e, t)) ? void 0 : r);
    },
    attrHooks: {
      type: {
        set: function set(e, t) {
          if (!y.radioValue && "radio" === t && A(e, "input")) {
            var n = e.value;
            return e.setAttribute("type", t), n && (e.value = n), t;
          }
        }
      }
    },
    removeAttr: function removeAttr(e, t) {
      var n,
          r = 0,
          i = t && t.match(R);
      if (i && 1 === e.nodeType) while (n = i[r++]) {
        e.removeAttribute(n);
      }
    }
  }), ht = {
    set: function set(e, t, n) {
      return !1 === t ? k.removeAttr(e, n) : e.setAttribute(n, n), n;
    }
  }, k.each(k.expr.match.bool.source.match(/\w+/g), function (e, t) {
    var a = gt[t] || k.find.attr;

    gt[t] = function (e, t, n) {
      var r,
          i,
          o = t.toLowerCase();
      return n || (i = gt[o], gt[o] = r, r = null != a(e, t, n) ? o : null, gt[o] = i), r;
    };
  });
  var vt = /^(?:input|select|textarea|button)$/i,
      yt = /^(?:a|area)$/i;

  function mt(e) {
    return (e.match(R) || []).join(" ");
  }

  function xt(e) {
    return e.getAttribute && e.getAttribute("class") || "";
  }

  function bt(e) {
    return Array.isArray(e) ? e : "string" == typeof e && e.match(R) || [];
  }

  k.fn.extend({
    prop: function prop(e, t) {
      return _(this, k.prop, e, t, 1 < arguments.length);
    },
    removeProp: function removeProp(e) {
      return this.each(function () {
        delete this[k.propFix[e] || e];
      });
    }
  }), k.extend({
    prop: function prop(e, t, n) {
      var r,
          i,
          o = e.nodeType;
      if (3 !== o && 8 !== o && 2 !== o) return 1 === o && k.isXMLDoc(e) || (t = k.propFix[t] || t, i = k.propHooks[t]), void 0 !== n ? i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : e[t] = n : i && "get" in i && null !== (r = i.get(e, t)) ? r : e[t];
    },
    propHooks: {
      tabIndex: {
        get: function get(e) {
          var t = k.find.attr(e, "tabindex");
          return t ? parseInt(t, 10) : vt.test(e.nodeName) || yt.test(e.nodeName) && e.href ? 0 : -1;
        }
      }
    },
    propFix: {
      "for": "htmlFor",
      "class": "className"
    }
  }), y.optSelected || (k.propHooks.selected = {
    get: function get(e) {
      var t = e.parentNode;
      return t && t.parentNode && t.parentNode.selectedIndex, null;
    },
    set: function set(e) {
      var t = e.parentNode;
      t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex);
    }
  }), k.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
    k.propFix[this.toLowerCase()] = this;
  }), k.fn.extend({
    addClass: function addClass(t) {
      var e,
          n,
          r,
          i,
          o,
          a,
          s,
          u = 0;
      if (m(t)) return this.each(function (e) {
        k(this).addClass(t.call(this, e, xt(this)));
      });
      if ((e = bt(t)).length) while (n = this[u++]) {
        if (i = xt(n), r = 1 === n.nodeType && " " + mt(i) + " ") {
          a = 0;

          while (o = e[a++]) {
            r.indexOf(" " + o + " ") < 0 && (r += o + " ");
          }

          i !== (s = mt(r)) && n.setAttribute("class", s);
        }
      }
      return this;
    },
    removeClass: function removeClass(t) {
      var e,
          n,
          r,
          i,
          o,
          a,
          s,
          u = 0;
      if (m(t)) return this.each(function (e) {
        k(this).removeClass(t.call(this, e, xt(this)));
      });
      if (!arguments.length) return this.attr("class", "");
      if ((e = bt(t)).length) while (n = this[u++]) {
        if (i = xt(n), r = 1 === n.nodeType && " " + mt(i) + " ") {
          a = 0;

          while (o = e[a++]) {
            while (-1 < r.indexOf(" " + o + " ")) {
              r = r.replace(" " + o + " ", " ");
            }
          }

          i !== (s = mt(r)) && n.setAttribute("class", s);
        }
      }
      return this;
    },
    toggleClass: function toggleClass(i, t) {
      var o = _typeof(i),
          a = "string" === o || Array.isArray(i);

      return "boolean" == typeof t && a ? t ? this.addClass(i) : this.removeClass(i) : m(i) ? this.each(function (e) {
        k(this).toggleClass(i.call(this, e, xt(this), t), t);
      }) : this.each(function () {
        var e, t, n, r;

        if (a) {
          t = 0, n = k(this), r = bt(i);

          while (e = r[t++]) {
            n.hasClass(e) ? n.removeClass(e) : n.addClass(e);
          }
        } else void 0 !== i && "boolean" !== o || ((e = xt(this)) && Q.set(this, "__className__", e), this.setAttribute && this.setAttribute("class", e || !1 === i ? "" : Q.get(this, "__className__") || ""));
      });
    },
    hasClass: function hasClass(e) {
      var t,
          n,
          r = 0;
      t = " " + e + " ";

      while (n = this[r++]) {
        if (1 === n.nodeType && -1 < (" " + mt(xt(n)) + " ").indexOf(t)) return !0;
      }

      return !1;
    }
  });
  var wt = /\r/g;
  k.fn.extend({
    val: function val(n) {
      var r,
          e,
          i,
          t = this[0];
      return arguments.length ? (i = m(n), this.each(function (e) {
        var t;
        1 === this.nodeType && (null == (t = i ? n.call(this, e, k(this).val()) : n) ? t = "" : "number" == typeof t ? t += "" : Array.isArray(t) && (t = k.map(t, function (e) {
          return null == e ? "" : e + "";
        })), (r = k.valHooks[this.type] || k.valHooks[this.nodeName.toLowerCase()]) && "set" in r && void 0 !== r.set(this, t, "value") || (this.value = t));
      })) : t ? (r = k.valHooks[t.type] || k.valHooks[t.nodeName.toLowerCase()]) && "get" in r && void 0 !== (e = r.get(t, "value")) ? e : "string" == typeof (e = t.value) ? e.replace(wt, "") : null == e ? "" : e : void 0;
    }
  }), k.extend({
    valHooks: {
      option: {
        get: function get(e) {
          var t = k.find.attr(e, "value");
          return null != t ? t : mt(k.text(e));
        }
      },
      select: {
        get: function get(e) {
          var t,
              n,
              r,
              i = e.options,
              o = e.selectedIndex,
              a = "select-one" === e.type,
              s = a ? null : [],
              u = a ? o + 1 : i.length;

          for (r = o < 0 ? u : a ? o : 0; r < u; r++) {
            if (((n = i[r]).selected || r === o) && !n.disabled && (!n.parentNode.disabled || !A(n.parentNode, "optgroup"))) {
              if (t = k(n).val(), a) return t;
              s.push(t);
            }
          }

          return s;
        },
        set: function set(e, t) {
          var n,
              r,
              i = e.options,
              o = k.makeArray(t),
              a = i.length;

          while (a--) {
            ((r = i[a]).selected = -1 < k.inArray(k.valHooks.option.get(r), o)) && (n = !0);
          }

          return n || (e.selectedIndex = -1), o;
        }
      }
    }
  }), k.each(["radio", "checkbox"], function () {
    k.valHooks[this] = {
      set: function set(e, t) {
        if (Array.isArray(t)) return e.checked = -1 < k.inArray(k(e).val(), t);
      }
    }, y.checkOn || (k.valHooks[this].get = function (e) {
      return null === e.getAttribute("value") ? "on" : e.value;
    });
  }), y.focusin = "onfocusin" in C;

  var Tt = /^(?:focusinfocus|focusoutblur)$/,
      Ct = function Ct(e) {
    e.stopPropagation();
  };

  k.extend(k.event, {
    trigger: function trigger(e, t, n, r) {
      var i,
          o,
          a,
          s,
          u,
          l,
          c,
          f,
          p = [n || E],
          d = v.call(e, "type") ? e.type : e,
          h = v.call(e, "namespace") ? e.namespace.split(".") : [];

      if (o = f = a = n = n || E, 3 !== n.nodeType && 8 !== n.nodeType && !Tt.test(d + k.event.triggered) && (-1 < d.indexOf(".") && (d = (h = d.split(".")).shift(), h.sort()), u = d.indexOf(":") < 0 && "on" + d, (e = e[k.expando] ? e : new k.Event(d, "object" == _typeof(e) && e)).isTrigger = r ? 2 : 3, e.namespace = h.join("."), e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = n), t = null == t ? [e] : k.makeArray(t, [e]), c = k.event.special[d] || {}, r || !c.trigger || !1 !== c.trigger.apply(n, t))) {
        if (!r && !c.noBubble && !x(n)) {
          for (s = c.delegateType || d, Tt.test(s + d) || (o = o.parentNode); o; o = o.parentNode) {
            p.push(o), a = o;
          }

          a === (n.ownerDocument || E) && p.push(a.defaultView || a.parentWindow || C);
        }

        i = 0;

        while ((o = p[i++]) && !e.isPropagationStopped()) {
          f = o, e.type = 1 < i ? s : c.bindType || d, (l = (Q.get(o, "events") || {})[e.type] && Q.get(o, "handle")) && l.apply(o, t), (l = u && o[u]) && l.apply && G(o) && (e.result = l.apply(o, t), !1 === e.result && e.preventDefault());
        }

        return e.type = d, r || e.isDefaultPrevented() || c._default && !1 !== c._default.apply(p.pop(), t) || !G(n) || u && m(n[d]) && !x(n) && ((a = n[u]) && (n[u] = null), k.event.triggered = d, e.isPropagationStopped() && f.addEventListener(d, Ct), n[d](), e.isPropagationStopped() && f.removeEventListener(d, Ct), k.event.triggered = void 0, a && (n[u] = a)), e.result;
      }
    },
    simulate: function simulate(e, t, n) {
      var r = k.extend(new k.Event(), n, {
        type: e,
        isSimulated: !0
      });
      k.event.trigger(r, null, t);
    }
  }), k.fn.extend({
    trigger: function trigger(e, t) {
      return this.each(function () {
        k.event.trigger(e, t, this);
      });
    },
    triggerHandler: function triggerHandler(e, t) {
      var n = this[0];
      if (n) return k.event.trigger(e, t, n, !0);
    }
  }), y.focusin || k.each({
    focus: "focusin",
    blur: "focusout"
  }, function (n, r) {
    var i = function i(e) {
      k.event.simulate(r, e.target, k.event.fix(e));
    };

    k.event.special[r] = {
      setup: function setup() {
        var e = this.ownerDocument || this,
            t = Q.access(e, r);
        t || e.addEventListener(n, i, !0), Q.access(e, r, (t || 0) + 1);
      },
      teardown: function teardown() {
        var e = this.ownerDocument || this,
            t = Q.access(e, r) - 1;
        t ? Q.access(e, r, t) : (e.removeEventListener(n, i, !0), Q.remove(e, r));
      }
    };
  });
  var Et = C.location,
      kt = Date.now(),
      St = /\?/;

  k.parseXML = function (e) {
    var t;
    if (!e || "string" != typeof e) return null;

    try {
      t = new C.DOMParser().parseFromString(e, "text/xml");
    } catch (e) {
      t = void 0;
    }

    return t && !t.getElementsByTagName("parsererror").length || k.error("Invalid XML: " + e), t;
  };

  var Nt = /\[\]$/,
      At = /\r?\n/g,
      Dt = /^(?:submit|button|image|reset|file)$/i,
      jt = /^(?:input|select|textarea|keygen)/i;

  function qt(n, e, r, i) {
    var t;
    if (Array.isArray(e)) k.each(e, function (e, t) {
      r || Nt.test(n) ? i(n, t) : qt(n + "[" + ("object" == _typeof(t) && null != t ? e : "") + "]", t, r, i);
    });else if (r || "object" !== w(e)) i(n, e);else for (t in e) {
      qt(n + "[" + t + "]", e[t], r, i);
    }
  }

  k.param = function (e, t) {
    var n,
        r = [],
        i = function i(e, t) {
      var n = m(t) ? t() : t;
      r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(null == n ? "" : n);
    };

    if (null == e) return "";
    if (Array.isArray(e) || e.jquery && !k.isPlainObject(e)) k.each(e, function () {
      i(this.name, this.value);
    });else for (n in e) {
      qt(n, e[n], t, i);
    }
    return r.join("&");
  }, k.fn.extend({
    serialize: function serialize() {
      return k.param(this.serializeArray());
    },
    serializeArray: function serializeArray() {
      return this.map(function () {
        var e = k.prop(this, "elements");
        return e ? k.makeArray(e) : this;
      }).filter(function () {
        var e = this.type;
        return this.name && !k(this).is(":disabled") && jt.test(this.nodeName) && !Dt.test(e) && (this.checked || !pe.test(e));
      }).map(function (e, t) {
        var n = k(this).val();
        return null == n ? null : Array.isArray(n) ? k.map(n, function (e) {
          return {
            name: t.name,
            value: e.replace(At, "\r\n")
          };
        }) : {
          name: t.name,
          value: n.replace(At, "\r\n")
        };
      }).get();
    }
  });
  var Lt = /%20/g,
      Ht = /#.*$/,
      Ot = /([?&])_=[^&]*/,
      Pt = /^(.*?):[ \t]*([^\r\n]*)$/gm,
      Rt = /^(?:GET|HEAD)$/,
      Mt = /^\/\//,
      It = {},
      Wt = {},
      $t = "*/".concat("*"),
      Ft = E.createElement("a");

  function Bt(o) {
    return function (e, t) {
      "string" != typeof e && (t = e, e = "*");
      var n,
          r = 0,
          i = e.toLowerCase().match(R) || [];
      if (m(t)) while (n = i[r++]) {
        "+" === n[0] ? (n = n.slice(1) || "*", (o[n] = o[n] || []).unshift(t)) : (o[n] = o[n] || []).push(t);
      }
    };
  }

  function _t(t, i, o, a) {
    var s = {},
        u = t === Wt;

    function l(e) {
      var r;
      return s[e] = !0, k.each(t[e] || [], function (e, t) {
        var n = t(i, o, a);
        return "string" != typeof n || u || s[n] ? u ? !(r = n) : void 0 : (i.dataTypes.unshift(n), l(n), !1);
      }), r;
    }

    return l(i.dataTypes[0]) || !s["*"] && l("*");
  }

  function zt(e, t) {
    var n,
        r,
        i = k.ajaxSettings.flatOptions || {};

    for (n in t) {
      void 0 !== t[n] && ((i[n] ? e : r || (r = {}))[n] = t[n]);
    }

    return r && k.extend(!0, e, r), e;
  }

  Ft.href = Et.href, k.extend({
    active: 0,
    lastModified: {},
    etag: {},
    ajaxSettings: {
      url: Et.href,
      type: "GET",
      isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(Et.protocol),
      global: !0,
      processData: !0,
      async: !0,
      contentType: "application/x-www-form-urlencoded; charset=UTF-8",
      accepts: {
        "*": $t,
        text: "text/plain",
        html: "text/html",
        xml: "application/xml, text/xml",
        json: "application/json, text/javascript"
      },
      contents: {
        xml: /\bxml\b/,
        html: /\bhtml/,
        json: /\bjson\b/
      },
      responseFields: {
        xml: "responseXML",
        text: "responseText",
        json: "responseJSON"
      },
      converters: {
        "* text": String,
        "text html": !0,
        "text json": JSON.parse,
        "text xml": k.parseXML
      },
      flatOptions: {
        url: !0,
        context: !0
      }
    },
    ajaxSetup: function ajaxSetup(e, t) {
      return t ? zt(zt(e, k.ajaxSettings), t) : zt(k.ajaxSettings, e);
    },
    ajaxPrefilter: Bt(It),
    ajaxTransport: Bt(Wt),
    ajax: function ajax(e, t) {
      "object" == _typeof(e) && (t = e, e = void 0), t = t || {};
      var c,
          f,
          p,
          n,
          d,
          r,
          h,
          g,
          i,
          o,
          v = k.ajaxSetup({}, t),
          y = v.context || v,
          m = v.context && (y.nodeType || y.jquery) ? k(y) : k.event,
          x = k.Deferred(),
          b = k.Callbacks("once memory"),
          w = v.statusCode || {},
          a = {},
          s = {},
          u = "canceled",
          T = {
        readyState: 0,
        getResponseHeader: function getResponseHeader(e) {
          var t;

          if (h) {
            if (!n) {
              n = {};

              while (t = Pt.exec(p)) {
                n[t[1].toLowerCase() + " "] = (n[t[1].toLowerCase() + " "] || []).concat(t[2]);
              }
            }

            t = n[e.toLowerCase() + " "];
          }

          return null == t ? null : t.join(", ");
        },
        getAllResponseHeaders: function getAllResponseHeaders() {
          return h ? p : null;
        },
        setRequestHeader: function setRequestHeader(e, t) {
          return null == h && (e = s[e.toLowerCase()] = s[e.toLowerCase()] || e, a[e] = t), this;
        },
        overrideMimeType: function overrideMimeType(e) {
          return null == h && (v.mimeType = e), this;
        },
        statusCode: function statusCode(e) {
          var t;
          if (e) if (h) T.always(e[T.status]);else for (t in e) {
            w[t] = [w[t], e[t]];
          }
          return this;
        },
        abort: function abort(e) {
          var t = e || u;
          return c && c.abort(t), l(0, t), this;
        }
      };

      if (x.promise(T), v.url = ((e || v.url || Et.href) + "").replace(Mt, Et.protocol + "//"), v.type = t.method || t.type || v.method || v.type, v.dataTypes = (v.dataType || "*").toLowerCase().match(R) || [""], null == v.crossDomain) {
        r = E.createElement("a");

        try {
          r.href = v.url, r.href = r.href, v.crossDomain = Ft.protocol + "//" + Ft.host != r.protocol + "//" + r.host;
        } catch (e) {
          v.crossDomain = !0;
        }
      }

      if (v.data && v.processData && "string" != typeof v.data && (v.data = k.param(v.data, v.traditional)), _t(It, v, t, T), h) return T;

      for (i in (g = k.event && v.global) && 0 == k.active++ && k.event.trigger("ajaxStart"), v.type = v.type.toUpperCase(), v.hasContent = !Rt.test(v.type), f = v.url.replace(Ht, ""), v.hasContent ? v.data && v.processData && 0 === (v.contentType || "").indexOf("application/x-www-form-urlencoded") && (v.data = v.data.replace(Lt, "+")) : (o = v.url.slice(f.length), v.data && (v.processData || "string" == typeof v.data) && (f += (St.test(f) ? "&" : "?") + v.data, delete v.data), !1 === v.cache && (f = f.replace(Ot, "$1"), o = (St.test(f) ? "&" : "?") + "_=" + kt++ + o), v.url = f + o), v.ifModified && (k.lastModified[f] && T.setRequestHeader("If-Modified-Since", k.lastModified[f]), k.etag[f] && T.setRequestHeader("If-None-Match", k.etag[f])), (v.data && v.hasContent && !1 !== v.contentType || t.contentType) && T.setRequestHeader("Content-Type", v.contentType), T.setRequestHeader("Accept", v.dataTypes[0] && v.accepts[v.dataTypes[0]] ? v.accepts[v.dataTypes[0]] + ("*" !== v.dataTypes[0] ? ", " + $t + "; q=0.01" : "") : v.accepts["*"]), v.headers) {
        T.setRequestHeader(i, v.headers[i]);
      }

      if (v.beforeSend && (!1 === v.beforeSend.call(y, T, v) || h)) return T.abort();

      if (u = "abort", b.add(v.complete), T.done(v.success), T.fail(v.error), c = _t(Wt, v, t, T)) {
        if (T.readyState = 1, g && m.trigger("ajaxSend", [T, v]), h) return T;
        v.async && 0 < v.timeout && (d = C.setTimeout(function () {
          T.abort("timeout");
        }, v.timeout));

        try {
          h = !1, c.send(a, l);
        } catch (e) {
          if (h) throw e;
          l(-1, e);
        }
      } else l(-1, "No Transport");

      function l(e, t, n, r) {
        var i,
            o,
            a,
            s,
            u,
            l = t;
        h || (h = !0, d && C.clearTimeout(d), c = void 0, p = r || "", T.readyState = 0 < e ? 4 : 0, i = 200 <= e && e < 300 || 304 === e, n && (s = function (e, t, n) {
          var r,
              i,
              o,
              a,
              s = e.contents,
              u = e.dataTypes;

          while ("*" === u[0]) {
            u.shift(), void 0 === r && (r = e.mimeType || t.getResponseHeader("Content-Type"));
          }

          if (r) for (i in s) {
            if (s[i] && s[i].test(r)) {
              u.unshift(i);
              break;
            }
          }
          if (u[0] in n) o = u[0];else {
            for (i in n) {
              if (!u[0] || e.converters[i + " " + u[0]]) {
                o = i;
                break;
              }

              a || (a = i);
            }

            o = o || a;
          }
          if (o) return o !== u[0] && u.unshift(o), n[o];
        }(v, T, n)), s = function (e, t, n, r) {
          var i,
              o,
              a,
              s,
              u,
              l = {},
              c = e.dataTypes.slice();
          if (c[1]) for (a in e.converters) {
            l[a.toLowerCase()] = e.converters[a];
          }
          o = c.shift();

          while (o) {
            if (e.responseFields[o] && (n[e.responseFields[o]] = t), !u && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), u = o, o = c.shift()) if ("*" === o) o = u;else if ("*" !== u && u !== o) {
              if (!(a = l[u + " " + o] || l["* " + o])) for (i in l) {
                if ((s = i.split(" "))[1] === o && (a = l[u + " " + s[0]] || l["* " + s[0]])) {
                  !0 === a ? a = l[i] : !0 !== l[i] && (o = s[0], c.unshift(s[1]));
                  break;
                }
              }
              if (!0 !== a) if (a && e["throws"]) t = a(t);else try {
                t = a(t);
              } catch (e) {
                return {
                  state: "parsererror",
                  error: a ? e : "No conversion from " + u + " to " + o
                };
              }
            }
          }

          return {
            state: "success",
            data: t
          };
        }(v, s, T, i), i ? (v.ifModified && ((u = T.getResponseHeader("Last-Modified")) && (k.lastModified[f] = u), (u = T.getResponseHeader("etag")) && (k.etag[f] = u)), 204 === e || "HEAD" === v.type ? l = "nocontent" : 304 === e ? l = "notmodified" : (l = s.state, o = s.data, i = !(a = s.error))) : (a = l, !e && l || (l = "error", e < 0 && (e = 0))), T.status = e, T.statusText = (t || l) + "", i ? x.resolveWith(y, [o, l, T]) : x.rejectWith(y, [T, l, a]), T.statusCode(w), w = void 0, g && m.trigger(i ? "ajaxSuccess" : "ajaxError", [T, v, i ? o : a]), b.fireWith(y, [T, l]), g && (m.trigger("ajaxComplete", [T, v]), --k.active || k.event.trigger("ajaxStop")));
      }

      return T;
    },
    getJSON: function getJSON(e, t, n) {
      return k.get(e, t, n, "json");
    },
    getScript: function getScript(e, t) {
      return k.get(e, void 0, t, "script");
    }
  }), k.each(["get", "post"], function (e, i) {
    k[i] = function (e, t, n, r) {
      return m(t) && (r = r || n, n = t, t = void 0), k.ajax(k.extend({
        url: e,
        type: i,
        dataType: r,
        data: t,
        success: n
      }, k.isPlainObject(e) && e));
    };
  }), k._evalUrl = function (e, t) {
    return k.ajax({
      url: e,
      type: "GET",
      dataType: "script",
      cache: !0,
      async: !1,
      global: !1,
      converters: {
        "text script": function textScript() {}
      },
      dataFilter: function dataFilter(e) {
        k.globalEval(e, t);
      }
    });
  }, k.fn.extend({
    wrapAll: function wrapAll(e) {
      var t;
      return this[0] && (m(e) && (e = e.call(this[0])), t = k(e, this[0].ownerDocument).eq(0).clone(!0), this[0].parentNode && t.insertBefore(this[0]), t.map(function () {
        var e = this;

        while (e.firstElementChild) {
          e = e.firstElementChild;
        }

        return e;
      }).append(this)), this;
    },
    wrapInner: function wrapInner(n) {
      return m(n) ? this.each(function (e) {
        k(this).wrapInner(n.call(this, e));
      }) : this.each(function () {
        var e = k(this),
            t = e.contents();
        t.length ? t.wrapAll(n) : e.append(n);
      });
    },
    wrap: function wrap(t) {
      var n = m(t);
      return this.each(function (e) {
        k(this).wrapAll(n ? t.call(this, e) : t);
      });
    },
    unwrap: function unwrap(e) {
      return this.parent(e).not("body").each(function () {
        k(this).replaceWith(this.childNodes);
      }), this;
    }
  }), k.expr.pseudos.hidden = function (e) {
    return !k.expr.pseudos.visible(e);
  }, k.expr.pseudos.visible = function (e) {
    return !!(e.offsetWidth || e.offsetHeight || e.getClientRects().length);
  }, k.ajaxSettings.xhr = function () {
    try {
      return new C.XMLHttpRequest();
    } catch (e) {}
  };
  var Ut = {
    0: 200,
    1223: 204
  },
      Xt = k.ajaxSettings.xhr();
  y.cors = !!Xt && "withCredentials" in Xt, y.ajax = Xt = !!Xt, k.ajaxTransport(function (i) {
    var _o, a;

    if (y.cors || Xt && !i.crossDomain) return {
      send: function send(e, t) {
        var n,
            r = i.xhr();
        if (r.open(i.type, i.url, i.async, i.username, i.password), i.xhrFields) for (n in i.xhrFields) {
          r[n] = i.xhrFields[n];
        }

        for (n in i.mimeType && r.overrideMimeType && r.overrideMimeType(i.mimeType), i.crossDomain || e["X-Requested-With"] || (e["X-Requested-With"] = "XMLHttpRequest"), e) {
          r.setRequestHeader(n, e[n]);
        }

        _o = function o(e) {
          return function () {
            _o && (_o = a = r.onload = r.onerror = r.onabort = r.ontimeout = r.onreadystatechange = null, "abort" === e ? r.abort() : "error" === e ? "number" != typeof r.status ? t(0, "error") : t(r.status, r.statusText) : t(Ut[r.status] || r.status, r.statusText, "text" !== (r.responseType || "text") || "string" != typeof r.responseText ? {
              binary: r.response
            } : {
              text: r.responseText
            }, r.getAllResponseHeaders()));
          };
        }, r.onload = _o(), a = r.onerror = r.ontimeout = _o("error"), void 0 !== r.onabort ? r.onabort = a : r.onreadystatechange = function () {
          4 === r.readyState && C.setTimeout(function () {
            _o && a();
          });
        }, _o = _o("abort");

        try {
          r.send(i.hasContent && i.data || null);
        } catch (e) {
          if (_o) throw e;
        }
      },
      abort: function abort() {
        _o && _o();
      }
    };
  }), k.ajaxPrefilter(function (e) {
    e.crossDomain && (e.contents.script = !1);
  }), k.ajaxSetup({
    accepts: {
      script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
    },
    contents: {
      script: /\b(?:java|ecma)script\b/
    },
    converters: {
      "text script": function textScript(e) {
        return k.globalEval(e), e;
      }
    }
  }), k.ajaxPrefilter("script", function (e) {
    void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET");
  }), k.ajaxTransport("script", function (n) {
    var r, _i;

    if (n.crossDomain || n.scriptAttrs) return {
      send: function send(e, t) {
        r = k("<script>").attr(n.scriptAttrs || {}).prop({
          charset: n.scriptCharset,
          src: n.url
        }).on("load error", _i = function i(e) {
          r.remove(), _i = null, e && t("error" === e.type ? 404 : 200, e.type);
        }), E.head.appendChild(r[0]);
      },
      abort: function abort() {
        _i && _i();
      }
    };
  });
  var Vt,
      Gt = [],
      Yt = /(=)\?(?=&|$)|\?\?/;
  k.ajaxSetup({
    jsonp: "callback",
    jsonpCallback: function jsonpCallback() {
      var e = Gt.pop() || k.expando + "_" + kt++;
      return this[e] = !0, e;
    }
  }), k.ajaxPrefilter("json jsonp", function (e, t, n) {
    var r,
        i,
        o,
        a = !1 !== e.jsonp && (Yt.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Yt.test(e.data) && "data");
    if (a || "jsonp" === e.dataTypes[0]) return r = e.jsonpCallback = m(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, a ? e[a] = e[a].replace(Yt, "$1" + r) : !1 !== e.jsonp && (e.url += (St.test(e.url) ? "&" : "?") + e.jsonp + "=" + r), e.converters["script json"] = function () {
      return o || k.error(r + " was not called"), o[0];
    }, e.dataTypes[0] = "json", i = C[r], C[r] = function () {
      o = arguments;
    }, n.always(function () {
      void 0 === i ? k(C).removeProp(r) : C[r] = i, e[r] && (e.jsonpCallback = t.jsonpCallback, Gt.push(r)), o && m(i) && i(o[0]), o = i = void 0;
    }), "script";
  }), y.createHTMLDocument = ((Vt = E.implementation.createHTMLDocument("").body).innerHTML = "<form></form><form></form>", 2 === Vt.childNodes.length), k.parseHTML = function (e, t, n) {
    return "string" != typeof e ? [] : ("boolean" == typeof t && (n = t, t = !1), t || (y.createHTMLDocument ? ((r = (t = E.implementation.createHTMLDocument("")).createElement("base")).href = E.location.href, t.head.appendChild(r)) : t = E), o = !n && [], (i = D.exec(e)) ? [t.createElement(i[1])] : (i = we([e], t, o), o && o.length && k(o).remove(), k.merge([], i.childNodes)));
    var r, i, o;
  }, k.fn.load = function (e, t, n) {
    var r,
        i,
        o,
        a = this,
        s = e.indexOf(" ");
    return -1 < s && (r = mt(e.slice(s)), e = e.slice(0, s)), m(t) ? (n = t, t = void 0) : t && "object" == _typeof(t) && (i = "POST"), 0 < a.length && k.ajax({
      url: e,
      type: i || "GET",
      dataType: "html",
      data: t
    }).done(function (e) {
      o = arguments, a.html(r ? k("<div>").append(k.parseHTML(e)).find(r) : e);
    }).always(n && function (e, t) {
      a.each(function () {
        n.apply(this, o || [e.responseText, t, e]);
      });
    }), this;
  }, k.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (e, t) {
    k.fn[t] = function (e) {
      return this.on(t, e);
    };
  }), k.expr.pseudos.animated = function (t) {
    return k.grep(k.timers, function (e) {
      return t === e.elem;
    }).length;
  }, k.offset = {
    setOffset: function setOffset(e, t, n) {
      var r,
          i,
          o,
          a,
          s,
          u,
          l = k.css(e, "position"),
          c = k(e),
          f = {};
      "static" === l && (e.style.position = "relative"), s = c.offset(), o = k.css(e, "top"), u = k.css(e, "left"), ("absolute" === l || "fixed" === l) && -1 < (o + u).indexOf("auto") ? (a = (r = c.position()).top, i = r.left) : (a = parseFloat(o) || 0, i = parseFloat(u) || 0), m(t) && (t = t.call(e, n, k.extend({}, s))), null != t.top && (f.top = t.top - s.top + a), null != t.left && (f.left = t.left - s.left + i), "using" in t ? t.using.call(e, f) : c.css(f);
    }
  }, k.fn.extend({
    offset: function offset(t) {
      if (arguments.length) return void 0 === t ? this : this.each(function (e) {
        k.offset.setOffset(this, t, e);
      });
      var e,
          n,
          r = this[0];
      return r ? r.getClientRects().length ? (e = r.getBoundingClientRect(), n = r.ownerDocument.defaultView, {
        top: e.top + n.pageYOffset,
        left: e.left + n.pageXOffset
      }) : {
        top: 0,
        left: 0
      } : void 0;
    },
    position: function position() {
      if (this[0]) {
        var e,
            t,
            n,
            r = this[0],
            i = {
          top: 0,
          left: 0
        };
        if ("fixed" === k.css(r, "position")) t = r.getBoundingClientRect();else {
          t = this.offset(), n = r.ownerDocument, e = r.offsetParent || n.documentElement;

          while (e && (e === n.body || e === n.documentElement) && "static" === k.css(e, "position")) {
            e = e.parentNode;
          }

          e && e !== r && 1 === e.nodeType && ((i = k(e).offset()).top += k.css(e, "borderTopWidth", !0), i.left += k.css(e, "borderLeftWidth", !0));
        }
        return {
          top: t.top - i.top - k.css(r, "marginTop", !0),
          left: t.left - i.left - k.css(r, "marginLeft", !0)
        };
      }
    },
    offsetParent: function offsetParent() {
      return this.map(function () {
        var e = this.offsetParent;

        while (e && "static" === k.css(e, "position")) {
          e = e.offsetParent;
        }

        return e || ie;
      });
    }
  }), k.each({
    scrollLeft: "pageXOffset",
    scrollTop: "pageYOffset"
  }, function (t, i) {
    var o = "pageYOffset" === i;

    k.fn[t] = function (e) {
      return _(this, function (e, t, n) {
        var r;
        if (x(e) ? r = e : 9 === e.nodeType && (r = e.defaultView), void 0 === n) return r ? r[i] : e[t];
        r ? r.scrollTo(o ? r.pageXOffset : n, o ? n : r.pageYOffset) : e[t] = n;
      }, t, e, arguments.length);
    };
  }), k.each(["top", "left"], function (e, n) {
    k.cssHooks[n] = ze(y.pixelPosition, function (e, t) {
      if (t) return t = _e(e, n), $e.test(t) ? k(e).position()[n] + "px" : t;
    });
  }), k.each({
    Height: "height",
    Width: "width"
  }, function (a, s) {
    k.each({
      padding: "inner" + a,
      content: s,
      "": "outer" + a
    }, function (r, o) {
      k.fn[o] = function (e, t) {
        var n = arguments.length && (r || "boolean" != typeof e),
            i = r || (!0 === e || !0 === t ? "margin" : "border");
        return _(this, function (e, t, n) {
          var r;
          return x(e) ? 0 === o.indexOf("outer") ? e["inner" + a] : e.document.documentElement["client" + a] : 9 === e.nodeType ? (r = e.documentElement, Math.max(e.body["scroll" + a], r["scroll" + a], e.body["offset" + a], r["offset" + a], r["client" + a])) : void 0 === n ? k.css(e, t, i) : k.style(e, t, n, i);
        }, s, n ? e : void 0, n);
      };
    });
  }), k.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function (e, n) {
    k.fn[n] = function (e, t) {
      return 0 < arguments.length ? this.on(n, null, e, t) : this.trigger(n);
    };
  }), k.fn.extend({
    hover: function hover(e, t) {
      return this.mouseenter(e).mouseleave(t || e);
    }
  }), k.fn.extend({
    bind: function bind(e, t, n) {
      return this.on(e, null, t, n);
    },
    unbind: function unbind(e, t) {
      return this.off(e, null, t);
    },
    delegate: function delegate(e, t, n, r) {
      return this.on(t, e, n, r);
    },
    undelegate: function undelegate(e, t, n) {
      return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n);
    }
  }), k.proxy = function (e, t) {
    var n, r, i;
    if ("string" == typeof t && (n = e[t], t = e, e = n), m(e)) return r = s.call(arguments, 2), (i = function i() {
      return e.apply(t || this, r.concat(s.call(arguments)));
    }).guid = e.guid = e.guid || k.guid++, i;
  }, k.holdReady = function (e) {
    e ? k.readyWait++ : k.ready(!0);
  }, k.isArray = Array.isArray, k.parseJSON = JSON.parse, k.nodeName = A, k.isFunction = m, k.isWindow = x, k.camelCase = V, k.type = w, k.now = Date.now, k.isNumeric = function (e) {
    var t = k.type(e);
    return ("number" === t || "string" === t) && !isNaN(e - parseFloat(e));
  }, "function" == typeof define && define.amd && define("jquery", [], function () {
    return k;
  });
  var Qt = C.jQuery,
      Jt = C.$;
  return k.noConflict = function (e) {
    return C.$ === k && (C.$ = Jt), e && C.jQuery === k && (C.jQuery = Qt), k;
  }, e || (C.jQuery = C.$ = k), k;
});
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

!function (t, e, i) {
  !function () {
    var s,
        a,
        n,
        h = "2.2.3",
        o = "datepicker",
        r = ".datepicker-here",
        c = !1,
        d = '<div class="datepicker"><i class="datepicker--pointer"></i><nav class="datepicker--nav"></nav><div class="datepicker--content"></div></div>',
        l = {
      classes: "",
      inline: !1,
      language: "ru",
      startDate: new Date(),
      firstDay: "",
      weekends: [6, 0],
      dateFormat: "",
      altField: "",
      altFieldDateFormat: "@",
      toggleSelected: !0,
      keyboardNav: !0,
      position: "bottom left",
      offset: 12,
      view: "days",
      minView: "days",
      showOtherMonths: !0,
      selectOtherMonths: !0,
      moveToOtherMonthsOnSelect: !0,
      showOtherYears: !0,
      selectOtherYears: !0,
      moveToOtherYearsOnSelect: !0,
      minDate: "",
      maxDate: "",
      disableNavWhenOutOfRange: !0,
      multipleDates: !1,
      multipleDatesSeparator: ",",
      range: !1,
      todayButton: !1,
      clearButton: !1,
      showEvent: "focus",
      autoClose: !1,
      monthsField: "monthsShort",
      prevHtml: '<svg><path d="M 17,12 l -5,5 l 5,5"></path></svg>',
      nextHtml: '<svg><path d="M 14,12 l 5,5 l -5,5"></path></svg>',
      navTitles: {
        days: "MM, <i>yyyy</i>",
        months: "yyyy",
        years: "yyyy1 - yyyy2"
      },
      timepicker: !1,
      onlyTimepicker: !1,
      dateTimeSeparator: " ",
      timeFormat: "",
      minHours: 0,
      maxHours: 24,
      minMinutes: 0,
      maxMinutes: 59,
      hoursStep: 1,
      minutesStep: 1,
      onSelect: "",
      onShow: "",
      onHide: "",
      onChangeMonth: "",
      onChangeYear: "",
      onChangeDecade: "",
      onChangeView: "",
      onRenderCell: ""
    },
        u = {
      ctrlRight: [17, 39],
      ctrlUp: [17, 38],
      ctrlLeft: [17, 37],
      ctrlDown: [17, 40],
      shiftRight: [16, 39],
      shiftUp: [16, 38],
      shiftLeft: [16, 37],
      shiftDown: [16, 40],
      altUp: [18, 38],
      altRight: [18, 39],
      altLeft: [18, 37],
      altDown: [18, 40],
      ctrlShiftUp: [16, 17, 38]
    },
        m = function m(t, a) {
      this.el = t, this.$el = e(t), this.opts = e.extend(!0, {}, l, a, this.$el.data()), s == i && (s = e("body")), this.opts.startDate || (this.opts.startDate = new Date()), "INPUT" == this.el.nodeName && (this.elIsInput = !0), this.opts.altField && (this.$altField = "string" == typeof this.opts.altField ? e(this.opts.altField) : this.opts.altField), this.inited = !1, this.visible = !1, this.silent = !1, this.currentDate = this.opts.startDate, this.currentView = this.opts.view, this._createShortCuts(), this.selectedDates = [], this.views = {}, this.keys = [], this.minRange = "", this.maxRange = "", this._prevOnSelectValue = "", this.init();
    };

    n = m, n.prototype = {
      VERSION: h,
      viewIndexes: ["days", "months", "years"],
      init: function init() {
        c || this.opts.inline || !this.elIsInput || this._buildDatepickersContainer(), this._buildBaseHtml(), this._defineLocale(this.opts.language), this._syncWithMinMaxDates(), this.elIsInput && (this.opts.inline || (this._setPositionClasses(this.opts.position), this._bindEvents()), this.opts.keyboardNav && !this.opts.onlyTimepicker && this._bindKeyboardEvents(), this.$datepicker.on("mousedown", this._onMouseDownDatepicker.bind(this)), this.$datepicker.on("mouseup", this._onMouseUpDatepicker.bind(this))), this.opts.classes && this.$datepicker.addClass(this.opts.classes), this.opts.timepicker && (this.timepicker = new e.fn.datepicker.Timepicker(this, this.opts), this._bindTimepickerEvents()), this.opts.onlyTimepicker && this.$datepicker.addClass("-only-timepicker-"), this.views[this.currentView] = new e.fn.datepicker.Body(this, this.currentView, this.opts), this.views[this.currentView].show(), this.nav = new e.fn.datepicker.Navigation(this, this.opts), this.view = this.currentView, this.$el.on("clickCell.adp", this._onClickCell.bind(this)), this.$datepicker.on("mouseenter", ".datepicker--cell", this._onMouseEnterCell.bind(this)), this.$datepicker.on("mouseleave", ".datepicker--cell", this._onMouseLeaveCell.bind(this)), this.inited = !0;
      },
      _createShortCuts: function _createShortCuts() {
        this.minDate = this.opts.minDate ? this.opts.minDate : new Date(-86399999136e5), this.maxDate = this.opts.maxDate ? this.opts.maxDate : new Date(86399999136e5);
      },
      _bindEvents: function _bindEvents() {
        this.$el.on(this.opts.showEvent + ".adp", this._onShowEvent.bind(this)), this.$el.on("mouseup.adp", this._onMouseUpEl.bind(this)), this.$el.on("blur.adp", this._onBlur.bind(this)), this.$el.on("keyup.adp", this._onKeyUpGeneral.bind(this)), e(t).on("resize.adp", this._onResize.bind(this)), e("body").on("mouseup.adp", this._onMouseUpBody.bind(this));
      },
      _bindKeyboardEvents: function _bindKeyboardEvents() {
        this.$el.on("keydown.adp", this._onKeyDown.bind(this)), this.$el.on("keyup.adp", this._onKeyUp.bind(this)), this.$el.on("hotKey.adp", this._onHotKey.bind(this));
      },
      _bindTimepickerEvents: function _bindTimepickerEvents() {
        this.$el.on("timeChange.adp", this._onTimeChange.bind(this));
      },
      isWeekend: function isWeekend(t) {
        return -1 !== this.opts.weekends.indexOf(t);
      },
      _defineLocale: function _defineLocale(t) {
        "string" == typeof t ? (this.loc = e.fn.datepicker.language[t], this.loc || (console.warn("Can't find language \"" + t + '" in Datepicker.language, will use "ru" instead'), this.loc = e.extend(!0, {}, e.fn.datepicker.language.ru)), this.loc = e.extend(!0, {}, e.fn.datepicker.language.ru, e.fn.datepicker.language[t])) : this.loc = e.extend(!0, {}, e.fn.datepicker.language.ru, t), this.opts.dateFormat && (this.loc.dateFormat = this.opts.dateFormat), this.opts.timeFormat && (this.loc.timeFormat = this.opts.timeFormat), "" !== this.opts.firstDay && (this.loc.firstDay = this.opts.firstDay), this.opts.timepicker && (this.loc.dateFormat = [this.loc.dateFormat, this.loc.timeFormat].join(this.opts.dateTimeSeparator)), this.opts.onlyTimepicker && (this.loc.dateFormat = this.loc.timeFormat);
        var i = this._getWordBoundaryRegExp;
        (this.loc.timeFormat.match(i("aa")) || this.loc.timeFormat.match(i("AA"))) && (this.ampm = !0);
      },
      _buildDatepickersContainer: function _buildDatepickersContainer() {
        c = !0, s.append('<div class="datepickers-container" id="datepickers-container"></div>'), a = e("#datepickers-container");
      },
      _buildBaseHtml: function _buildBaseHtml() {
        var t,
            i = e('<div class="datepicker-inline">');
        t = "INPUT" == this.el.nodeName ? this.opts.inline ? i.insertAfter(this.$el) : a : i.appendTo(this.$el), this.$datepicker = e(d).appendTo(t), this.$content = e(".datepicker--content", this.$datepicker), this.$nav = e(".datepicker--nav", this.$datepicker);
      },
      _triggerOnChange: function _triggerOnChange() {
        if (!this.selectedDates.length) {
          if ("" === this._prevOnSelectValue) return;
          return this._prevOnSelectValue = "", this.opts.onSelect("", "", this);
        }

        var t,
            e = this.selectedDates,
            i = n.getParsedDate(e[0]),
            s = this,
            a = new Date(i.year, i.month, i.date, i.hours, i.minutes);
        t = e.map(function (t) {
          return s.formatDate(s.loc.dateFormat, t);
        }).join(this.opts.multipleDatesSeparator), (this.opts.multipleDates || this.opts.range) && (a = e.map(function (t) {
          var e = n.getParsedDate(t);
          return new Date(e.year, e.month, e.date, e.hours, e.minutes);
        })), this._prevOnSelectValue = t, this.opts.onSelect(t, a, this);
      },
      next: function next() {
        var t = this.parsedDate,
            e = this.opts;

        switch (this.view) {
          case "days":
            this.date = new Date(t.year, t.month + 1, 1), e.onChangeMonth && e.onChangeMonth(this.parsedDate.month, this.parsedDate.year);
            break;

          case "months":
            this.date = new Date(t.year + 1, t.month, 1), e.onChangeYear && e.onChangeYear(this.parsedDate.year);
            break;

          case "years":
            this.date = new Date(t.year + 10, 0, 1), e.onChangeDecade && e.onChangeDecade(this.curDecade);
        }
      },
      prev: function prev() {
        var t = this.parsedDate,
            e = this.opts;

        switch (this.view) {
          case "days":
            this.date = new Date(t.year, t.month - 1, 1), e.onChangeMonth && e.onChangeMonth(this.parsedDate.month, this.parsedDate.year);
            break;

          case "months":
            this.date = new Date(t.year - 1, t.month, 1), e.onChangeYear && e.onChangeYear(this.parsedDate.year);
            break;

          case "years":
            this.date = new Date(t.year - 10, 0, 1), e.onChangeDecade && e.onChangeDecade(this.curDecade);
        }
      },
      formatDate: function formatDate(t, e) {
        e = e || this.date;
        var i,
            s = t,
            a = this._getWordBoundaryRegExp,
            h = this.loc,
            o = n.getLeadingZeroNum,
            r = n.getDecade(e),
            c = n.getParsedDate(e),
            d = c.fullHours,
            l = c.hours,
            u = t.match(a("aa")) || t.match(a("AA")),
            m = "am",
            p = this._replacer;

        switch (this.opts.timepicker && this.timepicker && u && (i = this.timepicker._getValidHoursFromDate(e, u), d = o(i.hours), l = i.hours, m = i.dayPeriod), !0) {
          case /@/.test(s):
            s = s.replace(/@/, e.getTime());

          case /aa/.test(s):
            s = p(s, a("aa"), m);

          case /AA/.test(s):
            s = p(s, a("AA"), m.toUpperCase());

          case /dd/.test(s):
            s = p(s, a("dd"), c.fullDate);

          case /d/.test(s):
            s = p(s, a("d"), c.date);

          case /DD/.test(s):
            s = p(s, a("DD"), h.days[c.day]);

          case /D/.test(s):
            s = p(s, a("D"), h.daysShort[c.day]);

          case /mm/.test(s):
            s = p(s, a("mm"), c.fullMonth);

          case /m/.test(s):
            s = p(s, a("m"), c.month + 1);

          case /MM/.test(s):
            s = p(s, a("MM"), this.loc.months[c.month]);

          case /M/.test(s):
            s = p(s, a("M"), h.monthsShort[c.month]);

          case /ii/.test(s):
            s = p(s, a("ii"), c.fullMinutes);

          case /i/.test(s):
            s = p(s, a("i"), c.minutes);

          case /hh/.test(s):
            s = p(s, a("hh"), d);

          case /h/.test(s):
            s = p(s, a("h"), l);

          case /yyyy/.test(s):
            s = p(s, a("yyyy"), c.year);

          case /yyyy1/.test(s):
            s = p(s, a("yyyy1"), r[0]);

          case /yyyy2/.test(s):
            s = p(s, a("yyyy2"), r[1]);

          case /yy/.test(s):
            s = p(s, a("yy"), c.year.toString().slice(-2));
        }

        return s;
      },
      _replacer: function _replacer(t, e, i) {
        return t.replace(e, function (t, e, s, a) {
          return e + i + a;
        });
      },
      _getWordBoundaryRegExp: function _getWordBoundaryRegExp(t) {
        var e = "\\s|\\.|-|/|\\\\|,|\\$|\\!|\\?|:|;";
        return new RegExp("(^|>|" + e + ")(" + t + ")($|<|" + e + ")", "g");
      },
      selectDate: function selectDate(t) {
        var e = this,
            i = e.opts,
            s = e.parsedDate,
            a = e.selectedDates,
            h = a.length,
            o = "";
        if (Array.isArray(t)) return void t.forEach(function (t) {
          e.selectDate(t);
        });

        if (t instanceof Date) {
          if (this.lastSelectedDate = t, this.timepicker && this.timepicker._setTime(t), e._trigger("selectDate", t), this.timepicker && (t.setHours(this.timepicker.hours), t.setMinutes(this.timepicker.minutes)), "days" == e.view && t.getMonth() != s.month && i.moveToOtherMonthsOnSelect && (o = new Date(t.getFullYear(), t.getMonth(), 1)), "years" == e.view && t.getFullYear() != s.year && i.moveToOtherYearsOnSelect && (o = new Date(t.getFullYear(), 0, 1)), o && (e.silent = !0, e.date = o, e.silent = !1, e.nav._render()), i.multipleDates && !i.range) {
            if (h === i.multipleDates) return;
            e._isSelected(t) || e.selectedDates.push(t);
          } else i.range ? 2 == h ? (e.selectedDates = [t], e.minRange = t, e.maxRange = "") : 1 == h ? (e.selectedDates.push(t), e.maxRange ? e.minRange = t : e.maxRange = t, n.bigger(e.maxRange, e.minRange) && (e.maxRange = e.minRange, e.minRange = t), e.selectedDates = [e.minRange, e.maxRange]) : (e.selectedDates = [t], e.minRange = t) : e.selectedDates = [t];

          e._setInputValue(), i.onSelect && e._triggerOnChange(), i.autoClose && !this.timepickerIsActive && (i.multipleDates || i.range ? i.range && 2 == e.selectedDates.length && e.hide() : e.hide()), e.views[this.currentView]._render();
        }
      },
      removeDate: function removeDate(t) {
        var e = this.selectedDates,
            i = this;
        if (t instanceof Date) return e.some(function (s, a) {
          return n.isSame(s, t) ? (e.splice(a, 1), i.selectedDates.length ? i.lastSelectedDate = i.selectedDates[i.selectedDates.length - 1] : (i.minRange = "", i.maxRange = "", i.lastSelectedDate = ""), i.views[i.currentView]._render(), i._setInputValue(), i.opts.onSelect && i._triggerOnChange(), !0) : void 0;
        });
      },
      today: function today() {
        this.silent = !0, this.view = this.opts.minView, this.silent = !1, this.date = new Date(), this.opts.todayButton instanceof Date && this.selectDate(this.opts.todayButton);
      },
      clear: function clear() {
        this.selectedDates = [], this.minRange = "", this.maxRange = "", this.views[this.currentView]._render(), this._setInputValue(), this.opts.onSelect && this._triggerOnChange();
      },
      update: function update(t, i) {
        var s = arguments.length,
            a = this.lastSelectedDate;
        return 2 == s ? this.opts[t] = i : 1 == s && "object" == _typeof(t) && (this.opts = e.extend(!0, this.opts, t)), this._createShortCuts(), this._syncWithMinMaxDates(), this._defineLocale(this.opts.language), this.nav._addButtonsIfNeed(), this.opts.onlyTimepicker || this.nav._render(), this.views[this.currentView]._render(), this.elIsInput && !this.opts.inline && (this._setPositionClasses(this.opts.position), this.visible && this.setPosition(this.opts.position)), this.opts.classes && this.$datepicker.addClass(this.opts.classes), this.opts.onlyTimepicker && this.$datepicker.addClass("-only-timepicker-"), this.opts.timepicker && (a && this.timepicker._handleDate(a), this.timepicker._updateRanges(), this.timepicker._updateCurrentTime(), a && (a.setHours(this.timepicker.hours), a.setMinutes(this.timepicker.minutes))), this._setInputValue(), this;
      },
      _syncWithMinMaxDates: function _syncWithMinMaxDates() {
        var t = this.date.getTime();
        this.silent = !0, this.minTime > t && (this.date = this.minDate), this.maxTime < t && (this.date = this.maxDate), this.silent = !1;
      },
      _isSelected: function _isSelected(t, e) {
        var i = !1;
        return this.selectedDates.some(function (s) {
          return n.isSame(s, t, e) ? (i = s, !0) : void 0;
        }), i;
      },
      _setInputValue: function _setInputValue() {
        var t,
            e = this,
            i = e.opts,
            s = e.loc.dateFormat,
            a = i.altFieldDateFormat,
            n = e.selectedDates.map(function (t) {
          return e.formatDate(s, t);
        });
        i.altField && e.$altField.length && (t = this.selectedDates.map(function (t) {
          return e.formatDate(a, t);
        }), t = t.join(this.opts.multipleDatesSeparator), this.$altField.val(t)), n = n.join(this.opts.multipleDatesSeparator), this.$el.val(n);
      },
      _isInRange: function _isInRange(t, e) {
        var i = t.getTime(),
            s = n.getParsedDate(t),
            a = n.getParsedDate(this.minDate),
            h = n.getParsedDate(this.maxDate),
            o = new Date(s.year, s.month, a.date).getTime(),
            r = new Date(s.year, s.month, h.date).getTime(),
            c = {
          day: i >= this.minTime && i <= this.maxTime,
          month: o >= this.minTime && r <= this.maxTime,
          year: s.year >= a.year && s.year <= h.year
        };
        return e ? c[e] : c.day;
      },
      _getDimensions: function _getDimensions(t) {
        var e = t.offset();
        return {
          width: t.outerWidth(),
          height: t.outerHeight(),
          left: e.left,
          top: e.top
        };
      },
      _getDateFromCell: function _getDateFromCell(t) {
        var e = this.parsedDate,
            s = t.data("year") || e.year,
            a = t.data("month") == i ? e.month : t.data("month"),
            n = t.data("date") || 1;
        return new Date(s, a, n);
      },
      _setPositionClasses: function _setPositionClasses(t) {
        t = t.split(" ");
        var e = t[0],
            i = t[1],
            s = "datepicker -" + e + "-" + i + "- -from-" + e + "-";
        this.visible && (s += " active"), this.$datepicker.removeAttr("class").addClass(s);
      },
      setPosition: function setPosition(t) {
        t = t || this.opts.position;

        var e,
            i,
            s = this._getDimensions(this.$el),
            a = this._getDimensions(this.$datepicker),
            n = t.split(" "),
            h = this.opts.offset,
            o = n[0],
            r = n[1];

        switch (o) {
          case "top":
            e = s.top - a.height - h;
            break;

          case "right":
            i = s.left + s.width + h;
            break;

          case "bottom":
            e = s.top + s.height + h;
            break;

          case "left":
            i = s.left - a.width - h;
        }

        switch (r) {
          case "top":
            e = s.top;
            break;

          case "right":
            i = s.left + s.width - a.width;
            break;

          case "bottom":
            e = s.top + s.height - a.height;
            break;

          case "left":
            i = s.left;
            break;

          case "center":
            /left|right/.test(o) ? e = s.top + s.height / 2 - a.height / 2 : i = s.left + s.width / 2 - a.width / 2;
        }

        this.$datepicker.css({
          left: i,
          top: e
        });
      },
      show: function show() {
        var t = this.opts.onShow;
        this.setPosition(this.opts.position), this.$datepicker.addClass("active"), this.visible = !0, t && this._bindVisionEvents(t);
      },
      hide: function hide() {
        var t = this.opts.onHide;
        this.$datepicker.removeClass("active").css({
          left: "-100000px"
        }), this.focused = "", this.keys = [], this.inFocus = !1, this.visible = !1, this.$el.blur(), t && this._bindVisionEvents(t);
      },
      down: function down(t) {
        this._changeView(t, "down");
      },
      up: function up(t) {
        this._changeView(t, "up");
      },
      _bindVisionEvents: function _bindVisionEvents(t) {
        this.$datepicker.off("transitionend.dp"), t(this, !1), this.$datepicker.one("transitionend.dp", t.bind(this, this, !0));
      },
      _changeView: function _changeView(t, e) {
        t = t || this.focused || this.date;
        var i = "up" == e ? this.viewIndex + 1 : this.viewIndex - 1;
        i > 2 && (i = 2), 0 > i && (i = 0), this.silent = !0, this.date = new Date(t.getFullYear(), t.getMonth(), 1), this.silent = !1, this.view = this.viewIndexes[i];
      },
      _handleHotKey: function _handleHotKey(t) {
        var e,
            i,
            s,
            a = n.getParsedDate(this._getFocusedDate()),
            h = this.opts,
            o = !1,
            r = !1,
            c = !1,
            d = a.year,
            l = a.month,
            u = a.date;

        switch (t) {
          case "ctrlRight":
          case "ctrlUp":
            l += 1, o = !0;
            break;

          case "ctrlLeft":
          case "ctrlDown":
            l -= 1, o = !0;
            break;

          case "shiftRight":
          case "shiftUp":
            r = !0, d += 1;
            break;

          case "shiftLeft":
          case "shiftDown":
            r = !0, d -= 1;
            break;

          case "altRight":
          case "altUp":
            c = !0, d += 10;
            break;

          case "altLeft":
          case "altDown":
            c = !0, d -= 10;
            break;

          case "ctrlShiftUp":
            this.up();
        }

        s = n.getDaysCount(new Date(d, l)), i = new Date(d, l, u), u > s && (u = s), i.getTime() < this.minTime ? i = this.minDate : i.getTime() > this.maxTime && (i = this.maxDate), this.focused = i, e = n.getParsedDate(i), o && h.onChangeMonth && h.onChangeMonth(e.month, e.year), r && h.onChangeYear && h.onChangeYear(e.year), c && h.onChangeDecade && h.onChangeDecade(this.curDecade);
      },
      _registerKey: function _registerKey(t) {
        var e = this.keys.some(function (e) {
          return e == t;
        });
        e || this.keys.push(t);
      },
      _unRegisterKey: function _unRegisterKey(t) {
        var e = this.keys.indexOf(t);
        this.keys.splice(e, 1);
      },
      _isHotKeyPressed: function _isHotKeyPressed() {
        var t,
            e = !1,
            i = this,
            s = this.keys.sort();

        for (var a in u) {
          t = u[a], s.length == t.length && t.every(function (t, e) {
            return t == s[e];
          }) && (i._trigger("hotKey", a), e = !0);
        }

        return e;
      },
      _trigger: function _trigger(t, e) {
        this.$el.trigger(t, e);
      },
      _focusNextCell: function _focusNextCell(t, e) {
        e = e || this.cellType;
        var i = n.getParsedDate(this._getFocusedDate()),
            s = i.year,
            a = i.month,
            h = i.date;

        if (!this._isHotKeyPressed()) {
          switch (t) {
            case 37:
              "day" == e ? h -= 1 : "", "month" == e ? a -= 1 : "", "year" == e ? s -= 1 : "";
              break;

            case 38:
              "day" == e ? h -= 7 : "", "month" == e ? a -= 3 : "", "year" == e ? s -= 4 : "";
              break;

            case 39:
              "day" == e ? h += 1 : "", "month" == e ? a += 1 : "", "year" == e ? s += 1 : "";
              break;

            case 40:
              "day" == e ? h += 7 : "", "month" == e ? a += 3 : "", "year" == e ? s += 4 : "";
          }

          var o = new Date(s, a, h);
          o.getTime() < this.minTime ? o = this.minDate : o.getTime() > this.maxTime && (o = this.maxDate), this.focused = o;
        }
      },
      _getFocusedDate: function _getFocusedDate() {
        var t = this.focused || this.selectedDates[this.selectedDates.length - 1],
            e = this.parsedDate;
        if (!t) switch (this.view) {
          case "days":
            t = new Date(e.year, e.month, new Date().getDate());
            break;

          case "months":
            t = new Date(e.year, e.month, 1);
            break;

          case "years":
            t = new Date(e.year, 0, 1);
        }
        return t;
      },
      _getCell: function _getCell(t, i) {
        i = i || this.cellType;
        var s,
            a = n.getParsedDate(t),
            h = '.datepicker--cell[data-year="' + a.year + '"]';

        switch (i) {
          case "month":
            h = '[data-month="' + a.month + '"]';
            break;

          case "day":
            h += '[data-month="' + a.month + '"][data-date="' + a.date + '"]';
        }

        return s = this.views[this.currentView].$el.find(h), s.length ? s : e("");
      },
      destroy: function destroy() {
        var t = this;
        t.$el.off(".adp").data("datepicker", ""), t.selectedDates = [], t.focused = "", t.views = {}, t.keys = [], t.minRange = "", t.maxRange = "", t.opts.inline || !t.elIsInput ? t.$datepicker.closest(".datepicker-inline").remove() : t.$datepicker.remove();
      },
      _handleAlreadySelectedDates: function _handleAlreadySelectedDates(t, e) {
        this.opts.range ? this.opts.toggleSelected ? this.removeDate(e) : 2 != this.selectedDates.length && this._trigger("clickCell", e) : this.opts.toggleSelected && this.removeDate(e), this.opts.toggleSelected || (this.lastSelectedDate = t, this.opts.timepicker && (this.timepicker._setTime(t), this.timepicker.update()));
      },
      _onShowEvent: function _onShowEvent(t) {
        this.visible || this.show();
      },
      _onBlur: function _onBlur() {
        !this.inFocus && this.visible && this.hide();
      },
      _onMouseDownDatepicker: function _onMouseDownDatepicker(t) {
        this.inFocus = !0;
      },
      _onMouseUpDatepicker: function _onMouseUpDatepicker(t) {
        this.inFocus = !1, t.originalEvent.inFocus = !0, t.originalEvent.timepickerFocus || this.$el.focus();
      },
      _onKeyUpGeneral: function _onKeyUpGeneral(t) {
        var e = this.$el.val();
        e || this.clear();
      },
      _onResize: function _onResize() {
        this.visible && this.setPosition();
      },
      _onMouseUpBody: function _onMouseUpBody(t) {
        t.originalEvent.inFocus || this.visible && !this.inFocus && this.hide();
      },
      _onMouseUpEl: function _onMouseUpEl(t) {
        t.originalEvent.inFocus = !0, setTimeout(this._onKeyUpGeneral.bind(this), 4);
      },
      _onKeyDown: function _onKeyDown(t) {
        var e = t.which;

        if (this._registerKey(e), e >= 37 && 40 >= e && (t.preventDefault(), this._focusNextCell(e)), 13 == e && this.focused) {
          if (this._getCell(this.focused).hasClass("-disabled-")) return;
          if (this.view != this.opts.minView) this.down();else {
            var i = this._isSelected(this.focused, this.cellType);

            if (!i) return this.timepicker && (this.focused.setHours(this.timepicker.hours), this.focused.setMinutes(this.timepicker.minutes)), void this.selectDate(this.focused);

            this._handleAlreadySelectedDates(i, this.focused);
          }
        }

        27 == e && this.hide();
      },
      _onKeyUp: function _onKeyUp(t) {
        var e = t.which;

        this._unRegisterKey(e);
      },
      _onHotKey: function _onHotKey(t, e) {
        this._handleHotKey(e);
      },
      _onMouseEnterCell: function _onMouseEnterCell(t) {
        var i = e(t.target).closest(".datepicker--cell"),
            s = this._getDateFromCell(i);

        this.silent = !0, this.focused && (this.focused = ""), i.addClass("-focus-"), this.focused = s, this.silent = !1, this.opts.range && 1 == this.selectedDates.length && (this.minRange = this.selectedDates[0], this.maxRange = "", n.less(this.minRange, this.focused) && (this.maxRange = this.minRange, this.minRange = ""), this.views[this.currentView]._update());
      },
      _onMouseLeaveCell: function _onMouseLeaveCell(t) {
        var i = e(t.target).closest(".datepicker--cell");
        i.removeClass("-focus-"), this.silent = !0, this.focused = "", this.silent = !1;
      },
      _onTimeChange: function _onTimeChange(t, e, i) {
        var s = new Date(),
            a = this.selectedDates,
            n = !1;
        a.length && (n = !0, s = this.lastSelectedDate), s.setHours(e), s.setMinutes(i), n || this._getCell(s).hasClass("-disabled-") ? (this._setInputValue(), this.opts.onSelect && this._triggerOnChange()) : this.selectDate(s);
      },
      _onClickCell: function _onClickCell(t, e) {
        this.timepicker && (e.setHours(this.timepicker.hours), e.setMinutes(this.timepicker.minutes)), this.selectDate(e);
      },

      set focused(t) {
        if (!t && this.focused) {
          var e = this._getCell(this.focused);

          e.length && e.removeClass("-focus-");
        }

        this._focused = t, this.opts.range && 1 == this.selectedDates.length && (this.minRange = this.selectedDates[0], this.maxRange = "", n.less(this.minRange, this._focused) && (this.maxRange = this.minRange, this.minRange = "")), this.silent || (this.date = t);
      },

      get focused() {
        return this._focused;
      },

      get parsedDate() {
        return n.getParsedDate(this.date);
      },

      set date(t) {
        return t instanceof Date ? (this.currentDate = t, this.inited && !this.silent && (this.views[this.view]._render(), this.nav._render(), this.visible && this.elIsInput && this.setPosition()), t) : void 0;
      },

      get date() {
        return this.currentDate;
      },

      set view(t) {
        return this.viewIndex = this.viewIndexes.indexOf(t), this.viewIndex < 0 ? void 0 : (this.prevView = this.currentView, this.currentView = t, this.inited && (this.views[t] ? this.views[t]._render() : this.views[t] = new e.fn.datepicker.Body(this, t, this.opts), this.views[this.prevView].hide(), this.views[t].show(), this.nav._render(), this.opts.onChangeView && this.opts.onChangeView(t), this.elIsInput && this.visible && this.setPosition()), t);
      },

      get view() {
        return this.currentView;
      },

      get cellType() {
        return this.view.substring(0, this.view.length - 1);
      },

      get minTime() {
        var t = n.getParsedDate(this.minDate);
        return new Date(t.year, t.month, t.date).getTime();
      },

      get maxTime() {
        var t = n.getParsedDate(this.maxDate);
        return new Date(t.year, t.month, t.date).getTime();
      },

      get curDecade() {
        return n.getDecade(this.date);
      }

    }, n.getDaysCount = function (t) {
      return new Date(t.getFullYear(), t.getMonth() + 1, 0).getDate();
    }, n.getParsedDate = function (t) {
      return {
        year: t.getFullYear(),
        month: t.getMonth(),
        fullMonth: t.getMonth() + 1 < 10 ? "0" + (t.getMonth() + 1) : t.getMonth() + 1,
        date: t.getDate(),
        fullDate: t.getDate() < 10 ? "0" + t.getDate() : t.getDate(),
        day: t.getDay(),
        hours: t.getHours(),
        fullHours: t.getHours() < 10 ? "0" + t.getHours() : t.getHours(),
        minutes: t.getMinutes(),
        fullMinutes: t.getMinutes() < 10 ? "0" + t.getMinutes() : t.getMinutes()
      };
    }, n.getDecade = function (t) {
      var e = 10 * Math.floor(t.getFullYear() / 10);
      return [e, e + 9];
    }, n.template = function (t, e) {
      return t.replace(/#\{([\w]+)\}/g, function (t, i) {
        return e[i] || 0 === e[i] ? e[i] : void 0;
      });
    }, n.isSame = function (t, e, i) {
      if (!t || !e) return !1;
      var s = n.getParsedDate(t),
          a = n.getParsedDate(e),
          h = i ? i : "day",
          o = {
        day: s.date == a.date && s.month == a.month && s.year == a.year,
        month: s.month == a.month && s.year == a.year,
        year: s.year == a.year
      };
      return o[h];
    }, n.less = function (t, e, i) {
      return t && e ? e.getTime() < t.getTime() : !1;
    }, n.bigger = function (t, e, i) {
      return t && e ? e.getTime() > t.getTime() : !1;
    }, n.getLeadingZeroNum = function (t) {
      return parseInt(t) < 10 ? "0" + t : t;
    }, n.resetTime = function (t) {
      return "object" == _typeof(t) ? (t = n.getParsedDate(t), new Date(t.year, t.month, t.date)) : void 0;
    }, e.fn.datepicker = function (t) {
      return this.each(function () {
        if (e.data(this, o)) {
          var i = e.data(this, o);
          i.opts = e.extend(!0, i.opts, t), i.update();
        } else e.data(this, o, new m(this, t));
      });
    }, e.fn.datepicker.Constructor = m, e.fn.datepicker.language = {
      ru: {
        days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
        daysShort: ["Вос", "Пон", "Вто", "Сре", "Чет", "Пят", "Суб"],
        daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
        today: "Сегодня",
        clear: "Очистить",
        dateFormat: "dd.mm.yyyy",
        timeFormat: "hh:ii",
        firstDay: 1
      }
    }, e(function () {
      e(r).datepicker();
    });
  }(), function () {
    var t = {
      days: '<div class="datepicker--days datepicker--body"><div class="datepicker--days-names"></div><div class="datepicker--cells datepicker--cells-days"></div></div>',
      months: '<div class="datepicker--months datepicker--body"><div class="datepicker--cells datepicker--cells-months"></div></div>',
      years: '<div class="datepicker--years datepicker--body"><div class="datepicker--cells datepicker--cells-years"></div></div>'
    },
        s = e.fn.datepicker,
        a = s.Constructor;
    s.Body = function (t, i, s) {
      this.d = t, this.type = i, this.opts = s, this.$el = e(""), this.opts.onlyTimepicker || this.init();
    }, s.Body.prototype = {
      init: function init() {
        this._buildBaseHtml(), this._render(), this._bindEvents();
      },
      _bindEvents: function _bindEvents() {
        this.$el.on("click", ".datepicker--cell", e.proxy(this._onClickCell, this));
      },
      _buildBaseHtml: function _buildBaseHtml() {
        this.$el = e(t[this.type]).appendTo(this.d.$content), this.$names = e(".datepicker--days-names", this.$el), this.$cells = e(".datepicker--cells", this.$el);
      },
      _getDayNamesHtml: function _getDayNamesHtml(t, e, s, a) {
        return e = e != i ? e : t, s = s ? s : "", a = a != i ? a : 0, a > 7 ? s : 7 == e ? this._getDayNamesHtml(t, 0, s, ++a) : (s += '<div class="datepicker--day-name' + (this.d.isWeekend(e) ? " -weekend-" : "") + '">' + this.d.loc.daysMin[e] + "</div>", this._getDayNamesHtml(t, ++e, s, ++a));
      },
      _getCellContents: function _getCellContents(t, e) {
        var i = "datepicker--cell datepicker--cell-" + e,
            s = new Date(),
            n = this.d,
            h = a.resetTime(n.minRange),
            o = a.resetTime(n.maxRange),
            r = n.opts,
            c = a.getParsedDate(t),
            d = {},
            l = c.date;

        switch (e) {
          case "day":
            n.isWeekend(c.day) && (i += " -weekend-"), c.month != this.d.parsedDate.month && (i += " -other-month-", r.selectOtherMonths || (i += " -disabled-"), r.showOtherMonths || (l = ""));
            break;

          case "month":
            l = n.loc[n.opts.monthsField][c.month];
            break;

          case "year":
            var u = n.curDecade;
            l = c.year, (c.year < u[0] || c.year > u[1]) && (i += " -other-decade-", r.selectOtherYears || (i += " -disabled-"), r.showOtherYears || (l = ""));
        }

        return r.onRenderCell && (d = r.onRenderCell(t, e) || {}, l = d.html ? d.html : l, i += d.classes ? " " + d.classes : ""), r.range && (a.isSame(h, t, e) && (i += " -range-from-"), a.isSame(o, t, e) && (i += " -range-to-"), 1 == n.selectedDates.length && n.focused ? ((a.bigger(h, t) && a.less(n.focused, t) || a.less(o, t) && a.bigger(n.focused, t)) && (i += " -in-range-"), a.less(o, t) && a.isSame(n.focused, t) && (i += " -range-from-"), a.bigger(h, t) && a.isSame(n.focused, t) && (i += " -range-to-")) : 2 == n.selectedDates.length && a.bigger(h, t) && a.less(o, t) && (i += " -in-range-")), a.isSame(s, t, e) && (i += " -current-"), n.focused && a.isSame(t, n.focused, e) && (i += " -focus-"), n._isSelected(t, e) && (i += " -selected-"), (!n._isInRange(t, e) || d.disabled) && (i += " -disabled-"), {
          html: l,
          classes: i
        };
      },
      _getDaysHtml: function _getDaysHtml(t) {
        var e = a.getDaysCount(t),
            i = new Date(t.getFullYear(), t.getMonth(), 1).getDay(),
            s = new Date(t.getFullYear(), t.getMonth(), e).getDay(),
            n = i - this.d.loc.firstDay,
            h = 6 - s + this.d.loc.firstDay;
        n = 0 > n ? n + 7 : n, h = h > 6 ? h - 7 : h;

        for (var o, r, c = -n + 1, d = "", l = c, u = e + h; u >= l; l++) {
          r = t.getFullYear(), o = t.getMonth(), d += this._getDayHtml(new Date(r, o, l));
        }

        return d;
      },
      _getDayHtml: function _getDayHtml(t) {
        var e = this._getCellContents(t, "day");

        return '<div class="' + e.classes + '" data-date="' + t.getDate() + '" data-month="' + t.getMonth() + '" data-year="' + t.getFullYear() + '">' + e.html + "</div>";
      },
      _getMonthsHtml: function _getMonthsHtml(t) {
        for (var e = "", i = a.getParsedDate(t), s = 0; 12 > s;) {
          e += this._getMonthHtml(new Date(i.year, s)), s++;
        }

        return e;
      },
      _getMonthHtml: function _getMonthHtml(t) {
        var e = this._getCellContents(t, "month");

        return '<div class="' + e.classes + '" data-month="' + t.getMonth() + '">' + e.html + "</div>";
      },
      _getYearsHtml: function _getYearsHtml(t) {
        var e = (a.getParsedDate(t), a.getDecade(t)),
            i = e[0] - 1,
            s = "",
            n = i;

        for (n; n <= e[1] + 1; n++) {
          s += this._getYearHtml(new Date(n, 0));
        }

        return s;
      },
      _getYearHtml: function _getYearHtml(t) {
        var e = this._getCellContents(t, "year");

        return '<div class="' + e.classes + '" data-year="' + t.getFullYear() + '">' + e.html + "</div>";
      },
      _renderTypes: {
        days: function days() {
          var t = this._getDayNamesHtml(this.d.loc.firstDay),
              e = this._getDaysHtml(this.d.currentDate);

          this.$cells.html(e), this.$names.html(t);
        },
        months: function months() {
          var t = this._getMonthsHtml(this.d.currentDate);

          this.$cells.html(t);
        },
        years: function years() {
          var t = this._getYearsHtml(this.d.currentDate);

          this.$cells.html(t);
        }
      },
      _render: function _render() {
        this.opts.onlyTimepicker || this._renderTypes[this.type].bind(this)();
      },
      _update: function _update() {
        var t,
            i,
            s,
            a = e(".datepicker--cell", this.$cells),
            n = this;
        a.each(function (a, h) {
          i = e(this), s = n.d._getDateFromCell(e(this)), t = n._getCellContents(s, n.d.cellType), i.attr("class", t.classes);
        });
      },
      show: function show() {
        this.opts.onlyTimepicker || (this.$el.addClass("active"), this.acitve = !0);
      },
      hide: function hide() {
        this.$el.removeClass("active"), this.active = !1;
      },
      _handleClick: function _handleClick(t) {
        var e = t.data("date") || 1,
            i = t.data("month") || 0,
            s = t.data("year") || this.d.parsedDate.year,
            a = this.d;
        if (a.view != this.opts.minView) return void a.down(new Date(s, i, e));

        var n = new Date(s, i, e),
            h = this.d._isSelected(n, this.d.cellType);

        return h ? void a._handleAlreadySelectedDates.bind(a, h, n)() : void a._trigger("clickCell", n);
      },
      _onClickCell: function _onClickCell(t) {
        var i = e(t.target).closest(".datepicker--cell");
        i.hasClass("-disabled-") || this._handleClick.bind(this)(i);
      }
    };
  }(), function () {
    var t = '<div class="datepicker--nav-action" data-action="prev">#{prevHtml}</div><div class="datepicker--nav-title">#{title}</div><div class="datepicker--nav-action" data-action="next">#{nextHtml}</div>',
        i = '<div class="datepicker--buttons"></div>',
        s = '<span class="datepicker--button" data-action="#{action}">#{label}</span>',
        a = e.fn.datepicker,
        n = a.Constructor;
    a.Navigation = function (t, e) {
      this.d = t, this.opts = e, this.$buttonsContainer = "", this.init();
    }, a.Navigation.prototype = {
      init: function init() {
        this._buildBaseHtml(), this._bindEvents();
      },
      _bindEvents: function _bindEvents() {
        this.d.$nav.on("click", ".datepicker--nav-action", e.proxy(this._onClickNavButton, this)), this.d.$nav.on("click", ".datepicker--nav-title", e.proxy(this._onClickNavTitle, this)), this.d.$datepicker.on("click", ".datepicker--button", e.proxy(this._onClickNavButton, this));
      },
      _buildBaseHtml: function _buildBaseHtml() {
        this.opts.onlyTimepicker || this._render(), this._addButtonsIfNeed();
      },
      _addButtonsIfNeed: function _addButtonsIfNeed() {
        this.opts.todayButton && this._addButton("today"), this.opts.clearButton && this._addButton("clear");
      },
      _render: function _render() {
        var i = this._getTitle(this.d.currentDate),
            s = n.template(t, e.extend({
          title: i
        }, this.opts));

        this.d.$nav.html(s), "years" == this.d.view && e(".datepicker--nav-title", this.d.$nav).addClass("-disabled-"), this.setNavStatus();
      },
      _getTitle: function _getTitle(t) {
        return this.d.formatDate(this.opts.navTitles[this.d.view], t);
      },
      _addButton: function _addButton(t) {
        this.$buttonsContainer.length || this._addButtonsContainer();
        var i = {
          action: t,
          label: this.d.loc[t]
        },
            a = n.template(s, i);
        e("[data-action=" + t + "]", this.$buttonsContainer).length || this.$buttonsContainer.append(a);
      },
      _addButtonsContainer: function _addButtonsContainer() {
        this.d.$datepicker.append(i), this.$buttonsContainer = e(".datepicker--buttons", this.d.$datepicker);
      },
      setNavStatus: function setNavStatus() {
        if ((this.opts.minDate || this.opts.maxDate) && this.opts.disableNavWhenOutOfRange) {
          var t = this.d.parsedDate,
              e = t.month,
              i = t.year,
              s = t.date;

          switch (this.d.view) {
            case "days":
              this.d._isInRange(new Date(i, e - 1, 1), "month") || this._disableNav("prev"), this.d._isInRange(new Date(i, e + 1, 1), "month") || this._disableNav("next");
              break;

            case "months":
              this.d._isInRange(new Date(i - 1, e, s), "year") || this._disableNav("prev"), this.d._isInRange(new Date(i + 1, e, s), "year") || this._disableNav("next");
              break;

            case "years":
              var a = n.getDecade(this.d.date);
              this.d._isInRange(new Date(a[0] - 1, 0, 1), "year") || this._disableNav("prev"), this.d._isInRange(new Date(a[1] + 1, 0, 1), "year") || this._disableNav("next");
          }
        }
      },
      _disableNav: function _disableNav(t) {
        e('[data-action="' + t + '"]', this.d.$nav).addClass("-disabled-");
      },
      _activateNav: function _activateNav(t) {
        e('[data-action="' + t + '"]', this.d.$nav).removeClass("-disabled-");
      },
      _onClickNavButton: function _onClickNavButton(t) {
        var i = e(t.target).closest("[data-action]"),
            s = i.data("action");
        this.d[s]();
      },
      _onClickNavTitle: function _onClickNavTitle(t) {
        return e(t.target).hasClass("-disabled-") ? void 0 : "days" == this.d.view ? this.d.view = "months" : void (this.d.view = "years");
      }
    };
  }(), function () {
    var t = '<div class="datepicker--time"><div class="datepicker--time-current">   <span class="datepicker--time-current-hours">#{hourVisible}</span>   <span class="datepicker--time-current-colon">:</span>   <span class="datepicker--time-current-minutes">#{minValue}</span></div><div class="datepicker--time-sliders">   <div class="datepicker--time-row">      <input type="range" name="hours" value="#{hourValue}" min="#{hourMin}" max="#{hourMax}" step="#{hourStep}"/>   </div>   <div class="datepicker--time-row">      <input type="range" name="minutes" value="#{minValue}" min="#{minMin}" max="#{minMax}" step="#{minStep}"/>   </div></div></div>',
        i = e.fn.datepicker,
        s = i.Constructor;
    i.Timepicker = function (t, e) {
      this.d = t, this.opts = e, this.init();
    }, i.Timepicker.prototype = {
      init: function init() {
        var t = "input";
        this._setTime(this.d.date), this._buildHTML(), navigator.userAgent.match(/trident/gi) && (t = "change"), this.d.$el.on("selectDate", this._onSelectDate.bind(this)), this.$ranges.on(t, this._onChangeRange.bind(this)), this.$ranges.on("mouseup", this._onMouseUpRange.bind(this)), this.$ranges.on("mousemove focus ", this._onMouseEnterRange.bind(this)), this.$ranges.on("mouseout blur", this._onMouseOutRange.bind(this));
      },
      _setTime: function _setTime(t) {
        var e = s.getParsedDate(t);
        this._handleDate(t), this.hours = e.hours < this.minHours ? this.minHours : e.hours, this.minutes = e.minutes < this.minMinutes ? this.minMinutes : e.minutes;
      },
      _setMinTimeFromDate: function _setMinTimeFromDate(t) {
        this.minHours = t.getHours(), this.minMinutes = t.getMinutes(), this.d.lastSelectedDate && this.d.lastSelectedDate.getHours() > t.getHours() && (this.minMinutes = this.opts.minMinutes);
      },
      _setMaxTimeFromDate: function _setMaxTimeFromDate(t) {
        this.maxHours = t.getHours(), this.maxMinutes = t.getMinutes(), this.d.lastSelectedDate && this.d.lastSelectedDate.getHours() < t.getHours() && (this.maxMinutes = this.opts.maxMinutes);
      },
      _setDefaultMinMaxTime: function _setDefaultMinMaxTime() {
        var t = 23,
            e = 59,
            i = this.opts;
        this.minHours = i.minHours < 0 || i.minHours > t ? 0 : i.minHours, this.minMinutes = i.minMinutes < 0 || i.minMinutes > e ? 0 : i.minMinutes, this.maxHours = i.maxHours < 0 || i.maxHours > t ? t : i.maxHours, this.maxMinutes = i.maxMinutes < 0 || i.maxMinutes > e ? e : i.maxMinutes;
      },
      _validateHoursMinutes: function _validateHoursMinutes(t) {
        this.hours < this.minHours ? this.hours = this.minHours : this.hours > this.maxHours && (this.hours = this.maxHours), this.minutes < this.minMinutes ? this.minutes = this.minMinutes : this.minutes > this.maxMinutes && (this.minutes = this.maxMinutes);
      },
      _buildHTML: function _buildHTML() {
        var i = s.getLeadingZeroNum,
            a = {
          hourMin: this.minHours,
          hourMax: i(this.maxHours),
          hourStep: this.opts.hoursStep,
          hourValue: this.hours,
          hourVisible: i(this.displayHours),
          minMin: this.minMinutes,
          minMax: i(this.maxMinutes),
          minStep: this.opts.minutesStep,
          minValue: i(this.minutes)
        },
            n = s.template(t, a);
        this.$timepicker = e(n).appendTo(this.d.$datepicker), this.$ranges = e('[type="range"]', this.$timepicker), this.$hours = e('[name="hours"]', this.$timepicker), this.$minutes = e('[name="minutes"]', this.$timepicker), this.$hoursText = e(".datepicker--time-current-hours", this.$timepicker), this.$minutesText = e(".datepicker--time-current-minutes", this.$timepicker), this.d.ampm && (this.$ampm = e('<span class="datepicker--time-current-ampm">').appendTo(e(".datepicker--time-current", this.$timepicker)).html(this.dayPeriod), this.$timepicker.addClass("-am-pm-"));
      },
      _updateCurrentTime: function _updateCurrentTime() {
        var t = s.getLeadingZeroNum(this.displayHours),
            e = s.getLeadingZeroNum(this.minutes);
        this.$hoursText.html(t), this.$minutesText.html(e), this.d.ampm && this.$ampm.html(this.dayPeriod);
      },
      _updateRanges: function _updateRanges() {
        this.$hours.attr({
          min: this.minHours,
          max: this.maxHours
        }).val(this.hours), this.$minutes.attr({
          min: this.minMinutes,
          max: this.maxMinutes
        }).val(this.minutes);
      },
      _handleDate: function _handleDate(t) {
        this._setDefaultMinMaxTime(), t && (s.isSame(t, this.d.opts.minDate) ? this._setMinTimeFromDate(this.d.opts.minDate) : s.isSame(t, this.d.opts.maxDate) && this._setMaxTimeFromDate(this.d.opts.maxDate)), this._validateHoursMinutes(t);
      },
      update: function update() {
        this._updateRanges(), this._updateCurrentTime();
      },
      _getValidHoursFromDate: function _getValidHoursFromDate(t, e) {
        var i = t,
            a = t;
        t instanceof Date && (i = s.getParsedDate(t), a = i.hours);
        var n = e || this.d.ampm,
            h = "am";
        if (n) switch (!0) {
          case 0 == a:
            a = 12;
            break;

          case 12 == a:
            h = "pm";
            break;

          case a > 11:
            a -= 12, h = "pm";
        }
        return {
          hours: a,
          dayPeriod: h
        };
      },

      set hours(t) {
        this._hours = t;

        var e = this._getValidHoursFromDate(t);

        this.displayHours = e.hours, this.dayPeriod = e.dayPeriod;
      },

      get hours() {
        return this._hours;
      },

      _onChangeRange: function _onChangeRange(t) {
        var i = e(t.target),
            s = i.attr("name");
        this.d.timepickerIsActive = !0, this[s] = i.val(), this._updateCurrentTime(), this.d._trigger("timeChange", [this.hours, this.minutes]), this._handleDate(this.d.lastSelectedDate), this.update();
      },
      _onSelectDate: function _onSelectDate(t, e) {
        this._handleDate(e), this.update();
      },
      _onMouseEnterRange: function _onMouseEnterRange(t) {
        var i = e(t.target).attr("name");
        e(".datepicker--time-current-" + i, this.$timepicker).addClass("-focus-");
      },
      _onMouseOutRange: function _onMouseOutRange(t) {
        var i = e(t.target).attr("name");
        this.d.inFocus || e(".datepicker--time-current-" + i, this.$timepicker).removeClass("-focus-");
      },
      _onMouseUpRange: function _onMouseUpRange(t) {
        this.d.timepickerIsActive = !1;
      }
    };
  }();
}(window, jQuery);
"use strict";

/*
 jQuery animateNumber plugin v0.0.14
 (c) 2013, Alexandr Borisov.
 https://github.com/aishek/jquery-animateNumber
*/
(function (d) {
  var r = function r(b) {
    return b.split("").reverse().join("");
  },
      m = {
    numberStep: function numberStep(b, a) {
      var e = Math.floor(b);
      d(a.elem).text(e);
    }
  },
      g = function g(b) {
    var a = b.elem;
    a.nodeType && a.parentNode && (a = a._animateNumberSetter, a || (a = m.numberStep), a(b.now, b));
  };

  d.Tween && d.Tween.propHooks ? d.Tween.propHooks.number = {
    set: g
  } : d.fx.step.number = g;
  d.animateNumber = {
    numberStepFactories: {
      append: function append(b) {
        return function (a, e) {
          var f = Math.floor(a);
          d(e.elem).prop("number", a).text(f + b);
        };
      },
      separator: function separator(b, a, e) {
        b = b || " ";
        a = a || 3;
        e = e || "";
        return function (f, k) {
          var u = 0 > f,
              c = Math.floor((u ? -1 : 1) * f).toString(),
              n = d(k.elem);

          if (c.length > a) {
            for (var h = c, l = a, m = h.split("").reverse(), c = [], p, s, q, t = 0, g = Math.ceil(h.length / l); t < g; t++) {
              p = "";

              for (q = 0; q < l; q++) {
                s = t * l + q;
                if (s === h.length) break;
                p += m[s];
              }

              c.push(p);
            }

            h = c.length - 1;
            l = r(c[h]);
            c[h] = r(parseInt(l, 10).toString());
            c = c.join(b);
            c = r(c);
          }

          n.prop("number", f).text((u ? "-" : "") + c + e);
        };
      }
    }
  };

  d.fn.animateNumber = function () {
    for (var b = arguments[0], a = d.extend({}, m, b), e = d(this), f = [a], k = 1, g = arguments.length; k < g; k++) {
      f.push(arguments[k]);
    }

    if (b.numberStep) {
      var c = this.each(function () {
        this._animateNumberSetter = b.numberStep;
      }),
          n = a.complete;

      a.complete = function () {
        c.each(function () {
          delete this._animateNumberSetter;
        });
        n && n.apply(this, arguments);
      };
    }

    return e.animate.apply(e, f);
  };
})(jQuery);
"use strict";

// ==================================================
// fancyBox v3.5.7
//
// Licensed GPLv3 for open source use
// or fancyBox Commercial License for commercial use
//
// http://fancyapps.com/fancybox/
// Copyright 2019 fancyApps
//
// ==================================================
!function (t, e, n, o) {
  "use strict";

  function i(t, e) {
    var o,
        i,
        a,
        s = [],
        r = 0;
    t && t.isDefaultPrevented() || (t.preventDefault(), e = e || {}, t && t.data && (e = h(t.data.options, e)), o = e.$target || n(t.currentTarget).trigger("blur"), (a = n.fancybox.getInstance()) && a.$trigger && a.$trigger.is(o) || (e.selector ? s = n(e.selector) : (i = o.attr("data-fancybox") || "", i ? (s = t.data ? t.data.items : [], s = s.length ? s.filter('[data-fancybox="' + i + '"]') : n('[data-fancybox="' + i + '"]')) : s = [o]), r = n(s).index(o), r < 0 && (r = 0), a = n.fancybox.open(s, e, r), a.$trigger = o));
  }

  if (t.console = t.console || {
    info: function info(t) {}
  }, n) {
    if (n.fn.fancybox) return void console.info("fancyBox already initialized");

    var a = {
      closeExisting: !1,
      loop: !1,
      gutter: 50,
      keyboard: !0,
      preventCaptionOverlap: !0,
      arrows: !0,
      infobar: !0,
      smallBtn: "auto",
      toolbar: "auto",
      buttons: ["zoom", "slideShow", "thumbs", "close"],
      idleTime: 3,
      protect: !1,
      modal: !1,
      image: {
        preload: !1
      },
      ajax: {
        settings: {
          data: {
            fancybox: !0
          }
        }
      },
      iframe: {
        tpl: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" allowfullscreen="allowfullscreen" allow="autoplay; fullscreen" src=""></iframe>',
        preload: !0,
        css: {},
        attr: {
          scrolling: "auto"
        }
      },
      video: {
        tpl: '<video class="fancybox-video" controls controlsList="nodownload" poster="{{poster}}"><source src="{{src}}" type="{{format}}" />Sorry, your browser doesn\'t support embedded videos, <a href="{{src}}">download</a> and watch with your favorite video player!</video>',
        format: "",
        autoStart: !0
      },
      defaultType: "image",
      animationEffect: "zoom",
      animationDuration: 366,
      zoomOpacity: "auto",
      transitionEffect: "fade",
      transitionDuration: 366,
      slideClass: "",
      baseClass: "",
      baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1"><div class="fancybox-bg"></div><div class="fancybox-inner"><div class="fancybox-infobar"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div><div class="fancybox-toolbar">{{buttons}}</div><div class="fancybox-navigation">{{arrows}}</div><div class="fancybox-stage"></div><div class="fancybox-caption"><div class="fancybox-caption__body"></div></div></div></div>',
      spinnerTpl: '<div class="fancybox-loading"></div>',
      errorTpl: '<div class="fancybox-error"><p>{{ERROR}}</p></div>',
      btnTpl: {
        download: '<a download data-fancybox-download class="fancybox-button fancybox-button--download" title="{{DOWNLOAD}}" href="javascript:;"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.62 17.09V19H5.38v-1.91zm-2.97-6.96L17 11.45l-5 4.87-5-4.87 1.36-1.32 2.68 2.64V5h1.92v7.77z"/></svg></a>',
        zoom: '<button data-fancybox-zoom class="fancybox-button fancybox-button--zoom" title="{{ZOOM}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.7 17.3l-3-3a5.9 5.9 0 0 0-.6-7.6 5.9 5.9 0 0 0-8.4 0 5.9 5.9 0 0 0 0 8.4 5.9 5.9 0 0 0 7.7.7l3 3a1 1 0 0 0 1.3 0c.4-.5.4-1 0-1.5zM8.1 13.8a4 4 0 0 1 0-5.7 4 4 0 0 1 5.7 0 4 4 0 0 1 0 5.7 4 4 0 0 1-5.7 0z"/></svg></button>',
        close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12 10.6L6.6 5.2 5.2 6.6l5.4 5.4-5.4 5.4 1.4 1.4 5.4-5.4 5.4 5.4 1.4-1.4-5.4-5.4 5.4-5.4-1.4-1.4-5.4 5.4z"/></svg></button>',
        arrowLeft: '<button data-fancybox-prev class="fancybox-button fancybox-button--arrow_left" title="{{PREV}}"><div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M11.28 15.7l-1.34 1.37L5 12l4.94-5.07 1.34 1.38-2.68 2.72H19v1.94H8.6z"/></svg></div></button>',
        arrowRight: '<button data-fancybox-next class="fancybox-button fancybox-button--arrow_right" title="{{NEXT}}"><div><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M15.4 12.97l-2.68 2.72 1.34 1.38L19 12l-4.94-5.07-1.34 1.38 2.68 2.72H5v1.94z"/></svg></div></button>',
        smallBtn: '<button type="button" data-fancybox-close class="fancybox-button fancybox-close-small" title="{{CLOSE}}"><svg xmlns="http://www.w3.org/2000/svg" version="1" viewBox="0 0 24 24"><path d="M13 12l5-5-1-1-5 5-5-5-1 1 5 5-5 5 1 1 5-5 5 5 1-1z"/></svg></button>'
      },
      parentEl: "body",
      hideScrollbar: !0,
      autoFocus: !0,
      backFocus: !0,
      trapFocus: !0,
      fullScreen: {
        autoStart: !1
      },
      touch: {
        vertical: !0,
        momentum: !0
      },
      hash: null,
      media: {},
      slideShow: {
        autoStart: !1,
        speed: 3e3
      },
      thumbs: {
        autoStart: !1,
        hideOnClose: !0,
        parentEl: ".fancybox-container",
        axis: "y"
      },
      wheel: "auto",
      onInit: n.noop,
      beforeLoad: n.noop,
      afterLoad: n.noop,
      beforeShow: n.noop,
      afterShow: n.noop,
      beforeClose: n.noop,
      afterClose: n.noop,
      onActivate: n.noop,
      onDeactivate: n.noop,
      clickContent: function clickContent(t, e) {
        return "image" === t.type && "zoom";
      },
      clickSlide: "close",
      clickOutside: "close",
      dblclickContent: !1,
      dblclickSlide: !1,
      dblclickOutside: !1,
      mobile: {
        preventCaptionOverlap: !1,
        idleTime: !1,
        clickContent: function clickContent(t, e) {
          return "image" === t.type && "toggleControls";
        },
        clickSlide: function clickSlide(t, e) {
          return "image" === t.type ? "toggleControls" : "close";
        },
        dblclickContent: function dblclickContent(t, e) {
          return "image" === t.type && "zoom";
        },
        dblclickSlide: function dblclickSlide(t, e) {
          return "image" === t.type && "zoom";
        }
      },
      lang: "en",
      i18n: {
        en: {
          CLOSE: "Close",
          NEXT: "Next",
          PREV: "Previous",
          ERROR: "The requested content cannot be loaded. <br/> Please try again later.",
          PLAY_START: "Start slideshow",
          PLAY_STOP: "Pause slideshow",
          FULL_SCREEN: "Full screen",
          THUMBS: "Thumbnails",
          DOWNLOAD: "Download",
          SHARE: "Share",
          ZOOM: "Zoom"
        },
        de: {
          CLOSE: "Schlie&szlig;en",
          NEXT: "Weiter",
          PREV: "Zur&uuml;ck",
          ERROR: "Die angeforderten Daten konnten nicht geladen werden. <br/> Bitte versuchen Sie es sp&auml;ter nochmal.",
          PLAY_START: "Diaschau starten",
          PLAY_STOP: "Diaschau beenden",
          FULL_SCREEN: "Vollbild",
          THUMBS: "Vorschaubilder",
          DOWNLOAD: "Herunterladen",
          SHARE: "Teilen",
          ZOOM: "Vergr&ouml;&szlig;ern"
        }
      }
    },
        s = n(t),
        r = n(e),
        c = 0,
        l = function l(t) {
      return t && t.hasOwnProperty && t instanceof n;
    },
        d = function () {
      return t.requestAnimationFrame || t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || t.oRequestAnimationFrame || function (e) {
        return t.setTimeout(e, 1e3 / 60);
      };
    }(),
        u = function () {
      return t.cancelAnimationFrame || t.webkitCancelAnimationFrame || t.mozCancelAnimationFrame || t.oCancelAnimationFrame || function (e) {
        t.clearTimeout(e);
      };
    }(),
        f = function () {
      var t,
          n = e.createElement("fakeelement"),
          o = {
        transition: "transitionend",
        OTransition: "oTransitionEnd",
        MozTransition: "transitionend",
        WebkitTransition: "webkitTransitionEnd"
      };

      for (t in o) {
        if (void 0 !== n.style[t]) return o[t];
      }

      return "transitionend";
    }(),
        p = function p(t) {
      return t && t.length && t[0].offsetHeight;
    },
        h = function h(t, e) {
      var o = n.extend(!0, {}, t, e);
      return n.each(e, function (t, e) {
        n.isArray(e) && (o[t] = e);
      }), o;
    },
        g = function g(t) {
      var o, i;
      return !(!t || t.ownerDocument !== e) && (n(".fancybox-container").css("pointer-events", "none"), o = {
        x: t.getBoundingClientRect().left + t.offsetWidth / 2,
        y: t.getBoundingClientRect().top + t.offsetHeight / 2
      }, i = e.elementFromPoint(o.x, o.y) === t, n(".fancybox-container").css("pointer-events", ""), i);
    },
        b = function b(t, e, o) {
      var i = this;
      i.opts = h({
        index: o
      }, n.fancybox.defaults), n.isPlainObject(e) && (i.opts = h(i.opts, e)), n.fancybox.isMobile && (i.opts = h(i.opts, i.opts.mobile)), i.id = i.opts.id || ++c, i.currIndex = parseInt(i.opts.index, 10) || 0, i.prevIndex = null, i.prevPos = null, i.currPos = 0, i.firstRun = !0, i.group = [], i.slides = {}, i.addContent(t), i.group.length && i.init();
    };

    n.extend(b.prototype, {
      init: function init() {
        var o,
            i,
            a = this,
            s = a.group[a.currIndex],
            r = s.opts;
        r.closeExisting && n.fancybox.close(!0), n("body").addClass("fancybox-active"), !n.fancybox.getInstance() && !1 !== r.hideScrollbar && !n.fancybox.isMobile && e.body.scrollHeight > t.innerHeight && (n("head").append('<style id="fancybox-style-noscroll" type="text/css">.compensate-for-scrollbar{margin-right:' + (t.innerWidth - e.documentElement.clientWidth) + "px;}</style>"), n("body").addClass("compensate-for-scrollbar")), i = "", n.each(r.buttons, function (t, e) {
          i += r.btnTpl[e] || "";
        }), o = n(a.translate(a, r.baseTpl.replace("{{buttons}}", i).replace("{{arrows}}", r.btnTpl.arrowLeft + r.btnTpl.arrowRight))).attr("id", "fancybox-container-" + a.id).addClass(r.baseClass).data("FancyBox", a).appendTo(r.parentEl), a.$refs = {
          container: o
        }, ["bg", "inner", "infobar", "toolbar", "stage", "caption", "navigation"].forEach(function (t) {
          a.$refs[t] = o.find(".fancybox-" + t);
        }), a.trigger("onInit"), a.activate(), a.jumpTo(a.currIndex);
      },
      translate: function translate(t, e) {
        var n = t.opts.i18n[t.opts.lang] || t.opts.i18n.en;
        return e.replace(/\{\{(\w+)\}\}/g, function (t, e) {
          return void 0 === n[e] ? t : n[e];
        });
      },
      addContent: function addContent(t) {
        var e,
            o = this,
            i = n.makeArray(t);
        n.each(i, function (t, e) {
          var i,
              a,
              s,
              r,
              c,
              l = {},
              d = {};
          n.isPlainObject(e) ? (l = e, d = e.opts || e) : "object" === n.type(e) && n(e).length ? (i = n(e), d = i.data() || {}, d = n.extend(!0, {}, d, d.options), d.$orig = i, l.src = o.opts.src || d.src || i.attr("href"), l.type || l.src || (l.type = "inline", l.src = e)) : l = {
            type: "html",
            src: e + ""
          }, l.opts = n.extend(!0, {}, o.opts, d), n.isArray(d.buttons) && (l.opts.buttons = d.buttons), n.fancybox.isMobile && l.opts.mobile && (l.opts = h(l.opts, l.opts.mobile)), a = l.type || l.opts.type, r = l.src || "", !a && r && ((s = r.match(/\.(mp4|mov|ogv|webm)((\?|#).*)?$/i)) ? (a = "video", l.opts.video.format || (l.opts.video.format = "video/" + ("ogv" === s[1] ? "ogg" : s[1]))) : r.match(/(^data:image\/[a-z0-9+\/=]*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg|ico)((\?|#).*)?$)/i) ? a = "image" : r.match(/\.(pdf)((\?|#).*)?$/i) ? (a = "iframe", l = n.extend(!0, l, {
            contentType: "pdf",
            opts: {
              iframe: {
                preload: !1
              }
            }
          })) : "#" === r.charAt(0) && (a = "inline")), a ? l.type = a : o.trigger("objectNeedsType", l), l.contentType || (l.contentType = n.inArray(l.type, ["html", "inline", "ajax"]) > -1 ? "html" : l.type), l.index = o.group.length, "auto" == l.opts.smallBtn && (l.opts.smallBtn = n.inArray(l.type, ["html", "inline", "ajax"]) > -1), "auto" === l.opts.toolbar && (l.opts.toolbar = !l.opts.smallBtn), l.$thumb = l.opts.$thumb || null, l.opts.$trigger && l.index === o.opts.index && (l.$thumb = l.opts.$trigger.find("img:first"), l.$thumb.length && (l.opts.$orig = l.opts.$trigger)), l.$thumb && l.$thumb.length || !l.opts.$orig || (l.$thumb = l.opts.$orig.find("img:first")), l.$thumb && !l.$thumb.length && (l.$thumb = null), l.thumb = l.opts.thumb || (l.$thumb ? l.$thumb[0].src : null), "function" === n.type(l.opts.caption) && (l.opts.caption = l.opts.caption.apply(e, [o, l])), "function" === n.type(o.opts.caption) && (l.opts.caption = o.opts.caption.apply(e, [o, l])), l.opts.caption instanceof n || (l.opts.caption = void 0 === l.opts.caption ? "" : l.opts.caption + ""), "ajax" === l.type && (c = r.split(/\s+/, 2), c.length > 1 && (l.src = c.shift(), l.opts.filter = c.shift())), l.opts.modal && (l.opts = n.extend(!0, l.opts, {
            trapFocus: !0,
            infobar: 0,
            toolbar: 0,
            smallBtn: 0,
            keyboard: 0,
            slideShow: 0,
            fullScreen: 0,
            thumbs: 0,
            touch: 0,
            clickContent: !1,
            clickSlide: !1,
            clickOutside: !1,
            dblclickContent: !1,
            dblclickSlide: !1,
            dblclickOutside: !1
          })), o.group.push(l);
        }), Object.keys(o.slides).length && (o.updateControls(), (e = o.Thumbs) && e.isActive && (e.create(), e.focus()));
      },
      addEvents: function addEvents() {
        var e = this;
        e.removeEvents(), e.$refs.container.on("click.fb-close", "[data-fancybox-close]", function (t) {
          t.stopPropagation(), t.preventDefault(), e.close(t);
        }).on("touchstart.fb-prev click.fb-prev", "[data-fancybox-prev]", function (t) {
          t.stopPropagation(), t.preventDefault(), e.previous();
        }).on("touchstart.fb-next click.fb-next", "[data-fancybox-next]", function (t) {
          t.stopPropagation(), t.preventDefault(), e.next();
        }).on("click.fb", "[data-fancybox-zoom]", function (t) {
          e[e.isScaledDown() ? "scaleToActual" : "scaleToFit"]();
        }), s.on("orientationchange.fb resize.fb", function (t) {
          t && t.originalEvent && "resize" === t.originalEvent.type ? (e.requestId && u(e.requestId), e.requestId = d(function () {
            e.update(t);
          })) : (e.current && "iframe" === e.current.type && e.$refs.stage.hide(), setTimeout(function () {
            e.$refs.stage.show(), e.update(t);
          }, n.fancybox.isMobile ? 600 : 250));
        }), r.on("keydown.fb", function (t) {
          var o = n.fancybox ? n.fancybox.getInstance() : null,
              i = o.current,
              a = t.keyCode || t.which;
          if (9 == a) return void (i.opts.trapFocus && e.focus(t));
          if (!(!i.opts.keyboard || t.ctrlKey || t.altKey || t.shiftKey || n(t.target).is("input,textarea,video,audio,select"))) return 8 === a || 27 === a ? (t.preventDefault(), void e.close(t)) : 37 === a || 38 === a ? (t.preventDefault(), void e.previous()) : 39 === a || 40 === a ? (t.preventDefault(), void e.next()) : void e.trigger("afterKeydown", t, a);
        }), e.group[e.currIndex].opts.idleTime && (e.idleSecondsCounter = 0, r.on("mousemove.fb-idle mouseleave.fb-idle mousedown.fb-idle touchstart.fb-idle touchmove.fb-idle scroll.fb-idle keydown.fb-idle", function (t) {
          e.idleSecondsCounter = 0, e.isIdle && e.showControls(), e.isIdle = !1;
        }), e.idleInterval = t.setInterval(function () {
          ++e.idleSecondsCounter >= e.group[e.currIndex].opts.idleTime && !e.isDragging && (e.isIdle = !0, e.idleSecondsCounter = 0, e.hideControls());
        }, 1e3));
      },
      removeEvents: function removeEvents() {
        var e = this;
        s.off("orientationchange.fb resize.fb"), r.off("keydown.fb .fb-idle"), this.$refs.container.off(".fb-close .fb-prev .fb-next"), e.idleInterval && (t.clearInterval(e.idleInterval), e.idleInterval = null);
      },
      previous: function previous(t) {
        return this.jumpTo(this.currPos - 1, t);
      },
      next: function next(t) {
        return this.jumpTo(this.currPos + 1, t);
      },
      jumpTo: function jumpTo(t, e) {
        var o,
            i,
            a,
            s,
            r,
            c,
            l,
            d,
            u,
            f = this,
            h = f.group.length;

        if (!(f.isDragging || f.isClosing || f.isAnimating && f.firstRun)) {
          if (t = parseInt(t, 10), !(a = f.current ? f.current.opts.loop : f.opts.loop) && (t < 0 || t >= h)) return !1;
          if (o = f.firstRun = !Object.keys(f.slides).length, r = f.current, f.prevIndex = f.currIndex, f.prevPos = f.currPos, s = f.createSlide(t), h > 1 && ((a || s.index < h - 1) && f.createSlide(t + 1), (a || s.index > 0) && f.createSlide(t - 1)), f.current = s, f.currIndex = s.index, f.currPos = s.pos, f.trigger("beforeShow", o), f.updateControls(), s.forcedDuration = void 0, n.isNumeric(e) ? s.forcedDuration = e : e = s.opts[o ? "animationDuration" : "transitionDuration"], e = parseInt(e, 10), i = f.isMoved(s), s.$slide.addClass("fancybox-slide--current"), o) return s.opts.animationEffect && e && f.$refs.container.css("transition-duration", e + "ms"), f.$refs.container.addClass("fancybox-is-open").trigger("focus"), f.loadSlide(s), void f.preload("image");
          c = n.fancybox.getTranslate(r.$slide), l = n.fancybox.getTranslate(f.$refs.stage), n.each(f.slides, function (t, e) {
            n.fancybox.stop(e.$slide, !0);
          }), r.pos !== s.pos && (r.isComplete = !1), r.$slide.removeClass("fancybox-slide--complete fancybox-slide--current"), i ? (u = c.left - (r.pos * c.width + r.pos * r.opts.gutter), n.each(f.slides, function (t, o) {
            o.$slide.removeClass("fancybox-animated").removeClass(function (t, e) {
              return (e.match(/(^|\s)fancybox-fx-\S+/g) || []).join(" ");
            });
            var i = o.pos * c.width + o.pos * o.opts.gutter;
            n.fancybox.setTranslate(o.$slide, {
              top: 0,
              left: i - l.left + u
            }), o.pos !== s.pos && o.$slide.addClass("fancybox-slide--" + (o.pos > s.pos ? "next" : "previous")), p(o.$slide), n.fancybox.animate(o.$slide, {
              top: 0,
              left: (o.pos - s.pos) * c.width + (o.pos - s.pos) * o.opts.gutter
            }, e, function () {
              o.$slide.css({
                transform: "",
                opacity: ""
              }).removeClass("fancybox-slide--next fancybox-slide--previous"), o.pos === f.currPos && f.complete();
            });
          })) : e && s.opts.transitionEffect && (d = "fancybox-animated fancybox-fx-" + s.opts.transitionEffect, r.$slide.addClass("fancybox-slide--" + (r.pos > s.pos ? "next" : "previous")), n.fancybox.animate(r.$slide, d, e, function () {
            r.$slide.removeClass(d).removeClass("fancybox-slide--next fancybox-slide--previous");
          }, !1)), s.isLoaded ? f.revealContent(s) : f.loadSlide(s), f.preload("image");
        }
      },
      createSlide: function createSlide(t) {
        var e,
            o,
            i = this;
        return o = t % i.group.length, o = o < 0 ? i.group.length + o : o, !i.slides[t] && i.group[o] && (e = n('<div class="fancybox-slide"></div>').appendTo(i.$refs.stage), i.slides[t] = n.extend(!0, {}, i.group[o], {
          pos: t,
          $slide: e,
          isLoaded: !1
        }), i.updateSlide(i.slides[t])), i.slides[t];
      },
      scaleToActual: function scaleToActual(t, e, o) {
        var i,
            a,
            s,
            r,
            c,
            l = this,
            d = l.current,
            u = d.$content,
            f = n.fancybox.getTranslate(d.$slide).width,
            p = n.fancybox.getTranslate(d.$slide).height,
            h = d.width,
            g = d.height;
        l.isAnimating || l.isMoved() || !u || "image" != d.type || !d.isLoaded || d.hasError || (l.isAnimating = !0, n.fancybox.stop(u), t = void 0 === t ? .5 * f : t, e = void 0 === e ? .5 * p : e, i = n.fancybox.getTranslate(u), i.top -= n.fancybox.getTranslate(d.$slide).top, i.left -= n.fancybox.getTranslate(d.$slide).left, r = h / i.width, c = g / i.height, a = .5 * f - .5 * h, s = .5 * p - .5 * g, h > f && (a = i.left * r - (t * r - t), a > 0 && (a = 0), a < f - h && (a = f - h)), g > p && (s = i.top * c - (e * c - e), s > 0 && (s = 0), s < p - g && (s = p - g)), l.updateCursor(h, g), n.fancybox.animate(u, {
          top: s,
          left: a,
          scaleX: r,
          scaleY: c
        }, o || 366, function () {
          l.isAnimating = !1;
        }), l.SlideShow && l.SlideShow.isActive && l.SlideShow.stop());
      },
      scaleToFit: function scaleToFit(t) {
        var e,
            o = this,
            i = o.current,
            a = i.$content;
        o.isAnimating || o.isMoved() || !a || "image" != i.type || !i.isLoaded || i.hasError || (o.isAnimating = !0, n.fancybox.stop(a), e = o.getFitPos(i), o.updateCursor(e.width, e.height), n.fancybox.animate(a, {
          top: e.top,
          left: e.left,
          scaleX: e.width / a.width(),
          scaleY: e.height / a.height()
        }, t || 366, function () {
          o.isAnimating = !1;
        }));
      },
      getFitPos: function getFitPos(t) {
        var e,
            o,
            i,
            a,
            s = this,
            r = t.$content,
            c = t.$slide,
            l = t.width || t.opts.width,
            d = t.height || t.opts.height,
            u = {};
        return !!(t.isLoaded && r && r.length) && (e = n.fancybox.getTranslate(s.$refs.stage).width, o = n.fancybox.getTranslate(s.$refs.stage).height, e -= parseFloat(c.css("paddingLeft")) + parseFloat(c.css("paddingRight")) + parseFloat(r.css("marginLeft")) + parseFloat(r.css("marginRight")), o -= parseFloat(c.css("paddingTop")) + parseFloat(c.css("paddingBottom")) + parseFloat(r.css("marginTop")) + parseFloat(r.css("marginBottom")), l && d || (l = e, d = o), i = Math.min(1, e / l, o / d), l *= i, d *= i, l > e - .5 && (l = e), d > o - .5 && (d = o), "image" === t.type ? (u.top = Math.floor(.5 * (o - d)) + parseFloat(c.css("paddingTop")), u.left = Math.floor(.5 * (e - l)) + parseFloat(c.css("paddingLeft"))) : "video" === t.contentType && (a = t.opts.width && t.opts.height ? l / d : t.opts.ratio || 16 / 9, d > l / a ? d = l / a : l > d * a && (l = d * a)), u.width = l, u.height = d, u);
      },
      update: function update(t) {
        var e = this;
        n.each(e.slides, function (n, o) {
          e.updateSlide(o, t);
        });
      },
      updateSlide: function updateSlide(t, e) {
        var o = this,
            i = t && t.$content,
            a = t.width || t.opts.width,
            s = t.height || t.opts.height,
            r = t.$slide;
        o.adjustCaption(t), i && (a || s || "video" === t.contentType) && !t.hasError && (n.fancybox.stop(i), n.fancybox.setTranslate(i, o.getFitPos(t)), t.pos === o.currPos && (o.isAnimating = !1, o.updateCursor())), o.adjustLayout(t), r.length && (r.trigger("refresh"), t.pos === o.currPos && o.$refs.toolbar.add(o.$refs.navigation.find(".fancybox-button--arrow_right")).toggleClass("compensate-for-scrollbar", r.get(0).scrollHeight > r.get(0).clientHeight)), o.trigger("onUpdate", t, e);
      },
      centerSlide: function centerSlide(t) {
        var e = this,
            o = e.current,
            i = o.$slide;
        !e.isClosing && o && (i.siblings().css({
          transform: "",
          opacity: ""
        }), i.parent().children().removeClass("fancybox-slide--previous fancybox-slide--next"), n.fancybox.animate(i, {
          top: 0,
          left: 0,
          opacity: 1
        }, void 0 === t ? 0 : t, function () {
          i.css({
            transform: "",
            opacity: ""
          }), o.isComplete || e.complete();
        }, !1));
      },
      isMoved: function isMoved(t) {
        var e,
            o,
            i = t || this.current;
        return !!i && (o = n.fancybox.getTranslate(this.$refs.stage), e = n.fancybox.getTranslate(i.$slide), !i.$slide.hasClass("fancybox-animated") && (Math.abs(e.top - o.top) > .5 || Math.abs(e.left - o.left) > .5));
      },
      updateCursor: function updateCursor(t, e) {
        var o,
            i,
            a = this,
            s = a.current,
            r = a.$refs.container;
        s && !a.isClosing && a.Guestures && (r.removeClass("fancybox-is-zoomable fancybox-can-zoomIn fancybox-can-zoomOut fancybox-can-swipe fancybox-can-pan"), o = a.canPan(t, e), i = !!o || a.isZoomable(), r.toggleClass("fancybox-is-zoomable", i), n("[data-fancybox-zoom]").prop("disabled", !i), o ? r.addClass("fancybox-can-pan") : i && ("zoom" === s.opts.clickContent || n.isFunction(s.opts.clickContent) && "zoom" == s.opts.clickContent(s)) ? r.addClass("fancybox-can-zoomIn") : s.opts.touch && (s.opts.touch.vertical || a.group.length > 1) && "video" !== s.contentType && r.addClass("fancybox-can-swipe"));
      },
      isZoomable: function isZoomable() {
        var t,
            e = this,
            n = e.current;

        if (n && !e.isClosing && "image" === n.type && !n.hasError) {
          if (!n.isLoaded) return !0;
          if ((t = e.getFitPos(n)) && (n.width > t.width || n.height > t.height)) return !0;
        }

        return !1;
      },
      isScaledDown: function isScaledDown(t, e) {
        var o = this,
            i = !1,
            a = o.current,
            s = a.$content;
        return void 0 !== t && void 0 !== e ? i = t < a.width && e < a.height : s && (i = n.fancybox.getTranslate(s), i = i.width < a.width && i.height < a.height), i;
      },
      canPan: function canPan(t, e) {
        var o = this,
            i = o.current,
            a = null,
            s = !1;
        return "image" === i.type && (i.isComplete || t && e) && !i.hasError && (s = o.getFitPos(i), void 0 !== t && void 0 !== e ? a = {
          width: t,
          height: e
        } : i.isComplete && (a = n.fancybox.getTranslate(i.$content)), a && s && (s = Math.abs(a.width - s.width) > 1.5 || Math.abs(a.height - s.height) > 1.5)), s;
      },
      loadSlide: function loadSlide(t) {
        var e,
            o,
            i,
            a = this;

        if (!t.isLoading && !t.isLoaded) {
          if (t.isLoading = !0, !1 === a.trigger("beforeLoad", t)) return t.isLoading = !1, !1;

          switch (e = t.type, o = t.$slide, o.off("refresh").trigger("onReset").addClass(t.opts.slideClass), e) {
            case "image":
              a.setImage(t);
              break;

            case "iframe":
              a.setIframe(t);
              break;

            case "html":
              a.setContent(t, t.src || t.content);
              break;

            case "video":
              a.setContent(t, t.opts.video.tpl.replace(/\{\{src\}\}/gi, t.src).replace("{{format}}", t.opts.videoFormat || t.opts.video.format || "").replace("{{poster}}", t.thumb || ""));
              break;

            case "inline":
              n(t.src).length ? a.setContent(t, n(t.src)) : a.setError(t);
              break;

            case "ajax":
              a.showLoading(t), i = n.ajax(n.extend({}, t.opts.ajax.settings, {
                url: t.src,
                success: function success(e, n) {
                  "success" === n && a.setContent(t, e);
                },
                error: function error(e, n) {
                  e && "abort" !== n && a.setError(t);
                }
              })), o.one("onReset", function () {
                i.abort();
              });
              break;

            default:
              a.setError(t);
          }

          return !0;
        }
      },
      setImage: function setImage(t) {
        var o,
            i = this;
        setTimeout(function () {
          var e = t.$image;
          i.isClosing || !t.isLoading || e && e.length && e[0].complete || t.hasError || i.showLoading(t);
        }, 50), i.checkSrcset(t), t.$content = n('<div class="fancybox-content"></div>').addClass("fancybox-is-hidden").appendTo(t.$slide.addClass("fancybox-slide--image")), !1 !== t.opts.preload && t.opts.width && t.opts.height && t.thumb && (t.width = t.opts.width, t.height = t.opts.height, o = e.createElement("img"), o.onerror = function () {
          n(this).remove(), t.$ghost = null;
        }, o.onload = function () {
          i.afterLoad(t);
        }, t.$ghost = n(o).addClass("fancybox-image").appendTo(t.$content).attr("src", t.thumb)), i.setBigImage(t);
      },
      checkSrcset: function checkSrcset(e) {
        var n,
            o,
            i,
            a,
            s = e.opts.srcset || e.opts.image.srcset;

        if (s) {
          i = t.devicePixelRatio || 1, a = t.innerWidth * i, o = s.split(",").map(function (t) {
            var e = {};
            return t.trim().split(/\s+/).forEach(function (t, n) {
              var o = parseInt(t.substring(0, t.length - 1), 10);
              if (0 === n) return e.url = t;
              o && (e.value = o, e.postfix = t[t.length - 1]);
            }), e;
          }), o.sort(function (t, e) {
            return t.value - e.value;
          });

          for (var r = 0; r < o.length; r++) {
            var c = o[r];

            if ("w" === c.postfix && c.value >= a || "x" === c.postfix && c.value >= i) {
              n = c;
              break;
            }
          }

          !n && o.length && (n = o[o.length - 1]), n && (e.src = n.url, e.width && e.height && "w" == n.postfix && (e.height = e.width / e.height * n.value, e.width = n.value), e.opts.srcset = s);
        }
      },
      setBigImage: function setBigImage(t) {
        var o = this,
            i = e.createElement("img"),
            a = n(i);
        t.$image = a.one("error", function () {
          o.setError(t);
        }).one("load", function () {
          var e;
          t.$ghost || (o.resolveImageSlideSize(t, this.naturalWidth, this.naturalHeight), o.afterLoad(t)), o.isClosing || (t.opts.srcset && (e = t.opts.sizes, e && "auto" !== e || (e = (t.width / t.height > 1 && s.width() / s.height() > 1 ? "100" : Math.round(t.width / t.height * 100)) + "vw"), a.attr("sizes", e).attr("srcset", t.opts.srcset)), t.$ghost && setTimeout(function () {
            t.$ghost && !o.isClosing && t.$ghost.hide();
          }, Math.min(300, Math.max(1e3, t.height / 1600))), o.hideLoading(t));
        }).addClass("fancybox-image").attr("src", t.src).appendTo(t.$content), (i.complete || "complete" == i.readyState) && a.naturalWidth && a.naturalHeight ? a.trigger("load") : i.error && a.trigger("error");
      },
      resolveImageSlideSize: function resolveImageSlideSize(t, e, n) {
        var o = parseInt(t.opts.width, 10),
            i = parseInt(t.opts.height, 10);
        t.width = e, t.height = n, o > 0 && (t.width = o, t.height = Math.floor(o * n / e)), i > 0 && (t.width = Math.floor(i * e / n), t.height = i);
      },
      setIframe: function setIframe(t) {
        var e,
            o = this,
            i = t.opts.iframe,
            a = t.$slide;
        t.$content = n('<div class="fancybox-content' + (i.preload ? " fancybox-is-hidden" : "") + '"></div>').css(i.css).appendTo(a), a.addClass("fancybox-slide--" + t.contentType), t.$iframe = e = n(i.tpl.replace(/\{rnd\}/g, new Date().getTime())).attr(i.attr).appendTo(t.$content), i.preload ? (o.showLoading(t), e.on("load.fb error.fb", function (e) {
          this.isReady = 1, t.$slide.trigger("refresh"), o.afterLoad(t);
        }), a.on("refresh.fb", function () {
          var n,
              o,
              s = t.$content,
              r = i.css.width,
              c = i.css.height;

          if (1 === e[0].isReady) {
            try {
              n = e.contents(), o = n.find("body");
            } catch (t) {}

            o && o.length && o.children().length && (a.css("overflow", "visible"), s.css({
              width: "100%",
              "max-width": "100%",
              height: "9999px"
            }), void 0 === r && (r = Math.ceil(Math.max(o[0].clientWidth, o.outerWidth(!0)))), s.css("width", r || "").css("max-width", ""), void 0 === c && (c = Math.ceil(Math.max(o[0].clientHeight, o.outerHeight(!0)))), s.css("height", c || ""), a.css("overflow", "auto")), s.removeClass("fancybox-is-hidden");
          }
        })) : o.afterLoad(t), e.attr("src", t.src), a.one("onReset", function () {
          try {
            n(this).find("iframe").hide().unbind().attr("src", "//about:blank");
          } catch (t) {}

          n(this).off("refresh.fb").empty(), t.isLoaded = !1, t.isRevealed = !1;
        });
      },
      setContent: function setContent(t, e) {
        var o = this;
        o.isClosing || (o.hideLoading(t), t.$content && n.fancybox.stop(t.$content), t.$slide.empty(), l(e) && e.parent().length ? ((e.hasClass("fancybox-content") || e.parent().hasClass("fancybox-content")) && e.parents(".fancybox-slide").trigger("onReset"), t.$placeholder = n("<div>").hide().insertAfter(e), e.css("display", "inline-block")) : t.hasError || ("string" === n.type(e) && (e = n("<div>").append(n.trim(e)).contents()), t.opts.filter && (e = n("<div>").html(e).find(t.opts.filter))), t.$slide.one("onReset", function () {
          n(this).find("video,audio").trigger("pause"), t.$placeholder && (t.$placeholder.after(e.removeClass("fancybox-content").hide()).remove(), t.$placeholder = null), t.$smallBtn && (t.$smallBtn.remove(), t.$smallBtn = null), t.hasError || (n(this).empty(), t.isLoaded = !1, t.isRevealed = !1);
        }), n(e).appendTo(t.$slide), n(e).is("video,audio") && (n(e).addClass("fancybox-video"), n(e).wrap("<div></div>"), t.contentType = "video", t.opts.width = t.opts.width || n(e).attr("width"), t.opts.height = t.opts.height || n(e).attr("height")), t.$content = t.$slide.children().filter("div,form,main,video,audio,article,.fancybox-content").first(), t.$content.siblings().hide(), t.$content.length || (t.$content = t.$slide.wrapInner("<div></div>").children().first()), t.$content.addClass("fancybox-content"), t.$slide.addClass("fancybox-slide--" + t.contentType), o.afterLoad(t));
      },
      setError: function setError(t) {
        t.hasError = !0, t.$slide.trigger("onReset").removeClass("fancybox-slide--" + t.contentType).addClass("fancybox-slide--error"), t.contentType = "html", this.setContent(t, this.translate(t, t.opts.errorTpl)), t.pos === this.currPos && (this.isAnimating = !1);
      },
      showLoading: function showLoading(t) {
        var e = this;
        (t = t || e.current) && !t.$spinner && (t.$spinner = n(e.translate(e, e.opts.spinnerTpl)).appendTo(t.$slide).hide().fadeIn("fast"));
      },
      hideLoading: function hideLoading(t) {
        var e = this;
        (t = t || e.current) && t.$spinner && (t.$spinner.stop().remove(), delete t.$spinner);
      },
      afterLoad: function afterLoad(t) {
        var e = this;
        e.isClosing || (t.isLoading = !1, t.isLoaded = !0, e.trigger("afterLoad", t), e.hideLoading(t), !t.opts.smallBtn || t.$smallBtn && t.$smallBtn.length || (t.$smallBtn = n(e.translate(t, t.opts.btnTpl.smallBtn)).appendTo(t.$content)), t.opts.protect && t.$content && !t.hasError && (t.$content.on("contextmenu.fb", function (t) {
          return 2 == t.button && t.preventDefault(), !0;
        }), "image" === t.type && n('<div class="fancybox-spaceball"></div>').appendTo(t.$content)), e.adjustCaption(t), e.adjustLayout(t), t.pos === e.currPos && e.updateCursor(), e.revealContent(t));
      },
      adjustCaption: function adjustCaption(t) {
        var e,
            n = this,
            o = t || n.current,
            i = o.opts.caption,
            a = o.opts.preventCaptionOverlap,
            s = n.$refs.caption,
            r = !1;
        s.toggleClass("fancybox-caption--separate", a), a && i && i.length && (o.pos !== n.currPos ? (e = s.clone().appendTo(s.parent()), e.children().eq(0).empty().html(i), r = e.outerHeight(!0), e.empty().remove()) : n.$caption && (r = n.$caption.outerHeight(!0)), o.$slide.css("padding-bottom", r || ""));
      },
      adjustLayout: function adjustLayout(t) {
        var e,
            n,
            o,
            i,
            a = this,
            s = t || a.current;
        s.isLoaded && !0 !== s.opts.disableLayoutFix && (s.$content.css("margin-bottom", ""), s.$content.outerHeight() > s.$slide.height() + .5 && (o = s.$slide[0].style["padding-bottom"], i = s.$slide.css("padding-bottom"), parseFloat(i) > 0 && (e = s.$slide[0].scrollHeight, s.$slide.css("padding-bottom", 0), Math.abs(e - s.$slide[0].scrollHeight) < 1 && (n = i), s.$slide.css("padding-bottom", o))), s.$content.css("margin-bottom", n));
      },
      revealContent: function revealContent(t) {
        var e,
            o,
            i,
            a,
            s = this,
            r = t.$slide,
            c = !1,
            l = !1,
            d = s.isMoved(t),
            u = t.isRevealed;
        return t.isRevealed = !0, e = t.opts[s.firstRun ? "animationEffect" : "transitionEffect"], i = t.opts[s.firstRun ? "animationDuration" : "transitionDuration"], i = parseInt(void 0 === t.forcedDuration ? i : t.forcedDuration, 10), !d && t.pos === s.currPos && i || (e = !1), "zoom" === e && (t.pos === s.currPos && i && "image" === t.type && !t.hasError && (l = s.getThumbPos(t)) ? c = s.getFitPos(t) : e = "fade"), "zoom" === e ? (s.isAnimating = !0, c.scaleX = c.width / l.width, c.scaleY = c.height / l.height, a = t.opts.zoomOpacity, "auto" == a && (a = Math.abs(t.width / t.height - l.width / l.height) > .1), a && (l.opacity = .1, c.opacity = 1), n.fancybox.setTranslate(t.$content.removeClass("fancybox-is-hidden"), l), p(t.$content), void n.fancybox.animate(t.$content, c, i, function () {
          s.isAnimating = !1, s.complete();
        })) : (s.updateSlide(t), e ? (n.fancybox.stop(r), o = "fancybox-slide--" + (t.pos >= s.prevPos ? "next" : "previous") + " fancybox-animated fancybox-fx-" + e, r.addClass(o).removeClass("fancybox-slide--current"), t.$content.removeClass("fancybox-is-hidden"), p(r), "image" !== t.type && t.$content.hide().show(0), void n.fancybox.animate(r, "fancybox-slide--current", i, function () {
          r.removeClass(o).css({
            transform: "",
            opacity: ""
          }), t.pos === s.currPos && s.complete();
        }, !0)) : (t.$content.removeClass("fancybox-is-hidden"), u || !d || "image" !== t.type || t.hasError || t.$content.hide().fadeIn("fast"), void (t.pos === s.currPos && s.complete())));
      },
      getThumbPos: function getThumbPos(t) {
        var e,
            o,
            i,
            a,
            s,
            r = !1,
            c = t.$thumb;
        return !(!c || !g(c[0])) && (e = n.fancybox.getTranslate(c), o = parseFloat(c.css("border-top-width") || 0), i = parseFloat(c.css("border-right-width") || 0), a = parseFloat(c.css("border-bottom-width") || 0), s = parseFloat(c.css("border-left-width") || 0), r = {
          top: e.top + o,
          left: e.left + s,
          width: e.width - i - s,
          height: e.height - o - a,
          scaleX: 1,
          scaleY: 1
        }, e.width > 0 && e.height > 0 && r);
      },
      complete: function complete() {
        var t,
            e = this,
            o = e.current,
            i = {};
        !e.isMoved() && o.isLoaded && (o.isComplete || (o.isComplete = !0, o.$slide.siblings().trigger("onReset"), e.preload("inline"), p(o.$slide), o.$slide.addClass("fancybox-slide--complete"), n.each(e.slides, function (t, o) {
          o.pos >= e.currPos - 1 && o.pos <= e.currPos + 1 ? i[o.pos] = o : o && (n.fancybox.stop(o.$slide), o.$slide.off().remove());
        }), e.slides = i), e.isAnimating = !1, e.updateCursor(), e.trigger("afterShow"), o.opts.video.autoStart && o.$slide.find("video,audio").filter(":visible:first").trigger("play").one("ended", function () {
          Document.exitFullscreen ? Document.exitFullscreen() : this.webkitExitFullscreen && this.webkitExitFullscreen(), e.next();
        }), o.opts.autoFocus && "html" === o.contentType && (t = o.$content.find("input[autofocus]:enabled:visible:first"), t.length ? t.trigger("focus") : e.focus(null, !0)), o.$slide.scrollTop(0).scrollLeft(0));
      },
      preload: function preload(t) {
        var e,
            n,
            o = this;
        o.group.length < 2 || (n = o.slides[o.currPos + 1], e = o.slides[o.currPos - 1], e && e.type === t && o.loadSlide(e), n && n.type === t && o.loadSlide(n));
      },
      focus: function focus(t, o) {
        var i,
            a,
            s = this,
            r = ["a[href]", "area[href]", 'input:not([disabled]):not([type="hidden"]):not([aria-hidden])', "select:not([disabled]):not([aria-hidden])", "textarea:not([disabled]):not([aria-hidden])", "button:not([disabled]):not([aria-hidden])", "iframe", "object", "embed", "video", "audio", "[contenteditable]", '[tabindex]:not([tabindex^="-"])'].join(",");
        s.isClosing || (i = !t && s.current && s.current.isComplete ? s.current.$slide.find("*:visible" + (o ? ":not(.fancybox-close-small)" : "")) : s.$refs.container.find("*:visible"), i = i.filter(r).filter(function () {
          return "hidden" !== n(this).css("visibility") && !n(this).hasClass("disabled");
        }), i.length ? (a = i.index(e.activeElement), t && t.shiftKey ? (a < 0 || 0 == a) && (t.preventDefault(), i.eq(i.length - 1).trigger("focus")) : (a < 0 || a == i.length - 1) && (t && t.preventDefault(), i.eq(0).trigger("focus"))) : s.$refs.container.trigger("focus"));
      },
      activate: function activate() {
        var t = this;
        n(".fancybox-container").each(function () {
          var e = n(this).data("FancyBox");
          e && e.id !== t.id && !e.isClosing && (e.trigger("onDeactivate"), e.removeEvents(), e.isVisible = !1);
        }), t.isVisible = !0, (t.current || t.isIdle) && (t.update(), t.updateControls()), t.trigger("onActivate"), t.addEvents();
      },
      close: function close(t, e) {
        var o,
            i,
            a,
            s,
            r,
            c,
            l,
            u = this,
            f = u.current,
            h = function h() {
          u.cleanUp(t);
        };

        return !u.isClosing && (u.isClosing = !0, !1 === u.trigger("beforeClose", t) ? (u.isClosing = !1, d(function () {
          u.update();
        }), !1) : (u.removeEvents(), a = f.$content, o = f.opts.animationEffect, i = n.isNumeric(e) ? e : o ? f.opts.animationDuration : 0, f.$slide.removeClass("fancybox-slide--complete fancybox-slide--next fancybox-slide--previous fancybox-animated"), !0 !== t ? n.fancybox.stop(f.$slide) : o = !1, f.$slide.siblings().trigger("onReset").remove(), i && u.$refs.container.removeClass("fancybox-is-open").addClass("fancybox-is-closing").css("transition-duration", i + "ms"), u.hideLoading(f), u.hideControls(!0), u.updateCursor(), "zoom" !== o || a && i && "image" === f.type && !u.isMoved() && !f.hasError && (l = u.getThumbPos(f)) || (o = "fade"), "zoom" === o ? (n.fancybox.stop(a), s = n.fancybox.getTranslate(a), c = {
          top: s.top,
          left: s.left,
          scaleX: s.width / l.width,
          scaleY: s.height / l.height,
          width: l.width,
          height: l.height
        }, r = f.opts.zoomOpacity, "auto" == r && (r = Math.abs(f.width / f.height - l.width / l.height) > .1), r && (l.opacity = 0), n.fancybox.setTranslate(a, c), p(a), n.fancybox.animate(a, l, i, h), !0) : (o && i ? n.fancybox.animate(f.$slide.addClass("fancybox-slide--previous").removeClass("fancybox-slide--current"), "fancybox-animated fancybox-fx-" + o, i, h) : !0 === t ? setTimeout(h, i) : h(), !0)));
      },
      cleanUp: function cleanUp(e) {
        var o,
            i,
            a,
            s = this,
            r = s.current.opts.$orig;
        s.current.$slide.trigger("onReset"), s.$refs.container.empty().remove(), s.trigger("afterClose", e), s.current.opts.backFocus && (r && r.length && r.is(":visible") || (r = s.$trigger), r && r.length && (i = t.scrollX, a = t.scrollY, r.trigger("focus"), n("html, body").scrollTop(a).scrollLeft(i))), s.current = null, o = n.fancybox.getInstance(), o ? o.activate() : (n("body").removeClass("fancybox-active compensate-for-scrollbar"), n("#fancybox-style-noscroll").remove());
      },
      trigger: function trigger(t, e) {
        var o,
            i = Array.prototype.slice.call(arguments, 1),
            a = this,
            s = e && e.opts ? e : a.current;
        if (s ? i.unshift(s) : s = a, i.unshift(a), n.isFunction(s.opts[t]) && (o = s.opts[t].apply(s, i)), !1 === o) return o;
        "afterClose" !== t && a.$refs ? a.$refs.container.trigger(t + ".fb", i) : r.trigger(t + ".fb", i);
      },
      updateControls: function updateControls() {
        var t = this,
            o = t.current,
            i = o.index,
            a = t.$refs.container,
            s = t.$refs.caption,
            r = o.opts.caption;
        o.$slide.trigger("refresh"), r && r.length ? (t.$caption = s, s.children().eq(0).html(r)) : t.$caption = null, t.hasHiddenControls || t.isIdle || t.showControls(), a.find("[data-fancybox-count]").html(t.group.length), a.find("[data-fancybox-index]").html(i + 1), a.find("[data-fancybox-prev]").prop("disabled", !o.opts.loop && i <= 0), a.find("[data-fancybox-next]").prop("disabled", !o.opts.loop && i >= t.group.length - 1), "image" === o.type ? a.find("[data-fancybox-zoom]").show().end().find("[data-fancybox-download]").attr("href", o.opts.image.src || o.src).show() : o.opts.toolbar && a.find("[data-fancybox-download],[data-fancybox-zoom]").hide(), n(e.activeElement).is(":hidden,[disabled]") && t.$refs.container.trigger("focus");
      },
      hideControls: function hideControls(t) {
        var e = this,
            n = ["infobar", "toolbar", "nav"];
        !t && e.current.opts.preventCaptionOverlap || n.push("caption"), this.$refs.container.removeClass(n.map(function (t) {
          return "fancybox-show-" + t;
        }).join(" ")), this.hasHiddenControls = !0;
      },
      showControls: function showControls() {
        var t = this,
            e = t.current ? t.current.opts : t.opts,
            n = t.$refs.container;
        t.hasHiddenControls = !1, t.idleSecondsCounter = 0, n.toggleClass("fancybox-show-toolbar", !(!e.toolbar || !e.buttons)).toggleClass("fancybox-show-infobar", !!(e.infobar && t.group.length > 1)).toggleClass("fancybox-show-caption", !!t.$caption).toggleClass("fancybox-show-nav", !!(e.arrows && t.group.length > 1)).toggleClass("fancybox-is-modal", !!e.modal);
      },
      toggleControls: function toggleControls() {
        this.hasHiddenControls ? this.showControls() : this.hideControls();
      }
    }), n.fancybox = {
      version: "3.5.7",
      defaults: a,
      getInstance: function getInstance(t) {
        var e = n('.fancybox-container:not(".fancybox-is-closing"):last').data("FancyBox"),
            o = Array.prototype.slice.call(arguments, 1);
        return e instanceof b && ("string" === n.type(t) ? e[t].apply(e, o) : "function" === n.type(t) && t.apply(e, o), e);
      },
      open: function open(t, e, n) {
        return new b(t, e, n);
      },
      close: function close(t) {
        var e = this.getInstance();
        e && (e.close(), !0 === t && this.close(t));
      },
      destroy: function destroy() {
        this.close(!0), r.add("body").off("click.fb-start", "**");
      },
      isMobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
      use3d: function () {
        var n = e.createElement("div");
        return t.getComputedStyle && t.getComputedStyle(n) && t.getComputedStyle(n).getPropertyValue("transform") && !(e.documentMode && e.documentMode < 11);
      }(),
      getTranslate: function getTranslate(t) {
        var e;
        return !(!t || !t.length) && (e = t[0].getBoundingClientRect(), {
          top: e.top || 0,
          left: e.left || 0,
          width: e.width,
          height: e.height,
          opacity: parseFloat(t.css("opacity"))
        });
      },
      setTranslate: function setTranslate(t, e) {
        var n = "",
            o = {};
        if (t && e) return void 0 === e.left && void 0 === e.top || (n = (void 0 === e.left ? t.position().left : e.left) + "px, " + (void 0 === e.top ? t.position().top : e.top) + "px", n = this.use3d ? "translate3d(" + n + ", 0px)" : "translate(" + n + ")"), void 0 !== e.scaleX && void 0 !== e.scaleY ? n += " scale(" + e.scaleX + ", " + e.scaleY + ")" : void 0 !== e.scaleX && (n += " scaleX(" + e.scaleX + ")"), n.length && (o.transform = n), void 0 !== e.opacity && (o.opacity = e.opacity), void 0 !== e.width && (o.width = e.width), void 0 !== e.height && (o.height = e.height), t.css(o);
      },
      animate: function animate(t, e, o, i, a) {
        var s,
            r = this;
        n.isFunction(o) && (i = o, o = null), r.stop(t), s = r.getTranslate(t), t.on(f, function (c) {
          (!c || !c.originalEvent || t.is(c.originalEvent.target) && "z-index" != c.originalEvent.propertyName) && (r.stop(t), n.isNumeric(o) && t.css("transition-duration", ""), n.isPlainObject(e) ? void 0 !== e.scaleX && void 0 !== e.scaleY && r.setTranslate(t, {
            top: e.top,
            left: e.left,
            width: s.width * e.scaleX,
            height: s.height * e.scaleY,
            scaleX: 1,
            scaleY: 1
          }) : !0 !== a && t.removeClass(e), n.isFunction(i) && i(c));
        }), n.isNumeric(o) && t.css("transition-duration", o + "ms"), n.isPlainObject(e) ? (void 0 !== e.scaleX && void 0 !== e.scaleY && (delete e.width, delete e.height, t.parent().hasClass("fancybox-slide--image") && t.parent().addClass("fancybox-is-scaling")), n.fancybox.setTranslate(t, e)) : t.addClass(e), t.data("timer", setTimeout(function () {
          t.trigger(f);
        }, o + 33));
      },
      stop: function stop(t, e) {
        t && t.length && (clearTimeout(t.data("timer")), e && t.trigger(f), t.off(f).css("transition-duration", ""), t.parent().removeClass("fancybox-is-scaling"));
      }
    }, n.fn.fancybox = function (t) {
      var e;
      return t = t || {}, e = t.selector || !1, e ? n("body").off("click.fb-start", e).on("click.fb-start", e, {
        options: t
      }, i) : this.off("click.fb-start").on("click.fb-start", {
        items: this,
        options: t
      }, i), this;
    }, r.on("click.fb-start", "[data-fancybox]", i), r.on("click.fb-start", "[data-fancybox-trigger]", function (t) {
      n('[data-fancybox="' + n(this).attr("data-fancybox-trigger") + '"]').eq(n(this).attr("data-fancybox-index") || 0).trigger("click.fb-start", {
        $trigger: n(this)
      });
    }), function () {
      var t = null;
      r.on("mousedown mouseup focus blur", ".fancybox-button", function (e) {
        switch (e.type) {
          case "mousedown":
            t = n(this);
            break;

          case "mouseup":
            t = null;
            break;

          case "focusin":
            n(".fancybox-button").removeClass("fancybox-focus"), n(this).is(t) || n(this).is("[disabled]") || n(this).addClass("fancybox-focus");
            break;

          case "focusout":
            n(".fancybox-button").removeClass("fancybox-focus");
        }
      });
    }();
  }
}(window, document, jQuery), function (t) {
  "use strict";

  var e = {
    youtube: {
      matcher: /(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i,
      params: {
        autoplay: 1,
        autohide: 1,
        fs: 1,
        rel: 0,
        hd: 1,
        wmode: "transparent",
        enablejsapi: 1,
        html5: 1
      },
      paramPlace: 8,
      type: "iframe",
      url: "https://www.youtube-nocookie.com/embed/$4",
      thumb: "https://img.youtube.com/vi/$4/hqdefault.jpg"
    },
    vimeo: {
      matcher: /^.+vimeo.com\/(.*\/)?([\d]+)(.*)?/,
      params: {
        autoplay: 1,
        hd: 1,
        show_title: 1,
        show_byline: 1,
        show_portrait: 0,
        fullscreen: 1
      },
      paramPlace: 3,
      type: "iframe",
      url: "//player.vimeo.com/video/$2"
    },
    instagram: {
      matcher: /(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,
      type: "image",
      url: "//$1/p/$2/media/?size=l"
    },
    gmap_place: {
      matcher: /(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(((maps\/(place\/(.*)\/)?\@(.*),(\d+.?\d+?)z))|(\?ll=))(.*)?/i,
      type: "iframe",
      url: function url(t) {
        return "//maps.google." + t[2] + "/?ll=" + (t[9] ? t[9] + "&z=" + Math.floor(t[10]) + (t[12] ? t[12].replace(/^\//, "&") : "") : t[12] + "").replace(/\?/, "&") + "&output=" + (t[12] && t[12].indexOf("layer=c") > 0 ? "svembed" : "embed");
      }
    },
    gmap_search: {
      matcher: /(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(maps\/search\/)(.*)/i,
      type: "iframe",
      url: function url(t) {
        return "//maps.google." + t[2] + "/maps?q=" + t[5].replace("query=", "q=").replace("api=1", "") + "&output=embed";
      }
    }
  },
      n = function n(e, _n, o) {
    if (e) return o = o || "", "object" === t.type(o) && (o = t.param(o, !0)), t.each(_n, function (t, n) {
      e = e.replace("$" + t, n || "");
    }), o.length && (e += (e.indexOf("?") > 0 ? "&" : "?") + o), e;
  };

  t(document).on("objectNeedsType.fb", function (o, i, a) {
    var s,
        r,
        c,
        l,
        d,
        u,
        f,
        p = a.src || "",
        h = !1;
    s = t.extend(!0, {}, e, a.opts.media), t.each(s, function (e, o) {
      if (c = p.match(o.matcher)) {
        if (h = o.type, f = e, u = {}, o.paramPlace && c[o.paramPlace]) {
          d = c[o.paramPlace], "?" == d[0] && (d = d.substring(1)), d = d.split("&");

          for (var i = 0; i < d.length; ++i) {
            var s = d[i].split("=", 2);
            2 == s.length && (u[s[0]] = decodeURIComponent(s[1].replace(/\+/g, " ")));
          }
        }

        return l = t.extend(!0, {}, o.params, a.opts[e], u), p = "function" === t.type(o.url) ? o.url.call(this, c, l, a) : n(o.url, c, l), r = "function" === t.type(o.thumb) ? o.thumb.call(this, c, l, a) : n(o.thumb, c), "youtube" === e ? p = p.replace(/&t=((\d+)m)?(\d+)s/, function (t, e, n, o) {
          return "&start=" + ((n ? 60 * parseInt(n, 10) : 0) + parseInt(o, 10));
        }) : "vimeo" === e && (p = p.replace("&%23", "#")), !1;
      }
    }), h ? (a.opts.thumb || a.opts.$thumb && a.opts.$thumb.length || (a.opts.thumb = r), "iframe" === h && (a.opts = t.extend(!0, a.opts, {
      iframe: {
        preload: !1,
        attr: {
          scrolling: "no"
        }
      }
    })), t.extend(a, {
      type: h,
      src: p,
      origSrc: a.src,
      contentSource: f,
      contentType: "image" === h ? "image" : "gmap_place" == f || "gmap_search" == f ? "map" : "video"
    })) : p && (a.type = a.opts.defaultType);
  });
  var o = {
    youtube: {
      src: "https://www.youtube.com/iframe_api",
      class: "YT",
      loading: !1,
      loaded: !1
    },
    vimeo: {
      src: "https://player.vimeo.com/api/player.js",
      class: "Vimeo",
      loading: !1,
      loaded: !1
    },
    load: function load(t) {
      var e,
          n = this;
      if (this[t].loaded) return void setTimeout(function () {
        n.done(t);
      });
      this[t].loading || (this[t].loading = !0, e = document.createElement("script"), e.type = "text/javascript", e.src = this[t].src, "youtube" === t ? window.onYouTubeIframeAPIReady = function () {
        n[t].loaded = !0, n.done(t);
      } : e.onload = function () {
        n[t].loaded = !0, n.done(t);
      }, document.body.appendChild(e));
    },
    done: function done(e) {
      var n, o, i;
      "youtube" === e && delete window.onYouTubeIframeAPIReady, (n = t.fancybox.getInstance()) && (o = n.current.$content.find("iframe"), "youtube" === e && void 0 !== YT && YT ? i = new YT.Player(o.attr("id"), {
        events: {
          onStateChange: function onStateChange(t) {
            0 == t.data && n.next();
          }
        }
      }) : "vimeo" === e && void 0 !== Vimeo && Vimeo && (i = new Vimeo.Player(o), i.on("ended", function () {
        n.next();
      })));
    }
  };
  t(document).on({
    "afterShow.fb": function afterShowFb(t, e, n) {
      e.group.length > 1 && ("youtube" === n.contentSource || "vimeo" === n.contentSource) && o.load(n.contentSource);
    }
  });
}(jQuery), function (t, e, n) {
  "use strict";

  var o = function () {
    return t.requestAnimationFrame || t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || t.oRequestAnimationFrame || function (e) {
      return t.setTimeout(e, 1e3 / 60);
    };
  }(),
      i = function () {
    return t.cancelAnimationFrame || t.webkitCancelAnimationFrame || t.mozCancelAnimationFrame || t.oCancelAnimationFrame || function (e) {
      t.clearTimeout(e);
    };
  }(),
      a = function a(e) {
    var n = [];
    e = e.originalEvent || e || t.e, e = e.touches && e.touches.length ? e.touches : e.changedTouches && e.changedTouches.length ? e.changedTouches : [e];

    for (var o in e) {
      e[o].pageX ? n.push({
        x: e[o].pageX,
        y: e[o].pageY
      }) : e[o].clientX && n.push({
        x: e[o].clientX,
        y: e[o].clientY
      });
    }

    return n;
  },
      s = function s(t, e, n) {
    return e && t ? "x" === n ? t.x - e.x : "y" === n ? t.y - e.y : Math.sqrt(Math.pow(t.x - e.x, 2) + Math.pow(t.y - e.y, 2)) : 0;
  },
      r = function r(t) {
    if (t.is('a,area,button,[role="button"],input,label,select,summary,textarea,video,audio,iframe') || n.isFunction(t.get(0).onclick) || t.data("selectable")) return !0;

    for (var e = 0, o = t[0].attributes, i = o.length; e < i; e++) {
      if ("data-fancybox-" === o[e].nodeName.substr(0, 14)) return !0;
    }

    return !1;
  },
      c = function c(e) {
    var n = t.getComputedStyle(e)["overflow-y"],
        o = t.getComputedStyle(e)["overflow-x"],
        i = ("scroll" === n || "auto" === n) && e.scrollHeight > e.clientHeight,
        a = ("scroll" === o || "auto" === o) && e.scrollWidth > e.clientWidth;
    return i || a;
  },
      l = function l(t) {
    for (var e = !1;;) {
      if (e = c(t.get(0))) break;
      if (t = t.parent(), !t.length || t.hasClass("fancybox-stage") || t.is("body")) break;
    }

    return e;
  },
      d = function d(t) {
    var e = this;
    e.instance = t, e.$bg = t.$refs.bg, e.$stage = t.$refs.stage, e.$container = t.$refs.container, e.destroy(), e.$container.on("touchstart.fb.touch mousedown.fb.touch", n.proxy(e, "ontouchstart"));
  };

  d.prototype.destroy = function () {
    var t = this;
    t.$container.off(".fb.touch"), n(e).off(".fb.touch"), t.requestId && (i(t.requestId), t.requestId = null), t.tapped && (clearTimeout(t.tapped), t.tapped = null);
  }, d.prototype.ontouchstart = function (o) {
    var i = this,
        c = n(o.target),
        d = i.instance,
        u = d.current,
        f = u.$slide,
        p = u.$content,
        h = "touchstart" == o.type;

    if (h && i.$container.off("mousedown.fb.touch"), (!o.originalEvent || 2 != o.originalEvent.button) && f.length && c.length && !r(c) && !r(c.parent()) && (c.is("img") || !(o.originalEvent.clientX > c[0].clientWidth + c.offset().left))) {
      if (!u || d.isAnimating || u.$slide.hasClass("fancybox-animated")) return o.stopPropagation(), void o.preventDefault();
      i.realPoints = i.startPoints = a(o), i.startPoints.length && (u.touch && o.stopPropagation(), i.startEvent = o, i.canTap = !0, i.$target = c, i.$content = p, i.opts = u.opts.touch, i.isPanning = !1, i.isSwiping = !1, i.isZooming = !1, i.isScrolling = !1, i.canPan = d.canPan(), i.startTime = new Date().getTime(), i.distanceX = i.distanceY = i.distance = 0, i.canvasWidth = Math.round(f[0].clientWidth), i.canvasHeight = Math.round(f[0].clientHeight), i.contentLastPos = null, i.contentStartPos = n.fancybox.getTranslate(i.$content) || {
        top: 0,
        left: 0
      }, i.sliderStartPos = n.fancybox.getTranslate(f), i.stagePos = n.fancybox.getTranslate(d.$refs.stage), i.sliderStartPos.top -= i.stagePos.top, i.sliderStartPos.left -= i.stagePos.left, i.contentStartPos.top -= i.stagePos.top, i.contentStartPos.left -= i.stagePos.left, n(e).off(".fb.touch").on(h ? "touchend.fb.touch touchcancel.fb.touch" : "mouseup.fb.touch mouseleave.fb.touch", n.proxy(i, "ontouchend")).on(h ? "touchmove.fb.touch" : "mousemove.fb.touch", n.proxy(i, "ontouchmove")), n.fancybox.isMobile && e.addEventListener("scroll", i.onscroll, !0), ((i.opts || i.canPan) && (c.is(i.$stage) || i.$stage.find(c).length) || (c.is(".fancybox-image") && o.preventDefault(), n.fancybox.isMobile && c.parents(".fancybox-caption").length)) && (i.isScrollable = l(c) || l(c.parent()), n.fancybox.isMobile && i.isScrollable || o.preventDefault(), (1 === i.startPoints.length || u.hasError) && (i.canPan ? (n.fancybox.stop(i.$content), i.isPanning = !0) : i.isSwiping = !0, i.$container.addClass("fancybox-is-grabbing")), 2 === i.startPoints.length && "image" === u.type && (u.isLoaded || u.$ghost) && (i.canTap = !1, i.isSwiping = !1, i.isPanning = !1, i.isZooming = !0, n.fancybox.stop(i.$content), i.centerPointStartX = .5 * (i.startPoints[0].x + i.startPoints[1].x) - n(t).scrollLeft(), i.centerPointStartY = .5 * (i.startPoints[0].y + i.startPoints[1].y) - n(t).scrollTop(), i.percentageOfImageAtPinchPointX = (i.centerPointStartX - i.contentStartPos.left) / i.contentStartPos.width, i.percentageOfImageAtPinchPointY = (i.centerPointStartY - i.contentStartPos.top) / i.contentStartPos.height, i.startDistanceBetweenFingers = s(i.startPoints[0], i.startPoints[1]))));
    }
  }, d.prototype.onscroll = function (t) {
    var n = this;
    n.isScrolling = !0, e.removeEventListener("scroll", n.onscroll, !0);
  }, d.prototype.ontouchmove = function (t) {
    var e = this;
    return void 0 !== t.originalEvent.buttons && 0 === t.originalEvent.buttons ? void e.ontouchend(t) : e.isScrolling ? void (e.canTap = !1) : (e.newPoints = a(t), void ((e.opts || e.canPan) && e.newPoints.length && e.newPoints.length && (e.isSwiping && !0 === e.isSwiping || t.preventDefault(), e.distanceX = s(e.newPoints[0], e.startPoints[0], "x"), e.distanceY = s(e.newPoints[0], e.startPoints[0], "y"), e.distance = s(e.newPoints[0], e.startPoints[0]), e.distance > 0 && (e.isSwiping ? e.onSwipe(t) : e.isPanning ? e.onPan() : e.isZooming && e.onZoom()))));
  }, d.prototype.onSwipe = function (e) {
    var a,
        s = this,
        r = s.instance,
        c = s.isSwiping,
        l = s.sliderStartPos.left || 0;
    if (!0 !== c) "x" == c && (s.distanceX > 0 && (s.instance.group.length < 2 || 0 === s.instance.current.index && !s.instance.current.opts.loop) ? l += Math.pow(s.distanceX, .8) : s.distanceX < 0 && (s.instance.group.length < 2 || s.instance.current.index === s.instance.group.length - 1 && !s.instance.current.opts.loop) ? l -= Math.pow(-s.distanceX, .8) : l += s.distanceX), s.sliderLastPos = {
      top: "x" == c ? 0 : s.sliderStartPos.top + s.distanceY,
      left: l
    }, s.requestId && (i(s.requestId), s.requestId = null), s.requestId = o(function () {
      s.sliderLastPos && (n.each(s.instance.slides, function (t, e) {
        var o = e.pos - s.instance.currPos;
        n.fancybox.setTranslate(e.$slide, {
          top: s.sliderLastPos.top,
          left: s.sliderLastPos.left + o * s.canvasWidth + o * e.opts.gutter
        });
      }), s.$container.addClass("fancybox-is-sliding"));
    });else if (Math.abs(s.distance) > 10) {
      if (s.canTap = !1, r.group.length < 2 && s.opts.vertical ? s.isSwiping = "y" : r.isDragging || !1 === s.opts.vertical || "auto" === s.opts.vertical && n(t).width() > 800 ? s.isSwiping = "x" : (a = Math.abs(180 * Math.atan2(s.distanceY, s.distanceX) / Math.PI), s.isSwiping = a > 45 && a < 135 ? "y" : "x"), "y" === s.isSwiping && n.fancybox.isMobile && s.isScrollable) return void (s.isScrolling = !0);
      r.isDragging = s.isSwiping, s.startPoints = s.newPoints, n.each(r.slides, function (t, e) {
        var o, i;
        n.fancybox.stop(e.$slide), o = n.fancybox.getTranslate(e.$slide), i = n.fancybox.getTranslate(r.$refs.stage), e.$slide.css({
          transform: "",
          opacity: "",
          "transition-duration": ""
        }).removeClass("fancybox-animated").removeClass(function (t, e) {
          return (e.match(/(^|\s)fancybox-fx-\S+/g) || []).join(" ");
        }), e.pos === r.current.pos && (s.sliderStartPos.top = o.top - i.top, s.sliderStartPos.left = o.left - i.left), n.fancybox.setTranslate(e.$slide, {
          top: o.top - i.top,
          left: o.left - i.left
        });
      }), r.SlideShow && r.SlideShow.isActive && r.SlideShow.stop();
    }
  }, d.prototype.onPan = function () {
    var t = this;
    if (s(t.newPoints[0], t.realPoints[0]) < (n.fancybox.isMobile ? 10 : 5)) return void (t.startPoints = t.newPoints);
    t.canTap = !1, t.contentLastPos = t.limitMovement(), t.requestId && i(t.requestId), t.requestId = o(function () {
      n.fancybox.setTranslate(t.$content, t.contentLastPos);
    });
  }, d.prototype.limitMovement = function () {
    var t,
        e,
        n,
        o,
        i,
        a,
        s = this,
        r = s.canvasWidth,
        c = s.canvasHeight,
        l = s.distanceX,
        d = s.distanceY,
        u = s.contentStartPos,
        f = u.left,
        p = u.top,
        h = u.width,
        g = u.height;
    return i = h > r ? f + l : f, a = p + d, t = Math.max(0, .5 * r - .5 * h), e = Math.max(0, .5 * c - .5 * g), n = Math.min(r - h, .5 * r - .5 * h), o = Math.min(c - g, .5 * c - .5 * g), l > 0 && i > t && (i = t - 1 + Math.pow(-t + f + l, .8) || 0), l < 0 && i < n && (i = n + 1 - Math.pow(n - f - l, .8) || 0), d > 0 && a > e && (a = e - 1 + Math.pow(-e + p + d, .8) || 0), d < 0 && a < o && (a = o + 1 - Math.pow(o - p - d, .8) || 0), {
      top: a,
      left: i
    };
  }, d.prototype.limitPosition = function (t, e, n, o) {
    var i = this,
        a = i.canvasWidth,
        s = i.canvasHeight;
    return n > a ? (t = t > 0 ? 0 : t, t = t < a - n ? a - n : t) : t = Math.max(0, a / 2 - n / 2), o > s ? (e = e > 0 ? 0 : e, e = e < s - o ? s - o : e) : e = Math.max(0, s / 2 - o / 2), {
      top: e,
      left: t
    };
  }, d.prototype.onZoom = function () {
    var e = this,
        a = e.contentStartPos,
        r = a.width,
        c = a.height,
        l = a.left,
        d = a.top,
        u = s(e.newPoints[0], e.newPoints[1]),
        f = u / e.startDistanceBetweenFingers,
        p = Math.floor(r * f),
        h = Math.floor(c * f),
        g = (r - p) * e.percentageOfImageAtPinchPointX,
        b = (c - h) * e.percentageOfImageAtPinchPointY,
        m = (e.newPoints[0].x + e.newPoints[1].x) / 2 - n(t).scrollLeft(),
        v = (e.newPoints[0].y + e.newPoints[1].y) / 2 - n(t).scrollTop(),
        y = m - e.centerPointStartX,
        x = v - e.centerPointStartY,
        w = l + (g + y),
        $ = d + (b + x),
        S = {
      top: $,
      left: w,
      scaleX: f,
      scaleY: f
    };
    e.canTap = !1, e.newWidth = p, e.newHeight = h, e.contentLastPos = S, e.requestId && i(e.requestId), e.requestId = o(function () {
      n.fancybox.setTranslate(e.$content, e.contentLastPos);
    });
  }, d.prototype.ontouchend = function (t) {
    var o = this,
        s = o.isSwiping,
        r = o.isPanning,
        c = o.isZooming,
        l = o.isScrolling;
    if (o.endPoints = a(t), o.dMs = Math.max(new Date().getTime() - o.startTime, 1), o.$container.removeClass("fancybox-is-grabbing"), n(e).off(".fb.touch"), e.removeEventListener("scroll", o.onscroll, !0), o.requestId && (i(o.requestId), o.requestId = null), o.isSwiping = !1, o.isPanning = !1, o.isZooming = !1, o.isScrolling = !1, o.instance.isDragging = !1, o.canTap) return o.onTap(t);
    o.speed = 100, o.velocityX = o.distanceX / o.dMs * .5, o.velocityY = o.distanceY / o.dMs * .5, r ? o.endPanning() : c ? o.endZooming() : o.endSwiping(s, l);
  }, d.prototype.endSwiping = function (t, e) {
    var o = this,
        i = !1,
        a = o.instance.group.length,
        s = Math.abs(o.distanceX),
        r = "x" == t && a > 1 && (o.dMs > 130 && s > 10 || s > 50);
    o.sliderLastPos = null, "y" == t && !e && Math.abs(o.distanceY) > 50 ? (n.fancybox.animate(o.instance.current.$slide, {
      top: o.sliderStartPos.top + o.distanceY + 150 * o.velocityY,
      opacity: 0
    }, 200), i = o.instance.close(!0, 250)) : r && o.distanceX > 0 ? i = o.instance.previous(300) : r && o.distanceX < 0 && (i = o.instance.next(300)), !1 !== i || "x" != t && "y" != t || o.instance.centerSlide(200), o.$container.removeClass("fancybox-is-sliding");
  }, d.prototype.endPanning = function () {
    var t,
        e,
        o,
        i = this;
    i.contentLastPos && (!1 === i.opts.momentum || i.dMs > 350 ? (t = i.contentLastPos.left, e = i.contentLastPos.top) : (t = i.contentLastPos.left + 500 * i.velocityX, e = i.contentLastPos.top + 500 * i.velocityY), o = i.limitPosition(t, e, i.contentStartPos.width, i.contentStartPos.height), o.width = i.contentStartPos.width, o.height = i.contentStartPos.height, n.fancybox.animate(i.$content, o, 366));
  }, d.prototype.endZooming = function () {
    var t,
        e,
        o,
        i,
        a = this,
        s = a.instance.current,
        r = a.newWidth,
        c = a.newHeight;
    a.contentLastPos && (t = a.contentLastPos.left, e = a.contentLastPos.top, i = {
      top: e,
      left: t,
      width: r,
      height: c,
      scaleX: 1,
      scaleY: 1
    }, n.fancybox.setTranslate(a.$content, i), r < a.canvasWidth && c < a.canvasHeight ? a.instance.scaleToFit(150) : r > s.width || c > s.height ? a.instance.scaleToActual(a.centerPointStartX, a.centerPointStartY, 150) : (o = a.limitPosition(t, e, r, c), n.fancybox.animate(a.$content, o, 150)));
  }, d.prototype.onTap = function (e) {
    var o,
        i = this,
        s = n(e.target),
        r = i.instance,
        c = r.current,
        l = e && a(e) || i.startPoints,
        d = l[0] ? l[0].x - n(t).scrollLeft() - i.stagePos.left : 0,
        u = l[0] ? l[0].y - n(t).scrollTop() - i.stagePos.top : 0,
        f = function f(t) {
      var o = c.opts[t];
      if (n.isFunction(o) && (o = o.apply(r, [c, e])), o) switch (o) {
        case "close":
          r.close(i.startEvent);
          break;

        case "toggleControls":
          r.toggleControls();
          break;

        case "next":
          r.next();
          break;

        case "nextOrClose":
          r.group.length > 1 ? r.next() : r.close(i.startEvent);
          break;

        case "zoom":
          "image" == c.type && (c.isLoaded || c.$ghost) && (r.canPan() ? r.scaleToFit() : r.isScaledDown() ? r.scaleToActual(d, u) : r.group.length < 2 && r.close(i.startEvent));
      }
    };

    if ((!e.originalEvent || 2 != e.originalEvent.button) && (s.is("img") || !(d > s[0].clientWidth + s.offset().left))) {
      if (s.is(".fancybox-bg,.fancybox-inner,.fancybox-outer,.fancybox-container")) o = "Outside";else if (s.is(".fancybox-slide")) o = "Slide";else {
        if (!r.current.$content || !r.current.$content.find(s).addBack().filter(s).length) return;
        o = "Content";
      }

      if (i.tapped) {
        if (clearTimeout(i.tapped), i.tapped = null, Math.abs(d - i.tapX) > 50 || Math.abs(u - i.tapY) > 50) return this;
        f("dblclick" + o);
      } else i.tapX = d, i.tapY = u, c.opts["dblclick" + o] && c.opts["dblclick" + o] !== c.opts["click" + o] ? i.tapped = setTimeout(function () {
        i.tapped = null, r.isAnimating || f("click" + o);
      }, 500) : f("click" + o);

      return this;
    }
  }, n(e).on("onActivate.fb", function (t, e) {
    e && !e.Guestures && (e.Guestures = new d(e));
  }).on("beforeClose.fb", function (t, e) {
    e && e.Guestures && e.Guestures.destroy();
  });
}(window, document, jQuery), function (t, e) {
  "use strict";

  e.extend(!0, e.fancybox.defaults, {
    btnTpl: {
      slideShow: '<button data-fancybox-play class="fancybox-button fancybox-button--play" title="{{PLAY_START}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M6.5 5.4v13.2l11-6.6z"/></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M8.33 5.75h2.2v12.5h-2.2V5.75zm5.15 0h2.2v12.5h-2.2V5.75z"/></svg></button>'
    },
    slideShow: {
      autoStart: !1,
      speed: 3e3,
      progress: !0
    }
  });

  var n = function n(t) {
    this.instance = t, this.init();
  };

  e.extend(n.prototype, {
    timer: null,
    isActive: !1,
    $button: null,
    init: function init() {
      var t = this,
          n = t.instance,
          o = n.group[n.currIndex].opts.slideShow;
      t.$button = n.$refs.toolbar.find("[data-fancybox-play]").on("click", function () {
        t.toggle();
      }), n.group.length < 2 || !o ? t.$button.hide() : o.progress && (t.$progress = e('<div class="fancybox-progress"></div>').appendTo(n.$refs.inner));
    },
    set: function set(t) {
      var n = this,
          o = n.instance,
          i = o.current;
      i && (!0 === t || i.opts.loop || o.currIndex < o.group.length - 1) ? n.isActive && "video" !== i.contentType && (n.$progress && e.fancybox.animate(n.$progress.show(), {
        scaleX: 1
      }, i.opts.slideShow.speed), n.timer = setTimeout(function () {
        o.current.opts.loop || o.current.index != o.group.length - 1 ? o.next() : o.jumpTo(0);
      }, i.opts.slideShow.speed)) : (n.stop(), o.idleSecondsCounter = 0, o.showControls());
    },
    clear: function clear() {
      var t = this;
      clearTimeout(t.timer), t.timer = null, t.$progress && t.$progress.removeAttr("style").hide();
    },
    start: function start() {
      var t = this,
          e = t.instance.current;
      e && (t.$button.attr("title", (e.opts.i18n[e.opts.lang] || e.opts.i18n.en).PLAY_STOP).removeClass("fancybox-button--play").addClass("fancybox-button--pause"), t.isActive = !0, e.isComplete && t.set(!0), t.instance.trigger("onSlideShowChange", !0));
    },
    stop: function stop() {
      var t = this,
          e = t.instance.current;
      t.clear(), t.$button.attr("title", (e.opts.i18n[e.opts.lang] || e.opts.i18n.en).PLAY_START).removeClass("fancybox-button--pause").addClass("fancybox-button--play"), t.isActive = !1, t.instance.trigger("onSlideShowChange", !1), t.$progress && t.$progress.removeAttr("style").hide();
    },
    toggle: function toggle() {
      var t = this;
      t.isActive ? t.stop() : t.start();
    }
  }), e(t).on({
    "onInit.fb": function onInitFb(t, e) {
      e && !e.SlideShow && (e.SlideShow = new n(e));
    },
    "beforeShow.fb": function beforeShowFb(t, e, n, o) {
      var i = e && e.SlideShow;
      o ? i && n.opts.slideShow.autoStart && i.start() : i && i.isActive && i.clear();
    },
    "afterShow.fb": function afterShowFb(t, e, n) {
      var o = e && e.SlideShow;
      o && o.isActive && o.set();
    },
    "afterKeydown.fb": function afterKeydownFb(n, o, i, a, s) {
      var r = o && o.SlideShow;
      !r || !i.opts.slideShow || 80 !== s && 32 !== s || e(t.activeElement).is("button,a,input") || (a.preventDefault(), r.toggle());
    },
    "beforeClose.fb onDeactivate.fb": function beforeCloseFbOnDeactivateFb(t, e) {
      var n = e && e.SlideShow;
      n && n.stop();
    }
  }), e(t).on("visibilitychange", function () {
    var n = e.fancybox.getInstance(),
        o = n && n.SlideShow;
    o && o.isActive && (t.hidden ? o.clear() : o.set());
  });
}(document, jQuery), function (t, e) {
  "use strict";

  var n = function () {
    for (var e = [["requestFullscreen", "exitFullscreen", "fullscreenElement", "fullscreenEnabled", "fullscreenchange", "fullscreenerror"], ["webkitRequestFullscreen", "webkitExitFullscreen", "webkitFullscreenElement", "webkitFullscreenEnabled", "webkitfullscreenchange", "webkitfullscreenerror"], ["webkitRequestFullScreen", "webkitCancelFullScreen", "webkitCurrentFullScreenElement", "webkitCancelFullScreen", "webkitfullscreenchange", "webkitfullscreenerror"], ["mozRequestFullScreen", "mozCancelFullScreen", "mozFullScreenElement", "mozFullScreenEnabled", "mozfullscreenchange", "mozfullscreenerror"], ["msRequestFullscreen", "msExitFullscreen", "msFullscreenElement", "msFullscreenEnabled", "MSFullscreenChange", "MSFullscreenError"]], n = {}, o = 0; o < e.length; o++) {
      var i = e[o];

      if (i && i[1] in t) {
        for (var a = 0; a < i.length; a++) {
          n[e[0][a]] = i[a];
        }

        return n;
      }
    }

    return !1;
  }();

  if (n) {
    var o = {
      request: function request(e) {
        e = e || t.documentElement, e[n.requestFullscreen](e.ALLOW_KEYBOARD_INPUT);
      },
      exit: function exit() {
        t[n.exitFullscreen]();
      },
      toggle: function toggle(e) {
        e = e || t.documentElement, this.isFullscreen() ? this.exit() : this.request(e);
      },
      isFullscreen: function isFullscreen() {
        return Boolean(t[n.fullscreenElement]);
      },
      enabled: function enabled() {
        return Boolean(t[n.fullscreenEnabled]);
      }
    };
    e.extend(!0, e.fancybox.defaults, {
      btnTpl: {
        fullScreen: '<button data-fancybox-fullscreen class="fancybox-button fancybox-button--fsenter" title="{{FULL_SCREEN}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M7 14H5v5h5v-2H7v-3zm-2-4h2V7h3V5H5v5zm12 7h-3v2h5v-5h-2v3zM14 5v2h3v3h2V5h-5z"/></svg><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M5 16h3v3h2v-5H5zm3-8H5v2h5V5H8zm6 11h2v-3h3v-2h-5zm2-11V5h-2v5h5V8z"/></svg></button>'
      },
      fullScreen: {
        autoStart: !1
      }
    }), e(t).on(n.fullscreenchange, function () {
      var t = o.isFullscreen(),
          n = e.fancybox.getInstance();
      n && (n.current && "image" === n.current.type && n.isAnimating && (n.isAnimating = !1, n.update(!0, !0, 0), n.isComplete || n.complete()), n.trigger("onFullscreenChange", t), n.$refs.container.toggleClass("fancybox-is-fullscreen", t), n.$refs.toolbar.find("[data-fancybox-fullscreen]").toggleClass("fancybox-button--fsenter", !t).toggleClass("fancybox-button--fsexit", t));
    });
  }

  e(t).on({
    "onInit.fb": function onInitFb(t, e) {
      var i;
      if (!n) return void e.$refs.toolbar.find("[data-fancybox-fullscreen]").remove();
      e && e.group[e.currIndex].opts.fullScreen ? (i = e.$refs.container, i.on("click.fb-fullscreen", "[data-fancybox-fullscreen]", function (t) {
        t.stopPropagation(), t.preventDefault(), o.toggle();
      }), e.opts.fullScreen && !0 === e.opts.fullScreen.autoStart && o.request(), e.FullScreen = o) : e && e.$refs.toolbar.find("[data-fancybox-fullscreen]").hide();
    },
    "afterKeydown.fb": function afterKeydownFb(t, e, n, o, i) {
      e && e.FullScreen && 70 === i && (o.preventDefault(), e.FullScreen.toggle());
    },
    "beforeClose.fb": function beforeCloseFb(t, e) {
      e && e.FullScreen && e.$refs.container.hasClass("fancybox-is-fullscreen") && o.exit();
    }
  });
}(document, jQuery), function (t, e) {
  "use strict";

  var n = "fancybox-thumbs";
  e.fancybox.defaults = e.extend(!0, {
    btnTpl: {
      thumbs: '<button data-fancybox-thumbs class="fancybox-button fancybox-button--thumbs" title="{{THUMBS}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M14.59 14.59h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76H5.65v-3.76zm8.94-4.47h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76h-3.76v-3.76zm-4.47 0h3.76v3.76H5.65v-3.76zm8.94-4.47h3.76v3.76h-3.76V5.65zm-4.47 0h3.76v3.76h-3.76V5.65zm-4.47 0h3.76v3.76H5.65V5.65z"/></svg></button>'
    },
    thumbs: {
      autoStart: !1,
      hideOnClose: !0,
      parentEl: ".fancybox-container",
      axis: "y"
    }
  }, e.fancybox.defaults);

  var o = function o(t) {
    this.init(t);
  };

  e.extend(o.prototype, {
    $button: null,
    $grid: null,
    $list: null,
    isVisible: !1,
    isActive: !1,
    init: function init(t) {
      var e = this,
          n = t.group,
          o = 0;
      e.instance = t, e.opts = n[t.currIndex].opts.thumbs, t.Thumbs = e, e.$button = t.$refs.toolbar.find("[data-fancybox-thumbs]");

      for (var i = 0, a = n.length; i < a && (n[i].thumb && o++, !(o > 1)); i++) {
        ;
      }

      o > 1 && e.opts ? (e.$button.removeAttr("style").on("click", function () {
        e.toggle();
      }), e.isActive = !0) : e.$button.hide();
    },
    create: function create() {
      var t,
          o = this,
          i = o.instance,
          a = o.opts.parentEl,
          s = [];
      o.$grid || (o.$grid = e('<div class="' + n + " " + n + "-" + o.opts.axis + '"></div>').appendTo(i.$refs.container.find(a).addBack().filter(a)), o.$grid.on("click", "a", function () {
        i.jumpTo(e(this).attr("data-index"));
      })), o.$list || (o.$list = e('<div class="' + n + '__list">').appendTo(o.$grid)), e.each(i.group, function (e, n) {
        t = n.thumb, t || "image" !== n.type || (t = n.src), s.push('<a href="javascript:;" tabindex="0" data-index="' + e + '"' + (t && t.length ? ' style="background-image:url(' + t + ')"' : 'class="fancybox-thumbs-missing"') + "></a>");
      }), o.$list[0].innerHTML = s.join(""), "x" === o.opts.axis && o.$list.width(parseInt(o.$grid.css("padding-right"), 10) + i.group.length * o.$list.children().eq(0).outerWidth(!0));
    },
    focus: function focus(t) {
      var e,
          n,
          o = this,
          i = o.$list,
          a = o.$grid;
      o.instance.current && (e = i.children().removeClass("fancybox-thumbs-active").filter('[data-index="' + o.instance.current.index + '"]').addClass("fancybox-thumbs-active"), n = e.position(), "y" === o.opts.axis && (n.top < 0 || n.top > i.height() - e.outerHeight()) ? i.stop().animate({
        scrollTop: i.scrollTop() + n.top
      }, t) : "x" === o.opts.axis && (n.left < a.scrollLeft() || n.left > a.scrollLeft() + (a.width() - e.outerWidth())) && i.parent().stop().animate({
        scrollLeft: n.left
      }, t));
    },
    update: function update() {
      var t = this;
      t.instance.$refs.container.toggleClass("fancybox-show-thumbs", this.isVisible), t.isVisible ? (t.$grid || t.create(), t.instance.trigger("onThumbsShow"), t.focus(0)) : t.$grid && t.instance.trigger("onThumbsHide"), t.instance.update();
    },
    hide: function hide() {
      this.isVisible = !1, this.update();
    },
    show: function show() {
      this.isVisible = !0, this.update();
    },
    toggle: function toggle() {
      this.isVisible = !this.isVisible, this.update();
    }
  }), e(t).on({
    "onInit.fb": function onInitFb(t, e) {
      var n;
      e && !e.Thumbs && (n = new o(e), n.isActive && !0 === n.opts.autoStart && n.show());
    },
    "beforeShow.fb": function beforeShowFb(t, e, n, o) {
      var i = e && e.Thumbs;
      i && i.isVisible && i.focus(o ? 0 : 250);
    },
    "afterKeydown.fb": function afterKeydownFb(t, e, n, o, i) {
      var a = e && e.Thumbs;
      a && a.isActive && 71 === i && (o.preventDefault(), a.toggle());
    },
    "beforeClose.fb": function beforeCloseFb(t, e) {
      var n = e && e.Thumbs;
      n && n.isVisible && !1 !== n.opts.hideOnClose && n.$grid.hide();
    }
  });
}(document, jQuery), function (t, e) {
  "use strict";

  function n(t) {
    var e = {
      "&": "&amp;",
      "<": "&lt;",
      ">": "&gt;",
      '"': "&quot;",
      "'": "&#39;",
      "/": "&#x2F;",
      "`": "&#x60;",
      "=": "&#x3D;"
    };
    return String(t).replace(/[&<>"'`=\/]/g, function (t) {
      return e[t];
    });
  }

  e.extend(!0, e.fancybox.defaults, {
    btnTpl: {
      share: '<button data-fancybox-share class="fancybox-button fancybox-button--share" title="{{SHARE}}"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M2.55 19c1.4-8.4 9.1-9.8 11.9-9.8V5l7 7-7 6.3v-3.5c-2.8 0-10.5 2.1-11.9 4.2z"/></svg></button>'
    },
    share: {
      url: function url(t, e) {
        return !t.currentHash && "inline" !== e.type && "html" !== e.type && (e.origSrc || e.src) || window.location;
      },
      tpl: '<div class="fancybox-share"><h1>{{SHARE}}</h1><p><a class="fancybox-share__button fancybox-share__button--fb" href="https://www.facebook.com/sharer/sharer.php?u={{url}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m287 456v-299c0-21 6-35 35-35h38v-63c-7-1-29-3-55-3-54 0-91 33-91 94v306m143-254h-205v72h196" /></svg><span>Facebook</span></a><a class="fancybox-share__button fancybox-share__button--tw" href="https://twitter.com/intent/tweet?url={{url}}&text={{descr}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m456 133c-14 7-31 11-47 13 17-10 30-27 37-46-15 10-34 16-52 20-61-62-157-7-141 75-68-3-129-35-169-85-22 37-11 86 26 109-13 0-26-4-37-9 0 39 28 72 65 80-12 3-25 4-37 2 10 33 41 57 77 57-42 30-77 38-122 34 170 111 378-32 359-208 16-11 30-25 41-42z" /></svg><span>Twitter</span></a><a class="fancybox-share__button fancybox-share__button--pt" href="https://www.pinterest.com/pin/create/button/?url={{url}}&description={{descr}}&media={{media}}"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m265 56c-109 0-164 78-164 144 0 39 15 74 47 87 5 2 10 0 12-5l4-19c2-6 1-8-3-13-9-11-15-25-15-45 0-58 43-110 113-110 62 0 96 38 96 88 0 67-30 122-73 122-24 0-42-19-36-44 6-29 20-60 20-81 0-19-10-35-31-35-25 0-44 26-44 60 0 21 7 36 7 36l-30 125c-8 37-1 83 0 87 0 3 4 4 5 2 2-3 32-39 42-75l16-64c8 16 31 29 56 29 74 0 124-67 124-157 0-69-58-132-146-132z" fill="#fff"/></svg><span>Pinterest</span></a></p><p><input class="fancybox-share__input" type="text" value="{{url_raw}}" onclick="select()" /></p></div>'
    }
  }), e(t).on("click", "[data-fancybox-share]", function () {
    var t,
        o,
        i = e.fancybox.getInstance(),
        a = i.current || null;
    a && ("function" === e.type(a.opts.share.url) && (t = a.opts.share.url.apply(a, [i, a])), o = a.opts.share.tpl.replace(/\{\{media\}\}/g, "image" === a.type ? encodeURIComponent(a.src) : "").replace(/\{\{url\}\}/g, encodeURIComponent(t)).replace(/\{\{url_raw\}\}/g, n(t)).replace(/\{\{descr\}\}/g, i.$caption ? encodeURIComponent(i.$caption.text()) : ""), e.fancybox.open({
      src: i.translate(i, o),
      type: "html",
      opts: {
        touch: !1,
        animationEffect: !1,
        afterLoad: function afterLoad(t, e) {
          i.$refs.container.one("beforeClose.fb", function () {
            t.close(null, 0);
          }), e.$content.find(".fancybox-share__button").click(function () {
            return window.open(this.href, "Share", "width=550, height=450"), !1;
          });
        },
        mobile: {
          autoFocus: !1
        }
      }
    }));
  });
}(document, jQuery), function (t, e, n) {
  "use strict";

  function o() {
    var e = t.location.hash.substr(1),
        n = e.split("-"),
        o = n.length > 1 && /^\+?\d+$/.test(n[n.length - 1]) ? parseInt(n.pop(-1), 10) || 1 : 1,
        i = n.join("-");
    return {
      hash: e,
      index: o < 1 ? 1 : o,
      gallery: i
    };
  }

  function i(t) {
    "" !== t.gallery && n("[data-fancybox='" + n.escapeSelector(t.gallery) + "']").eq(t.index - 1).focus().trigger("click.fb-start");
  }

  function a(t) {
    var e, n;
    return !!t && (e = t.current ? t.current.opts : t.opts, "" !== (n = e.hash || (e.$orig ? e.$orig.data("fancybox") || e.$orig.data("fancybox-trigger") : "")) && n);
  }

  n.escapeSelector || (n.escapeSelector = function (t) {
    return (t + "").replace(/([\0-\x1f\x7f]|^-?\d)|^-$|[^\x80-\uFFFF\w-]/g, function (t, e) {
      return e ? "\0" === t ? "�" : t.slice(0, -1) + "\\" + t.charCodeAt(t.length - 1).toString(16) + " " : "\\" + t;
    });
  }), n(function () {
    !1 !== n.fancybox.defaults.hash && (n(e).on({
      "onInit.fb": function onInitFb(t, e) {
        var n, i;
        !1 !== e.group[e.currIndex].opts.hash && (n = o(), (i = a(e)) && n.gallery && i == n.gallery && (e.currIndex = n.index - 1));
      },
      "beforeShow.fb": function beforeShowFb(n, o, i, s) {
        var r;
        i && !1 !== i.opts.hash && (r = a(o)) && (o.currentHash = r + (o.group.length > 1 ? "-" + (i.index + 1) : ""), t.location.hash !== "#" + o.currentHash && (s && !o.origHash && (o.origHash = t.location.hash), o.hashTimer && clearTimeout(o.hashTimer), o.hashTimer = setTimeout(function () {
          "replaceState" in t.history ? (t.history[s ? "pushState" : "replaceState"]({}, e.title, t.location.pathname + t.location.search + "#" + o.currentHash), s && (o.hasCreatedHistory = !0)) : t.location.hash = o.currentHash, o.hashTimer = null;
        }, 300)));
      },
      "beforeClose.fb": function beforeCloseFb(n, o, i) {
        i && !1 !== i.opts.hash && (clearTimeout(o.hashTimer), o.currentHash && o.hasCreatedHistory ? t.history.back() : o.currentHash && ("replaceState" in t.history ? t.history.replaceState({}, e.title, t.location.pathname + t.location.search + (o.origHash || "")) : t.location.hash = o.origHash), o.currentHash = null);
      }
    }), n(t).on("hashchange.fb", function () {
      var t = o(),
          e = null;
      n.each(n(".fancybox-container").get().reverse(), function (t, o) {
        var i = n(o).data("FancyBox");
        if (i && i.currentHash) return e = i, !1;
      }), e ? e.currentHash === t.gallery + "-" + t.index || 1 === t.index && e.currentHash == t.gallery || (e.currentHash = null, e.close()) : "" !== t.gallery && i(t);
    }), setTimeout(function () {
      n.fancybox.getInstance() || i(o());
    }, 50));
  });
}(window, document, jQuery), function (t, e) {
  "use strict";

  var n = new Date().getTime();
  e(t).on({
    "onInit.fb": function onInitFb(t, e, o) {
      e.$refs.stage.on("mousewheel DOMMouseScroll wheel MozMousePixelScroll", function (t) {
        var o = e.current,
            i = new Date().getTime();
        e.group.length < 2 || !1 === o.opts.wheel || "auto" === o.opts.wheel && "image" !== o.type || (t.preventDefault(), t.stopPropagation(), o.$slide.hasClass("fancybox-animated") || (t = t.originalEvent || t, i - n < 250 || (n = i, e[(-t.deltaY || -t.deltaX || t.wheelDelta || -t.detail) < 0 ? "next" : "previous"]())));
      });
    }
  });
}(document, jQuery);
"use strict";

/*  jQuery Nice Select - v1.0
    https://github.com/hernansartorio/jquery-nice-select
    Made by Hernán Sartorio  */
!function (e) {
  e.fn.niceSelect = function (t) {
    function s(t) {
      t.after(e("<div></div>").addClass("nice-select").addClass(t.attr("class") || "").addClass(t.attr("disabled") ? "disabled" : "").attr("tabindex", t.attr("disabled") ? null : "0").html('<span class="current"></span><ul class="list"></ul>'));
      var s = t.next(),
          n = t.find("option"),
          i = t.find("option:selected");
      s.find(".current").html(i.data("display") || i.text()), n.each(function (t) {
        var n = e(this),
            i = n.data("display");
        s.find("ul").append(e("<li></li>").attr("data-value", n.val()).attr("data-display", i || null).addClass("option" + (n.is(":selected") ? " selected" : "") + (n.is(":disabled") ? " disabled" : "")).html(n.text()));
      });
    }

    if ("string" == typeof t) return "update" == t ? this.each(function () {
      var t = e(this),
          n = e(this).next(".nice-select"),
          i = n.hasClass("open");
      n.length && (n.remove(), s(t), i && t.next().trigger("click"));
    }) : "destroy" == t ? (this.each(function () {
      var t = e(this),
          s = e(this).next(".nice-select");
      s.length && (s.remove(), t.css("display", ""));
    }), 0 == e(".nice-select").length && e(document).off(".nice_select")) : console.log('Method "' + t + '" does not exist.'), this;
    this.hide(), this.each(function () {
      var t = e(this);
      t.next().hasClass("nice-select") || s(t);
    }), e(document).off(".nice_select"), e(document).on("click.nice_select", ".nice-select", function (t) {
      var s = e(this);
      e(".nice-select").not(s).removeClass("open"), s.toggleClass("open"), s.hasClass("open") ? (s.find(".option"), s.find(".focus").removeClass("focus"), s.find(".selected").addClass("focus")) : s.focus();
    }), e(document).on("click.nice_select", function (t) {
      0 === e(t.target).closest(".nice-select").length && e(".nice-select").removeClass("open").find(".option");
    }), e(document).on("click.nice_select", ".nice-select .option:not(.disabled)", function (t) {
      var s = e(this),
          n = s.closest(".nice-select");
      n.find(".selected").removeClass("selected"), s.addClass("selected");
      var i = s.data("display") || s.text();
      n.find(".current").text(i), n.prev("select").val(s.data("value")).trigger("change");
    }), e(document).on("keydown.nice_select", ".nice-select", function (t) {
      var s = e(this),
          n = e(s.find(".focus") || s.find(".list .option.selected"));
      if (32 == t.keyCode || 13 == t.keyCode) return s.hasClass("open") ? n.trigger("click") : s.trigger("click"), !1;

      if (40 == t.keyCode) {
        if (s.hasClass("open")) {
          var i = n.nextAll(".option:not(.disabled)").first();
          i.length > 0 && (s.find(".focus").removeClass("focus"), i.addClass("focus"));
        } else s.trigger("click");

        return !1;
      }

      if (38 == t.keyCode) {
        if (s.hasClass("open")) {
          var l = n.prevAll(".option:not(.disabled)").first();
          l.length > 0 && (s.find(".focus").removeClass("focus"), l.addClass("focus"));
        } else s.trigger("click");

        return !1;
      }

      if (27 == t.keyCode) s.hasClass("open") && s.trigger("click");else if (9 == t.keyCode && s.hasClass("open")) return !1;
    });
    var n = document.createElement("a").style;
    return n.cssText = "pointer-events:auto", "auto" !== n.pointerEvents && e("html").addClass("no-csspointerevents"), this;
  };
}(jQuery);
"use strict";

/*Copyright 2010-2018 Simplemaps.com
html5countrymapv3.81
Use pursuant to license agreement at https://simplemaps.com/license */

/* shifty (tweaked to avoid AMD conflict) - v1.5.2, Copyright (c) 2013 Jeremy Kahn, MIT license   http://jeremyckahn.github.io/shifty */
eval(function (x) {
  var d = "";
  var p = 0;

  while (p < x.length) {
    if (x.charAt(p) != "`") d += x.charAt(p++);else {
      var l = x.charCodeAt(p + 3) - 28;
      if (l > 4) d += d.substr(d.length - x.charCodeAt(p + 1) * 96 - x.charCodeAt(p + 2) + 3104 - l, l);else d += "`";
      p += 4;
    }
  }

  return d;
}("(function () {var root = this || F` 9#(\"return` 4!\")();var Tweenable = ` Y.formula` E!DEFAULT_SCHEDULE_FUNCTION` 1)EASING = \"linear\"` 1)DURATION = 500` 6!UPDATE_TIME = 16.66666666` \"\"8` @!_now = Date.now ? ` \"%: `!i)`\"=#+ new` @!;}` ]!` [\"typeof SHIFTY_DEBUG_NOW !== \"undefined\" ?` 1.:`!@!;if (` _#window` L,) {`\"q5 =` N#.requestAnimationFrame ||` 8$webkitR` ';oR` \";ms` C<mozCancel` 32&&` >'` S5setTimeout;} else`\"F:` C(`$T%noop() {` (&each(obj, fn`&i#key;for (key in obj) {if (Object.hasOwnProperty.call` ]\"key)) {fn(key);}}` ~&shallowCopy(targetObj, srcO` z!`!C!` (\",`&B'prop) {` I%[prop] =` R#` )\";});`)/$` A$`\"J'defaults`!1#`!0!`!''` |/`&U'` N\"`!/$`&U-` 1* src`!K$})`!8'twee`#,!s(forPosition, currentState, original` (#` v\"` &#dur`%J!, timestamp, easing`$P#normalized` x$ = `!#' <` Q& ? 0 : `!A( -` 5&) /`!(%`*4!prop` $!`!,\"`%B\"P` '*Fn`%p\"prop in`\"/)`#M#` '(`%w+`#u\"`$2!` t* =` y#`#R#`!%$`+_&` @.== \"`#t$\" ?` 0.:`.M$[` +,];`!Y(`$p%`$X%(`$8)` :\"`$>)` +$`!`$,`$&/);}}`'*#`!\"(`%]0(start, end` k%unc, p` d$`.f%` E! + (end -` )\") *` K'(` L%`! 'applyFilter(`!2!able, f` -!Name`1_$` ,!s =`2%&.prototype.` 8\"`%J!args`#+$` A!_` 7\"Args;`)1!` d#`).(n`!#\"`)/'` >#[name][`!E&]`/z/` 35.`\"9!`\",(args)`)Y\"var`'o!outHandler_endTime`\"#!` ,+`$C#` '4isEnded` '0offset;`$s&` 2)`!Q(`*1'delay`*L(`*bF`%+\", step, schedule, opt`!z(Override) {`\"P2 =`+''+`!L\" +`*p&`\"g6 = Math.min(` 3 || now()`\"P\"`!,.)` p,`#[#`!O#`!\"3>` --`$k$`$ 1 =`\"&% - (`\"Q3`-U\"` |2);`'/!`'d%isPlaying()`'H$`!r1) {step`1N#`$_$`(J&attachment`\"p-`!u\");` D&stop(true);} else {` `'`%:$Id =`%F%`!h'_` r*, UPDATE_TIME);`*i3\"before`*g!\"`\"^#`#}6`10(`&##)`!]#`2q\"1`'5H1, 1`2U%`\"D*` h\"`!06`35[`!u$`!3'`\"h4after`\"w$`%&!`!,*`$^J}}`/(&composeE`1p'(from`!(!Params`!W%`/6\"` K#d` M\" = {}`,C\"`.F!` -%`3D)`$m\"` 4)== \"string\" ||` O+`3h)`4d!ch`!E.`/h&prop) {`!M*`3h%`!=#}`$u&` ICif (!` a0` l<` (#|| DEFAULT_EASING`0F\"`4G$` U)`3;'`2k%`-+!initi`&,%`-=!onfig) {this.`&j$` <! =` =!` H( || {};` E#` V!ur`-6!false` 2#`*#$F`!=$=`!r%SCHEDULE_FUNCTION`$c'`!E'`38/` v!setC` <!`/8\"`!v\";}}`\"=%.prototype.`'C! =`#'`\"C)`!3!`!j!is` Z!`&p\"`#I#this;}if` I(`!P!`!K% || !`\"Y,`!I;` I\"`)k&=`1$\"`#/$tart`!N\"get`1<!` 3!`)<&);`!T'.resume();};`\"L0`!;%`\"W)`\"W%`\"-#= ` \"#`$a5tru`$q$`!@&` O%.` *&` >#pausedA`3h$null`%I+`/k!` -'`4[\"` i%` )\"|| 0`\"m(` 6&` )\"`4B!op` :%ep` 6(ep` 2+finish` 9&` )#` 7*`3j%`!B&` *$`)4'DURA`'6!`(>1shallowCopy({},`!'%rom`,7!`$v%`%/$`/]) =` 7'` :#`/q'` f6to` n,var self` l#` e$`/:)`%N)) {` 0*(self,` `!`'+'` )$`1)#` &#`1J%` *\"`0e*` .\"`2$+` p#`2-(` -\"`-1\"` &$ste`!*%`+I,);`0I\"`#t+`$&.`0o\"`#;)`#K-;defaults(`!G)` [(`$*$`!\\\"`%a!`2@.`\"M*`$8#` K#`/4-` j$f`4C!Args = [`4\"+`%U.,`\"!(` :$` ~\"];applyF` l!`+I!, \"`-G!Created\"`+9)`+&3get`%L,` N#`&P,`#[.`+v7` g)s` H!`1#3` 8!` R3`+]!`'*-`+h1`.4'isP` 8!`,k$`\"C>`.;\"` y,`0M(` l\"`!5%`/P&+`!5# -`\"z#`!M(`0!$`!G'`33(`1F&`.P*`)N+`$C@seek`!s)millisecond) {` #' = Math.max` 9(, 0`+D\"`$8#`#K)`\"`&`\"N' ` h)== 0`3F,`2T.` q(-` U(` ~!`3`\"isPlaying()`#g%`\"r4`#G.`#-*`(0\"`!9+` )$`,q#`0$*`'T0`)B4`+I,`)R*` &$`,r\"`2k*`,p$`+m%Tim`+m$`%|!();}`$s?top`%))gotoE`%-!`#%/`&M*`&]1`0D-`3/!(root.cancelAnim`#$!Frame || ` 8!webkitC` '8oC` \"8ms` @9moz` :\"Request` 03clearT`!\")`',#`#l$Id`'C\"`\"z&`-m/before`#0!\");`.)!Props(1`${Z1, 0`%Q*)`/+0after`!6$` #9End\"`0j&nish.call`'e)`!W0attachment`%}A`)D%`0.3`&6-&& `*!\"`&:%`0+6S`'+`0G*` .+`'K%` ), = ` #,` |3dispo`0_.var prop;for (prop i`\"4\"`/y(hasOw`%H!erty` B!)) {delete` I![prop];}}`!(2f`$e! = {` )3ormula = {linear:`!\\&pos`#j&pos;}};` F&` S7;`4Z(` ?%, {now:now, each:each, `'e&:`'p&` ,'` ,&, `&r':`&~', `!&':`!2', defaults:d` \"#, composeE`(,!Object:c` \".}`1;#ypeof SHIFTY_DEBUG_NOW`17!\"`#$$\") {`*f!`,Z-` #*;}` A!`\"j%`#;(`3h$` '&})();(`%i)`#k&`#N1`#y., {easeInQuad`$d4`46!pow(pos, 2);}, easeOut` ;8- (` N( - 1, 2) - 1` Z%In` M4if (` R!/= 0.5) < 1` r&0.5 *`!F/`!3$` :\"` [\"-= 2) * `!>\"`!u&InCubic`\"0B3`\"J(` /E`\"N\"3) + 1`\"F)` L2`\"\"N3`\"J&`\"L#`!0+2`!8#`\"K(Quart`\"1B4`$~+` >6`$y14`$x2` S/`\")N4`$p;`\"y,`%-+Quin`\">C5`\"W*` 1C`\"\\\"5`%*.` L2`\"/N5`$y?`!:!`\"K(Sine`$I6` \\!cos` \\!` i$PI / 2)`!|)Out` N8` S!sin` S1`\"`)`!39`\"+(cos` ^%`*'!`&+,Expo`1C7`.m!0 ? 0 :`#?&2, 10 *` J!` k!`$(` N@1 ? 1 :`#4$` m#-` p!` L!`%.,` `1if`!<\"`!_!`%+';}` .(`%E'1` 4\"`%HE`\"40`%n*`!m0--`!|#`-h)ir`-#5`*]$sqrt(1 -`#\"!`$;/Out` K8` V*`0Q0`%w*` W1`\"b:`&/)`!U6`(m/` D%`0m,`%'!`\"7(Bounc`'L.`$k$< 0.3636363636` \"\"5`!X&7.5625`1q#` |!;} else ` U(727272727` \"\"3` M/`!`$0.5454545454` \"\"`2n$+ 0.75` k0909090909` \"\"`#6'` l/818181818` \"\"`3l%+ 0.93` w&` H79`!S*6` V)84` `!`%1%Back`#M-var s = 1.70158;`)e'`#C$((s`$F!` *#- s`$I)` KG`)U\"` _#` h%` q-+ `*V/` cA`*!@`.>#`!2$(s *= 1.52`16\"`\";(`'A-`'4,` E9`\" #`*f#lasti`*P61`+s(4, -8`(>$` 1#sin`!9\"* 6`#6$(2` 6$PI)`0I!`#/$swingFromT`-l.`$!8`#-' ?`\"^O :`\"I^`!s%`&Bg` o!`\"=K-`0^!`&a?b`+W~`+W~`+W~`,&O`#T\"Past`\"\\~`#H22 - (`#2M)`#6J` m4`#I<` ~%` I<`#M>)`0@%`*3`-w@`,X%pos, 4`-u&-`*a0` @*3) -`-d$`!?#`-U4` u.` N\"`!c/` =10.2`\"K!);})();(` K&) {` $%cubicBezierAtTime(t, p1x, p1y, p2x, p2y, duration`+H#ax = 0, bx` \"\"c` )#ay` 1#y` 1#` #!;`!'%sampleCurveX(t`!r&((ax * t + bx)` ##c` $\";}` K0Y` P+y` W$y` V%` $\"` I2Derivati`!8,3 * `!B%2 *`!B)` _(olveEpsilon(`\"r'` a#1 / (200 *`#0&` N,(x, e` \\\"` O&`\"5)` B!`#&#` B'` ^(fabs(n`'4#n >= 0` c&n`(K,0 - n;}`!:+` p.`$\"t0, t1, t2, x2, d2, i;for (t2 = x, i`$z! i < 8; i++) {x2 =`$z+2) - x;if (`!l!x2) <`\"K.t2;}d` O+`$<)2)` Z&d` _!0.000001) {break;}`!V!t2 - x2 / d2;}t0`!b!t1 = 1;`!v\"` d!t2 < t`#''t0;}` 2#> t`*Q't1;}while (t0 <` 6\"`\"51`\":( - x`\"15if (x > x2) {`!^!t2`$H%`!g!t2`\"(#(t1 -`!d!*`,!!+`!d!` a'`);!3 * p1x;`)P!3`-W!2x - p1x) - cx;`)r!1` '! - bx;`)[!` R\"y;`)p!` Q#y` S!y` S!y;`*2!` S!y - by;`'&$`'C!t,`'x3`&}(getC`+c&Transition(x1, y1`&<\"y2`\"y&`,o3`,@.`-*!` X*, 1);};}Tweenable.set` O\"F` o$=` w'name` T,`'~#`!%'`!k& =`!gE;` L1.displayName = name` 23x1 = x1` '3y1 = y` &4`&{!x2` D42 = y2`$d$`#\"&prototype.formula[name] =`\"W2;};` R&un`#H>) {delete` r>;}`1b9getInterpolatedValues(from, current, targetState, po`!o\"`3H!ing, delay`&6&`!I&tweenProps(` L&` m%` \"` o)1` f#` t$);}var mock` m% = new` |&;` 1)._filterArgs = []`#O'i`\"?&`#E)`!6/`\"3.opt_`\"A$`'P\"`\"\"! =`\"E'shallowCopy({}`\"<\");var`\"0\" =` \\& || 0` 6!` {\"Objec` `*composeE` 5'`!d#` M\" || \"linear\")`\"W+set({}`!6\"`\"d)`\"t5;` =&.length`/m!` ,&[0`':\"`\"R!` ,(1] =`\"F!` )(2] =`#G(` 0(3] =`\"J)`$9'applyF` G!(`!_), \"`&*!Created\")` .Cbefore` -!\"`\"j\"`%<'`'i#`,'\"`'I[`\"%\"`(&$`!7Cafter`!U$`(Y#`!N.`)q/` \\%`&h#formatManifest`\"C!R_NUMBER_COMPONENT = /(\\d|\\-|\\.)/` ?#FORMAT_CHUNKS` >![^\\-0-9\\.]+)/g` @#UN` C\"TED_VALUE` G![0-9.\\-]+` ?%RGB`)\\#RegExp(\"rgb\\\\(\" +` Q1.source + /,\\s*/` &&`! 0.source + /,` \"H\"\\\\)\", \"g`%>#`!c!_PREFIX = /^.*\\(`\"j$HE` /!#([0-9]|[a-f]){3,6}/gi` ?!` z!_PLACEHOLDER = \"VAL\";`..(F`$0!ChunksFrom(raw`$l\", prefix`$W#accumulator`,O\"var ` D%L`)G$` )%`)[#`'I\";for (i`)j! i <` I,; i++) {` z'.push(\"_\" +`!B# + ` )\"i);}`&b#` F';}`\"&.String`\"3!`&X\"ted` .\"`-[$`\"T! =`&v#` 6%.match(`&K+);if (!` O\") {` U%[\"\", \"\"];} else if (` 7\"`,@%== 1 ||` x-charAt(0)`!*%`(#,)`!!%.unshift(\"\"`\"Y&` 4#join(`%#-)`\"k'sanitize`*`\"ForHex`2T\"stat` 0#) {`*m&each` 1(,`1G'prop`0z*Prop = ` E'[prop]`#0!typeof` =*== \"s`\"a!\" &&` 0(`\"c%HEX)) {` c- =`\"&%Hex`'9\"ToRGB(` [');}}`\"M0` C+str) {`#G#`/~\"`$6\"` ?\"`!@\", str, convertHex` W!` r(` ,+(hex`&>)rgbArr = h` ;#Array` <'`.5$\"rgb`,&!` L\"[0] + \",` ''1]` \",2` -!)\";}var` t*_` q\"` '!`)t\"`!j%`!9-) {hex`!^\".replace(/#/, \"\"`'w\"hex`'G(3` E)spli`&w\"` +%`!x\"hex`\"!\"hex`!x\"hex`\"!\"hex`!x\"` #\";}`!d5[0]`#6$Dec`!E!substr(0, 2)`!0!` C31` B42` :<2` B44` T\"`$M#` H5`%V'` Z(`&G&parseInt(hex, 16`&'(`&Z/pattern, un` 8\"`+A$,` G#`&G#` C!nMatches =` >-`({#` h#`1+\"` 4*` A0`%K$`!=%`+^/if ` :\"`!6$`!>0`0G%` ))`0H(`*!#`\"R!`0Y\"`0e!`0Y&` ]/`0_$` P(` s-`-|\");`\"=-`\"53`\"2-`#b$`+c$`!\"!));}`.j$` [*`+n/RGB`$h#`1D.`+l8RGB,`0Q,,` e-`,0` 4$(rgb` ?\"`$/\"numbers = ` 1$`.R%UN`2V\"TED_`\"_!S`%`\"` L#`$Y%` )#`$P(`!.$`#R&` m-RGB_PREFIX)[0]`$l1` |)`$s$` k,+=`(X&` J#[i], 10)`-^\";}`!<.` #+.slice(0, -1` Q!)\"`*0$` ;+`#O'getF`$%!Manifest`37,var m` 5#Accumulator = {};`2b~`3R.`1@$awValu`*q!get` &\"From`3-*`!h/`3r%{`&^\"`\"n\":`\"a%` *\"` Z-, chunkNames` C&`'O\"` H!`!I%, `\"M!}`4S!`#{#`!5/`#{'expand` l\"tedPropertie`$ )`(L$`$A%) {`#r+` 2+`#NS`#4G` >)`(c%` )%`(f$`($0` H+`(1%`!D'`\" +`!Y\".`#}&[i]] = +` b&[i];}delete`\"&/}`+H(collapse`\"t~`#LF` d\"`&.\" = extrac` N!erty`-z#`!A8`\"r-`$=\"v`#c\"ist`$c(List`!v#` u\"` K@`!y*`'%ted` o\"`\"^,` \\#`((,`!E');`\"Y-`,o'`0x&`&I)`$[)`\"b?`!u'`$&\"` M#`!l$`,g\"`4?,Name` P(`'V%` )&`'D8` I,`'Y$` s,` d)[i];`!N+[` A,`\"|!`#*'` /-;`'n/` 3.}`,3#`! +;}var`%V*_a`/k)[];`0S(`&!'`#[6` W5`#;#`#2!`#|!`\"l`! 6pus`1f)[`+K*)`\"y%` J5`340`'y,`'k$`,:%`&\\#` 8\"` F$` @\" =` 0#`4@#`-Ln` y9` )*.replace(VALUE_PLACEHOLDER,`.C+.toFixed(4)`#+&` _0`#\"*`0i'` D%` B\") {` V,`!M#match(R_UNFORMATTED_`![!S`*e*pandEasing`$v\"(e` \"', tokenData`/;.` 2%`/\"A` E%`/:'`%}& =` C(`-|'` >&`'K7`,D!`!r! =` \"#`0@-i;if (typeof` D%== \"s`#+!\"`-($` 5!`0n&` *!.split(\" \"`0?\"last`#.\"` D!` ?%` Q\"[` X(`)n$- 1]`&Y\"`):,`&I*`!q)`4O-` z)i] ||`!<,;}} else`'<!` I`;}}`-^#`#F/`0r)collapse`%,~`%,~fir`#O$`&#,`\"}'0]`!4\"`&0\"` I\"s =` )#` Y(`&R'` >%`&K.compose`)I#`+k%\"\"`/.6`3#+` M0+= \" \" +`!n5i]`2`%` '8}`%/. =`!_1.substr(1);`&f$` G1`\"u(}});}`%J&prototype.filter.`%-! = {tweenCreated:`%a&`%*#State, from` $#to` \"#`!7() {sanitiz`2,#ForHexProps` ^));` .6` |%` )8`!8#);this._`'0% =`2`&Manifest`!+,}, before`\"s!`!qX`/F>`!P*);` L\"`4A%Propertie`!V*` ,I`!\\(` ,F`\"*%` C-}, after`\"=]`,R$`!{O` ?8`\"48` <8`\"86`-@`\"u-};})(`(q%);}).call(null);"));
/* Raphaël 2.1.2 (tweaked, always global)- JavaScript Vector Library, Copyright © 2008-2016 Dmitry Baranovskiy (http://raphaeljs.com), Copyright © 2008-2016 Sencha Labs (http://sencha.com), Licensed under the MIT (http://raphaeljs.com/license.html) license. */

eval(function (x) {
  var d = "";
  var p = 0;

  while (p < x.length) {
    if (x.charAt(p) != "`") d += x.charAt(p++);else {
      var l = x.charCodeAt(p + 3) - 28;
      if (l > 4) d += d.substr(d.length - x.charCodeAt(p + 1) * 96 - x.charCodeAt(p + 2) + 3104 - l, l);else d += "`";
      p += 4;
    }
  }

  return d;
}("(function (glob, factory) {glob.eve =` -$();})(this, ` N&) {var version = \"0.4.2\", has = \"hasOwnProperty\", separator = /[\\.\\/]/, wildcard = \"*\"` }! =` z*}, numsort` -)a, b) {return a - b;}, current_event, stop, ` (!s = {n:{}}` ,!` `)name, scope) {name = String` 7!);var e =` d#, oldstop =` |#args = Array.prototype.slice.call(argum` W\"2), listeners` o\".` '%`!+\", z = 0, f`$&!lse, l, indexed = [], queue =`\"\x7F!out` /#ce =`\"X,erro`!&![];` .) = name;`\"&#0;for (var i`!:\"ii =`!c&.length; i < ii; i++) {if (\"zIndex\" in` F&[i]) {`!o#.push(` 0(.` N\");if ` %0 < 0) {`\"C![` -/]`!P([i];}}}`!%$sort(`%s#);while (` 6#[z]` t#l =`#A\"[` 2%++]];out`!h#.apply(`%X!`%\"\")`!j\"stop) {`#3#`%J#;`&j#out;}}`#H!`#D!`#\",l`!j,`#2-`#P#l`\"c$==`%6$[z]) {`!8Fbreak;}do {z++;`\"@/]];l && ` CM} `#R#l);} else`$E%`$7(;}` 5$` PN}`#R+`'/,ce`#i'`&y# ?`(+!: null;};eve.` K\"`)(#nts` 0!`):(`*a*`,{#names`(>#.split(`,a%),`*f)item, items, k, i, ii, j, jj, nes, ` f![e]`)e&`)*\"`) (`!-!`(q3n` [\"` O#j` P\"jj = ` H'j < jj; j` N!`!]!s[j].n;`!W!`!=![`!!![i]], e[`.^$]];k = 2`(F$k--) {item =`\"3\"[k`'$\"item`!J\"`$h\"` +!;`\"/\"out.concat` 2!.f || []);}}}`#F\"es;}`(<(`$&!`0b!`.c,f`.X3if (typeof f != \"` Q$\"`0*&`0X*;}`$BI`-22`$ 8` W!.n;` \"\"`2R*`!q!`-G\"&& `#s' || (` $(`1\\$);}e.f` f!`#9#`%X.e.f`.U7e.f[i] ==`#@!`\"j&;}}e.f`$Q\"f)`(T$`##&`.l#` _\"+ `+t&` $$) {f`,,%` -%;}}`$b#`1m!` f%`#J!`(f#att`1>#`2b31`!=/) {eve`+'#null, [`2/#null]`&D$` x!)` &$` j50)));`!c$`+T#`! )` -#1` <#`+_!` 7&sub`*w#`,F!` %%`!p#(new RegExp(\"(?:\\\\.|\\\\/|^)\" + ` H# + ` 0($)\")).test(`,o))`(<%` *)`!O%`,F*`!:&` A)`'T-`)&$f`&8!ve.unbind`)%3if (!`\"<#`-q0`''%`$Q#`(`B, key, splice`-Y,cur`,>!` {!`-1K`-M&`-A\"cur`-L'+=`!,#`0>$- 2) {` .\" = [j, 1]`)|!cur`-l#f`\"i\"s[i] != `-c$`(}$`)q&` a%`(u\"` .(`2U&`!\\!key in `%}$e[has](key)` S-key`-s\"cur`#E!c`(\"$cur`#:$);}}`\"v-`\"O(`,7.cur[i]`/`$e.n`!E#`%,$e.f`#B)`0~$`+g'`0y*`+j$j`+i%e.f`!k#(j`*5!break;}}!` \\& && delete e.f;`\" \"`\"w$`!L%e.n`\"y&`-|!.n`\"u!`!`!var`'6!`&p!` 0%`2p.` ?!`!c7` ;!`!u'` J\"`!l2` `(`!w)`!6%}`%*%`\"5'`!kP` f1`1$$}}`34$`'<!`)n0`\"z!2`*{,`*O&` C$2`.Q&`&;#this, `-r%);`*=$`+2!on` O'`!@\"versi`4b!` ##` 1!to`4T\"`,73\"You are running Eve \" +` [%`!*(;});`$D!` `\"glob, factory) {` ##` 1#glob.eve);})`\"&#` Q&window, eve`%%#` 3!R(first`$3#R.is` +\", \"` <$\")`!s&loaded ? ` @!() :`\"t$\"raphael.DOMload\",` >\");`%Q#` p,arra`*\\!` t#R._engine.create[`$0!](R` b#`&r$0, 3 + ` a&[0], nu))).add`\")#`!.$`4!\"rgs = Array.prototype`2w5`-?!` {!args[arg`'r%- 1]`\"k,`&L! =`!!!.pop(`&0%`#$&`! \"`\"(7args)`#0;`&!(` JD;}`\"a&`#G>`'p(}}R`'V'\"2.1.0\";R.ev`)H!ve;var`\"<#, `2F% = /[, ]+/, ele` i! = {circle:1, rect:1, path:1, ellips` 6!tex` 6!image:1}, formatrg = /\\{(\\d+)\\}/g, `$b! = \"`$g%\", has = \"hasOwnProperty\", g = {doc:doc`\"<!, win:`(.\"}, oldR`#j\" = {was:Object`%c&`,L!`#n\"g.win, \"` K#\"), is:` 1!.` -#}, Paper`*S,this.ca = ` $\"ustomAttribute`##!};}, paper`!7!, appendChild = \"` $'\"` 8!ly` 0#ly\", concat = \"` $\"\", supportsTouch = \"ontouchstart\" in `!w! ||` #\".D`\"~#` K\"&& g.doc instanceof ` 5), E = \"\", S = \" \", Str = `-A\"`4O\"`!N!` $!\"`+\x7F!`%P\"\"click dbl` #\"mousedown` $\"move` $\"out` \"#ver` $\"up `!}&` %\"` K!` %!end` #\"cancel\"[`!6!](S),` 2\"Map = {`!(%:\"`\"i',`!5&` 1#move` /$up` .#end\"}, lowerCase`\"M\"`+V'toL` 4$, math = Math, mmax =` /!.max, mmin` (%in, abs` ($abs, pow` ($pow, PI` '$PI, nu = \"number\", s`19$\"` $\"\"`.H#`%e!rray\", `1\\'\"` $$`/0!ll` /&fill\", o`()!T` H&`(/,`2L%`'5#`'E!, pus`&M!push\", ISURL = R._` #$/^url\\(['\"]?([^\\)]+?)` )!\\)$/i, colourRegExp` K!\\s*((#[a-f\\d]{6})|` $&3})|rgba?\\(\\s*([\\d\\.]+%?\\s*,\\s*[\\d\\.]+%?\\s` \"*(?:` #,)?)\\s*\\)|hs` `,(?:deg|\\xb0|%)` ^;(?:` #,)?)` .!` x!l` $u)\\s*`#>!isnan = {NaN:1, Infinity:1, '-` &$'`.K!bezier`.J\"^(?:cubic-)?` 2\"\\(([^,]+),([^,]+),` %'\\)]+)\\)/, round`&w$` (!, set`- %`&t!` %'`&i!Flo`,T!parse` (!, toIn` +%Int, upp`(t5U` 4$, availableAttrs`&M\"` #-{'arrow-end':\"none\", ` -#`*d!` ,&blur:0, 'clip-rect':\"0 0 1e9 1e9\", cursor:\"default\", cx:0, cy:0`(q\":\"#fff\", 'fill-opac`#r\", font:\"10px \\\"Arial\\\"` D!ont-family':\"` ).size':\"10` (&tyle`!t!rmal` .%weight':400, gradient:0, h` 4!` %!ref:\"http://r`2#\"js.com/\", 'letter-spacing':0, `!{#`4d%\"M0,0\", `#&!r`\"Y!r`\"Y!src:\"`,2\"oke:\"#00`!m!` *\"-dash`,2!'` A!` /$linecap':\"butt` *+join` (.miterlimit`!\\!` /$`#_(` ,$width` -!target:\"_blank\", 'text-anchor':\"middl`' !itle:\"R`\"v\"\", transform`!s!` m!:0, `\"U!y:0}`&](nim`&X1` -'{`&B!nu`&9+csv`&(\"nu`&*!nu`&($`-W\"`& .` ^!`%V'nu`% %nu`$N&nu`$Q$path`$T!nu`$U!` \"!` ;\"`$L$`!\"&`#E,`!)!`#H*nu`\"p)` \"%`\"}%nu, `!!\"y:nu}, whitespace = /[\\x09\\x0a\\x0b\\x0c\\x0d\\x20\\xa0\\u1680\\u180e\\u2000\\u2001` '!2` '!3` '!4` '!5` '!6` '!7` '!8` '!9` '!a\\u202f\\u205f\\u3` g#2` ?!29]/g, commaSpaces` [~`!1H*,` J~`!#E/, hs`0U!{hs:1, rg`0n!p2`#1!,?([achlmqrstvxz]),?/gi`&\\\"Comma`0O!/` ?#rqstvz])`!?~`!zC,]*((-?\\d*\\.?\\d*(?:e[\\-+]?\\d+)?` f~`%+F` K~`!#F)+)`%!!t`$v)rstm`#z~`#z~` f~`#z~`$:^pathValu`-Y\"`#.7`$0~`\"H~`-&rgi, radial_gradient = R._` #./^r(?:\\(([^,]+`\"R~`0M~` Wq([^\\)]+?)\\))?/, eldata = {}, sortByKey = function (a, b) {return a.key - b.key;` L%Number` =7toFloat(a) -` $%b);}, fun` L))`!H!pipe` *)x` c&x;}, rectPath`%K#` $&` H', y, w, h, r) {if (r` ]&[[\"M\", x + r, y], [\"l\", w - r * 2, 0` 0!a\", r, r, 0, 0, 1` )\"` H%0, h` L$` 94-` E*` G! - w` i7` N!-` z*` P$h`!>7` K$z\"]];}`\">+`\"<)`!5$`!}$` $%`!N'` `\", ellipse`#N3rx, ry`#\\$y == null) {ry = rx`!05m`!6\"- r` *\"`\"/!` i!`\"+$1` $!2 *` #<-` 9'`!o$ge`%['` #&{path:`!|&e`!f!`!\\\"el.attr(\"path\"`&\x7F!circle` @,var a =` I$s;` X%`#\"%(a.cx, a.cy, a.r` i!` 9#` ,_` '!ry` q!rect` KD`(G$(a.` ^!` i!width, a.height`!f&imag`!EE` W@` q!tex`![1bbox` r\"_getBBox()` l-bbox.x,` H!.y,` \"\"` \x7F#` '!` ~'se` &~;}}, map`&C%` \"&`!1&path, matrix`(D#!` %%`!3#path;}var`(=!, i, j, ii, jj,` 9!i;p` s\"path2curve` u!);for (i = 0, ii` ;#.length; i < ii; i++)`'z\"` ;$[i]` V\"j = 1, jj` 2#i` T%j < jj; j += 2) {x =`\"##.x`!:!i[j]`!Z#[j + 1]);y` ;&y` ,5` 0$ = x` %$` 6! = y;}`+0$`\"s\";R._g = g;R.ty`0>!g.win.SVGAngle || g.doc.implementation.hasFeature(\"http://www.w3.org/TR/SVG11/f` >\"#BasicStructure\", \"1.1\") ? \"SVG\" : \"VML\";if (`!C$=` .\"`&9#d`!W!doc.createE`!H\"(\"div\"), b;d.innerHTML = \"<v:shape adj=\\\"1\\\"/>\";b = d.firstChild;b.style.behavior = \"url(#default#VML)`!S\"!(b && typeof b.adj`!a!object\")`&M&`#E%E;}d `/7\";}R.svg = !(R.vml`'T!`\"=*;R._Paper = ` #!;R.fn`%z!perproto` 2$.` )!`! #R` $&;R._id = 0;R._o` \"%is`(P)o,`\"#!) {` \\#lowerCase.call(` 8!`\"Q!`!Y%finite\"`\"<&!isnan[has](+ o);}` C)array` D'o instanceof Array`1}%` K%null\" && o =`2N# ||` 7%`#t#o` <\"!` //`$%$` Z&O` ,!(o)` :)`!W\" &&`!D\".is` #! &` \"+` Q#` p\"ToString`\"}\"o).slice(8, -1).toL`#<$()`!f$;};`#l%clone(obj`,G#`\"&$b`&!\"` @$\" ||`!m%bj)`\"C!` P\"`#B$bj`,o\"res = new obj.cons`(~!or`+{\"var key i` N!`!/#obj`$M\"key`'-\"s[key] =`!W&` ,!)`+$&res;}R.a`*n!`%{(x1, y1, x2, y2, x3, y3`!##x3 `$;#`)}#x = x1 -` I\" = y1 - y2`(x\"x && !y`\"=&0`%W%(180 + math.atan2(- y, - x) * 180 / PI + 360) % 360;} else`)H'`!y!`!h&`!c#-` 0&`!v*;}};R.rad`\"D)deg`!W&deg`!'\" * PI / 180`.0!deg` I)rad` L&rad`!i(`!g#;R.snapTo` L)values, ` #!, toleranc`)s!` $$`*[!is(` (%,`)a'?` J& : 10`.;#is` r%`(%!)`$;#i =`!&\"s`1d$while (i--`$o#ab` T$[i] -` I\") <=`!Q)`\"E#` @%;}}`$)$` .\" = +` 8#;`'E\"m`!4$ %` 1$if (rem <` c5 - rem`+t\"rem >` T# -` 4;`!4&`']%` +!;}`!J!`0\x7F\"UUID`#Z!` \")(`$6&uuidRegEx, ` %\"placer`!(&` A&` *&\"xxxxxxxx-xxxx-4xxx-y` '$` 4$\".r` g\"` k5.toUpp`+i$;};})(/[xy]/g,`!-'c`$t#r =`([\"random(`(U!6 | 0, v = c`, !x\" ? r : r & 3 | 8;`\"v$.t`-8#(16);});R.setWindow`'()newwin) {eve(\"raphael` B&\", R, g.win, ` E#;` *!`,f\"win;g.doc =` @\".document`'6#_engine.initW`!$!` #-(` T!`)\x7F!var toHex`!W)color`'K#`4)!`\"\x7F#trim = /^\\s+|\\s+$/g` ]!bod;try` ?\"`!K!`.R#ActiveX`/)#\"htmlfile\");` A!.write(\"<body>` -%clo`$P!bod =` j\".body;} catch (e) {` 8\"`&V\"Popup()`\"c%` H#`$u!ange = bod`'\"#TextRange();`\"T$cacher`'1'`\"[$`\"2!bod.style.` 2! = Str` <#`&a%trim, E)`\"m!`(f\"=`!8\".queryCommandValue(\"ForeColor\");` E$`+4\" & 255) <<`&\\\"` -$65280 |` <&16711680) >>> 16`&d$\"#\" + (\"000000\"`*$$`&u)`46$-6)`#H*` \\$none\";}})`0B%`-:$`&Y!`#D#Element(\"i\");i.tit`2^!\"Rapha\\xEBl Colour Picker\";i`#B#display =`!#$` q\"body.appendChild(i`$!?` l$`$5$` #!`\"f$` r\"defaultView.getComputedStyle(i, E).getProperty`$=#` c!\"`*)!`->$`!G!`!2#;}, hsb`#O$`(U)`#?'hsb(\" + [this.h, ` #!s,` \"\"b] + \")\"` g\"l` K>l` R7l` b'rg`!:<` ]\"ex;}, prepareRGB`4-*, g, b`*d#g == null &&`3P\"r, \"o`*8!\") && \"r\" in r` '!g\"` \"&b` &\") {b = r.b;g` #!g;r` #!r`1_\"` m1s`\" !`3}$clr`10!getRGB(r)` ]!clr.r` o!clr.g;b` ##b`2V# > 1 || g ` \"#b > 1) {r /= 255;g ` \"#b` *$`%,$[`\"_#]`#$!ackag`\"q4, o` o!*` l$` \"#b` *$`,-!gb = {r:r, g:g, b:b, hex:R.rgb`#l%, `$Y$:`$b'};`\"s!o, \"finite`#t\"(rgb.opacity = o)`'x$rgb;};R`(4%`(T'lr`2V$gb`0W#is(cl`$e-h`$Y!cl`$f\"s`$g!` &$`$t\"` f\"`\"4\"R.hsb2rgb` }!;`$6! = rgb.r` )!`%6!gb.g` )!`%L!gb.b` )!h`*F!rgb`&t\"`+v\"`!$Tl`!G1l` oi`2~#`!T%`'(\"\")) {`&|+`!-!`&j!`!t4`(X\"`!v$`(Z\"`#L8rgb2hsl`\"\"&h`!X$` )!s` '#s` )!l` '#l;` Q*`\"`'v`\"F%`\"5$`!}\"{hex:`/\"\"}`#+%`#&$`# $`!8$`!2$` i$`!4$-1;}}clr`1?%`!'\"` &$`'($clr`'+!`%}#`'%)h, s, v`) \"if (`,^!is(h`%@2h`%I'` &\"`#@\"h) {v = h.b;s` #!s;h` #!h;o` #!o;}h *= 360`)o!R, G, B, X, C` F\" % 360 / 60;C = v * s;X = C * (1 - abs(h % 2 - 1));R = G = B` J!-` c#~~h;R += [C` {!0, 0`!#\"][h];G` 6!X, C, ` 7&` 6!B` 6!` F&` ;\"` 6!`#G#`,E&(`!z%o)`#Z#`($!`#N/l`#&W`)-\"h) {l`#D!l`#P-`'}!h`.`$s`.i$l`.j#h /`#q\"s /= 100;l ` \"#`#bM2 * s * (l < 0.5 ? l : 1 - l)`$!Fl - C / 2`#M~`$56`)f#`4(3b =`4Y'` 0%`2w!b[0]`2v!b[1]`2u!b[2]`#?!H, S, V, C;V = mmax` R&C = V - mmin` ,&H`#1!== 0 ?`4E\": V == r ? (g - b) / C` /$g ? (b - r` 1\"+ 2 : (r - g` *$4` q!(H + 360) % 6 * `$e!360;S`!'(0 :`$(!V`#,${h:H, s:S, b:V`3*'hs`3*'`#9&l`\"OpL, M, m, C;M`#8-m` -!`#9(C = M - m`#:1M`#92` 2!`\"l_L = (M + m)`'P!`#N-L`(E%C / (2 * L)`#p#(2 -`(s!L)`#n/l:L`#t)l`#s*_path2`2|\"`#y)) {` f#`+l!join(\",\").replace(p2s, \"$1\");};` O%repush(array, item) {for (var i = 0, ii = ` >!.length; i < ii; i++`-!#` ;![i] ===` a$`!K#` U\"` \x7F&.splice(i, 1)[0]);}}}`!J%cacher(f, scope, postprocessor) {` A%newf() {var arg = A`!$\"rototype.slice.call(arguments, 0), args`\"1!g`#%#\\u2400\"),`!;\" =` z!.`!I! =`!'!` &#|| {}, count` 1%ount` >%` (!|| [];if (` R![has](args)`\"u!`\"j!` E!`!G\"`%D%`\"B) ?` \"*` e#args]) :`!f\"` )\";}` i!`$<# >`/m!0 && delete` E#` ?\"shift()];` )\"`$5#gs);` g' = f[apply](`$%#arg`!&_` Z#newf;}var preload = R._` #&`'U&src, f`%%#img = g.doc.createElement(\"img\");img.style.cssText = \"position:absolute;left:-9999em;top` $$\"` \\!on`!5-) {f`&(\"this);`)&!` ?%null;`!P\"body.removeChild` I#}` r#error` j,` ;;` 4'append` 9\"img`\"I#rc = src`*3(clrT`+9#`*r,hex;}R.getRGB =`).%`!J%colour`*7#!` (\" || !!(` 5# = Str` A$).indexOf(\"-\") + 1`'c\"`,|\"r:-1, g:-1, b` '!hex:\"none\", `\"l!:1`-/'`!o'};}`(a!`!-#= ` P\"` TE` ]3!(hsrg`)Q\"` v\".toLowerCase().sub`.C\"(0, 2)) || ` B#charAt()`!I!#\") &&`!W& toHex`\"n%`2?!res, red, green, blue, opacity, t, valu` A!gb =`!!$match` d#RegExp)`+N!rgb`$3#rgb[2]) {blue`!8!Int` /#`!x'5), 16);`!:!` -63, ` B$red` .61, 3` A#`$8!rgb[3`!5-(t = ` 5\"`#+$3)) + t`!90` ;/2` B(`!E(` 9/1` @(`!L%4]`+0!lues` K#4][split](commaSpace`.Y!` x\"Float(` L\"`1W!` \"%`0h\"(-1`%1\"%\"`%1!red *= 2.55`\"%(` Z)1` ]&1` P3` T\"` `%`#>%` [)2` ^&2` Q3` T!` ^%rgb[1]`'H,` S!0, 4` T\"rgba` W\"`&h#`!'.3]`'C!` %# &&`''#[3`!03` b$/`1A!`$%'5`#\x7F-5`#.~`#.~`#iC`!~&` F$3` H\"deg\" ||`#O$`\",-\\xB0`+`#red /= 360`$YChs`#~~`.c#R.hsb2rgb(`,s5`%A'6`%;-6`$J~`$J~`$J~`$pXl`$J~`%B&l`%+=`2H\"{r:` ?\":` ?$:` C\"`4F2rgb.hex `3x! + (16777216 |` |! |`!)\" << 8 | red << 16).` i$(16`\"v$1);R.is`\" $, \"finite`#\\$gb.`##&`!q%`\"?#rgb;}` %#{r:-1, g:-1, b` '!hex:\"none\", error:1`!}4}, R);`(]! = cacher(function (h, s, b) {`({-` 3$.hex;}` ^\"l` J6l` V*`$>\"` 3$` Z&`$4\"` S-r, g`!;)`#p/ | g`#s%`#\\:`!&!getColor = ` \x7F&`&Q!`*|!r start = this` E%.start = ` \"0|| {h:0, s:`$(!` i! || 0.75}, `\"8\"` U!`#?$` T!.h,`!(\".s,` \"#b);` 4# += 0.075;if ` H$ > 1) {` &$= 0` H#s -= 0.2` &%<= 0`&.!`!q2`!c)`!4#})`&5%`'n#;}`#&'.reset`#*)) {delete`\"7\"` c!;};` 8%catmullRom2bezier(crp, z`#c#d = [];for (var i = 0, iL`-w!crp.length;` -\"- 2 * !z > i; i += 2` c#p = [{x:+ crp[i - 2], y` %'1]}, ` 8&` 1)+` ., +` Q++ 3` 1-4` 3+5]}]`$G!z) {if (!i) {p[0] =` O'`\"##` Q(` -\"1]};} else if (` /#4 == ` c\"3` \\)0` W'` E22` Q&2` >9` k,`!Q(3]};}` u#`\"1!`!A3p[2]`!8(`\"G2`#e.;}}d.push([\"C\", (- p[0].x + 6 * p[1` '\"p[2].x) / 6` <&y` :(y` >$y` >$` T%` >\"2].x - p[3` _'` R%` ;%y` <$` `$` T\",` 7#]`(=&d;}R.parsePath`+m\"`(4)p` .%`%L$` %)` `#null;}`'C!th = paths` P(`&2!pth.arr` M&pathClone` 2%` ^#aramCounts = {a:7, c:6, h:1, l:2, m:2, r:4, q:4, s:4, t:2, v:1, z:0}, data`)[\"if (`1q!`!I&, array`1q!` ,+`&:!` 8\") {` _#`!a'`\",'}`\"n!data`*C#) {Str` 8(.replac` R\"Command,`#W'a, b, c`*`$arams`!q!, name = b.toLowerCase();c` f)Values` b,) {b &&` i#`&z\"+ b`0^!if (` u\"= \"m\"` <'`!~\" >`,2!`\"1!`'S\"b][concat](` C#sp`1O!0, 2)));`!c#\"l\";b = b` s$? \"l\" : \"L\"`#&\"`!/%r\"` d;))`)F${while `!3$`!]$=`%\\([name]`!LF` I.)`\"s\"!` a0break;}}}});}` }!`48$ = R._path2s` ,!;`'D#`%f)data);`),$ata`1S!`)3!Transform` g%cacher(`$f&T`)3*` %&`),.`(K*r:3`(/(m:6`( 2` k#`(\"-` 1#`'q;`!H$`'t5` 9$`'.%t`'RQl`'{$.call(b`'G^`&?<`$k>`$f));`!u\"ths`-v*s`\".$`-S$.ps`-^$.ps || {}`-]\"[ps]`13!ps].sleep = 100`(#%` 5! = {` 8!:100};}setTimeout`%l') {for (var key in p`%}#p[has](ke`%'\"key != `!c!p[key`!4#--;!` $( && delete ` 1\";`(-!`/F$[ps]`'T!findDotsAtSegment`\"a*1x, p1y, c1x, c` %!2x, c2y, p2x, p2y, t`#/#t1 = 1 - t, t13 = pow(t1, 3), t12` ''2), t2 = t *` K!3 = t2` '\"x =` \\!* p1x +` V!* 3 *` I!c` -#` ('` 2!2` 2!` Q!2x, y` Z'y` R/` .\"` T-` 2!` _\"y, mx =`!>#`!W! * (c1x - p1x) +`!m\"(c2x -` =!`!W\"p1x), my` X!y` O*y` X!y` Q(y` T%y` X!y), nx =` g#` S&`!\"!c`!.(p`!0&`\"[!c`!5!ny` X!`!,*`!\"!c`!.(p`!0&`\"S!c`!5!a`#y\"`#t&`\"1\", a`#M\"`#H&`!m\", c` I%`#~#`#|$c` I%`#R#`#P$alpha = 90 - math.atan2(mx - nx`#7!- ny) * 180 / PI;(mx > nx || my <` ;!&& (` h\"+= 180`']%{x:x, y:y, m:{x:m` )!my}, n:{x:n` +!ny}, start:{x:a` /!ay}, end:{x:c` -!cy}`!m#:`!'!}`(Y!bezierBBox`($O`0=$`/;!` N!\"`/5!\")) {p1x = [` DB]`0m\"bb`!C!curveDim.apply(null,`&k!`\"m'bbox.min.`\"8!` %%y, x2` (#ax` 8!` $'y, width` 6' -`!1!` c$height` G'` 4(y`#.\"isPointInside`#*-bbox, x, y`3+&x >=` a\"x && x <` '$2 && y` 6%y` )\"` 7$y2`!*#BBoxIntersec`,s*bbox1,` J!2`,]#i`0V!`!U-`#5$i` O!2` N\"1.x` W\"1.y) ||` --` ?%` )2` 5%2` ?;` 4(`!h$` W$2` p)` 4&` W#` '4` 5%` f2` T&` :\"` 9\".x <` 9% &&`!b$ >` .$ ||` \"%` E\"1` @'2` B%1.x`)u\"`\";#` o%y` m'y` o%y` n&` C$1` @'2` B%1.y);};`$z%base3(t, p1, p2, p3, p4`1e(-`0=\" + 9`,9! -` #\"3 +`06!p4`1^(1 + 6` K\"- `0m!p2` +$3`%^$` E!2 -` Z\"1` a$2;}`!O&ezlen(x1, y1, x2, y2, x3, y3, x4, y4, z`+ #z == null) {z = 1;}z = z > 1 ? 1 : z < 0 ? 0 : z;var z2` >!/ 2, n = 12, Tvalues = [-0.1252, 0.` \"\"-0.3678, 0.` \"\"-0.5873, 0.` \"\"-0.7699, 0.` \"\"-0.9041, 0.` \"\"-0.9816` ,!816], C`!(&0.249` H!249` \"\"335` +!33` \"\"03`!L!20` \"\"160` C!160` \"\"06`!9!10` \"\"047` C!0472], sum = 0;for (`*'$0; i < n; i++`%3#`*d!z2 *`\"i$[i] + z2, xbase =`%t#ct, x`$:#x`$4!), y` 2-y1`$Z\"y3, y4), comb =` f#*` n#+` S#*` \"\";sum +=`\"z$[i] *`2g\"sqrt(comb);}`&-#`!h!sum`%y'getTatL`%d?`%s!if (ll`%a!||`&;B) <` V\"`!?\"`1#\"t = 1, step`(1!`&;!`(;#-` 2!, l, e = 0.01;l =`'GDt2);while (abs(l -`!:!> e) {`!,!/= 2;t2 += (l`!Y!`'x#-1) *`!9!` aL`#Z$t`)T(i`0I$`\"a=`#S!mmax` E!x2) < mmin(`%h# ||` )#` 8#> ` H\"` 3(ax(`%}\"` U%`&##` U%` 7$` W#` 8#`$,+nx = (x1 * y2 - y1 *`!/!* (x3 -`!,!-` =!-` -'* y4 - y3 *`'[\"ny` N6y3 -`!U!- (y1 -`!Q!` Q3denominator` j#`!.%` L7`!b!;if (!` V'`\"<+px = nx /` y(, py = ny` &-x2 = + px.toFixed(2), py` 0#y` ,'`!5!px2 < +`$M)` 9' ||` o!>` >!`%2&` 0/` \\&`%+\"` I9` -2y` ['`%A#` 0/` ]%` )6` \\&`%y\"` I9` 2-`#w'`(L#{x:px, y:py}`(M,(bez1, bez2` P%` 4\"Help` 2*` L,Count` 6G, 1` S-` 2/just` v!`/\x7F#bbox1 = R.bezierBBox` L!),` 7!2` )/`%Y#!R.isBBoxI`+ %` i!` R#`#\"&`!+&`45#[]`';\"l1`,>%.apply(0`!c!1), l2` &22), n1 =`$<\"~~(l1 / 5), 1), n2` .(2` /'dots1 = []` &\"2` %#xy = {}, r`4<!`!X/`3511 + 1`3C(p`#$!findDotsAtSegment`!u#R`\"2\".concat(i / n1));`!Q!.push({x:p.`&$\".y, t:` ?\"});}`!=!`!2(2`!2(` v@2`!-)2`!3#2` ~82`!%1`!-%`\"m%j` 8\"j`!S!; j`\"i%di =`#q\"[i], di1` && + 1], dj` ,#2[j` *!` ;$2[j` <#ci = abs(di1.x - di.x) < 0.001 ? \"y\" : \"x\", cj` D$j` E#j` 35is =`28'di.x, di.y`!e!` '\"1` )!j` (!j` &\"` 4\"j1.y`'\x7F\"is`2X#xy[is.`.2&4)] == is.`./&`1h!continue;}` >1` B,;var t`\"s!i.t +`\";!(is[ci]`\"i![ci]) / (di1` '*`0h!di1.t`#1\"t), t2 = dj` \\*j` P!j[cj` c#j1` '*` d\"j` c#j.t`\"W\"t1 >= 0 && t1 <= 1`#c!&& t2` 0&2` 1%`#$#`+x(res++;} else` )!`&j%is`&n\"is`&p!1:`/1!t1`*)\"t2` )#`-4!});}}}}`.l$res;}R.path`,3%ion = `-W%(path1, path`.,,Path`-t#` 9);};` c.Number` <[`/7\";`/0*` 35`#4(` 5!`*\\!_` 9!curve` J\");` -!` (12`&2\"x1, y`4R!, y2, x1m, y1m, x2m, y2m`,R\"`0V$`-=C, ii =`!j!1.length`+9\"ii`-f)` <%[i]`&,!pi[0`(R!\"M\") {`1U!x1m = pi[1];y1 = y` )$2]`%p%` O*C\") {bez`/k!`\">\"]`-9$pi.slice(1));` |!bez1[6` v#` (!7` o&` X*,`#(&`# &` ##]` h\"x1m`!^%`-y#`-]%, jj`\"L#2`\"o%j < jj`-m(p` <%[j`\"q#j`\"j*2 = x2`\"b!j`\"s!2 = y` )$`\"h,` T%`\"p$`2W!`%.\"`\"n&j`\"l(`3v#2`\"s!` $%`\"j*` ['`%w'`%o%` \"$]` h\"x2m`!^%;}`%f!ntr`.\\$`'a#`&@(`'\\&`\"^!`+&+ +` U!r`!H%`#a%k`#d\"kk` v\"r`#`%k < kk; k`#d!intr[k].s`3(\"1 = i;` &+2 = j` ,%`%D#bez1` ((`\"v$;}`(8\"res`3d%ntr`,%1isPointInsidePath`+/-, x, y`%J#bbox`*\"!pathBBox` >!);` n#` a+` <!bbox` \\$&&`+<1, [[\"M\"` @\"], [\"H\",`!'!.x2 + 10]], 1) % 2 == 1`-;!_removedFactory`!l)methodname`--&` 4&) {eve(\"raphael.log\", null, \"Rapha\\xEBl: you are calling to ` t\" \\u201C\" +` *#name + \"` 3!D of `!W# object\",` >');};}`-2!pathDimensions`#M)`#x-`)L$`$?!paths`#v#`(v!th.bbox`\"E&clone` .&;}if (!` b#` =#{x:0, y:0, width:0, height:0, x2` 9!2:0};}p`%V\"`//+`/6#`'c\"y`'j\"X = [], Y ` \"\"p`.Z6`.\\3p` 8#`.d%`+f* = p`+g!` $!2];X`4E\"x);Y` $\"y)`)E%var dim = `!q!Dim(x, y,` ^!,` [!, p[3` '!4` '!5` '!6]);X = X[`(^\"](dim.min.x, ` &!ax.x);Y = Y` 3-y` :&y);`!m\"5`!k$6];}`+t!xmin = mmin[apply](0, X), y` %1Y), xmax` 5!ax` C-` &0Y)`$v# =` V\"-`!2!`%!$ =` R\"-`!.!, bb = {x:` >\"y:` 0\"x2:xmax`%I!y` $!`%j\"` t!`%k%` l\", c` Y\" +`!2#/ 2, c` f\" +`!1$/ 2};`&x$ =`'*#bb`+E%bb;},`%D!Clone`( -Arra`,1$`-,\"`'m#` 3%;res.toString`(y!_`'<!s` ,!`! $`-F!`!#\"ToRelative` D&` ')`!(6`)H,`!;#`)U$rel`).&`\"+%` 2%`)X#R.i` S', a` {\"|| ` ,+ &&` a!` (![0]` C$) {` :&`+I\"rs`/T!`\"_\"`!O(`&^!`#3\"[],`)w+m` (#m` )#start = 0`\",\"`!0'`)K.` 1(`)`$` ((2];` z!x;` y!y;` x!++`$J!`)|!`0$');}`+3)` G!`+9'` q!`+23`\"<!`2d\"[i]`,($a`!C)`+\\$a[0] != lowerCase.call` 2\")) {r`\"C!` )2;switch (r[0]) {case \"a\":r[1]`!)![1];r[2` &#2];r[3` &#3];r[4` &#4];r[5` &#5];r[6] = +`!\\!6] - x).toFixed(3);r[7` 6&7] - y` 3)break;`!J\"v`!G%` I\"1` 1:m\":`$A!`!w\"`$E!`!u\"default:`$.%j = 1, jj` >!`$#%j < jj; j`$'!r[j`!1&j] - (j % 2 ? x : y)`!<)}}`/9$`$Q+`$B'== \"m\") {`!]& +`&-$`!f! + y`%t'k`'E\"kk`!Z*k < kk; k`!b\"`!'![k`#}#k`/$$len`!A%` S$`%.%` N\"`%1'z\":`.g!x`'y!my`#M)h\":x += +` h#[len - 1]`$?,y` *8`#\x7F$` W02];` E3}}`-6:`,*#`(<#`,?\"res`.l%`-`)Absolut`-`)` )'`-4Uabs`-U4abs`,k~`-Y0`!.!` E&|| ` $&`%I#`!j&[`,[\"0, 0]]`-Pu+`.61` '+`.18`,m\"`.B'`!e\"crz`.*/ == 3`#;,`!J&` ,*1][0].toUpp`-i\"()` C!R` 9+2` 13Z\";`)n%r, pa,`/LM`0d%r`#s!);`/Z;u`!;$`/c2` *1`/c2A`/&o+ x`/\\.+ y`/&*V`/X'`-I$`+n*H` //x` 5)R\":var dot`',!`%S![concat](pa.slice(1))`$8&j = 2`/z#dots`/l3dots`/~$` $$+ x;` '!++` )-y;`,H!pop();`(Z\"res`!C%catmullRom2bezier(dots, crz)`\"i*M`2$#`\"G(`()!` ,!`0A#`2*)`1eJ`2,\"+`2\",`1|&`1g*R\"`\"f#`#A;`\"#V`'n!\"R\"` f/-2));`3U$`2LK`2h*`'g1Z`2=8H` 7\"r[`2<+V\":y` $0`$P$r[r`+}$`27!`$W!` ()1]`2e'` ?.` <0`2F@abs`2J<l2c`2>)x1, y1, x2, y2`0('` +*` 9$];}, q` M3ax, ay` a'var _13 = 0.333333333` \"#, _2` 4\"666666666` \"#`!j$[_13 * x1 +` G!*` |!` /\"y` *'y` -$x2` 60` *'`!S%`\"\"!a`!p3rx, ry, angle, large_arc_flag, sweep` %#`\"u$recursive`\"L%20 = PI * 120 / 180, rad` 0\"` +! * (+`! \" || 0),`3j(y, rotate = cacher(`!`', y` j!`!)#X = x * math.cos(` 6!- y` ,$sin` /!, Y` ?(` /$ +` A&` W$`#\x7F${x:X, y:Y};})`1?!!`\"C(x`'(!`!e!`#9%-`!V!;x1 = xy.x;y` ##y;` B)`#7#` G$2` F%` ##y;var cos =`!R&`#4'`#7!), sin` :$sin` .0x = (x1 - x2) / 2, y = (y1 -`''!/ 2`!(!h`\"x#x / (rx * rx`\"w$y` /!y * ry`\"d\"h > 1) {h`!6%qrt(h);rx = h` Y!;ry` $$y;}var r`\"E!` v#, r`\"J!` q#,`,w!(`&G* ==`&L' ? -1 : 1)`$^%qrt(abs((rx2` b!2 -`!\"!*`!r#-` /!*`\"5\")`\"6\"` 4&+` 1))), cx = k`!\\!`\"R#ry +`#>!+`#9&cy` ?#-`!y\"x / rx +`#S!+`#P$, f1`\"x$asin((`#q\"cy)` s!).toFixed(9)), f2` =,2` 54;` t!x1 < cx ? PI - f1 : f1;` c!x2` .*2 : f2;f1 < 0 && (` ^!`)d!2 + f1);f2` 3&2` 0)2`%0\"`$ '&& f1 > f2) {` f!f1 -` L#;}`(I!` @+2 > f1) {` x!f2` D'`2?%1`3:!`(~#[0]`\"'\"` (&1];`$=!` (&2];`$)!` (&3]`&H\"df` \x7F$f1`!z!abs(df) >`,B!`+;#f2old` D!, x` %#`)Z\"` &\"y2`!>\"f`.T!12`,Y!`\"%1 ? 1 : -1`*+#cx +`&!\"`)z%f2)`*>\"c`&V\"`,#(f2);`-A\"a2c`*z%`.e+0`.W,old`!e#, [f2,`\"*\", cx, cy]);}`\"Z)`+R!`&l%`!N!1), s` *%`!E!1), c`&e%`!r#, s` *%`!i#, t` *$tan(df / 4), hx = 1`2I-`(n$t, hy` )5y` <\"m1 =`4%$], m2` )\" + hx * s` 4! - hy * c` >!3` >!2` :%`#>!` :%2], m4` =\"`2e\"m2[0] = 2 * m1[0] - ` /!;m2[1` /'1` 3#1]`&#!`/z(`4K$m2, m3, m4][concat](res);`'S$`2;#` 04.join()[split](\",\")`$H!new`2t$;for (var i = 0, ii`'s!s.length; i < ii; i++) {` U\"[i] = i % 2 ?`19$` 3! - 1]`3v![i]`3O\".y :` 7)` 8$ +` F\"ad).x;}`\"O#` |\";}}, findDotAtSegment = `4L&p1x, p1y, c1x, c` %!2x, c2y, p2x, p2y, t`)M#t1 = 1 - t`4!'pow(t1, 3) * p1x + ` +$2) * 3 * t`%G!x + t1` ('` 2!2` I%` \\$2x, y` h,y` ^6y` b2` I%` o%y};}, curveDim = cacher(`\"BL`\"h#a =`!z!-`&k!`\":\"p1x - (p` -&`\";!c1x), b`'4#(c` A!p1x)` ?#(` b\"` A\"c =` a#`!@!`#q!(- b +`3S'b * b - 4 * a * c)`27! / a, t2` K$-` +Dy = [`\"U!p2y], x` *\"`\"L!x], dot;abs(t1) > \"1e12\"`1N!`!Z!0.5)` 9\"2` /,2` 7$if` H!>`2%\"t1 < 1) {do`&p!`&y+`&RF1);x.push(dot.x);y` $&y)`2M\"t2`!.%2` Pc2` x;`%W\"y`%T%`&i!1y`%W\"` .%`' !c1y);`%Q'y`%W!y`%Q'y`%G!y);`%U\"` (#;`%)M;`%(M`$2~`$2~`$2~`!0#`-M$min:{x:mmin[apply](0, x), y` %,y)}, max` J\"ax` =/` ()y)}};}), path2`,f! = R._` #)`,f.ath` M#`,V#pth = !` .! &&` 7!s` C!`$P\"` /'th.` t!`3M&pathClone(` 3&;}` r! =` 9!ToAbsolute` n\", p2` 4#`!&%` 5+2), attrs = {x:0, y:0, b` &!b` '!X:0, Y:0, qx:null, qy` $!}` U#2` &SprocessPa`\"r!`#),d, pcom`#4#nx, ny`\"|&`\"n&[\"C\", d.x, d.y, d.x, d.y` *&];}!` p![0] in {T:1, Q:1})`(V!d.qx = d.qy = null);switch`!F\"[0]) {case \"M\":d.X`#O#[1];d.Y` &$2];break;` E\"A\":p`\"5\"[\"C\"][concat](a2c`&2'[`!]%` 8%p`+ !lice(1))))` i)S\":if (pcom == \"C\" ||`#%!` *!S\") {n`\")\"x * 2 - d.bx;ny` /!y` *&y;} else` F&` <%;}`!o',`#v#`!I4`!R)T`!P+Q`!Q*T\") {`#g%`!W&qx;`#x#`!W(q`!Y&` J&` >'`!Z)`!U&q2c(`%:(q` &!qy`)G\"[1]` \"#2]`!n+Q\":`!!#`$t&`!'!`$w$` l?` s,` \"#3` *$4`!%,L`%P3l` Z9` V+H` 7Jd.y` R+V` <A` %!` _#` R+Z` :CX, d.Y` T%default:`/@%path;}, fixArc`*j*p, i) {`&<!p[i].length > 7) {` -\"shift();`-T!i = ` 2!;while (pi` M#` K!.sp`'@!i++, 0,`!k+pi` 8$0, 6)));}` H', 1);ii = mmax(p` q#`.O!&& p2`!U$|| 0);}`\"-\"M`,x-1`0<#, a1, a2`\"8'ath1`/0$`/8%1[i][0]`(i!M\"` 6%` /#!` 1!) {` 0!`!b'`\"9!M\", a2.x, a2.y]);a1.bx = 0` %!y` \"$`'z$`!'!1];a1.`'~$` .!2]`\"-I;for (var i = 0, ` 3F i < ii; i++) {p[i] =`0M((p[i]`1A#);`%o\"(`%c!;` l\"(p2` B/2` L&2)` C$` V$`#\x7F!;fixM(p, p2` B#` H$` 3'2, p` -&` ?#i`&W\"seg = `!Y\"seg`4%!`\"=#` ,$len = seg`\"]%seg2` 0\"` F\"seg`\"j$;` z!`$7!seg[` X#- 2]` 3#y` *,1` 2$`$\x7F!toFloat(` 6)4]) ||`!t\".x` G$y` 743` C(y` I\"2` y\"`#g#` L'2[`\"&$` z*2.x)` R%`&3!` >9`!((2.y` R%`!+&` B+`\"i$2`'/\"` /01];}if (!p2) {pth.curve`']#Clone(p)`,*&2 ? [`%G!] : p;}, null`)u\"` J!), parseDots = R._` #(cacher(`*P&gradient) {var d` A\"[]`(52` F$`%B$`('+` [# = {}`!B!` H'[i].match(/^([^:]*):?([\\d\\.]*)/);dot.color`!q!getRGB(par[1]);if (` 8%.error) {`\"o#null;}` T(` A&hex;par[2]`$v!dot.offset`#^!` 3!+ \"%\"`!;!s.push(dot);}`\"\\!i = 1`\"V#` <!`*n#- 1`\"M,`$b!dots[i]` }#`\"l#start`'?'` ?\"`%9!`!E$`+`!, end`-6!`#t%j = i + 1; j`!'#j`!%%` c!j`!\"'` Y\"` )*`28#}`&A\"end` ?%100;` {!i;}` ,\"`!Y$end`+5\"d = (end -`!\x7F\") / (j -`!N\")`!b\"`\"c\"j`\"`$`\"H\"+= d`#N!`\"d& =`\"c#`#l!;}}`'N$dots;}), te`%n!R._` ##`&~&e`'_!per) {el ==` (\".top`.7\"` %%= el.prev);` =(bottom` A'` *#` J!next);` \"#` ?!` %#` d!` e*` -\"` =#prev` O\"` ]'}, tofront`\"&#` $%`!z2`3^\"`!w%== el`'U%;}tear` E'`!^%=`'s\"`!J$`\"g';` e%`!R&` *&` )\"`!`!back`!\\$` %#`!C<`#/$`!<F` L(`!c'`!x!` 0(`#b&` *)`!f%insertafte`%^#` #*`!k*el2`!t&`!J.2`%b<` @!`%C(` '\"`!Z&`\"6(` 6$`\"3'el2` X&`!t*before`!w(` )%`!LT`'A8`\"$\"`'*'` '\"`!P&`'K&`\"$\"prev` J&`!w!`\">)`%u\"Matrix`\"!!` \"'`!w&path, transform`-D#bb`24#Dimensions` E!), el = {_:{` K%:E}, getBBox:` p&`/o&bb;}};extractT` O$`#!!`!/&;` F#el.m`!b!`!{!` 7$Path`!\x7F\"` #+`!n8` k#mapPath` 9$`\"_#` ?-`*l!`!U,`%9\"` #/`%5*tst`)7$tstr =`(R\"`!:&el._`!r&;}` @\" Str` L!).replace(/\\.{3}|\\u2026/g,` K+ || E`/o\"tdata`![!parse`!T%String` q\", deg = 0, dx ` \"\"y` \"\"sx`3.\"sy` \"\"_`-U\"_, m = new `#\"\";`!2(=`!/#|| [];`\"J!data) {`2b%i` y\"`4&!` 8!`4%#`3x,`!~!` <$[i], tlen = t` J#, comma`2M!`##![0]).toLowerCase(), absolute = t[0] !=` N$, inv`+m!` ;%? m.` 0!t() : 0, x1, y1, x2, y2, bb`\"0!`!0%= \"t\" &&`!U#= 3`$\x7F#` l$) {x1 =`!&\".x(0, 0);y` )&y` ,#x2` ;'t[1], t[2]);y` /&y` ,)m`#r\"late(x2 -`!g\"2 - y1);} else {` :(` R(}` :#`!\x7F,r\"`' $`\"+#2) {`*J!bb || el.`*$#(1);m.rot` q&bb.x + bb.width / `#/!.y` /\"height / 2);deg += t[1]`!d$`!*(4`#5/`\"t*2`\"&!3`\"p.` /&`!O+`$_\"`\"o(` 2)` N(}`!R)`\"t3s`\"z- ||`%=)`\"}7scal`!0&` P!- `\"uKsx *`!]#sy` %\"` `%`#A05`#4=3`!I!4`#C.` /&`!k,2`#E1` 2.` X(}`!g/2`#O6m`(v)7) {m.add` h3, t[5` '!6]);}_.dirtyT = 1;`0T% = m;}}` \"*_.`-0!sx;_.`-2!sy;_.`-\\\"deg;_.`-`!`-e!m.e;_.`-h!`-m!m.f`*v!sx == 1 &&`-u!` $#!deg && _.bbox) {` #\".x += + dx;` *#y` *#y`#C%`!s)}`3D\"Empty`0x)ite`44$l = item[0];switch (l`-J*) {case \"t\":`1:#[l, 0, 0];` 4\"m` -)1` 7\", ` \"#` >$r\":if`!8\"`.o#`)u$` i*`!K!2]` \"#3]`&v%` ;)];}` q\"s` b15` i*1`!I!` j#` \"#4` p&` N/3` O.`!411];}default:`#n!equali`39'`3R!_` #0`$)&t1, t2) {t2`1Z$2`4M7t1);t1`4571)`3P#` k!` .42` >$var max`\"V$ mmax(t1`34%t2` $#), from = [], to` \"#`4A#j, jj, t`\")!t2;`4c!`4F\"` u%`4K$t`!{!t1[i] ||`' %(t2[i]);t`!t!` (!` 1*t1)`(W!tt1`4=#tt2[0` A!` .\"`'1*`1b# &&` N\"2` K%2` K%3` +%3]`\"n!` P5s` _&` I(` L$4` +%4])`%x%;}from[i]`#)!;to` \"%`\"|!j`#6#j`#p%`#n(`#q&; j < jj; j`#=!j in tt`+/!(` {#[j] =`!F!j]);` ;#2`!n\"o` 5'2` ;!}}`'B#{from:from, to:to};};R._getContainer`'#)x, y, w, h`+B#c` ?$;c` F'`(e!null`,s!R.is(x, \"object\") ? g.doc.getElementById(x) : x`%\"!` c'` d\"`#K'` 3).tagName`1p#`.*!` C) {` C%:` M%, `34!` '&.style.pixelW`3O!||`\"3&.offset` 4!, `3Y\"` F2H`3u\"` J/` 4\"}`+<,`!B8y`!#%w}`$J'` H&1, x:x, y:y` L$w` K%h`$g\"pathToRelative = ` #*;R._engine = {` I$2cur` D%` '\";R`2]&`%G&a, b, c, d, e, f`#m&new M` L!` 3.;};` Z%` ,4`$j\"a !`$g%this.a = + a;` (!b` (!b` '\"c` (!c` '\"d` (!d` '\"e` (!e` '\"f` (!f`#\x7F%` m%1` j&0` h&` $#d` 7&e` ,&f` &!}}(`\"g&`\"~\"proto) {` #'.add`\"{<var out = [[],`.>![]], `.T![`!d\", `!V\",` \"\"e], ` 4\"b` +#d` 3#f], [`4+#` `!`$V$[[a`!3!` V!b`!;!` <,`*B\"z, res`)K!a && a instanceof`$Q#`\";%` r\".a, a.c, a`!S\"a.b, a.d, a`!A+;}`-@!x`#=! x < 3; x`,|!` 4!y` 3\"y` 3\"y` 4!res` 1!` <!z` ;\"z` ;\"z` 8%+= m[x][z] *`\"M#[z][y];}out[x][y] =`\"4!}}`%4%out[0][0]`%;&out[1` )&c` <&1` ,#d` <&` +$e` <&2` ,#f` <&2];};`%H(invert`%J)`%@#me =`$c!, x = me.a * me.d - me.b` (\"c;`(}.` @!/ x,` B$` \"&c` &\"` i!` %!(` 0!` f!f` =\"d` (\"e)` 7&`!%#e` ;\"`!A#f` <!)`\"\"+clo`+E!`\"#)`!Q.`'>)`'1$`'K$`'9$e`'A$` u,translat` {*x, y`*}%dd(1`&F!0, 1`'J\"` V,scal` P., cx, cy) {`0c%`3C!y = x);(cx`/Y!y) &&`!Z\"` },` Z#`%8\"add(x` 5$y` #\"` AA-` c!-` e!`!k*rot`\"L,a`\"<# {a = R.rad(a);x = `!$!0;y = y` %\"`4R\"s`-C!math.cos(a).toFixed(9), sin` 6&sin` 2*`\"1&cos` H!, - ` \"!` ,!`#i\"`\" 3`\"1!`\"$-`0i*`\"&#`%r#x *`\"}# + y` %$c +`%a#` \\+y` G@b` _(d` c$f` \\+ge`)O*i` `&` L\"[Str.fromCharCode(97 + i)]`#&%4`'.-oString`(03R.svg ? \"` N\"(\" +`/[#get(0)`(/#get(1)` \"'2` .(3` .(4` .(5)].join() + \")\" :` f4` d)` ~)3)`$l\"` b$`\"0-Filter`\"*3\"progid:DXImageTransform.Microsoft.`+ #M11=\"`$4$`!Z\" + \", M12` +*2` 1$2` C+1` 0%` C+3` 2#Dx` **4` 0$y` **5` 1#sizingmethod='auto expand')\"`\"Y+offs`%r+`\"\\&`#o\"e`%\\'`-'$` ('];};` T%norm(a` X&a[0] * ` \"!+ a[1` (\"1];}` G)alize` T!`0t!ag =`*1#qrt(` o#);` c!&& (` $!/= mag);` r!` 0\"1` ,&}`-|)pli`1o1out = {};out.dx`2*#.e` *\"y` ($f`,-!row = [`\"q\"`03$c],`#\"#`0=$d]]` Z!`/9!`2u!`!|*row[0]));`\"P&` ,#` Q\"hear = ` .\"`#;\"row[1` '!+` 0$`#F!` 1#1];` $\" = [` B&-` W)` s%,` M&` 9&` j!` :%`!v'y`!k21`!o.1`!t)/=` i\"` _!`#:!`/+\"`!''`.f!`\"C#`\" \"if (cos < 0) {out`0`&R.deg(`!E!acos(cos))` L!sin` =0360 -`!?!` -\";}} else` ]7sin(sin));}out.isSimp`3}!!+`\"s&`0{'`&R!`%\"&` .(=`\"T(` -(|| !`!8&`#-\"isSuper` k@` _K&&`!\"(`!(!noRota`(\"!` m9` L(`*)#out`-x-`-T%`0@/shorter`)&#s = ` +# ||`1W\"`)U!](`$c#`#j%) {s`(W&+ ` %$`1g(` -#y` 5(`\"R&` <!`$}%+ ` %$` 5(`\"/#(s.dx || s.dy ? \"t`2,!s.dx,` /!] : E) + (`!L%!= 1` M\"`!>#` ,!? \"s` U#` B\",` 8%`1H#` _'`!V#? \"r` M#` ,\"` >(;`',$`1Q$m`2zn;}};})(`2+\".`$r!type)`)p!vers`%d\"navigator.userAgent.match(/V` ?\"\\/(.*?)\\s/`'a!` 67Chrome\\/(\\d+)/`%7\"` E&vendor == \"Apple Computer, Inc.\"`)1!`!Q$&&`!\\$[1] < 4`!-*plat`4U!slice(0, 2`(I!\"iP\"`!W+`!,'Google`!$&` u48) {paper`1 #afari`0t0rect`0e$rect(-99, -99`$$#width + ` (%height` -!).attr({stroke:\"none\"});setTimeout(`4O+ct.remove();});}`%}%`!U3;}var preventDefaul`2}-`!t#turnValue = false;},` R$Touch` H,`'2#` Z!originalEvent.`!\"*();}, stopPropag`,9$`!3.cancelBubb`-g!tru`!@!stop` sK` \x7F+`!=\"get` 9!Posi`!3-e`,c$crollY = g.doc.documentElem` u!` ;!Top ||` <#body` .&,` \\#X` D;Left` O1Left`,Y${x:e.clientX +` r$, y` .%Y` .%Y};}, add`\"@! = `%o*if (`!&\"` ;$Listen`/@!`!(#` G&obj, type, fn, e`!u\"`\"}#f`#%1pos =`#T-(e`.e%fn.call(` f#, e, pos.x,` \"!y);};obj`!O-(`!A#,`&r\"`0k#upports`%S\"&& touchMap[type]`!M#_`!<K, olde = e;for (var i = 0, ii = e.targe`(+\"es &&` #,.length; i < ii; i++`#w#` ;+[i]` *# == obj) {`!6!` 6-;e`'Q* =`!b!;e`({+ =`)d);e`'x, =`(c&;break;}}`#]]`#l), _`$?&` s%`&](obj`,^#`$B`` U5`!G+`!C.`+Y\"`-z&`(6'ttach` k!`'Y_`$o!`*D\"win.`$;!`3}!`*i~`+G@, x`&P!`+R0` .'`+]'`&D0` \"-||`1(+`&Y1` \".||`0;,`+#8x, `&o&`$L&(\"on\" +`$=$)`#j!detacher`0V,obj.` 6\"` F2`%a*` '#` e$;};}})(), drag = []` %\"Mov`3E!`+k-`#a)`#U+`$i$`$R~`%4<dragi, j =` &!`,n$while (j--) {` ?!` :#[j]`)[2e.`)E!es`\"X#`-\x7F#`-`),`*1\"` v$i` y!` .!` A([i` }\"` *!.identifier =`!=\"i.el._`!i!id) {x =` r\"`#e$;y` #+Y;(`.*,? ` \",: e)`'N+()`-t%`+5#{`'p,();}var no`0j!`!W%node, o, next =` ;!.nextSibling, par`/]\"` 6!` (\"N` Q!display` 3$style.` .#;`+W\"opera &&` _#`-S#Child(node);` I. = \"none\";o`!^(paper.ge`%p$ByPoint(`)-\"` R1`!53(`\"G!?`!L$insertBefore`!P!`\"g\") :` :$append`!k');o`&'!ve(\"raphael.`%$!over.\" +`!m&id`'6#.el, o);x +=`(,$;y` #&Y;` ].move` W4move_scop`/G!` y&x -`&O,x, y` $.y`,I\", e);}}` q\"Up`*s-R.unmous`$U!(`+C$)` .$up` /!Up`,o\"`)8$`(a%`!0!`(\\*`)[)i];`!J* = {}`\"Y/end`#A5nd`\"Z,start` %,`\"{4e);}`-f%;}, elproto = R.el`!L\"for (`+)%vent`+'$; i--;) {(`#/'ventNam`#=![` %%] =` u$` '*` P&fn, `!_!) {if (R.is` 0!\"` <$\")) {this`4V\"s = ` \"(|| [];` ''.push({name:`!,%, f:fn, unbind:add`1(\"` S!sha`\"~\"` )!`+/!`.s$,`\"V\"` \\#`!d$` D$)});}`1Y$his;};R[\"u`1|!`\"P1` (0`\"`(`.\x7F#`\"/6, l`#\x7F-`&G#l`&I!if`$'#s[l].name =` J#Name`*l!`#Z&undefined\") ||` q#[l].f == fn)) {` +&`#7\"();`!9#splice(l, 1);!`!H) && delete`!x(;}`\"\x7F+})`!e$i]);}`# #.data`\"m)key, valu`4_$` ;#eldata[` ~!id`#d\"` $*|| {}`1t!arg`4$!`!Z%== 0) {`!K#data;}` 141`&s(`!I!\"object`&y!`(B'in key` G#key[has](i`';%data(i, key`\"N\"`\"d*`*[*ata.get`*d!`\"+#,` $!,`!j![key]` f!);`!{'` 2!;}` \"% =`#5\"`+W+ata.s` g2` J!` k*`$J#`$:$`24\"D`$5/`\"Q& == null) {`$6.{};`4T%` .+`%q&` ++`\"2#`!<2get`!>-`$t&clone(`%H1)` Z'hover`)F*_in, f_out`*V#` ,!` $\"out` y&` q!`0@!over` R#` J$)` 3#ut(` _)out ||` =&`!A'un`!19`!*+`1Y#`!2%`1i%`!*%);}`1o!draggable`/z\"`)+%`0,\"`!\"&onmove, on`0y!, onend, `0m&, `1-', `1O%) {` ^%` =!`3U!(e.original`.W!`+l!).pr`*]!Default(`3R\"x = e.clientX, y` $'Y, scrollY =`.~\".doc`)l!Element.` :\"Top`/>%.body` .&` [$X` D;Lef`!b!` S-Left`1.\"`4`!.id`!]!identifier`+m!supportsTouc`-M!e.touches`,_#`3W!` ,$`+g#, ` +!`/N$i`/Q!` .!` A([i`2M#`!5'` 6!`!4,` %, =`0o#` Q$) {x` T%`#k#;y` #+Y;break;}}}` R'x = x +`#E$`!E(y = y` 2%Y;!` 1!`02&R`(*\"move(dragMove`(=#up` -!Up);` P!`4i\"el:`,6\"`&U&:`&S3:`&`2:`&w%});`'N#`$0!ve.on`-N'rag.` 9!`-M*` L#);`(3\"` B5move` J,move);`(\\!` ?5end` G,end`3h!`!;>`\"H' ||`\"z' ||`/N#`(g%`$H&` +&Y`$B&, e);`$|'`/'\"`+-%`$ ,`!5!`#m\"})`%=\"`$b!down(`#+#`.|2onDragO`,s-) {f ?`\"o2over`\"L*f) :` G!unbind` 1;`.3*`-#-`)M'`\"G&`'0\"`)C*if `'!!` =![i].el`(r$) {`.f(`\"c!` >)`\"n#` w&splice(i, 1`%*!`!u2*`!y+}`(o!`!W(`(s\"`!1#`(l+` /#`(n+`/}\"};paper`\"p\"circ`07!`\"l&x, y, r`\"v#out = R._engine` J#(`%H\"x || 0, y ` \"\"r` #!`%Q#__set__ &&`+|#` *\"`&,\"`1]!`%b#out`!G*rect`!@/w, h`!?6rect`!@3w` )#h` yjellips`#(1x, ry`!R3` P#`#+4` )$r` ,\"`!%]path`!V)pathString) {` #& && !R.is` 6', s` B#` */[0], array` :!` /' += E);`\"I0path(R.format[apply](R, arg`4-!s),`(q\"`!w\\imag`$-*src, `& &`$'3` S!`$5#src || \"about:blank\"`%~<`!8]tex`'i0text`!\\3tex`'b4Str(` U!` v]se`!H*itemsA`%8\"{`%U\"` )&, \"`%T!\"`%T\"` /& = ` #!.` p!typ`-G$.call(`%=%, 0`%I'`-*#)`&'(new Set`!<(`!{Dout.`\"5! `/=\"` ,!type = \"set\"`\"I8Star`\"\\*set`/}$`!4$= set`4$$.set()` X-Finis`)L*` d\"`\":&` h(;delete` &*`!L8iz`'q*width, height) {` U#`%}&` N#`$$\"`&)\"` F*` s-ViewBox`.r5fi` q4` S#`! (` I,`('+op = ` &'bottom = null`0P)`3@\" = R`%j!getOff`'/,elem`#e#b`!}!elem.getBoundingClientRect(), doc` <$ownerDoc`&v!, body = doc.body` F!Elem` -#d` B#Ele` J\"c` s!T`\"*!` D#.` *&||` l!` $*0` N$Left` F-` 0!` J*` +$0, `#2\"box`#=!+ (g.win.pageY`\"p#||` i%scroll`!=(` )%) -`!r&, l`!E\"box.` &!` m)X` b4`!_)` )&` r&Left`'N${y:top, x:left}`%B*ge`#K$ByPoin`-a.`$w#`*=(, svg`%\x7F$.canvas, targ`%Y!g.doc.e` q\"From` t!` g\";if`\":$opera`+Z!` T!.tagName == \"svg\"`!2#so =`&S&(svg), sr = svg.createSVG`&<\";sr.x = x - so.x;sr.y = y` *\"y;sr.`)H! = sr.`)K\" = 1`'^!hits` q#getInterse`\"f!List(sr,`(A!`!\x7F\"hi`.'& {`\"T%hits[` 1' - 1];}}if (!` @\"`*\"&`)/!}while (`\"`#parentNode`\"r& !`!P\"` .*` h#`)b$`!:(` [-;}` 4$`$I*` d+`!>#`\"_\");` a+`$B'`*v$?` g#getById`!}$` 8#id) :`\"@\"`\"K#` :\"`&L4sByB`-9,bbox`%C$`!F\"`0\"&` &!forEach(`,)() {if (R.isBBox`$b%(el`!f!Box(), ` w!) {set`22\"el);}}`4U%s`!X/ById`4T*d`-8%`!V%`.0\";`$s#bot`!J#bot.id == ` Q!`! #bot;}` X\"bot.next;}`%V)`!8(`\"I#`!7)callback,`!:!Arg`!)F` S$`0Z&Arg, ` <!=== false`!b&this`!T5` 7\"`$c5`+D9`$LMel.is` n!Inside` g\"`$M=` a%x_y(`\"**.x + S +` '\"y;}` @(_w_h` 2:` Q(`+_\"+ \" \\xD7 \"` 0$`+l\";}el`\"z\"`!u)`\"c4rp`\"r$realPa`,Z!getPath[` 3!type]`$X!`,7\"` 0!attr(\"transform\")`){!` %1`,_&`!'!R.` 3%Path(rp`&3\"` G.)`%3%R`!~*` O%`/q\"};`\"J$`)8#`(U*sWithoutT` r$`$}#`\"N#move`(E'{};}var _`\"o$_`\"P!` M5_.dirty || !_.bboxwt) {`#)E` L$`2N!thDimensions`!X$` ]\"` ?&.toString =`%s$;`!=$= 0`#+%` H$;`0:!`!\\'` $#T`!h'`!y/`!0* {`!]'0`(Y\"`!z@}` Q\"`\"+.map`$a!` ])`%B#matrix)`\"K$`\"3:`\"%%`\"J.`%N'clon`'u*`%(9`-S\"var ou`+1%`+u!`\"7()`'-\"`'3&)`+R#__set__`(*%` )#`+3\"out`+-%out`'2(low`!W)glow`!\\(type == \"text\"`!_,` X#` \"!|| {};`-6! = {`+-!:` k!`+7#|| 10) + (`+6#`)8\"stroke-` ?!\")` A!), fill:` X!fill ||`/3\", opacity` 6\"` &# || 0.5, offsetx` 6#` '\"` 8!` 3$` P$` '\"` 4#color` 3\"` &! || \"#000\"}, c = `-8$/ 2, r`$0),`$H#r`/`\",`&P!`,n-||`&v6` E(`&a\" ? `'$$p`&r- :` ~!;for (var i = 1; i < c +` &!++) {ou`0P#r.p` c$`%x#{`#n\":s`\"J\"`#g#s`#e\"? ` 0# : \"none\", '`$>#linejoin':\"round` ,+cap` '/`#7!':+ (`#>&c * i).toFixed(3)`$o&` C!`$q$/ c` :(}`.v'out.insertBefore`#I\"`/d\"late(s`%:$, ` #$`/5!var curves`0@\"`'*!},`$=!ointAtSegmentL` :!`(2)p1x, p1y, c1x, c` %!2x, c2y, p2x, p2y, `17%if (` '\" ==`(Q!`(Z&bezlen` MC);} else` U%R.findDots`!h%`!6EgetTatL` tE`\"(%);}`#$\"`\"v\"Factory`2N+total, sub`&O!`!i%`#<'`'R!` n\", onlystart) {`('#path2`$@!`'2\"`$P!x, y, p, l, sp = \"\"` }%`$]$point` v!`.y!`(9)0, ii` v#.`!7\"`(R\"ii`(M$p` 8#[i]`3\x7F!p[0]`-:!M\") {x = + p[1];y` $#2]`$/%l`1O#`%q/(`!t#[1],` P!, p[3` '!4` '!5` '!6])`!1!len + l >`%z*`\"D# && !`\"O$.`#0%oint` __`$/$ - len);sp += [\"C\" +`#X\"`!-\".x`#f#` (#y` '$m` 0&m` ,&` +%y`#S\"`%((`%X#sp;}`\")* = sp;`%&![\"M`!7&` c& + `!N(n` 1&n`!2&end` .&end` 5!`\"H%].join();`#q!= l;`$\x7F$5`$}&6];continue;}if (!`'`#`$!(`#+w`\"|#{x:`\"Y%y` $#y, alpha` (#` '!};}}`!p;}`$s\"p.shift() + `#t(end`#x\"`\"*$`\"H$?`(w!:`)0$ ?`)8&:`+\x7F1`\"K$0`\"6!`\"=81)`!(\"`!~\" && (`!6$`\"0E`#\"%` 0!;}`/i#getTotal`/J%`,S,(1)`/v(` .6` A\"S`\"M#` .80`\"-!R.`!(0` &'` <\"`!%/` &)` @\"`!0#`1E*`.'!from, to`+$#this`!*+`.#\" - to < 0.000001`)5&`\"#/` q').end;}var a`!Y\"` 63to`\"R!`/k$rom ?` :1a` n' : a;};elproto`# .`\"D&) {var`.\x7F! = `\"?$Path(`-v\"!`1')`)g\"` B!node` k+`\"S&` *4();}`\"v&`#=-`!f*`$T,`!l&`/>%`!NJ`!0&` n)`32)`!4,`%O,`!*'`'[\"` ;\"R._` &#[`!D!type`.t\"` &%`2S!text\" ||`!i\"` .%set\"`#V+` h#`4Z&`\"4$this`#=&path`!e*`'A0`'D'`\"^Q`(B(`(2,`*l#ef`\"b!easing_formulas = {linear:`!D&n`%_&n;}, '<'` (2pow(n, 1.7)` C!>` *:0.48` C\"<` 9-var q =` ?! - n / 1.04, Q = math.sqrt(0.1734 + q * q), x = Q - q, X =`!%!abs(x), 0.333333333` \"#) * (x < 0 ? -1 : 1), y = -` ]$Y` Z'y` K6y` U,t = X + Y + 0.5`*i$(1 - t) * 3 * t * t + ` \"\"* t;}, backIn`\"\\/s = 1.70158` g$n * n * ((s + 1)` *!- s`#Q!backOut` [+n =` ?!1`%A!` LD+ s) + 1;}, elastic` i+if (n == !!`%b*`%)'2, -10 * n) *`$_#in((n - 0.075`#G!2 * PI) / 0.3`!-%bounce`\"`37.5625, p = 2.75, l`(:!n < 1 / p) {l = s`\"5#n;} else`!o$< 2` ?#n -= 1.5 / p;` F)`$d!75` H.` G\"` T$2.2` E493` T'` F#6` 4684` K!}`#:$l;}};ef.easeIn = ef['ease-in']` )#<']` <$Out` 9(out` =%>` <&In` 9+in` >)<` F\"['back`! &.`'!\"` 0&` J&` 6!Out`&<!animationElements = [], requestAnimFrame = window.` /'` T!` 9\"||` 8$webkitR` ';moz` #<o` C=s` )5`&]&callback) {setTimeout` -%, 16`)H!`\"_%`1J0Now = + new Date, l = 0;for (; l <`#6..`2Z\"; l++` `#e =` 6.[l`22\"e.el.removed || e.paused) {continue;}var ti`$)!Now - e.start, ms = e.ms, `0M\"` *!` %\"`0w\"` ,!`1 \"diff` *!diff, to` (!to`-Q\"e.t, tha` &\"el, set = {}, now, ini` ($key`!w#initstatus) {`!]#` *) * e.anim.top`!v!prev) / (e.percent` ,'* ms;`\":!tu`\"6\"` a&;delete` &*e.stop &&`#y/splice(l--, 1)`*;%` t'(`!2\" +`!64(`\"/!/ ms)) /`\" ';}if` 7#< 0`$B)` .'ms`%>#po`\")!`$4!` i'`&&\"`)M!ttr in`$O!`/1#from[has](attr)) {switch (availableAnimAttrs[attr]) {case nu:n`'6#` `!` 8! +`!A!* ms *`%H!` 2\";break;` T!\"colour\"` X#\"rgb(\" + [upto255(round`!T\"` X!.r` _4.r)), ` ?5g` ?5g` ;9b` ?5b))].join(\",\") + \")\"`\"/)path`\"2$[]`#q&i = 0, ii =`#$'`*!%i < ii; i`*-!now[i] = [` A&[i][0]` g'j = 1, jj` e)[i` k&j < jj; j` j'[j]`$D+` .#`\";3` 8\";}`!J%` #\"`\"^\"S)` 4!` 0\"` *%`\"c(transform\":if (`$N(eal`!P\"`\"\x7F'`\"8j`\"FX`\"PL}`*Q$var g`,p!`/b&i) {`4c#`#k+` \\7;};`\"v#[\"m\", get(0)` \"\"1)` \"\"2` )#3` )#4` )#5)]];}`)')sv`#}\"`*_!== \"clip-rect\"`#{(i = 4;while (i--`#\\(`!PH`!6#default:var` X!2`!!![concat]`))')`\"E$`!=\"that.paper.custo`+|!ibute`+\x7F#`$[$`!N82`!E<`!^\"}set`,W#`'D!;}}`!:!attr(set);(`$`'d`1{\",`0&!) {setTimeout` ;') {eve(\"raphael`/K\"frame.\" + ` V+;});})(`!0!` 1&`0!\"`0h&` t'f, el, a` hSel`!\"!` ]\";` 7/inish` 8/R.is(f, \"`!#$\") && f.call(el`\"\"$e` ,!back`!},`#O&to);`3-=if (e.repeat > 1 && !e.next) {`*@!key in to`2*#to`2)\"key)) {init[key] = e.totalOrigin` 0!;}}e.el`!L\"init);runA`!O$(`!q\", e.el`!}$`4[$s[0], nul` 5!` o', `!j%- 1)`4F\"`!o\"`!y#stop) {` n9next` `;);}}}R.svg &&`#d! &` \"\"`(U\"` .$`(b#safari(`#c0`(g\" && requestAnimFrame(` D%);}`3*%`,q)color`,x&` *! > 255 ? 255 :` -#< 0 ? 0` ($;};elproto`\"V!ateWith` n)`&S!`\"r!params, ms, easing, `&=$)`.P\"e`\"(\"`+9!is`%v\"` -\".removed) {` L$ &&` W%`'7$` G!);`\"!#` )#;}var a =`!<# instanceof `$U% ?` 6$: R`\"'#ion(`!Y9, x, y`&C*a,`!0$, a`&51`\"+#`'-!))`2C\"var `3/(`$i4`32,`0+!` <,[i]`!}! =` c\" &&` _.[i].el == el) {` -/i - 1].start`!?0[i` 6#;`._#}`#h,;`%?%CubicBezierA`-*!(t, p1x, p1y, p2x, p2y, dur`'&\"`%L\"cx = 3 *` F\"b` &$(p2x - p1x) - cx, ax = 1` (! - bx, cy` R%y, b` &$(p2y` V!y` V!y, ay` S$y - by`!l&sampleCurveX(t`(7&((ax * t + bx)` ##c` $\";}` U&olve(x, epsil`\"+%t =` 5\"` r#` 6'`'2$((ay` y$y` x%` $\"` q,` T.`! #0, t1, t2, x2, d2, i`&H\"t2 = x,`&L\"`&-\"8`&*$x2 =`\"J+2) - x`)J!abs(x2) <`!\"'`!m#t2;}d2 = (3 * `\"{\"2 + 2 *`\"~$2`#\"!` `%d` d!0.000001) {`&!#`!Z!t2 - x2 / d2;}t0`!f!t1 = 1;`!z\"` c!t2 < t0`!<'0`/)\"t2 > t1` .'1;}while (t0 <` 6\"`\"91`\"?' - x`\"55if (x > x2) {`!]!t2;} else {`!f!t2`\"'#(t1 -`!c!/ 2 +`!a!` _'` $#`%k\"t, 1 / (200 *`('&);}`.E$o`+f&`.@)f) {f ? eve.on(\"raphael`2O\"frame.\" +`.8!.id, f) :` H!unbind` 1<`&v%`.y!`*3'`-3'`/f!ms`&]#`-7$ = [], newA`,3! {};` p!ms = ms` '\"times`$|!`,o$) {`-V%attr in`,#!`-/'[has](attr)) {` {#[toFloat` 2\"]`,W#[attr];`!M$.push(` =));}}` 6%sort(sortByNumber);}`!c!`.6\"`\"&$`!u#op =`\"G%[` \\%`41#- 1]` C\"`\"h'` #$;}`#8%.`$|!type.del`,5!`$u&` -!`#S#a`!;\"`#p'`!R%,`$J\"ms);a`#K%`#W&;a.del = + ` }\"|| 0`$r$a;};`!90repeat`!A)` h!` \x7FK`!3\"` ,!del`!T'math.floor(mmax` w\", 0)) || 1`!S(`!9%`3i*`!&!`3q%`#T#, status, totalOrigin, `!s$` ?# = `%>$` +#);`'9!arams, isI`!%!, ` \"$Se` z&`'S&xt, prev` z#tamp, `']!`(k!ms, fro`'y\", to` \"#diff`(-\"if (`!k\"`'q$`.}!, ii`3m0`&6#`/;\"ii`/:$var e`4:3` ~!e.el.id ==`#)$.id && e`'T#` V\"`(~#e`'+$ !`'*%) {`!<.splice(i, 1);`\"\x7F'`*-!`-O$` 0$ = e;}`!7$attr(e.`$:');`0&#}` R$`\"s\"`'3!to;}`*o%`\"s,`(y%`\"h3`+1$` ?%[i] =`\"2% ||` Y*[i] >`&!# *` 6\"top`%r)` A,;prev` &.`*b!`)*\"`-2# /` h% * `&W$ -`&#!);nex` l/ + 1];`&x\"` 6$`,{!` <#]`#4$`#4\"`&))`#e)` P&`!q,]);}`2;!!`!!\"`2P%`2P\"!`$_$`.m0` I%if (` '\"`.w*`#}!vailableAnimAttr` 8( ||`&q%paper.custo` C!ibute` b+fro`/V# `'I&`\"<\"ttr);` 4(= null && (` J)`!G&`!H!` 2!);to` 8%`\"##`0^\"switch`!r1` T\" {case nu:diff` \\%(` h%-`*g!` F$/ ms`$O#` S!\"colour\":`!W)R.getRGB`!o'`,D\"toC` K!` 8(`!\"$);`!5){r:(` G$.r`!:).r`!C\", g` 9'g` 6*g` <$b` 9'b` 6*b` >\"}`\"%)path\":`.2\"th`/q!path2curve`\"'', `!q%, toPath` D#es[1]`$`)` /$0]`\"7*[];`-n-`!U'`*c2` X&`*m! [0` _#var j = 1, jj` ])[i` c&j < jj; j` [.[j`%5#Path` )#`#%(` -\"`%?#}}`#*(transform`#5\"_`'V'_, eq = equaliseT` C$(_`#9-`/r\"q`(;-q.from`'[(eq.to`#=-` &&.rea`4`!rue`#!``\"X*0]`#m#`#*b` %)`#W7`0|$var `1V!`#\\#matrix || new M` )!, to2 = {_:{`$5%:_.` #%}, getBBox:function (`-x%` s%` =#(1);}}`'H*[m.a, m.b, m.c, m.d, m.e, m.f];extract`%&&to2`% (`$c'to2.`!P'`$i+(to2`\"=#.a - m.a`*6$` /(b` 6!`*.#` -*c` 6!c` '0d` 6!d` '0e` 6!e` '0f` 6!f` 5\"];`'\x7F)csv`( \"valu`+<!Str`0z$`\"V\"[split](separator),`%1!2` F#`-n'` 9.`(@!`2!!== \"clip-rect\"`(F,` l!`(2-`'w$2`&z$while (i--`'e/(`!~\"[i`&g-`&l%`$\\'` G\"`.(#default:`\"]%[][concat]`\"_*`&0!2` 3*`0N(`!n0`3X;`*.)`\"#:`\"7'|| 0) -`3l\"2` )%`\"B$`\"1\"}}}}var easing`3h%.` *\", easyeasy`2?!` /\"_formulas[` *\"]`$]!!` B$) {` J'Str(` A\").match(bezierrg`-W#` C$&&`!#%`\"C# == 5)`*y\"`1j! =` :%;` x'`*E&t`*F&CubicB`!&!AtTime(t, +` b\"[1]` \"&2]` \"&3` -'4], ms);};`,?$`!('pipe;}}timestamp`#.&start || anim` $&+`,f!Date;e = {anim:anim, percent:p` \"\", ` m%:` w%, ` e!` '& + (` }!del`$q\"` >!tus:0, init` '#` /\"` >!, stop:false, ms:ms`${!ing:`\"4$`)r\":from, diff:diff, to:to, el:`'$#, callback:`\"S#` ($, prev:prev, next:next, repea`!|#`\"x%` )!, origin` t$.attr(), totalO` 6\"t` \"&};animationE` L\"s.push(e`&.\"`\"M#&& !isInAnim &` \"'Set) {e.stop`3L$e`$E#=`$@% - ms *`#P#`,@\"`!&-`' &1`&R&` >%();}}if (`!(.` {-e.`!\"(}` n9 && requestAnimFrame`!S&)`')%`!/$.`%G& =`\"(%` 5$`!:/` 3%`!G)eve(\"raphael.`'V&.\" +`,Y%id,` $$,`\"d!);}R` H!`!W!`)b)`%s\",`1I!`+h$`%~$) {if` ;$ instanceof A`\">%`#g%` ?\";`#e!R.is`+j#, \"`!$$\") || !`,\"# {` ~$ =`!(% ||`-=$|| null;`-K%` )!}`!E#= Object`!U#);` /!+ m`) \";var p = {}, json, attr;for`2@#i`!j$`\"7)[has]` <!) && toFloat` *#!=` `!` '.+ \"%\"` 7$) {js`#_!`'N!p`0}%`1T(`&k#!js`#7(new`#P&`$.'`&\"&`\"f#&& (p`0\"#`.j\"ing);`#=%` :\"`#M/);` v1{100:p}`!%#};elproto`%k#e`%DH`2\"!`&U\" = thi`)o\"`&r$removed`%2(&&` W%`!n!` D$`!c%` )#;`2~!anim`#D%`&^1 ?` 6$: `'j'`![:;ru`)'!` E\"`0.\"`(U).`03#s[0],`&g!` 8%`-c#`!^.`#?%se`2Y!`#;)` ~\"value`&\\#`\"6!&&` .\" !`'l\") {thi`2.!tus` O#mmin(` B!`!V#ms) /` \"%;}`!C#`#z!`!<'`+u$`!.4var out = [], i = 0, len, e`$a!`!J,`#\"/this, -1`!W*1`\"t&`!N\"`'I#len =`/!&`.8+`)i\"; i < len; i++) {e` ?0[i]`&C\".el.id =`&W\".id`()!!`#X!|| e`%M! =` ]\")`#p'`)>&`06!`.H!out`2/\"{anim:` W\",`.e#:` @$}`0z$` Y*0`#\x7F%out`(k(paus`%;.) {`\"K!var`#u\"`\"S\"`\"d5`\"g#`2^1[i]`\"I;` >1`\"g0`0~.`!z!`1+!` s#`%.$` [5) !== false) `#F!` 6-` p!d`-n$}}`',3resu`(V/`\":~`\"kM`-D!`&m9`#H-`\"7\"`#F0`&S\"`#9*delete e`#4#;`*{(`'!$`&z$)`#86stop`\"N~`%ttstop`%\x7Fh.splice(i--, 1`#$/`\"\x7F%stop`4A(per`\"Co` s! == ` y$`!N@eve.on`# &`3o\"\",`!f*);` 8,clear` 4/`%c#toString`%a)`-%&\"Rapha\\xEBl\\u2019s object\";};var Set` N)items`28$` (!`14!`'C\"`\"}\"`#@!` +!type = \"set\"`(Z!` V$`#d*, ii = ` 8!`#]& < ii`#c(` ;![i]`&c!` %$.constructo`#n!`\"P$` +(||` v\"` </Set)`\"'#[`\")&`!>#] `'t$` Y!` +1` q$`\"U(++;}}}}, set`!I! `!$!.` '!type;` 1$`1c!`$+,`\"s!tem`4a!`3I\"`\"{,argu`&g+`#!*tem` ;(`,W$` 3!`#7$`\"pG`# 3len`\"v)`!F$`#J!len`#2+` +#item`\"~-`)c*`\"v&`,y+`&R$`&C#&& `.+#`$X&` 5\"--];` i'`!Y#pop()` t(forEac`#u*callback`,1\"Arg`&y4`\"G.`'#/` f$.call(` k#` u\"`\"p#i], i) =`,x'`\"a)`,],`!C$method in`$R$`.I$` (\"[has](` ?\")) {`\"L$[` -\"] = (`\"K&` /\"nam`!4'`&e-arg`&*(`#T)`#C#` h'el) {el`!)#name][apply](el, arg);});};})`!_$;}}`$2%attr`$+)name, value`\"C#name && R.is` 8#array)` ()[0], \"`,p#)`$a(j`$g\"jj = name`$^%j < jj; j`$b!`$A'j]`!S!` r\"j]);}} else`%*T`%:)` q&`\"?$`'\x7F6`/r!`(+,while `&@!`!)#` {$`'r#` V'`1<\"`/N*ndex, count, inser`0h! {` 5! = ` \"\"< 0 ? mmax` \x7F( +` 9\", 0) :` &\";` g! =` G\"0, mmin` H)-` M$` E!))`10!tail`0r!, tode` $$arg`1(\", i`(!\"i = 2`4J#`-A.`#]!args`.B!(`-B();}` X%0` [\"`!<!`$/%odel` Q\"`+U!`\"S\"+ ` P&` O\"`!x/` Z%ai` I5`(f#`-z\"`!]!`!n#`!E,` >#+`\"n!`\"++`%p(` t%`&N#` $*` _'?`!*![i] :` o![i -` 5#];}`&\x7F1` ,$`\"?$=`#%\"` O%;`&L'[i]) {`.h(i++];`'>$new Set(`#Z!`.\\)exclud`&b*`+<!`(^6`.:6`!C#`2/\"`#\"$`'k\"(i, 1)`,Q%rue;}`(+(animat`!:*params, ms, easing, `/O$) {(`+e!` 1$\"` P$\") || !` 2\"`,-!`0#% =` a# || null`'q\"`383,`\"V!len,`36!, set` A#, collector;if (!len`0O,`!,%`!=!` G$`++,!--len &&`\"1%`1k\"set`/!!`!i#=`.1\"`\"E$str`\"8!?`\"($:`!F'`'`!nim` P!`#R\"ion`#82` M$);item`\"Y)[--i]`$0$(anim)`&J$i--`-t, && !`.(*removed &&`3h*` l$With(`#T\"anim,`!p!);` OD|| len--`'\\%`.{,`.#\"Afte`#h*`&v!`'R$`08.`\"*6` f((`(c!`!#3getBBox`$|,var x`.\"#y`.*#x2` )$` #\"`,$\"`!D6 i--;`)(#`\"c2`!\"#b`!;!` 4*`!Q#();x`-\\\"box.x);y` $&y);x2` 2' + box.width);y` 0'y` 4#height);}}x`1,!in[apply](0, x);y` $.`! !`1W#` A(2);`\"b!` )+y2`+0%{x:x, y:y, x2:x2, y2:y2, `!\\!:x2 - x, `!N\":y2 - y}`4R*on`+X*s) {s`##$paper.set()`#|*`-5)`$#+`-;)`2N#`#k*`!1!()`%h&`%b)toS`*+!`%`,` E#\"Rapha\\xEBl\\u2018s set\"`&D)low` Q)glowConfig`%H#r`,S%`\"7)` ,!forEach(`\"i'hape,`3R\"` Z#g = ` 2!`!(!` u(`-=!g !=`.-\" {g` Z42` l#2`\"7\"`#$\"` 4\"`-(!}}`% %ret`)\x7F)sPointInsi`1^+x, y`*($` 5,alse`\"+4`*a!if (el` p*` l\") {console.log(\"runned\");` u,`2&!`!n#`!+\"`!{'` @);};R.registerFont`!{)font`*0$font.face`%;&font;}`\"%#nt`'(%` '\"|| {}`/t!fontcopy = {w:` d!w, face:{}, glyphs:{}}, family =` O!`!*!['font-` 3\"'`,!'prop i`!@\"`!Q$if`!o\"` T\"has](prop)) {`!=$` 5\"prop]` z)` -!;}}if `(C\"`\"!![`!2\"]`.O$` ()`%f\"` q$);} else` 60 = [` @$];` \x7F!`#K\"svg`!K-'units-per-em'] = toInt`\".'` 3+, 10`*p'`#K!`\"q%`#X\"`\"p(` ,\"`\"x\"` '!)`'%#path`\"l$` >#` \"!];`!X%` *)`$j\"path.w, k`$h!d` *\"`3]!\"M\" +` q!.d.replace(/[mlcxtrv]/g,`&A'command`&4&{l:\"L\", c:\"C\", x:\"z\", t:\"m\", r:\"l\", v:\"c\"}[` U#] || \"M\";}) + \"z\"}`*b!`!<!k`#b!`\"{#k in`!R\"k`\"r#path`\"l\"k`%`)`\"C).k[k] =` S#[k];}}}}}`-x$`()\";`,f!`3k%`(b.`%M!, w`0*!, style, stretch) {` ## =` ,$`\"1!normal\";` J!` 7!yle` -)` k\" = +` u#`)=!` 8\":400, bold:700, lighter:3` /$er:800}[` T\"`#D!400`#:!!R`'F\"`$1%;}`*-$ = ` 6#`'a$` L\"`+1#var name = new RegExp(\"(^|\\\\s)\" +`*H#`%Q'^\\w\\d\\s+!~.:_-`%^!E`$h!(\\\\s|$)\", \"i\"`'b'fontName in`!D$`$l#`!R$`$q!` ?$)` 6#name.tes`(p\"` 2$`\"$,` 4#];break`%)!var the`%'!`(j$`&/(`4,(`(d!`3|2` \\#` >#[i`#3\"` /#`-,(`$+\"'] ==`$o$&&` 51`%M!` C\"`%T%!` .6) &&`\"\"$` 1*`&P!` a$`&n$`\"Y%`'f#`\"\\$`'c)pri`'_+x, y`'X!ing,`\"G!, size, origin, letter_spac` ?!line` &$) {` A\" =` I#`'m!middle\";` M* = mmax(mmin(` ,+|| 0, 1), -1);` w(` H*` -(|| 1, 3), 1)`2\"!` n\"s = Str(`\".\")[split](E), shift`%:\"notfirs` '#`.2#E, scale;R.is`%u!, \"` b\"\"`$*!` /!`3>$`+6#`&<\")`&A(` `! = (siz`%\"!16) /`3'(`02*`!y!bb`2C(.bbox`!t$separator), top`+>!bb[0]`$(\"H`+Q$bb[3] - bb[1]`\"H#y`\"<\"h`+s&` 9! + (`$S$= \"baseline\" ?` g(+ +`!H'descent :` 7(/ 2`*C'`(r(`$(#`(j3if `%>#s[i`'G!\"\\n\") {`$<%;curr` $!`$A(;`\"/#+=`!:(*`&j);} else`3.#rev =`%'&&&`34)`!;% - 1]`.4!{}, `!5#` 51]]`!?\" +` l'? (prev.w ||` Q\"w) +` .#k &&`!D!.k` [(`'r!)`#\\$w *`(~+ :`\"O*1;}if (`!W!&&`!_!.d) {`':!+= R.transformPath` D!.d, [\"t\"`'v$*`']\"`%U%` '%\"s\"`'x#`'\x7F#`&G!`%r$, ` _!(x - top) /` =$(y -` >#` -%])`+})is.path`4`!).attr({fill:\"#000\"`+{!oke:\"none\"})`,G*add`,F)json`0Z%is` +!, \"array\")`20#res`)`$set(),`&n)json`&q#, j`'9\"`&q,j` A#[i`%N#;eleme`1i%j.type`*r!res.push(this[` 3\"](`\"C#j));`\"c%res;};R.forma`.\\*token, params`!\x7F#args`2K!is(` 3\", `\"C!) ? [0][concat]` 6#) : argu`!a!;` l! &&` Y\"` x#`-O# &&` x!`\"^# - 1`1q\"` O!= ` #!.replace(`!h\"rg,`!d'str, i) {`\"3#args[++`*%\"null ? E`!J\"s[i];}));`%I$`! !|| E`\"f\"ullfill = (` v&`\"[#` G!Regex = /\\{([^\\}]+)\\}/g, objNotatio` :&(?:(?:^|\\.)(.+?)(?=\\[|\\.|$|\\()|\\[('|\"` 5\"\\2\\])(\\(\\))?/g, `\"I#`*T!`!@%all, key`!)!`&1)obj;key`\"~%`!>,`##(` a!name, quote, ` \"!dN` .!isFunc) {name =` D! ||` 9'`0z!res`'r#` <!i`&+!`#~!s = res[name];}typeof`!a\"= \"`!?$\" &&`!%#`%)!` O%(`&\x7F!);` +\"` 2\"`$_#||` _$obj ? all :`!2\"+ \"\"`$l$`'S\"` &#`%W+`#2\"` 5#S`&\\!(str)`#0%`${&`#\"-key` R&`$8$`$%+;`+-!})();R.ninja`$T)) {oldRaphael.was ? (g.win.` .# = ` 7'i`(c!delete window` A$`\"9$R`'4!`.o!set`,B!;`'1'doc, loaded, f`$L#doc.readyState`#W%&& doc.addEventListener) {` #0(` m%`\"3,` y\"move` :3, false);`!9, \"complete\";}` +7loading\";}`!0%isL` z!() {/in/.test`\";+) ? setTimeout(` K$, 9) : R.eve(\"r`#u#DOMload\");}` r&;})(doc`,r!, \"DOMContent` ;\"\");eve.on` U.`&#() {`\"f\" = true;})`$H(`$?#!R.svg`&J%;}var has = \"hasOwnProperty\", Str =`'K#, toFlo`/P!parse` (!, toIn` +%Int, math = Math, mmax =` /!.max, abs` ($abs, pow` ($pow, separator = /[, ]+/, eve`0B!eve, E = \"\", S = \" \";var xlink`!\x7F!ttp://www.w3.org/1999/` ;!\", markers = {block:\"M5,0 0,2.5 5,5z\", classic` +, 3.5,3 3.5,2z\", diamond:\"M2.5,0 5` D!2.5,5` O\"z\", open:\"M6,1 1,3.5 6,6\", oval` Q$A2.` V!,0,0,1` %!` a\"` &*0z\"}`!x$Count`/B!{};R.to`$=\"`(C,`*&#\"Your browser supports SVG.\\nYou are running `*d!\\xEBl \" + this.version;}`#f!$` z)el, attr`&8#at` \"%`.}#el`.~!`42\"\") {`,*!$(el);}for (var key in` Z,[has](key)` .#key.sub` g\"(0, 6)` y!`$z!:` x\".setAttributeNS(` 7!`.)!` P'6)`'Y!`! \"key]));} else` R-(`.c!` <-}}` F&`&w!_g.doc.createElementNS(`&_/2000/svg\", el);el.style`1n!` %$.webkitTapHighlightCol`(1!\"rgba(0`%W!0)\");}`%\"#el;}, addGradientFill`$?+`!R!, g` ;#) {var typ`-=\"inear\", id = ` G#.id +` I%, fx = 0.5, fy` \"$o` D'node, SVG` )'paper, s = o`\";\",`%U!`#.&get`#.#ById(id);`,K!el) {`!5$`,(\"(`!u%`3Y%R._radial_`!]'`3\\*_fx, _fy) {`\"G$` M\"\"`!$!_fx &&` :#`\">!`-)#(_fx);`\"F!` (&y)`('!dir = (fy > 0.5) * 2 - 1;pow(fx -`\"x\"2) + ` .!y` ('> 0.25`%1!` y!`-?!sqrt(` 3!-` L\"` Z') *`!-!+`!&\"&& fy !`#}!` \\&fy.toFixed(5)` W!00001` U\"`%b&E;});`#L'` #$.split(/\\s*\\-\\s*/`$!\"`#,\"`%X&`%q#angle` Q)hift();` 1$-`#+%` -!` g\"isNaN` *#`,=&null`1I\"vec`/z\"[0, 0`0k\".cos(R.rad` Q$` 1#sin` **], `1.\"1 / (mmax(abs(` o\"[2])`1?!` ($3])) || 1);` 9% *= max` )$3` (%if ` ^& < 0) {` )#0]`\"I!` 5%` c'= 0;}` P'3` L+1` N)3` R%3` T#`\"o!dots`(I\"`3d!Dots`'}&`(D\"dots`#?,`*#!id`(?%/[\\(\\)\\s,\\xb0#]/g, \"_\"` [\"`)d$`%M%&& id !`)|&`$~%id) {SVG.defs.removeChild` T-);delete` S-`\"m\"!` ?-`08%`&N!+ \"`,e$\", {id:id});`!Z-= el;$`1I!`'\"%`*7# ? {fx:fx, fy:fy} : {x1:`$P%, y` %%1], x2` &$2], y` %%3]`-n&Transform:`!C$matrix.invert()});`\"w%append`\"x$);`2M%i = 0, ii =`$|!.length; i < ii; i++`2.\"` W($(\"stop\", {offset:dots[i].` )\" ?` h!` &': i ? \"100%\" : \"0%\", 'stop-color'` Y%` *! || \"#fff\"}`2D\"$(o, {fill:\"url(#\" + `01!\")\", opacity:1, 'fill-` *#':1});s.f`1A\"E;s.` 3# = 1` 2#O` ''`'4#1;}, updatePosi`/?!`1z(o`+w#bbox`0l!getBBox(1);$(o.pattern, {` ##`$D&o`$8, + \" translate(\" +` r!.x + \",` &%y`\"F\"})`3[#Arrow`!G*, value, isEnd) {if (o.`&f%path`-p$` E!s`2'#` (!).toLowerCase()`.`#\"-\"), p`\"@!`3;$e =` }\" ? \"end`$m!start\", node` I!`3\x7F\"attr`3n\"` %!, stroke =` 4\"['` *\"-width'],`&m!`!W\"`&e#`(h$ \"classic\", from, to, dx, refX`!$\", w = 3, h ` \"!t = 5;while (i--) {switch `\"N\"s[i]) {case \"block\":` '\"`!$$` (#oval` $$diamond` 2%pen` $$none\":`!i#` y%;break;` =\"wide\":h`!T!` ,(narrow` 4\"2` -)long\":w` A-short` 3\"` I$default:;}`,w!`%1%`!_!) {w += 2;h ` \"!t` (\"d`1f!;refX`$n'4 : 1;attr =`)2$`\":!`$[$:`$\\!.` '\"};} else {` `#` p!w / 2` V*` I(` a%` s\"}`.k\"o._.`\"g!s`';#`'F$` /&.endPath && markerCounter[` 2.]--;` (*M` M!` <<` @\"]--`\"1%` 5'`'h!`!!=` =%`!5+` 3!`!#?` ='`!C!`!7. = {`#0#`%'!!=`&]#`*?#pathId `1F!phael-`!'\"-\" +`(r!,`!7#` ,5se` C# + w + h`4D!!R._g.doc.getE`1:\"ById(`!+\")) {p`1(.$($(`+~#, {`*R$linecap':\"round\", d:`!;\"s[type], id:` s\"}));`#!*` 2\"]`'=!`\"x$` -1++;}var`\"I# = `!k4`\"i$), use`\"I\"` .\")` s$ = `\"'!` (\"`\")!i`!m$Id`#L$Height:h` ($Width:w, orient:\"auto\"`,Y\":`,^\"refY:h / 2});u`.D!`!$!use`#.\"xlink:href':\"`2`!`\"9\",`0\\\"`0}!`(K\" ? \"rotate(180 \"`$[!/ 2`1*!\" + `!\"!`0p! \" : E`1D!scal`1>\"w / t`1:%h` &$)`4R\"`/)':(1 / ((` I$` F!) / 2)).toFixed(4)}`$R$`%F)use);`%X/`#j#`$~+`#Z$`$s8` ;%++`#;#`!0#`$}&sByTagName`#S#[0];}$(us`1j#);var delta`,e\"*`(u'`/w$ &&`(3\"!=`09#)`%h!`,G$from`2T!`*\"*` i!`2^#|| 0;to`&h!getTotalL`2P!(`-b\"path) -`!D#` P$`\"Q%` |#` /+` M@`.;'.end`!E,);}`/.$}`/;![`'e#`*m#] = \"url(`&^!`+:%`%[!`\"`!to ||`4B!) {attr.d`!M$Subpa`!D)`4f&);}$(nod`#\x7F%`!]&[`,%!\"Path\"] =`'q#` /.`-j\"` >!`!P$` 3.dx` <!`#4!` ,.Type` ;!type` -.String` <!valu`$%&`$Ftfrom`%$,` 4B`$e>`#I4&& `$\"${d:`$7>});delete`\"H'` e)` &5`$I$` (5dx` \"7`$F\"` &5`$D$;}for `!v! in`3e*)`$T\"`+9*has]` J!) && `/z#` 5$attr]`3\\#item`0S7`'A\"` @!&&` H!.parentNode.remove`--\"item);}}}}, dasharray = {'':[0], none` $\"'-':[3, 1], '.':[1` %#-.` 2#, ` )&` &*` 0&. ` T\"3` B! ':[4` %$-':[8` 1%.` 4#` V!` :#.` ;#` ')` '*` 4#}, addDashes = function (o,`(Z\", params`#L!lue =`\"V&[Str(` 1!).toLowerCase()]`+b!` 4\"`$#\"`1I!`).!`&|![`1X*] || \"1\", butt = {round:` 9!, square` &$butt:0}[` [,linecap` g\"`!o\"` *.` 8!0`$^\"`\"M![], i`+%$.l`)c!;while (i--) {` E\"[i`+J%[i] *`\"+#+ (i % 2 ? 1 : -1) *`!]!;}$(o.`)]#`!?$`#1%':` p\".join(\",\")});}}, setFillAndS`*d\"`$%+`$\"'r node`#O!`.k&s`#Z&, vis =` A!.style.visibility;n` \"0 = \"hidden\";`)N!var att in`!0&if (`#X#`)@$)` 0#!R._availableA`$9!` 7(continue;}var`#F\"`0G!` g!att]`2)!` %\"`/7%switch`*y!) {case \"blur\":o.blu`&Y$;break;` :\"href\":` &\"title\":var hl = $(` ,#);`!?#`*u(createTex`*^!` t$hl.append`*g\"val)`#;\"` *(hl`!7*target`!4\"pn`$!$`+V&`(?!pn.tagName`(T* != \"a\"`%\"#`!q$a\");pn.insertBefore(hl,` u!`!U-` /\"`!5!hl;}if`#?! ==`!R%) {pn.setAttributeNS(xlink, \"show\"`*^#` P!blank\" ? \"new\" :` 5\")`2S%` R5att` a#);}`\"v(cursor\":`&a'` -\"`%4%`#C)ransform\":o.` $%`%+0`1 !-start\":addArrow`,i%` 80end` 7/, 1` ?*clip-rec`$o#rect = `-;'split(separator)`$\x7F!rect`+S# == 4) {o.clip && ` $\"`1+(`1+3` 8-`':\"e`%T#clip`4g!), rc` .\"`!e!);el.id`'`!`'X\"UUID();$(rc, {x:rect`1u!y` %\"1],`,x\"` )\"2], height` *\"3]}` o!`&@(rc);o.paper.defs` 0)el);$`&j!, {'`#?!path':\"url(#\" + `!]\"+ \")\"});`\"w#= rc`'2\"!`0_(pa`0e!`%p!g`&F'`\"^\"` v!\"`#s\"path` N#` j#`*?%getElementById` E!.replace(/(^url\\(#|\\)$)/g, E));`$I$`$?,`$8(clip`\"53E});delete`%,#;}`'\x7F)`!r!:if (o.typ`)-\"`\"&\" {` i%d:`)J\"?`*'!rs.`\"f#R._pathToAbsolut`,R$) : \"M0,0`#Q\"_.dir`/b!1`\"|!o._.`'|!s) {\"`(L!String\" in ` 4& && `(2(` 0&.` J');\"e`1s!` 4I` J%`))!`\"g*`&a!`*x$`$\x7F(`+G(`\"%.`\"u\"fx) {`--! \"x\";`1'$` 7\"x`,N%`!*\"}`!+\"x`#}\"` V'` O$-` Q$ -` 8$`!Y! || 0)` X$r` W&`$\\!rx\"`*?\"`$j%`)N\"`!(+cx`\"#>pattern && updatePosition(o`\"T,`2^)`)u!`\"kUy`#:&y`#4,y`#.2y`#7)y`#1/y`#:&`!Y\"`#6+` X&`#-\"y`\"\x7FBy`\"Lur`)I-`!:$`)Q%rx`)U\", ry` $\"}`2q&`!9:}` \x7F6src`!*-image\")` c/`4S'href`4V$`3g+stroke-`(u#`*_$sx != 1 ||`)X!sy` *!`$T%/= mmax(abs` H#), ` $%y)) || 1`/(\"`0!$_vbSiz`/1\"lue *= ` ,+;}`\"\\:`%\x7F%['`!o#dasharray']) {addDashes(o,`&6\"` 50, params)`!W$`,3~`,fK`$,/`!w%\":`\"9)`&>#`\"*$` N(fill\":var isURL = Str`0\"#.match(R._ISURL`#U\"` B!`'9(fill`'4%`(v#`38\"r`1 !getRGB` l#` ^!!clr.error) {`29#`!V\".gradient`2O$`*K\"` -%!R.is`*_#opacity, \"undefined\") && ` ?!` i#` ,5`!v%` ;#:` h)})` \x7F(['fill-` 5#']` v9` +>`!2%` <*`!<\"` O,`*g&if (`*'(circle\"`)7\"` -%ellips` 1\"`$_'charAt() != \"r`!B\"addG`#R#Fill`%`%)) {if (\"`!>#`&}!`!X! ||`%l\"`!X$` 1&`)l!r `$B$`%5!_g.doc.getElementById`\"X!.g`)r(`&_\").replace(/^url\\(#|\\)$/g, E)`%r\"` u$`!%#stops =`!+%` ~'sByTagName(\"stop\");$(` K![` Q!.length - 1], {'stop`#\x7F&`\"F0?`&d#` 3# : 1) * `!v\"`\"].` H$`$_,` S!});}}`'L* =`#v\";`2[#ill = \"none\"`(m$clr[has]`!U&`%r9clr`!e%> 1 ? ` &(/ 100 :` ((})`+A)\":`)l2`.G3clr.hex);`3z$` _# && `!Y=`,Y#`!?Y`/q#`!,,`.8~`.]Y`%9$\":`):~`$Z!`!1(`$&$:`3+%`&g&&& !`'=\"`$T\"`$5*\")`/w(`$O-`4V\"`$T\"` &\"`$L$`0A$}`15&` k%`!4/) {`*Q~`!\"\"`*Vu`\"J%`*!#default:`'%$font-siz`'*\"`$p\" = toInt` (\", 10) + \"px\");`3X!ssrule = att`\"<&(\\-.)/g, function (w) {return w.subs`&d!(1).toUpperCase();}`*2$tyle[` y#]`,/%o._.dirty = 1`*L4`&6)}}}tuneText(o, `2x\"` y(.visibili` p!vis;}, leading = 1.2, ` Z$ =`\"('el` f%`1d\"el`(?\"!= \"text`(=!!`4)$`':\"` 3!) ||` T#` 0#font\")` \"1`$.\"` *.x` =/y\"))`#`%;}var a = el.`'K!, node` +\"`(N\"fontSize =` 6!.firstChild ?`%4#`'e%`%j#View.getComputedStyle`'v\"` T&, E).getPropertyValue`\"#)`&'\": 10`'}!`\"r/) {a.t`#p\"` 9\"` )!;while `!\",) {` -!remove` .!` 2-`\"l\"texts =`,W!` m').split(\"\\n\"), tspans = []` &#;for (var i = 0, ii =` h\"`)3#; i < ii; i++) {` R! = $(\"` &!\");i`1,\"` *!, {dy:`#z%*`&N$, x:a.x});` B!.append`\"-\"`$%%creat`&p!Node(`!B![i])`'T#` H(` a!` f#s[i] =`\"*#}} else`!b#s`%=$`+\\2`!z$`\"a!`\"U)` X!`\"L3if (i`/ !`!6%`\"5>`!O$` L%0` N#0});}}}`/i%` L!, y:a.y});el`*r)var bb`'i\"_getBBox(), dif = a.y - (bb.y + bb.height / 2);dif && R.is(dif, \"finite`21\"`!G-dif});}, `#1#`*{)`!f\"svg) {var X`#6\"Y = 0;this[0`$.!his`)Q!`$ #`$b\"raphael = true` J!.id`1?\"oid++` :)` 5!` =#` F\"matrix` K!` %\"()` 3\"realPath = null` /\"paper = svg` +\"`+2!` l$` '\"|| {}` 8\"_ = {transform:[], sx:1, sy:1, deg:0, dx:0, dy` '!irty:1};!svg.bottom`1*!` $'`!(\"`!`#prev`!K\".top;s` \"\"` J%top.n`*}\"` O\"` 9$` *\"` ^\"` :#`\"B!}, elproto`\"o!el;`$L#.` 0!`/;!=` ;$;` D#.constructor =`$}$;R._engine.p`#C\"`%*&pathS`2>!, SVG`%3#`$c!$(\"path\");SVG.canvas && ` $&`)p)el`3e\"p = new`!5$`19!SVG);p`1/\"= ` q\";setFillAndStroke(p, {fill:\"none\", s` 3!:\"#000\", path:`!g&});`4B#p;}`\"Y%rotate`\"7)deg, cx, cy`*B#`&2#moved`1>%`$#\"}deg`.M#deg`.D$separator)`0 !deg`+@# - 1) {cx = toFloat(deg[1]);cy` &+2])` x$` +(0` E#`%4\"`%n!` k!cy`!(\"cx` 3%|| ` @&`$F#bbo`!9!his.`+5$1);` ^!bbox.x`+2!ox.width / 2`!U\"` /!`+N\"ox`+K';}` g!`(G%`#)\"_` '&.concat([[\"r\"`(Z!`#X$]])`$-%`#I\"`$0%scal`$**sx, sy`#pGsx`$3#sx`$\"3sx`$**s`#w(sx`$1\"`$@(sx`$1!`$>)sx[3])`!(#` *'0]);s`$:*` {!sx`#lp}` ^\"` R&?`$V4 : cx`!}\"`!<'` H#`$m/ : cy`+l\"`$_As\", `$>*`$l7` \\!l`)2-x, d`(yA`${$d`$k4d`$t+d`(y)`$}\"` a\"` *'0]) || 0;` I!+ dy` )\"`\";Ft\", `\" \"`\"=<form`\"P)tstr`%%#_`%!$_`\"'!tstr`%C'` n#`!6';}R._extractT`!Z), ` r!`!|\"cli`1&!$`!|\"clip,`2E(`3W'.invert()}`1q$attern && updatePosition` g!` >#node` v'node` i4}`$A\"_.sx != 1 || _.sy` (!`\"`#sw`4>)[has](\"`/i\"-`'S!\") ?` 6('` 1('] : 1`!T\"attr({` 1*:sw});}`$.2hid`&n*) {!`&g( &&`!4\"paper.safari`\"N&.style.display = `1h\"`,~5how` Aj` r5` k\"` },`)\", || `!;\"` z!parentNode`&X%;}`4c!aper`$?$` (!;`!\\\"__set__ &&` >\"` )$.exclude`%q#eve.unbind(\"raphael.*.*.\" +` n\"id`%d\"`-3\"radient) {` o\"def`!k$Child` 8+`(4!tear`(&#` P!` e'`\"-+.tagName.toLowerCase() == \"a\") {` ?1` T'`!8-`#,,;} else` S3` J1);}for (var i i`$i\"`!E#[i] = typeof` 3!` ,!= \"`$s$\" ? R._`$m#Factory(i) :`+H!`4\\#`%+$= true`%`'_`1\"#`%T5`&G0`'b%`\"?#show();var `(n#`! \"`2*'{};try {`28(` v!`2A$);} catch (e) {} finall` I&` \"!|| {};}`!!!`(G$hide(`(\"%bbox`\"5'attr`\"/)name, value`1#?if` L\"`4;+res`\".\"`$O%a`$P$`,A\"` o(`,o'a)` |!s[a`$q!` 4&a];}}re`'N& && res.fill`#g& && (` -&` =!`(G&&& delete` +);re`2/'`1.%`0p'`\"K#re`\"J#`\"x!`\"J$ && R.is`#2#\"string\")`\";#`\"u$\"fill`![!`\",&`!j/` 2'`*>'`#h'` .+`#t*\"`!k%\"` G+`2})var nam`$?!name.split(separator), out`$R+i = 0, ii` N#s.length; i < ii; i++) {`!F\"` =\"[i]`+d!` /!`%0,out[name`${+` .!`*}$if (`#X!`/m'customAttribute` K#,`*G')` j0` A8.def`,/%` N(R._availableA`!S(`3+$ii - 1 ?`#,!: ` T$s[0]]`%J?array`!h$`#f'`#],`#U3`!'$[i]`#9)` z![i]);`!`$ou`%d#`!I\"!`)a)param`)j#` &\"`\"X%` J!`$\"(`$f!` X#`!~,object`\"*!` j%name`/4'key in`!+#) {eve`2K&attr`2Q!key + \"`2V(,` $!,` S#[key]`0/$` m#`$k7`+b(`%13`+z!key`+!!` ~#` ')`&F=key`&U-`#W#`&E;key].apply`4=#[].concat(`\"C();`(<'key] =`\"c(`)a&sub`#^&`\"U#par`\"*\"` 7\"`$:&[` +\"` f#` &$;}}}}setFillAndStroke`!W#`$V#`-O$`0#\"`0h%toFront`2w5`0N7`2K&parentNode.tagName.toLowerCase()`-(!a`3W%` D,` T'appendChild` k1)`*O%` U1` J1)`-}\"svg`$l);svg.top !` 0\"`%p\"_tofront`#6#svg`# 6Back`\"iL`&<#e`#n!`!p0`.]!` )\"`#-<` ?#`\"U'insertBefor`%9\"` n,`)R\"`#f8first`#=!`#c%`!G'` 4&`#/$`#]\"`!D%`!,2`! 3` \x7F)`#w!back`#t#`$@&);`$K1`$!2`!;\"After`$/)elem`49\"`#\x7F@node = ` N#`!t! ||` ($[` 0$`/b\" - 1]` ?!`39\"ode.nextSibling) {`\"I,`\"n4` J-`'f%`'BDR._` w\"after`#D#`!h#`#F*`##8`!T\"`\"Fv0`#(#`\"LH);`\"3%b` 7&`!pIblu`%G*size`/q#`)H$`$^!+ size !== 0` 9#fltr = $(\"filter\"), ` i#$(\"feGaussianBlur\");t`/s\"`!+$size;fltr.id = R.createUUID();$(blur, {stdDeviation:`!>#|| 1.5})` Y\"`%,(blur);t`1a#defs` 2)flt` :!_`\"O$ltr;$(`$(\", {`\"\"\":\"url(#\" +` <!.id + \")\"}`&R&`%,!` a\") {` ##`$K(`%C\"`&f#` B#;delete ` )#` %&`\"n&;}`!F\"` Y#`3Z%`#Y&;}`$a$;};R._engine.circl`&q*svg, x, y, r`$K#el`$6\"` H\"\")`/l!canva`/g!` $&`#))el`+i\"res = new E`&;\"(el`0'%s`\" \" = {cx:x, cy:y, r:r, fill:\"none\", s`4%!:\"#000\"}` U!type = `!O$;$` t!` k%`'3%res`\"H)rec`49*`\"H'w, h`\"G.rect`!_t`\"Q!`\"P!width:w, height:h`\"c! || 0, rx:` \"%y` \"%`\"[D`!|!`\"XEellips`%=6x, ry`\"t+` M#`$[~`\"e\"`!>!:ry`\"9F`!Y$`\"=Eimag`\"Q/src`%a(`\"X+` P!\")` ~#`$_:preserveAspectRatio`\"\"#});el.set`*!%NS(xlink, \"href\"`!C!`%p~`!c.src:src`#N*`\"P\"`#14tex`(w5text`#4+tex`(+~ 'text-anchor':\"middle\"`!K\":text, font:R._availabl`.3!s.font`&@&`&R#`&`\"`&D/`\")!;setFillAndS` S!(res`&@@setSiz`&X*`$0!`$+$) {`4I!` 0! =`$L\" ||`4^\"` )!;` &!` I\" =` Q#` :%` )\"` >\"`#Z#`&L((\"` _!\",` f')` 37` i\"` G$`!]#;`2S!his._viewBox`!o$setV` ,\".apply` A!` V#` C%`1r&hi`\"q*creat`\"p*`&&#con = R._getContainer` }#0, arguments), c` 7$ =` R!&& con.` .%, x` 4\".x, y` $#`(5$` (#`$')` -#`#S#if (!` d%`\"T!row`'&\"rror(\"SVG`!:'not found.\");}`\"'!nvs`(5\"svg\"), css = \"overflow:hidden;\", isFloating;x = x`0B!;y = y` %\"`%W-512;`%I/342;$(cnvs, {`*V$` \"!, version:1.1`*z%`\"Z\"xmlns:\"http://www.w3.org/2000/svg\"}`%G\"`#i'= 1) {cnvs.style.cssT`+,\"css + \"position:absolute;left:\" + x` ;!x;top` +!y` *\"\";R._g.doc.body`+&)cnvs);`\"\x7F& = 1;} else`! Crelative\"`!}*.first`! !) {` -&insertBefore`#K#` ?1`!;&`'$(`!i+}}`'#(new R._Paper;` M&`$x)` -'`$y+` /'`.B#=`&G!` ,(lear()` ''_lef`'[#` *$top = 0`#H(&&`\"m(renderfix`)Z,}` n(` =%(`-F%` 4%`-E,`+.#` c)`4*&, fit) {eve(\"rapha`3H\"` O#`,7#`+X+, [` S+]`0v\"s`.[\"mmax(w /`->', h` ($`-##, `\"g\"` /!top, aspectRatio = fit ? \"meet\" : \"xMinYMin\", vb, sw`&.!x == null) {`-p'bSize) {`!J#1;}delete`\" $` ;!;vb = \"0 0 \" +`!b' + S` )$`%9#`&I$` T( =`\"L!` a\"x` N#y` V#w` )#h;}$`!P\"`%s\", {`#?#:vb, preserveA`\"R&:`\"]'});while (`\"%!&& top) {sw = \"`3S\"-`11\" in top`4Z#?` \"&['` <('] : 1;` 6$({` 0*:sw})` 9!_.dirty`#9!` %'T` )$ =` ~!prev;}`%R) =`%R*!!fit]`'!$`1Q%prototype`'[6`/H'`#6)`)7$`+z\", pos;try {po` 2%getScreenCTM() ||` 1\"`2b\"SVGMatr`(\\!} catch (e)` T)` 7/var `*.#- pos.e % 1`'B$` -\"f % 1`&w!` E!||`$L#` *$`4h$`*v$`',#` )!+` y!)` ]!s.` :#` 3)`/-!}`'e!` q!` 7\"`!:\"` c#top +`!/\"` b\"`(}'` 6#` `\"}`#x*`,\\!`#r,R.`*m)` ?!`*n#`*M\"c`$.*`''$c`/T+.remove`.y#`/>*` Z!bottom`*e' `*2\";`\"0\"desc = $(\"desc\"))`/d)`1z%`$N\"TextNode(\"C` +!d with Rapha\\xEBl`*K!R.`47#));c` b)`!)%` \"3fs`!C$fs\"))`'=-move`#9,`#:)` =\"`#?%`#5'.paren`!y!`*I!` %1`#@)` 8');for (var i`*j!his`%]#[i] = typeof` 3!` ,!= \"`!]$\" ? R._` t\"dFactory(i) :`$!\"}}`/\\\"et`\"E! = R.st`!'&method in el` =!`'l#` '#[has](` ?\") && !` b$` -)) {` /%` -\"] = (`#@&` /\"name) {`+K#`+\"-arg = arguments`+n(.forEach` h'el) {el`!)#name].apply(el, arg);});};})`!^$;}}})();` Z'`\"G#!R.vml`!R%`+,\"has = \"hasOwnProperty\", Str = String, toFloat = parse` (!, math = Math, round =` 0!.` (!,`3g!` +$max, mmin` (%in, abs` ($abs, fill`!/\" = \"fill\", separator = /[, ]+/, e`'B!`*^!, m`\" ! progid:DXImageTransform.Microsoft\", S` E!\", E = \"\", map = {M:\"m\", L:\"l\", C:\"c\", Z:\"x\", m:\"t\", l:\"r\", c:\"v\", z:\"x\"}, bites = /([clmz]),?([^` &!*)/gi, blurregexp = /`!^$\\S+Blur\\([^\\)]+\\)/g, val = /-?[^,\\s-]+/g, cssDot = \"position:absolute;left:0;top:0;`3\"!:1px;height:1px\", zo`,X!21600, pathTyp`!o!{path:1, rect:1, image:1}, oval` >%circle:1, ellips` =\"path2vml`+B)path`'d#tot`\")\"[ahqstv]`\"i!comma`%f!R._`!H!oA`\"&$Str` [\".match(` ]!`)N!(` F-2curve);` |&`#o!/g`2;!` F%` t/`*>!` u2`!s#r`\"R!` 9&replace(`%'!,`\"C'all`\")%`)K!s` [#vals = [], isM`.N\"` A#.toLowerCase()`-$!m\",`!-#map[` A#];args`!3%val`!.(value`*3#` {#&&`!2!.length == 2`*I!s +=`!K\"+` t(`!3# ? \"l\" : \"L\"];`!o%;}` g!push(`**!`!,\" *`&*!)`+s!`,U#` }!` |!` '*`+\\\"pa`\"Q&`#I\", p, r;`\"K\"[]`0-(= 0, ii`+d!`\"!#; i < ii; i++) {p` 8![i];r` \"$[0]`#G*;r`\"8!z\"`%v!r = \"x\"`17'j = 1, jj = p` \x7F%j < jj; j`!#!r +=`,p\"(p[j]`\"Q$ + (j != jj - 1 ? \",\" : E);}re`#)$` (!`\"`$.join(S);}`%g!pensa`$`!`(L(deg, dx, dy`%|#m`'g!matrix();m.rotate(- ` G!0.5, 0.5`#i%{dx:m.x(` ^#, dy:m.y` ($};}, setCoords`)f*, sx, sy`!:$, deg`!@#_`\"\x7F!_,`!K!p`!I#`/$\"pos = _.` %#, o` >!node, s = o.style, y`#k\"flip`.P#dxdy, kx =`,:\"/`!>!ky` %'y;s.visibility`1b!idden\"`*=!!sx || !sy`2('o.c`\":!ize = abs(kx) + S +` (\"y);s`#C\"`#{\"deg * (sx * sy < 0 ? -1 : 1)`!\"!`\"_&c`'B\"`$V%`$F);dx = c.dx;dy` $\"y;}sx` m!&& (`\"V!+`&b#`!##` +) y\"`-$\"y = -1);s.`#(#flip;`\"3#orig`3?!dx * - kx`\"7#dy` *\"y`!q!`$6$||`$8#size`\"!#fill`$7!getElementsByTagName` U!`3|\");` C#` \"!&&` #![0];o.removeChild` L!`#%\"`!.#) {`\"u2m.x` <$[0]`%z%[1]), m.y` %4`!C\".`2U$`#a# * y`\"U#c.`\"Y!y;}if (`\"D)` U!`%H#` /&[0] *`%L!s`%V%` 3'1` 5%y);}o.append`\"C(}`&j,` )!le\";};R.to`#<\"`(x)`&}% \"Your browser doesn\\u2019t support SVG. Falling down to VML.\\nYou are running Rapha\\xEBl \" + this.version;};var addArrow`!B)o, `/L!, isEnd`1m&u`2S%`0}\"`.,*.split(\"-\"), se =` Y\" ? \"end`0b!start\",`/I!` j\"`.;#, type = \"classic\", w = \"medium\", h` #';while (i--) {switch`2P#s[i]) {case \"block\":` '\"` q$` (#oval` $$diamond` 2%pen` $$none\":`!V#` y%;break;` =\"wide` F%arrow\":h` 55long` F$short\":w` :/default:;}`3%!stroke`)V!node`)J2\"` @\"\")[0];` &\"[se + \"`!P\"] =`#b!` )/`$ \"` =!w` ,/wid` 7#h`0D#FillAndS`!P$`%q)params) {o.attr`/i\"` $\"|| {`&U\"nod`\"'&, a` ;&`0;\"`\"C!`0=#xy, newpath = pathTypes[o.type]`-W!`!/\".x != a.`/|!` ,#y` -\"y` ('`\",!` 1\"` %\"` 1&height` 6\"` %#` 3&c` x#c` t(c` z#c` v(r` C#r` >(r` C#r` =)` -#), isOva`.]!val`\"1.`!3!!=`!B'||`!2\"` *'` v!a` p\"` y%` -\"` Q(`!R\"`!F!` *'y), r`*A!o;for (var par in`$l&if`#f$[has](par)) {a[par]`$;!` 7!par];}`.S!`$U#) {a.`$^#R._getPath`\";$(o);o._.dir`-t!1;}`$+$ref`\"\\!`%O!` )!`!j%href);` &#title` @&` )\"` B%` *!` B&arget` B'` *\"` D&` +!` F%cursor` J!` $%`#k&` +!);\"blur\"`##& && o.blur`&w$blur`2m\"` *#`\"j!` B!`+_\"= \"path\" ||`'k$) {`!]!`'t'2vml(~Str(`#K\"`.[,indexOf(\"r\") ? R._`(P!oAbsolute` O$ : ` ##`!O!`!:'image\"`)\x7F!`2Y\"pos = [a.x, a.y]`$G!`2p$` 4\"`(f!,`(O%];setCoords(o, 1, 1, 0, 0, 0);}}\"transform`#\"-` 1%`\"\x7F$` (%`!c\"`(C\"`1S#cx = +`)Q!, cy` %$y, r` 2$`'h\"` %! || 0, r` @$`)F\"` 1&;`#`(R.format(\"ar{0},{1},{2},{3},{4` )#` \"\"x\", round((cx - rx) * zoom)` /&y - ry` &/x +` :4+` >0cx` *$`(D-if (\"clip-rect`#P'`#(#rect`4`#`)c#'` E%']`4_$separato`''#rect`4E# == 4) {rect[2]`#B!` $$+` $$0];` #!3` 5'3` 5'1]`0 !div`/c$clipRec`.I!R._g.doc.create`2D#(\"div\"), d`02! = div`0=\";` -\"` `!`$J)rect({1}px {2}px {3` '!0}px)\",`!H!`\"<\"!`!9)) {` l#posi`25!= \"a`(X#\"`!($top = 0` '$left` $(`0v\"= o.paper` )#+ \"px` Z%`0}#` ;&` *#` ?#`&g#rentNode.insertBefore(div,`#9!);div.appendChild`,o!)` X\"`#Q%`#,!`.l#!`%(0`+b#` H%`-T%` *$`#h\"`#_$\"auto\")` k#o.text`,J#var ` '$S`$G#` 7&`$N#`*5#fon`!%\"` D)` 1\"`.:%fon`.d%['font-family'`2v\"` J.F` :! = \"\\\"\" +`1O$` N*`'g#\",\")[0].replace(/^['\"]+|['\"]+$/g, E) +` e!`!6+size`!*5S`-U\"` >/` W+w`%/!` O5W`%\\$` @1`!B,tyl`!87`$&#` ?0)`+6#arrow-star`+0+addArrow(res,` Z%` D'` T,end` 8Fend'], 1` \\#`%2#opacity != null ||` N%stroke-`(q!']` 0.`1Z!` $/src` \"0` i!` TP` 9%`!e#` .2fill` $:` _#dasharra` (:miterlimit` *9linejoin` $=cap` :&`){#`#;!`.y#get`.a#sByTagName(fillString), new` J#false;` %$ill &&` #![0];!` )$(` F'` E\"`/i\"Nod` m)`.z\"o.type == \"image\" &&`$f') {fill`$w!`+,%src;}`%@(&& ` u!.`/K!true` w\"` +%`%m2== \"none\"` '.`#'%` m&`\"X\"`'?!` -$`!a&fi`#U%isURL`3})` 8\".match(R._ISURL`!\\\"` H!` \x7F$`0&&`!m!ode &&`$A\"remove`/~\"` i!`$\"!.rotate`\"M#` -\"`#$\"` s![1]` .\"`#]\" \"tile\"`4=!bbox`/7!getBBox(1` k#`2m'bbox.x + S +` '\"y;o._`\")!pos = [` =\",` 7#];R._preload`\"4\"[1], func` n!() {` Y$s`-~\"[this.offsetWidth, ` ''H`-F!];});} else`#\"#color = R.getRGB`#Z*hex`\"_(E`\"V*solid\"`#x!` I2error`&.!res` Q\"in {circle:1, ellipse:1} ||`$u.charAt() != \"r\") && addGradientFill`-[(` Q!,`(F!)) {a`&[#`&s#;a.g` R#`3*'ill`%1+`&v#`4M\"\"`,((`.~' || \"` %.`'1#`.j$= ((+ a`,i-+ 1 || 2) - 1) *` A\"`/@%` *1`#L2o` >+;`!1&mmin(mmax(` -#, 0)`0X!`).\"` @%` ##`*=&`+\"'`%W$`#<#}}`(^!append`(W(var `08#`-28\"` >\"`$d\"` $?[0]`-q!` r%`$7\"!` )#`-\\#` 5%` >%`-\\'` c%`+ \"`1}*`-V'`23%`,X,`2--`1gK`1a/`1M;`1G9`1A7) {` -\"`/3&;}`\"C+`.u0` 2%`!s&` ]'`/`-` `&0`\"k7== 0`%\"!(`!H(`$[!`%c(C`+i3` 8\");` S&`$;-` q'`&\x7F$` i'.hex`(#(`)C#`$A-`(xP` {(`)1-var `\"`! = (toFloat`\"/#`\"u,) ||` p\"0.75`)WA`#T6`$A!&& (`!5$`\"D&`!'$` F4`#?'w`0C! =`!}\");` g\"&&`\",#< 1` I!`!c$*` A$` O11`$V&`+]-`'H6` _'joinstyle`/)%`'|3\"`(\\!\"`!'%`(f&` H.`(|,8`!80`(l!`!?(endcap`!22` G\"== \"butt\" ? \"flat\" :` 0:square` K!` #$: \"round\"`,C(`+10`1$#` )% = {'-':\"shortdash\", '.` (%ot\", '-` '&ash` ,%` &,` 2#. ':\"` =$` '!` p#--':\"long` (% .` 8#` G$`!%!` ;$` +&` '+dot\"}`$M$dash`%&$`\"/%[has]`\"K9?` B'` .6] : E;}`0E&`1:$`2!(`+n$}if (res.typ`-n\"text\") {res.paper.canvas.`!l!.displ`$ !E`,w\"pan = ` D&span, m = 100, fontSize = a.font &&` ##.match(/\\d+(?:\\.\\d*)?(?=px)/);s =` x!`!2\";` S&(s` %\"` i$);a['font-family`'F$` 8!F` .!`*~\"` 7)` L&`)\x7F\"` I)W`*0$` 4,` L&`!Q!` H)S`$j#` 3+);`\"X'`-^$` ;%ize`)V\"` <%&&` #%[0`-r#0;` |#` `\"` \"%* m + \"px\";`$_!extpath.string`!V\"pan.innerHTML = Str`%+\"` ?*).replace(/</g, \"&#60;\"` ,'&` 0#38` *)\\n` 3!<br>\")`0/\"brect`$W$getBoundingClientRect()`!l!W`$`!w = (` O!.r`#s!-` \\\".left) / m` H!H` H!`1(!` 9\"bottom` E%top` C&X` H!x` '!Y` '!y +`'!!H / 2;(\"x\" in`,O#`.4!y` &'`/`\"`'O\"th.v`4S!format(\"m{0},{1}l{2` #!\", `,e!(a.x * zoom)` )&y ` \"-` =% + 1`#-#dirtyattrs = [\"x\", \"y\",`)I#, \"font\",` \"\"`'?#` &%`&y\"` &%`&U!` $&ize\"];for (`!%! = 0, dd =`!+'.length; d < dd; d++) {if (` ;&[d]`\"x(`*\x7F!_.` ;! = 1;break;}}switch (a['text-anchor`/F!case \"start\":`&Y+yle['v-` M\"lign'] = \"left`'[\"bbx`+e#W`$a!`!*\"` m\"end` ID`&A!` f(-` c-default` JCcenter` f(0`\"d$` F7ker` Z\"true;}}, addGradientFill = function (o, g` 5#, fill) {o.`%j$` \"$|| {}`&,!` ++, pow = Math.pow, opacity, oindex, `/}\" \"linear\", fxfy = \".5 .5\";` c#.`!?$ =`!I%;` )'Str(` '$`*~&R._radial_`!{'`\"4%all, fx, fy) {`!A$` K\"\";if (fx`-U!y) {fx`-\x7F'fx);`!a!` (%y);pow(fx - 0.5, 2) + ` .!y` ('> 0.25`*C!` [!m`$<!qrt(` 3!-` L\"` Z') * ((fy` U!5) * 2 - 1) +` +!;`#'#fx + S + fy;}return E;})`\"w(` #$.split(/\\s*\\-\\s*/)`\"G!`4#%`$##) {`$m!ng`0|!` T&hift();` 1$-`0v&ngle` g\"isNaN` *#`)n!`!M!null;}}`*k!ots`,{!_parseDots`$V&` ^!!dot`*I\"` Q'o`&7!shape || o.node` J!dot`+G$`&\x7F!removeChild(`'4!;fill.on`'s$` *!method = \"none\"` /\"color =`!_![0]` *\"` 0'2` 4$`!)' - 1` B$var cl`..\"`-8'i`-=\"ii` X#`-4%i < ii; i`-8!` u!i].offset &&` g!.push`\"@!` 1'`%-\"` -$`!8!);}`!_&s =` W\"`!]#?` (\"join() : \"0% \" +`*4!`!u#`%=)`($#) {` =!`(:$`$@$Titl`#,$focus = \"100%` ((siz` P!0 0` +(posi`)A!=`*L!` 4\"`&+$0;} else`!!3` a#` F$(270 -`'#\") % 360;}o.append`%5(`(1$1;}, Elem`(2\"`*g&node, vml) {this[0`-d!his`&C! = `&J!node.raphael`&&$` @!id`'M\"oid++` :)` 5!` =#` F\"X`\"C!` &!Y`\"N!` &!`-x${}` *\"paper = vml` +\"matrix`!\"!` %\"()` 3\"_ = {transform:[], sx:1, sy:1, dx:0, dy:0, deg` \"!irt` 6\"irtyT:1};!vml.bottom`,?!` $'`!y\"`!&#prev`!P\".top;v` \"\"` J%top.next` L%` 9$` *\"` ^\"` :#`*2\"`(O!elproto`\"8!el;`$;#.` 0!`%=#` <#;` D#.construct`)l!` K#` 5%`\"f%`$y)tstr) {`'e!str =`!?\"`+a&`#J\"` T&;`,M!vbs`$\\$`$1!._viewBoxShift, vbt =` A!? \"s\" + [vbs.scale, ` \"%] + \"-1-1t` :%dx` :\"dy] : E, oldt`)E!vbs) {old`#F!`!h!`2E!`!~!`2=%/\\.{3}|\\u2026/g,`!p- || E);}R._extractT` 3$(this`!w\"+` }!)`$&!`&>%`&J'.clone(), skew` 4$skew, o`(<(, `0}!, isGrad = ~`!p!`'X%.`)K!.indexOf(\"-\"), isPatt = !` /:url(\");`!M#`\"?!late(1, 1`15$` e!||`!9$||`!W\"`,p%image\") {skew`(j&\"1 0 0 1\";` 3!`.?#`,U$`\"0!`4N\"rix`3?#`!*$`!#!&&`\"T\".noRota`&J!|| !` /\"isSimple`1Z!style.filt`*;!`\"&$oF` -!(`$ \"bb`#O$getBBox(), b`&C!` ))1), dx = bb.x - bbt.x, dy` -\"y` ,#y;o.coordorigin = dx * - zoom`0e$y` )%;setCoords`%^#1, `+E!, dy, 0)`.}%`!~-E` K-`\"]\"`'}!x`%Y#` (\"y` &$d` /%d` +%rotate);}` n8`$V)Str(` '\")`$V+`#\\#` *\"();}`(r!&& `&F\"`(C(=`)=!);`*e';}`+M%`!O\"`+F)deg, cx, cy`+R$his.removed`+L*;}if` M!`+h-;}` 1!`!\x7F!deg)`&<#separator`&F\"deg`4h$- 1) {c`)c!oFloat(deg[1]);cy` &+2])` x$` +(0` E#`!E\"`\"j!` k!cy`!(\"cx` 3%|| ` @&) {`&|\"o`*}%`&d&;` ^!bbox.x +` #\"width / 2`!U\"` /!y` 5$height` 9!}`$'#`1O\" = 1`0I\"`$5%`$>-.concat([[\"r\"`29!`$$$]])`$M4`+/%`$Z*`'W!`$@?`))!`$C!x`$/4x`$8*`)H!`#q%x`$@!` a\"` *'0]) || 0;` I!`)I!` *!`!F%_.bbox) {` #'.x += dx`#\"\"` -#y` /!y`#H#`\"xAt\"`*5$`\"}7`)o!`#')sx, sy`'TGs`#4$s`#$4s`#-+s`#0(s`#6\"`'i)sx`'[!`'h)sx[3]);isNaN(cx)`'R&`';!` 1$y` 2#y` 0%`![\"` \\'0]);s`(6*`!M!sx`'hp}` ^\"` R&?`(R4 : cx`\"O\"`!<'` H#`(i/ : cy`(CGs\", `$p*`%T!`)O.`%U2hid`%\\*) {!`%M(`.S&node`/S#display = \"none\"`&P5how` BXE` a4_`$%#` m,`'=6{};}` #$x:` ?!X +` G#bbx`)\x7F!) -`%%\"W / 2, y` C\"Y` /$H, `$t!` 1\"W, `$R\"` )\"H}`14(`!;!`!?< || `\"j\"`\"Z!parentNode`1$'` :!paper.__set__ &&`!I\"` )).exclude` |!);R.eve.unbind(\"raphael.*.*.\" +` Y\"id);R._tear` P!,` k'`%{#`!H+`!q#Child`$M&` E#shape`!S%` )!` >9` =!);for (var i i`%\"\"`-s#[i] = typeof` 3!` ,!= \"`#K$\" ? R._`#E#Factory(i) :`) !`#4#`#c$= true`$8'attr`$4)name, value`-??if` L\"`+\"+res = {}`\".&a`\"0$`!-!s` o(` ,![has](a)` |!s[a`\"Q!` 4&a];}}res.gradient && res.fill`\"k!`)>!`(e!` -&` =!` J$`-y!delete` +);re`+['`!/$`+]'`)6$re`\"J#`\"x!`.!(R.is`#2#\"string\")`\";#`\"u$fillS` 6!`%T%`\"5!`!n/` 2'`!r&`#k(` .+;}var nam`#p!name`1[-, out`$#+i = 0, ii` N#s`2##; i < ii; i++) {`!|\"` =\"[i]`0K!` /!`$a,out[name`$L+` .!;} else if (`#)!`)p'customAttribute` K#,`'X')` j0` A8.def`!+$` M)R._availableA`!S(`-O$ii - 1 ?`#,!: ` T$s[0]]`%8\"`\"1& &&`(C\"`%44array`!v$`#t'`#k,`#c3`!5$[i]`#G)` z![i])`/D%ou`%?#params`$?!`!T\"!`)O%` 5\"`!@\"` &\"`\"m%` F!;}`!o9object\"`(a!(` c%nam`,R(key in`!?#) {eve`.M&attr`.S!key + \"`.X(`.P\",` S#[key])`#i\"` c%`! !` z#`%&7`+N(`%L3`+f!key`\"'!`!+#` ')`&a=key`&p-`#x#`&`;key].apply`1'#[].concat`\"C#`\"V\"`0g#`(\\\"key] =`\"p(`#h&sub`#k&`\"U#par`\"*\"` 7\")`%4%[` +\"` f#` &$;}}}`#`'.tex`.:!`!=!typ`%N!\"text\"`1N#` >!path.`-(\"`!T%` 4!;}setFillAndStrok`4(\"`$u$`',&`0]\"`1B%toFront`1A)) {!`1t)`!P$`4$,append`3~2`$&!` Q%`$3\"top !`$E\"`%,\"_tofront`!e#` A&)`/x$`!^-Back`!^,`2m@`!l1first`!{!`!P$`!}\"`#k#` ?,insertBefor`#Y\"` =!`!q#` b6);`\"E!back`\"6/`$\"3`!&\"After`\"B)elem`1W\"`\"2@` G#.constructor == R.s` ')) {` C# = ` ##[` T$`-b\" - 1`.f#` 1$`\"M!nextSibling` \\&`\"qE` R5`0`%` Z4`&E3}R._`!'\"after`#i#` ^#`%~@`!^(`\"w~`#d10];}`\"mX`&K!` C\"b` ?&`\"9Iblu`&I*size`.I#s`.F$`!W!runtimeStyle, f = s.filter;f = f.replace(blurregexp, E)`3)!+ size !== 0`),$`.Y!`!8$size;` h$ = f + S + ms`2'!Blur(pixelradius=\" +` m%|| 1.5) + \")\";s.margin = R.format(\"-{0}px 0 0 ` %\"\", round` U+`&d&`!@(` h(0;delete`\"s\"`!z&`*.,R._engine.path`#U)pathS`/U!, vml`#f#el = createNode(\"shape\");el.style.cssText = cssDot;el.coord`!}!= zoom`#0#zoom` 6%ori`!v\"vm` %);`3O! = new E`%m\"(el`!O\", attr = {fill:\"none\", s`1-!:\"#000\"};`\"!& && (attr`\"H$` 1&);p`2B# \"path\";p` =$[];p.P` &\"E;`2(-p`!@\");`!u!anvas`*b)el)`\" !skew`#.,kew\");skew.on = true;el` S)skew);p.` Y#skew;p.transform(E`(M%p`$f)rec`3;*vml, x, y, w, h, r`$o#`\"A#R._rectPath(` 9*, res`$5#path`%Y!), a = re`&;#;res.X = a.x = x` +!Y` +!y = y` +!W` +!wid`!-!w` /!H` /!height = h;a.r = r;a`$;(` F!`$:$rect\"`\"O$re`'>*ellips`-a*`\"Q'rx, ry`\"T#`\")+`!{5x - r`\")&y - r`\"(&rx * 2`\"$%ry` &%`!i$`!M#\"`%m.res, {cx:x, cy:`!\\!:`!^\":ry}`$t%`\"6,circl`\"+6`!lP`\"1*`\"4&`\"/$`\"%-`!>\"`!oB:r`!r6imag`!x/src`'%(`&j@`&t3`\"I!({`*q$`+#!}`\"],, node` +#`2\"\"fill =` 3!.get`+y#sByTagName(fill`+:#[0];a.src = src`'Ch`'o4`#3!\";fill`4+' =`!l\" &&`!t\"remove`+H\"fill)` L\"rotate`+n$` .!`!t&` *!`! $tile\"`!5!_.fillpos = [x, y]` ,'`/\\#[w, h];`!6!`,](`!5\"setC`0+!`%_\"1, 1, 0, 0, 0`%H5t`0m\"`'V1text`1@;,`%m$` 0(`/a!), o` )+text` 6\";`$T! || 0;`$O!` %\"`!F#` \"!|| \"\"`0}!.v`4`)m{0},{1}l{2` #!`4c%x *`2U!)` )$y ` \"+` 9% + 1)` w\"`!M$ok`$H$o.s`2.\"= Str(`\"a!;o`0Y*`3Lc\"0 0\"`3\\I`3u!`3\x7F&`41#font:R._availableAttrs.font`$a\":text};p.`$S! = el`3}&`'y!p`\"a% = o`4I'text\";p`*$\"`%h$`\"o&` 1$`)E\"` &$`)C\"` &$w = 1` %%h` )!`,i-p`\"E\")`46,o)` \",`+t!;vml.canvas` 0)el)`#L!skew`'$,kew\");skew`$`*` T(skew);p.` Y#skew;p.transform(E`(b%p`(\\)setS`%0\"`(b&`+z!, `+l\"`(c#cs = this`!u$`&&!;` -!`,F%idth` -\"`,D&` $!;` <#= +` @\" &&`!!# += \"px\");` L$= +` P#` A!` $#` >%c`!!,c` }.cs.clip = \"rect(0 \"`!1%+ \"` )!` p$ \" 0)\";if (`!v!_viewBox) {`\"z)V` 1\".apply` F!,`\"k\"` H%;}`#\\#thi`,A*` U&`#\\)`1]&, fit`!-!eve(\"raphael` M'\"`!'\"`!$+,`.N\"` Y']`&!\"`\"z$`$6&`$h$` -$` (\", `*M#1 / mmax(w /` X\", h /`%=$, H, W`# !`!c\"H`#d% / h;W`$&$ / w` B!w * H <` 0\") {x -=`%+$- ` 9!) / 2 / H;}if (h * W <`&N&y` N!` \x7F#- ` ;!` L$W;}}`\"^)`18$`\"d$!!fit]`&h\"` <$Shift = {dx:- x, dy:- y, scale:size}` L\"forEach(`$B&el) {el`(h'\"...\");}`(q%`%0#`(J!`)w%`%;'initW`-|!`) (n`({#doc`#@!n.document;doc.` d\"StyleSheet().addRule(\".rvml\", \"behavior:url(#default#VML)\");try {!doc.namespaces` U! && ` (+add(\"` n$urn:schemas-microsoft-com:vml\");`\";&`\"&)tagName) {`\"q#`!}&`0?$\"<rvml:\" + ` G#`)C!class=\\`!<!\\\">\");};} catch (e) {` VX` },xmlns=\\`\"02.`\"?#\\`!>3`*1(`$p#(R._g.win)` 1'`!?\"`!`)`.)$`/F!R._getContainer`+V#0, arg`%?!s), c` 7$ = con.` '%`*)'con`*)&,`*W%con`*W$x` (#x, y` $#y`)e!!` g%) {throw new Error(\"VML`!6'not found`'u!var res =` L!R._Paper, c = re`0C$`\"@#.`$\"/div\"),`0w\"c`0l#`4-! || 0;`4*!` %\"`/f) || 512`0I%`\"_$|| 342;re`03,re`0Vy`\"G!oord`.&#zoom * 1000 + S +` &(` D&orig`*j!\"0 0\"` 3!spa`%O$`\"|0span`!)#span`#,\".cssText = \"position:absolute;left:-9999em;top` #%padding:0;margin:0;line-`\"7\":1;\";c.appendChild(`!.$`3~!`!*&R.format(\"top:0`!+\"`$J#:{0}`#>#:{1};display:in`!&!block;`!k%relative;clip:`4R#{0} {1} 0);overflow:hidden\"`'`#`'~$)`'I!`(B'= 1`4_\"`#F\"body`\"$)c`\"%!le`0/!x +`$i!;cs.top`&U!` (&`!\\$ = \"`#S$\";} else {`!+).first` }!) {` -&insertBefore(c`*+'` E(` j%` 4&`!c+}}res.renderfix`+K,}`1E$res;};R.prototype.clear` @,R.eve(\"raphael` >\"\",`2&!)`2e\"`)\x7F\".innerHTML = E` 4\"`&gB` B%`&0z`&-*;\"`!p)`#8(`!G%`!T#bottom =`\"O!`%4#null`#/+remov`/9-`#0+` ?\"`#+1parentNod` c$`!C'` =\");for (var i in` ^\" {this[i] = typeof` 3!` ,!= \"`!E$\" ? R._` t\"dFactory(i) :`\"%#`%O#true;};var set`\"9!`$]!st`!3&method in el` =!)`'\\\"` '#[has](` ?\") && !` b$` -)) {` /%` -\"] = (`#4&` /\"n`4K)`2u-arg =`2c&`'U$`#1!forEach` h'el) {el`!)#name]`3K#el`3N!);}`4Z!)`!^$;}}})();oldR`$]#was ? (`4f!.` .# = R) : (window` *)`!L$R;});"));
/* Map path and default settings - you can edit this */

var simplemaps_countrymap_mapinfo = {
  map_name: "country",
  state_bbox_array: {
    KAZ4830: {
      y: 170,
      x: 606,
      x2: 615,
      y2: 183
    },
    KAZ3236: {
      y: 350,
      x: 87,
      x2: 251,
      y2: 527
    },
    KAZ3202: {
      y: 75,
      x: 464,
      x2: 680,
      y2: 221
    },
    KAZ3203: {
      y: 172,
      x: 396,
      x2: 765,
      y2: 369
    },
    KAZ3201: {
      y: 152,
      x: 1,
      x2: 198,
      y2: 298
    },
    KAZ3206: {
      y: 155,
      x: 738,
      x2: 999,
      y2: 383
    },
    KAZ3207: {
      y: 321,
      x: 669,
      x2: 885,
      y2: 497
    },
    KAZ3204: {
      y: 1,
      x: 479,
      x2: 674,
      y2: 135
    },
    KAZ3205: {
      y: 42,
      x: 660,
      x2: 803,
      y2: 221
    },
    KAZ3196: {
      y: 32,
      x: 331,
      x2: 533,
      y2: 290
    },
    KAZ3197: {
      y: 303,
      x: 308,
      x2: 532,
      y2: 491
    },
    KAZ3195: {
      y: 171,
      x: 177,
      x2: 434,
      y2: 396
    },
    KAZ4829: {
      y: 456,
      x: 742,
      x2: 748,
      y2: 463
    },
    KAZ3251: {
      y: 368,
      x: 478,
      x2: 599,
      y2: 549
    },
    KAZ3252: {
      y: 369,
      x: 555,
      x2: 718,
      y2: 492
    },
    KAZ3198: {
      y: 251,
      x: 15,
      x2: 245,
      y2: 376
    }
  },
  paths: {
    KAZ4830: "M613 182.5l-1.4 0.7-1.1-0.7-1-1.8-1.1-1.5-1.2-1.5-0.9-1.5-0.2-1.6 0.2-1.5 0.7-1.3 0.8-0.8 1.6-0.7 1.1 0.3 1.9 1.5 1.1 1.7 0.8 0.4 0.7 1.8-0.2 2.5-0.6 2.3-1.2 1.7z",
    KAZ3236: "M103.1 403.9l0.5 0.1 0.3-0.1 0.4-0.5 0.2 0 0.1 0.4 0.2 0.1 0.2 0.3 0 0.8 0 0.4 0.2 0.3 0.3 0.6 0.1 0.2-0.5-0.1-0.3 0-0.4 0.2-0.3 0.1-0.1-0.2 0.2-0.4 0-0.2-0.3-0.1 0-0.2 0.1-0.3-0.2-0.4-1.1-0.8-0.4-0.6 0.1-0.6 0.2 0 0.1 0.5 0.4 0.5z m-15.3 0.3l0.3 0.2 0.3-0.1 0.1 0.8 0 0.9 0.3 0.6 0.9 0.3 0.4 0.4 0.4 0 0.8-0.6 0.3-0.1-0.4 1.1-0.7 0.4-0.8-0.1-0.7-0.4-0.4-0.4-0.1-0.3-0.1-0.4-0.1-0.6-0.3-0.5-0.9-0.9-0.4-0.9 0-0.7 1.3-3.1 0.3-0.3 0.4-0.2 1.1-0.3 0 0.6-1.5 0.9-0.3 0.8-0.1 0.5-0.4 0.8 0 0.6 0 0.5 0.3 0.5z m7.7-5.3l0.2 0.4-0.3 0.7-0.1 0.3-0.2 0.2-0.1 0.2 0.2 1.3-0.2 0.7-0.5 0.5-0.6 0.1-0.4-0.2-0.2-0.8 0.1-0.8 0.2-0.9 0.1-0.7 0.1-0.6 0.3-0.2 0.5-0.1 0.5 0 0.3-0.2 0.1 0.1z m57.6-11.1l-0.7 0.2-1.2-0.4-0.9-0.8-0.7-0.8-0.2-0.8 0.3-0.5 0.4-0.3 0.3-0.3 0.1-0.2-0.1-0.3 0.2-0.1 0.8 0.1 0.5 0.1 0.4 0.6 0.4 1 0.3 1.2 0.1 0.5 0 0.8z m92.1-23.8l1.7 4.6 4 27.4 0 0.2-4.1 1.3-13.7 4.4-0.1 0.1 0 8 0 7.8 0.1 7.9 0 7.8 0 7.8 0 7.8 0 7.7 0 7.7 0 7.7 0 7.6 0 7.7 0 7.5 0 7.6 0 7.5 0 15-1.7-0.1-0.8 0.1-2.2 1-2.2 0.3-1.5 0.6-3.1 0.2-0.7-0.3-0.7-0.4-0.5-0.4-0.4-0.5-0.7-1.7-0.3-0.5-0.4-0.5-1.5-1-0.9-0.8-0.7-0.9-3.2-5.6-3.6-4.9-0.3-0.6-0.1-0.7 0-0.7 0.1-0.6 0-0.5-0.2-0.7-0.6-0.7-4.1-3.8-9.5-6.4-3.8-2.5-1.7-0.7-1.9-0.2-9.6 1.7-4.3 0.8-1.6 0.3-5.4 2.2-1.4 0.8-3.8 1.4-5.2 4.4-3.3 3.2-3.7 3.8-1 1-0.1-0.7 0.2-0.7 1-2.4 0.2-1.2-0.2-1.3-0.2-0.7-0.3-0.6-0.5-0.4-0.2-0.3-0.1-0.4 0.2-1.1 0-0.4-0.2-0.3-0.7-0.8 0.3-0.5 0.2-0.6 0.2-0.6 0-0.7 0.2-0.5 0.3-0.2 0.4-0.2 0.4-0.3 1.2-2.3 1.4-1.8 0.2-0.6 0.1-0.4 0-0.7 0-0.4 0.1-0.2 0.7-0.8 0.2-0.3 0.1-0.3 0-0.5 0.1-0.3 0.4-1.3 0.2-1.2 0-1.2-0.3-1.3-0.6-1.4-0.5-0.8-1-0.7-0.3-0.7 0-0.7 0.2-0.3 0.2 0.4 0.2 0.7 0.3 0.7 0.4 0.3 0.2 0.1 0.9 0.6 0.1 0.2 0.1 0.4 0.1 0 0.4 1.3 0.2 0.5 0.3 0.3 0.2-0.3 0.3-0.6 0.2-0.8 0.1-0.7-0.2-1.5-0.6-0.9-0.9-0.6-1.1-0.1-0.1-0.8-1.3-0.8-2.5-0.9-2.4-0.2-0.4 0-0.3 0.1-0.3 0.1-0.3 0.2-0.1 0.2-0.3 0.6-0.1 0.2-0.6-0.3-1.7-2.2-0.9-0.7-0.6-0.1-2.1 0.3-1.6 0.7-0.9 0.6-0.5 0.2-0.5-0.2-1.5-1.8-0.3-0.5-0.6-1-0.3-0.6-0.3-0.5-0.2-1.3-0.3-0.6-1.1-1.4-0.4-0.7-0.3-0.1-0.3 0-0.2-0.2-0.2-0.4 0.1-0.3 0.2-0.2 0.1-0.4-0.1-0.7-0.3-0.6-0.4-0.4-1 0.5-1.2-0.1-0.6 0.1-0.1 0.2-0.4 0.4-0.2 0.1-0.3-0.1-0.6-0.3-0.3-0.1-2.3 0.5-2.2 0-0.3-0.3 0.1-0.8 0.8-2.7 0.3-1.4 0.2-4.5-0.2-0.6-0.7-1.6-0.1-0.3-0.1-0.3 0-0.8-0.1-0.3-0.3-0.4-1-0.7-0.2-0.6-0.1-0.5-0.2-0.1-0.5 0.2-0.5-0.1-0.4-0.3-0.6-0.9-2.3-4.8-0.2-0.6-0.1-0.9-0.5-0.7 0-0.3 0.1-0.8-0.1-0.3-0.2-0.4-0.7-0.6-0.1-0.3-0.2-0.7-0.4-0.5-0.3-0.7-0.2-0.3-0.3-0.1-0.5-0.2-0.2-0.2-0.2-0.4 0-0.3 0.1-0.3 0.1-0.5-0.1-2.8-0.1-0.7-0.2-0.6-0.6-0.8-1-0.7-2.1-1-8.6-1.6-1.1-0.5-0.5-0.4-0.5-0.5-0.5-0.6-0.2-0.7 0.2-1.6 0-0.7-0.2-0.7 0.2-0.2-0.2-0.7-0.1-1.1 0-1.1 0.3-0.6 0.4 0.9 0.4-0.4 0.2-1 0.1-0.5 0.3-0.2 0-0.4 0-0.5 0.1-0.3 0.3-0.3 0.2 0.1 0.3 0.3 0.2 0.1 3.9 0.8 1.2-0.1 1.1-0.4 0.6 0 0.3 0.6 0.3 0.1 2.2 0 0.3 0.4 0.3 0.1 0.3-0.2 0.3-0.6 0.1-0.1 0.3-0.1 1.2 0.1 0.7 0.1 0.5 0.3 0.9 0.8 0.7 0.9 0.2 0.2 0.6-0.1 0.3 0.1 0.1 0.2 0 0.3-0.2 0.1-0.3 0.1 0.4 0.5 0.7 0.5 2 0.9 0.7 0.1 0.7 0 0.7-0.2 0.6-0.3 1.3-1.3 0.1-0.3 0.4-0.2 0.6 0 0.3 0 0.7-0.2 0.4 0-0.2 1.1 0.7 0.2 1.7-0.4 1.6 0.8 0.5-0.1 0.4-0.4-0.1-0.5-0.4-0.4-0.4-0.3-1.4-0.8-1-1-0.6 0-1.2 0.4-1.1 0.1-0.7-0.6-0.8-2.6-0.6-1-0.8-0.8-2-1.2-0.6-0.7-0.9-0.1-0.2-0.1-0.1-0.2-0.3-0.3-1.6-1-0.2-0.6 0-0.8 0.1-0.7 0.4-0.3 0.2-0.1 0.1-0.3 0-0.3-0.1-0.2-0.2-0.2-0.4 0-0.3-0.1-0.1-0.4 0.3-0.6 0.5-0.3 0.7-0.2 0.5-0.1 0.6 0.1 0.3-0.1 0.3-0.3 0.4-0.7 0.3-0.2 1-0.7 0.6-0.3 0.6-0.2 0.4 0.3 0.4 0.6 0.5 0.5 0.5 0.1 0.2-0.5-0.1-0.7-0.4-0.6-0.3-0.5-0.1-0.4 0.1-0.4 0.2-0.4 0.1-0.3-0.1-0.3-0.3-0.7 0.2-0.5 0.3-0.6 0.7-1.1 0.2-0.5 0.2-0.6 2.2-2.6 0.2-0.6 0.1-0.8 0.1-0.6 0.4 0.3 0.6 1.2 0.9 0.4 1.2 0 1.2-0.3 0.9-0.6 0.8-0.9 0.2-0.1 0.5 0 0.3-0.1 0.3-0.6 0.3-0.7 0.3-0.6 0.5-0.4 0.3 0.4 0.3 0.4 0.1 0.6 0.1 0.4 2.1 0.4 0.4 0.2 0.7 0.6 0.4 0.2 0.5-0.2 0.8-0.6 0.4-0.2 0.6 0.1 1 0.3 0.5 0.1 7.5-1 1.1 0.3 1.8-0.3 1.1 0.3 4.2 1.8 2.4 0.4 1.2 0.7 0.6 0.2 2.3-0.3 0.6 0.1 1.4 0.6 0.6 0 0.5-0.2 1-0.6 0.6-0.1 0.1-0.1 0.3-0.3 0.2-0.1 0.6 0.2 0.4-0.3 0.2-0.3-0.1-0.3-0.4-0.3-2.6-0.5-0.2-0.2-0.3-0.4-0.2-0.2-0.8-0.1-1.1-0.3-3.1-1.6-0.6-0.1-0.2-0.1-0.5-0.7-0.1-0.2-0.3-0.1-0.9-0.1-1.2-0.5-0.4-0.3 0.2-0.5-0.3-0.5 0-0.3 0.2-0.4 0-0.3-0.1-0.5 0.1-0.9 0.1-0.6 0.3-0.4 0.5-0.2 0.4-0.2 0.2-0.7 0.4-1.2 1-1.7 0.1-0.1 4.3 1 12.1-3 5.4-3.4-1.5-1.8 2.3-2.3 3.9-5.3 2-0.6 1.4 0.1 0.8-0.7 19.1-4 8.4-0.4 5.6-4.8 4.9 4.4 1.7-1.1 2 2 0.1 3.4 2.7 1.3 2.4-0.3 0.9 1.6 1.4 1 2.5-0.2 1.9 0.7 1.2 0 2.7 1.3z",
    KAZ3202: "M672.7 120.3l0.7 0.8 0.7 3.2 0.6 22.4-1.2 1.3-2.5 1.8-1.6 4.2 0 3.9 3-0.6 1.7 0.1 0.6 1.4 2.8 1 2.6 1.5-2 1.3-2.2 0.4-3.7 1.5-2.4 1.8-0.6 1.9-1.1 2.1-1.6 2-4.6-0.7-4.5 0.8-1.3-0.2-0.5 1.3 0.4 1 1.3 2.1 0.6 1.5-0.1 0.8 1.4-0.6 1.3-1.5 2.3 0.8 1.1 0.9-1.3 1.6-2.4 1.9-4.3 1.4-1.4 0.8 0.4 1.3-2.2 1.3-1.3 0.5-0.7 1-4.1-1-1.9 2.4-0.1 1.2-1.1 1.7-2 0.6-1 0-1 0.7-1.7 0.9-0.2 0.9 0.2 0.5-0.7 0.8 0.1 1.5 0.9 0.6-1.3 1.3-3-1.2-0.9 1.2-1.2 0.4-0.4 4-6.5-0.6-2.6 0.6-1.1 0.7-0.1-1.5 0.8-1.1 0.4-1.8 0.4-2.6-0.7-0.5 1.2-2.2-2.7-2.6-1.1 0-0.7-1-1.3 0.2-0.7-0.8-0.9-0.1-0.3 0.9-2.7 1.3-2.3 4.1-1.4 0.6-1 0.9-2.7 0.6-0.7 0.7 0.2 1.2-1 0.9-2.5 1.4-1.4 0.2-1 0.6-1 3.8-0.4 3.7-0.8-0.9-1.2-1.1-3.3 1.3-1.5 6.2-7.9 3.3-1-3 1.9-3.2-1.4-1.3-0.6-1.5-1.6-0.1-6.1 1.7-2.4 0.3-2-0.6 0.3-2.1-0.9-0.4-1.7-2.4-1.6 0.4-0.4-1.3-1.8 0.4-1.2 2.5-1.4 2.3-1.3 0-0.7-0.6 0.4 2.2-1.4 1-2.8 0.8-8.2-2.7 1.4-3.6 1.2-4.5 0.4-3.3 0.6-1.7 0.5-2.4 0.2-4.2-5.3-2.1-2.7 0.9-2.5 1.4-4.5 3-8.6 0.1 1 1.5 0.6 2.7-1.8 1.4-4 1.2-6.4 0.9-3.3 1.3-3.1 1.8-2.5-2-1.1-1.8 1.1-2 0-3-2.6-2.2-7.1 1.2-8.8-2.9-2.3-1.8-1.7-2.5-2.4 0.1-0.9-0.8-0.5-2.4-2.4-1.7-2.9 0.5-2.2 3.3-3.1 0.2-0.4-6.9-4.2-7.5 0.2-2.4-1-1.4 1.2-1.7 1.1-1 0.6-0.8-0.7-0.8 0.3-1.5 0.5-1.5 0.5-4.2-1.3-2.7-4-2.7 0.4-2.4 1.7 0.2 4.7-1.6 3.8-4.1 1-5.3 1.3-3.7 2-2.3 0.4-9 4.4 1.4 1.9 2.9 2.4-0.8 5.6-0.2 1.3 2.8 2.9 0.1 8.2 1.6 2.5-0.6 5-0.5 0.3-5.9 2.6-0.5 3.1 0.9 5.7 3.6 2.3 0.8 0.5-1.2 0-1.1 1.8-0.5 2.5-2.9-1.8-2.4-0.1-1.9 1.6-0.1 4.9-2.6 1.2 0.1 0.7-1.4 0.9-0.7 0.2-1.1 1-1.6 2.1-1.1 1.7-1.6 3-1.4-0.2-0.4-0.7-6.2 1.8-1.7 0.8-4.4-1.4-2.4-1.1-3-2.5-1.1-1.7-2-1.6-3.3 1.7-1.1 0.6-0.8 0.8-1 1.6-0.6-0.6-0.5-0.4-1.5 2.6-0.5 1.4-0.4-0.4-0.7-0.5-2.1 1.1 0 0.7-0.5 1.6-0.4 0.8 0.3 0.2 0.7 2 0.2 1.4-1 1.2-1.1 2.4 2 0.9 1.3 0.9 0.5 0.8 3.1 1.5-0.1 0.2-1.4-1.2-1.2 0.3-1.1 0.8-1.3 4-2.5 4.2-0.1 1.5 1 0.8 1.2 0.9 3.3 0.1 2.8 0.8-0.7 1.3 0.3 2.5 0 1 0.7-0.3 1.5-1 1.5-0.8 1.7 1.5 0.1 3.2-0.2 2.4 1.7 1.5 2.6 1.7 0.9 0.5-2.8 1.9-0.7 0.9-2.9 3.3-0.6 6.2-0.1 0.7 0.7 0.2 0.5 0 2.3 3.1 0.7 0.8 0.6 1.4 0.2 1.3-0.4 5.9-3.3 1.7 3 2.5 2.7 1.1 1.8-0.8 1.4-0.2 4.1-1.3 2.1 1.4 2.8 3.3 1.2 7.7 0.2 5-3.5 2-0.5 2.6 0.5 3-0.9 1.3-0.1 0.8-1 1.4 1.9 0 2.1 1.1 2.5 0.5 4.9 2.4 0.2 10.2 3.6 4.4 3.4 7.6 0.6z m-59.7 62.2l1.2-1.7 0.6-2.3 0.2-2.5-0.7-1.8-0.8-0.4-1.1-1.7-1.9-1.5-1.1-0.3-1.6 0.7-0.8 0.8-0.7 1.3-0.2 1.5 0.2 1.6 0.9 1.5 1.2 1.5 1.1 1.5 1 1.8 1.1 0.7 1.4-0.7z",
    KAZ3203: "M666.5 172.3l7.8 3 0.7 1.7 0.5 2 0.3 1.8-0.7 2.1-0.8 7.1-1.2 0.6 0.3 2.8-0.6 2.6 2.7 2.6 5.8-0.8 1.9-0.6 1.2-1 0.8 0.9 1.6 0.4 2.8 1.2-0.3 3.1 1.6 1.9 2.3-2.1 2.5 2.5 0.9 0.1 1.3 1.6 0.8 1.7-0.6 0.6 1.7 2.3 2 1.6-2.6 2-2.5 1.2-0.5 1-1 1.1 1.5 2.5 2.1 0.8 5.8-3.6 2.5-2 6.8 0 1.4-1.8 2 0.6 9.3-1.7 3 1.3 1.7-2.9 1.1-0.1 1.5-0.8 2.7-0.2 2.9 1.1 0.6-4.5 0.9-4 5.3-5.6 3.4 0.2 2.1 2.2 4.8 1.6 5.4 2.7 1.1 2.8-0.3 3.7 0.4 2.3 1.5 1.1-1.6 2.9-5.4 5.3-2.9 1.4-2.1 0.6-1.2 1.3-0.2 0.9 0.2 1.3 0.1 1.6-0.1 1.9-0.6 3.9-4.8 3.8-5 2-3.4 5.3 1.5 2.5 2.8 1.6 2.4 2.6 0.1 1.5 3.4 3-1.7 1.6-2.3 1 0.1 4.4 2.1 2.2 5.4-0.7 4.1 0.3 0.3 3.8-0.5 1.9-0.2 1.8-2.2 2.5-3.2 2-2.5 0.2-0.8 1.3-0.4 1.6-1.1 0.9 0.6 1.2 1.9 1.1 0.6 1.4 2.5 0.2 2.2 1-2.4 1 0.5 2.7 2.9 1.7 2.9 1.1-3.3 1.9-1 3.2-1.6 2.6-2.3 8.6 0.6 0.8 0.6 1.8 1 4.5-4.4 1.2 0.1 1.4-0.5 2-1 1.4-1.2 0.6 1.4 0.8 3.1 3.4 5 1.4 1.3-2.7 6.4-0.4 0.8 12.9-0.3 5-2.1 1-0.5 0.3-0.7-0.1-1.8 0.7-2.1 0.2-0.5 0.2-0.5 0.4-0.3 0.5-0.3 0.5-0.3 0.5-1.7 0.4-0.2 0-0.4-0.3 0-0.2 0.1-0.2-0.1-0.3-0.8-1.3-0.9-0.9-1.1-0.4-1.2 0.2-0.6 0.2-0.3 0-0.3 0-0.2-0.1-1.2-1.5-0.6-0.3-0.3 0.2 0.2 1.1 0.8 1.3 1 1.1 0.7 0.9-0.7-0.3-1.4-0.8-2.3-0.9-0.6-0.5-0.4-0.7 0.3-0.2 0.1-0.3-0.1-0.4-0.1-0.3-0.5 0.5-0.5 0.2-0.5-0.1-1.3-0.3-0.3 0-1.2 0.5-0.2 0.2-0.3 0.4-0.1 0.7-0.1 0.4-0.2 0-0.5-0.5-2-1.2-0.2 0.6-0.3 0-0.2-0.4-1.3 0.2-4.1-1.1-6.4-0.3-6.4 0.7-5.7-0.3-15.6 9.9-14.2 15.7-122.5 0.7-46.3-0.8-0.6-3.5-2.5-0.2-4.1 0.3-34.8-8.3-5.2-2.5-5.9-5.3-2.1-0.3-29.2-11.8-0.8-0.7-1.3 0.4-3.3-0.3-6.1-2.9-3.4-0.5-6.3-4.3-7.3 1.2 0.4-1.6 4.2-3.3 30.9-22.5 2.4-2.6-1-2.2-4.7-7.8 5.8-5.4 0.2-3.8 5.2-2.8 2.9-0.9-1.1-2.6 0.2-2-0.1-1 2.9-0.1 2.8-6.8 8.1-1.4 11 1.7 5.2-1.1 2.3-2.8 3.2-1.8 4.9-0.7 8.4-7.7 2.8-0.9 1.2-3.7 2.8-1.1 2.2-3 15.9-14.8 5.8-7.4 0.2-2-0.5-1.1 0.2-0.4 0.3-0.5-0.3-0.8 0-1.3 1.4-0.7 7.2-2.2 2.5-2.6 1.5-3-2.5-1.8-1-1.3-0.8-0.5-0.6-2.7-1-1.5 8.6-0.1 4.5-3 2.5-1.4 2.7-0.9 5.3 2.1-0.2 4.2-0.5 2.4-0.6 1.7-0.4 3.3-1.2 4.5-1.4 3.6 8.2 2.7 2.8-0.8 1.4-1-0.4-2.2 0.7 0.6 1.3 0 1.4-2.3 1.2-2.5 1.8-0.4 0.4 1.3 1.6-0.4 1.7 2.4 0.9 0.4-0.3 2.1 2 0.6 2.4-0.3 6.1-1.7 1.6 0.1 0.6 1.5 1.4 1.3-1.9 3.2 1 3 7.9-3.3 1.5-6.2 3.3-1.3 1.2 1.1 0.8 0.9 0.4-3.7 1-3.8 1-0.6 1.4-0.2 2.5-1.4 1-0.9-0.2-1.2 0.7-0.7 2.7-0.6 1-0.9 1.4-0.6 2.3-4.1 2.7-1.3 0.3-0.9 0.9 0.1 0.7 0.8 1.3-0.2 0.7 1 1.1 0 2.7 2.6-1.2 2.2 0.7 0.5-0.4 2.6-0.4 1.8-0.8 1.1 0.1 1.5 1.1-0.7 2.6-0.6 6.5 0.6 0.4-4 1.2-0.4 0.9-1.2 3 1.2 1.3-1.3-0.9-0.6-0.1-1.5 0.7-0.8-0.2-0.5 0.2-0.9 1.7-0.9 1-0.7 1 0 2-0.6 1.1-1.7 0.1-1.2 1.9-2.4 4.1 1 0.7-1 1.3-0.5 2.2-1.3-0.4-1.3 1.4-0.8 4.3-1.4 2.4-1.9 1.3-1.6-1.1-0.9-2.3-0.8-1.3 1.5-1.4 0.6 0.1-0.8-0.6-1.5-1.3-2.1-0.4-1 0.5-1.3 1.3 0.2 4.5-0.8 4.6 0.7z",
    KAZ3201: "M198.1 200.4l-1.8 2.9-0.4 1.9-1.6 1.8 1.1 3.9-0.3 4.1 1.3 2.3 1.1 0.8 0.7 1.4-0.6 2.2-0.3 1.6-1.1 2.3-0.4 2.1-1.3 1.9-1.5 1.4-1 1.4-0.1 1.5-2.8 1.5-2.9-0.3-1.3 0.1-0.7 1.3-3 0.9-1.2 1.1-1 2.2-1.2 0.3 0.1 2.3-1.2 4.3 0.1 3.8-4.7 4.6-0.7 0.9 0.3 1.1-0.8 0.5-2.6 0.8-8.7 0-1.4-0.4-3.7 4.6 0.2 4.4-3.6 5.7-0.8 4.4 0 1.6-1.6 0.7-8.5-2.8-0.9 1.2-2.2-0.1-2-0.4-3.8-1.5 0.7-2.9-0.3-1.2-1.8-0.6-9.4-1.2-2.7 1-2.5 0.3-2.7 2.2-2.4 3.2-1.2 0.1-1.2 0.6-1.2 1.1-1.8 0.5-20 13.9-11 1-0.7 2.8-12.8-4.5-43.8-6.6 0-0.4-0.4-0.5-0.5-0.4-2.4-0.6-12.2-4.3 0-0.1 3.4-9.5 3.3-9.1 0.6-0.8 0.9-0.6 1.8-1 0.5-0.5 0.5-0.4 1.2-1.6 1-1.9 0.5-2-0.3-1.9-1.2-1.6-3.6-2.8-1.4-0.5 1.6-9.2 1.5-9.2 0.5-1.5 0.9-0.9 5.4-2.4 1.6-1.7 1.6-2 0.5-1.1-0.1-1.4-1.3-1.8-0.2-1.1 0.3-0.5 0.9-0.9 0.2-0.6-0.1-0.6-0.4-0.4-0.2-0.4 0.3-0.8 0.4-0.3 1.7-0.7 0.3-0.5 0.8-1.8 0.8-1.1 1-1 1.1-0.6 1.1 0 0.6 0.4 1.6 2.1 1.1 0.7 0.3 0.3 0.3 0.5 0.2 0.4 0.2 0.3 0.5 0.2 0.3 0.2 1.1 1.3 0.5 0.7 0.9 0.9 1 1.5 1 0.9 0.2 0.4 0.1 0.5 0.2 0.6 0.3 0.4 1.2 1 0.5 1 0.2 1.1 0.2 1.1 0.3 1.1 0.2 0.3 0.2 0.8 0.2 0.3 0.3 0.3 1 0.8 0.4 0.9 0.2 0.9 0.3 0.5 0.9 0.1 0.7-0.1 0.2 0.1 1.1 1.1 1.5 0.8 0.3 0.1 0.3-0.2 0.5-0.8 0.4-0.3 5-1.9 2.4-1.5 1.9-2.2 0.4-0.7 0.2-0.4 0-0.4-0.3-1.5-0.2-0.6-0.3-0.4-1.7-0.6-0.1-0.9 0.2-1.4-0.4-1.5-1-2-0.7-2.7-0.7-5.5 0.1-2.7-0.1-1.2-0.6-0.8-1.2-0.3-0.6-0.3-0.2-0.8 0.4-0.5 0.6 0.1 3.2 1.9 1.2 0.3 1-0.2 4.5-3.4 2.2-2.9 1-0.9 3.4-0.8 2.1-0.9 1-0.9 0.3-1.3-0.4-1.2-0.8-1-0.6-1 0.3-1.1 0.6-1 0.2-0.6 0.2-1.3 0.2-0.7 0.3-0.6 0.4-0.4 0.5-0.2 2 0.6 5.8 0 0.4-0.3 1.2-1.6 3.5-3.1 1-0.5 4.4-0.9 2.9-1.5 0.4-0.3 0.2-0.4-0.1-0.5-0.2-0.7 0-0.8 0.2-0.3 0.4-0.3 0.3-0.3-0.3-0.4-0.1-0.3 0.2-0.2 0.3-0.1 1.5-0.1 0.4-0.1 0.3-0.4 0.2-0.7 0.6-0.1 0.4-0.1 0.2-0.4 0.1-0.9-0.2-2.3 0.1-0.6 0.4-0.4 0.5-0.1 0.3-0.3 0-1 0.1-0.9 0.5 0 0.9 0.8 1.2 0 0.2 0.2-0.5 0.6-0.4 0.7 0.3 0.4 0.6 0 0.6 0 1.1-0.5 0.6-0.7 0.1-1.1-1.1-3.3-0.1-1.3 0.6-0.7 1.3 0.3 0.4 0.3 0.5 0.4 0.4 0.5 0.2 0.6 0.4 0.8 0.6 0.3 6 0.4 0.5-0.2 0.6-0.3 0.5-0.2 0.4 0.3 0.6 1.1 0.3 0.3 0.5 0.2 1.1 0.1 0.4 0.2 0.3 0.8 0 0.9-0.2 0.6-0.4 0.4-0.5 0.3-2.3 0.4-0.2 0.3 0.3 0.6 0.4 0.6 0.2 0.5 0.8 0.8 1.8 0.2 2-0.3 1.3-0.7 0.5-0.6 0.3-0.6 0.3-0.4 0.6 0 0.7 0.2 0.2 0.4 0.1 0.6 0 0.9 0.6 1.1 1.1-0.1 1.2-0.7 1-0.8 0-1.4-0.4-1.3-0.1-1.2 1.4-1.6 0.7-1.1 0.5-0.5 0.7-0.2 0.7 0 0.6 0.3 0.7 0.4 0.6 0.2 0.8 0.1 0.7 0 0.6-0.2 0.5-0.5 0.6-1.2 0.3-0.6 0.6-0.3 1.4 0.1 0.7-0.1 0.5-0.4 0.3-1.2 0.4-0.4 0.7 0.4 0.4 1.4 0.3 1.5 0.3 1.2 1.7 3.2 0.6 2.5 0.2 0.4 0.3 0.5 0.4 0.6 0.4 0.4 0.5 0.3 0.2 0.1 0.5-0.4 2.2 0.2 0.1 0.3 0.3-0.2 0.3-0.3 0.2-0.4 0.2-0.2 0.3-0.1 0.6-0.1 0.2-0.1 0.1-0.3 0.2-0.3 0.3-0.1 0.3 0.1 0.5 0.6 0.2 0.1 1.4-0.1 0.5 0.2-0.3 0.7 0.5 0.1 0.6 0.4 0.4 0.1 0.3-0.1 0.4-0.3 0.3-0.4 0.3-0.3 0.9-0.4 3 0.1-0.2-0.7 0.1-0.3 0.3 0 0.4 0 0.2 0 0.2 0.4 0.1 0.2 0.3 0.1 0.4-0.2 1.8 0.3 0.4-0.1 0.6-0.3 0.5 0.4 0.1 0 0.9 0.3 0.3 0.4 0.5 0.9 0.3 0.4 2.5 1 1 0.8 0.3 0.3 0.2 0.6-0.2 0.4-0.8 0.4 0.4 0.5 0 0.7-0.1 0.7 0.1 0.6 0.9 0.8 0.3 0.5 0.2 0.7 0.6 0.7 4.5 1.4 1.2-0.3 0.7 0.2 0.9 0.8 0.5 0.5 0.6 1.3 0.5 0.4 1.3 0.2 0.5 0.3 0.2 0.6 0.2 1.5 0.2 0.5 0.2 0.4 0.1 0.4-0.1 0.6 0.8 1 0.4 0.4 1 0.6 0.3 0.4 0.3 0.6 0.4 0.5 0.6 0.3 2 0.4 0.2 0.2 0.5 0.4 0.2 0.2 1.1 0.3 0.4 0.1 0.1 0.4 0.1 0.3-0.1 0.4-0.1 0.4-1.4 1.8-0.2 0.8-0.1 1.7-0.1 0.8-0.5 1.7-0.2 0.8 0.1 0.7 0.4 0.8 0.9 1.2 0.5 0.5 0.6 0.4 0.5 0.1 1.1-0.3z",
    KAZ3206: "M876.2 382.5l-0.6 0.3-3.6-1.4 0.3-3.6-1.3-4-2.5-3.5-1.7-5.6-6.5-8.7-1-2.9-4.7-2-0.1-1-0.9-0.6-2.7 0.1-0.7-0.9-1.6-0.9-2.8-0.1-6.7-1.7-10.5-5.2-1.9-1.2-0.1-1.6-6.1-2.3-12.9 0.7-4.4-6.1-6.5-1.2-2.3 0.3-3.6-0.5-1.6-2.3-2.4-0.4-1.9-1.8-7-3.9-4 3.2-1.6-1.6-2.4 1-2.4 1.9-4.7-0.8-1.9 0.9-6.4 0.4-1.3 2.7-5-1.4-3.1-3.4-1.4-0.8 1.2-0.6 1-1.4 0.5-2-0.1-1.4 4.4-1.2-1-4.5-0.6-1.8-0.6-0.8 2.3-8.6 1.6-2.6 1-3.2 3.3-1.9-2.9-1.1-2.9-1.7-0.5-2.7 2.4-1-2.2-1-2.5-0.2-0.6-1.4-1.9-1.1-0.6-1.2 1.1-0.9 0.4-1.6 0.8-1.3 2.5-0.2 3.2-2 2.2-2.5 0.2-1.8 0.5-1.9-0.3-3.8-4.1-0.3-5.4 0.7-2.1-2.2-0.1-4.4 2.3-1 1.7-1.6-3.4-3-0.1-1.5-2.4-2.6-2.8-1.6-1.5-2.5 3.4-5.3 5-2 4.8-3.8 0.6-3.9 0.1-1.9-0.1-1.6-0.2-1.3 0.2-0.9 1.2-1.3 2.1-0.6 2.9-1.4 5.4-5.3 1.6-2.9 2-0.3 3.2 2.7 5.2 2.3 7.6 2 4.9-4.9 2.2-4 1.4-3.5-1.3-3.6-1.2-0.7 0.2-4-0.9-5.2-6.4-4.1-1.6-2-0.2-0.2-0.2-0.3-0.3-0.3-0.9 0-0.3-0.3-0.3-0.5-0.6-1.5-0.1-0.5-0.4-0.1-0.3-0.4-0.1-0.2-0.2-1.2-0.4-0.5-0.6-0.1-0.6-0.4 0.2-0.8-0.4-0.5-0.4-0.5-0.1-0.6 0.2-0.1 0.3-0.4 0.2-0.4 1.3-1.2 0.9 0.1 2.7-1.7 0.6-1.2 1.1-0.4 0.6 1 1.6-0.3 6.4-3.5 1.2-1.2 0.2-1.6-1-0.4 1-2.3 3.4-0.5 0.8 0.5 1.3-0.9 1.2-1.2 0.5-1.8 0-1 3.4-1.6 0.9-3.2 0.5-0.1 1.4 2.7 2.6 4.9 4.4 9.4 4.3 9.4 2.5 5.2 0.7 2 0.2 0.6 0.6 0.9 0.8 0.8 0.8 0.5 0.8 0.1-1.4-2.2-0.4-1.2 0.9-0.5 0.4 0.2 0.8 1 0.5 0.2 0.5-0.1 0.3-0.3 0.4-1.3 0.3-0.6 0.3-0.3 1-0.4 0.2-0.2 0.4-0.5 0.2-0.2 0.3 0 1 0.1 1-0.2 1-0.6 0.9-0.9 0.5-1.4 0.1-0.5 0-0.7 0-0.7-0.1-0.6-0.2-0.6-0.7-0.7-0.3-0.5-0.1-0.5 0-0.7 0.2-1.3 0.2-0.7 0.3-0.6 0.4-0.2 0.6-0.1 2.1 0.3 0.9-0.3 0.3-1 0.1-0.6 0.1-0.6 0.3-0.5 0.3-0.4 0.5-0.4 0.4-0.1 3.3 1.6 0.6 0.1 0.3-0.3 0.3-0.4 0.4-0.3 0.6 0 0.2 0.5 0 1.4 0.2 0.7 0.3 0.4 1 0.6 1.1 0.6 0.6 0.1 0.6 0.1 0.7-0.8 0.3-0.2 0.5 0.1 0.5 0.3 0.2 0.5-0.1 0.6-0.2 0.9-1.6 4.1-0.7 2.3-0.1 0.6 0.2 0.3 0.5 0.3 0.6 0.1 1.4 0 2.9-1 3 0.1 0 1.4 0.4 1.3 0.5 1.2 0.7 1-0.6 0.8-0.4 1-0.2 1 0.3 0.8 0.7 0.4 5.3-0.2 0.5-0.4 0.7-1.1 0.5-0.4 0.4 0 1 0.7 0.5 0 1.8-0.3 1.2 0.2 1.1 0.4 0.5 0.3 1.5 1.4 0.6 0.3 1.2 0.2 0.7 0 0.6-0.3 0.4-0.4 0.5-0.6 0.5-0.4 0.4 0.1 0.4 0.2 0.3-0.1 0.3-0.3 0.3-0.1 0.4 0 0.5 0.3 1.1-0.2 0.5 0.3 1.1 1 3.2-2.1 1.7-0.8 0.6-0.8 0-2.7 0.7-0.8 0.9-0.2 1.9 0.6 3.3 0.3 1.1-0.3 0.9-1 0.7-1.4 0.5-1.6 0.2 0 0.5 0 0.6 0.2 0.4 0 0.4 0 0.4-0.2 2.8-0.3 2.7 0.3 0.6 1.1 0.4 0.5 0.5 0.3 2.8 0.5 0.5 0.3 1.4 1.3 0.6 0.3 0.5 0.1 1.1 0 0.5 0.2 0.5 0.3 0.4 0.4 0.8 1.4 0.3 0.3 1 0.5 0.7 0.8 0 2.4 0.6 1.2 3.1 2.2 0.7 1.1 0.3 0.7 0 0.6 0.2 0.6 0.8 0.9 0.3 0.6 0 0.7-0.5 2.1 0.4 1.2 0.6 1.2 0.5 1.2 0.1 0.9-0.1 0.9 0 0.8 0.3 0.7 0.3 0.2 0.7 0.2 0.3 0.2 0.2 0.3 0.3 0.6 0.2 0.3 0.9 0 0.9-0.7 0.9-0.5 0.8 0.6 0.7 0.9 0.8 0.5 0.8 0.2 0.9-0.1 0.9 0.1 0.6 0.6 0.5 0.7 0.8 0.6 0.4 0 0.9-0.1 0.4 0.2 0.3 0.4 0.6 1.4 0.3 0.2 0.4-0.1 0.4-0.3 0.4-0.2 0.5 0.1 1.6 0.7 0.3 0.3 0.8 1.3 0.2 0.5-0.1 0.6-0.4 0.3-1 0.2-0.3 0.1-0.1 2.1 0.1 1 0.5 0.5 1.4 0.4 0.6 0.4 0.3 0.9 0.2 1.4 0.1 0.5 0.4 0.7 1.6 1.8 0.5 1.1 0.3 3 0.7 1.1 0.9 0.5 1-0.3 0.4-0.3 0.6-0.4 0.5-0.2 0.5-0.1 0.6 0.3 1 0.8 0.6 0.2 1-0.1 0.4-0.1 0.4-0.3 0.8-0.7 0.4-0.2 0.4 0.1 0.4 0.4 0.3 1.1 0.4 0.5 0.4 0.1 0.4-0.1 0.9-0.3 0.5 0 0.4 0.2 0.4 0.4 0.3 0.5 0.4 0.2 0.4-0.1 0.9-0.7 0.4-0.2 0.5 0 1 0.2 0 0.3-0.2 1.5 0 0.7 0.3 0.4 0.5 0.1 0.4-0.1 0.4-0.4 0.7-0.9 1.1-0.4 1 0.2 0.6 1.1 1 1.1 0.9-1.2 0.8-2.1 0.6-1.2 1.4-1 1.5-0.6 1.7-1.5 0.4-0.2 0.1-0.3 0.1-0.3-0.1-0.4-0.2-0.5 1.2-0.7 0.4-0.4 0.2-0.1 0.3 0 0.2-0.1 0.4-0.6 0.3-0.8 0.4-0.5 0.8 0 1 0.6 0.4 0 0.7-0.2 0.4 0 0.3 0.2 0 0.4-0.2 0.4-0.2 0.5-0.1 0.3 0 0.5 0.3 0.4 0.6 0.7-0.5 0.5-0.5 0.3-0.4 0.2-1.2 0.1-0.5 0.4-0.9 1.3-0.4 0.7-0.1 0.5 0.2 0.5 0.5 0.6 0.6 0.4 0.5 0 1.1-0.2 0.6 0.1 1.7 1.1 0.5 0.5 0 0.6-0.2 0.7-0.1 0.9 0.4 0.6 0.6 0.5 1.3 0.6 0.7 0.5-0.1 0.4-0.4 0.4-0.2 0.7 0.2 0.5 0.5 0.5 1.6 1.3 0.3 0.3 0.1 0.3 0 0.6 0.2 0.3 0.3 0.3 0.3 0.1 0.6 0.1 2.1-0.2 0.7 0.2 0.7 0.3 1.3 0.9 0.4 0.8 0.5 2.6 0.4 1.1-3-0.9-1-0.5-1.1-0.3-3.4 1.4-0.7-0.1-1.5-0.5-0.6 0.1-0.6 0.4-0.5 1.4-0.4 0.6-1 0.8-0.4 0.4-0.4 0.7-0.3 0.8-0.1 0.5 0 0.7 0.2 0.9 0.6 1.2 0.8 0.8 0.3 0.7-0.6 1.2-0.4 0.5-0.2 0.4 0 0.4 0.3 1.6 0 0.5-0.2 0.4-1.8 2.6-1.2 1.2-0.3 0.4-0.3 0.6-0.5 1.3-0.2 0.5-0.5 0.3-2.8 1.2-2.4-0.2-0.6 0.3-0.6 0.5-0.9 1.2-1.4 0.7-4.5-0.4-5.4 0.8-0.9 0.6-0.7 1.2-3.3 8-0.6 4-0.3 1.4-0.5 0.9-0.2 0.8-0.1 0.9 2 10.8 0 1.2-0.4 2.4 0.1 1.2 0.4 1 1.7 2.9 0.2 0.7-0.1 0.7-0.2 0.7 0.1 0.6 0.2 0.7 0 0.5-0.2 1.2-0.1 0.8-0.2 0.5-0.3 0.3-1.1 0.1-0.6 0.2-0.5 0.4-0.2 0.6-0.1 0.7-0.1 0.5-0.2 0.5-0.4 0.5-0.2 0.7 0 0.6-0.2 0.5-1.6-0.1-3.2 0.5-1.4-0.2-0.5 0.1-0.5 0.3-0.3 0.4-0.6 1.1-0.5 0.5-1.7 1.2-0.7 0.9-0.3 0.2-1.1 0.2-0.4 0.2-0.4 0.4-0.5 1.1-0.5 0.4-1.9 0.4-1.8 0.7-0.6 0.1-0.5-0.2-0.1-0.5 0.2-0.5 0.3-0.6 0.3-0.6 0.1-1.2-0.5-1.1-0.8-0.8-0.9-0.5-1.2-0.1-2.3 0.6-1.1-0.2-1.3-0.6-0.4-0.1-0.4 0.1-3-0.3-2.6 0.3-1.6 0.9-0.6 0.1-1.7-0.4-2.3 0-9.3-3.3-4.2-2.8-1-0.5-2.2-0.5-0.5-0.3-1.3-1.1-0.6-0.2-0.6-0.1-0.7 0.1-0.6 0.1-0.6 0.3-0.2 0.4 0 1.5-0.1 0.4-0.2 0.8-0.1 0.4 0 0.3 0.2 0.7 0 0.4-0.4 1.4-0.1 0.7 0.1 0.7 0.1 0.6 0 0.6-0.2 0.6-3.6 7.5-2.2 8.5-1 2.1-0.1 0.6-0.3 1.9-0.4 1.1-1.6 2.7-1 2.1-0.6 2.3-0.7 4.9-0.6 3.2-0.4 1-0.6 1.7-1.9 3.2-0.9 3.4-0.7 1.9-0.2 0.9z",
    KAZ3207: "M876.2 382.5l-0.2 0.8 0.9 1.4 0.9 0.4 3.7 0.5 2.5 1.1 0.4 0.7 0.1 1.3-0.1 1.3-0.5 3.2-0.7 2.5-1.1 1.8-1.6 0.3-1.1-0.7-2.1-2.4-1.2-0.8-1.2-0.1-1.3 0.3-4.7 2.4-1.2 0.3-1-0.5-0.4-0.5-0.4-0.3-1.7-0.2-0.3-0.3-0.2-0.4 0-0.8-0.1-1.2-0.4-1.5-0.7-1-0.8-0.1-1-0.3-1 0.5-2 1.4-9.7 3.1-2 1.1-1 0.4-1.1 0-3 1.2-0.6 0.1-2-0.7-1.2 0-3.9 1.5-1.7-0.2-0.6 0.1-0.5 0.4-0.4 1.3-0.4 0.3-0.6 0.1-1.9-0.1-1.7 0.7-1.7-0.3-1.1 0.1-1 0.5-1 0.8-2.4 2.2-1.1 0.6-0.5 0.3-0.4 1.1 0.6 0.8 3 1.9 1.2 0.1 4.4-0.9 0.6 0.1 0.5 0.2 1.2 1 4 1.5 0.4 0.3 0.1 0.4-0.1 0.5-0.4 0.3-1.6 0.3-0.9 0.4-0.5 0.7 0 0.4 0.1 0.3 0.5 0.6 0.1 0.4-0.1 0.4-0.3 0.7-0.7 2.6-0.2 1.4 0.1 1.3 1 5 0 1.3-0.7 2.5-0.2 1.3 0.2 0.6 0.3 0.4 0.8 0.9 0.3 0.5 0.2 0.6 0 0.6 0 0.7 0.3 0.7 0.4 0.8 0.9 1.3 2.9 7.7 2.9 7.6-0.1 1.1-0.4 1.1-1.2 2.3 0.3 0.5 1 0.2 0.9 0.5 0.3 0.5 0.2 0.6 0.1 0.6 0 0.7 0.1 0.6 0.6 1.1 0.1 0.6-0.1 0.7-0.4 0.5-0.4 0.3-0.6 0.1-0.5-0.1-1.2-1-1 0-1.1 0.5-1.9 1.4-2.1 1-1.1 0.6-0.3 1 0.5 0.8 0.8 0.7 1.7 0.8 1.1 0.2 0.5 0.3 0.3 0.6-0.1 0.6-0.6 0.4-1.2 0.1-1.9 0.7-0.4 0.4-1.2 0.5-0.4 0-0.9-0.2-0.4 0-0.4 0.2-0.5 0.9-0.5 2-0.4 0.9-0.9 1.4-0.2 0.5-0.1 0.8 0.1 0.5 0.2 0.5 0.2 1.3 0.3 0.5 0.6 1 0.3 0.5 0 0.4-0.1 0.5-0.3 0.6 0 0.4 0.6 2.6 0.2 0.6 0.2 0.4 0.3 0.5 0.5 2.4-0.2 0.7-0.3 0.4-0.9 0.5-1.1-0.6-0.7-1-0.7-1.2-0.8-1-1.6-1.5-0.9-1.4-0.3-0.4-1.1-0.7-5.4-1.1-1-0.1-2 0.4-2.3-0.1-1.3-0.6-0.7-0.9-1.1-2.6-0.7-0.9-2-0.9-0.8-0.9-0.4-0.6-0.2-0.6-0.2-0.7 0-0.7 0.1-1.3-0.1-0.4-0.7-0.1-0.9 0.2-1.9 0.9-1 0-0.9-0.4-1.6-0.1-2-0.8-2.9-0.3-0.4-0.2-0.9-0.7-1-0.6-2.4-0.9-1.6-0.1-1.1 0.1-0.5 0.1-0.9 0.6-0.4 0.2-0.5-0.1-1-0.3-0.5-0.1-1.1 0.2-1.1-0.1-2.7 0.2-1.1-0.1-1.3-0.9-0.6-0.2-0.6 0-0.5 0-0.2 0.1-0.4 0.3-0.7 0.3-0.3-0.2-0.1-0.2-0.3-0.4-1.6-0.4-1.6-0.1-0.5 0.1-1.3 0.6-0.4 0-0.5-0.2-0.4-0.4-0.5-0.3-1 0-0.6-0.2-0.4 0-0.4 0.1-1 0.5-0.8 0.2-2.4-0.4-0.9 0-0.4-0.1-0.4-0.3-0.7-1-0.2-0.2-0.9-0.3-2.7-0.5-0.5-0.2-0.5 0.1-1 0.3-1 0.1-0.5 0-0.5 0.4-0.5 0.6-0.3 0.1-0.5-0.1-0.2 0.1-0.2 0.4-0.9 0.5-0.5 0.1-1 0-1 0.4-0.5 0.1-0.6-0.1-1.3-0.6-0.6 0-0.5 0.2-0.7 0.7-0.5 0.1-0.2-0.1-0.3-0.5-0.2-0.2-0.3 0-0.5 0.2-0.3 0.1-2.1-0.7-2.2 0-1.8-0.4-0.6 0-1.6 0.5-0.6 0-1.9-0.6-0.9-0.1-1.3 0.1-0.8-0.8-4.7-0.7 1.7-1.2-2-1.8-2.9-1.2-2.1-1.7-0.7-1.2-0.3-0.9-0.2-1-0.5-1.6 1.6-3.3 1.6-2.4-2-1.2-2.2 0-0.8-1-0.9 0.4-2-0.5-2.1-0.9 6.2-10.6 1.2-3.1 0.2-3.8-2-2.5-2.4-2.3-4.1-2.4-5.4-0.8-3.6-3.4-1.3-3.6-0.5-4.7-9.4-8.7-1-2.3-0.4-3-2.4-0.5-2.4-0.1-2.1-3.3 0.8-2.9 6-0.4-0.5-1.7-0.5-1.9-0.5-5.7 0.6-8.4 3.1-6.3 14.2-15.7 15.6-9.9 5.7 0.3 6.4-0.7 6.4 0.3 4.1 1.1 1.3-0.2 0.2 0.4 0.3 0 0.2-0.6 2 1.2 0.5 0.5 0.2 0 0.1-0.4 0.1-0.7 0.3-0.4 0.2-0.2 1.2-0.5 0.3 0 1.3 0.3 0.5 0.1 0.5-0.2 0.5-0.5 0.1 0.3 0.1 0.4-0.1 0.3-0.3 0.2 0.4 0.7 0.6 0.5 2.3 0.9 1.4 0.8 0.7 0.3-0.7-0.9-1-1.1-0.8-1.3-0.2-1.1 0.3-0.2 0.6 0.3 1.2 1.5 0.2 0.1 0.3 0 0.3 0 0.6-0.2 1.2-0.2 1.1 0.4 0.9 0.9 0.8 1.3 0.1 0.3-0.1 0.2 0 0.2 0.4 0.3 0.2 0 1.7-0.4 0.3-0.5 0.3-0.5 0.3-0.5 0.5-0.4 0.5-0.2 2.1-0.2 1.8-0.7 0.7 0.1 0.5-0.3 2.1-1 0.3-5-0.8-12.9 1.9-0.9 4.7 0.8 2.4-1.9 2.4-1 1.6 1.6 4-3.2 7 3.9 1.9 1.8 2.4 0.4 1.6 2.3 3.6 0.5 2.3-0.3 6.5 1.2 4.4 6.1 12.9-0.7 6.1 2.3 0.1 1.6 1.9 1.2 10.5 5.2 6.7 1.7 2.8 0.1 1.6 0.9 0.7 0.9 2.7-0.1 0.9 0.6 0.1 1 4.7 2 1 2.9 6.5 8.7 1.7 5.6 2.5 3.5 1.3 4-0.3 3.6 3.6 1.4 0.6-0.3z m-129.4 79.8l0.7-2.2 0.1-3-0.9-0.8-1.5 0.5-0.8 0.8-0.9 1.1-0.7 1.4-0.7 0.6 0 1.6 0.8 0.7 1.6-0.1 1.3 0.4 1-1z",
    KAZ3204: "M660 84.5l0 0.1 3.1 6.3-1.6 0.3-1.4 1.1-0.2 3.1 1 2.2 1.6-1.4 1.4-1.7 1.1-0.7 0.6 0.6 0.8-0.2 1.4 0.8-1.6 2.5-1.1 2.7-2.1 3.8-0.8 1.2 2.3-1.1 1.2 0.6-0.5 1.6-0.4 3.5 3 0.7 1.8-3.4 2.7-0.7 1.9 2.3-0.7 3.1-0.8 1.2-0.5 1.9-1 2.5-0.8-1.5-2.6-1.6-0.8-0.1 0.6 3.3 3.3 1.1 1.8 1.7-7.6-0.6-4.4-3.4-10.2-3.6-2.4-0.2-0.5-4.9-1.1-2.5 0-2.1-1.4-1.9-0.8 1-1.3 0.1-3 0.9-2.6-0.5-2 0.5-5 3.5-7.7-0.2-3.3-1.2-1.4-2.8 1.3-2.1 0.2-4.1 0.8-1.4-1.1-1.8-2.5-2.7-1.7-3-5.9 3.3-1.3 0.4-1.4-0.2-0.8-0.6-3.1-0.7 0-2.3-0.2-0.5-0.7-0.7-6.2 0.1-3.3 0.6-0.9 2.9-1.9 0.7-0.5 2.8-1.7-0.9-1.5-2.6-2.4-1.7-3.2 0.2-1.5-0.1 0.8-1.7 1-1.5 0.3-1.5-1-0.7-2.5 0-1.3-0.3-0.8 0.7-0.1-2.8-0.9-3.3-0.8-1.2-1.5-1-4.2 0.1-4 2.5-0.8 1.3-0.3 1.1 1.2 1.2-0.2 1.4-1.5 0.1-0.8-3.1-0.9-0.5-0.9-1.3-2.4-2-1.2 1.1-1.4 1-2-0.2-0.2-0.7-0.8-0.3-1.6 0.4-0.7 0.5-1.1 0 0.5 2.1 0.4 0.7-1.4 0.4-2.6 0.5 0.4 1.5 0.6 0.5-1.6 0.6-0.8 1-0.6 0.8-1.7 1.1 1.6 3.3 1.7 2 2.5 1.1 1.1 3 1.4 2.4-0.8 4.4-1.8 1.7 0.7 6.2 0.2 0.4-3 1.4-1.7 1.6-2.1 1.1-1 1.6-0.2 1.1-0.9 0.7-0.7 1.4-1.2-0.1-4.9 2.6-1.6 0.1 0.1 1.9 1.8 2.4-2.5 2.9-1.8 0.5 0 1.1-0.5 1.2-2.3-0.8-5.7-3.6-3.1-0.9-2.6 0.5-0.3 5.9-5 0.5-2.5 0.6-8.2-1.6-2.9-0.1-1.3-2.8-5.6 0.2-2.4 0.8-1.9-2.9-4.4-1.4 0.1-6.7 0.8-6.3 1.9-2.1 0.3-3.1 0-1.9-0.7-1.9-0.8-1-0.3-1.5 1.4-2.8 5.3-0.8 2.3-1.6-2.3-2.2-1.4-2.6-0.1-4.4-0.8-4.8-3.1-0.6-1-3.5 2.2-2.5 0.5-4.3-1.7-2.4-1.6-0.3-1.3-4-0.5-3.6 2.6-1.8 6-2.7-2-3.5-2.9-1.3-1.5-2.1 2.9-1.3-3.2-14.2 0-0.1 8.5-3.1 1.5-0.2 3.2 0.1 1.5-0.3 0.5-0.3 1-1 0.5-0.3 4.8-0.2 0.7-0.2 1.4-0.8 0.8-0.2 1.9 0 0.9-0.3 0.7-0.6 0.5-1 0.5-0.7 0.7-0.3 1.6 0.4 1.6-0.3 4.4 0.1 1.9-0.6 1.4-1.2 1-1.7 0.5-0.6 0.7-0.5 0.9-0.3 0.7 0 2.4 1.2 3.8-0.6 0.4-0.5 0.2-1.1 0.5-0.2 0.3-0.4 0.1-0.6 0.2-0.7 0.4-0.3 0.7-0.1 0.6-0.2 0.1-0.9-2-1.2-0.9-1 0.4-1.3-0.5-0.5-0.2-0.2 1.5-0.8 0.6-0.2 0.3 0.2 0.3 0.2 0.4 0.2 0.5-0.1 1.2-0.5 0.6 0 1.9 0.5 1.6-0.6 0.6 0 0.6 0.2 0.3-0.1 0.3-0.5 0-0.4-0.1-0.8 0-0.4 0.4-0.5 1.2-0.8 0.1-0.6 0-0.9 0-0.8 0.2-0.7 0.3-0.7 0.5 0.3 2.4 0.4 0.6 0.3 0.5 0.5 0.9 1.5 0.5 0.6 0.5 0.3 0.7-0.4 0.2-0.5 0.1-0.8-0.2-1.6-0.6-0.4-0.5-0.2-0.3-0.5-0.3-1.4-0.1-0.3 0-0.4 0.2-0.3 0.4-0.3 0.4 0 4.4 1.6 0.6 0.8 0.4 1.4 0.5 0.9 1-0.4 2.1-2 0.5-0.2 3.5 1.6 4.8 0.6 3.2 1.8 0.7 0.7 1.9 2.8 0.7 0.5 2 0.2 0.9 0.3 3.4 2.3 0.4 0.1 0.5-0.4 0.8-1.4 0.5-0.4 2.1-0.7 0.3-0.3 0-0.3 0.1-0.7 0.3-0.7 1-1.6 0.4-0.2 2 0.8 0.5 0 0.6-0.2 0.5-0.3 0.5-0.5 1-0.7 1.3-0.2 1.4 0.3 1.1 0.5-0.4 0.6-0.4 0.7-0.2 0.7 0.3 0.7 1.7 2.2 2.4 3.2 0.4 1.2 0 0.5 0 0.9-0.1 1.1-0.1 0.6-0.2 0.6-0.1 0.8 0.4 1.1-0.2 0.5-0.3 0.5-0.1 0.7-0.1 0.8 0.2 0.6 0.9 1.1 0.2 0.5-0.1 1.2 0.3 1.4 1 1.1 1.2 0.7 1 0.2 1 0 0.5 0.1 0.5 0.5 0.8 1.3 0.3 0.9 0 0.9-0.3 0.6-0.5 0.3-1.3 0.4-0.8 0.7-0.1 1.2 0.2 3-0.1 0.5 0 0.2 0.5 0.6 0.1 0.5-0.1 1.4 0.1 0.2 0.1 0.2 0.5 0.1 0.1 0.2 0 0.4-0.1 0.7 0 0.6-0.1 1.7-1.4 0.3-0.4 0-0.5-0.2-0.9-0.5-0.5-0.1-0.9-0.1-0.5 0.4-0.3 0.8 0.2 1.5 1.5 0.1 0.3 0.1 0.1 0.7 0 1-0.1 1-0.2 0.6 1 0.5 0.3 0.3 0.4 0.7 0.8 1.5 0.4 0.5 0.5 0 0.9-0.6 0.6-0.8 1.3-2.3 0.6-0.5 0.7 0.2 0.8 0.4 0.7 0.1 0.8 0 0.2 0.2 0.2 0.4 0.2 0.5 0.2 2.1 0.5-0.1 1.3-0.7 0.6-0.1 0.2 0.2 0.5 0.5 0.3 0.2 0.4 0.1 0.4-0.1 0.8-0.4 0.7-0.6 0.2-0.6-0.2-1.8 0.1-0.9 0.3-0.7 0.4-0.6 0.5-0.2 4.8 0.5 0.6 0.2 0.4 0.2 0.4 0.5 1.5 2.7 0.5 0.7 0.7 0.3 0.7-0.1 0.5-0.3 0.3-0.7 0-1.2-0.5-0.7-0.6-0.3-0.8-0.1-0.6-0.3-0.5-0.4 0-0.2 0-0.5 0-0.5 0.3-0.9 0.5-1.1 0.1-0.5-0.2-0.4-0.6-0.5-0.8-0.5-0.3-0.4 0-0.7 0.4-0.3 2.7 0.4 0.8 0.5 0.9 1 0.8 1.3 0.3 1.2 0.3 0.5 1.1 0.3 0.3 0.8-0.1 0.5-0.8 1.9 4.5 1.8 0 0.1-0.4 0.9-0.3 0.2-0.4-0.1-0.4 0-0.1 0.1-0.2 0.6-0.1 0.1-0.2 0-0.7-0.3-0.3 0.2 0 0.4 0.1 0.6 0.1 0.3 0.4 0.7 0.6 0.5 0.3 0.7-0.3 0.9-0.6 1.5-0.2 0.9 0 0.6 0.5 0.5 0.7 0.2 0.8 0 0.5-0.4 1-1.2 0.7-1.2 0.3-0.2 0.4 0 2.4 1.1 0.7-0.3-0.1-1.5-0.5-1.2-0.5-0.8-0.6-0.5-0.8 0-2.2 0.4-0.1-0.3 0.1-0.7 0.3-0.9 0.2-0.4 1.1-1.2 1.2-0.3 6.4 0.7 1.4-0.1 0.4 0.1 0.2 0.6 0.2 0.7 0 1.6 1.2 0.6 0.3 0.6 0.2 1.1 0.4 0.6 7.3 1.7 0.3-0.1 0.2-0.3 0.1-0.9 0.2-0.2 0.8 0.1 0.2 0 0.3 0.3 0.6 0.7 0.3 0.1 0.6-0.2 0.3-0.2 0.1-0.6-0.1-1 0.2-0.7 1.4-0.4 0.5-0.6-0.1-0.5-0.2-0.5-0.1-0.3 0.3-0.2 1.3-0.3 0.4 0.1 0.1 1 0.2 0.1 1.4-0.4-1.1 6.5-0.3 1.1-0.5 0.6-0.7 0.1-3.6-0.7-0.7 0.1-0.6 0.3-0.5 0.6-0.5 1 1.1 0.8-2.2 0.8-0.4 0.5-0.6 3.7-1 0.2-0.5 0.3-0.4 0.5-0.2 0.8 0.1 0.5 0.8 1-0.7 1.4 4 2.2-1.1 2.5 1.6 1.2 0.3-0.1 0.6-0.7z",
    KAZ3205: "M803.1 155.4l-0.5 0.1-0.9 3.2-3.4 1.6 0 1-0.5 1.8-1.2 1.2-1.3 0.9-0.8-0.5-3.4 0.5-1 2.3 1 0.4-0.2 1.6-1.2 1.2-6.4 3.5-1.6 0.3-0.6-1-1.1 0.4-0.6 1.2-2.7 1.7-0.9-0.1-1.3 1.2-0.2 0.4-0.3 0.4-0.2 0.1 0.1 0.6 0.4 0.5 0.4 0.5-0.2 0.8 0.6 0.4 0.6 0.1 0.4 0.5 0.2 1.2 0.1 0.2 0.3 0.4 0.4 0.1 0.1 0.5 0.6 1.5 0.3 0.5 0.3 0.3 0.9 0 0.3 0.3 0.2 0.3 0.2 0.2 1.6 2 6.4 4.1 0.9 5.2-0.2 4 1.2 0.7 1.3 3.6-1.4 3.5-2.2 4-4.9 4.9-7.6-2-5.2-2.3-3.2-2.7-2 0.3-1.5-1.1-0.4-2.3 0.3-3.7-1.1-2.8-5.4-2.7-4.8-1.6-2.1-2.2-3.4-0.2-5.3 5.6-0.9 4-0.6 4.5-2.9-1.1-2.7 0.2-1.5 0.8-1.1 0.1-1.7 2.9-3-1.3-9.3 1.7-2-0.6-1.4 1.8-6.8 0-2.5 2-5.8 3.6-2.1-0.8-1.5-2.5 1-1.1 0.5-1 2.5-1.2 2.6-2-2-1.6-1.7-2.3 0.6-0.6-0.8-1.7-1.3-1.6-0.9-0.1-2.5-2.5-2.3 2.1-1.6-1.9 0.3-3.1-2.8-1.2-1.6-0.4-0.8-0.9-1.2 1-1.9 0.6-5.8 0.8-2.7-2.6 0.6-2.6-0.3-2.8 1.2-0.6 0.8-7.1 0.7-2.1-0.3-1.8-0.5-2-0.7-1.7-7.8-3 1.6-2 1.1-2.1 0.6-1.9 2.4-1.8 3.7-1.5 2.2-0.4 2-1.3-2.6-1.5-2.8-1-0.6-1.4-1.7-0.1-3 0.6 0-3.9 1.6-4.2 2.5-1.8 1.2-1.3-0.6-22.4-0.7-3.2-0.7-0.8-1.8-1.7-3.3-1.1-0.6-3.3 0.8 0.1 2.6 1.6 0.8 1.5 1-2.5 0.5-1.9 0.8-1.2 0.7-3.1-1.9-2.3-2.7 0.7-1.8 3.4-3-0.7 0.4-3.5 0.5-1.6-1.2-0.6-2.3 1.1 0.8-1.2 2.1-3.8 1.1-2.7 1.6-2.5-1.4-0.8-0.8 0.2-0.6-0.6-1.1 0.7-1.4 1.7-1.6 1.4-1-2.2 0.2-3.1 1.4-1.1 1.6-0.3-3.1-6.3 0-0.1 1.8-1.8 0.6-0.6 0.2-0.3 0.2-0.5 0.2-0.7 0.1-0.4 0.1-0.2 0.5-0.2 0.2 0 0.6 0.1 0.2-0.1 0-0.7 0.2-0.6 0.1-0.6 0.1-0.1 1.9 0.1 0.4 0.1 0.9 0.5 0.3-0.2 1.7-1.3 0.8-0.3 0.8 0 1 0.1 1.6 0.2 0.4 0.3 0.1 0.6-0.1 0.8 0.1 0.7 0.3 0.3 0.8 0 0.2-0.2 0.5-1 0.3-0.1 1.6 0 0.2 0.1 0.3 0.5 0.1 0.6 0.3 0.5 0.4 0.1-0.1 1 0.1 0.6 0.9 0.9 1.3 1.1 0.7 0.1 0.5-0.6 1.2-3.8 0-0.3-0.3-0.3-0.4-0.5-0.2-0.2 0.2-2.1 0.3-0.8 0.3-0.5 4 0.2 0.3-0.3 0.4-2 0.2-0.5 0.2-0.3 0.3-0.1 1.3-0.1 0.2-0.2 0.2-0.6 0.3-1.2 0.2-0.6 0.2-0.4 0.8-0.2 2.1 0 0.8 0.2 1.9 1.1 0.7-0.1 6.1-4.6 3.8-2.8-1.8-3.1 1.7-1.2 0.8-0.5 0.9 0 2.5 0.3 0.9-0.2 2.8-1.3 10.5-5.1-0.3-0.9 0-0.7 0.1-0.7 0.5-0.8 0.5-0.7 0.4-0.2 4.9 1.3 1.1-0.3 1.5-1.1 0.5-0.3 0.4 0 0.8 0.1 0.3 0 0.5-0.5 0.1-0.7 0-0.8 0.1-0.8 0.6-0.6 0.6-0.2 1.4 0.1 0.6-0.1 1.7-0.9 0.5-0.1 0.6 0 0.4 0.4-0.2 0.9-0.4 0.4-2 0.6-0.5 0.3-0.5 0.5-0.3 0.6 0.3 0.5 1.2 0.4 0.5 0.4 0.2 1-0.1 1-0.4 0.8-0.5 0.7-0.5 0.8-0.3 1.2-0.2 0.5-0.2 0.3-0.6 0.6-0.3 0.4-1.2 1.5-1.5 0-3.1-1.2-1.6-0.1-0.5 0.2-0.4 0.3 0 0.3 2.5 4.4 0.1 0.5 0.3 1.5 0.8 1.3 5 4.2 4.6 3.6 4.5 3.7 4.5 3.6 4.6 3.7 3.3 2.6 3 3.4 3.2 3.6 2.7 4.3 4.3 7.5 2.6 4.7 2.7 4.6 2.6 4.7 3.5 6.5 3.4 6.4 3.5 6.5 3.5 6.4 5.2 9.9 1.2 2.2z",
    KAZ3196: "M480.4 35l0 0.1 3.2 14.2-2.9 1.3 1.5 2.1 2.9 1.3 2 3.5-6 2.7-2.6 1.8 0.5 3.6 1.3 4 1.6 0.3 1.7 2.4-0.5 4.3-2.2 2.5 1 3.5 3.1 0.6 0.8 4.8 0.1 4.4 1.4 2.6 2.3 2.2-2.3 1.6-5.3 0.8-1.4 2.8 0.3 1.5 0.8 1 0.7 1.9 0 1.9-0.3 3.1-1.9 2.1-0.8 6.3-0.1 6.7-0.4 9-2 2.3-1.3 3.7-1 5.3-3.8 4.1-4.7 1.6-1.7-0.2-0.4 2.4 4 2.7 1.3 2.7-0.5 4.2-0.5 1.5-0.3 1.5 0.7 0.8-0.6 0.8-1.1 1-1.2 1.7 1 1.4-0.2 2.4 4.2 7.5 0.4 6.9 3.1-0.2 2.2-3.3 2.9-0.5 2.4 1.7 0.5 2.4 0.9 0.8 2.4-0.1 1.7 2.5 2.3 1.8 8.8 2.9 7.1-1.2 2.6 2.2 0 3-1.1 2 1.1 1.8 2.5 2 3.1-1.8 3.3-1.3 6.4-0.9 4-1.2 1.8-1.4 0.8 0.5 1 1.3 2.5 1.8-1.5 3-2.5 2.6-7.2 2.2-1.4 0.7 0 1.3 0.3 0.8-0.3 0.5-0.2 0.4 0.5 1.1-0.2 2-5.8 7.4-15.9 14.8-2.2 3-2.8 1.1-1.2 3.7-2.8 0.9-8.4 7.7-4.9 0.7-3.2 1.8-2.3 2.8-5.2 1.1-11-1.7-8.1 1.4-2.8 6.8-2.9 0.1 0.1 1-0.2 2 1.1 2.6-2.9 0.9-5.2 2.8-0.2 3.8-5.8 5.4-7.6-11.7-15.4-8.2 0.6-3.6-0.6-2.8-3-0.7-2.2 1.2-3.7-1-5.3-6.5-1.8-4.3-2.4-0.7 2.6-1.3 2.5-0.3-4.7-8.1-0.3-1.7 1-1.3 3.2 0.1-1-3 1.2-3.2 2.4-2.5 1.3-3.9 2.5-2 2.6 1.3 2.2-0.9-0.5-4.8-4.8-7.6-3.9-8.3-2.2-9.1-2.3 0.1-0.9-0.9-0.2-2.2 1.7-3.8-3.3-3.6 0.5-2.6 0.8-1.1-3.4-2.7-3 1.7-1.8-2.5-0.5-2.2-0.3-2.1-1.3-2.2-1.4 1.1-3.5 0.2-0.6 0-1.3-0.6-0.7-0.7-0.6-1-1-3.1-0.6-1-0.9-0.2-1.1-0.7-0.9-0.4-0.3 0.1-0.6 0.2-0.3 0-0.4-0.1-0.8-0.5-0.6-0.1-1.7 0.1-0.5-0.1-1-0.3-0.5-0.1-0.8 0.1-0.3-0.1-0.3-0.2-0.5-0.6-0.8-0.6-0.3-0.3-0.4-0.8-0.1-0.5-0.1-0.4 0-1.2 0.1-0.5-0.4-0.1-9.9-0.2-0.3-0.1-0.1-0.3-0.1-0.7-0.3-0.2-2.6-0.5-0.6-0.3 0.1-0.8 0.3-0.5 1.2-0.6 0.5-0.4 0-0.3 0-0.7 0.2-0.2 0.4-0.4 0.3-0.3 0.1-0.3-0.7-0.8-4.3-1.1-2.5-1.9-0.9-0.3-0.2 0.4-0.3 0.6-0.6 0.3-0.7-0.1-0.6-0.5-1.1-2.6-0.2-0.6 0.1-0.7 0.3-0.9 4.9 0 3.6-3.9 1.9-1.6 1.9-0.8 1.4 0 1.4 0.3 1.3-0.1 1.2-1 1-1.6 0.5-0.4 2.3-1 0.5-0.4 1.2-1.4 0.5-0.4 1-0.6 0.8-0.8-1.9-2.7-0.2-0.7-0.1-1.6-0.2-0.7-0.2-0.4-0.3-0.2-2.2-0.8-0.4-0.4-0.1-0.6 0.1-1.9-0.1-0.8-0.1-0.7-0.1-0.2-0.3-0.1-1.2 0.2-0.4-0.1-0.3-0.2-0.3-0.8-0.4-0.2-0.2-0.3-0.1-0.3 0-0.4 0-0.5 0.6-1.6 1-0.7 1.2-0.3 1-0.5 0.4-0.5 0.5-1.2 0.5-0.5 4.2-3.9-2-2 1.9 0 0.9-0.3 0.8-0.6 0.8-0.7 0.9-0.1 0.8 0.3 0.8 0.5 0.7 0.3 0.8 0.1 0.7-0.1 0.7-0.3 1.2-1 0.6-0.1 0.7 0.3 0.9 0.8 1.4 1.5 0.8 0.3 0.8-0.5 0.4-0.4 0.5-0.1 1 0.1 2.5-0.3 0.9 0.3 1.6 1.3 0.9 0.2 1.9-0.8 0.9-0.6 0.5-1 0.5-2 0.1-0.9-0.2-1-0.6-1-0.8-0.5-5.3-1-1.7-0.8-1.5-1.4-1-1.4-0.5-0.2-2.3 1.3-0.8 0.1-0.9-0.2-2.4-1.9-0.9-0.4-2.3 0-1.1-0.2-1-1-0.5-0.9-0.2-1.1 0.1-1 0.4-1 1.1-1.6 0.2-0.5 0.1-0.5 0.1-1.3 0.4-0.4 0.7-0.2 0.6 0.2 0.5 0.4 0.5 0.9 0.6 0.9 0.7 0.5 0.8 0 0.6-0.4 1.2-1.4 0.7-0.5 1-0.5-0.7-2.8-0.3-0.5-0.3-0.2-2.8 0.1-0.7 0.2-1.1 1.5-0.8 0.2-3.5-0.9-0.4-0.3-0.2-0.2-0.3-0.7-0.1-0.3-1.3-0.4-3.4-0.2-0.6-0.8 0.6-1 2.5 0.9 0.8-0.8 0.1-0.5 0-0.5 0-0.5 0.2-0.5 0.4-0.2 0.9-0.2 0.2-0.4 0.3-1 0.6-0.7 0.7-0.7 0.5-0.7 0-0.9-0.6-0.5-0.8-0.4-0.7-0.4-0.5-0.8-0.2-0.2-0.5-0.1-1.6-0.1-0.5-0.1-0.2-0.4 0-0.5 0-0.5 0.2-0.4 0.4-0.6 0.5-0.4 1.2-0.2 0.8-0.1 1.1 0.3 0.2 0.2 0.3 0.7 0.3 0.5 0.4 0.4 0.5 0 0.4-0.8 0-0.7-0.1-0.7-0.6-1.6-0.2-0.8 1.4-0.1 0.6-0.4 0.5-0.7 0.4-0.6 0.7-0.1 1.9 0.3 0.4 0.3 0.7 1.6 0.4 0.3 1.8 0.4 0.1 1.3 0.2 0.4 0.4 0.2 0.5-0.1 0.2-0.4 0.2-0.6 0-0.8 2.4 0 0.3 0.3 0.3 0.2 0.3-0.1 0.6-0.7 0.3 0 0.4 0.1 0.3 0.2 0.2 0.8 0.1 0.6 0.1 0.4 0.5 0.3 0.9 0.3 2.7 0.1 0-2.5 0-0.2 0-0.8 10-0.4 0.5 0.1 0.3 0.4 0 0.9-1.1 0.8-0.3 0.5 0.1 0.9 0.4 0.7 1.2 0.6 0.5 0.6 0.2 0.4 0.3 0.3 0.3 0.2 0.4 0 0.4-0.2 0.1-0.2-0.3-1.3-0.1-0.4 0-0.3 0.3-0.5 1.1-1.1-0.5-0.8-0.1-0.2-0.3-1.4 0.7-1.1 1.2-0.7 1-0.3 2.1-0.2 1.4-0.5 0.4 0.1 1.1 0.5 0.7 0.1 2-0.6 2.4 0.3 0.7-0.4 0.4-0.7-0.1-0.6-0.2-0.6-0.1-0.8 0.3-0.6 0.8-0.3 1.5 0.1 2.8 0.6 0.9 0 0.7-0.3 1.3-1.1 0.6-0.3 1.7-0.1 4-1.8 0.8 0 0.8 0.4 2 1.6 0.7 0.2 0.3 0.1 0.3-0.5 0.7-0.4 0.9-0.1 0.8-0.4 0.4-1.1-0.8-1.1-0.3-0.7 0-0.3 0.2-0.1 0.2-0.3 0.2-0.2 0.5 0.5 0.3 0.1 0.3 0.1 0.3-0.1 0.2-0.1 0.4-0.5 0.2-0.2 0.3 0 1.1 0.3 0.6-0.2 1.2-0.6 0.8-0.1 0.5-0.3 0.4-0.3 0.3 0 0.7 0.3 2.4-0.3 0.3-0.1 0.5-0.7 0.3 0 0.5 0.2 0.3 0.1 0.6-0.2 1-0.7 0.4 0.3 1.5 1.3 0.7 0.5 0.9 0.1 1.6-0.8 1.5-1.3 1.6-0.8 1.6 0.7 1.9 2.6 0.4 0.4 0.3 0.2 0.4 0.2 0.4-0.1 0.2-0.1 0.3-0.3 0.1 0 0.8 0.3 0.2 0 0.7-1.4-0.7-4.5 0.5-0.9-0.1-0.9 0-1 0.4-0.5 3.1-1.4 0.7 0 1.4 0.4 0.5-0.2 0.2-0.9-0.3-1.5 0.1-0.6 0.7-0.2 2.4 0.3 3.4 1.2 0.7-0.1 0.7-0.7 0.6-1.3 0.7-1.1 0.8-0.5 1.8-0.7 0.5 0.3 0.2 2.8 0.4 0.7 3-1.1z",
    KAZ3197: "M308.3 390.6l-0.5-12.9 0.2 0.3 0.4-0.2 0.3-0.7 0.1-0.9 0.1-1 0.5-1.7 0.3 0 0.3 0.7 0.5 0.5 0.7 0.1 0.3 1.1 0.3 0.8 0.6 0.5 0.3 1 0.7 0.2 0.4 0.3 0.6 0.4 0.4 0.2 0 0.4-0.2 0.8 0.3 1 0.2 0.6 0.4-0.2 0.3-0.1 0.5-0.3 0.3 0.5 0.2 0.8 0 1-0.4 1.3 0 0.7 0 0.8 0.3 1.2 0.2 1.3 0.5 1.2 0.3 0.9 0.3 0.4 0.1 1.3-0.3 0.7-0.1 0.5 0 0.8 0.3 1.2-0.2 0.9-3.7-2.5-5.8-3.9z m29.7-23.9l0.1 0.1 0.8 0.2 0.2 0.1-0.1 0.4-0.4 0.5-0.1 0.2 0.1 0.2 0.2-0.1 0.3-0.2 0.4-0.3 0.3-0.1 0.7 0 1 0.3 0.3 0.2 0.3 0.2 0.1 0.4 0 0.3 0 0.3-0.3 0.4-0.4 0-0.6-0.5-0.5 0.1-0.5 0.1-0.6 0-0.6-0.3-0.4-0.3-1-0.5-0.7-0.4-0.2 0-1.7-0.1-0.1 0.1-0.3 0.2-0.2 0.5-0.1 0.5-0.8-0.7-0.3-0.4-0.2-0.6 0.2-0.6 0.3-0.2 0.3-0.1 1.9 0.1 0.5-0.1 0.2-0.1 0.7-0.7 0.3 0 0.3 0.2 0.4 0.5 0.2 0.2z m4-0.2l0 0.4-0.1 0.2-0.3 0.1-0.2-0.1-0.5-0.5-0.2-0.1-0.8 0.1-0.2 0-0.1-0.2 0-0.3 0.3-0.3 0.9-0.2 0.3 0 0.3 0.1 0.3 0.2 0.1 0.2 0.2 0.4z m58.8-41.7l-4.2 3.3-0.4 1.6 7.3-1.2 6.3 4.3 3.4 0.5 6.1 2.9 3.3 0.3 1.3-0.4 0.8 0.7 29.2 11.8 2.1 0.3 5.9 5.3 5.2 2.5 34.8 8.3 4.1-0.3 2.5 0.2 0.6 3.5 0.4 3.4 2.6 2.1 1 3.6-1.9 3.2-1.6 8.2-0.1 4.1 2.3 2 3.2 2 1.9 2.3 0.3 26.2 2.4 1.8 1-0.2 1.5 0.6 1.4 1.1 0.3 1 2.3 2.9 5.7 4.3 0 2.2-2.2 0-1.7 2 0.7 1.3 0.2 2.3-1 2.7-2.5-1.5-0.1 3.5-1.8 3.1-0.6 3-2.1 2 0.4 3.3 0.7-2.8 0.9-0.4-0.1 1.2 1 0.1 0.2 2.4 1.7 1.4 0.6 0.4-1.1 1.2-12 5.9-11.8 7.9-5.2 4.6-2.2-0.8-3.7 1.8-7.6 7.5-4.2-0.7 0-1 0.2-0.4 0.5-0.2 0.3-0.6 0.1-1.8 1-16.5-7 3.8-0.4 0-4.8-9.8-2-4.3-1-0.8-5-3.2-2.5-2.6-5.2-6.9-0.8-0.7-0.8-0.1-0.9 0.3-7.2 4-2.4 1.4-0.9 0.2-4.7-0.4-13.9-1.3-3.7-0.3-7.4-0.7-2.3 0.4-15.6 2.5-11 2.1-1-0.5-1.8-2.1-4.2-4.7-5.6-6.5-3.1-3.6-5.4-5.9-1.5-2.4-0.2-0.8-0.2-1.7-0.1-0.8-0.5-1-0.6-0.7-6.6-4.4-7.6-5.1-9.7-6.4 0-1.5 0-1.3 0-3.9-0.1-1.8 0-1.9 0.2-1.5 0.3-2 0.2-1.7 0-1 0-1.3-0.3-1.5-0.2-1.7-0.3-2.4-0.4-1.2-0.3-0.5-0.5-0.3-0.8-0.2-0.7-0.1-0.5 0.1-1 0.4-2.4 0.1-0.5-0.2-0.4-0.3-0.3-0.3 0-0.4 0-0.4 0.1-1 0-0.9 0-0.4 0.2-0.4 0.2-0.2 0.2-0.2 0.3-0.4 0.2-0.4 0.1-0.4-0.1-0.5 0-0.4 0.1-0.3 0.3-0.2 0.6-0.1 0-0.8-0.1-0.3-0.3-0.2 0.1-0.3 0.3-0.1 0.3-0.3-0.1-0.3 0.1-0.3 0.3-0.1 0.3 0.3 0.3-0.1-0.1-0.5-0.2-0.4-0.9-0.5 0.3-0.3 0.2-0.4 0.4-0.2 0.3 0.1 0.2 0.3 0.8-0.2 0.2-0.2 0.5 0 0.5 0.1 0.6 0.1 0.5 0.3 0.4 0.3 1 0.3 0.5 0 0.8-0.3 0.4 0 0.2 0.3 0.8-0.1 0.8 0.2 0.2 0.1 0.7-0.1 0.2 0 0.2 0.2 0.5 0.7 0.2 0.2 0.2 0 0.3 0 0.5-0.3 0.3 0 0.3 0 0.3 0.2 0.8 0.5 0.2 0.1 0.2 0.1 0.5-0.2 0.4-0.3 0.3-0.5 0.6-1.8 0.9-1.3 1.1-0.8 2.4-1 0.5-0.3 0.4-0.4 0.2-0.2 1-2.2 0.2-0.3 1-0.9 0.3-0.1 0.6 0 1.6-0.4 0.5 0 0.8-0.7 2.2-1.3 1.1-0.9 0.3-0.2 0.4 0 0.9 0.3 0.4 0 0.3-0.1 0.3-0.2 0.3-0.3 0.2-0.4 0.1-0.6 0.2-0.4 0.3-0.4 0.2-0.4 0.2-0.8 0.2-0.4 0.1-0.3 0.1-0.5 0.3-0.5 0-0.3 0-0.6-0.1-0.7-0.1-0.5-0.2-0.8-0.2-0.3-0.2-0.1-0.7-0.2-0.4 0-0.3 0.1-1 0.5-0.4 0-0.4-0.1-1.1-0.6-2.6-0.3-0.7 0-0.4 0-0.8-0.3-0.7-0.4-0.3-0.3-1-1.6-0.7-0.8-0.1-0.2-0.2-0.3 0-0.3 0-0.3 0.2-0.7 0.2-1.2 0.4-1.2 0.1-0.6 0-0.7 0-0.6-0.2-0.3-0.2-0.1-1.1-0.3-0.2 0-0.4 0.3-0.1-0.1-0.6-0.5-0.7-0.2-0.7 0-0.9-0.4-0.3 0-0.7 0.1 0.3 1 0.2 0.4 0.2 0.3 0.4 0.3 1.3 0.6 2.7 1.8 0.1 0.2 0 0.3-0.3 0.2-0.9 0.5-0.2 0.2-0.7 0.9-0.4 0.3-0.3 0.2-1.9 0.6-0.4 0-0.4-0.1-0.3-0.2-1.5-1.6-0.1 0.4-0.3 0.9-0.1 0.4-0.7 1.8-0.3 1.1-0.5-2.7-1.1-3.4-0.2-0.5-0.3-0.4-0.3-0.4-0.4-0.1-0.3-0.1-0.3-0.1-0.7 0.2-0.6 0.4-0.3 0.2-0.7 0.8-0.9 0.6-1.2 1.1-0.4 0.2-0.9 0.2-0.7 0.7-0.2 0.5-0.2 0.5-0.1 1.2-0.1 1.3 0.1 0.6 0.1 0.6 0.3 0.4 1.3 0.8 0.5 0.9 0.6 0.7 0.6 0.1 5.3-1.8 0.5 0 0.4 0.2 1.2 1.8 1 0.9 1.1 0.6 1.2 0.4 1.1-1.5 0.2-0.1 1.1-0.3 0.2-0.1 0.2-0.3 0.3-0.7 0.1-0.4 0.9 0.4 0.2 0.3 0 0.2 0 0.7 0.1 0.8 0.2 0.3 0.2 0.4 0.3 0.8 0.6 0.9 0.4 0.4 0.1 0.4 0 1.9 0 0.5-0.2 0.5-0.2 0.4-0.3 0.4-2.3 2.3-0.3 0.3-0.4 0.2-0.5 0.1-0.4 0-1.7-0.5-2.6 0-0.4-0.1-1.4-0.6-0.5-0.1-4.2 0.2-0.5-0.2-1.3-1-0.5-0.2-0.6-0.1-0.6 0.1-0.6 0.2-2.3 1.6-1 0.4-1.9 0.3-0.5 0-0.4-0.1-0.3-0.3 0.2-0.4 0.6-0.5 0.4-0.4 0.2-0.5 0-0.6 0-0.6-0.4-1.8 0-1.7 0-0.4-0.3-0.6-0.3-0.6-0.6-0.6-0.3-0.2-0.2-0.1-1.4-0.1-1.9 0.3-0.2 0-0.2-0.1-0.6-0.6-0.1-0.1-0.5 0-0.3 0.2-0.2 0.3 0.1 0.6 0.5 0.7 0.7 1.4 0.7 1.2 1.1 1.3 0.8 1.1 0.6 0.9 0.4 0.9 0.3 0.5 0.1 0.6-0.2 1.1-0.5 1.3-0.7 2.1-0.4 1.2-0.3 0.9-0.4 0.9-0.1 0.8 0 0.6 0.3 0.7 0 0.6-0.4 0.4-0.6 0-0.6-0.1-0.7-0.2-0.5 0.3-0.5 0.7-0.7 0.8-0.4 0.6-0.4 0.6-0.4 0.2 0.1-1.5-0.3-0.5-0.5-0.4-0.4-0.1-0.5 0.2-0.3 0.5-0.2 0.6 0 0.4 0.2 0.6 0.1 0.5-0.1 0.2-0.3-0.1-0.3-0.3-0.4-0.5-0.6-0.4-0.4-0.5-0.3-0.5-0.2-0.1-0.2 0-0.2-0.1-0.2-0.5 0-0.7 0.6-2.6 0.9-1.9 0.4-0.5 0.8-0.5 0.2-0.2 0.3-0.6-0.1-0.5-0.3-0.6-0.3-0.4-0.4-0.4-0.4 0-0.5 0.1-0.4 0.3-1.7 1.5-0.5 0.1-1-0.1-1.1 0.1 0.9-3.8 13.6-18.2 6.1-6.4 6.1-4.5 5.9-1.4 0.7-4.3 2.4-2.4 5.6 0.4 6.1-9.4 2.3-6 5.3-7.5 6-1.5 7 0 4.6 7.4 3 1 3.6 3.6 3.7 5.5-0.5 1.6 2.3 0.7 2.3 0.3 2.8 1.4 2.2-0.1z m14.7 57.3l1.4-0.2 1.4-0.4 1.3-0.5 1.2-0.6 1.2-0.7 1.1-0.8 1-1 0.9-1 0.9-1.1 0.7-1.2 0.6-1.3 0.5-1.3 0.3-1.4 0.2-1.4 0.1-1.5-0.1-1.5-0.2-1.4-0.3-1.4-0.5-1.4-0.6-1.2-0.7-1.2-0.9-1.2-0.9-1-1-0.9-1.1-0.9-1.2-0.7-1.2-0.6-1.3-0.5-1.4-0.4-1.4-0.2-1.5-0.1-1.4 0.1-1.4 0.2-1.4 0.4-1.3 0.5-1.2 0.6-1.2 0.7-1.1 0.9-1 0.9-0.9 1-0.9 1.2-0.7 1.2-0.6 1.2-0.5 1.4-0.3 1.4-0.2 1.4-0.1 1.5 0.1 1.5 0.2 1.4 0.3 1.4 0.5 1.3 0.6 1.3 0.7 1.2 0.9 1.1 0.9 1 1 1 1.1 0.8 1.2 0.7 1.2 0.6 1.3 0.5 1.4 0.4 1.4 0.2 1.4 0.1 1.5-0.1z",
    KAZ3195: "M307.8 377.7l0.5 12.9-7.7-5.2 0.3-0.5 0.6-1.2 0.9-0.7 0.9-0.6 1-0.9 0.9-1 0.6-0.7 0.4-1.1 0.1-1.3-0.1-1 0-1.1 0.2-0.5 0.6-0.3 0.3-0.2 0.3 0.2 0 0.1 0 0.6-0.2 1.1 0.2 1.2 0.2 0.2z m120.6-88l4.7 7.8 1 2.2-2.4 2.6-30.9 22.5-2.2 0.1-2.8-1.4-2.3-0.3-2.3-0.7 0.5-1.6-3.7-5.5-3.6-3.6-3-1-4.6-7.4-7 0-6 1.5-5.3 7.5-2.3 6-6.1 9.4-5.6-0.4-2.4 2.4-0.7 4.3-5.9 1.4-6.1 4.5-6.1 6.4-13.6 18.2-0.9 3.8-0.8 0.1-0.5-0.2-0.2-0.6-1.2-0.1-0.3 0.1-0.1 0.3 0 0.3 0.2 0.6 0 0.1-0.3 0.4-0.4 0-1.5-0.4-0.1 0-0.5 0.3-0.5 0.2-0.2 0.2-0.5 0.9-0.3 0.4-0.4 0.1-0.2-0.1-0.3 0.1-0.2 0.2-0.8 1.4-0.1 0.4-0.5 1.5-0.4 1.1-0.1 0.4 0 0.6-0.1 0.4-0.2 0.9-0.1 0.8-0.1 0.7 0 0.6-0.1 0.6-0.1 0.2-0.6 0.7-0.1 0.2-0.1 0.6-0.2 0.5-0.2-0.2-0.6-0.2-0.6-0.1-2 0.6-5.9 1.7-5.6 1.7-12.2 3.7-6.9 2.2-6.9 2.3-5.1 1.6 0-0.2-4-27.4-1.7-4.6-1.3-2.5-0.5-2.2-0.5-1.8-2-3-0.6-3.7-19-7.2-5-4.7-3-4.5-2.2-5.4-2-3.1 0.1-5.2 1.8-12.6-0.3-4.2-2.2-9.1-2.4-2.9 0.1-4-0.9-4.4-1.1-4 0.6-4.6 0.6-0.8 0.1-2-1.2-4.1-2.2-0.2-1.1-0.9-0.8-1.4-1.3-0.2-1 0.7-1.1 1.7-1.2 3.3-9.1-2.1 0.5-1.3-0.3-1.8 0.1-3.5-0.7-4.2-1.9-4.5-1.8 2.6-0.9-0.6-0.2-1.1-2.8-1.5-1.7-1.6-0.1-3.8 1.2-4.3-0.1-2.3 1.2-0.3 1-2.2 1.2-1.1 3-0.9 0.7-1.3 1.3-0.1 2.9 0.3 2.8-1.5 0.1-1.5 1-1.4 1.5-1.4 1.3-1.9 0.4-2.1 1.1-2.3 0.3-1.6 0.6-2.2-0.7-1.4-1.1-0.8-1.3-2.3 0.3-4.1-1.1-3.9 1.6-1.8 0.4-1.9 1.8-2.9 0.1 0 1-0.5 1.4-1.1 0.7-1.2-0.1-1.2-0.8-2.8 0-0.8 0.3-1.5-0.1-0.8-0.1-0.2-0.3-0.4-0.1-0.3 0.1-0.4 0.3-0.7 0.2-0.3-0.3-1.5-1.4-0.8-1.4-0.4-0.5-0.5 0.3-0.3 0.4-0.3 0.3-0.3-0.5-1.5 0.2-0.5 0.5-0.2 2.1-0.7 0.5 0 0.6 0.1 0.5 0.3 1 0.9 1.6 0.8 0.5 0.5 0.7 1 0.8 0.9 3.8 1 0.3 0.9 0 1.5 0.5 1 5.5 5.1 0.5 0.4 0.5 0.1 0.4 0 1.3-0.3 0.6 0 0.6 0.1 0.6 0.5 0.4 0.6 0.1 0.6 0.3 0.5 1.1 0.5 0.4 0.4 0.8 1.1 1 0.7 0.9-0.3 1.7-1.9 4.1-2.3 3.6-3 0.5-0.9 0.9-4.7 0.2-0.9 0.3-0.5 0.4-0.1 2 0.9 0.8 0.2 0.6-0.1 0.5-0.5 0.2-0.6 0.2-1.4 0.3-0.4 1.3-0.2 0.4-0.5 0-0.5-0.3-1.2 0-0.5 0.3-0.5 0.3-0.4 0.8-0.7 0.7 0.3 2.2 3.2 0.5 0.2 1.7 0.4 0.5-0.1 0.1-0.4-0.2-0.7-0.5-1.2 0-0.8 0.3-0.5 0.5-0.4 0.5-0.2 0.6-0.1 0.4 0.2 0.9 0.7 0.6 0.3 0.7-0.1 0.6-0.1 0.6-0.3 0.6-0.2 2.1 0.2 1.1-0.3 1.6-1 0.4-0.1 0.4 0.2 0.3 0.8 0.2 1.5 0.2 0.7 0.4 0.3 0.6 0 0.4 0.1 0.4 0.4 0.4 0.6 0.2 0.6 0.3 1.4 0.3 0.6 0.3 0.6 0.5 0.5 1.6 0.9 0.6 0.2 0.6-0.1 0.6-0.3 0.4-0.5 0.3-0.6 0.3-0.6 0.5-0.3 1 0.1 2.3 0.8 0.8-0.5 0.4-1.1 0-1.3-0.2-2.6 0.1-1.4 0.5-0.9 0.3-0.9 0.7-0.1 1.1 0.6 0.4 0.1 1.4 0 0.3 0.2 0.2 0.2 0.4 0.6 0.2 0 0.7-0.3 0.3-0.1 1.9-0.4 0.2 0.1 0.1 0.2 0 0.6 0.6 0.4 0.6 0.4 0.7 0 0.6-0.4 0.2-0.7 0.3-0.8 0.3-0.7 0.5-0.3 0.5-0.2 1-0.6 0.4 0.3 0.7 0.8 0.3 0.5 0.1 0.6 0.1 0.5 0.1 0.4 0.5 0.3 3 0 1.1 0.5 0.7 1 0.2 1.7-0.1 0.7-0.3 0.6-0.4 0.4-0.4 0.5-0.1 0.5 0 0.7 0.3 0.7 0.3 0.4 0.4 0.1 1.3 0 0.2 0.3-0.2 1.5 0.1 0.6 0.4 0.4 0.7 0.1 0.7-0.2 0.7-0.1 0.5 0.5 0.4 1.3 0.2 0.4 0.4 0.5 1.9 1.4 0.9 0.4 6.7 0.6 0.5 0.2 0.4 0.5 0.3 0.6 0.3 0.3 0.3 0.2 0.4 0 2.4-0.5 1.2-0.1 1 0.7 0.5 0.5 1.3 0.6 0.3 0.2-0.1 1-0.6 0.3-1.4-0.2-0.6 0.5-0.1 0.6 0.4 0.6 1.4 1.2 0.8-0.3 0.8-0.9 0.8-0.7 1.3-0.2 1.3 0.1 1.2-0.2 1-1.1 1-2.3 0.3-0.6 0.8-0.9 0.3-0.5 0.1-0.7-0.1-1.3 0-0.8 0.1-0.7 1-2.5 0.2-0.3 0.2-0.3 1.1-0.4 1.5-0.1 1.3 0.5 0.4 1.4 0 1.4 0.6 1.1 0.8 0.9 0.9 0.7 1.3 0.7 10.2 1.2 2.5-0.4 6.7-2.3 6.8-2.3 0.8-0.8 0.5-1.2 0.4-1.6 0.4-3.5 0.2-1.1 1.1-6.5 0.6-1.8 0.8-1.1 1-0.6 1.3-0.3-0.4-0.2 0.6 0 3.5-0.2 1.4-1.1 1.3 2.2 0.3 2.1 0.5 2.2 1.8 2.5 3-1.7 3.4 2.7-0.8 1.1-0.5 2.6 3.3 3.6-1.7 3.8 0.2 2.2 0.9 0.9 2.3-0.1 2.2 9.1 3.9 8.3 4.8 7.6 0.5 4.8-2.2 0.9-2.6-1.3-2.5 2-1.3 3.9-2.4 2.5-1.2 3.2 1 3-3.2-0.1-1 1.3 0.3 1.7 4.7 8.1-2.5 0.3-2.6 1.3 2.4 0.7 1.8 4.3 5.3 6.5 3.7 1 2.2-1.2 3 0.7 0.6 2.8-0.6 3.6 15.4 8.2 7.6 11.7z",
    KAZ4829: "M746.8 462.3l-1 1-1.3-0.4-1.6 0.1-0.8-0.7 0-1.6 0.7-0.6 0.7-1.4 0.9-1.1 0.8-0.8 1.5-0.5 0.9 0.8-0.1 3-0.7 2.2z",
    KAZ3251: "M555.4 369.2l3.1 5 5.4 14.1 7 25.1 2.6 16.9 0.2 2.6-0.8 1.2-5.9 17.8-2.2 3-0.6 1.1 1.3 0.5 0.9 0.8-0.1 1.5 1.4 2.1 1.1 0.6 2.6 0.9 2 0.1 1.2 0.3 1.7 1 1.9 0.7 0.5 2.2 0.8 0.9 1-0.1 0.7 0.7-0.6 1.1 1.4 1-0.4 1 0.6 1.1-0.2 2.5 0.3 2.9 1.3 2 1.8 0.4 1 0.5 1.6 1.4 1.1 1.5 0.7 1.1 1.1 0.5 2.5 2.8 1.8 0.7 0.1 1 0.6 0.4 0.1 0.4 1.3 1.3 0.1 0-0.6 0.5-0.3 0.6 0.2 0.4 0.9 1 0.6 0.3 0.7 0.2-1.7 1.3-0.9 0.5-0.9 0.1-1.2-0.3-0.6 0-0.3 0.5-0.2 0.6-0.3 0.5-0.6 0.8-0.6 1.1-0.5 1.2-0.5 0.9-0.7 0.4-0.6-0.1-0.5-0.2-0.4-0.4-0.2-0.6-0.2-0.4-0.4-0.4-0.4-0.4-0.4 0-2.3 1-1.1 0.9-0.6 1.4-0.2 0.5-0.3 0.4-0.4 0.2-0.4 0.1-0.5 0.3-0.3 0.4-1.7 2.8-0.6 0.8-2 1.5-1.4 0.5-0.4 0.3-0.3 0.5-0.7 0.9-1 0.4-2.8 0.1-3.3 0.9-0.4 0.4-0.7 0.8-2.5 1.9-1.4 0.5-0.6 0.5-0.3 0.8 0.1 0.4 0.4 0.7 0 0.4-0.3 0.1-1.4-0.2-0.4 0.1-0.9 0.6-0.4 0-0.7-0.3-0.4 0.1-1.4 0.9-0.5 0.6-0.3 0.6-0.5-0.1-0.4 0.1-1.1 0.6-0.2 0.1-0.2 0.3 0.1 0.2 0.4 0.5 0.1 0.4 0 0.5-0.4 3.1-0.2 0.4-0.4 0.4-0.8 0.6-0.9 0.4-1 0.2-0.6 0.1-0.3 0.5-0.6 1-1.7 0.8-0.6 0.7-0.9 2.6-0.5 0.6-1.5 0.7-0.7 0.5-0.3 0.7-0.4-0.2 0 0.3 0 0.8 0 0.3-0.1 0.3-0.3 0.3-0.1 0.5 0.2 2.7 0.2 0.6 0.2 0.3 0.2 0.1 0.6 0.2 0 0.4 0.1 0.4 0.2 0.4 0.2 0.3-0.1 0.3-0.6 0.9 0.5 0.3-0.2 0.4-0.5 0.3-0.6-0.1-1-0.5-0.3 0-0.2 0.1-0.1 0.3-0.2 0.3-0.4-0.1-0.3 0.3-0.1 0.2-5.7-3-1.7-1.3-2.2-2.3-0.8-0.4-0.9-0.2-0.2-0.1 0.1-0.3 0.2-0.2 0.9-0.3 0.4-0.4 0.3-0.4 0.3-0.4 0.4-0.8 0.3-1.1 0.3-1.1 0-0.9-0.3-0.5 0-0.3 0.5-0.4-0.3-0.1 0.1-0.3-0.1-0.8-0.5 0.1-0.4 0.7-0.2-0.3-0.3-0.1-0.3 0.2-0.3 0.2-0.3-0.5-0.2-1.3-0.3-0.6-1.1-1.4-0.2-0.4-0.3-0.9-2 1.3-0.5 0.1-0.5-0.2-1.1-0.5-0.5-0.2-3.1 0.2-5.2 0.3-14.5 0.9-2-0.5-1.1-1.4-2.1-9.6-1.6-8-0.8-8.3-0.1-0.1-0.6 0-11.2-0.1-0.2-0.1-0.1-0.4 0.2-11.9 4.2 0.7 7.6-7.5 3.7-1.8 2.2 0.8 5.2-4.6 11.8-7.9 12-5.9 1.1-1.2-0.6-0.4-1.7-1.4-0.2-2.4-1-0.1 0.1-1.2-0.9 0.4-0.7 2.8-0.4-3.3 2.1-2 0.6-3 1.8-3.1 0.1-3.5 2.5 1.5 1-2.7-0.2-2.3-0.7-1.3 1.7-2 2.2 0 0-2.2-5.7-4.3-2.3-2.9-0.3-1-1.4-1.1-1.5-0.6-1 0.2-2.4-1.8-0.3-26.2-1.9-2.3-3.2-2-2.3-2 0.1-4.1 1.6-8.2 1.9-3.2-1-3.6-2.6-2.1-0.4-3.4 46.3 0.8z",
    KAZ3252: "M717.6 471.9l-0.6 0.1-0.3 0.3-0.8 2.1-0.2 0.4-0.6 0.6-0.7 0.6-0.7 0.3-0.7 0-1.5-0.7-0.5 0-1 0.1-5.5-0.8-1.6 0.1-0.7-0.2-1.4-0.9-0.6-0.4-0.7-0.5-1.5-0.4-0.7-0.4-0.7-0.6-2.1-1-2.8-0.5-0.8-0.3-2.5-2-0.4-0.4-0.6-1-0.3-0.5-0.5-0.2-1-0.3-0.8-0.6-1.1-0.1-0.5-0.3-1.2-1-0.7-0.4-0.8-0.1-0.7-0.3-1.3-1.2-0.7-0.4 0.7 0.9 0.2 0.6 0 0.5-0.4 0.2-3.9 0.5-0.4-0.1-0.9-0.9-0.5-0.2-0.7 0.6-0.9 2.3-0.9 0.5-1 0-0.4 0-4.2 1.8-1.2 0.7-0.3 0.3-0.3 0.5-0.9 2.7-0.5 0.9-0.2 0.5-0.1 0.6 0 0.5 0.3 1.1 0.1 0.6-0.1 0.6-0.6 1.4-1.3 2.9-0.3 1.2-0.1 1.3 0.2 1.1 0.4 1 1.8 3.5 0 0.5-0.7 0.2-1.5-0.6-2.2-0.4-0.3-0.4 0-0.7 0-1.1-0.3-0.4-0.6-0.2-1.7-0.1-0.5-0.2-0.4-0.2-0.5-0.4-0.9-0.4-1.1 0-3.1 0.5-0.8 0-0.9-0.2-0.8-0.4-0.6-0.6-0.9-1.6-0.5-0.6-0.8-0.4-3.5-0.9-1.7 0-0.9-0.1-2.9-2-1.6-0.6-3.1-0.2-0.4 0.1-0.7 0.4-0.4 0-0.7-0.2-2.9-1.6-1.5-0.4-0.4-0.1-0.4-0.3-0.3-0.2-0.4 0.1-0.9 0.3-1.7 0.1-0.8 0.3-2.7 1.7-0.4 0.1-0.3-0.1-1-0.9-0.2-0.1-0.3 0-0.4 0.2-1.2-0.4-0.6 0-0.4 0.4-0.3 0.5-0.3 0.5-0.4 0.4-0.4 0.2-0.5-0.1-0.6-0.3-0.5-0.2-0.5 0.2-0.5 1-0.3 0.4-0.4 0.3-0.9 0.3-0.2 0.3-0.1 0.7 0.1 1.3-0.2 0.5-0.4 0.4-2.1 0.4-0.9 0.5 0.3 1.3 0.2 0.3 0.9 0.8 0.2 0.3-0.1 0.4-0.2 0.3-0.4 0.2-0.4 0-1.1-0.8-0.4 0.4-0.3 0.5-0.3 0.7-0.2 0.6 0.1 0.4 0.2 0.4-0.2 0.4-0.8 1-0.3 0.3-0.1 0-1.3-1.3-0.1-0.4-0.6-0.4-0.1-1-1.8-0.7-2.5-2.8-1.1-0.5-0.7-1.1-1.1-1.5-1.6-1.4-1-0.5-1.8-0.4-1.3-2-0.3-2.9 0.2-2.5-0.6-1.1 0.4-1-1.4-1 0.6-1.1-0.7-0.7-1 0.1-0.8-0.9-0.5-2.2-1.9-0.7-1.7-1-1.2-0.3-2-0.1-2.6-0.9-1.1-0.6-1.4-2.1 0.1-1.5-0.9-0.8-1.3-0.5 0.6-1.1 2.2-3 5.9-17.8 0.8-1.2-0.2-2.6-2.6-16.9-7-25.1-5.4-14.1-3.1-5 122.5-0.7-3.1 6.3-0.6 8.4 0.5 5.7 0.5 1.9 0.5 1.7-6 0.4-0.8 2.9 2.1 3.3 2.4 0.1 2.4 0.5 0.4 3 1 2.3 9.4 8.7 0.5 4.7 1.3 3.6 3.6 3.4 5.4 0.8 4.1 2.4 2.4 2.3 2 2.5-0.2 3.8-1.2 3.1-6.2 10.6 2.1 0.9 2 0.5 0.9-0.4 0.8 1 2.2 0 2 1.2-1.6 2.4-1.6 3.3 0.5 1.6 0.2 1 0.3 0.9 0.7 1.2 2.1 1.7 2.9 1.2 2 1.8-1.7 1.2 4.7 0.7 0.8 0.8z",
    KAZ3198: "M82.7 359.3l-0.4 0.2-0.6-0.5-0.4-0.8-1.1-3.2 0-0.7 0.7 0 1 0.7 0.5 0.4 0.3 0.5 0.4 0.9 0 0.9-0.4 1.6z m162.5 4.7l-2.7-1.3-1.2 0-1.9-0.7-2.5 0.2-1.4-1-0.9-1.6-2.4 0.3-2.7-1.3-0.1-3.4-2-2-1.7 1.1-4.9-4.4-5.6 4.8-8.4 0.4-19.1 4-0.8 0.7-1.4-0.1-2 0.6-3.9 5.3-2.3 2.3 1.5 1.8-5.4 3.4-12.1 3-4.3-1 0.3-0.4 0.2-0.2 0.7-0.2 0.2-0.2 0.8-1.2 0.3-0.6 0.5-1.4 0.4-0.7 0.7-0.7 0.2-0.3 0.1-0.3 0.2-0.8 0.2-0.4 0.5-0.9 0.1-0.6-0.1-0.6-0.1-0.4 0.1-0.4 0.6-0.8 0.2-0.5 0.1-0.6 0.2-2.4 0.8-3.2 0-2.4-0.7-1.9-1.1-1.4-1.3-1.3 0.5 0.1 0.8 0.6 0.4 0.3 0-0.3-0.4-0.3-0.2-0.6 0-0.6 0.1-0.7-0.4 0.2-0.6 0.6-0.5 0.4-0.5-0.2-0.1-0.7 0.4-1 0.7-1.5 0 0.7 0.4 0.3 0.4 0.1 0.4-0.3 0.2-0.6 0.1-1.3 0.2-0.6 0.2 0.7 0.2-0.1 0.2-0.6 0.2-0.5 0.2 0 0.6 0.1 0.2-0.1 0.4-0.2 0.1-0.5 0.1-1.4-0.5-0.8-0.7-0.4-0.8-0.1-0.5-0.2-0.1-0.5 0-1.1-0.4-2.5-0.5-1-0.1-0.4-0.2-0.2-1.1 0.1-0.4 0-0.4-0.5-0.9-1.5-0.4-0.3-1.1 0.1-2.3 0.4-1.3-0.2-2.8 0-0.6-0.2-0.4-0.2-0.9-1-0.4-0.2-1.6-0.2-0.4 0-0.3 0.4-0.1 0.4 0 0.9-0.1 0.3-1.1 1.5-0.4 0.3-2.3 0.9-0.7 0-0.8-0.6-0.3 0.1-0.2 0.3 0.1 0.5 0.1 0.1 0.7 0.2 0.2 0.1 0.3 0.6 0.1 0.4 0.1 0.4-0.2 0.1-3.1-0.2-0.6-0.3-0.4-0.3 0-0.3-0.1-0.5-0.1 0.1-0.3-0.3-0.8-0.8-0.2-0.2-0.2-0.2-0.1-0.4 0.2-0.2 0.2-0.3 0.2-0.3 0-0.4-0.5-0.2-0.5 0.7-0.5 1.3-0.4 0.2-0.8 0-0.8-0.2-0.6-0.3 0.8-1.1 0.2-0.2-0.5-0.2-0.6 0.3-1 0.7-0.6 0-0.5-0.3-0.5-0.4-0.4-0.5-0.9-0.7-0.3-0.6 0.5-1.5-0.1-0.8-0.3-0.5-0.6-0.1-0.5 0.4-0.2 0.7-0.2 0.7-0.2 0.7-0.5 0.1-0.5-0.7-0.6-1.2-0.5-0.5-0.6-0.3-1.5-0.3-1.4-0.5-3.1-0.7-1-0.6-0.7 0.1-1.1 0.8-0.9 0.5-3.3 0.8-0.4 0.2-0.7 1-0.5 0.2-0.3-0.1-0.6-0.5-0.2-0.1-0.3 0.1-0.1 0.3-0.1 0.2 0.1 0.1-0.1 0.4-2 3.3-0.4 0.4-0.2-0.6 0.1-0.5 0.4-0.7 0.1-0.4-0.1-0.3-0.3 0.2-0.9 1-0.6 0.3-0.7 0.2-0.5-0.2-0.3-0.1-0.2 0.7 0 1.7-0.2 0.6-0.4 0.5-0.4 0.3-0.4-0.2 0.4-0.6 0.3-0.5 0-0.4-0.6-0.2-0.8 0.3-0.2 0.1-0.3 0.3-0.2-0.1-0.2-0.2-0.8-0.7-0.2-0.1-0.4-0.1-0.3 0.5-0.8 2.3-0.4 0.4 0.1-0.4 0.1-1.3-0.4 0.3-0.1 0.4-0.2 0.5-0.2 0.3-0.1 0.1-0.5 0-0.2 0.1-0.3 0.3-1 2-0.5 0.6-0.5-0.1 0-0.3 0.2-0.5 0.1-0.3-0.3-0.2-0.3 0.1-0.8 0.5-2.1 2.3-0.7 1.2-0.4 0.5-0.5 0.3-0.9-0.1-0.4 0.1-0.2 0.2-0.4 0.5-0.2 0.3-0.9 0.3-0.3 0.6-0.3 0-0.7-0.2-0.3 0.3-0.1 0.7-0.1 0.4-0.7-0.2-0.3-0.5-0.2-0.2-0.2 0.1 0 0.8-0.1 0.3-0.2-0.1-0.5-0.7-0.4-0.2-0.2 0-0.1 0.3 0.1 0.9 0 0.3-1.6-2.1-0.4-0.2-0.1 0.7 0.2 1.6-0.5-0.3-0.2 0.2-0.2 0.4-0.3 0.4-0.2-0.8-0.3-0.5-1.4-0.6-0.5-0.3-0.2-0.5 0 0.4 0 0.6 0.4 0.4 0.2 0.9 0.2 0.4-0.3 0-0.8-0.4 0-0.3 0.1-0.3 0-0.2-0.1-0.2-0.5-0.4-0.2-0.2-0.2 0.2 0 0.3-0.8-0.1-0.9-0.3-0.7-0.6-0.4-0.7 0.1 0.9 1.6 1.9 0.4 1.1-0.5-0.2-0.9 0-0.1 0-0.3-0.4-0.2-0.6-0.3-0.4-0.5-0.1 0 0.3 0.4 0.4 0.1 0.7 0.1 0.7 0.6 0.4 1 0.9 0.1 0.4 0 0.3 0 0.4-0.1 0.4-1.1-0.8-0.4-0.4-0.4-0.5-0.3-0.6-0.3-0.4-0.4-0.2-0.7 0 0.5 0.4 0.2 0.4 0 0.5 0.1 0.3 0.9 1-0.3 0.3-0.4-0.3-1.9-0.6-0.3 0-0.1 0.2 0.1 0.3 0.1 0.3 0.3 0.1 0.3 0 0.4 0.3 0.1 0.7 0.1 0.6 0 0.6-4.7-2.8-1.8-1.4-1-1-0.5-0.3-0.7 0-0.6 0.1-0.6 0-1.5-1.2-0.6-0.8-0.7-0.5-1-0.3-2.2 0-0.9-0.4 0.6-0.8-0.4-0.3-0.1-0.5-0.1-0.5-0.2-0.4-0.9-0.5-0.5-0.2-0.4-0.1 0.6-0.8 0.6-1.7 1.3-1.1 1.5-0.3 1.4 0.5 0.5 0.4 0.4 0.6 0.6 1.4 0.6 0.2 3.8-0.7 0.7-0.6 1.4-1.7-7.5-11.9-3.7-9.7-0.3-0.8-0.2-0.6-0.4-0.3-0.9-0.1-0.9-0.3-0.6-0.6-1.2-1.7-4.6-8-1.5-1.3-1.7-0.8-9.6 0.2-5.2-2.5-0.6-0.1-0.4 0.3-0.2 0.6 0 2.2-0.1 1.7-0.2 0.7-0.4 0.1-4.5-3.2-0.8-0.8-0.5-0.6-0.1-0.4 0.2-0.4 0.3-0.7 0.5-0.5 0-0.3-0.1-0.2-0.8-1.4-1.7-2.1-0.2-0.5 0.3-0.3 2-1.2 1-0.9 0.3-0.7-0.4-0.5-1.4-0.1-0.3-0.5 0-0.6 0.1-0.7 0.2-0.6-0.2-0.7-0.2-0.3-0.5-0.5-0.1-0.4 0.1-0.2 0.4-0.4 0.4-0.7 43.8 6.6 12.8 4.5 0.7-2.8 11-1 20-13.9 1.8-0.5 1.2-1.1 1.2-0.6 1.2-0.1 2.4-3.2 2.7-2.2 2.5-0.3 2.7-1 9.4 1.2 1.8 0.6 0.3 1.2-0.7 2.9 3.8 1.5 2 0.4 2.2 0.1 0.9-1.2 8.5 2.8 1.6-0.7 0-1.6 0.8-4.4 3.6-5.7-0.2-4.4 3.7-4.6 1.4 0.4 8.7 0 2.6-0.8 0.8-0.5-0.3-1.1 0.7-0.9 4.7-4.6 1.7 1.6 2.8 1.5 0.2 1.1 0.9 0.6 1.8-2.6 1.9 4.5 0.7 4.2-0.1 3.5 0.3 1.8-0.5 1.3 9.1 2.1 1.2-3.3 1.1-1.7 1-0.7 1.3 0.2 0.8 1.4 1.1 0.9 2.2 0.2 1.2 4.1-0.1 2-0.6 0.8-0.6 4.6 1.1 4 0.9 4.4-0.1 4 2.4 2.9 2.2 9.1 0.3 4.2-1.8 12.6-0.1 5.2 2 3.1 2.2 5.4 3 4.5 5 4.7 19 7.2 0.6 3.7 2 3 0.5 1.8 0.5 2.2 1.3 2.5z"
  },
  names: {
    KAZ4830: "Astana",
    KAZ3236: "Mangghystau",
    KAZ3202: "Aqmola",
    KAZ3203: "Qaraghandy",
    KAZ3201: "West Kazakhstan",
    KAZ3206: "East Kazakhstan",
    KAZ3207: "Almaty",
    KAZ3204: "North Kazakhstan",
    KAZ3205: "Pavlodar",
    KAZ3196: "Qostanay",
    KAZ3197: "Qyzylorda",
    KAZ3195: "AqtÃ¶be",
    KAZ4829: "Almaty City",
    KAZ3251: "South Kazakhstan",
    KAZ3252: "Zhambyl",
    KAZ3198: "Atyrau"
  },
  default_regions: {},
  proj: "mercator",
  default_labels: {},
  proj_coordinates: [{
    lat: 51.1175,
    x: 291.4,
    lng: 58.364722,
    y: 177.7
  }, {
    lat: 52.8773,
    x: 729.5,
    lng: 76.293491,
    y: 107.8
  }, {
    lat: 51.0599,
    x: 287.9,
    lng: 58.222752,
    y: 179.9
  }],
  initial_view: {
    x2: 1008.99,
    x: 0.99,
    y2: 554.49,
    y: 0.99
  }
};
/* Map logic - do not edit */

eval(function (x) {
  var d = "";
  var p = 0;

  while (p < x.length) {
    if (x.charAt(p) != "`") d += x.charAt(p++);else {
      var l = x.charCodeAt(p + 3) - 28;
      if (l > 4) d += d.substr(d.length - x.charCodeAt(p + 1) * 96 - x.charCodeAt(p + 2) + 3104 - l, l);else d += "`";
      p += 4;
    }
  }

  return d;
}("(function (plugin_name) {` .') ` \"(funcName, baseObj) {` ,$ = ` \"%|| \"docReady\";` E# =` N$ || window;var readyList = []` )&Fired = false` -&EventHandlersInstall` ='`!e%` H!() {if (!` h&) {` r)true;for (var i = 0; i <`!N&.length; i++` V$List[i].fn.call(`\"*\",` K&[i].ctx);}`\"1+}}`!U*StateChange`!f$document.` 7& === \"complete\"`!?$();}}`#P#[`#x$]`$%#`$O\"callback, context` ~#`\"e)setTimeout`%/*` U$(` T$;}, 1);return;} else`\"h'.push({fn:`!,'tx:` X#});}`!{B || !` >%attach`%5! && ` F5interactiv`\"~!`\"+'` E!`!v!`!m#`%L&`%{2`#{,add` D!Listener) {` #5(\"DOMContentLoaded\"`%S#,`' \");`%j\"` G/loa` <.`#`$`\"]0(\"on` I!statec`&$!` Y$`&1'`!)&` M*`!$(`'%$`(j5`(G!}};})(`*7&,`*&#);`%q'console, Object, Array`#N#typeof ` =#`$U\"undefined`%6!` 4*.log` 6,) {` W% {};` >)`'t') {}`&O\"` m#`!K\".create !== \"` F$\") {` 1*` ^(o`-'#` )!F` t!F.prototype = o`(0# new F;`!0#!`\"U!` ?&.forEach) {` #3`!))fn, scope) {`,8*, len = this`,9& < len; ++i) {`,7$` ]!,` F![i], i` &\")`$h!})`,X#.`$M%` )#`$U$` (#`$^\";})()`.k!helper =`04\"`#g%` $%delete_element(e) {e.parentNode.removeChild(e);`-O&clear_sets(arr`\"I-`\"A\"arr`.k+var set = arr[i];set`#M$`!X'`!B\"`!7\"();})` C!splice(0, se`/k$)`/ *placeAll(str, find,` .$`.l!`%8!str.` .#(new RegExp(` H\"\"g\")` J&`\"G'to_float(str`\"##num = parseF` 2%;if (isNaN(num)`!-&`2X\"`.g&`&c\"um`!v(`+f$(obj,`(!, fn`)N#obj`*p() {obj[\"e\" +` F! + fn`0v!n;obj[` &*`%4(` D0`&:$e` w!;};`! +(\"on` J$, ` r*`-6&` P!`-c,`\" $`-d&`\"H&linePath(startX, ` #!Y, endX, endY`%p$tart = {x:` D$y` $\"Y}`'h!end` 9\"` U\"y:endY}`*K$\"M\" +` _\".x + \" ` &&y` +!L\" + end` 4'end.y`'j)one(src`.O!n`&1!`,O'` .' != \"o`)Y!`-k!` /(=== null`&h'` 4&;}`&6!ew` E&` 3(`*]!tructor();`),'i` Z)) {` Y'[i] =`!l.[i]`0m!`-L$`!6*isMobi`/9\"Android:`%z)` Q$avigator.userAgent.match(/` P#/i);}, BlackBerry` 8L` P&` d#iOS` 4LiPhone|iPad|iPo`!M$Opera` <L` P!\\sMini` `#W`(Y!s` 9LIE`#^\"` _#an`\"e2`$#$.`#J#() ||` ,&`\"u&` *+iOS` #+`!u!` %+`!u#(`1(!`!6%isF` $#`.q%ToCheck`)J#getT`3R\"{`(}%` ;+ &&` ?$.toString`2J\"` b-=== \"[`(L\" `!2$]\"`)2'findPos(obj`1x(getStyle`-n\"styleProp`-j'curren` @\"`!u#y = ` +,[` S%]`,h$if `-P$getComputed` Y,` +3`!K\"`*D!` n)`#$#`+L(scrollDist(` q#html = document.getE`4@\"sByTagName(\"html\")[0]`11!html.` e\"Top &&` U&` `$` ]#` <&`&:&[` T'Left,`!?!` @&`\"h)` |+||` NY +` ?<`!=0` 4>`!f,` F%body`!D1` .0`!6\"` E3`$f&[0, 0]`-u\"body_posi`$y!=`&z&` Y), \"` <$\")`$]!` I+= \"relative\") {`!4+tyle.` z'\"static\"`!=\"`'S#` vC` K#top` #$left =` .# = 0, scr`(Q\", fixed = false;while ((` ;\"scr.parentNode) &&` U!!`'C'body) {` $-` O\"`%F'|| 0;`!5#` 1)`&G#0`#*!`\"1%scr`\"#)`#6!`!_!\") {`!e$true;}}if (` -\"&& !`)v#opera`)5#scrDist`\"%\"`)O%;`!f$+` 3!Dist[0]`!]$` *'1];}do`\"4&+`+e\"offsetLeft` I'` /&Top;} `#C#obj`,:#` 7\"P`#J!);`%#;`%r)`.k$[`!A#,`$e#]`+m'distance(xy0, xy1`\"[#x0 = xy0.x`%J!y` '$y` *!x1` *!1` 6$` '$` 8\"dx = x1 - x0` +\"y = y1 - y0`!M$Math.sqrt(dy * dy + dx * dx)`!U'rotate(point, transform`!^$ = ` 6![0]`!D\"` (%1` +\"str = Raphael.` W%Path(\"M\" + x + \",\" + y` u(`1e%(`(6\"re = /M(-?\\d+.?\\d*),(` \"'/` A!m = re.exec(str)`#z%m[1], m[2]`#r(bbox_union(arr`\"-$a = [`!y\"x2` \"'y2` /'y` %#for (var i = 0; i < arr.length; i++` q#bb = arr[i];xa.push(bb.x);x2` $'2);y` %&y);y` 3'y2)`+W\"x = helper.min(xa`# \"x2` -'ax(x2` 2#y` B*y` /$` A+y2a`#+%{x:x, x2:x2, y:y, y2:y2, width:x2 - x, height:y2 - y}`#R'mi`#Q!ay`/T&`&P!min.apply(Math, ` >\"` M(ax` C2ax` ;:`''\"_bbox(bbox`&.`$P!bbox.x,`%9!.y`$i\"b` /&2` ,*c` A.2` 4\"d` A/` 4#a2 =`(T$a`!;(`!'\"` 3'b` --c` 3'c` --d` 3'd` --x_`\"p! = [a2[0], b2` \"!c` (\"d2[0]`':#` >)1` E\"1` E\"1` E\"1` D#x_min =`$m!` {#`!'$max =`$M!` ,)y` G'`!\"#` 0%` I%` 0%`&F'_min`&I!` #\"x2:` !`&V\"` $\"`&X#` d!-`!V\"`&\\&` /#` Z!`&a(x_in`!)\"(x, `1 $i = a`)c$`/s#i--) {if (a[i] === x`&_&`1{#` '#`3w\"` &${min:`!I!max:`!g!addEvent:a` \"#, isMobile:i` \"#, linePath:l` \"#, clone:clone, isF`\"!#:i` \"%, findPos:f` \"\", replaceAll:r` \"%,`(+(:`(8'` 1$` -#`'<\"`-H\":`-O&,`1Z%:`1d$,`#C':`#O&, clear_sets:c` \"%, delete_element:d` \"), to_float:t` \"#};})`/o#mapdata = window[plugin_name + \"_` :#\"] ? ` \"=:`$J#` s#info` ]9info` [;` <#` k+` =!= ` E'.subs`2*\"0,` ,)`&X\" - 3).`$O#(\"simplemaps_\", \"\"`*U#emo =`&^\"`+C!randed =`!3'autoload`*4&`4\"#hared_paths = {rounded_box:\"m2.158.263h5.684c1.05 0 1.895.845` $\"` *\"v` =\"0 1.05-` 4&-` 9'h-` ?\"-` c\"` 5\"` G!` $#` d\"` ?#0` B!` 4&` d(z\"`#*!s:\"m4.8 1.5c-.111 0-.2.089-.2.2v3h-2.9` /(134-.2.3 0 .166.089.3.2.3h2.9v3c0 .111` 2!2.2.2h.2c` X\" .2-` 2\"-.2v-3h3.1` /(134.2-.3 0-.166` G!-.3-` 0!h-3.1v-3c0`!G!` 6#2` 8!2z\", min`\")!1.8 4.7h6.6` s&` v\"`!l%`!A\"3`\"#!h-6.6`\"6'`!C!`!.\"`!B#`!A#` .!\"`2z!ow:\"m7.07 8.721c2.874-1.335 2.01-5.762-2.35-5.661v-1.778l-3.445 2.694 ` $$843v-1.818c3.638-.076 3.472 2.802 2.35 3.721z\"}`&R!hooks_object = {over_s`+r!`'/!, ` -!region` &)loca`-.!` -$ut` J+ut` H,ut` F-click` K*` -\"` M*` .\"` K.ose_popup` +$zoomable_` f/` -+` s+omple` C&refresh_` (,zooming` '-back` R&ady`!m*x` )#`#q\"`+Z#`#y!`#h+[]`#f*` '%`\"T%` -!`#e%` &$` C(`#\\([]`#W*` &&` G'`#P+[], pre` G,` *%` O'` +%`!\"+`$/&[]`#3` &/`!G(`#X$[]`#z/` T$`#})[]`$ #` M\"ady`\"2'xy:[]`#x\"api`'h'`19#:`1A#,`13$:`0X#, load:load,`(J\":helper.clone(`(W()`0S%` 6/` .(), copy:function () {var new_` C\"`!P(` [)this.` 3#)`!e&` /1info)`!%n` z!copy`\"r'};`2.*.push(`\"&&);return`\"8';}, cre`)^!`\"CBwindow[`46' + \"_` 8#\"] ? `!p)` 0<) :`47\"`#>&` 96info` WH` J\"` x'`#%~`#NUmobile_device`!2$isM` 2!.any() ? true`\"%&loaded`,p$`$Y%trigger`!f!(name, args`$q#`1#+`)B&.` 5!`)\\!fn =` <)[name];if (fn) {fn.apply(null` x#;}`.'/` t'` .(`.Y(`#!! =` M)`!)#for (var i = 0; i <` I).length; i++`\"8#`!v!` 8([i`!W=}}`#3%load`'z$`,:)thi`!p\"`'4#`\".*` .#` 9$info` 0-info`!E!!` [$|| !`*|$ {console.log(\"The`!!%o` j&`!H#is missing or corrupted.\"`&5$`$&\"`$g@`$%Gback_image_url, ` &!s_directory, di` \"%`0~!_specific, main_settings, normalizing_factor`'6&pre`#$` X* =`\"l$.` +*;` q)` <'` +)`\" !scripts = document.getElementsByTagName(\"` C\"\")`$n\"ysrc =` V$[` _#`&Y# - 1].src`\"x+`!D!`!6'.` 0'!= \"no\" ?` )6`*A#;`#j*` Z7` :!` W>` B!` u$`$U,` n-` 0-!= \"default` t.` =-`! $` u(` 4-?`%{- :`#n\".substring(0,` ,#lastIndexOf(\"/countrymap.js\") + 1) + \"map`\"I\"s/\"`)1\"`\"S+&&`$@') {`#V-`!C&+` ?';}`(w!ignore_pos, fly_in, rotat`2%!nual_zoom, responsive, div, initi` 6%` \"(_solo, tooltip_` a\", last_clicked` 4&up, regions`(Q&get_`\"T!nfo() {div`$`-` /!== undefined ?`#)!\" :` 8.;`!T(`%J.` 0)` e+-1` `-` D(` m)`\"U!` b9` ;#= \"yes\" &&`#-) != -1`2r+;`$2\"` f-` 1\"_fr`!d-||` .9`(q$` y!`\",-` A';`%+&`!--width` h!` ;&\"`!j,`&$\"` K-` 0#`)%,` 0#`**%f (` Y$= \"0\") {` h%` =\"}zooming_o`\"m.`$f#`\"=,true;`'O'` G-` /)`$@$`\"(,`'+\"`.|\"info.`+C#_` 2$&& `!Q'?` /5`\"A(`/f$` 5#) {` {)` 0(;}` E(labels) {` #\"` D'` +\";}`)>*`#7%`)G(` ,%`)P&` *%`*|&`#q&`0<$ground`3E(` %-bbox,`\"k!_time` $#mobil` %$incr`1w!, custom_shapes, popup_centered` ($orientation, order_number` i#percentage`,2&back, link_text` D\"`$<\", fade`!W#hide_eastern_`#<\", `#D\",`-i$`$`$`#Z#var adjacent_opacity` 0!op` \"&`\";%al` +!` W!_size` $'color` %'` U(new_tab` '!`!8%oc`\"a!` <)hook`!O\"b`\"v\"` |%`#G#` |%` *\"max`*+!` ('` c(` ,\"shadow` Y)rner`!\"\"` ,\"nocs` $(font`/H*refreshable`/U%`%r0`)F-` 01&&` \"@!`*\\%` *?`)I$`'01`!4>` A!` h=` A!`!\"$`#c#` `8transparent`,((0 : 1;`%}&` R-` 0'`!D,` 0': 22` _#`%T!` P3` 6\"` R2` 6\": \"#ffffff\";`'%#` [-url_` 3%`.(3`'D4`!G.` 1,`!M-` 1,: 1;`(9!` [-js_` 2#`!?3`(`'`$:.` 1'`%--` 1': 1.5;`):'` Y-` 0(` Z,` 0(`#`(`-B-` _3` 6(` f2` 6(: \"auto` w$`.Z$`!Z4` 7$`!^3` 7$` g+`$b4` 0*`!V3`$k%0.9` i#`,?\"` X3` 6#> -1` \\3` ;#: 1`$<%`,!`$.5` 8\"`$14` 8\": `%+$`-[!` U3` 5#`&F3`.z*` L3` 6%`!:2` 6%` g*font` Z3` 6!` W2` 6!: \"12px/1.5 Verdana, Arial, Helvetica, sans-serif\";`2Z'`!$-`4U!out_` :'ly`\"o!`/:\"`!r! :`\"v!;`3`,` ^-` 0-`!z,` 0-: 0.3;`!B!tim`*5.` 0&` [,` 0&: 0.5` ^\"`!~%` S2` 5&` Y1` 5&: 2` k\"mobil`!@3` 4$`\"w2fade`!~2` 0&`!6,` 0&* 1000 : 200`0D\"`'@\"pdata`/}\"s;custom_shape`'S.` 0*` {,` 0*: {};initial_back` ]-` 0)&&` \"8!`\"h%` *7`(*$hide_eastern_`\">'` M(` /1`)`3link_tex`%0.` 0&`1c-` 1%: \"View Website\";`0d\"numbe`0H.` 0)` e,` 0)`!V$`%s!percentag`&!3` 5'`&r1` 5'`.d!9;}func`0^!is_onclick(`+1!s) {if ` &#`#1!on_` <!\") {return`'+\"} else ` ?+detect\" && touch` ?2` ,$`\"@\"}`!B*ff`!26ff`!03` b,var vml;var tough` %!ie` ,!ios` #!`\"3$` (!`1l#ff =` _#var reload` &)`\"3!` I&s;`!u%get_client_info() {vml = Raphael.typ`*[\"VML`&Z-ie = document.all` 0-os = helper.isM`+K!.iOS()` ;,`!_!` :/any` ;.`#Y$`0U._up`3'1` 3!:`1#0s;`#M$`#+%`#M(`$r*;`$B!map_outer`#O!map_inn` $&div` 0%hold` ='zoom`#t&create_dom_structure() {` ^\"`#P(getEl`/5!ById(div);` u&` ,: + \"` B#\") ?` Y8` =*`$]%f (`!#&`!W%.removeChild` 4(`&M\"t_to_del`!A7\"tt_sm_\" + `\",!if (` O%) {` #%.parentNode`!()` C&;}}`\"Y2`#]\"`!/#(\"div\"`#4\"`$[!` %Azoom` $A`%@!` sF.id = `#n$` .!\"`!(%` /*zoom` 6\"` !` /*` .!` 8\"`\"Z\"` 1*`$h#` 4(style.posi`&b!= \"relative` y(` &<`!\"` 4.absolut` ?#`\"(!` Y.` ,5zIndex = \"1` u.` -,div.append`&T.`\"E'` 3,zoom` $9`!/!` %9`\"~!`*h#transform_rotat`.N\"widt`.N\"height` &!scal` 9\"original_` >&` *%` F'initial_view` ,!normalizing_facto`+V\"ratio`!.&_to` ^$`+\\-imensions(res` d!`*(&`#`\"` ^! = \"`%b/` 1'if (responsive) {` 4$` f#offsetW`\";!if (` 9\"< 1` 9.`*5'` J(`*)'`!0+` \"\"+ \"px\"`3H%` k&`/Y(` /#== undefined ? 800`/~-`$%\"`\"V1`!\"*` (** 1`-[$info.calibrate) {`$T( = {};` &(.x = -1 * ` K-.x_adjust` C*y` 76y` <1x2 =`%y).x +` K/`\"V\"` ~*2 = (` ^,-` _+) /` \\/`&j\"`$1$`\"W+` G$`'P)}`($*`!e-`!(.;`(B+` C,y` B-y;`(/+ =`):+ /`)6-` A%` 9$`(r,`)A.` c01000`%b!!`),'`*k!` p'`*g+`#A! =`&9#`+W\") {var bbox_array = [];for (`+-! in`#Y%state_` B&` T%`#{'` 4,[i];` $&.push(bb`-$#path` G! = helper.` G!union(` ~'`,'!center_x = 0.5 * (` W%.x2 +` e&.x) *`-T'` S#y` F0y` L*y` P&`.Y,`1o!\" + ` (#+ \",\" +`!N&` \"+y`.&\"iv`\"4&` T\"`!%!`'~),`/e-)`&f&`':$riv`(r#`&y.riv.`%z#}` a&`%I$\"s\" +`\"0\"`!\\%` $&0,0\"`\"C& =`\"8$?`!J'` F$`\"c-:` 5,;}`$t#pe`0g\"everything` *!all_lines` %%visible_`%v!` 1\"location_label` A&externa` J4` C'`!M%` @!`!T,`'2!ackground` >!` %&_col`2b#` *'imag` S\"all_pil`!L'`!o'all_region` &&`\"&$`!S#op` #+bottom` &+` T!`\"*\"`3e,canvas() {`#a! = Raphael(map_inner,`*_\", `%O\");`\" & =`$7\".set(` -(`\"R\"` 6%rect`/!+ -`+z,* 2,`-G+` 9(`&y#` A!` I-5` )'` @%5)`,9!`#i,_url`+h#` *\"`+/#` 7-` 3!?`$H-` 2\":`!^)`\"O(` @!`\"R%` )!`!/1,`!4'.x,` \"(y` .*`0J!` 35` /+y`#|(`-^#`!/+);}` (7`$V!` X)`'j%(`'n+` 9)hide();`';&`%O+`)N.` ./`'i#` '/`'J%` -+`'w)`&a,`'+` c0`(3!` *+`+%+`!n7` (5`+Q*` ./`*R!`!=0` =/`-.&` B+` /&`$a\"`#s&,`+!*,`'<'` 4#`!u!` &\"`!_*`3K#trial_`.S&map_` /! = false`+<-` L\"text() {if (!demo) {return;}if (`!L$.hostname.match(\"simplemaps.com\")) {demo`!&%` Y(`!D%`*##parent =`!\\&.` -\"Node;` %\".removeChild` V';`\"'.}` '(document.`\"@\"Element(\"div\"` R'.style.cssText = \"display:inline !important\"` E-posi`#E!= \"absolute\"`,X\"randed`\"@#h = 20`$*!w = 140;} else` 5&3` 5&200`\" '`!$#left =`/N\" - w + \"px`!A.top =`/m# - h` 04zIndex = \"1` 7\"`0P!.append`#[-`&*'`0}+` 5!, w, h`/1#`\"M)t`#O\"` U'.`&D!w - 5, h * 0.5, \"S`%w+;text.attr({'text-anchor':\"end\", 'font-size':14` ($w`\"T!':\"bold\", cursor:\"pointer` O%family':\"a`!{!sans-serif\", title:\"Built with `!W\"Maps\"})`$N)`!iS Trial`!vL8`!\\f});}`!7!node.setAttribute(\"href\", \"http://`*0,`!n\"click(`+B%() {window.`*&ref =` P4;}`,J#`#1!_back`'}!back_arrow` *!zoom_in` \"&out` #&` M%` )!ab` 0)in_`!a!` O)` ,#`-4,nav_buttons(`'4#navig`0j\"`3W!`,A!in_settings.` 0-? ` \";: \"#f7f7f7\"`!P!` 6'border` h>` ;)` v7` ;): \"#636363`!'-opacity` w8` ;$` s7` ;$: 0.8` ~!`$]!`!x3` 0(` e,` 0(:`#s-` v!legac`!`.` O'`\"W#` a8` ;$` w)`#)(`!{'`#9` 0/`!!2`$#+`\" \"`$'$w`#e8size === undefined ? 40 :` 5:`!p.`$u4` =$x == \"yes\" ? 1 : 0;`!U#` \"!* 1`)Y&`0|#` 5\"` 2!m` >&0.` D\"s` +$/ 1`2;\"image_w,` \"#h;`):#`!>!()`)H-` 1# {`*n& =`+.\".set()`3.%`!?%h` %$`1J!ack_`!&\"url`*4#img = new Image;img.onload = `,g)`!h# = img.`3y!;`!s#` -#`\"b\";mak`!{&}` m!src =`#'\"`!8%`0T%` B*`!+%` +( {`!l1`-f&`3|+outer`#Q$`#X&`\"|\"`#5&`#7*` d!.` F!` |+, 0, 0` \\0`%2*`$a$` r,`20#`1L,}` Y#` >\"push`!2\"` O'` 5)`1#\"` ,!` &!_handler`3~&`\"Q<w, `\"^#fill`'z'`&~\"`'r'`\"g(path(shared_paths.rounded_box)`\"6#fill:`+K', 'stroke-`%f!':1, ` +\"` ?#`*Y(` C&`!L#'` 7*` /#, 'fill` ;&0, `#B/`\"!&attrs = {` w?`!_$.5`!;/1,`#-!`\"2+`!?*`#G(`!E/`!L'vect`-{!`#H9`%;\"`#Y\"`\"$'`%G(`*Q+`%~,`$g%,`!,)`\"|\"t = \"S\" + s + \",\" ` \"$0,0 T0,0\"` e(transform(t);}if (!initial`!{!`,*).hide();}`&R%.style.left = m + \"px\";` 0,top` 3(if (manual_zoom) {`-D#zoom_buttons();}}`+-&` -1`-&\"`-E2` D\"`(A.zoom,` >!,`/D$2 + m`#8\"` Q!in`$F! = \"m 64,13.787 0,100.426 m -50.213,` \"\"2001 ` 6#,0\"` e!plus` _$`%3)plus`0h\"in` ,3` 5!`,o\"ox` -1`)_'`!k)`*?\"`\"S%`&Y\"` ]$`*')`3@'`)y<` @'`);1`(M-` Q'`(J6`#n*`(^%`!x+`#I%`\" $`!b+0`!>/0`*:#`!i6`!a*1, ` %#:1`!T1`!^#`)S+` /#`)Y\"`$!',`\"&+`)9F` j$`)S)`\"}%out`$)~`$+|out`$s5`(2&`$+~`$n;out`$z0out`%$'`$*#`%)#`\"0&`$jC\" + (`,)!`,%!` x%`%1)`,V$`.()`.y!height + m`,g#`.]%` E'`.u,`!Y``.v%mov`.w\"ing_dimensions(dire` =!`/!'last_destin`$@!.sm.` I..w / ` V%`/S%` 6Ch` L-x` 3Fx + (`!=E- w) / 2`!!!y` \\Fy` [Gh - h` |&r = w / (original_`(1! * scale);return {x:x, y:y, w:w, h:h, r:r};`3O&`!&$allowed`#dj` =$`&A\"` 3% < 1 ? true : false;if (initial`&$! != -1 &&`\"o2type == \"manual\" || ` S(_solo)`!}#` 1$`# \"= region_array[` J(]`\"\"4`\")!outside_` K#`$ !>` n+- 1`\"&!`\"N(&&` K,`!O%` a+is_` .\"`\"C-,`!Z7)`#6#` `)`\"[1 {` '!to(` R8`%w#`$2\"}}}`!}/w >`!C*-1`##5 - 1) {if (!`!*E-1]);}`!<*` '#true`'2+_about`'/)`!*!`'C6) {` i\";}var `#_' = {sm:{type:`&7$, zp:1}}`$s%_tween) {`$9,` A:`(DB = current_viewbox` D1bbox = {x:` ?+.x /`*e\", y` ,-y` 3&`'/!` 0-w` 7&`0t\"` 1-h` :$}`#9\"new`!q*`/n>`#E!!` K*`$*'`\"`@` O*`1t\"to(` P');}` 1!in_click = `%o%(`&S$`%v\"` A#crement);}`2a%` :=1 /`&`\"` R)api_object`!y!_in =` ?$` m\"` 2-`-a\"`!**`!A\"in.` )!`!_$` 6\"`4C'` 3'` R%` 8#in.touchend` F5` 3*` W'}`${!cattr, la` \"!r` (\"`)i#map, label_attributes, loc`#~!s, set_state` %\"` G!, ela;`#I%set` S'() {` W% = mapdata.` +%;`!L! = [];`!P!` %\"`!H&` 3$`!H*` ?#` G&ela` #\"var`!?!`+|$` G(`%*)`+'\"faul` E$ = {};` &*.color =`,S#` .+hover_` &9opacity`\"W!in_settings.`!Y#` 7$? ` \"9: 1`!&2` `;` 7*` n3` 7*: 0.6` },url`\")4descrip`-k#` #<_mobile` 24inactiv` '5zoomab` [!`0X!` 0+popup`\"T4` 8!s`\"M4` 7#: ` #\"`%6-ascade` d4` 8#_all == \"yes\" ?`!_! :`!q6_percentage`+'$` (&` A,x`\"]4y`\"v4x2` :5` 0&if `4J#s) {for (var`*m# in` ##` 0)i = 0; i <` 6$[` \"\"].`*z!s.length; i++`)%#` 4! =` :3[i`*I([` 0!]` E%;}}}`!3&d`!H*`*a![id] = O`.^\"create(`\"G*`1c\"`!7$id].url` T(`&2(`\"v\"`!+&prop`!,'[id]) {` b+[prop] != \"`!4#\"` t(` 8#`\"Y&` +%;}` T2`&#$` Q1`'|!` >7no` B2`\"4#}}}`-d%`#j!`-Q7` ;%` ,)id`-x+` ;$`-{'` .!`-|%`(N*`!0\"` 7!` A+image_`+o\"` @0` 6&`)~,` 0,`)-,` t(siz`).` U(` <!` f8` <!: \"auto\"` o1posi`-T#` M6` <%` p8` <%: \"center` |2x` n9x` g9x : \"0` b2`0r.` N(`0k.` 0*` ^6`%@8` 6(` d8` <\"`$p8`2>\"`%r<` <&` r8` <&` p>`&#=` <'` r>`&.>` B\"`&1A` <+` |>`&:D` H\"`&J:` <$` t>`&X8` <\"`&\\:` <$` h>`&j8` <\"`&j>` <(` p>`&|8` p>` 7'`,j2ourc`2#&` 5*descrip`%K7` 7'` M+`(M6url`!;,nactiv`'s.all` =\"s_` :&`2I# ?`29!`\"b4id` R9hidden` BG_label`\"r3bord`#p5` 0)`% ,` 0): \"#ffffff`&1,` @#`$t8` 0/` v3`%v>` <)`,*1` 0.` p9`,.#` x0empha` }#`$'!` 1+zoom_percentage = ` #+` =/abl`%!9` :&`${Apopup`'73` 7!s`)q3` 6#: ` #\"` h+opacit`,\"4` 6$` d2` 6$: 1`*_1` ^:` 6*` l2` 6*` v.`*P'_mobi`#_/`*s-` A$? ` \"5`#z$var region_id =` $$map[id] ?` 5$` )$` P$if (` N&&& rattr[` *%].cascade) {` G!` .-olor) {`\"@*`),$` 92;}` V1`(~'` `-`)}*` ?8` f3`#1'` l-`/+*` ?8` f3url` d-`/L\"` 70` N3`/+$` Y-`/e'` <5`#;4i`$U!`/X1` 81;}}c` 3!id] = Object.create(` Z))`%w!mapnam`*t\"us\" && (id` *!GU\" || ` '#PR` \"(VI` \"(MP` \"(AS\")) {`!>%`!h$`-(\"`\"C!`!#0`11!eastern`18\"s)`!C(VT` y(NJ` \"(DE` 0(DC` =)H`!^)A` \"(C` g)R`\"'*D`!t/`2n%`\"&#for (var prop in`*B#specific[id]`)2#` '.[prop] != \"`$2#\"`!$(` 8#=` `/` 5\"`#E\"` _5`/d$` _1tru`&L#` C9no` I2`+v\"}` \\!`-s6`*G#= \"off` \\)`*`+` .&`*[#};`#P%id in mapinfo.paths) {se`'?#(id);}}`-{!set`$:\"_attributes = func`*v!() {var `'$`$d${}`/c%` .!.font_famil`0`.` {\"font`0X-` 0': \"arial,sans-serif\"` v+`\"U$` P0` 6\"` j2` 6\": \"white` f,`#K*` L0` 6(` l2` 6(:`# *`$:#` &*size = ` W\"size`!D,`*Z\"` }*`(d&s`'\"% ?`&t!`24%` Y*line`&S%`!3+ca`3}/`!J#` 7!`\"42` 6\"` y2` 7!_limit` i-` 0(` j,` 0(: 0.125` k+rotat`!W4` 6#`!Z2` 6#: 0`\"i/`$y9` 6'` e2` 6': \"#000000`&.,` @!`%(#` L5` ;!` j7` ;!: \"1` i1x`$l3` 8!y` &3parent_typ`1$!`*u!` n,` =#id` M3anch`#&5` 6#`\" 2` 6#: \"middl`!(.ill` ~3width`!#-pill_` 5\"`!\",` 0'`'!2`#(4`##4`4!\" \"Not Named`!n,displa` G4`#D'`.*-s`\"(-impor` 4&`0|\" ? {} :`/[%` V*` r!apply_`/(,`/O&id) {`/k,[id] = Object.create(` W))`0}&`4X$`!/*`4U'` '.`4J2`!20`3/%` M4`4%\"` j5`4`'` b9true` H?no` I=`$l\"}`3R#`#y\"mapdata`#g4if (!` a0`#dS}`$+-`$#-`#OY`#n2`#9_`#2^`\"Q%id`&b.) {`'|/(id`#'(` S\"` B+`$T)` K\"se`(V5` m4` S5;`%g\"` n!ocation`\"@'` t)) {`+!)` H# = {}`+W&` /#`3Ae06`3(` v$`23`!z$`2}3` 1*: \"#FF0067`-s'` y$hover`4%4` `$` 9(` t5` 9(`/m-`!'$borde` o7` 9#` l5` 9#: 1.`\"{/` 8\"`!n<` 9)` k;`#6)FFFF`#*5`!z<` 9)`#&;`\"=%2`&=/ize` l6siz`#c/descrip`'?#` D3` :'` E9_mobil`!)7` 9/? ` \"8`%U5url` m6url` @.inactiv`!L.all` =%s_` =&`-z# ?`-_!`!16typ`\"87typ` A/posi`#m#\"top`%h/puls` \\7` 8#`!<D` J!`%>!` T;` >\"`&L5` 9': 4` o5peed` h=` @!` j<` @!: 0.5`/ !` 0\"`)g;` :'`!J4` Z$` \"(&&` \")!= \"auto\" ?` +)`#K5image_sourc`#K7` 9)`\"E5` 9): \"`+H0id`',<` >!` t,` 0/: \"no\",`2'-.opacity =` *-_` 4#`2?3 =`&l!`-:4` n&`,p9` ?$`-5;` ?$`#{;`*v9` 9&`#u;url` i<`*2'` N9` ?%` t;` ?%: \"center`*|0opup`'y7opups`(t7` :\": ` #\"` n.x =`\">4y ` \"5displa`$n7` 9$`!@5` 9$: \"all`'OUden`-U5if (`/?3= undefined) {`/c4\"square\";}for (var id in`22%s) {lattr[id] = Object.create`!+-);` ^%prop` \\)[id]) {if (` 7!== \"overwrite`%i#` E$\"`!*(`'x)` b)[prop];continue;}` p)reg` Z-`$l&` 7$` O\"` d/`.\"!`\":#` X)` 8#`!62` U5`$[$` U1`+m!` >9no` D2`%B\"}` W!!`\">&`+R)`\"U)`,W,` 0&`-P$` V2`0^!` W/`1H$` .&`1y\"}}}`2S!set_line_attributes = func`*t!() {var`.|&ine = {}`(E&ine` n\"`):.in`24$`)5-` 1&: \"#cecece`)9'ine.siz`2</ine_` 5!` ^1` 5!: 1` \\*dash` T2` 5!` U1` 5!`2|!var lin`\"t!mapdata` >!`-/\"` $(:` '%borders`)&&`)p#ines) {ela`)S:ine`)Y0ine`)](` '%`'}2` #`('&` A*`'K#` U+`'~'` P,`'|'` B/`'}\"` D,`'y$}};set_`*0\"`&B'()` 3!stat`&X(` 0#label` $/`/<$` (.`'=*();}var currently_zooming`!Q%var max_width` )!` B&pann` <,` 3'inch` 3(`(L%`$g\"_tooltip`([$find_pos = helper.findPos(map_inner)` ~!x0_page =` J%[0]` 5!y` (.1` 4\"x0 = 0` >#` \"%h` ,%w` +%u`'J\"` P\"_mid` O\"_` \"$left = 5` (!tt, h;return {`\"2\":`*p)tt = documen`'?$Element(\"div\");tt.setA`$-$(\"id\", \"tt_sm_\" + div` @\"tyle.posi` z!= \"absolute\"` 5&`08'none\";`#5%.appendChild(tt)` /'onmousemove = this.pos;tt` \"4}, show`\"A'e`\"/\"`2{$opup_off) {`\"y\";}ignore`$|#`%M\"if (tt == null) {`%M#`*H$);}`\"%0block`\"I'zIndex = 2` *&maxWidt`,r\"`'9# + \"px` T!`\"S!HTML = `!t#.sm.content;`!@$updat`!o!`\"8%;}, reset_pos`\"V'x, y,` d$`\"`#`\"6\"undefined`\"+2` +#` k#(y0 + y, x0 + x` h&;}, `!E&`#e1` [,u, l` S*` K+, manual`!h#` $%u`#1!nual.u;l` #&l;} else` <\"ie ? ev`'T!lientY +`'b&`'m$`'g#.scrollTop : e.pageY` v!` U-X` D>Left` ^%X;}u = u -`*Y$` s!l -`+#$`&C!`&j% || `###_`\"2\" || `&s'` 5'up && on_click`'B'`#W0`#]!`%G.`#x*`#d\"!tt || !u || !l` n'`+o! =`%9\"0.5 * `.V\"`+~! = `%[!` 3\"height`\"9!l >`,I\" && u >`,L\") {quad = 4`$R$` G\"<` 093` ?*` m)<` =,2`%K%var ` .#1`0q#entered`({& &&`)$(`#R$ &&`$:$` J&`3c# || ` (/auto\" &&`\"y\" < 401) ?`4!! :`+V'` N$`.i!`*w#top = \"-100`*j#` 2\"`/W#` '.bottom`.O!uto` .'righ` N!` 2\"h = parseInt(tt.offsetH`$D!, 10);w` -1`,G!` ;\"var side =`\"4#- w > 0 ?`%0#(` .%) :`1p#bar`34!`!I!- h` C*` .&` L\"`\"Y+bar`-P'`\"])`!D!` ,,`\"E+`\"l5`%Q$`)4'rienta`1z\"= \"below\"`(.#`%v\"= 3`&3&1;}` .(4`&J)`&z'` h2above` n,1`'[)` .(2`(B)` .)1`%q(`%35`#H\"u + 5`#84l + ` &!` 00`#I+`\"5'`!W(` jZ` |$`%|$l`!R8`!/63`!41`&?%`!&2`)/#`!_+`\"h.3`\"SC` Zd`\"?Q}}, hide`/_'`&C#tt != undefin`+j*display = \"none\";}find`11!= helper.findPos(map_inner)`,b!` A$) {`20# = ` -$[0];`2V#` *(1];}}};}`!Z%getxy(lat, lng`!i#mapinfo.proj`(S!lambert\")`/N\"` 2\" ` /#`%B(` I-xy\") {alert(\"This map only supports x/y loc`)b!s.  These can be added to the mapdata.js file.\")`! 9robinson_pacific`!q+` /,` O9mercator` U+` /$`2G)` q+`2U\"initial = {lat:`#k$:lng};`$'%interse` (!(x0, y0, r0, x1, y1, r1`!:#a, dx, dy, d, h, rx, ry`/x!x2, y2` &!dx = x1 - x`03\"dy = y1 - y` *# = Math.sqrt(dy * dy + dx * dx`1:\"a = (r0 * r0 - r1 * r1 + d * d) / (2` %!`!9# = x0` ]$a / d` 4!y2 = y` 4!y` ,)h`!8)` &a * a` o\"rx = -` S\"(h /`!%$ry =`!\"\"` *(xi = x2 + rx` *#_prime` 1\"-` /$yi = y` C!`#6\"y` >&y` C!y;return {opt1:{x:xi, y:yi}, opt2` -\"` P\"` 3\"` &\"`(|)`(F#(lat`)#\"`!v!adian = 0.017453293`!@!pi`\"h$PI` +\"hi`)-!tlng.lat *` V#` 9!lam` 3'ng` 0*phi0 = 45` D-0 = 9`#g!` A)1 = 33` &-2` R/n`!`$log(` $!cos(phi1) * (1 /` 9\"` /#2)))` +$` I%tan(0.25 * pi + 0.` &!hi2` V*` /51))`%/\"F`!F$`!9(` ,!pow` n;1), n) / `\"3\"rho = F` T(`!'>` ^!` W$0` .N0` [\"`&H$x:rho` Z$sin(n * (lam -`$Y!)), y:`!&!-`!l!` D#cos` 9.`&a(`,2$`&`*earthRadius = 1`\"\"\"`&j4roundToNearest = `2L&` 4#, value) {`\")#`!W!floor(` 5! /` ^$) *` #$;}` z!getSign` k)` `+` ^\"< 0 ? -1 : 1` U#lng` U#` _#`\"@#.lng`$8\"la` v$` 2,at` ;#ng`&$$abs` M0` +0` U%ow`/{!`\"w((5,` R!- 1e-10);` ?\"` e!=`\" !0 : low`-J\"igh =` d!+ 5` m$Index` 0#/` 0#high` 0$` Q!` 1$ratio = (`!0\"low)` 3%AA = [0.8487, 0.84751182` &\"479598` &\"0213` %!3359314` '!257851` &!1475` Q\"0006949, 0.7821619` 3!7606049` T!7365867` l!7086645, 0.67777`!7#6447573` b!609875` 2\"5713448` b!5272973`!<!485626`!R\"45167814]`\"?!BB`\"?!, 0.0838426, 0.16768`!l\"251527`\"C!335370` _\"19`\"L#503055` Q!58689`!Z#718226`\"*\"533663`\"z#51804` j!915371`#U\"99339958, 1.06872269, 1.14066505, 1.2084152` ?!27035062, 1.31998003` '!523`\"3\"adj`$[!(AA[`%5%] - AA[`%]$]`(n!`%=!+` +)` Z$`\"y!(BB` T*BB` L2` ,(`,4'`!D\"*`(I!`0/$` )\"`(z!*`+G(, y:`!2\"*`)7%` 4)`,-0_pacific`,;*`)W\"`2D'- 150;if (lng < -180) {` A#ng + 360;}`!s#`-6%{lat:`)s&, lng:lng})`!A'mercator`!6*y`2H-tan(`*_' / 90 + `2c\"`4e# / 4))`21\"80`0($PI`/h(`\".&, y:y};}var calibrate = mapinfo.proj_coordinates;`!k%find_point(initial, pt1, pt2, pt3`!#` ]!` ;# =` *!` I$`,d\"pt1_proj` 5$pt1` 1$2` *+2` 1$3` *+3` 2#roj_r_pt1 = helper.distance(`!=(`!f!` _!` J+2` 6?`!P\"` S\"dist`!D$` G-` |#` ?1act` =2` I!` C\"scale =` z'/` T%`.Z\"`\"D#`\"M'/` P\"` 6%2` 3(2` 2)opts = interse`$q!(pt1.x`\"y!.y,` z\"`!L!` /\"2` -$`!Z#`!H!third`\"t?`$N\")`!T*emnan`!!`2A%` X,opts.o`!B#3) -`!,'`!A\"` _#2` B@`&t$` X*if (`!C%<` h%) {`(K&`!?%.x, y` $'y};} else` :0`#5!` D&2` I!`)*!rules`({,` 0!`!I\"ules) {for (var i in` O\"`(s#ru`%Y!` .![i`.Q\"condition_stri`,8!rule.` /%;try` Y\"` *% = eval`!-!` ?&);} catch (e) {console`,'!\"The` T'\" +`!&.+ \" is not valid JavaScript\");}if (`!!&`+##oin`&m!`!D!` (\"`,M$`+Z0`,G%[` F\"[0]]` #/1]` \"02]]);}}`/($` b:0` U)1` c)2])`-y\"tt_css_set = false`-c&set_` :\"() {if (` D&`&<%`0&'newStyle(str`\"v$a = document.getElementsByTagName(\"head\")[0`%F\"el` F(create` N#(\"style\");el.type = \"text/css\";el.media = \"screen\"`&o!el.` S!She`!s!` #).cssText = str`(*%el.appendChild(`!A+TextNod`\"C\");}pa` A)el`1n%el`\"u'getsupportedprop(proparray`(4$oot`\"R(`!/$`\"Y#;`(k'= 0; i < ` [%.length; i++`$H#` 3%[i]`)C!oot`\"V\"`!*#answer`.G\"` A%;` -%` #\".replace(\"borderRadius\", ` )#-r` +\")` ?6MozB` M+-moz-` 8EWebkit` S-w` 2!` =FboxShadow`!x\"x-s` (\"`![<` K'`!q#` 7@`!m$` R(`!i%` V'`%e#` Y\";}`.B#ound`\"3\"prop =`%m.[`$ -`#U/`#'/]`1:#cs`,S!` z+?`!*-+ \":`-k!popup_corners + \"px;\" : \"\"` l!min = width / 2 > 250 ?` '': 250;max_` .\"=` l#max` +\"?` }#` (%: min` y!`#)\"`\"P8`#]'`$F,`#{,`\"\\#s`\"]\"` p'?` |(`\"P%3 *`!R#` 8\"`\"V\"`\"o!3 ` \"54` %2rgba(0,0,0,.5)`#9$`)k!` A(< 0.01) {`!^#\"\"`/,\"m` (#.tt_mobile_sm{margin-top: 5px;} .tt_sm{\" +`%$\"+`\"H\"+ \"z-index: 1000000; backg`%2!-color`%!*lor + \"; padding: 7px; opacity:` A&` +#` G\"font` \\(font` 4\"` |#black`!b#nam`\"\"!float: left` \\\"-weight: bold` F\"custom_sm{overflow: hidden;}\";`\"z!+= \".btn_simplemaps{`!))text-decoration: none;`\"]&: #ffffff;display: inline-block;`\"a%5px`#k!`#z\": 0;`'i\"`#X!%; `*D)iz` O!`,H#box; `+E&` */` =4`!D!h`\"r#1.43`\"*\"align: center;white-space: nowrap;vertical` C$middle;-ms-touch-a`1e!: manipul`\"x!;to` \"5cursor: poi`!0!`\"G$user-select`#U#`\"D!` #0s` #/` 6.`\"Y\": 1px solid` +#`/n#: 4`'p!   `%)+:hover{  `%\"-underline;}`,Z\"xml_`&\\! = vml ? \"left`)\\!right`&2(xmark`'('\" +` V'`'c!`)M#left`)O\" `\",`$\\*0px;}\";newStyle(mcss);tt_css_set = true;}fun`$(! get_zooming_dimensions(element) {if ` &$.sm.` :.) {`18#` *9`+u\"bbox = helper.rotate_bbox` x(bbox, transform`.C\"gotoX =` ]!.x` *%Y` +$y` *%W` +$`(b!` .%H` /$`#'\"`1{\"atio` %!zp =`!t)p` 2!paperW`1##original`14#* scale` >&H` t!` =(`!%\"` A%`!|$` \"\"- (`!i\"/ zp -`!v\") * 0.5` F!Y` D#Y` C$H` ?(H` ?(W` D#` g\"` .!H` ,#` O\"`09!` :$` 1\">`+b\"_to`!a#) {`\"l!` a'`\"V&`!K#-= (`\"A(*`#>\"`!J&/ 2`!2)W /` x,;} else`! *H`!&$` s\"`#%#`!&%`#~\"`! *W`!$&`\"X$H *` |.`&x#{x:` p!, y` $!Y, w` $!W, h` $!H, r:` }!}`(8'reset_s`'&!attributes(region`(;#!` %%`!'\";}all` N\"s.stop();for (var i = 0; i < ` Q\".sm.` E#length; ++i) {var ` 4! =` \"\"_array[` G,[i]];` &!.attr(` %\"sm` )!`!j\");highlight_labels` @\", \"`\"<!\", false, \"` 3!\");}`\"T,las`\"b#(`\"Q#` -!destin`-O! && ` $,.sm.type ==` t$` /4`!d'`#Q#` 20ignore_`.r!) {` 2-`\"^!` `;;}`\"^-` B,, \"out`\"T0`#y\"`%\"G`$H$`%C\"` '#`!b!`$`&`!Q(`$k|`%\"E` X\"`\"T,`\"Y!or`(-#by_`(O!() {all`\"w#s.forEach(` P%`\"x*`!O&id == -1`#&'` 3*`0\\..r >`*f\"` P!`)E8`+d&`$>:;}})`*E-`\"&&` F(`!nW!`\"@%`!!Hshow_point(point, display, `'&'`%?#`)&\"` -(`):$`0 !` S#`)I!all\"`4V&true`#&$` :,out`)q!`* %`(6!` 6A`%}#` Q)`&0$`\"|\"Raphael.isPointInsideBBox(`!v+bbox, `\"^!.x,` \"#y)`!8,`!23`+r&`, +`!@#in`,v#`#\\$`#P'` l3`#k\"hreshold = helper.to_float`!8$)`#v!` ?&&&`2##<` T&` r-` '#`)G!;`%C%`!\\&t,`*D\"`%1#x = pt.x;var y` '\"y` (!potential = []` .!`#{\" =`*&$`+'\"`0O%` 0\"]`!q!`'D%`+{7item`,(&i++`!M#curren`)V#`!%%` I\"[i` z\"`%$6` Q)`%6&x, `%/!`\".%.push` <.id);}`$A%` ?+` 8*var `!x\" =`#%&`\")$`2E!` 8\"< `,=&`$3#`,H!` U$=` ;!if (` ]%[0] =`.n#`!*#`%r7pa`!?!mapinfo.paths`$.&id`\"y7Path(path`\"y&`%v7`/G'animate_transform(e, t, `0p$a = {` 4%:t}`!?!!vml && !touch` %!i) {e.` h#(a, zoom_time * 1000`.d&e`2Z\"a`0n)`1I!_corre` ,!`*T(, initial`.W#`1t\".hide(`3%)i` `$`'O!`!|#lbl =` ,(`&V$lbl.sm` b!) {continu`$n#`.L'` @#` (!0, ` (#`.Q1`!(&_set`!-%`0j!`!5!` V#id];` >#.show()`&$#` 9\"lin`*2$line_`%R#get_` '%(lbl);` F'`#O\"{path:` =%, `$Q'` \"$}`! *scal`!'$factor =`,<#>`\"F$` ?!_limit ?` 6#:` */`+\\!t = ` .\"`#8!,` n$*` 0\");`&C.` B!t`$~&`$4(pill`!X#pill =` \"!`#?.` d.pil` h+}}`&G(oc`$R!`&.D` H#`&P%` '*forEach(` v%(lct`*j#lct`&?'`)F\"`&6.` ?\"`&?%` )\"`&04` :!`%o(` I\"`$O:` 9'`$[.` ,-`$[.ct`$HAct`#f,);`#m&hide_and_`\"W!before`#`4`!=!`4M!`\"T(.sm.type;back_arrow`$,$`$KE;`+KB;`$f&update_`2\"\"s(`$t#helper.x_i`3C#(type, [\"`/5!\", \"` L\"\", \"out\"]`.w\"`*o!ll` h#_attributes`$i$`\"Q\"=` V%` M%` s!` E(`%N(`.=$` U)`!D\"` G6`!<$`(m!`#Y+` 6\"]` c2manual` t\"` ^!or` w#by_`&4!(`%9!(`#5/opacity`#@$` o!!=`\"~\" &&`%1\"!` x)all` y\"s.stop`)<#pill` $)` <#`-J\"'fill-`!&#':adjacent`!7$}` X(` *D`\"v)`!2\"` '(` R11` H,m.`2 #`+&/abel`+3$abel.sm &&`0h\"`-H'` #)`!B$` '*`!35}`!A+`4R${'stroke-width':`!e+border_hover_size * (` F! / original_` ,!) * normalizing_`+0%1.25}, zoom_time * 1000`&:%`$|(`!g5`$7;`\"<#`%C+`\"-3`\".#`!>m`,|3after`)i)`%%\"`+m#_` e!solo &&`,!$` 0!`(*!-1`(:!`-</`*+` g(back) {`-e'`0T$`#l#`1L$`*;'` j4`+H\" ||`!$< ||`!v%`!!<if (`*C\"`\"D\"`\"+3`*y$` U2`#g&zd_to_tween(bb`3k% {x:bb.x, y:bb.y, w:bb.w, h:bb.h};}var end_`!3'`2c!`$y\"wee` )\"current_viewbox;`!0&` C!o`0Y1, call`\"d#`*7\"st_`&o#d`*,!` %'`*'&` *' = false`0>\"`!?#ly_over && !`!-( ==`!a$` <#)) {out.call` O+);}`!%!click` ~'`\"^+`3|*;tooltip`3w$` (#_up` W%` |&zooming = true`->,` 7#_dimensions = get` O$` .'`2D*var to =`%()` \\=` R\"from` K+`\"`!` <?`2:!`\"`*` 52.r;`*R*before`%M1)`%&`3!\"Zoom`$A$`,a\") {`&N+ `$}%` <\";paper.setViewBox` S*.x,` B*.y,` \"+w` 2,h,`$h\"`,i(whenDone() {`,b;`\"5'`#O,`&!+`%Z0`&\"\"on`&u\"` (%`\"{\"`-h\"level();trigg`1[!ok(\"`$$$complete\", []);if (helper.isF`\"(#(`)R%) {` $$`+P!if (!vml && (!touch ||`*C\"mobile)`)5!`\";$ {`&$!able = new T` ($;`+<& = ` >%.`&T\"{from:from, to:to, du`&B!n:`0n,, easing:\"easeOutQuad\", step:`$:%`%i-`%|5;}, finish` S') {`$|%to`3L!`0H%`&R.to`&G.to.x, to.y, to.w` '!`&#&`%y&`/1(create_bbox`!X\"(auto) {var pri`!p!r`%V\"\"\"`*#!` B!` L\"array = {};for (` 6% in mapinfo.paths` j$ath_to_add =` 4*[` T!];` 5*Raphael._pathToAbsolute(` ;'`+X\"bt` C'pathBBox` 5.w = bt.x2 -` #!` 0!r`'\"!w < 10) {r = 10`4E& = 1`1g\"x = Math.round(bt.x * r) / r` e!y` 0-y` 2+2` 0.2` 5*x` 1.x` 8'`$()+= \"'\" +`#o#+` '#\":{x: \" + x + \",y:\" + y` '!x2` (!x2` 4\"` (\"y` )!},\";`$n,`$0#`#2!;}`%C+` #(.sub` $\"(0,` ,*length - 1)`!m.}\"`#y!!`&Q#console.log(\"The`*0!`&B-is: \\n\\n{\" +` ));}return` C-`.#'`'n#content(element`&|#` /# = ` 1#.sm.descrip`-r!var embedded_im`(5!data:image/svg+xml,%3Csvg%20`+r\"-backg`$m!%3D%22new%200%200%20256%20256%22%20height` C\"256px` 1\"i` U#Layer_1` /\"version` 4\"1.` .$`*p\"` 0\"` o3width` t-xml%3Aspace` 6\"preserve` 6%ns` 3\"http%3A%2F%2Fwww.w3.org%2F2000%2F`\"\\!` J'Axlink` =<1999%2F` F\"22%3E%3Cpath%20`\"Z#M137.051%2C128l75.475-7` \"!c2.5-2.5%2C2.5-6.5` C!0-9.051s` +\"-2.5` +\"%2C0L128%2C118.949L52.525%2C43.475%`#;!c` N!` i%` S*s`!*#`!$%%2C` 8!L` m#`!b#`!\\#%2C`!e#`!^'` K3`!7#1.`!M!1.` \"!2.88`!m!.8` i!4`!j#` +!s3.275-0.6`!\".525-` 5!`\"@$`#7\"`#0#`!C&` YG`!P\"` p6`#g:L`$R)z`%_!F`%\"\"`%o#3E`1'\"xmark_modern = \"<img id=\\\"xpic_sm`-*!_\" + div + \"\\\"src=\\\"\" +`)Y*` 7! style=\\\"`'w!: 18px;\\\" alt=\\\"Close\\\" border=\\\"0\\\" />`!?(vml`!C!a` a%line-`)k\": 1.5\\\"`!A<>X</a` j( = vml ?` y':`\"N)` F!url`,(*url ?`,;(url :`4@$url_sub = url` -!js_` ^\"` 4#`/%*11) == \"java`-(\":\" ? true : false` a!tab_click = \"(`.-$(){window.open(`#z\"url`\"L\",\\\"_blank\\\")})()`!\\\"reg` a%`!U#?` ^1location.href`$u#` s%` k! :` D1top` :?`\"r'_clean = helper.replaceAll(`#.#, \"'\", \"\\\"\")` Q$`\"Z1\" +` g*+ `!$'upon` O%new_tab ?`#R':`\"u&`2|!` e\") {` M)`!5$;}var mobile_par`1f6_` ?\"`%k*` -/: \"<div class=\\\"tt` 3#_sm\\\"><a` 1%btn_simplemaps\\\" onClick='`#f!`!e&+ \"'>\" + link_text + \"</a></div>\"`\"D!!`!M'` S$) {`(A$\"\";`\"9*\"\";}if `4_$`( $== \"\" && ` d(`\"6.) {` ].var content` /$` (#` p#? (` 1+\"\")`\"0custom`#.!; /`\"X!` b$`\"V!`\"Q\"return` L.`#d\"div>` *,nam`$\"#`-#!`\"8&name` q&` J)`+K\"` L%`#K\"` =*`,l$clear: both;\\\"`$8#`!g#`!y&`\"V\"+`&J)` f&`\"5#}`(+$ is_forgery() {if (map`!k!!= \"continent\") {`\"j#`+o\"`$*!i = 0;for (` *!d in mapinfo.paths) {i++`%F\"i > 8` ^&true;} else` l,`!R)inside(small_`#N#, big` %$) {var ` <! =` \"\"` 4$.sm.zooming_dimensions`'N!` C!.w >` `(` :2.w`\"J1bb =` M,bbox`+G!big = {x:bb.x * scale, y:bb.y` &&x2` <!2` 7'` -!y` )%};`\",\"xbar`\"7$.x +` ##w / 2` <#y` 7(y` :%h` >!`\"K%` d\"`\"R!.x &&`#+#` X!` 0\"y`%D#` D'<` I\"2` A+` 1\"y2`$g,}`# *`$g%create_pattern(e, par`$Z#hovering = par.` *! ? \"_` %!`0i!`/!\"` W#_id =`4Y$` k$_`)6!.sm.id +` k%` S!exist` {\"docu`$N!getE`$X\"ById(` q&)`#,!` K$) {`16#delete`%+$` 7&`%M\"svg = map_inner.firstChild`!=!SVG_NS = svg`*q!spaceURI` 9!defs` 5#querySelector(\"defs`23#`![#`!x(`#O\"`\" #NS(`! \", \"` E#` S#`#*!`\"t#;` 4#.` 0!`\"L&` /%setAttribute(` a$Units\", \"objectBoundingBox` {$mage`!6A` E!` O#rect` 0Arect` M#bg_color`%f#` j!` +#?`%z!` &(:` -!` '!;rect`\"/+fill\", \"#ffffff\")` 10opacity\", \"0\");` z!` 6)NS(\"http://www.w3.org/1999/xlink\", \"` $!:href\"`'u!`!_#url)`#q%append`%o!(rect` $2` T!);defs` +)` E#);svg` ,)defs`$:'_posi`)N!`#5(` -$` C!auto` 1)size`3g!auto\" ?`*G! :`*>#`$d\"peat =` s-= \"` 6\"` B1manual` A2` 6\"` B1center` A2` 6\"` B1fi`0C!`!e#||`!7$||` j$?` H\" :`,h\"`/f\"ox = Raphael`28!BBox(`2A)[id]`&x#box_width =` (!.x2 -` ##` 8&height` ;$y` :%y` Z'2` [$` i#/` O(;`!I$_preload(`&)), `.Q%(`.?#` 6\"`!S$this.offsetWidth`%V'`!S%` 9'H`!/\"var `!R\"` b(/` L);`!1%get_per`!9$p`$N!`&(*`.G!auto`12#`$,,` 0#w2h > 1` '#`!0(>`\"w') {` ~\"1;} else` ($`!_*` G&;}` @$` m&`\"W#` q#` (\"` o&` V%2h` o1` W#`$>)` G)`!*$if (`&O\"` t%w2`!T&2h`\"u!`\"R\"` +$` A%per`!/+1 /` 0\"`\"%)per`#0#`#h0 * normalizing_factor`\"p,`4h#` w!`$[&`$q%`%M!new_`&0*`'+'*` W!` 9*`&9%` J,/ w2`&e\"`1R$x = 0` %)y ` \",` W!,` &%`&`#`'G&` Z&` *\"` \\\"`&E&) {` ^)`#[#` b*` /\" *`)S&`!g\"`$e'`'*%` ^,1` Z.1`0c\"`!W!`$-&x` w%`#z!`!e&` 9&y` ;$`\"@#`%{1` rJ0.5 * (`$J'-`$*,`2Q#`#5\"` D'` i#` D(`(L#;}`39/x\", 0`3Q1y\"` \"4`!;!\",`*<(` 30`!1\"` @$`!=$` 6/fill\", bg_color`3\\&`!?.`&J%` -4x` :'x` -Y`\">$`$N)` 54`\"C%`$i*`$>#`!Q/`%%#` +2y` 8%y` +2`!T$`%B.f (rotate`/\\#cx`-A%x +`*.-* 0.5`4\\\"y` @%y` <)`&*#` D\"`!90transform\", \"`!7\"(-\" + ` &\" + \",\" + cx ` \"%y + \")\");}` ^0`#E%`'1/);`-:#\"url(\\\"#\" +`#n$.id + \"\\\")\"`-V\"state_`&[!array = false`\"M!make_` =!`&|#storage,` P#` M!`32&creat` J#s(refresh`2Y#!` %&` ^( = {};` c'` +\"}`!D/mapinfo.` +,`!e!scaled_border`0D\"= ` \"(*` =\"`0J2* 1.25;`\"E& = `\"-%(id`%c#brand_new =`\"U([id] ?`#1\" : tru`#8\"` b$` M&? paper.path(`\"($paths[id]) :` g,` c!attrs = cattr` 1!if (` m%) {` Q!.sm = {id:id};}`#p!vml` 6%nod`%v,class\", \"sm`\"R\"_\" + id)`%V\"a` B$s = {fill:`!G!.`, !, opacity` .#` '#, stroke` /#`#}#` K#cursor:\"pointer\", '` H\"-` W#':1` *&`)<!':`$j.` 9&linejoin':\"round\"}`#.!` E#hov`!4$ =`#B\"`!I$` /(?`#]\"` '0: main_setting`\"!*` x.`&Q#` [/` 5!` s1` 5!:`' (`'>/` r)` \".`'9@var ` M!`$h*`$V*` n\"`$a%`%('`\"j'`$y%`\"~.`$=;`!i&`*M#.sm.`,Y!`+n%if (`!,\"`,p\"url && `'C#var`.[#paramete`(5!{` t!:` ]!`00$ur`!s$` '%` 3$siz`&w$` '&` 5$position` 4)` -$`1M%` 1)x`1C%` *)y` /$`\"v!` .)` -!`'%\"`2g$url =`-{$` .#(`.U#`\"C,)`#,.`+q!`$q&.fill =` r(`#J-`$(\"ur`#7>tru`#?5` ]%`#E5`%,&`#G9` A\"`#L:` >\"`#Y3` 7\"`#[7`&~'`#@X`(W+`#W0} else {` '@`/%!`!`#nactive) {` J'`-o\" = \"default\"`/X\"`'5)ource`0 '.ignor`\"M#`%F6\"url(\" + directory +`,?#` m( + \")`!0#`!1#`-Z/||`,i4) &&` 5#empha` .\"`!c&` -$abl`'D%`#<$` -4`+*\"}` 6%`-FA`(R#attr`\"#!`\"h\"`(h$transform(t` \"$`) '`-l)` #&` 7&`.-.` #+` A&description`0H%` )'`!*'djacent`/(+'fill`3+&` =%` +#`.4'hid`1J&hid`\"[$` 4#_label` 4)` -\"`+*!brand_new`$A(reg`!q\"`#r,nam`!!&` (!`2J$` (!: mapinfo` *!s[id]`! !!` T)` ~(` j#id` z'`)f\"` q\"url`-?'`(V#` 5%` )$`$6'n_click = is_on` (!`'I#popup`%#'` +!_off` F#ff` 23`#)!s = []` +&zp`!>%zoom_percentag`!C'zoom`'E#` @&abl` 8'`%H'_mobi` B'` ).` M&type = \"` ,!\"`$}0`';$`%%)` ?%content`-J&` *#`-N\")`-w!sba = ` ,!_bbox_array`$j'ba) {` @\"Raphael.pathBBox(`%A$path`%D!);}var bbox = {x:sba.x, x2` $\"2, y` %!y, y` .\"y2`'['` R#bbox` '*.width` 4#.x2 -` ##` 8+height` @$y` ?%y`\")!`#5)`&y%hide()`,@%`(A,all_visible_` L!s.push`#=$`/s\"`(q-`#>&`#W$;all` K2;for (`4W!d in`(w%`#R!) {mak`!4#(id);}` &&[-1]` l(`\"#$func`,0!style_background() {` $&`/k\"`.!\"{fill:main_settings.` :,, `,B+` \"#, stroke:\"none\"}`%@#`+\\\"`\"d\", last_destination, ` \"'`&g!initial_`)&!`\"-!`,5%` .-manual;`\"@%`'[#`!-\"s(refresh)`$i\"!` %&`!H( = {}`2h\"` Q#) {`#}+` /&var`)&\" = rattr`(@!`\"='object =` L$` 1+` 4!`!B! ?`\"n)`%`!: paper.set()`)5\"`!e,.sm`!l\"` &%.`%1\"`,O\"`!}&` m&) {console.log(\"Duplicate R`!^\"\");continue`$N\"all_bb` l\"`\"a& = 0; i <`\">*`!9#.length; i++`#\"#`'~\"id`\"e%` D*[i`\"t\"`$}$`(G(` S$`0j(`!y+` u%+ \" does not exist`\"$)`*G)`!B\"` V0`1[%+ \" already assigned to a`!~#` p)`37.id`$*-`*?'_id`$r#(vml &&`\"P\"`2O!gnore_hover && `1\\#`)/! ||`/[$over`)C\"))`%L&`+?)`$Y\"` )'`-R$)`\"h\"` d\"x &&` n#y ` \"%x2` ,'2`-3#`%F\"{x:` =#, y` $#y`/R!` S$`/K!` T$}];}`\"`&`/N#helper` )!_union(` z\"`+&#attribu`'I\"{`+Y+` s\"`+e%cursor:\"pointer\"}`&(!`\"z!` D?`#B\"` .#}`$.!`#e') {` Z&.fill`3[%` ;!`#<(`$&( {`!1+` N*` @'` V(inactive`!(*`\":\" = \"default\"`#D(`\"6)` #&`&Q'`'H!`)*,name` 9'descrip`-x!`!]$` )'` 42_mobile` =0` 4#` M'ur`\"U&url` /'labels =`-')` 5&on_click = is_on` (!`#*#popup` A(content =`/{$` *#`-E#` k)`%P-` #+` A'adjacent`%r;` >$`&%%` X&zoomab`#\"'` )$` 9'`\")!_off`\"D#ff`\"/4zp` [)_percentag` c(`&&$` B%` )$` 9'type = \"`,k#` R(d`,^\"`1.+all`3O$`+R\"`#K*`1y'`&J$`'!(zooming_dimensions = get`4a!` +*` p%}`(<!`4>3[-1]`2a\"`*O!u`3g&` 6&;out`3%%` &\"`\"O%out\"` ,$`#P!1` d!`,7*clone(initial_view);bbox.width =` J!.x2 -` ##` 8\"height` 7$y` 6%y`!)$` #bbox`!;%`\"VGout);last_destina`)w#out`$Q!typeof `!p$zoom === \"`*]\"\") {` 0(_manual`#!\"` &/`#<#type:\"` -\"\", zp:1,`!~!:` H(` J4`!tI` P/)` k) = -1` |*solo = false;} else if ` _) != -1 && !` **in`%p))) {` K-in`4D\"` <#`#5+` 6! =` 7([` 5(]`!L>{console.log(\"The`$T*is not the id of a`!m# or`!1\"\");}`\"t.`(a!fly_in`(S-2`(Y'`%~*` 8,;` 4'`%76` d\"ivd`)~<`\"n0)` X!w = ivd.w` )!h` '#h` )!xinc` 7$ *`\".$ - 1) * 0.5` A!y` <&h` 02`!~*`&e2{x:ivd.x -`!2!, y` +!y -` |!, w:w * ` w\", h:h` #'r:` $\"};}}}func`#Q!`1S#lines() {var ` (! = mapdata.` *\"? ` \"*:` '%borders`.x\"` 8!`$p!turn;}for (i in` q\"` x' =` -\"[i]`\"!`03! = ela` +$b`4 %path(line` %!);b.transform(t` \"$);b.attr({stroke:`1%\"color, fill:\"none\", cursor:\"pointer\", '` N\"-dash`%E!':[` Z\"dash]` 7&`.D!'` u#size` 1&linejoin':\"round` l'miterlimit':4});b`/}%b`.S(.getBBox(true);all_external`$?\"`2C\"b);}` )/hide();`$u&get_label_bbox(e`#x#bb = e` }+if (vml` :%2` ?!_` 7*bb`0_(2` &#;}`(&!a = 0.5 * bb`1J\"`'w\"` +)` K#var pt` ~!sm.`$\"!0` 0!new_`![!{x:pt`'g\"a, y:pt`'d\"a, x2` 5\"+` 5\"` )!y +` 7!`!3!:`!9$, `!*\":`!1%};`&u\"`!\"#`!u\"`\"~\"`%2!`!@!` *\"set` )'make`#G\"`!w\"il` J$`(]-abel`(j!`4L%ear_sets([all` :#, ` '!ine` $#pills]);`!C'`%]\"`!\"&` *\"`!P+` )(attribut`)}!` #,.reverse();`\"$& = `!x%(id`%9#`)F$` S,[id`)V\"force_scale`0M%` T!lready_rotated` 3%`*x\"`!A,hasOwnProperty(id)`+1'`&c!rand_new`!B&`/!\"d] ?` u\" : tru`!8\"`\"q%`+1%set(`/A\"`&?\"`&4\"`)x\"x * 1, y` (#y * `0I#` G!`1?&pare` +!`\">&resize_` ++if (` g\"` 3\"_type == \"`2h# {` I%`4,(` G)id]`3y$` V6`1o\"` d)`1{)` @Nloc`1;!` g)` -$` ]5`$s!`#G$&& ` %#y &&`#0#) {if (` '\".sm.` }1`%f.`$p!`$$!.x`$j!` X$x` /#y` ))y` 0\"0` ()`+R#`&*` t!`!E*auto_size`&Z!`$l*` J!}}`\"E!`#$2`0p!) {`\"g\"`\"M%console.log(\"The following object does not exist: \" + id)`,<#`#W\"`!-\"nam`!&\"Not Named\"`#U)` :(`\"U'id` _\"`(^%`$&#!`#`+`*O#`#s&`,S#` +\"(`%1#x,`*r\".y], `3l'`(E%x:` [#[0], y` %%1]}`.>'`)S%text(`${#,`).\".y` |$name`-?)`*O!`*_#`'F$`!q!` m$` =+;}` ,!`3`%` &$`3\"! =`! #hid`%A( &&`%L(hid`-_$||`#M'hide)) {` d,`%f\"` -%`%{%` #\";` W&`/}\"`4h\"` '!`3{\"` 7&`*'\"`&j!`*+'` 0,]` U3`#h!`/){'stroke-`2s!':0, fill`-4#color, 'font-size'` 2#size` 0$w`3<!':\"bold\", cursor:\"`$T!er\"` @$family` Z$font_` -\", 'text-anchor` 9$` (\"`.C\"over`1v*{`!['h` :!`!g!` H#ut` 46` A#`#I*inactive`(L#`1@#`\"$\" = \"default\"`$]$attr`)>!` D\"`&j#`+.!`!,(` #&` 7&`!z.` #+` @'`!x,` #*` ?&`+m\" \"` ,!\"` .&id = id` '&`40%`-/'?`4H):`'z#` )!` J+_limit`(;%` )'` ?&`.o!`)v\"` +&`.n!`*\"\"` +&`.n&`.h\"` 0%line_x` ~%` )\"` 0+y` 1*` l'lin`\"=!`3t!` -%`+m\"` L%` )\"` 8#`,J%(`\",\"t`(I\", ` *!)`)=\"`1z#display`-i#`3d<`!6%` P# = \"out\"`3HJ` S/`'0&` -#`-1%` :/main_settings`*p#_` 7$? ` \"9: \"all\";}` e7`\"m)`1R(`$&!||` :#pil`-j!`3<)`\"3(bbox = get`.6\"_bbox`,R%` i*`1S#`%8!path` P$` ($` O$` =$`0t%` 7\"` ?$` 7&_siz`%V&` (&* normalizing_factor *`%U\" * 1.25` Z&` X! = {`./\"`+k#` 6!`.$#`-J/`.S*`!1%};line`+d\"` s&)` 0\"`1t%` '\"`*H5`#o!`($%` F#`\"42` 6%`+$-`#9#line;all`#`!`0p$in`4%%set` )(`$L!`&~&`'Q%state\" &&`%L'`$f$`%-%`4O$`%O$`#^!p = 0.1`#j\"calculated_`#5!` J$` Z!.` -\"* (1 + p * 3`%$\"pill` G%`!:\"` (\"?`!G#` (\":` n-` T&h`2J!`!!*` -#`!($`!&\"`!x)x - 0.5 *`!8'` A!y` :(y` 8*` q\"` B!r =`!2)/ 5`1}\"ill_array[id]`#A#`%/#` .*`*y%` 6(`(.!rect(x, y,`!R'` %#`!)\", r);` a*` y#;}pil`-`?` A!`'.$` &#`-.#`,f%` /%`2G)helper.clone`&%'`3q(`4]+mage) {` \\..f`\"Q\"cattr[` G'd].`)[!`\"'#`411`!13` :+`!-<` @+`!68h` G!`!I+adjacent`!7A` :/`$##`+Q!`\"o.`!x\"` g#x_in`%*\"`$g\"`/j', [`*!#,`0:\"])) {`!/&`)|#` b#bbox_union(`\"8'bbox,`*8*]`+/#` {B`3`#`!9% &&`2m'` 8\"`(*#` '\" =` \"#`'?#` =,];` \"\"`!d:` 6*`!p.` 6&zooming_dimensions`.b%` &;get_` '.(`\"-#;}`/P,`)E!all_pill`.t#`.3!`.k,`.H!`.-`3[#`*y#` '3if `#v- != \"out`/J!` (1all\" ||` 3&hide)` y(hide(`!4&all_visible_` @!`1\"$` I$`!;,`0p/location`!R!!`1{)) {` 6$` f5`2?!` &2` \"%`-<)` -%`(Z!!vml`\":$.node.setA`(|$(\"class\", \"sm`!!\"_\" + id`\"2#resize_`\"1\"`/I$adding = main_settings.`\"\"%auto_` >$? 1 +` %A* 2 : 1.3`0!size`.h!` 5$`'N)`2t#/ normalizing_factor` Y!lc`2l%`$-&` 5!old`#I# = lc`$G!` *\"`!1\"hape` 0&` *!_type`#I!` 7#= \"triangle\") {`!d#` \"!*`!|!`%y#` E*star` =.2;}`!j#_id`!/&id;l`.b!` 3\"].` K';make_`#=$(` <\")`\"L(`#W%`%S!` \\#`%j\"`2>)lct;`\"W) =`\"v'` -*`)4)` 3#`$[!`!S#true;}};for (var id in`&h#`/K' {`!y\"abel(`&D!`'X'`)8$function`+x!line_pat`!6$`&a\"bb`$v(bbox`\"]!w = bb.x2 -` #!` 0!h` /\"y` .#y` 0!r` T(scale ? ratio : 1` >!x_adj = 0.5 * (1 - r) * w` 9!y` '3h` V\"` s(`!~!`!F\"y` '-`!I\"miss`(q\"!x || !` 1\"`$F\"`&^!`'@.`+a$`&x!` =)`+i,` w#) {`!K)` ]&point0.x;`!S)` /-y`'P(`!\",`4 \"`!!*`!!`$>*`3i*;x`#9&p`$T\"+ ` $!)`!G!` 0'y` 3$y)`(P\"cur`!=!`\"C$ = {x:x, y:y}`#9\"ts = [];pt`';#{x:`%X$`$z!, y:` }#bb.y +`%d\")})` C- +` &I` 9&x` =\"x2), y:` M#`%z!` (G2 -` K%`'e!inner = {`)*'k in pts`#s#`\"x%abe`47!ts[k]` Z!distance_between = helper.` 2$(` Q),`#]-)`&R!k == 0 ||` `.<`!]#` i%) {` +#`!I$` v);` 5$`$e&`!$,` ;$` j$ =`!&-;}}return`\"($linePath(`!0(.x,`!V$` *\"y` &&`!%#` 7(` +$y`,1(`+0!_t(e, s, t, x, y, r`#u$x = x === undefined ? e.sm.x : `*j\"cy = y` 02y : y`#a!t` 3*) {t = \"0,0\"`4C#` 0-r =` `\"rotate;`\"n$\"t \" + t + \" s\" + s + \",\" + s ` \"$cx` ,%cy + \"r\" + r`1g#`0t)`,Y\"`1H(;`\"z%creat` /&s(refresh`\"|#`3Z\"paths = {`3N$:\"M -0.57735,.3333 .5` \"'0,-.6666 Z\", diamond:\"M 0,-0.5 -0.4,0 0,0.5 ` &\"Z\", marker:\"m-.015-.997c-.067 0-.13.033-.18.076-.061.054-.099.136-.092.219-.0001.073.034.139.068.201.058.104.122.206.158.32.02` 5\"039.117` A\"75.006.009.011-.004.` \"#9.037-.125.079-.249.144-.362.043-.08.095-.157.124-.244.022-.075.016-.161-.026-.229-.048-.08-.134-`\"*\"227-.146-.013`\"0\"-.027` \"%4` $\"z\", heart`#5!275-.5c-.137.003-.257.089-.3.235-`\"u!379.348.539.58.765.202-.262.596-.33.576-.718-.017-.08`#q!`\"0\"-.13-.206-.087-.066-.20`!!9-.3`#$!5-.055.02-.106.053-.143.098` c#081-.169-.127-.272`#R!\", star:\"m0-.549c-.044.12`!*!4.25` B\".379-.135`\"k#271` ##405.002.108.078.216.155.323.23`#I!2.0`#t!16.057-.023.085-.032.099`\"0!.1` $!97.298.049-.031`$w\"068.143-.101.062`!]$4`\"`!.185-.133.109.077`!=#8.326`!=#4`\"Q#082-.2`\"z!23-`$8!109-.079`'L\"156.3`\"}!36`\"L!`%<$`%H\"2`\"L!-`%)\"04`\"x!`#&!1-`##%-.377\"`1?'id in custom_`*C!s) {`*F'[id]`/]!` 7'[id]`+]\"supported` 3#`4`\"` o+` c') {` C,`3P\"id);}`0##clear_sets([all`,-&]);`,f*`3Q\"`,h) = `,n%(id`,[#posi` 7#\"center\"`-K!attrs = lattr`\"2!if (` 1!.type != \"image\"` c#attribut`\"G!{'stroke-width':` S\"border *`14\" * normalizing_factor, ` S\"` E)_color, fill` 2#` +#opacity` .#` '#, cursor:\"poi`\".!}`\"/!over_`!D?h` D!`!\"g` k\"`!^1` 5\"`!^7} else {`$>'` T\"`#x!_` /$`$L%`\"2&`\"<E`!\"/`%#&inactive) {` L&.` M\" = \"default\"`'p#`'M!`%\\!`%q'`%m!`!9!size`!y%` (!`#D0`&A'x &&` H#y`'*%int0`'\\\"` &\".x` k%x,` 9#.y` +%y`#<%` R)getxy`! #lat,` G#lng)`\"'\"rotated = `)9#` +\"([`!.$`!\"&], transform)`\"E!` 5! = {x:` a#[0], y` %%1]}`\"='`\"s\"= \"auto`(k$l = {sm:{}};l.sm.displ`*9!` P\"` )#` 7\"auto_`#Z#true` 0\"`$%#\"`*_$\"` 1\"hide_label = fals` H#` .!`,G#` ,!`#&%` #\"` /\"x` ($.x` +\"y` (%`!F#`%<)` #&` 7\"id = id`,E+`.*#l;return`&_\"` \\(= \"circl`+j%`,p'pa`-\\!` :!(`!T#`$8#.y,`&R\"* 0.5`$8\"bbox`$7\"` E# -` 9' * ratio, y` 9#y` )3x2` X%+` K2` 9$y` ,1`*7%if (`&?#x_i`\"k#`\"S',`0a-)`\"^#cs =` u!`\"4!`&]%`\"q$\"S\" + cs + \",\" ` \"%0,0 T\" +`$`$` 7%`$_$`'@!ath = Raphael.` t%Path`!V#`2f\"`!_&`'~(`!?!).toString() + \"Z\"`'r!`$j+marker\")`,|)\"bottom-`1Q$`)Q!`$T#`!L$pathBBox(path`$w\"`%F-path` 9#`#d'`!3+`2 )`.7\"` ^'`.H(url ?`.Y)url : directory +` 1)sourc`$9\"`!J-` =!(`!!*, 0, 0)`(6%.sm`-(\"`\"T'`)m\"`\"]$_preload` X-func`!+!(`\"1$wh = this.`2O! /` '\"height` }!new_` )\"`&-(new_` L\"=` 6(* iwh` 8%`*m' -` H'/ 2` ;%`+%\"`$|$`$t- ?`&Z$` [#`!\"#:`&q$` )*/ 2`\"~&attr({` 4\":` ;&, `!A!` -!` %!, x` (!x, y` $!y}`#a).`*u&` 9*`*L!`\"C\"+`\"5&`*A!`\"5\"` 0\"`!1\"};}`&G%`#w!`#-H`# O`\"g3`&B1rect(`\"5#`\"3#`\"$'` X&`-^,`\">M`\"b%`'<-`#d(attributes`)$#` '\"`$k+` /&`$@*original_`+p%`2]!` %#` =*ver_`!!)` #+` A)`1X,.sm.name`*g%name` 1)scal` 5&` )!` .._limit` 8*` .\"` ?)`->'` #$` 3)url` [%url` /)`3r#\"` ,$\"`!8*`4%4` 9'descrip`-2)` )'` 44_mobi`\"X'` ).`#T*nactiv` F&` )$`$H*n_click = is_on` (!`%J!s.popup`%B*` .!_off` I#ff` 17uls`!4&` )!`'n!underlay` 1&`,A.\" ? true :`.h#`$1)` W&` #$`!'._speed`!1*` .\"` 80iz`![+` .!` 7/color` <+` .\"`1o$` ((:` -#` )!` a)`+q'` +)`+e'`#j+int0` 5$0`-0bbo` ^*labels = []`&v*`\"H\"`\"1-hid`\"\\&hid`''+isp`$J(` )#` :&`*n%(`)1\"t(`3#&ratio * ` 5!));if ` 5%` p)= \"region\" ||`.4%` 3,state` =!`!c&) {` F%hide()`-M).content = create_` *#`!?%`!P\"`&*$) {`&e\"_` 8$s.push` D'`19$top` *7all` $6` \"$_array[id] =`\"M%`!K!!vml`\"/(node.setA`.0$(\"class\", \"s`!c&_\" + id);}};for (var id in` v%s) {make` K%(` J\"func`)J!`#a!_or_`$,\"(` +!)`3k\"leve`-_!pi_object.zoom_` 3!`2s\"evel_`/c!` 01_id` C!` w\" =`!)\".sm.` +#?` 4#`\"u#` 0+]`*r%if (` .\") {`&4!`!V\"= \"out\") {return` h#`$O$` <*`&N#` V(`!w!=` O#`1n\"` b&`!I!`%9%` r+` m1`'.\"`#8#current_` 0!`\"T$`\"B#`!5$]`\"5!` =)`\"s'=` pgmanual`\"U$`)f\">` s'zooming_dimens`'C!r`#?4`!7+`!)$` ',`&<%is_adjacent(element`%Vi`#n3`$y)!= `!4#`#<,tru`#:-`&P\"`#42`%}&`'N)` s'`'G2` 0-`'?<`&`9`!T*`!~*`!v3`44!pd`&a!ttr = `$9%(e, pos, anim,`.L\"`!D#anim =`4!\"fined) {` .\"` q$` A!ttrs` 6-` /\"` <(!` o(po` N!\"over\"` E'`'!over`!c!`-=!s`*'(` P$`&-$` K-` 1$` M/` :+` 5'`!Y#nim ||` 9\"image) {e` D!`\"?\"`0*&e.animate` 3\", fade_time`.n!`#d'pill`#b.pill`#p#overr`2;\"if (` '$`#A,` .&`#H(` B(`+u*par`2i\"pill.sm.` +\"`#@(` P)`'M*` U%`':)` ]*`'C'`\"o%` O)`0q,` Q*);`&q!`#;!`(!!`%;4helper.clone(`!1&`%V+`$3%`%F<` X3`%e/`%%&` <<` G(`!;!` 2%`&\"! &&`\"o#.sm.typ`$=/`0<#`!\"#cattr[` \\'`0H#`\"o1.fill`0|&` .!h`\"z!color`!x*` 90` @#}`*w'`&v(`&K!`*r$;};`'>%highlight_labels`/Z$, type, `#L$`'T-!`-F'` U\"`,u%`%~)` 6\"`.4*` .\";}` \"\".forEach(`(r&` 4!`!!$` (!.sm` u'`&!`\"_\"` 6$.pill`#\\!`$4%`#Z$` <#top();`\"u(` 4!,` A$`$8\"ill) {`*J,`#E#` B$`0o(`!&%reset\" ||`#6!`!<\"u`'3!` 1ut` lCut\"`#s'` N!`$.$` <7`(O%` P(}}}});}`#`%`#{\"_inactive`%5$`$!~`$u*` M'`!C$`$N%`\"/!{cursor:\"default\"}`*/&` 30pointer` B!}`,?#inserting`.V%`!D%emphasiz`\"Q,`!M'`$n!!`*F(`\"a$`\"z,` e$able` ;(`!4'true;` K$` 3\"Before(all_visible_`!%!s);setTimeout`#4') {`!.}, 1`\"G#currently_ove`1t!` =!`(T!u` \"$last_`2_#d` 7)`0M#click` D#bel` %'` 0&over` #\"ut` #!background` O-` C%` (#` G'` A'` %&_handler`$A&create_event` 6$s(`%f$`\"D%`\"{)f (this`1m'`4&\".call` +,;}};`!U%` EBut` K;`\"'!` _)`-~$` \\-` F!` [0, `){!;`$Y!` ^*, manua`-h%e` Q!` C!&& !` 4%`-r(`,=\" ` F!shape_type`+X!` /!`'g%location`,k*`2o!` .!ratio < 0.05`.r,`!d#e`3g#);top_` i$s.toFront();` ,$`1>#` /'var mag = 1 *`!i\"` r!_siz`'%\"stroke_width`!(!`2@!['` 2\"-` 3!']` F!anim_to = {` 1*:` X)* 4, ` 9$opacity':0`#u#`+d#'fill` 3',`!B#:`!Y'`40!}`!\"callba`%8+) {` n\"remove();}`)9\"`$2%cale ?`#_#: 1` :!ty = (mag - 1) * 0.5`\"v$size * r * ` X!`*I&_t` n$posi`!<!== \"bottom-ce`-m! ?` Q\"_t(e,` \\& * mag, \"0,\" + ty) :` .7);`#d#.transform =`!>$`#?$`+r\"(` C#,`$p)peed * 1000, \"ease-`2b\"`#?$);};`*90xy_hook_check(`3(\"!`(e!id`()!` '!`'?%se`4<!`/@(`.-&|| no_tooltip`2P(var`2D$ = `/#!_or_`-o\"`)s!`!3\"`0+'`.%$) {` #$`0!$}popup_off`35*` .%` g!`/G&pann`!X#` *'inch` %-zoom` .#`\" #_up && `!D'`\"P(`0G+`#+!` Q#`+7+`1*$`0w-this`#p\"`3B&` {$`*8\"_inactiv`3d&`\"},` 9$` P'`41.`%:!`%(!` Z*!`\"1'`!v#.show` <&`!!%top`%\\#vml &&`#r(`%Z%`-7&` 3*`-l&`-G'`#A+`\"\\$`\"0!gnore_hover) {update_`+H!` ?#, \"over\");highlight`-J#` 0.`(C!`\")&}} else`/z#`%\"&) {`!L~`!n\\`!|,`\"<>}}`-a#eset_appearanc`2i+` N$`++%`%G&hide`\"f#is_adjacent`!;%`3A%`!x5` 2$`,^$` ?'attributes, fade_time, whenDone);}` A8`!D#`#&!` I9`\"j8`-f!true);`%M(`$9{ || `#('`%4F`\"pY`\"I;);}`%J%` N$(`$y#helper.isF` ;#(`%b%) {` $$(`&J\"out`&2)force`&0)`-t*`.Q'`0P2`)Y.`0/'`.'4` 2\"`1hJ`1GD`$\\(`.P:out`.G/last`!{\"ed`\" *` .(`2b(`\"^#`./`);B`#]-`1O-`+bR`(T(` -2`!J$` ?%`(M~`-mv`!T(`(M>`0t3 || `(/! ===` h\" {`.q,`.b/;`%X!`*T#d =`\"G$`)0!`(&%`/L&`(m8` +&pann` #.inch`%Z(`'rF`'r:pre`!e!`((), e` V\"api_object`$($` H!`*`(`(A'` '`(>%`!=!`\"R#`#g'&& e`%j&touchend`%?-` 4:star`+\")` P!`)@/`%v#pos();}popup_off`!e*` .%`#&,zoomable &&` +)`!D%`#m\"\" ||`+)\"destin`(!! !` {%`,-+` U%`&w!) {` $`!x\"`#~.`+Q+`\"p'`.0(`,Z,ut.call` +)`(L\",`&p'`!8#_to`-N&}`*I&` *.` 3#`-4,`!d3var lin`%U+url`!m\"ink != \"`+.!!no_urls) {var js_url =` [!.substring(0, 10)`#<!javascript\" ?`\"6! :`1\"(new_tab ||` g#`&h#` $%window.`,R$.href`!+#`\"Z%` =#top` 13`&\\$` B*open`\"<!, \"_blank\"`$Y-`'5%`,3(`$D(`%y(&&`1F)`,50` 3);}`*$7e`#^#coords = get_` (!inates(`&L'pos(e, {l:` H\".x, u` $$y});}` B$show`%a&`&{)true`0s9ver\");puls`.H%`!e\"`0Gg`/y)`0x6` 2$`1'(`,v!ver`1C!ibutes);}}`$@)`/}&`#V!lose_`!7! = docu` c!getE` @\"ById(\"xpic_sm\" + \"_\" + div`\"S\"` V'`(|!` %%.on`0r.`-*'`*^F.sm`*s4)`/T)` Y\"trigger`&-\"\"`!L\"`-x!\", []);}`2[!back` O&`!X%`34%`0,#ypeof`3J%`3|!\"undefine`0+!` 1&`!8#}`!2*back`!2#if (`!x\"`.W'`.;-`.s0`/:1&& initial`4D!_solo)` *(`\"!#`+(3`,-':\" +` I)`*$if (incr`%E!a`']!`!>9state`!U!` 30`!s\"`#S#`&g)&& `%>&`/bQ` v\"_array[`!#6]`0C1` 5D,`$}\",`%G%`1 &`,C!inside = is_` &\"`$~-, `! )`$U(]`1C\"` :\" =`#L:manual`%>#`!1!?` _7 :` /*-1]`(u-`#<e`#H7`#,1`)@(_handler`+%,` 8&(`)|!` 4%`0 .`&9\"e.touche`4@$` )!_obj = e.changedT` =\" ?` \"-[0] : ` \\%[0]`2`# {x:` c%.clientX, y` $-Y}`\"[%var y = ie`!$\"` :\" +`.E&`.P$`.M#.scrollTop`!@!pageY`%W!x` V,X` D>Left` ^%X`!|'x, y:y};}`#D&setup_panning`#p$ground`-Y/`#d#`.Y$`/J.`&3,) {reset_appearanc`(:#`/D&`/|/`/P-}};`%3)new_viewbox`%;!`23!oords =`%N/`#E!newX =` @#.x` ,$Y` *&y;dX = (startX -` N!) * ` ,!.r;dY` 6%Y` 9\"Y` 3(var pan_threshold = 5` 5'if (Math.abs(dX) >` @+|| ` 6&Y` /-) {currently`$K$ =`)Z!;}`$|&`!)\"x + d`'#!` *\"y + dY, w` )#w, h` $#h, r` $#r};}var mousedown`#z%var`\"#\"`\"B!`\"}\"` +&Y`$6&` +!_pan`)Q%`+W)`!a#`$q#e.preventDefault`'d!` %*() : (e.` S\"Value`!M$);`!(! = {x:`\"t#`%P$.`'h!` %,y, w` $-`\"f!` %,`\"p!` 9- / original_width / scale};`#'(`$.!`&U<`&@#`&h'`&3#`&f'`(p+` '$pos(e, {l`$Q\"X, u` $\"Y})`-`'during`#)!`!^%`$#%;}`-r) &&`-=&.length > 1` E'var v`\".#`)=*;paper.setViewBox(v.x, v.y, v.w, v.h`!V(finish`!K3 || !`'a-`'o3`&+\"`'&.`!,d`%<+ = v;`4@, = {sm:{}}` *-.sm.zooming_dimensions = ` i+` D1type = \"manual\"`\"3/setTimeout(`/!)`\"p6}, 1)`4\"\"arrow.show();}helper.addEvent(mapdiv, \"`!-%\",`*;&);` 2:move\",`&_'` 1<up\",`%L'` /<lea` |!` -A`'P!`!v!`!\\C` L!`!WH`(Q\"nd`!C+`3_-inch`3j#var `%/\"istanc`,u%`2E*` N!` ;%`2M%xy0`-9\"`)j%[0].page`/w!` %-Y}`,3!xy1` G,1` B1` /#Y}`(b# `\"T#`!@%xy0, xy1`\"9(move`!k\"`*-%`&_&`'#`+LO`'B'`#9#`.?(`#4'`\"{1`3z!`#\\)`#<#diff =` V&-`$!*` x!magnitude =`48'iff` q\"` 5&> 10`\"L#` s!> 0) {zoom_in_click();} else` 1#out` /&`%:,`!=%}` L$` '7`.&.`#{%`&,2`*t?`#`&`+.&00`*l8`(y)`%A&`(`C` 'F`(y*` L\"`\"X&order() {all_states.toBa`#b!bottom_loc`.?!` .(ackground` *&if (all_external_lines` t#` '*.toFront();}all_label` *(top`!*)` 0$` ,$` @.`+/)_events(refresh`3+$` %&`\"C'hover(over, out);` 1'`&J\"`&Q!`\"G)` 2\"` '&`&s\"` 6)` q\"reset_tooltip, ` \")`0j)` h&` f\"_handler`#Q\"responsive) {set_` '&` A$()`*|\"`2W\"`+:!`$f,`&H$`\")$` -,end`\"@(`!S#` 2%`!S&);`.y#`3'\"(` '%`/'%;}`$r\"`$Z%`#Q1` 5&`#\\)`%G'` Q\"` (!_` W\"` %#` Z%` B\"` Z\"` 6\"` _#`\"W2`&)(`\"E*` +/`\"}-`&M%` T#`!$)}}`-b!etec`$<!iz`1Q'`$=4 {` :%` M\"(`/m!size_pap`$r\"var` 7#Timer;` w) = `,'*lear`,G$` K');` \"' = `,j'` 3\", 3`,M!`#5!window`+K%Listener) {` #3(\"` `\"\",`\"U*,`-K\");` ?5orient`#s!chang` I6`/T$` c$ttach`-.\"` vD` G(` nGif (vml) {document.body.on`#o2`$d$;};`1$'`$j*`,/\"`/'\".offsetWidth <`4b!return;}create_dimensions(true);` a!.setSize(width, height)`4(!scaled_b`/^!_`!_#` \"(* ` Q\" / original_` ,!) * normalizing_factor * 1.25`/_%`+5\" && `(N)`+h*forEach`3-'` 4!) {` #!.attr({'stroke-`!9!':`!h.});` J\"sm` N!ibutes[` J*] =`\"B/` O&over_` @:` C$` Y#`+z!`\"PY}`+I,`\"_.` 4$`%J#lattr[` -$.sm.id].type != \"image\") {` 6(`!x9` [2`\"<\"`!_T` b(`#5>`!,2`#W\"` ac`%s1`\"]B});}`$?$in`&1line) {var adj` B!`(i$lin`%m!`%)X` _!`!m1`!!)});})`*h%trial_text(`*J\"min = `!8$2 > 250 ?` '': 250;max`!O\" = popup_max` +\"? ` \"+: min;`,V(veal_map(refresh`\"p#region_out = fly_in ?` .$array[-2] :` %*initi`4:#]`\" !` 9\" =` ,8if (!` +$back) {back_arrow.hid`.U!` B!`!\\&if (` \\(`,<\"`!x#destin`%h! = ` 5.`13$` T-manual` D>` B\"`1y%` <.`#7&;}var `!6\"lowly =`!f0 ||` o0) &&`#w&`1{! : true;` A!to(`!.',` |(`#-#` }*olo &&` }) != -1`#n#ground.show()`#sDfor (var i = 0; i <`\"Z#`(f!`0U\"length; i++`#Y#id`# %` >&[i`%t\"`\"o\"`/=#`%p$d`%g#`/P%hid`13&`!n#}`!D(in label` \\\"`!2#` -!`,d!` 1%`!/$` -\"set` 6%set` 9&`/L\"bel.sm.parent`/_%` )).sm`/_\"== \"`!Z!\"` /6`(?#!`\"b(id || !` 55) {` 3%hide =`%P\"`!l%`$5%}}}all_externa`-<6`/5\"`!_#Raphael.isPointInsideBBox(`!Y&bbox, ` Q\"` ($.x,` \",y)) {` ,$`$;$`&5#`*)#`'[&) {`'P$`*y6);}return;}`'\".all_visible`(t\"s` &0`#\"!` -)`\"q+`'v(`!U- && !`)ZG`!nD`.n(`!.!`.v!call`)6#get_` 4#able_info();`&!`2i%();`0n#`\"Z\"(true` +%`!;\"` &+`3S$` ',`#!!();style`*]!`#g\"();hide_and_show_before(last_`,$)` j\"`%9!`!b#event`!!$resize_pap` ;!`1F'` :\"` x*after` t-);upd`+!!pi();trigger_hook(\"`#A$complete\", []`$r\"helper.isF`#o#`#`&) {` $$`'*!var tooltip;`$;%load`$1,mapdata = api_object.` .#;mapinfo` ,-info`!M!map_inn`)A!delete window.`\"|!;}expand`\"B#preload();get_client`%J$get_` h\"` *!`0$!s_forgery()) {alert(\"The continent map can't be used with other data.\");`)4$`&V3`%u#d`(0!ructure` ,'imens`&F!` +%canva` %'trial_text`):$popup_nocss) {set_tt_css();}`$1# = ` W$` +\"` f&nav_butt`!/\"`(<-`'J/`(R*`(K-`(7&in` ]\"etTimeout`.&') {`(n-`(i.re`!L!ll` w#`!N*`(Q$` O#`(W!_handler`\"%$`(j#`(Q));`\"_#.` T\"(`((9`'`Yxy` a!_che` +\", 1);`(N!getting_xy = false`3`!get_xy_fr`-]!p = `#+&log`1<#!` S'|| ` 3\"` e)`2L!} else {`&n$everything.mousedown`$*'e, a, b) {var l = ie ? `#?!.`(^\"X + document.d` \"#Ele` +!scrollLeft : e.pageX`\"0!u` W0Y` I>Top` a%Y` e!find_pos = `##findPos`*u'` D!x0 =` E%[0]` 0!y` ()1` /\"go = `-h,.sm.zooming`)L'` K!this_width = go.r * ` )\"/ scal`$b\"` A!height` =&` )#` <(x` 8\"x` -$ +` u(* (l - x0) /` \"`\"\"\"` O\"y` E,` y#* (u - y` Q!` .\";x = Math.`*!\"x * 10000` ?!` $\";y` 6*y` 00var print_string = \"You clicked on\\nx: \" + x + \",\" + \"\\ny` .\"y` .\"`(b!`'0\"console.log(` m();}`):+lick_xy\", [{x:x, y:y}]);});}`!S!lo`(g$`+\\(`(c+`36#` L\"`)S)` I,`*F!ooks`1G$`!:$ || plugin` Y!s` 0%.length > 0`!&/`*@!);}`!5\"ov`\"3#`!*)e`'z\"`)<#type = ` .#.sm.type`#8!` 5\"= \"`.c!\") {`# *` z!` 6\", [` W'id]);}` V)`.[$` O3` 6%` H=`/ \"` P3` 6#` Q0`\"k#ut`!uxut`\"-_ut`\"+`ut`\"L>`&.\"`\"X4, e`\"1^` }\"`\"_2, e`!gE` b\"`!{3` U0`#G6` e\"`#J5` f#`\"|\"pre`\"#~\"` }%`\"F^` b%`\"Fa` e%`\"hBzoomable_`\"2~\"` }+`\"X^` b+`#\":};`\"-%` C\"_zoom(id, callback`\"<#` 9\" =` @$array[id];zoom_to(` 3\", `/7!` U';}` {%`\"9!` j5` 9! =` ?#` o.` 3!` c9`$w$` y&zp` z)if (!manual` ?!`3F+\"L` Z#`%>! only works when the map is in ` \\' mode.\");return`$@&of zp === \"undefined\") {zp = 4` ;)`!^$` ;/` 1&`\"W\";}var destin`!i\"= {sm:{type:\"`!S\"\", zp:zp`'M#`\"s$ =`\"|&`#Y&var w` 0'.sm.size * scale * zp` A!h = w * original_height /` (&width` H!x` d+x -` X!0.5` ;!y` 1+y - h` 5'r`!(!/ (` n*`!N$);`\"Y'.sm.zooming_dimensions = {x:x, y:y, w:w, h:h, r:r}`&0%` ['`&\"9reset_tooltip(`&(#currently_over) {out.call` *,`%(\"!` X#_`$@\") {`%l$ else {` 2*`%)&if (on`*0\"` K'` H#.hide();setTimeout(`,F&) {}, 100`\"!(popup`&X!, id`\"&#`,<.var`,p$`)H/`!n#`,\"3` N*`+(-`\"J$` 9*`&k/`#3\"` <#`)d+`!2!+ \" \" + id` &!does not exist`)N%`(Q(`#B$`/(*` .$`&X!bo`':!ast_`&&=`/a&!`1:+` e!b` y*bbox`(7%(bb.x + bb.x2)`(.+(bb.y` 7\"y` 4%x = x`'|$;y = y` %%`#H(x`!(*`!-\"y` '*y`\"i\"`'$#_`!L!x - box.x) / ratio` U!` <$`!S!y` =#y` :&`(\"'_x > `)c$1.1 ||` T'> `+-#` 7!`.S,Not in this`%l#`$P-`(2-tru`3h\"`(6'` #!`)C\"`%r$`\"u%over` +,`(a$`*?\"pos`\"#&,`!s&,`#5$);ignore_pos`!:$`)d3`!v#` >!`)6&`0X#pulse(`)<!`(\"\"`'m2`'{#`*N'` S\"el,` t!`*)-_`*k\"`+G%`*y$`!\\$up`!W%`#*+`,W%`'v!` 1!ed`#1'`,f5`-](fresh_`+*!`4#!`3y'set` 0%)`&5!` )!`+K/`1s!abels` 5$.sm.` ,\";make` ]'for (var i = 0; i <` U#.length; i++`#_#` 4!_id`*;!bels[i].sm.id;set_` /!(` =$)`!%\"` %,`,0!helper.isF`\"T#(`\"@%`4M'();}`)\"!no`0V$s`#u%`#,%disable_`$Z!s() {` A*`&7(`$f$` T>en` X9`-H'no_ur`#f!`!B3url` Z$` B#`'M\"` @9`!9#` J.`!3#` @%go_`#'!`%_'back`&[\"` ,&` G'expand_api() {api_objec`&z!ibr`&.\"create_bbox`%h\";` >'get_xy`)-!g_xy` /(proj = get` **loa`%r!oad_map` /(`2:#zoom`2E&zoom` 9(`'^\"` =#` #&` 7(zoom_in`\"{%` +,out` (0`+-%` v#` #)` =(ba`2S!`#j#`\"H)op`*s!` #!` ,)ulse =`,H)` H-`+n!` V$` (!`!j-lev`,}!\"out\"` 2(`.&#`%(!`%T&` J1`)r\"` 0-`&z( =`'()` ;(`&e' =`&r(` X0`(P\"` c'` +\"` ]/` A%` #)`%P*`-O!`%U!`-X\"`%n-`-j'` <'`'%-loaded`)6%`('%upd`&'$`#$\"`((+`#u)`/X!destin`%a!.sm.typ`#W9` @0id ?` X1id :`*4-`!\\#`)a.`\"@!`/^%`/~\"`(j.s`(n&` 1-`'g$s`30-` 9)`0g$`/C\"` /-`-T# = ` ##;}`+[(;`*P$();}window[plugin_name] = (`\"D%(`4E% ` x&;})();docReady` ?*trigger_hook(\"ready\")`4:!` x/`%@#` {%;}`2O0auto`!o!`\"E!`2X/` m\" =` ;+[i`3y\"`!@!_to_`-1#` H#&&` ##.mapdata &` \",.main_settings.au` ]$!= \"no\" ?`';!`%\\%if (`!!)) {`\"t'` w\") {setTimeout`#/*`!7#load();}, 1);})` P$;}}});`\"_+push(`$1&` K!\"simplemaps_countrymap\");"));
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*! Select2 4.1.0-rc.0 | https://github.com/select2/select2/blob/master/LICENSE.md */
!function (n) {
  "function" == typeof define && define.amd ? define(["jquery"], n) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = function (e, t) {
    return void 0 === t && (t = "undefined" != typeof window ? require("jquery") : require("jquery")(e)), n(t), t;
  } : n(jQuery);
}(function (t) {
  var e,
      n,
      s,
      p,
      r,
      _o,
      h,
      f,
      g,
      m,
      y,
      v,
      i,
      a,
      _,
      s = (t && t.fn && t.fn.select2 && t.fn.select2.amd && (u = t.fn.select2.amd), u && u.requirejs || (u ? n = u : u = {}, g = {}, m = {}, y = {}, v = {}, i = Object.prototype.hasOwnProperty, a = [].slice, _ = /\.js$/, h = function h(e, t) {
    var n,
        s,
        i = c(e),
        r = i[0],
        t = t[1];
    return e = i[1], r && (n = x(r = l(r, t))), r ? e = n && n.normalize ? n.normalize(e, (s = t, function (e) {
      return l(e, s);
    })) : l(e, t) : (r = (i = c(e = l(e, t)))[0], e = i[1], r && (n = x(r))), {
      f: r ? r + "!" + e : e,
      n: e,
      pr: r,
      p: n
    };
  }, f = {
    require: function require(e) {
      return w(e);
    },
    exports: function exports(e) {
      var t = g[e];
      return void 0 !== t ? t : g[e] = {};
    },
    module: function module(e) {
      return {
        id: e,
        uri: "",
        exports: g[e],
        config: (t = e, function () {
          return y && y.config && y.config[t] || {};
        })
      };
      var t;
    }
  }, r = function r(e, t, n, s) {
    var i,
        r,
        o,
        a,
        l,
        c = [],
        u = _typeof(n),
        d = A(s = s || e);

    if ("undefined" == u || "function" == u) {
      for (t = !t.length && n.length ? ["require", "exports", "module"] : t, a = 0; a < t.length; a += 1) {
        if ("require" === (r = (o = h(t[a], d)).f)) c[a] = f.require(e);else if ("exports" === r) c[a] = f.exports(e), l = !0;else if ("module" === r) i = c[a] = f.module(e);else if (b(g, r) || b(m, r) || b(v, r)) c[a] = x(r);else {
          if (!o.p) throw new Error(e + " missing " + r);
          o.p.load(o.n, w(s, !0), function (t) {
            return function (e) {
              g[t] = e;
            };
          }(r), {}), c[a] = g[r];
        }
      }

      u = n ? n.apply(g[e], c) : void 0, e && (i && i.exports !== p && i.exports !== g[e] ? g[e] = i.exports : u === p && l || (g[e] = u));
    } else e && (g[e] = n);
  }, e = n = _o = function o(e, t, n, s, i) {
    if ("string" == typeof e) return f[e] ? f[e](t) : x(h(e, A(t)).f);

    if (!e.splice) {
      if ((y = e).deps && _o(y.deps, y.callback), !t) return;
      t.splice ? (e = t, t = n, n = null) : e = p;
    }

    return t = t || function () {}, "function" == typeof n && (n = s, s = i), s ? r(p, e, t, n) : setTimeout(function () {
      r(p, e, t, n);
    }, 4), _o;
  }, _o.config = function (e) {
    return _o(e);
  }, e._defined = g, (s = function s(e, t, n) {
    if ("string" != typeof e) throw new Error("See almond README: incorrect module build, no module name");
    t.splice || (n = t, t = []), b(g, e) || b(m, e) || (m[e] = [e, t, n]);
  }).amd = {
    jQuery: !0
  }, u.requirejs = e, u.require = n, u.define = s), u.define("almond", function () {}), u.define("jquery", [], function () {
    var e = t || $;
    return null == e && console && console.error && console.error("Select2: An instance of jQuery or a jQuery-compatible library was not found. Make sure that you are including jQuery before Select2 on your web page."), e;
  }), u.define("select2/utils", ["jquery"], function (r) {
    var s = {};

    function c(e) {
      var t,
          n = e.prototype,
          s = [];

      for (t in n) {
        "function" == typeof n[t] && "constructor" !== t && s.push(t);
      }

      return s;
    }

    s.Extend = function (e, t) {
      var n,
          s = {}.hasOwnProperty;

      function i() {
        this.constructor = e;
      }

      for (n in t) {
        s.call(t, n) && (e[n] = t[n]);
      }

      return i.prototype = t.prototype, e.prototype = new i(), e.__super__ = t.prototype, e;
    }, s.Decorate = function (s, i) {
      var e = c(i),
          t = c(s);

      function r() {
        var e = Array.prototype.unshift,
            t = i.prototype.constructor.length,
            n = s.prototype.constructor;
        0 < t && (e.call(arguments, s.prototype.constructor), n = i.prototype.constructor), n.apply(this, arguments);
      }

      i.displayName = s.displayName, r.prototype = new function () {
        this.constructor = r;
      }();

      for (var n = 0; n < t.length; n++) {
        var o = t[n];
        r.prototype[o] = s.prototype[o];
      }

      for (var a = 0; a < e.length; a++) {
        var l = e[a];

        r.prototype[l] = function (e) {
          var t = function t() {};

          e in r.prototype && (t = r.prototype[e]);
          var n = i.prototype[e];
          return function () {
            return Array.prototype.unshift.call(arguments, t), n.apply(this, arguments);
          };
        }(l);
      }

      return r;
    };

    function e() {
      this.listeners = {};
    }

    e.prototype.on = function (e, t) {
      this.listeners = this.listeners || {}, e in this.listeners ? this.listeners[e].push(t) : this.listeners[e] = [t];
    }, e.prototype.trigger = function (e) {
      var t = Array.prototype.slice,
          n = t.call(arguments, 1);
      this.listeners = this.listeners || {}, null == n && (n = []), 0 === n.length && n.push({}), (n[0]._type = e) in this.listeners && this.invoke(this.listeners[e], t.call(arguments, 1)), "*" in this.listeners && this.invoke(this.listeners["*"], arguments);
    }, e.prototype.invoke = function (e, t) {
      for (var n = 0, s = e.length; n < s; n++) {
        e[n].apply(this, t);
      }
    }, s.Observable = e, s.generateChars = function (e) {
      for (var t = "", n = 0; n < e; n++) {
        t += Math.floor(36 * Math.random()).toString(36);
      }

      return t;
    }, s.bind = function (e, t) {
      return function () {
        e.apply(t, arguments);
      };
    }, s._convertData = function (e) {
      for (var t in e) {
        var n = t.split("-"),
            s = e;

        if (1 !== n.length) {
          for (var i = 0; i < n.length; i++) {
            var r = n[i];
            (r = r.substring(0, 1).toLowerCase() + r.substring(1)) in s || (s[r] = {}), i == n.length - 1 && (s[r] = e[t]), s = s[r];
          }

          delete e[t];
        }
      }

      return e;
    }, s.hasScroll = function (e, t) {
      var n = r(t),
          s = t.style.overflowX,
          i = t.style.overflowY;
      return (s !== i || "hidden" !== i && "visible" !== i) && ("scroll" === s || "scroll" === i || n.innerHeight() < t.scrollHeight || n.innerWidth() < t.scrollWidth);
    }, s.escapeMarkup = function (e) {
      var t = {
        "\\": "&#92;",
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': "&quot;",
        "'": "&#39;",
        "/": "&#47;"
      };
      return "string" != typeof e ? e : String(e).replace(/[&<>"'\/\\]/g, function (e) {
        return t[e];
      });
    }, s.__cache = {};
    var n = 0;
    return s.GetUniqueElementId = function (e) {
      var t = e.getAttribute("data-select2-id");
      return null != t || (t = e.id ? "select2-data-" + e.id : "select2-data-" + (++n).toString() + "-" + s.generateChars(4), e.setAttribute("data-select2-id", t)), t;
    }, s.StoreData = function (e, t, n) {
      e = s.GetUniqueElementId(e);
      s.__cache[e] || (s.__cache[e] = {}), s.__cache[e][t] = n;
    }, s.GetData = function (e, t) {
      var n = s.GetUniqueElementId(e);
      return t ? s.__cache[n] && null != s.__cache[n][t] ? s.__cache[n][t] : r(e).data(t) : s.__cache[n];
    }, s.RemoveData = function (e) {
      var t = s.GetUniqueElementId(e);
      null != s.__cache[t] && delete s.__cache[t], e.removeAttribute("data-select2-id");
    }, s.copyNonInternalCssClasses = function (e, t) {
      var n = (n = e.getAttribute("class").trim().split(/\s+/)).filter(function (e) {
        return 0 === e.indexOf("select2-");
      }),
          t = (t = t.getAttribute("class").trim().split(/\s+/)).filter(function (e) {
        return 0 !== e.indexOf("select2-");
      }),
          t = n.concat(t);
      e.setAttribute("class", t.join(" "));
    }, s;
  }), u.define("select2/results", ["jquery", "./utils"], function (d, p) {
    function s(e, t, n) {
      this.$element = e, this.data = n, this.options = t, s.__super__.constructor.call(this);
    }

    return p.Extend(s, p.Observable), s.prototype.render = function () {
      var e = d('<ul class="select2-results__options" role="listbox"></ul>');
      return this.options.get("multiple") && e.attr("aria-multiselectable", "true"), this.$results = e;
    }, s.prototype.clear = function () {
      this.$results.empty();
    }, s.prototype.displayMessage = function (e) {
      var t = this.options.get("escapeMarkup");
      this.clear(), this.hideLoading();
      var n = d('<li role="alert" aria-live="assertive" class="select2-results__option"></li>'),
          s = this.options.get("translations").get(e.message);
      n.append(t(s(e.args))), n[0].className += " select2-results__message", this.$results.append(n);
    }, s.prototype.hideMessages = function () {
      this.$results.find(".select2-results__message").remove();
    }, s.prototype.append = function (e) {
      this.hideLoading();
      var t = [];

      if (null != e.results && 0 !== e.results.length) {
        e.results = this.sort(e.results);

        for (var n = 0; n < e.results.length; n++) {
          var s = e.results[n],
              s = this.option(s);
          t.push(s);
        }

        this.$results.append(t);
      } else 0 === this.$results.children().length && this.trigger("results:message", {
        message: "noResults"
      });
    }, s.prototype.position = function (e, t) {
      t.find(".select2-results").append(e);
    }, s.prototype.sort = function (e) {
      return this.options.get("sorter")(e);
    }, s.prototype.highlightFirstItem = function () {
      var e = this.$results.find(".select2-results__option--selectable"),
          t = e.filter(".select2-results__option--selected");
      (0 < t.length ? t : e).first().trigger("mouseenter"), this.ensureHighlightVisible();
    }, s.prototype.setClasses = function () {
      var t = this;
      this.data.current(function (e) {
        var s = e.map(function (e) {
          return e.id.toString();
        });
        t.$results.find(".select2-results__option--selectable").each(function () {
          var e = d(this),
              t = p.GetData(this, "data"),
              n = "" + t.id;
          null != t.element && t.element.selected || null == t.element && -1 < s.indexOf(n) ? (this.classList.add("select2-results__option--selected"), e.attr("aria-selected", "true")) : (this.classList.remove("select2-results__option--selected"), e.attr("aria-selected", "false"));
        });
      });
    }, s.prototype.showLoading = function (e) {
      this.hideLoading();
      e = {
        disabled: !0,
        loading: !0,
        text: this.options.get("translations").get("searching")(e)
      }, e = this.option(e);
      e.className += " loading-results", this.$results.prepend(e);
    }, s.prototype.hideLoading = function () {
      this.$results.find(".loading-results").remove();
    }, s.prototype.option = function (e) {
      var t = document.createElement("li");
      t.classList.add("select2-results__option"), t.classList.add("select2-results__option--selectable");
      var n,
          s = {
        role: "option"
      },
          i = window.Element.prototype.matches || window.Element.prototype.msMatchesSelector || window.Element.prototype.webkitMatchesSelector;

      for (n in (null != e.element && i.call(e.element, ":disabled") || null == e.element && e.disabled) && (s["aria-disabled"] = "true", t.classList.remove("select2-results__option--selectable"), t.classList.add("select2-results__option--disabled")), null == e.id && t.classList.remove("select2-results__option--selectable"), null != e._resultId && (t.id = e._resultId), e.title && (t.title = e.title), e.children && (s.role = "group", s["aria-label"] = e.text, t.classList.remove("select2-results__option--selectable"), t.classList.add("select2-results__option--group")), s) {
        var r = s[n];
        t.setAttribute(n, r);
      }

      if (e.children) {
        var o = d(t),
            a = document.createElement("strong");
        a.className = "select2-results__group", this.template(e, a);

        for (var l = [], c = 0; c < e.children.length; c++) {
          var u = e.children[c],
              u = this.option(u);
          l.push(u);
        }

        i = d("<ul></ul>", {
          class: "select2-results__options select2-results__options--nested",
          role: "none"
        });
        i.append(l), o.append(a), o.append(i);
      } else this.template(e, t);

      return p.StoreData(t, "data", e), t;
    }, s.prototype.bind = function (t, e) {
      var i = this,
          n = t.id + "-results";
      this.$results.attr("id", n), t.on("results:all", function (e) {
        i.clear(), i.append(e.data), t.isOpen() && (i.setClasses(), i.highlightFirstItem());
      }), t.on("results:append", function (e) {
        i.append(e.data), t.isOpen() && i.setClasses();
      }), t.on("query", function (e) {
        i.hideMessages(), i.showLoading(e);
      }), t.on("select", function () {
        t.isOpen() && (i.setClasses(), i.options.get("scrollAfterSelect") && i.highlightFirstItem());
      }), t.on("unselect", function () {
        t.isOpen() && (i.setClasses(), i.options.get("scrollAfterSelect") && i.highlightFirstItem());
      }), t.on("open", function () {
        i.$results.attr("aria-expanded", "true"), i.$results.attr("aria-hidden", "false"), i.setClasses(), i.ensureHighlightVisible();
      }), t.on("close", function () {
        i.$results.attr("aria-expanded", "false"), i.$results.attr("aria-hidden", "true"), i.$results.removeAttr("aria-activedescendant");
      }), t.on("results:toggle", function () {
        var e = i.getHighlightedResults();
        0 !== e.length && e.trigger("mouseup");
      }), t.on("results:select", function () {
        var e,
            t = i.getHighlightedResults();
        0 !== t.length && (e = p.GetData(t[0], "data"), t.hasClass("select2-results__option--selected") ? i.trigger("close", {}) : i.trigger("select", {
          data: e
        }));
      }), t.on("results:previous", function () {
        var e,
            t = i.getHighlightedResults(),
            n = i.$results.find(".select2-results__option--selectable"),
            s = n.index(t);
        s <= 0 || (e = s - 1, 0 === t.length && (e = 0), (s = n.eq(e)).trigger("mouseenter"), t = i.$results.offset().top, n = s.offset().top, s = i.$results.scrollTop() + (n - t), 0 === e ? i.$results.scrollTop(0) : n - t < 0 && i.$results.scrollTop(s));
      }), t.on("results:next", function () {
        var e,
            t = i.getHighlightedResults(),
            n = i.$results.find(".select2-results__option--selectable"),
            s = n.index(t) + 1;
        s >= n.length || ((e = n.eq(s)).trigger("mouseenter"), t = i.$results.offset().top + i.$results.outerHeight(!1), n = e.offset().top + e.outerHeight(!1), e = i.$results.scrollTop() + n - t, 0 === s ? i.$results.scrollTop(0) : t < n && i.$results.scrollTop(e));
      }), t.on("results:focus", function (e) {
        e.element[0].classList.add("select2-results__option--highlighted"), e.element[0].setAttribute("aria-selected", "true");
      }), t.on("results:message", function (e) {
        i.displayMessage(e);
      }), d.fn.mousewheel && this.$results.on("mousewheel", function (e) {
        var t = i.$results.scrollTop(),
            n = i.$results.get(0).scrollHeight - t + e.deltaY,
            t = 0 < e.deltaY && t - e.deltaY <= 0,
            n = e.deltaY < 0 && n <= i.$results.height();
        t ? (i.$results.scrollTop(0), e.preventDefault(), e.stopPropagation()) : n && (i.$results.scrollTop(i.$results.get(0).scrollHeight - i.$results.height()), e.preventDefault(), e.stopPropagation());
      }), this.$results.on("mouseup", ".select2-results__option--selectable", function (e) {
        var t = d(this),
            n = p.GetData(this, "data");
        t.hasClass("select2-results__option--selected") ? i.options.get("multiple") ? i.trigger("unselect", {
          originalEvent: e,
          data: n
        }) : i.trigger("close", {}) : i.trigger("select", {
          originalEvent: e,
          data: n
        });
      }), this.$results.on("mouseenter", ".select2-results__option--selectable", function (e) {
        var t = p.GetData(this, "data");
        i.getHighlightedResults().removeClass("select2-results__option--highlighted").attr("aria-selected", "false"), i.trigger("results:focus", {
          data: t,
          element: d(this)
        });
      });
    }, s.prototype.getHighlightedResults = function () {
      return this.$results.find(".select2-results__option--highlighted");
    }, s.prototype.destroy = function () {
      this.$results.remove();
    }, s.prototype.ensureHighlightVisible = function () {
      var e,
          t,
          n,
          s,
          i = this.getHighlightedResults();
      0 !== i.length && (e = this.$results.find(".select2-results__option--selectable").index(i), s = this.$results.offset().top, t = i.offset().top, n = this.$results.scrollTop() + (t - s), s = t - s, n -= 2 * i.outerHeight(!1), e <= 2 ? this.$results.scrollTop(0) : (s > this.$results.outerHeight() || s < 0) && this.$results.scrollTop(n));
    }, s.prototype.template = function (e, t) {
      var n = this.options.get("templateResult"),
          s = this.options.get("escapeMarkup"),
          e = n(e, t);
      null == e ? t.style.display = "none" : "string" == typeof e ? t.innerHTML = s(e) : d(t).append(e);
    }, s;
  }), u.define("select2/keys", [], function () {
    return {
      BACKSPACE: 8,
      TAB: 9,
      ENTER: 13,
      SHIFT: 16,
      CTRL: 17,
      ALT: 18,
      ESC: 27,
      SPACE: 32,
      PAGE_UP: 33,
      PAGE_DOWN: 34,
      END: 35,
      HOME: 36,
      LEFT: 37,
      UP: 38,
      RIGHT: 39,
      DOWN: 40,
      DELETE: 46
    };
  }), u.define("select2/selection/base", ["jquery", "../utils", "../keys"], function (n, s, i) {
    function r(e, t) {
      this.$element = e, this.options = t, r.__super__.constructor.call(this);
    }

    return s.Extend(r, s.Observable), r.prototype.render = function () {
      var e = n('<span class="select2-selection" role="combobox"  aria-haspopup="true" aria-expanded="false"></span>');
      return this._tabindex = 0, null != s.GetData(this.$element[0], "old-tabindex") ? this._tabindex = s.GetData(this.$element[0], "old-tabindex") : null != this.$element.attr("tabindex") && (this._tabindex = this.$element.attr("tabindex")), e.attr("title", this.$element.attr("title")), e.attr("tabindex", this._tabindex), e.attr("aria-disabled", "false"), this.$selection = e;
    }, r.prototype.bind = function (e, t) {
      var n = this,
          s = e.id + "-results";
      this.container = e, this.$selection.on("focus", function (e) {
        n.trigger("focus", e);
      }), this.$selection.on("blur", function (e) {
        n._handleBlur(e);
      }), this.$selection.on("keydown", function (e) {
        n.trigger("keypress", e), e.which === i.SPACE && e.preventDefault();
      }), e.on("results:focus", function (e) {
        n.$selection.attr("aria-activedescendant", e.data._resultId);
      }), e.on("selection:update", function (e) {
        n.update(e.data);
      }), e.on("open", function () {
        n.$selection.attr("aria-expanded", "true"), n.$selection.attr("aria-owns", s), n._attachCloseHandler(e);
      }), e.on("close", function () {
        n.$selection.attr("aria-expanded", "false"), n.$selection.removeAttr("aria-activedescendant"), n.$selection.removeAttr("aria-owns"), n.$selection.trigger("focus"), n._detachCloseHandler(e);
      }), e.on("enable", function () {
        n.$selection.attr("tabindex", n._tabindex), n.$selection.attr("aria-disabled", "false");
      }), e.on("disable", function () {
        n.$selection.attr("tabindex", "-1"), n.$selection.attr("aria-disabled", "true");
      });
    }, r.prototype._handleBlur = function (e) {
      var t = this;
      window.setTimeout(function () {
        document.activeElement == t.$selection[0] || n.contains(t.$selection[0], document.activeElement) || t.trigger("blur", e);
      }, 1);
    }, r.prototype._attachCloseHandler = function (e) {
      n(document.body).on("mousedown.select2." + e.id, function (e) {
        var t = n(e.target).closest(".select2");
        n(".select2.select2-container--open").each(function () {
          this != t[0] && s.GetData(this, "element").select2("close");
        });
      });
    }, r.prototype._detachCloseHandler = function (e) {
      n(document.body).off("mousedown.select2." + e.id);
    }, r.prototype.position = function (e, t) {
      t.find(".selection").append(e);
    }, r.prototype.destroy = function () {
      this._detachCloseHandler(this.container);
    }, r.prototype.update = function (e) {
      throw new Error("The `update` method must be defined in child classes.");
    }, r.prototype.isEnabled = function () {
      return !this.isDisabled();
    }, r.prototype.isDisabled = function () {
      return this.options.get("disabled");
    }, r;
  }), u.define("select2/selection/single", ["jquery", "./base", "../utils", "../keys"], function (e, t, n, s) {
    function i() {
      i.__super__.constructor.apply(this, arguments);
    }

    return n.Extend(i, t), i.prototype.render = function () {
      var e = i.__super__.render.call(this);

      return e[0].classList.add("select2-selection--single"), e.html('<span class="select2-selection__rendered"></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>'), e;
    }, i.prototype.bind = function (t, e) {
      var n = this;

      i.__super__.bind.apply(this, arguments);

      var s = t.id + "-container";
      this.$selection.find(".select2-selection__rendered").attr("id", s).attr("role", "textbox").attr("aria-readonly", "true"), this.$selection.attr("aria-labelledby", s), this.$selection.attr("aria-controls", s), this.$selection.on("mousedown", function (e) {
        1 === e.which && n.trigger("toggle", {
          originalEvent: e
        });
      }), this.$selection.on("focus", function (e) {}), this.$selection.on("blur", function (e) {}), t.on("focus", function (e) {
        t.isOpen() || n.$selection.trigger("focus");
      });
    }, i.prototype.clear = function () {
      var e = this.$selection.find(".select2-selection__rendered");
      e.empty(), e.removeAttr("title");
    }, i.prototype.display = function (e, t) {
      var n = this.options.get("templateSelection");
      return this.options.get("escapeMarkup")(n(e, t));
    }, i.prototype.selectionContainer = function () {
      return e("<span></span>");
    }, i.prototype.update = function (e) {
      var t, n;
      0 !== e.length ? (n = e[0], t = this.$selection.find(".select2-selection__rendered"), e = this.display(n, t), t.empty().append(e), (n = n.title || n.text) ? t.attr("title", n) : t.removeAttr("title")) : this.clear();
    }, i;
  }), u.define("select2/selection/multiple", ["jquery", "./base", "../utils"], function (i, e, c) {
    function r(e, t) {
      r.__super__.constructor.apply(this, arguments);
    }

    return c.Extend(r, e), r.prototype.render = function () {
      var e = r.__super__.render.call(this);

      return e[0].classList.add("select2-selection--multiple"), e.html('<ul class="select2-selection__rendered"></ul>'), e;
    }, r.prototype.bind = function (e, t) {
      var n = this;

      r.__super__.bind.apply(this, arguments);

      var s = e.id + "-container";
      this.$selection.find(".select2-selection__rendered").attr("id", s), this.$selection.on("click", function (e) {
        n.trigger("toggle", {
          originalEvent: e
        });
      }), this.$selection.on("click", ".select2-selection__choice__remove", function (e) {
        var t;
        n.isDisabled() || (t = i(this).parent(), t = c.GetData(t[0], "data"), n.trigger("unselect", {
          originalEvent: e,
          data: t
        }));
      }), this.$selection.on("keydown", ".select2-selection__choice__remove", function (e) {
        n.isDisabled() || e.stopPropagation();
      });
    }, r.prototype.clear = function () {
      var e = this.$selection.find(".select2-selection__rendered");
      e.empty(), e.removeAttr("title");
    }, r.prototype.display = function (e, t) {
      var n = this.options.get("templateSelection");
      return this.options.get("escapeMarkup")(n(e, t));
    }, r.prototype.selectionContainer = function () {
      return i('<li class="select2-selection__choice"><button type="button" class="select2-selection__choice__remove" tabindex="-1"><span aria-hidden="true">&times;</span></button><span class="select2-selection__choice__display"></span></li>');
    }, r.prototype.update = function (e) {
      if (this.clear(), 0 !== e.length) {
        for (var t = [], n = this.$selection.find(".select2-selection__rendered").attr("id") + "-choice-", s = 0; s < e.length; s++) {
          var i = e[s],
              r = this.selectionContainer(),
              o = this.display(i, r),
              a = n + c.generateChars(4) + "-";
          i.id ? a += i.id : a += c.generateChars(4), r.find(".select2-selection__choice__display").append(o).attr("id", a);
          var l = i.title || i.text;
          l && r.attr("title", l);
          o = this.options.get("translations").get("removeItem"), l = r.find(".select2-selection__choice__remove");
          l.attr("title", o()), l.attr("aria-label", o()), l.attr("aria-describedby", a), c.StoreData(r[0], "data", i), t.push(r);
        }

        this.$selection.find(".select2-selection__rendered").append(t);
      }
    }, r;
  }), u.define("select2/selection/placeholder", [], function () {
    function e(e, t, n) {
      this.placeholder = this.normalizePlaceholder(n.get("placeholder")), e.call(this, t, n);
    }

    return e.prototype.normalizePlaceholder = function (e, t) {
      return "string" == typeof t && (t = {
        id: "",
        text: t
      }), t;
    }, e.prototype.createPlaceholder = function (e, t) {
      var n = this.selectionContainer();
      n.html(this.display(t)), n[0].classList.add("select2-selection__placeholder"), n[0].classList.remove("select2-selection__choice");
      t = t.title || t.text || n.text();
      return this.$selection.find(".select2-selection__rendered").attr("title", t), n;
    }, e.prototype.update = function (e, t) {
      var n = 1 == t.length && t[0].id != this.placeholder.id;
      if (1 < t.length || n) return e.call(this, t);
      this.clear();
      t = this.createPlaceholder(this.placeholder);
      this.$selection.find(".select2-selection__rendered").append(t);
    }, e;
  }), u.define("select2/selection/allowClear", ["jquery", "../keys", "../utils"], function (i, s, a) {
    function e() {}

    return e.prototype.bind = function (e, t, n) {
      var s = this;
      e.call(this, t, n), null == this.placeholder && this.options.get("debug") && window.console && console.error && console.error("Select2: The `allowClear` option should be used in combination with the `placeholder` option."), this.$selection.on("mousedown", ".select2-selection__clear", function (e) {
        s._handleClear(e);
      }), t.on("keypress", function (e) {
        s._handleKeyboardClear(e, t);
      });
    }, e.prototype._handleClear = function (e, t) {
      if (!this.isDisabled()) {
        var n = this.$selection.find(".select2-selection__clear");

        if (0 !== n.length) {
          t.stopPropagation();
          var s = a.GetData(n[0], "data"),
              i = this.$element.val();
          this.$element.val(this.placeholder.id);
          var r = {
            data: s
          };
          if (this.trigger("clear", r), r.prevented) this.$element.val(i);else {
            for (var o = 0; o < s.length; o++) {
              if (r = {
                data: s[o]
              }, this.trigger("unselect", r), r.prevented) return void this.$element.val(i);
            }

            this.$element.trigger("input").trigger("change"), this.trigger("toggle", {});
          }
        }
      }
    }, e.prototype._handleKeyboardClear = function (e, t, n) {
      n.isOpen() || t.which != s.DELETE && t.which != s.BACKSPACE || this._handleClear(t);
    }, e.prototype.update = function (e, t) {
      var n, s;
      e.call(this, t), this.$selection.find(".select2-selection__clear").remove(), this.$selection[0].classList.remove("select2-selection--clearable"), 0 < this.$selection.find(".select2-selection__placeholder").length || 0 === t.length || (n = this.$selection.find(".select2-selection__rendered").attr("id"), s = this.options.get("translations").get("removeAllItems"), (e = i('<button type="button" class="select2-selection__clear" tabindex="-1"><span aria-hidden="true">&times;</span></button>')).attr("title", s()), e.attr("aria-label", s()), e.attr("aria-describedby", n), a.StoreData(e[0], "data", t), this.$selection.prepend(e), this.$selection[0].classList.add("select2-selection--clearable"));
    }, e;
  }), u.define("select2/selection/search", ["jquery", "../utils", "../keys"], function (s, a, l) {
    function e(e, t, n) {
      e.call(this, t, n);
    }

    return e.prototype.render = function (e) {
      var t = this.options.get("translations").get("search"),
          n = s('<span class="select2-search select2-search--inline"><textarea class="select2-search__field" type="search" tabindex="-1" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" ></textarea></span>');
      this.$searchContainer = n, this.$search = n.find("textarea"), this.$search.prop("autocomplete", this.options.get("autocomplete")), this.$search.attr("aria-label", t());
      e = e.call(this);
      return this._transferTabIndex(), e.append(this.$searchContainer), e;
    }, e.prototype.bind = function (e, t, n) {
      var s = this,
          i = t.id + "-results",
          r = t.id + "-container";
      e.call(this, t, n), s.$search.attr("aria-describedby", r), t.on("open", function () {
        s.$search.attr("aria-controls", i), s.$search.trigger("focus");
      }), t.on("close", function () {
        s.$search.val(""), s.resizeSearch(), s.$search.removeAttr("aria-controls"), s.$search.removeAttr("aria-activedescendant"), s.$search.trigger("focus");
      }), t.on("enable", function () {
        s.$search.prop("disabled", !1), s._transferTabIndex();
      }), t.on("disable", function () {
        s.$search.prop("disabled", !0);
      }), t.on("focus", function (e) {
        s.$search.trigger("focus");
      }), t.on("results:focus", function (e) {
        e.data._resultId ? s.$search.attr("aria-activedescendant", e.data._resultId) : s.$search.removeAttr("aria-activedescendant");
      }), this.$selection.on("focusin", ".select2-search--inline", function (e) {
        s.trigger("focus", e);
      }), this.$selection.on("focusout", ".select2-search--inline", function (e) {
        s._handleBlur(e);
      }), this.$selection.on("keydown", ".select2-search--inline", function (e) {
        var t;
        e.stopPropagation(), s.trigger("keypress", e), s._keyUpPrevented = e.isDefaultPrevented(), e.which !== l.BACKSPACE || "" !== s.$search.val() || 0 < (t = s.$selection.find(".select2-selection__choice").last()).length && (t = a.GetData(t[0], "data"), s.searchRemoveChoice(t), e.preventDefault());
      }), this.$selection.on("click", ".select2-search--inline", function (e) {
        s.$search.val() && e.stopPropagation();
      });
      var t = document.documentMode,
          o = t && t <= 11;
      this.$selection.on("input.searchcheck", ".select2-search--inline", function (e) {
        o ? s.$selection.off("input.search input.searchcheck") : s.$selection.off("keyup.search");
      }), this.$selection.on("keyup.search input.search", ".select2-search--inline", function (e) {
        var t;
        o && "input" === e.type ? s.$selection.off("input.search input.searchcheck") : (t = e.which) != l.SHIFT && t != l.CTRL && t != l.ALT && t != l.TAB && s.handleSearch(e);
      });
    }, e.prototype._transferTabIndex = function (e) {
      this.$search.attr("tabindex", this.$selection.attr("tabindex")), this.$selection.attr("tabindex", "-1");
    }, e.prototype.createPlaceholder = function (e, t) {
      this.$search.attr("placeholder", t.text);
    }, e.prototype.update = function (e, t) {
      var n = this.$search[0] == document.activeElement;
      this.$search.attr("placeholder", ""), e.call(this, t), this.resizeSearch(), n && this.$search.trigger("focus");
    }, e.prototype.handleSearch = function () {
      var e;
      this.resizeSearch(), this._keyUpPrevented || (e = this.$search.val(), this.trigger("query", {
        term: e
      })), this._keyUpPrevented = !1;
    }, e.prototype.searchRemoveChoice = function (e, t) {
      this.trigger("unselect", {
        data: t
      }), this.$search.val(t.text), this.handleSearch();
    }, e.prototype.resizeSearch = function () {
      this.$search.css("width", "25px");
      var e = "100%";
      "" === this.$search.attr("placeholder") && (e = .75 * (this.$search.val().length + 1) + "em"), this.$search.css("width", e);
    }, e;
  }), u.define("select2/selection/selectionCss", ["../utils"], function (n) {
    function e() {}

    return e.prototype.render = function (e) {
      var t = e.call(this),
          e = this.options.get("selectionCssClass") || "";
      return -1 !== e.indexOf(":all:") && (e = e.replace(":all:", ""), n.copyNonInternalCssClasses(t[0], this.$element[0])), t.addClass(e), t;
    }, e;
  }), u.define("select2/selection/eventRelay", ["jquery"], function (o) {
    function e() {}

    return e.prototype.bind = function (e, t, n) {
      var s = this,
          i = ["open", "opening", "close", "closing", "select", "selecting", "unselect", "unselecting", "clear", "clearing"],
          r = ["opening", "closing", "selecting", "unselecting", "clearing"];
      e.call(this, t, n), t.on("*", function (e, t) {
        var n;
        -1 !== i.indexOf(e) && (t = t || {}, n = o.Event("select2:" + e, {
          params: t
        }), s.$element.trigger(n), -1 !== r.indexOf(e) && (t.prevented = n.isDefaultPrevented()));
      });
    }, e;
  }), u.define("select2/translation", ["jquery", "require"], function (t, n) {
    function s(e) {
      this.dict = e || {};
    }

    return s.prototype.all = function () {
      return this.dict;
    }, s.prototype.get = function (e) {
      return this.dict[e];
    }, s.prototype.extend = function (e) {
      this.dict = t.extend({}, e.all(), this.dict);
    }, s._cache = {}, s.loadPath = function (e) {
      var t;
      return e in s._cache || (t = n(e), s._cache[e] = t), new s(s._cache[e]);
    }, s;
  }), u.define("select2/diacritics", [], function () {
    return {
      "Ⓐ": "A",
      "Ａ": "A",
      "À": "A",
      "Á": "A",
      "Â": "A",
      "Ầ": "A",
      "Ấ": "A",
      "Ẫ": "A",
      "Ẩ": "A",
      "Ã": "A",
      "Ā": "A",
      "Ă": "A",
      "Ằ": "A",
      "Ắ": "A",
      "Ẵ": "A",
      "Ẳ": "A",
      "Ȧ": "A",
      "Ǡ": "A",
      "Ä": "A",
      "Ǟ": "A",
      "Ả": "A",
      "Å": "A",
      "Ǻ": "A",
      "Ǎ": "A",
      "Ȁ": "A",
      "Ȃ": "A",
      "Ạ": "A",
      "Ậ": "A",
      "Ặ": "A",
      "Ḁ": "A",
      "Ą": "A",
      "Ⱥ": "A",
      "Ɐ": "A",
      "Ꜳ": "AA",
      "Æ": "AE",
      "Ǽ": "AE",
      "Ǣ": "AE",
      "Ꜵ": "AO",
      "Ꜷ": "AU",
      "Ꜹ": "AV",
      "Ꜻ": "AV",
      "Ꜽ": "AY",
      "Ⓑ": "B",
      "Ｂ": "B",
      "Ḃ": "B",
      "Ḅ": "B",
      "Ḇ": "B",
      "Ƀ": "B",
      "Ƃ": "B",
      "Ɓ": "B",
      "Ⓒ": "C",
      "Ｃ": "C",
      "Ć": "C",
      "Ĉ": "C",
      "Ċ": "C",
      "Č": "C",
      "Ç": "C",
      "Ḉ": "C",
      "Ƈ": "C",
      "Ȼ": "C",
      "Ꜿ": "C",
      "Ⓓ": "D",
      "Ｄ": "D",
      "Ḋ": "D",
      "Ď": "D",
      "Ḍ": "D",
      "Ḑ": "D",
      "Ḓ": "D",
      "Ḏ": "D",
      "Đ": "D",
      "Ƌ": "D",
      "Ɗ": "D",
      "Ɖ": "D",
      "Ꝺ": "D",
      "Ǳ": "DZ",
      "Ǆ": "DZ",
      "ǲ": "Dz",
      "ǅ": "Dz",
      "Ⓔ": "E",
      "Ｅ": "E",
      "È": "E",
      "É": "E",
      "Ê": "E",
      "Ề": "E",
      "Ế": "E",
      "Ễ": "E",
      "Ể": "E",
      "Ẽ": "E",
      "Ē": "E",
      "Ḕ": "E",
      "Ḗ": "E",
      "Ĕ": "E",
      "Ė": "E",
      "Ë": "E",
      "Ẻ": "E",
      "Ě": "E",
      "Ȅ": "E",
      "Ȇ": "E",
      "Ẹ": "E",
      "Ệ": "E",
      "Ȩ": "E",
      "Ḝ": "E",
      "Ę": "E",
      "Ḙ": "E",
      "Ḛ": "E",
      "Ɛ": "E",
      "Ǝ": "E",
      "Ⓕ": "F",
      "Ｆ": "F",
      "Ḟ": "F",
      "Ƒ": "F",
      "Ꝼ": "F",
      "Ⓖ": "G",
      "Ｇ": "G",
      "Ǵ": "G",
      "Ĝ": "G",
      "Ḡ": "G",
      "Ğ": "G",
      "Ġ": "G",
      "Ǧ": "G",
      "Ģ": "G",
      "Ǥ": "G",
      "Ɠ": "G",
      "Ꞡ": "G",
      "Ᵹ": "G",
      "Ꝿ": "G",
      "Ⓗ": "H",
      "Ｈ": "H",
      "Ĥ": "H",
      "Ḣ": "H",
      "Ḧ": "H",
      "Ȟ": "H",
      "Ḥ": "H",
      "Ḩ": "H",
      "Ḫ": "H",
      "Ħ": "H",
      "Ⱨ": "H",
      "Ⱶ": "H",
      "Ɥ": "H",
      "Ⓘ": "I",
      "Ｉ": "I",
      "Ì": "I",
      "Í": "I",
      "Î": "I",
      "Ĩ": "I",
      "Ī": "I",
      "Ĭ": "I",
      "İ": "I",
      "Ï": "I",
      "Ḯ": "I",
      "Ỉ": "I",
      "Ǐ": "I",
      "Ȉ": "I",
      "Ȋ": "I",
      "Ị": "I",
      "Į": "I",
      "Ḭ": "I",
      "Ɨ": "I",
      "Ⓙ": "J",
      "Ｊ": "J",
      "Ĵ": "J",
      "Ɉ": "J",
      "Ⓚ": "K",
      "Ｋ": "K",
      "Ḱ": "K",
      "Ǩ": "K",
      "Ḳ": "K",
      "Ķ": "K",
      "Ḵ": "K",
      "Ƙ": "K",
      "Ⱪ": "K",
      "Ꝁ": "K",
      "Ꝃ": "K",
      "Ꝅ": "K",
      "Ꞣ": "K",
      "Ⓛ": "L",
      "Ｌ": "L",
      "Ŀ": "L",
      "Ĺ": "L",
      "Ľ": "L",
      "Ḷ": "L",
      "Ḹ": "L",
      "Ļ": "L",
      "Ḽ": "L",
      "Ḻ": "L",
      "Ł": "L",
      "Ƚ": "L",
      "Ɫ": "L",
      "Ⱡ": "L",
      "Ꝉ": "L",
      "Ꝇ": "L",
      "Ꞁ": "L",
      "Ǉ": "LJ",
      "ǈ": "Lj",
      "Ⓜ": "M",
      "Ｍ": "M",
      "Ḿ": "M",
      "Ṁ": "M",
      "Ṃ": "M",
      "Ɱ": "M",
      "Ɯ": "M",
      "Ⓝ": "N",
      "Ｎ": "N",
      "Ǹ": "N",
      "Ń": "N",
      "Ñ": "N",
      "Ṅ": "N",
      "Ň": "N",
      "Ṇ": "N",
      "Ņ": "N",
      "Ṋ": "N",
      "Ṉ": "N",
      "Ƞ": "N",
      "Ɲ": "N",
      "Ꞑ": "N",
      "Ꞥ": "N",
      "Ǌ": "NJ",
      "ǋ": "Nj",
      "Ⓞ": "O",
      "Ｏ": "O",
      "Ò": "O",
      "Ó": "O",
      "Ô": "O",
      "Ồ": "O",
      "Ố": "O",
      "Ỗ": "O",
      "Ổ": "O",
      "Õ": "O",
      "Ṍ": "O",
      "Ȭ": "O",
      "Ṏ": "O",
      "Ō": "O",
      "Ṑ": "O",
      "Ṓ": "O",
      "Ŏ": "O",
      "Ȯ": "O",
      "Ȱ": "O",
      "Ö": "O",
      "Ȫ": "O",
      "Ỏ": "O",
      "Ő": "O",
      "Ǒ": "O",
      "Ȍ": "O",
      "Ȏ": "O",
      "Ơ": "O",
      "Ờ": "O",
      "Ớ": "O",
      "Ỡ": "O",
      "Ở": "O",
      "Ợ": "O",
      "Ọ": "O",
      "Ộ": "O",
      "Ǫ": "O",
      "Ǭ": "O",
      "Ø": "O",
      "Ǿ": "O",
      "Ɔ": "O",
      "Ɵ": "O",
      "Ꝋ": "O",
      "Ꝍ": "O",
      "Œ": "OE",
      "Ƣ": "OI",
      "Ꝏ": "OO",
      "Ȣ": "OU",
      "Ⓟ": "P",
      "Ｐ": "P",
      "Ṕ": "P",
      "Ṗ": "P",
      "Ƥ": "P",
      "Ᵽ": "P",
      "Ꝑ": "P",
      "Ꝓ": "P",
      "Ꝕ": "P",
      "Ⓠ": "Q",
      "Ｑ": "Q",
      "Ꝗ": "Q",
      "Ꝙ": "Q",
      "Ɋ": "Q",
      "Ⓡ": "R",
      "Ｒ": "R",
      "Ŕ": "R",
      "Ṙ": "R",
      "Ř": "R",
      "Ȑ": "R",
      "Ȓ": "R",
      "Ṛ": "R",
      "Ṝ": "R",
      "Ŗ": "R",
      "Ṟ": "R",
      "Ɍ": "R",
      "Ɽ": "R",
      "Ꝛ": "R",
      "Ꞧ": "R",
      "Ꞃ": "R",
      "Ⓢ": "S",
      "Ｓ": "S",
      "ẞ": "S",
      "Ś": "S",
      "Ṥ": "S",
      "Ŝ": "S",
      "Ṡ": "S",
      "Š": "S",
      "Ṧ": "S",
      "Ṣ": "S",
      "Ṩ": "S",
      "Ș": "S",
      "Ş": "S",
      "Ȿ": "S",
      "Ꞩ": "S",
      "Ꞅ": "S",
      "Ⓣ": "T",
      "Ｔ": "T",
      "Ṫ": "T",
      "Ť": "T",
      "Ṭ": "T",
      "Ț": "T",
      "Ţ": "T",
      "Ṱ": "T",
      "Ṯ": "T",
      "Ŧ": "T",
      "Ƭ": "T",
      "Ʈ": "T",
      "Ⱦ": "T",
      "Ꞇ": "T",
      "Ꜩ": "TZ",
      "Ⓤ": "U",
      "Ｕ": "U",
      "Ù": "U",
      "Ú": "U",
      "Û": "U",
      "Ũ": "U",
      "Ṹ": "U",
      "Ū": "U",
      "Ṻ": "U",
      "Ŭ": "U",
      "Ü": "U",
      "Ǜ": "U",
      "Ǘ": "U",
      "Ǖ": "U",
      "Ǚ": "U",
      "Ủ": "U",
      "Ů": "U",
      "Ű": "U",
      "Ǔ": "U",
      "Ȕ": "U",
      "Ȗ": "U",
      "Ư": "U",
      "Ừ": "U",
      "Ứ": "U",
      "Ữ": "U",
      "Ử": "U",
      "Ự": "U",
      "Ụ": "U",
      "Ṳ": "U",
      "Ų": "U",
      "Ṷ": "U",
      "Ṵ": "U",
      "Ʉ": "U",
      "Ⓥ": "V",
      "Ｖ": "V",
      "Ṽ": "V",
      "Ṿ": "V",
      "Ʋ": "V",
      "Ꝟ": "V",
      "Ʌ": "V",
      "Ꝡ": "VY",
      "Ⓦ": "W",
      "Ｗ": "W",
      "Ẁ": "W",
      "Ẃ": "W",
      "Ŵ": "W",
      "Ẇ": "W",
      "Ẅ": "W",
      "Ẉ": "W",
      "Ⱳ": "W",
      "Ⓧ": "X",
      "Ｘ": "X",
      "Ẋ": "X",
      "Ẍ": "X",
      "Ⓨ": "Y",
      "Ｙ": "Y",
      "Ỳ": "Y",
      "Ý": "Y",
      "Ŷ": "Y",
      "Ỹ": "Y",
      "Ȳ": "Y",
      "Ẏ": "Y",
      "Ÿ": "Y",
      "Ỷ": "Y",
      "Ỵ": "Y",
      "Ƴ": "Y",
      "Ɏ": "Y",
      "Ỿ": "Y",
      "Ⓩ": "Z",
      "Ｚ": "Z",
      "Ź": "Z",
      "Ẑ": "Z",
      "Ż": "Z",
      "Ž": "Z",
      "Ẓ": "Z",
      "Ẕ": "Z",
      "Ƶ": "Z",
      "Ȥ": "Z",
      "Ɀ": "Z",
      "Ⱬ": "Z",
      "Ꝣ": "Z",
      "ⓐ": "a",
      "ａ": "a",
      "ẚ": "a",
      "à": "a",
      "á": "a",
      "â": "a",
      "ầ": "a",
      "ấ": "a",
      "ẫ": "a",
      "ẩ": "a",
      "ã": "a",
      "ā": "a",
      "ă": "a",
      "ằ": "a",
      "ắ": "a",
      "ẵ": "a",
      "ẳ": "a",
      "ȧ": "a",
      "ǡ": "a",
      "ä": "a",
      "ǟ": "a",
      "ả": "a",
      "å": "a",
      "ǻ": "a",
      "ǎ": "a",
      "ȁ": "a",
      "ȃ": "a",
      "ạ": "a",
      "ậ": "a",
      "ặ": "a",
      "ḁ": "a",
      "ą": "a",
      "ⱥ": "a",
      "ɐ": "a",
      "ꜳ": "aa",
      "æ": "ae",
      "ǽ": "ae",
      "ǣ": "ae",
      "ꜵ": "ao",
      "ꜷ": "au",
      "ꜹ": "av",
      "ꜻ": "av",
      "ꜽ": "ay",
      "ⓑ": "b",
      "ｂ": "b",
      "ḃ": "b",
      "ḅ": "b",
      "ḇ": "b",
      "ƀ": "b",
      "ƃ": "b",
      "ɓ": "b",
      "ⓒ": "c",
      "ｃ": "c",
      "ć": "c",
      "ĉ": "c",
      "ċ": "c",
      "č": "c",
      "ç": "c",
      "ḉ": "c",
      "ƈ": "c",
      "ȼ": "c",
      "ꜿ": "c",
      "ↄ": "c",
      "ⓓ": "d",
      "ｄ": "d",
      "ḋ": "d",
      "ď": "d",
      "ḍ": "d",
      "ḑ": "d",
      "ḓ": "d",
      "ḏ": "d",
      "đ": "d",
      "ƌ": "d",
      "ɖ": "d",
      "ɗ": "d",
      "ꝺ": "d",
      "ǳ": "dz",
      "ǆ": "dz",
      "ⓔ": "e",
      "ｅ": "e",
      "è": "e",
      "é": "e",
      "ê": "e",
      "ề": "e",
      "ế": "e",
      "ễ": "e",
      "ể": "e",
      "ẽ": "e",
      "ē": "e",
      "ḕ": "e",
      "ḗ": "e",
      "ĕ": "e",
      "ė": "e",
      "ë": "e",
      "ẻ": "e",
      "ě": "e",
      "ȅ": "e",
      "ȇ": "e",
      "ẹ": "e",
      "ệ": "e",
      "ȩ": "e",
      "ḝ": "e",
      "ę": "e",
      "ḙ": "e",
      "ḛ": "e",
      "ɇ": "e",
      "ɛ": "e",
      "ǝ": "e",
      "ⓕ": "f",
      "ｆ": "f",
      "ḟ": "f",
      "ƒ": "f",
      "ꝼ": "f",
      "ⓖ": "g",
      "ｇ": "g",
      "ǵ": "g",
      "ĝ": "g",
      "ḡ": "g",
      "ğ": "g",
      "ġ": "g",
      "ǧ": "g",
      "ģ": "g",
      "ǥ": "g",
      "ɠ": "g",
      "ꞡ": "g",
      "ᵹ": "g",
      "ꝿ": "g",
      "ⓗ": "h",
      "ｈ": "h",
      "ĥ": "h",
      "ḣ": "h",
      "ḧ": "h",
      "ȟ": "h",
      "ḥ": "h",
      "ḩ": "h",
      "ḫ": "h",
      "ẖ": "h",
      "ħ": "h",
      "ⱨ": "h",
      "ⱶ": "h",
      "ɥ": "h",
      "ƕ": "hv",
      "ⓘ": "i",
      "ｉ": "i",
      "ì": "i",
      "í": "i",
      "î": "i",
      "ĩ": "i",
      "ī": "i",
      "ĭ": "i",
      "ï": "i",
      "ḯ": "i",
      "ỉ": "i",
      "ǐ": "i",
      "ȉ": "i",
      "ȋ": "i",
      "ị": "i",
      "į": "i",
      "ḭ": "i",
      "ɨ": "i",
      "ı": "i",
      "ⓙ": "j",
      "ｊ": "j",
      "ĵ": "j",
      "ǰ": "j",
      "ɉ": "j",
      "ⓚ": "k",
      "ｋ": "k",
      "ḱ": "k",
      "ǩ": "k",
      "ḳ": "k",
      "ķ": "k",
      "ḵ": "k",
      "ƙ": "k",
      "ⱪ": "k",
      "ꝁ": "k",
      "ꝃ": "k",
      "ꝅ": "k",
      "ꞣ": "k",
      "ⓛ": "l",
      "ｌ": "l",
      "ŀ": "l",
      "ĺ": "l",
      "ľ": "l",
      "ḷ": "l",
      "ḹ": "l",
      "ļ": "l",
      "ḽ": "l",
      "ḻ": "l",
      "ſ": "l",
      "ł": "l",
      "ƚ": "l",
      "ɫ": "l",
      "ⱡ": "l",
      "ꝉ": "l",
      "ꞁ": "l",
      "ꝇ": "l",
      "ǉ": "lj",
      "ⓜ": "m",
      "ｍ": "m",
      "ḿ": "m",
      "ṁ": "m",
      "ṃ": "m",
      "ɱ": "m",
      "ɯ": "m",
      "ⓝ": "n",
      "ｎ": "n",
      "ǹ": "n",
      "ń": "n",
      "ñ": "n",
      "ṅ": "n",
      "ň": "n",
      "ṇ": "n",
      "ņ": "n",
      "ṋ": "n",
      "ṉ": "n",
      "ƞ": "n",
      "ɲ": "n",
      "ŉ": "n",
      "ꞑ": "n",
      "ꞥ": "n",
      "ǌ": "nj",
      "ⓞ": "o",
      "ｏ": "o",
      "ò": "o",
      "ó": "o",
      "ô": "o",
      "ồ": "o",
      "ố": "o",
      "ỗ": "o",
      "ổ": "o",
      "õ": "o",
      "ṍ": "o",
      "ȭ": "o",
      "ṏ": "o",
      "ō": "o",
      "ṑ": "o",
      "ṓ": "o",
      "ŏ": "o",
      "ȯ": "o",
      "ȱ": "o",
      "ö": "o",
      "ȫ": "o",
      "ỏ": "o",
      "ő": "o",
      "ǒ": "o",
      "ȍ": "o",
      "ȏ": "o",
      "ơ": "o",
      "ờ": "o",
      "ớ": "o",
      "ỡ": "o",
      "ở": "o",
      "ợ": "o",
      "ọ": "o",
      "ộ": "o",
      "ǫ": "o",
      "ǭ": "o",
      "ø": "o",
      "ǿ": "o",
      "ɔ": "o",
      "ꝋ": "o",
      "ꝍ": "o",
      "ɵ": "o",
      "œ": "oe",
      "ƣ": "oi",
      "ȣ": "ou",
      "ꝏ": "oo",
      "ⓟ": "p",
      "ｐ": "p",
      "ṕ": "p",
      "ṗ": "p",
      "ƥ": "p",
      "ᵽ": "p",
      "ꝑ": "p",
      "ꝓ": "p",
      "ꝕ": "p",
      "ⓠ": "q",
      "ｑ": "q",
      "ɋ": "q",
      "ꝗ": "q",
      "ꝙ": "q",
      "ⓡ": "r",
      "ｒ": "r",
      "ŕ": "r",
      "ṙ": "r",
      "ř": "r",
      "ȑ": "r",
      "ȓ": "r",
      "ṛ": "r",
      "ṝ": "r",
      "ŗ": "r",
      "ṟ": "r",
      "ɍ": "r",
      "ɽ": "r",
      "ꝛ": "r",
      "ꞧ": "r",
      "ꞃ": "r",
      "ⓢ": "s",
      "ｓ": "s",
      "ß": "s",
      "ś": "s",
      "ṥ": "s",
      "ŝ": "s",
      "ṡ": "s",
      "š": "s",
      "ṧ": "s",
      "ṣ": "s",
      "ṩ": "s",
      "ș": "s",
      "ş": "s",
      "ȿ": "s",
      "ꞩ": "s",
      "ꞅ": "s",
      "ẛ": "s",
      "ⓣ": "t",
      "ｔ": "t",
      "ṫ": "t",
      "ẗ": "t",
      "ť": "t",
      "ṭ": "t",
      "ț": "t",
      "ţ": "t",
      "ṱ": "t",
      "ṯ": "t",
      "ŧ": "t",
      "ƭ": "t",
      "ʈ": "t",
      "ⱦ": "t",
      "ꞇ": "t",
      "ꜩ": "tz",
      "ⓤ": "u",
      "ｕ": "u",
      "ù": "u",
      "ú": "u",
      "û": "u",
      "ũ": "u",
      "ṹ": "u",
      "ū": "u",
      "ṻ": "u",
      "ŭ": "u",
      "ü": "u",
      "ǜ": "u",
      "ǘ": "u",
      "ǖ": "u",
      "ǚ": "u",
      "ủ": "u",
      "ů": "u",
      "ű": "u",
      "ǔ": "u",
      "ȕ": "u",
      "ȗ": "u",
      "ư": "u",
      "ừ": "u",
      "ứ": "u",
      "ữ": "u",
      "ử": "u",
      "ự": "u",
      "ụ": "u",
      "ṳ": "u",
      "ų": "u",
      "ṷ": "u",
      "ṵ": "u",
      "ʉ": "u",
      "ⓥ": "v",
      "ｖ": "v",
      "ṽ": "v",
      "ṿ": "v",
      "ʋ": "v",
      "ꝟ": "v",
      "ʌ": "v",
      "ꝡ": "vy",
      "ⓦ": "w",
      "ｗ": "w",
      "ẁ": "w",
      "ẃ": "w",
      "ŵ": "w",
      "ẇ": "w",
      "ẅ": "w",
      "ẘ": "w",
      "ẉ": "w",
      "ⱳ": "w",
      "ⓧ": "x",
      "ｘ": "x",
      "ẋ": "x",
      "ẍ": "x",
      "ⓨ": "y",
      "ｙ": "y",
      "ỳ": "y",
      "ý": "y",
      "ŷ": "y",
      "ỹ": "y",
      "ȳ": "y",
      "ẏ": "y",
      "ÿ": "y",
      "ỷ": "y",
      "ẙ": "y",
      "ỵ": "y",
      "ƴ": "y",
      "ɏ": "y",
      "ỿ": "y",
      "ⓩ": "z",
      "ｚ": "z",
      "ź": "z",
      "ẑ": "z",
      "ż": "z",
      "ž": "z",
      "ẓ": "z",
      "ẕ": "z",
      "ƶ": "z",
      "ȥ": "z",
      "ɀ": "z",
      "ⱬ": "z",
      "ꝣ": "z",
      "Ά": "Α",
      "Έ": "Ε",
      "Ή": "Η",
      "Ί": "Ι",
      "Ϊ": "Ι",
      "Ό": "Ο",
      "Ύ": "Υ",
      "Ϋ": "Υ",
      "Ώ": "Ω",
      "ά": "α",
      "έ": "ε",
      "ή": "η",
      "ί": "ι",
      "ϊ": "ι",
      "ΐ": "ι",
      "ό": "ο",
      "ύ": "υ",
      "ϋ": "υ",
      "ΰ": "υ",
      "ώ": "ω",
      "ς": "σ",
      "’": "'"
    };
  }), u.define("select2/data/base", ["../utils"], function (n) {
    function s(e, t) {
      s.__super__.constructor.call(this);
    }

    return n.Extend(s, n.Observable), s.prototype.current = function (e) {
      throw new Error("The `current` method must be defined in child classes.");
    }, s.prototype.query = function (e, t) {
      throw new Error("The `query` method must be defined in child classes.");
    }, s.prototype.bind = function (e, t) {}, s.prototype.destroy = function () {}, s.prototype.generateResultId = function (e, t) {
      e = e.id + "-result-";
      return e += n.generateChars(4), null != t.id ? e += "-" + t.id.toString() : e += "-" + n.generateChars(4), e;
    }, s;
  }), u.define("select2/data/select", ["./base", "../utils", "jquery"], function (e, a, l) {
    function n(e, t) {
      this.$element = e, this.options = t, n.__super__.constructor.call(this);
    }

    return a.Extend(n, e), n.prototype.current = function (e) {
      var t = this;
      e(Array.prototype.map.call(this.$element[0].querySelectorAll(":checked"), function (e) {
        return t.item(l(e));
      }));
    }, n.prototype.select = function (i) {
      var e,
          r = this;
      if (i.selected = !0, null != i.element && "option" === i.element.tagName.toLowerCase()) return i.element.selected = !0, void this.$element.trigger("input").trigger("change");
      this.$element.prop("multiple") ? this.current(function (e) {
        var t = [];
        (i = [i]).push.apply(i, e);

        for (var n = 0; n < i.length; n++) {
          var s = i[n].id;
          -1 === t.indexOf(s) && t.push(s);
        }

        r.$element.val(t), r.$element.trigger("input").trigger("change");
      }) : (e = i.id, this.$element.val(e), this.$element.trigger("input").trigger("change"));
    }, n.prototype.unselect = function (i) {
      var r = this;

      if (this.$element.prop("multiple")) {
        if (i.selected = !1, null != i.element && "option" === i.element.tagName.toLowerCase()) return i.element.selected = !1, void this.$element.trigger("input").trigger("change");
        this.current(function (e) {
          for (var t = [], n = 0; n < e.length; n++) {
            var s = e[n].id;
            s !== i.id && -1 === t.indexOf(s) && t.push(s);
          }

          r.$element.val(t), r.$element.trigger("input").trigger("change");
        });
      }
    }, n.prototype.bind = function (e, t) {
      var n = this;
      (this.container = e).on("select", function (e) {
        n.select(e.data);
      }), e.on("unselect", function (e) {
        n.unselect(e.data);
      });
    }, n.prototype.destroy = function () {
      this.$element.find("*").each(function () {
        a.RemoveData(this);
      });
    }, n.prototype.query = function (t, e) {
      var n = [],
          s = this;
      this.$element.children().each(function () {
        var e;
        "option" !== this.tagName.toLowerCase() && "optgroup" !== this.tagName.toLowerCase() || (e = l(this), e = s.item(e), null !== (e = s.matches(t, e)) && n.push(e));
      }), e({
        results: n
      });
    }, n.prototype.addOptions = function (e) {
      this.$element.append(e);
    }, n.prototype.option = function (e) {
      var t;
      e.children ? (t = document.createElement("optgroup")).label = e.text : void 0 !== (t = document.createElement("option")).textContent ? t.textContent = e.text : t.innerText = e.text, void 0 !== e.id && (t.value = e.id), e.disabled && (t.disabled = !0), e.selected && (t.selected = !0), e.title && (t.title = e.title);
      e = this._normalizeItem(e);
      return e.element = t, a.StoreData(t, "data", e), l(t);
    }, n.prototype.item = function (e) {
      var t = {};
      if (null != (t = a.GetData(e[0], "data"))) return t;
      var n = e[0];
      if ("option" === n.tagName.toLowerCase()) t = {
        id: e.val(),
        text: e.text(),
        disabled: e.prop("disabled"),
        selected: e.prop("selected"),
        title: e.prop("title")
      };else if ("optgroup" === n.tagName.toLowerCase()) {
        t = {
          text: e.prop("label"),
          children: [],
          title: e.prop("title")
        };

        for (var s = e.children("option"), i = [], r = 0; r < s.length; r++) {
          var o = l(s[r]),
              o = this.item(o);
          i.push(o);
        }

        t.children = i;
      }
      return (t = this._normalizeItem(t)).element = e[0], a.StoreData(e[0], "data", t), t;
    }, n.prototype._normalizeItem = function (e) {
      e !== Object(e) && (e = {
        id: e,
        text: e
      });
      return null != (e = l.extend({}, {
        text: ""
      }, e)).id && (e.id = e.id.toString()), null != e.text && (e.text = e.text.toString()), null == e._resultId && e.id && null != this.container && (e._resultId = this.generateResultId(this.container, e)), l.extend({}, {
        selected: !1,
        disabled: !1
      }, e);
    }, n.prototype.matches = function (e, t) {
      return this.options.get("matcher")(e, t);
    }, n;
  }), u.define("select2/data/array", ["./select", "../utils", "jquery"], function (e, t, c) {
    function s(e, t) {
      this._dataToConvert = t.get("data") || [], s.__super__.constructor.call(this, e, t);
    }

    return t.Extend(s, e), s.prototype.bind = function (e, t) {
      s.__super__.bind.call(this, e, t), this.addOptions(this.convertToOptions(this._dataToConvert));
    }, s.prototype.select = function (n) {
      var e = this.$element.find("option").filter(function (e, t) {
        return t.value == n.id.toString();
      });
      0 === e.length && (e = this.option(n), this.addOptions(e)), s.__super__.select.call(this, n);
    }, s.prototype.convertToOptions = function (e) {
      var t = this,
          n = this.$element.find("option"),
          s = n.map(function () {
        return t.item(c(this)).id;
      }).get(),
          i = [];

      for (var r = 0; r < e.length; r++) {
        var o,
            a,
            l = this._normalizeItem(e[r]);

        0 <= s.indexOf(l.id) ? (o = n.filter(function (e) {
          return function () {
            return c(this).val() == e.id;
          };
        }(l)), a = this.item(o), a = c.extend(!0, {}, l, a), a = this.option(a), o.replaceWith(a)) : (a = this.option(l), l.children && (l = this.convertToOptions(l.children), a.append(l)), i.push(a));
      }

      return i;
    }, s;
  }), u.define("select2/data/ajax", ["./array", "../utils", "jquery"], function (e, t, r) {
    function n(e, t) {
      this.ajaxOptions = this._applyDefaults(t.get("ajax")), null != this.ajaxOptions.processResults && (this.processResults = this.ajaxOptions.processResults), n.__super__.constructor.call(this, e, t);
    }

    return t.Extend(n, e), n.prototype._applyDefaults = function (e) {
      var t = {
        data: function data(e) {
          return r.extend({}, e, {
            q: e.term
          });
        },
        transport: function transport(e, t, n) {
          e = r.ajax(e);
          return e.then(t), e.fail(n), e;
        }
      };
      return r.extend({}, t, e, !0);
    }, n.prototype.processResults = function (e) {
      return e;
    }, n.prototype.query = function (t, n) {
      var s = this;
      null != this._request && ("function" == typeof this._request.abort && this._request.abort(), this._request = null);
      var i = r.extend({
        type: "GET"
      }, this.ajaxOptions);

      function e() {
        var e = i.transport(i, function (e) {
          e = s.processResults(e, t);
          s.options.get("debug") && window.console && console.error && (e && e.results && Array.isArray(e.results) || console.error("Select2: The AJAX results did not return an array in the `results` key of the response.")), n(e);
        }, function () {
          "status" in e && (0 === e.status || "0" === e.status) || s.trigger("results:message", {
            message: "errorLoading"
          });
        });
        s._request = e;
      }

      "function" == typeof i.url && (i.url = i.url.call(this.$element, t)), "function" == typeof i.data && (i.data = i.data.call(this.$element, t)), this.ajaxOptions.delay && null != t.term ? (this._queryTimeout && window.clearTimeout(this._queryTimeout), this._queryTimeout = window.setTimeout(e, this.ajaxOptions.delay)) : e();
    }, n;
  }), u.define("select2/data/tags", ["jquery"], function (t) {
    function e(e, t, n) {
      var s = n.get("tags"),
          i = n.get("createTag");
      void 0 !== i && (this.createTag = i);
      i = n.get("insertTag");
      if (void 0 !== i && (this.insertTag = i), e.call(this, t, n), Array.isArray(s)) for (var r = 0; r < s.length; r++) {
        var o = s[r],
            o = this._normalizeItem(o),
            o = this.option(o);

        this.$element.append(o);
      }
    }

    return e.prototype.query = function (e, c, u) {
      var d = this;
      this._removeOldTags(), null != c.term && null == c.page ? e.call(this, c, function e(t, n) {
        for (var s = t.results, i = 0; i < s.length; i++) {
          var r = s[i],
              o = null != r.children && !e({
            results: r.children
          }, !0);
          if ((r.text || "").toUpperCase() === (c.term || "").toUpperCase() || o) return !n && (t.data = s, void u(t));
        }

        if (n) return !0;
        var a,
            l = d.createTag(c);
        null != l && ((a = d.option(l)).attr("data-select2-tag", "true"), d.addOptions([a]), d.insertTag(s, l)), t.results = s, u(t);
      }) : e.call(this, c, u);
    }, e.prototype.createTag = function (e, t) {
      if (null == t.term) return null;
      t = t.term.trim();
      return "" === t ? null : {
        id: t,
        text: t
      };
    }, e.prototype.insertTag = function (e, t, n) {
      t.unshift(n);
    }, e.prototype._removeOldTags = function (e) {
      this.$element.find("option[data-select2-tag]").each(function () {
        this.selected || t(this).remove();
      });
    }, e;
  }), u.define("select2/data/tokenizer", ["jquery"], function (c) {
    function e(e, t, n) {
      var s = n.get("tokenizer");
      void 0 !== s && (this.tokenizer = s), e.call(this, t, n);
    }

    return e.prototype.bind = function (e, t, n) {
      e.call(this, t, n), this.$search = t.dropdown.$search || t.selection.$search || n.find(".select2-search__field");
    }, e.prototype.query = function (e, t, n) {
      var s = this;
      t.term = t.term || "";
      var i = this.tokenizer(t, this.options, function (e) {
        var t,
            n = s._normalizeItem(e);

        s.$element.find("option").filter(function () {
          return c(this).val() === n.id;
        }).length || ((t = s.option(n)).attr("data-select2-tag", !0), s._removeOldTags(), s.addOptions([t])), t = n, s.trigger("select", {
          data: t
        });
      });
      i.term !== t.term && (this.$search.length && (this.$search.val(i.term), this.$search.trigger("focus")), t.term = i.term), e.call(this, t, n);
    }, e.prototype.tokenizer = function (e, t, n, s) {
      for (var i = n.get("tokenSeparators") || [], r = t.term, o = 0, a = this.createTag || function (e) {
        return {
          id: e.term,
          text: e.term
        };
      }; o < r.length;) {
        var l = r[o];
        -1 !== i.indexOf(l) ? (l = r.substr(0, o), null != (l = a(c.extend({}, t, {
          term: l
        }))) ? (s(l), r = r.substr(o + 1) || "", o = 0) : o++) : o++;
      }

      return {
        term: r
      };
    }, e;
  }), u.define("select2/data/minimumInputLength", [], function () {
    function e(e, t, n) {
      this.minimumInputLength = n.get("minimumInputLength"), e.call(this, t, n);
    }

    return e.prototype.query = function (e, t, n) {
      t.term = t.term || "", t.term.length < this.minimumInputLength ? this.trigger("results:message", {
        message: "inputTooShort",
        args: {
          minimum: this.minimumInputLength,
          input: t.term,
          params: t
        }
      }) : e.call(this, t, n);
    }, e;
  }), u.define("select2/data/maximumInputLength", [], function () {
    function e(e, t, n) {
      this.maximumInputLength = n.get("maximumInputLength"), e.call(this, t, n);
    }

    return e.prototype.query = function (e, t, n) {
      t.term = t.term || "", 0 < this.maximumInputLength && t.term.length > this.maximumInputLength ? this.trigger("results:message", {
        message: "inputTooLong",
        args: {
          maximum: this.maximumInputLength,
          input: t.term,
          params: t
        }
      }) : e.call(this, t, n);
    }, e;
  }), u.define("select2/data/maximumSelectionLength", [], function () {
    function e(e, t, n) {
      this.maximumSelectionLength = n.get("maximumSelectionLength"), e.call(this, t, n);
    }

    return e.prototype.bind = function (e, t, n) {
      var s = this;
      e.call(this, t, n), t.on("select", function () {
        s._checkIfMaximumSelected();
      });
    }, e.prototype.query = function (e, t, n) {
      var s = this;

      this._checkIfMaximumSelected(function () {
        e.call(s, t, n);
      });
    }, e.prototype._checkIfMaximumSelected = function (e, t) {
      var n = this;
      this.current(function (e) {
        e = null != e ? e.length : 0;
        0 < n.maximumSelectionLength && e >= n.maximumSelectionLength ? n.trigger("results:message", {
          message: "maximumSelected",
          args: {
            maximum: n.maximumSelectionLength
          }
        }) : t && t();
      });
    }, e;
  }), u.define("select2/dropdown", ["jquery", "./utils"], function (t, e) {
    function n(e, t) {
      this.$element = e, this.options = t, n.__super__.constructor.call(this);
    }

    return e.Extend(n, e.Observable), n.prototype.render = function () {
      var e = t('<span class="select2-dropdown"><span class="select2-results"></span></span>');
      return e.attr("dir", this.options.get("dir")), this.$dropdown = e;
    }, n.prototype.bind = function () {}, n.prototype.position = function (e, t) {}, n.prototype.destroy = function () {
      this.$dropdown.remove();
    }, n;
  }), u.define("select2/dropdown/search", ["jquery"], function (r) {
    function e() {}

    return e.prototype.render = function (e) {
      var t = e.call(this),
          n = this.options.get("translations").get("search"),
          e = r('<span class="select2-search select2-search--dropdown"><input class="select2-search__field" type="search" tabindex="-1" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" /></span>');
      return this.$searchContainer = e, this.$search = e.find("input"), this.$search.prop("autocomplete", this.options.get("autocomplete")), this.$search.attr("aria-label", n()), t.prepend(e), t;
    }, e.prototype.bind = function (e, t, n) {
      var s = this,
          i = t.id + "-results";
      e.call(this, t, n), this.$search.on("keydown", function (e) {
        s.trigger("keypress", e), s._keyUpPrevented = e.isDefaultPrevented();
      }), this.$search.on("input", function (e) {
        r(this).off("keyup");
      }), this.$search.on("keyup input", function (e) {
        s.handleSearch(e);
      }), t.on("open", function () {
        s.$search.attr("tabindex", 0), s.$search.attr("aria-controls", i), s.$search.trigger("focus"), window.setTimeout(function () {
          s.$search.trigger("focus");
        }, 0);
      }), t.on("close", function () {
        s.$search.attr("tabindex", -1), s.$search.removeAttr("aria-controls"), s.$search.removeAttr("aria-activedescendant"), s.$search.val(""), s.$search.trigger("blur");
      }), t.on("focus", function () {
        t.isOpen() || s.$search.trigger("focus");
      }), t.on("results:all", function (e) {
        null != e.query.term && "" !== e.query.term || (s.showSearch(e) ? s.$searchContainer[0].classList.remove("select2-search--hide") : s.$searchContainer[0].classList.add("select2-search--hide"));
      }), t.on("results:focus", function (e) {
        e.data._resultId ? s.$search.attr("aria-activedescendant", e.data._resultId) : s.$search.removeAttr("aria-activedescendant");
      });
    }, e.prototype.handleSearch = function (e) {
      var t;
      this._keyUpPrevented || (t = this.$search.val(), this.trigger("query", {
        term: t
      })), this._keyUpPrevented = !1;
    }, e.prototype.showSearch = function (e, t) {
      return !0;
    }, e;
  }), u.define("select2/dropdown/hidePlaceholder", [], function () {
    function e(e, t, n, s) {
      this.placeholder = this.normalizePlaceholder(n.get("placeholder")), e.call(this, t, n, s);
    }

    return e.prototype.append = function (e, t) {
      t.results = this.removePlaceholder(t.results), e.call(this, t);
    }, e.prototype.normalizePlaceholder = function (e, t) {
      return "string" == typeof t && (t = {
        id: "",
        text: t
      }), t;
    }, e.prototype.removePlaceholder = function (e, t) {
      for (var n = t.slice(0), s = t.length - 1; 0 <= s; s--) {
        var i = t[s];
        this.placeholder.id === i.id && n.splice(s, 1);
      }

      return n;
    }, e;
  }), u.define("select2/dropdown/infiniteScroll", ["jquery"], function (n) {
    function e(e, t, n, s) {
      this.lastParams = {}, e.call(this, t, n, s), this.$loadingMore = this.createLoadingMore(), this.loading = !1;
    }

    return e.prototype.append = function (e, t) {
      this.$loadingMore.remove(), this.loading = !1, e.call(this, t), this.showLoadingMore(t) && (this.$results.append(this.$loadingMore), this.loadMoreIfNeeded());
    }, e.prototype.bind = function (e, t, n) {
      var s = this;
      e.call(this, t, n), t.on("query", function (e) {
        s.lastParams = e, s.loading = !0;
      }), t.on("query:append", function (e) {
        s.lastParams = e, s.loading = !0;
      }), this.$results.on("scroll", this.loadMoreIfNeeded.bind(this));
    }, e.prototype.loadMoreIfNeeded = function () {
      var e = n.contains(document.documentElement, this.$loadingMore[0]);
      !this.loading && e && (e = this.$results.offset().top + this.$results.outerHeight(!1), this.$loadingMore.offset().top + this.$loadingMore.outerHeight(!1) <= e + 50 && this.loadMore());
    }, e.prototype.loadMore = function () {
      this.loading = !0;
      var e = n.extend({}, {
        page: 1
      }, this.lastParams);
      e.page++, this.trigger("query:append", e);
    }, e.prototype.showLoadingMore = function (e, t) {
      return t.pagination && t.pagination.more;
    }, e.prototype.createLoadingMore = function () {
      var e = n('<li class="select2-results__option select2-results__option--load-more"role="option" aria-disabled="true"></li>'),
          t = this.options.get("translations").get("loadingMore");
      return e.html(t(this.lastParams)), e;
    }, e;
  }), u.define("select2/dropdown/attachBody", ["jquery", "../utils"], function (u, o) {
    function e(e, t, n) {
      this.$dropdownParent = u(n.get("dropdownParent") || document.body), e.call(this, t, n);
    }

    return e.prototype.bind = function (e, t, n) {
      var s = this;
      e.call(this, t, n), t.on("open", function () {
        s._showDropdown(), s._attachPositioningHandler(t), s._bindContainerResultHandlers(t);
      }), t.on("close", function () {
        s._hideDropdown(), s._detachPositioningHandler(t);
      }), this.$dropdownContainer.on("mousedown", function (e) {
        e.stopPropagation();
      });
    }, e.prototype.destroy = function (e) {
      e.call(this), this.$dropdownContainer.remove();
    }, e.prototype.position = function (e, t, n) {
      t.attr("class", n.attr("class")), t[0].classList.remove("select2"), t[0].classList.add("select2-container--open"), t.css({
        position: "absolute",
        top: -999999
      }), this.$container = n;
    }, e.prototype.render = function (e) {
      var t = u("<span></span>"),
          e = e.call(this);
      return t.append(e), this.$dropdownContainer = t;
    }, e.prototype._hideDropdown = function (e) {
      this.$dropdownContainer.detach();
    }, e.prototype._bindContainerResultHandlers = function (e, t) {
      var n;
      this._containerResultsHandlersBound || (n = this, t.on("results:all", function () {
        n._positionDropdown(), n._resizeDropdown();
      }), t.on("results:append", function () {
        n._positionDropdown(), n._resizeDropdown();
      }), t.on("results:message", function () {
        n._positionDropdown(), n._resizeDropdown();
      }), t.on("select", function () {
        n._positionDropdown(), n._resizeDropdown();
      }), t.on("unselect", function () {
        n._positionDropdown(), n._resizeDropdown();
      }), this._containerResultsHandlersBound = !0);
    }, e.prototype._attachPositioningHandler = function (e, t) {
      var n = this,
          s = "scroll.select2." + t.id,
          i = "resize.select2." + t.id,
          r = "orientationchange.select2." + t.id,
          t = this.$container.parents().filter(o.hasScroll);
      t.each(function () {
        o.StoreData(this, "select2-scroll-position", {
          x: u(this).scrollLeft(),
          y: u(this).scrollTop()
        });
      }), t.on(s, function (e) {
        var t = o.GetData(this, "select2-scroll-position");
        u(this).scrollTop(t.y);
      }), u(window).on(s + " " + i + " " + r, function (e) {
        n._positionDropdown(), n._resizeDropdown();
      });
    }, e.prototype._detachPositioningHandler = function (e, t) {
      var n = "scroll.select2." + t.id,
          s = "resize.select2." + t.id,
          t = "orientationchange.select2." + t.id;
      this.$container.parents().filter(o.hasScroll).off(n), u(window).off(n + " " + s + " " + t);
    }, e.prototype._positionDropdown = function () {
      var e = u(window),
          t = this.$dropdown[0].classList.contains("select2-dropdown--above"),
          n = this.$dropdown[0].classList.contains("select2-dropdown--below"),
          s = null,
          i = this.$container.offset();
      i.bottom = i.top + this.$container.outerHeight(!1);
      var r = {
        height: this.$container.outerHeight(!1)
      };
      r.top = i.top, r.bottom = i.top + r.height;
      var o = this.$dropdown.outerHeight(!1),
          a = e.scrollTop(),
          l = e.scrollTop() + e.height(),
          c = a < i.top - o,
          e = l > i.bottom + o,
          a = {
        left: i.left,
        top: r.bottom
      },
          l = this.$dropdownParent;
      "static" === l.css("position") && (l = l.offsetParent());
      i = {
        top: 0,
        left: 0
      };
      (u.contains(document.body, l[0]) || l[0].isConnected) && (i = l.offset()), a.top -= i.top, a.left -= i.left, t || n || (s = "below"), e || !c || t ? !c && e && t && (s = "below") : s = "above", ("above" == s || t && "below" !== s) && (a.top = r.top - i.top - o), null != s && (this.$dropdown[0].classList.remove("select2-dropdown--below"), this.$dropdown[0].classList.remove("select2-dropdown--above"), this.$dropdown[0].classList.add("select2-dropdown--" + s), this.$container[0].classList.remove("select2-container--below"), this.$container[0].classList.remove("select2-container--above"), this.$container[0].classList.add("select2-container--" + s)), this.$dropdownContainer.css(a);
    }, e.prototype._resizeDropdown = function () {
      var e = {
        width: this.$container.outerWidth(!1) + "px"
      };
      this.options.get("dropdownAutoWidth") && (e.minWidth = e.width, e.position = "relative", e.width = "auto"), this.$dropdown.css(e);
    }, e.prototype._showDropdown = function (e) {
      this.$dropdownContainer.appendTo(this.$dropdownParent), this._positionDropdown(), this._resizeDropdown();
    }, e;
  }), u.define("select2/dropdown/minimumResultsForSearch", [], function () {
    function e(e, t, n, s) {
      this.minimumResultsForSearch = n.get("minimumResultsForSearch"), this.minimumResultsForSearch < 0 && (this.minimumResultsForSearch = 1 / 0), e.call(this, t, n, s);
    }

    return e.prototype.showSearch = function (e, t) {
      return !(function e(t) {
        for (var n = 0, s = 0; s < t.length; s++) {
          var i = t[s];
          i.children ? n += e(i.children) : n++;
        }

        return n;
      }(t.data.results) < this.minimumResultsForSearch) && e.call(this, t);
    }, e;
  }), u.define("select2/dropdown/selectOnClose", ["../utils"], function (s) {
    function e() {}

    return e.prototype.bind = function (e, t, n) {
      var s = this;
      e.call(this, t, n), t.on("close", function (e) {
        s._handleSelectOnClose(e);
      });
    }, e.prototype._handleSelectOnClose = function (e, t) {
      if (t && null != t.originalSelect2Event) {
        var n = t.originalSelect2Event;
        if ("select" === n._type || "unselect" === n._type) return;
      }

      n = this.getHighlightedResults();
      n.length < 1 || null != (n = s.GetData(n[0], "data")).element && n.element.selected || null == n.element && n.selected || this.trigger("select", {
        data: n
      });
    }, e;
  }), u.define("select2/dropdown/closeOnSelect", [], function () {
    function e() {}

    return e.prototype.bind = function (e, t, n) {
      var s = this;
      e.call(this, t, n), t.on("select", function (e) {
        s._selectTriggered(e);
      }), t.on("unselect", function (e) {
        s._selectTriggered(e);
      });
    }, e.prototype._selectTriggered = function (e, t) {
      var n = t.originalEvent;
      n && (n.ctrlKey || n.metaKey) || this.trigger("close", {
        originalEvent: n,
        originalSelect2Event: t
      });
    }, e;
  }), u.define("select2/dropdown/dropdownCss", ["../utils"], function (n) {
    function e() {}

    return e.prototype.render = function (e) {
      var t = e.call(this),
          e = this.options.get("dropdownCssClass") || "";
      return -1 !== e.indexOf(":all:") && (e = e.replace(":all:", ""), n.copyNonInternalCssClasses(t[0], this.$element[0])), t.addClass(e), t;
    }, e;
  }), u.define("select2/dropdown/tagsSearchHighlight", ["../utils"], function (s) {
    function e() {}

    return e.prototype.highlightFirstItem = function (e) {
      var t = this.$results.find(".select2-results__option--selectable:not(.select2-results__option--selected)");

      if (0 < t.length) {
        var n = t.first(),
            t = s.GetData(n[0], "data").element;
        if (t && t.getAttribute && "true" === t.getAttribute("data-select2-tag")) return void n.trigger("mouseenter");
      }

      e.call(this);
    }, e;
  }), u.define("select2/i18n/en", [], function () {
    return {
      errorLoading: function errorLoading() {
        return "The results could not be loaded.";
      },
      inputTooLong: function inputTooLong(e) {
        var t = e.input.length - e.maximum,
            e = "Please delete " + t + " character";
        return 1 != t && (e += "s"), e;
      },
      inputTooShort: function inputTooShort(e) {
        return "Please enter " + (e.minimum - e.input.length) + " or more characters";
      },
      loadingMore: function loadingMore() {
        return "Loading more results…";
      },
      maximumSelected: function maximumSelected(e) {
        var t = "You can only select " + e.maximum + " item";
        return 1 != e.maximum && (t += "s"), t;
      },
      noResults: function noResults() {
        return "No results found";
      },
      searching: function searching() {
        return "Searching…";
      },
      removeAllItems: function removeAllItems() {
        return "Remove all items";
      },
      removeItem: function removeItem() {
        return "Remove item";
      },
      search: function search() {
        return "Search";
      }
    };
  }), u.define("select2/defaults", ["jquery", "./results", "./selection/single", "./selection/multiple", "./selection/placeholder", "./selection/allowClear", "./selection/search", "./selection/selectionCss", "./selection/eventRelay", "./utils", "./translation", "./diacritics", "./data/select", "./data/array", "./data/ajax", "./data/tags", "./data/tokenizer", "./data/minimumInputLength", "./data/maximumInputLength", "./data/maximumSelectionLength", "./dropdown", "./dropdown/search", "./dropdown/hidePlaceholder", "./dropdown/infiniteScroll", "./dropdown/attachBody", "./dropdown/minimumResultsForSearch", "./dropdown/selectOnClose", "./dropdown/closeOnSelect", "./dropdown/dropdownCss", "./dropdown/tagsSearchHighlight", "./i18n/en"], function (l, r, o, a, c, u, d, p, h, f, g, t, m, y, v, _, b, $, w, x, A, D, S, E, O, C, L, T, q, I, e) {
    function n() {
      this.reset();
    }

    return n.prototype.apply = function (e) {
      var t;
      null == (e = l.extend(!0, {}, this.defaults, e)).dataAdapter && (null != e.ajax ? e.dataAdapter = v : null != e.data ? e.dataAdapter = y : e.dataAdapter = m, 0 < e.minimumInputLength && (e.dataAdapter = f.Decorate(e.dataAdapter, $)), 0 < e.maximumInputLength && (e.dataAdapter = f.Decorate(e.dataAdapter, w)), 0 < e.maximumSelectionLength && (e.dataAdapter = f.Decorate(e.dataAdapter, x)), e.tags && (e.dataAdapter = f.Decorate(e.dataAdapter, _)), null == e.tokenSeparators && null == e.tokenizer || (e.dataAdapter = f.Decorate(e.dataAdapter, b))), null == e.resultsAdapter && (e.resultsAdapter = r, null != e.ajax && (e.resultsAdapter = f.Decorate(e.resultsAdapter, E)), null != e.placeholder && (e.resultsAdapter = f.Decorate(e.resultsAdapter, S)), e.selectOnClose && (e.resultsAdapter = f.Decorate(e.resultsAdapter, L)), e.tags && (e.resultsAdapter = f.Decorate(e.resultsAdapter, I))), null == e.dropdownAdapter && (e.multiple ? e.dropdownAdapter = A : (t = f.Decorate(A, D), e.dropdownAdapter = t), 0 !== e.minimumResultsForSearch && (e.dropdownAdapter = f.Decorate(e.dropdownAdapter, C)), e.closeOnSelect && (e.dropdownAdapter = f.Decorate(e.dropdownAdapter, T)), null != e.dropdownCssClass && (e.dropdownAdapter = f.Decorate(e.dropdownAdapter, q)), e.dropdownAdapter = f.Decorate(e.dropdownAdapter, O)), null == e.selectionAdapter && (e.multiple ? e.selectionAdapter = a : e.selectionAdapter = o, null != e.placeholder && (e.selectionAdapter = f.Decorate(e.selectionAdapter, c)), e.allowClear && (e.selectionAdapter = f.Decorate(e.selectionAdapter, u)), e.multiple && (e.selectionAdapter = f.Decorate(e.selectionAdapter, d)), null != e.selectionCssClass && (e.selectionAdapter = f.Decorate(e.selectionAdapter, p)), e.selectionAdapter = f.Decorate(e.selectionAdapter, h)), e.language = this._resolveLanguage(e.language), e.language.push("en");

      for (var n = [], s = 0; s < e.language.length; s++) {
        var i = e.language[s];
        -1 === n.indexOf(i) && n.push(i);
      }

      return e.language = n, e.translations = this._processTranslations(e.language, e.debug), e;
    }, n.prototype.reset = function () {
      function a(e) {
        return e.replace(/[^\u0000-\u007E]/g, function (e) {
          return t[e] || e;
        });
      }

      this.defaults = {
        amdLanguageBase: "./i18n/",
        autocomplete: "off",
        closeOnSelect: !0,
        debug: !1,
        dropdownAutoWidth: !1,
        escapeMarkup: f.escapeMarkup,
        language: {},
        matcher: function e(t, n) {
          if (null == t.term || "" === t.term.trim()) return n;

          if (n.children && 0 < n.children.length) {
            for (var s = l.extend(!0, {}, n), i = n.children.length - 1; 0 <= i; i--) {
              null == e(t, n.children[i]) && s.children.splice(i, 1);
            }

            return 0 < s.children.length ? s : e(t, s);
          }

          var r = a(n.text).toUpperCase(),
              o = a(t.term).toUpperCase();
          return -1 < r.indexOf(o) ? n : null;
        },
        minimumInputLength: 0,
        maximumInputLength: 0,
        maximumSelectionLength: 0,
        minimumResultsForSearch: 0,
        selectOnClose: !1,
        scrollAfterSelect: !1,
        sorter: function sorter(e) {
          return e;
        },
        templateResult: function templateResult(e) {
          return e.text;
        },
        templateSelection: function templateSelection(e) {
          return e.text;
        },
        theme: "default",
        width: "resolve"
      };
    }, n.prototype.applyFromElement = function (e, t) {
      var n = e.language,
          s = this.defaults.language,
          i = t.prop("lang"),
          t = t.closest("[lang]").prop("lang"),
          t = Array.prototype.concat.call(this._resolveLanguage(i), this._resolveLanguage(n), this._resolveLanguage(s), this._resolveLanguage(t));
      return e.language = t, e;
    }, n.prototype._resolveLanguage = function (e) {
      if (!e) return [];
      if (l.isEmptyObject(e)) return [];
      if (l.isPlainObject(e)) return [e];

      for (var t, n = Array.isArray(e) ? e : [e], s = [], i = 0; i < n.length; i++) {
        s.push(n[i]), "string" == typeof n[i] && 0 < n[i].indexOf("-") && (t = n[i].split("-")[0], s.push(t));
      }

      return s;
    }, n.prototype._processTranslations = function (e, t) {
      for (var n = new g(), s = 0; s < e.length; s++) {
        var i = new g(),
            r = e[s];
        if ("string" == typeof r) try {
          i = g.loadPath(r);
        } catch (e) {
          try {
            r = this.defaults.amdLanguageBase + r, i = g.loadPath(r);
          } catch (e) {
            t && window.console && console.warn && console.warn('Select2: The language file for "' + r + '" could not be automatically loaded. A fallback will be used instead.');
          }
        } else i = l.isPlainObject(r) ? new g(r) : r;
        n.extend(i);
      }

      return n;
    }, n.prototype.set = function (e, t) {
      var n = {};
      n[l.camelCase(e)] = t;
      n = f._convertData(n);
      l.extend(!0, this.defaults, n);
    }, new n();
  }), u.define("select2/options", ["jquery", "./defaults", "./utils"], function (c, n, u) {
    function e(e, t) {
      this.options = e, null != t && this.fromElement(t), null != t && (this.options = n.applyFromElement(this.options, t)), this.options = n.apply(this.options);
    }

    return e.prototype.fromElement = function (e) {
      var t = ["select2"];
      null == this.options.multiple && (this.options.multiple = e.prop("multiple")), null == this.options.disabled && (this.options.disabled = e.prop("disabled")), null == this.options.autocomplete && e.prop("autocomplete") && (this.options.autocomplete = e.prop("autocomplete")), null == this.options.dir && (e.prop("dir") ? this.options.dir = e.prop("dir") : e.closest("[dir]").prop("dir") ? this.options.dir = e.closest("[dir]").prop("dir") : this.options.dir = "ltr"), e.prop("disabled", this.options.disabled), e.prop("multiple", this.options.multiple), u.GetData(e[0], "select2Tags") && (this.options.debug && window.console && console.warn && console.warn('Select2: The `data-select2-tags` attribute has been changed to use the `data-data` and `data-tags="true"` attributes and will be removed in future versions of Select2.'), u.StoreData(e[0], "data", u.GetData(e[0], "select2Tags")), u.StoreData(e[0], "tags", !0)), u.GetData(e[0], "ajaxUrl") && (this.options.debug && window.console && console.warn && console.warn("Select2: The `data-ajax-url` attribute has been changed to `data-ajax--url` and support for the old attribute will be removed in future versions of Select2."), e.attr("ajax--url", u.GetData(e[0], "ajaxUrl")), u.StoreData(e[0], "ajax-Url", u.GetData(e[0], "ajaxUrl")));
      var n = {};

      function s(e, t) {
        return t.toUpperCase();
      }

      for (var i = 0; i < e[0].attributes.length; i++) {
        var r = e[0].attributes[i].name,
            o = "data-";
        r.substr(0, o.length) == o && (r = r.substring(o.length), o = u.GetData(e[0], r), n[r.replace(/-([a-z])/g, s)] = o);
      }

      c.fn.jquery && "1." == c.fn.jquery.substr(0, 2) && e[0].dataset && (n = c.extend(!0, {}, e[0].dataset, n));
      var a,
          l = c.extend(!0, {}, u.GetData(e[0]), n);

      for (a in l = u._convertData(l)) {
        -1 < t.indexOf(a) || (c.isPlainObject(this.options[a]) ? c.extend(this.options[a], l[a]) : this.options[a] = l[a]);
      }

      return this;
    }, e.prototype.get = function (e) {
      return this.options[e];
    }, e.prototype.set = function (e, t) {
      this.options[e] = t;
    }, e;
  }), u.define("select2/core", ["jquery", "./options", "./utils", "./keys"], function (t, i, r, s) {
    var o = function o(e, t) {
      null != r.GetData(e[0], "select2") && r.GetData(e[0], "select2").destroy(), this.$element = e, this.id = this._generateId(e), t = t || {}, this.options = new i(t, e), o.__super__.constructor.call(this);
      var n = e.attr("tabindex") || 0;
      r.StoreData(e[0], "old-tabindex", n), e.attr("tabindex", "-1");
      t = this.options.get("dataAdapter");
      this.dataAdapter = new t(e, this.options);
      n = this.render();

      this._placeContainer(n);

      t = this.options.get("selectionAdapter");
      this.selection = new t(e, this.options), this.$selection = this.selection.render(), this.selection.position(this.$selection, n);
      t = this.options.get("dropdownAdapter");
      this.dropdown = new t(e, this.options), this.$dropdown = this.dropdown.render(), this.dropdown.position(this.$dropdown, n);
      n = this.options.get("resultsAdapter");
      this.results = new n(e, this.options, this.dataAdapter), this.$results = this.results.render(), this.results.position(this.$results, this.$dropdown);
      var s = this;
      this._bindAdapters(), this._registerDomEvents(), this._registerDataEvents(), this._registerSelectionEvents(), this._registerDropdownEvents(), this._registerResultsEvents(), this._registerEvents(), this.dataAdapter.current(function (e) {
        s.trigger("selection:update", {
          data: e
        });
      }), e[0].classList.add("select2-hidden-accessible"), e.attr("aria-hidden", "true"), this._syncAttributes(), r.StoreData(e[0], "select2", this), e.data("select2", this);
    };

    return r.Extend(o, r.Observable), o.prototype._generateId = function (e) {
      return "select2-" + (null != e.attr("id") ? e.attr("id") : null != e.attr("name") ? e.attr("name") + "-" + r.generateChars(2) : r.generateChars(4)).replace(/(:|\.|\[|\]|,)/g, "");
    }, o.prototype._placeContainer = function (e) {
      e.insertAfter(this.$element);

      var t = this._resolveWidth(this.$element, this.options.get("width"));

      null != t && e.css("width", t);
    }, o.prototype._resolveWidth = function (e, t) {
      var n = /^width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/i;

      if ("resolve" == t) {
        var s = this._resolveWidth(e, "style");

        return null != s ? s : this._resolveWidth(e, "element");
      }

      if ("element" == t) {
        s = e.outerWidth(!1);
        return s <= 0 ? "auto" : s + "px";
      }

      if ("style" != t) return "computedstyle" != t ? t : window.getComputedStyle(e[0]).width;
      e = e.attr("style");
      if ("string" != typeof e) return null;

      for (var i = e.split(";"), r = 0, o = i.length; r < o; r += 1) {
        var a = i[r].replace(/\s/g, "").match(n);
        if (null !== a && 1 <= a.length) return a[1];
      }

      return null;
    }, o.prototype._bindAdapters = function () {
      this.dataAdapter.bind(this, this.$container), this.selection.bind(this, this.$container), this.dropdown.bind(this, this.$container), this.results.bind(this, this.$container);
    }, o.prototype._registerDomEvents = function () {
      var t = this;
      this.$element.on("change.select2", function () {
        t.dataAdapter.current(function (e) {
          t.trigger("selection:update", {
            data: e
          });
        });
      }), this.$element.on("focus.select2", function (e) {
        t.trigger("focus", e);
      }), this._syncA = r.bind(this._syncAttributes, this), this._syncS = r.bind(this._syncSubtree, this), this._observer = new window.MutationObserver(function (e) {
        t._syncA(), t._syncS(e);
      }), this._observer.observe(this.$element[0], {
        attributes: !0,
        childList: !0,
        subtree: !1
      });
    }, o.prototype._registerDataEvents = function () {
      var n = this;
      this.dataAdapter.on("*", function (e, t) {
        n.trigger(e, t);
      });
    }, o.prototype._registerSelectionEvents = function () {
      var n = this,
          s = ["toggle", "focus"];
      this.selection.on("toggle", function () {
        n.toggleDropdown();
      }), this.selection.on("focus", function (e) {
        n.focus(e);
      }), this.selection.on("*", function (e, t) {
        -1 === s.indexOf(e) && n.trigger(e, t);
      });
    }, o.prototype._registerDropdownEvents = function () {
      var n = this;
      this.dropdown.on("*", function (e, t) {
        n.trigger(e, t);
      });
    }, o.prototype._registerResultsEvents = function () {
      var n = this;
      this.results.on("*", function (e, t) {
        n.trigger(e, t);
      });
    }, o.prototype._registerEvents = function () {
      var n = this;
      this.on("open", function () {
        n.$container[0].classList.add("select2-container--open");
      }), this.on("close", function () {
        n.$container[0].classList.remove("select2-container--open");
      }), this.on("enable", function () {
        n.$container[0].classList.remove("select2-container--disabled");
      }), this.on("disable", function () {
        n.$container[0].classList.add("select2-container--disabled");
      }), this.on("blur", function () {
        n.$container[0].classList.remove("select2-container--focus");
      }), this.on("query", function (t) {
        n.isOpen() || n.trigger("open", {}), this.dataAdapter.query(t, function (e) {
          n.trigger("results:all", {
            data: e,
            query: t
          });
        });
      }), this.on("query:append", function (t) {
        this.dataAdapter.query(t, function (e) {
          n.trigger("results:append", {
            data: e,
            query: t
          });
        });
      }), this.on("keypress", function (e) {
        var t = e.which;
        n.isOpen() ? t === s.ESC || t === s.UP && e.altKey ? (n.close(e), e.preventDefault()) : t === s.ENTER || t === s.TAB ? (n.trigger("results:select", {}), e.preventDefault()) : t === s.SPACE && e.ctrlKey ? (n.trigger("results:toggle", {}), e.preventDefault()) : t === s.UP ? (n.trigger("results:previous", {}), e.preventDefault()) : t === s.DOWN && (n.trigger("results:next", {}), e.preventDefault()) : (t === s.ENTER || t === s.SPACE || t === s.DOWN && e.altKey) && (n.open(), e.preventDefault());
      });
    }, o.prototype._syncAttributes = function () {
      this.options.set("disabled", this.$element.prop("disabled")), this.isDisabled() ? (this.isOpen() && this.close(), this.trigger("disable", {})) : this.trigger("enable", {});
    }, o.prototype._isChangeMutation = function (e) {
      var t = this;

      if (e.addedNodes && 0 < e.addedNodes.length) {
        for (var n = 0; n < e.addedNodes.length; n++) {
          if (e.addedNodes[n].selected) return !0;
        }
      } else {
        if (e.removedNodes && 0 < e.removedNodes.length) return !0;
        if (Array.isArray(e)) return e.some(function (e) {
          return t._isChangeMutation(e);
        });
      }

      return !1;
    }, o.prototype._syncSubtree = function (e) {
      var e = this._isChangeMutation(e),
          t = this;

      e && this.dataAdapter.current(function (e) {
        t.trigger("selection:update", {
          data: e
        });
      });
    }, o.prototype.trigger = function (e, t) {
      var n = o.__super__.trigger,
          s = {
        open: "opening",
        close: "closing",
        select: "selecting",
        unselect: "unselecting",
        clear: "clearing"
      };

      if (void 0 === t && (t = {}), e in s) {
        var i = s[e],
            s = {
          prevented: !1,
          name: e,
          args: t
        };
        if (n.call(this, i, s), s.prevented) return void (t.prevented = !0);
      }

      n.call(this, e, t);
    }, o.prototype.toggleDropdown = function () {
      this.isDisabled() || (this.isOpen() ? this.close() : this.open());
    }, o.prototype.open = function () {
      this.isOpen() || this.isDisabled() || this.trigger("query", {});
    }, o.prototype.close = function (e) {
      this.isOpen() && this.trigger("close", {
        originalEvent: e
      });
    }, o.prototype.isEnabled = function () {
      return !this.isDisabled();
    }, o.prototype.isDisabled = function () {
      return this.options.get("disabled");
    }, o.prototype.isOpen = function () {
      return this.$container[0].classList.contains("select2-container--open");
    }, o.prototype.hasFocus = function () {
      return this.$container[0].classList.contains("select2-container--focus");
    }, o.prototype.focus = function (e) {
      this.hasFocus() || (this.$container[0].classList.add("select2-container--focus"), this.trigger("focus", {}));
    }, o.prototype.enable = function (e) {
      this.options.get("debug") && window.console && console.warn && console.warn('Select2: The `select2("enable")` method has been deprecated and will be removed in later Select2 versions. Use $element.prop("disabled") instead.'), null != e && 0 !== e.length || (e = [!0]);
      e = !e[0];
      this.$element.prop("disabled", e);
    }, o.prototype.data = function () {
      this.options.get("debug") && 0 < arguments.length && window.console && console.warn && console.warn('Select2: Data can no longer be set using `select2("data")`. You should consider setting the value instead using `$element.val()`.');
      var t = [];
      return this.dataAdapter.current(function (e) {
        t = e;
      }), t;
    }, o.prototype.val = function (e) {
      if (this.options.get("debug") && window.console && console.warn && console.warn('Select2: The `select2("val")` method has been deprecated and will be removed in later Select2 versions. Use $element.val() instead.'), null == e || 0 === e.length) return this.$element.val();
      e = e[0];
      Array.isArray(e) && (e = e.map(function (e) {
        return e.toString();
      })), this.$element.val(e).trigger("input").trigger("change");
    }, o.prototype.destroy = function () {
      r.RemoveData(this.$container[0]), this.$container.remove(), this._observer.disconnect(), this._observer = null, this._syncA = null, this._syncS = null, this.$element.off(".select2"), this.$element.attr("tabindex", r.GetData(this.$element[0], "old-tabindex")), this.$element[0].classList.remove("select2-hidden-accessible"), this.$element.attr("aria-hidden", "false"), r.RemoveData(this.$element[0]), this.$element.removeData("select2"), this.dataAdapter.destroy(), this.selection.destroy(), this.dropdown.destroy(), this.results.destroy(), this.dataAdapter = null, this.selection = null, this.dropdown = null, this.results = null;
    }, o.prototype.render = function () {
      var e = t('<span class="select2 select2-container"><span class="selection"></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>');
      return e.attr("dir", this.options.get("dir")), this.$container = e, this.$container[0].classList.add("select2-container--" + this.options.get("theme")), r.StoreData(e[0], "element", this.$element), e;
    }, o;
  }), u.define("jquery-mousewheel", ["jquery"], function (e) {
    return e;
  }), u.define("jquery.select2", ["jquery", "jquery-mousewheel", "./select2/core", "./select2/defaults", "./select2/utils"], function (i, e, r, t, o) {
    var a;
    return null == i.fn.select2 && (a = ["open", "close", "destroy"], i.fn.select2 = function (t) {
      if ("object" == _typeof(t = t || {})) return this.each(function () {
        var e = i.extend(!0, {}, t);
        new r(i(this), e);
      }), this;
      if ("string" != typeof t) throw new Error("Invalid arguments for Select2: " + t);
      var n,
          s = Array.prototype.slice.call(arguments, 1);
      return this.each(function () {
        var e = o.GetData(this, "select2");
        null == e && window.console && console.error && console.error("The select2('" + t + "') method was called on an element that is not using Select2."), n = e[t].apply(e, s);
      }), -1 < a.indexOf(t) ? this : n;
    }), null == i.fn.select2.defaults && (i.fn.select2.defaults = t), r;
  }), {
    define: u.define,
    require: u.require
  });

  function b(e, t) {
    return i.call(e, t);
  }

  function l(e, t) {
    var n,
        s,
        i,
        r,
        o,
        a,
        l,
        c,
        u,
        d,
        p = t && t.split("/"),
        h = y.map,
        f = h && h["*"] || {};

    if (e) {
      for (t = (e = e.split("/")).length - 1, y.nodeIdCompat && _.test(e[t]) && (e[t] = e[t].replace(_, "")), "." === e[0].charAt(0) && p && (e = p.slice(0, p.length - 1).concat(e)), c = 0; c < e.length; c++) {
        "." === (d = e[c]) ? (e.splice(c, 1), --c) : ".." === d && (0 === c || 1 === c && ".." === e[2] || ".." === e[c - 1] || 0 < c && (e.splice(c - 1, 2), c -= 2));
      }

      e = e.join("/");
    }

    if ((p || f) && h) {
      for (c = (n = e.split("/")).length; 0 < c; --c) {
        if (s = n.slice(0, c).join("/"), p) for (u = p.length; 0 < u; --u) {
          if (i = h[p.slice(0, u).join("/")], i = i && i[s]) {
            r = i, o = c;
            break;
          }
        }
        if (r) break;
        !a && f && f[s] && (a = f[s], l = c);
      }

      !r && a && (r = a, o = l), r && (n.splice(0, o, r), e = n.join("/"));
    }

    return e;
  }

  function w(t, n) {
    return function () {
      var e = a.call(arguments, 0);
      return "string" != typeof e[0] && 1 === e.length && e.push(null), _o.apply(p, e.concat([t, n]));
    };
  }

  function x(e) {
    var t;
    if (b(m, e) && (t = m[e], delete m[e], v[e] = !0, r.apply(p, t)), !b(g, e) && !b(v, e)) throw new Error("No " + e);
    return g[e];
  }

  function c(e) {
    var t,
        n = e ? e.indexOf("!") : -1;
    return -1 < n && (t = e.substring(0, n), e = e.substring(n + 1, e.length)), [t, e];
  }

  function A(e) {
    return e ? c(e) : [];
  }

  var u = s.require("jquery.select2");

  return t.fn.select2.amd = s, u;
});
"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/**
 * Swiper 5.2.1
 * Most modern mobile touch slider and framework with hardware accelerated transitions
 * http://swiperjs.com
 *
 * Copyright 2014-2019 Vladimir Kharlampidi
 *
 * Released under the MIT License
 *
 * Released on: November 16, 2019
 */
!function (e, t) {
  "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : (e = e || self).Swiper = t();
}(void 0, function () {
  "use strict";

  var e = "undefined" == typeof document ? {
    body: {},
    addEventListener: function addEventListener() {},
    removeEventListener: function removeEventListener() {},
    activeElement: {
      blur: function blur() {},
      nodeName: ""
    },
    querySelector: function querySelector() {
      return null;
    },
    querySelectorAll: function querySelectorAll() {
      return [];
    },
    getElementById: function getElementById() {
      return null;
    },
    createEvent: function createEvent() {
      return {
        initEvent: function initEvent() {}
      };
    },
    createElement: function createElement() {
      return {
        children: [],
        childNodes: [],
        style: {},
        setAttribute: function setAttribute() {},
        getElementsByTagName: function getElementsByTagName() {
          return [];
        }
      };
    },
    location: {
      hash: ""
    }
  } : document,
      t = "undefined" == typeof window ? {
    document: e,
    navigator: {
      userAgent: ""
    },
    location: {},
    history: {},
    CustomEvent: function CustomEvent() {
      return this;
    },
    addEventListener: function addEventListener() {},
    removeEventListener: function removeEventListener() {},
    getComputedStyle: function getComputedStyle() {
      return {
        getPropertyValue: function getPropertyValue() {
          return "";
        }
      };
    },
    Image: function Image() {},
    Date: function Date() {},
    screen: {},
    setTimeout: function setTimeout() {},
    clearTimeout: function clearTimeout() {}
  } : window,
      i = function i(e) {
    for (var t = 0; t < e.length; t += 1) {
      this[t] = e[t];
    }

    return this.length = e.length, this;
  };

  function s(s, a) {
    var r = [],
        n = 0;
    if (s && !a && s instanceof i) return s;
    if (s) if ("string" == typeof s) {
      var o,
          l,
          d = s.trim();

      if (d.indexOf("<") >= 0 && d.indexOf(">") >= 0) {
        var h = "div";

        for (0 === d.indexOf("<li") && (h = "ul"), 0 === d.indexOf("<tr") && (h = "tbody"), 0 !== d.indexOf("<td") && 0 !== d.indexOf("<th") || (h = "tr"), 0 === d.indexOf("<tbody") && (h = "table"), 0 === d.indexOf("<option") && (h = "select"), (l = e.createElement(h)).innerHTML = d, n = 0; n < l.childNodes.length; n += 1) {
          r.push(l.childNodes[n]);
        }
      } else for (o = a || "#" !== s[0] || s.match(/[ .<>:~]/) ? (a || e).querySelectorAll(s.trim()) : [e.getElementById(s.trim().split("#")[1])], n = 0; n < o.length; n += 1) {
        o[n] && r.push(o[n]);
      }
    } else if (s.nodeType || s === t || s === e) r.push(s);else if (s.length > 0 && s[0].nodeType) for (n = 0; n < s.length; n += 1) {
      r.push(s[n]);
    }
    return new i(r);
  }

  function a(e) {
    for (var t = [], i = 0; i < e.length; i += 1) {
      -1 === t.indexOf(e[i]) && t.push(e[i]);
    }

    return t;
  }

  s.fn = i.prototype, s.Class = i, s.Dom7 = i;
  var r = {
    addClass: function addClass(e) {
      if (void 0 === e) return this;

      for (var t = e.split(" "), i = 0; i < t.length; i += 1) {
        for (var s = 0; s < this.length; s += 1) {
          void 0 !== this[s] && void 0 !== this[s].classList && this[s].classList.add(t[i]);
        }
      }

      return this;
    },
    removeClass: function removeClass(e) {
      for (var t = e.split(" "), i = 0; i < t.length; i += 1) {
        for (var s = 0; s < this.length; s += 1) {
          void 0 !== this[s] && void 0 !== this[s].classList && this[s].classList.remove(t[i]);
        }
      }

      return this;
    },
    hasClass: function hasClass(e) {
      return !!this[0] && this[0].classList.contains(e);
    },
    toggleClass: function toggleClass(e) {
      for (var t = e.split(" "), i = 0; i < t.length; i += 1) {
        for (var s = 0; s < this.length; s += 1) {
          void 0 !== this[s] && void 0 !== this[s].classList && this[s].classList.toggle(t[i]);
        }
      }

      return this;
    },
    attr: function attr(e, t) {
      var i = arguments;
      if (1 === arguments.length && "string" == typeof e) return this[0] ? this[0].getAttribute(e) : void 0;

      for (var s = 0; s < this.length; s += 1) {
        if (2 === i.length) this[s].setAttribute(e, t);else for (var a in e) {
          this[s][a] = e[a], this[s].setAttribute(a, e[a]);
        }
      }

      return this;
    },
    removeAttr: function removeAttr(e) {
      for (var t = 0; t < this.length; t += 1) {
        this[t].removeAttribute(e);
      }

      return this;
    },
    data: function data(e, t) {
      var i;

      if (void 0 !== t) {
        for (var s = 0; s < this.length; s += 1) {
          (i = this[s]).dom7ElementDataStorage || (i.dom7ElementDataStorage = {}), i.dom7ElementDataStorage[e] = t;
        }

        return this;
      }

      if (i = this[0]) {
        if (i.dom7ElementDataStorage && e in i.dom7ElementDataStorage) return i.dom7ElementDataStorage[e];
        var a = i.getAttribute("data-" + e);
        return a || void 0;
      }
    },
    transform: function transform(e) {
      for (var t = 0; t < this.length; t += 1) {
        var i = this[t].style;
        i.webkitTransform = e, i.transform = e;
      }

      return this;
    },
    transition: function transition(e) {
      "string" != typeof e && (e += "ms");

      for (var t = 0; t < this.length; t += 1) {
        var i = this[t].style;
        i.webkitTransitionDuration = e, i.transitionDuration = e;
      }

      return this;
    },
    on: function on() {
      for (var e, t = [], i = arguments.length; i--;) {
        t[i] = arguments[i];
      }

      var a = t[0],
          r = t[1],
          n = t[2],
          o = t[3];

      function l(e) {
        var t = e.target;

        if (t) {
          var i = e.target.dom7EventData || [];
          if (i.indexOf(e) < 0 && i.unshift(e), s(t).is(r)) n.apply(t, i);else for (var a = s(t).parents(), o = 0; o < a.length; o += 1) {
            s(a[o]).is(r) && n.apply(a[o], i);
          }
        }
      }

      function d(e) {
        var t = e && e.target && e.target.dom7EventData || [];
        t.indexOf(e) < 0 && t.unshift(e), n.apply(this, t);
      }

      "function" == typeof t[1] && (a = (e = t)[0], n = e[1], o = e[2], r = void 0), o || (o = !1);

      for (var h, p = a.split(" "), c = 0; c < this.length; c += 1) {
        var u = this[c];
        if (r) for (h = 0; h < p.length; h += 1) {
          var v = p[h];
          u.dom7LiveListeners || (u.dom7LiveListeners = {}), u.dom7LiveListeners[v] || (u.dom7LiveListeners[v] = []), u.dom7LiveListeners[v].push({
            listener: n,
            proxyListener: l
          }), u.addEventListener(v, l, o);
        } else for (h = 0; h < p.length; h += 1) {
          var f = p[h];
          u.dom7Listeners || (u.dom7Listeners = {}), u.dom7Listeners[f] || (u.dom7Listeners[f] = []), u.dom7Listeners[f].push({
            listener: n,
            proxyListener: d
          }), u.addEventListener(f, d, o);
        }
      }

      return this;
    },
    off: function off() {
      for (var e, t = [], i = arguments.length; i--;) {
        t[i] = arguments[i];
      }

      var s = t[0],
          a = t[1],
          r = t[2],
          n = t[3];
      "function" == typeof t[1] && (s = (e = t)[0], r = e[1], n = e[2], a = void 0), n || (n = !1);

      for (var o = s.split(" "), l = 0; l < o.length; l += 1) {
        for (var d = o[l], h = 0; h < this.length; h += 1) {
          var p = this[h],
              c = void 0;
          if (!a && p.dom7Listeners ? c = p.dom7Listeners[d] : a && p.dom7LiveListeners && (c = p.dom7LiveListeners[d]), c && c.length) for (var u = c.length - 1; u >= 0; u -= 1) {
            var v = c[u];
            r && v.listener === r ? (p.removeEventListener(d, v.proxyListener, n), c.splice(u, 1)) : r && v.listener && v.listener.dom7proxy && v.listener.dom7proxy === r ? (p.removeEventListener(d, v.proxyListener, n), c.splice(u, 1)) : r || (p.removeEventListener(d, v.proxyListener, n), c.splice(u, 1));
          }
        }
      }

      return this;
    },
    trigger: function trigger() {
      for (var i = [], s = arguments.length; s--;) {
        i[s] = arguments[s];
      }

      for (var a = i[0].split(" "), r = i[1], n = 0; n < a.length; n += 1) {
        for (var o = a[n], l = 0; l < this.length; l += 1) {
          var d = this[l],
              h = void 0;

          try {
            h = new t.CustomEvent(o, {
              detail: r,
              bubbles: !0,
              cancelable: !0
            });
          } catch (t) {
            (h = e.createEvent("Event")).initEvent(o, !0, !0), h.detail = r;
          }

          d.dom7EventData = i.filter(function (e, t) {
            return t > 0;
          }), d.dispatchEvent(h), d.dom7EventData = [], delete d.dom7EventData;
        }
      }

      return this;
    },
    transitionEnd: function transitionEnd(e) {
      var t,
          i = ["webkitTransitionEnd", "transitionend"],
          s = this;

      function a(r) {
        if (r.target === this) for (e.call(this, r), t = 0; t < i.length; t += 1) {
          s.off(i[t], a);
        }
      }

      if (e) for (t = 0; t < i.length; t += 1) {
        s.on(i[t], a);
      }
      return this;
    },
    outerWidth: function outerWidth(e) {
      if (this.length > 0) {
        if (e) {
          var t = this.styles();
          return this[0].offsetWidth + parseFloat(t.getPropertyValue("margin-right")) + parseFloat(t.getPropertyValue("margin-left"));
        }

        return this[0].offsetWidth;
      }

      return null;
    },
    outerHeight: function outerHeight(e) {
      if (this.length > 0) {
        if (e) {
          var t = this.styles();
          return this[0].offsetHeight + parseFloat(t.getPropertyValue("margin-top")) + parseFloat(t.getPropertyValue("margin-bottom"));
        }

        return this[0].offsetHeight;
      }

      return null;
    },
    offset: function offset() {
      if (this.length > 0) {
        var i = this[0],
            s = i.getBoundingClientRect(),
            a = e.body,
            r = i.clientTop || a.clientTop || 0,
            n = i.clientLeft || a.clientLeft || 0,
            o = i === t ? t.scrollY : i.scrollTop,
            l = i === t ? t.scrollX : i.scrollLeft;
        return {
          top: s.top + o - r,
          left: s.left + l - n
        };
      }

      return null;
    },
    css: function css(e, i) {
      var s;

      if (1 === arguments.length) {
        if ("string" != typeof e) {
          for (s = 0; s < this.length; s += 1) {
            for (var a in e) {
              this[s].style[a] = e[a];
            }
          }

          return this;
        }

        if (this[0]) return t.getComputedStyle(this[0], null).getPropertyValue(e);
      }

      if (2 === arguments.length && "string" == typeof e) {
        for (s = 0; s < this.length; s += 1) {
          this[s].style[e] = i;
        }

        return this;
      }

      return this;
    },
    each: function each(e) {
      if (!e) return this;

      for (var t = 0; t < this.length; t += 1) {
        if (!1 === e.call(this[t], t, this[t])) return this;
      }

      return this;
    },
    html: function html(e) {
      if (void 0 === e) return this[0] ? this[0].innerHTML : void 0;

      for (var t = 0; t < this.length; t += 1) {
        this[t].innerHTML = e;
      }

      return this;
    },
    text: function text(e) {
      if (void 0 === e) return this[0] ? this[0].textContent.trim() : null;

      for (var t = 0; t < this.length; t += 1) {
        this[t].textContent = e;
      }

      return this;
    },
    is: function is(a) {
      var r,
          n,
          o = this[0];
      if (!o || void 0 === a) return !1;

      if ("string" == typeof a) {
        if (o.matches) return o.matches(a);
        if (o.webkitMatchesSelector) return o.webkitMatchesSelector(a);
        if (o.msMatchesSelector) return o.msMatchesSelector(a);

        for (r = s(a), n = 0; n < r.length; n += 1) {
          if (r[n] === o) return !0;
        }

        return !1;
      }

      if (a === e) return o === e;
      if (a === t) return o === t;

      if (a.nodeType || a instanceof i) {
        for (r = a.nodeType ? [a] : a, n = 0; n < r.length; n += 1) {
          if (r[n] === o) return !0;
        }

        return !1;
      }

      return !1;
    },
    index: function index() {
      var e,
          t = this[0];

      if (t) {
        for (e = 0; null !== (t = t.previousSibling);) {
          1 === t.nodeType && (e += 1);
        }

        return e;
      }
    },
    eq: function eq(e) {
      if (void 0 === e) return this;
      var t,
          s = this.length;
      return new i(e > s - 1 ? [] : e < 0 ? (t = s + e) < 0 ? [] : [this[t]] : [this[e]]);
    },
    append: function append() {
      for (var t, s = [], a = arguments.length; a--;) {
        s[a] = arguments[a];
      }

      for (var r = 0; r < s.length; r += 1) {
        t = s[r];

        for (var n = 0; n < this.length; n += 1) {
          if ("string" == typeof t) {
            var o = e.createElement("div");

            for (o.innerHTML = t; o.firstChild;) {
              this[n].appendChild(o.firstChild);
            }
          } else if (t instanceof i) for (var l = 0; l < t.length; l += 1) {
            this[n].appendChild(t[l]);
          } else this[n].appendChild(t);
        }
      }

      return this;
    },
    prepend: function prepend(t) {
      var s, a;

      for (s = 0; s < this.length; s += 1) {
        if ("string" == typeof t) {
          var r = e.createElement("div");

          for (r.innerHTML = t, a = r.childNodes.length - 1; a >= 0; a -= 1) {
            this[s].insertBefore(r.childNodes[a], this[s].childNodes[0]);
          }
        } else if (t instanceof i) for (a = 0; a < t.length; a += 1) {
          this[s].insertBefore(t[a], this[s].childNodes[0]);
        } else this[s].insertBefore(t, this[s].childNodes[0]);
      }

      return this;
    },
    next: function next(e) {
      return this.length > 0 ? e ? this[0].nextElementSibling && s(this[0].nextElementSibling).is(e) ? new i([this[0].nextElementSibling]) : new i([]) : this[0].nextElementSibling ? new i([this[0].nextElementSibling]) : new i([]) : new i([]);
    },
    nextAll: function nextAll(e) {
      var t = [],
          a = this[0];
      if (!a) return new i([]);

      for (; a.nextElementSibling;) {
        var r = a.nextElementSibling;
        e ? s(r).is(e) && t.push(r) : t.push(r), a = r;
      }

      return new i(t);
    },
    prev: function prev(e) {
      if (this.length > 0) {
        var t = this[0];
        return e ? t.previousElementSibling && s(t.previousElementSibling).is(e) ? new i([t.previousElementSibling]) : new i([]) : t.previousElementSibling ? new i([t.previousElementSibling]) : new i([]);
      }

      return new i([]);
    },
    prevAll: function prevAll(e) {
      var t = [],
          a = this[0];
      if (!a) return new i([]);

      for (; a.previousElementSibling;) {
        var r = a.previousElementSibling;
        e ? s(r).is(e) && t.push(r) : t.push(r), a = r;
      }

      return new i(t);
    },
    parent: function parent(e) {
      for (var t = [], i = 0; i < this.length; i += 1) {
        null !== this[i].parentNode && (e ? s(this[i].parentNode).is(e) && t.push(this[i].parentNode) : t.push(this[i].parentNode));
      }

      return s(a(t));
    },
    parents: function parents(e) {
      for (var t = [], i = 0; i < this.length; i += 1) {
        for (var r = this[i].parentNode; r;) {
          e ? s(r).is(e) && t.push(r) : t.push(r), r = r.parentNode;
        }
      }

      return s(a(t));
    },
    closest: function closest(e) {
      var t = this;
      return void 0 === e ? new i([]) : (t.is(e) || (t = t.parents(e).eq(0)), t);
    },
    find: function find(e) {
      for (var t = [], s = 0; s < this.length; s += 1) {
        for (var a = this[s].querySelectorAll(e), r = 0; r < a.length; r += 1) {
          t.push(a[r]);
        }
      }

      return new i(t);
    },
    children: function children(e) {
      for (var t = [], r = 0; r < this.length; r += 1) {
        for (var n = this[r].childNodes, o = 0; o < n.length; o += 1) {
          e ? 1 === n[o].nodeType && s(n[o]).is(e) && t.push(n[o]) : 1 === n[o].nodeType && t.push(n[o]);
        }
      }

      return new i(a(t));
    },
    filter: function filter(e) {
      for (var t = [], s = 0; s < this.length; s += 1) {
        e.call(this[s], s, this[s]) && t.push(this[s]);
      }

      return new i(t);
    },
    remove: function remove() {
      for (var e = 0; e < this.length; e += 1) {
        this[e].parentNode && this[e].parentNode.removeChild(this[e]);
      }

      return this;
    },
    add: function add() {
      for (var e = [], t = arguments.length; t--;) {
        e[t] = arguments[t];
      }

      var i, a;

      for (i = 0; i < e.length; i += 1) {
        var r = s(e[i]);

        for (a = 0; a < r.length; a += 1) {
          this[this.length] = r[a], this.length += 1;
        }
      }

      return this;
    },
    styles: function styles() {
      return this[0] ? t.getComputedStyle(this[0], null) : {};
    }
  };
  Object.keys(r).forEach(function (e) {
    s.fn[e] = s.fn[e] || r[e];
  });

  var n = {
    deleteProps: function deleteProps(e) {
      var t = e;
      Object.keys(t).forEach(function (e) {
        try {
          t[e] = null;
        } catch (e) {}

        try {
          delete t[e];
        } catch (e) {}
      });
    },
    nextTick: function nextTick(e, t) {
      return void 0 === t && (t = 0), setTimeout(e, t);
    },
    now: function now() {
      return Date.now();
    },
    getTranslate: function getTranslate(e, i) {
      var s, a, r;
      void 0 === i && (i = "x");
      var n = t.getComputedStyle(e, null);
      return t.WebKitCSSMatrix ? ((a = n.transform || n.webkitTransform).split(",").length > 6 && (a = a.split(", ").map(function (e) {
        return e.replace(",", ".");
      }).join(", ")), r = new t.WebKitCSSMatrix("none" === a ? "" : a)) : s = (r = n.MozTransform || n.OTransform || n.MsTransform || n.msTransform || n.transform || n.getPropertyValue("transform").replace("translate(", "matrix(1, 0, 0, 1,")).toString().split(","), "x" === i && (a = t.WebKitCSSMatrix ? r.m41 : 16 === s.length ? parseFloat(s[12]) : parseFloat(s[4])), "y" === i && (a = t.WebKitCSSMatrix ? r.m42 : 16 === s.length ? parseFloat(s[13]) : parseFloat(s[5])), a || 0;
    },
    parseUrlQuery: function parseUrlQuery(e) {
      var i,
          s,
          a,
          r,
          n = {},
          o = e || t.location.href;
      if ("string" == typeof o && o.length) for (r = (s = (o = o.indexOf("?") > -1 ? o.replace(/\S*\?/, "") : "").split("&").filter(function (e) {
        return "" !== e;
      })).length, i = 0; i < r; i += 1) {
        a = s[i].replace(/#\S+/g, "").split("="), n[decodeURIComponent(a[0])] = void 0 === a[1] ? void 0 : decodeURIComponent(a[1]) || "";
      }
      return n;
    },
    isObject: function isObject(e) {
      return "object" == _typeof(e) && null !== e && e.constructor && e.constructor === Object;
    },
    extend: function extend() {
      for (var e = [], t = arguments.length; t--;) {
        e[t] = arguments[t];
      }

      for (var i = Object(e[0]), s = 1; s < e.length; s += 1) {
        var a = e[s];
        if (null != a) for (var r = Object.keys(Object(a)), o = 0, l = r.length; o < l; o += 1) {
          var d = r[o],
              h = Object.getOwnPropertyDescriptor(a, d);
          void 0 !== h && h.enumerable && (n.isObject(i[d]) && n.isObject(a[d]) ? n.extend(i[d], a[d]) : !n.isObject(i[d]) && n.isObject(a[d]) ? (i[d] = {}, n.extend(i[d], a[d])) : i[d] = a[d]);
        }
      }

      return i;
    }
  },
      o = {
    touch: t.Modernizr && !0 === t.Modernizr.touch || !!(t.navigator.maxTouchPoints > 0 || "ontouchstart" in t || t.DocumentTouch && e instanceof t.DocumentTouch),
    pointerEvents: !!t.PointerEvent && "maxTouchPoints" in t.navigator && t.navigator.maxTouchPoints > 0,
    observer: "MutationObserver" in t || "WebkitMutationObserver" in t,
    passiveListener: function () {
      var e = !1;

      try {
        var i = Object.defineProperty({}, "passive", {
          get: function get() {
            e = !0;
          }
        });
        t.addEventListener("testPassiveListener", null, i);
      } catch (e) {}

      return e;
    }(),
    gestures: "ongesturestart" in t
  },
      l = function l(e) {
    void 0 === e && (e = {});
    var t = this;
    t.params = e, t.eventsListeners = {}, t.params && t.params.on && Object.keys(t.params.on).forEach(function (e) {
      t.on(e, t.params.on[e]);
    });
  },
      d = {
    components: {
      configurable: !0
    }
  };

  l.prototype.on = function (e, t, i) {
    var s = this;
    if ("function" != typeof t) return s;
    var a = i ? "unshift" : "push";
    return e.split(" ").forEach(function (e) {
      s.eventsListeners[e] || (s.eventsListeners[e] = []), s.eventsListeners[e][a](t);
    }), s;
  }, l.prototype.once = function (e, t, i) {
    var s = this;
    if ("function" != typeof t) return s;

    function a() {
      for (var i = [], r = arguments.length; r--;) {
        i[r] = arguments[r];
      }

      s.off(e, a), a.f7proxy && delete a.f7proxy, t.apply(s, i);
    }

    return a.f7proxy = t, s.on(e, a, i);
  }, l.prototype.off = function (e, t) {
    var i = this;
    return i.eventsListeners ? (e.split(" ").forEach(function (e) {
      void 0 === t ? i.eventsListeners[e] = [] : i.eventsListeners[e] && i.eventsListeners[e].length && i.eventsListeners[e].forEach(function (s, a) {
        (s === t || s.f7proxy && s.f7proxy === t) && i.eventsListeners[e].splice(a, 1);
      });
    }), i) : i;
  }, l.prototype.emit = function () {
    for (var e = [], t = arguments.length; t--;) {
      e[t] = arguments[t];
    }

    var i,
        s,
        a,
        r = this;
    if (!r.eventsListeners) return r;
    "string" == typeof e[0] || Array.isArray(e[0]) ? (i = e[0], s = e.slice(1, e.length), a = r) : (i = e[0].events, s = e[0].data, a = e[0].context || r);
    var n = Array.isArray(i) ? i : i.split(" ");
    return n.forEach(function (e) {
      if (r.eventsListeners && r.eventsListeners[e]) {
        var t = [];
        r.eventsListeners[e].forEach(function (e) {
          t.push(e);
        }), t.forEach(function (e) {
          e.apply(a, s);
        });
      }
    }), r;
  }, l.prototype.useModulesParams = function (e) {
    var t = this;
    t.modules && Object.keys(t.modules).forEach(function (i) {
      var s = t.modules[i];
      s.params && n.extend(e, s.params);
    });
  }, l.prototype.useModules = function (e) {
    void 0 === e && (e = {});
    var t = this;
    t.modules && Object.keys(t.modules).forEach(function (i) {
      var s = t.modules[i],
          a = e[i] || {};
      s.instance && Object.keys(s.instance).forEach(function (e) {
        var i = s.instance[e];
        t[e] = "function" == typeof i ? i.bind(t) : i;
      }), s.on && t.on && Object.keys(s.on).forEach(function (e) {
        t.on(e, s.on[e]);
      }), s.create && s.create.bind(t)(a);
    });
  }, d.components.set = function (e) {
    this.use && this.use(e);
  }, l.installModule = function (e) {
    for (var t = [], i = arguments.length - 1; i-- > 0;) {
      t[i] = arguments[i + 1];
    }

    var s = this;
    s.prototype.modules || (s.prototype.modules = {});
    var a = e.name || Object.keys(s.prototype.modules).length + "_" + n.now();
    return s.prototype.modules[a] = e, e.proto && Object.keys(e.proto).forEach(function (t) {
      s.prototype[t] = e.proto[t];
    }), e.static && Object.keys(e.static).forEach(function (t) {
      s[t] = e.static[t];
    }), e.install && e.install.apply(s, t), s;
  }, l.use = function (e) {
    for (var t = [], i = arguments.length - 1; i-- > 0;) {
      t[i] = arguments[i + 1];
    }

    var s = this;
    return Array.isArray(e) ? (e.forEach(function (e) {
      return s.installModule(e);
    }), s) : s.installModule.apply(s, [e].concat(t));
  }, Object.defineProperties(l, d);
  var h = {
    updateSize: function updateSize() {
      var e,
          t,
          i = this.$el;
      e = void 0 !== this.params.width ? this.params.width : i[0].clientWidth, t = void 0 !== this.params.height ? this.params.height : i[0].clientHeight, 0 === e && this.isHorizontal() || 0 === t && this.isVertical() || (e = e - parseInt(i.css("padding-left"), 10) - parseInt(i.css("padding-right"), 10), t = t - parseInt(i.css("padding-top"), 10) - parseInt(i.css("padding-bottom"), 10), n.extend(this, {
        width: e,
        height: t,
        size: this.isHorizontal() ? e : t
      }));
    },
    updateSlides: function updateSlides() {
      var e = this.params,
          i = this.$wrapperEl,
          s = this.size,
          a = this.rtlTranslate,
          r = this.wrongRTL,
          o = this.virtual && e.virtual.enabled,
          l = o ? this.virtual.slides.length : this.slides.length,
          d = i.children("." + this.params.slideClass),
          h = o ? this.virtual.slides.length : d.length,
          p = [],
          c = [],
          u = [];

      function v(t) {
        return !e.cssMode || t !== d.length - 1;
      }

      var f = e.slidesOffsetBefore;
      "function" == typeof f && (f = e.slidesOffsetBefore.call(this));
      var m = e.slidesOffsetAfter;
      "function" == typeof m && (m = e.slidesOffsetAfter.call(this));
      var g = this.snapGrid.length,
          b = this.snapGrid.length,
          w = e.spaceBetween,
          y = -f,
          x = 0,
          T = 0;

      if (void 0 !== s) {
        var E, S;
        "string" == typeof w && w.indexOf("%") >= 0 && (w = parseFloat(w.replace("%", "")) / 100 * s), this.virtualSize = -w, a ? d.css({
          marginLeft: "",
          marginTop: ""
        }) : d.css({
          marginRight: "",
          marginBottom: ""
        }), e.slidesPerColumn > 1 && (E = Math.floor(h / e.slidesPerColumn) === h / this.params.slidesPerColumn ? h : Math.ceil(h / e.slidesPerColumn) * e.slidesPerColumn, "auto" !== e.slidesPerView && "row" === e.slidesPerColumnFill && (E = Math.max(E, e.slidesPerView * e.slidesPerColumn)));

        for (var C, M = e.slidesPerColumn, P = E / M, z = Math.floor(h / e.slidesPerColumn), k = 0; k < h; k += 1) {
          S = 0;
          var $ = d.eq(k);

          if (e.slidesPerColumn > 1) {
            var L = void 0,
                I = void 0,
                D = void 0;

            if ("row" === e.slidesPerColumnFill && e.slidesPerGroup > 1) {
              var O = Math.floor(k / (e.slidesPerGroup * e.slidesPerColumn)),
                  A = k - e.slidesPerColumn * e.slidesPerGroup * O,
                  G = 0 === O ? e.slidesPerGroup : Math.min(Math.ceil((h - O * M * e.slidesPerGroup) / M), e.slidesPerGroup);
              L = (I = A - (D = Math.floor(A / G)) * G + O * e.slidesPerGroup) + D * E / M, $.css({
                "-webkit-box-ordinal-group": L,
                "-moz-box-ordinal-group": L,
                "-ms-flex-order": L,
                "-webkit-order": L,
                order: L
              });
            } else "column" === e.slidesPerColumnFill ? (D = k - (I = Math.floor(k / M)) * M, (I > z || I === z && D === M - 1) && (D += 1) >= M && (D = 0, I += 1)) : I = k - (D = Math.floor(k / P)) * P;

            $.css("margin-" + (this.isHorizontal() ? "top" : "left"), 0 !== D && e.spaceBetween && e.spaceBetween + "px");
          }

          if ("none" !== $.css("display")) {
            if ("auto" === e.slidesPerView) {
              var B = t.getComputedStyle($[0], null),
                  H = $[0].style.transform,
                  N = $[0].style.webkitTransform;
              if (H && ($[0].style.transform = "none"), N && ($[0].style.webkitTransform = "none"), e.roundLengths) S = this.isHorizontal() ? $.outerWidth(!0) : $.outerHeight(!0);else if (this.isHorizontal()) {
                var X = parseFloat(B.getPropertyValue("width")),
                    V = parseFloat(B.getPropertyValue("padding-left")),
                    Y = parseFloat(B.getPropertyValue("padding-right")),
                    F = parseFloat(B.getPropertyValue("margin-left")),
                    W = parseFloat(B.getPropertyValue("margin-right")),
                    R = B.getPropertyValue("box-sizing");
                S = R && "border-box" === R ? X + F + W : X + V + Y + F + W;
              } else {
                var q = parseFloat(B.getPropertyValue("height")),
                    j = parseFloat(B.getPropertyValue("padding-top")),
                    K = parseFloat(B.getPropertyValue("padding-bottom")),
                    U = parseFloat(B.getPropertyValue("margin-top")),
                    _ = parseFloat(B.getPropertyValue("margin-bottom")),
                    Z = B.getPropertyValue("box-sizing");

                S = Z && "border-box" === Z ? q + U + _ : q + j + K + U + _;
              }
              H && ($[0].style.transform = H), N && ($[0].style.webkitTransform = N), e.roundLengths && (S = Math.floor(S));
            } else S = (s - (e.slidesPerView - 1) * w) / e.slidesPerView, e.roundLengths && (S = Math.floor(S)), d[k] && (this.isHorizontal() ? d[k].style.width = S + "px" : d[k].style.height = S + "px");

            d[k] && (d[k].swiperSlideSize = S), u.push(S), e.centeredSlides ? (y = y + S / 2 + x / 2 + w, 0 === x && 0 !== k && (y = y - s / 2 - w), 0 === k && (y = y - s / 2 - w), Math.abs(y) < .001 && (y = 0), e.roundLengths && (y = Math.floor(y)), T % e.slidesPerGroup == 0 && p.push(y), c.push(y)) : (e.roundLengths && (y = Math.floor(y)), T % e.slidesPerGroup == 0 && p.push(y), c.push(y), y = y + S + w), this.virtualSize += S + w, x = S, T += 1;
          }
        }

        if (this.virtualSize = Math.max(this.virtualSize, s) + m, a && r && ("slide" === e.effect || "coverflow" === e.effect) && i.css({
          width: this.virtualSize + e.spaceBetween + "px"
        }), e.setWrapperSize && (this.isHorizontal() ? i.css({
          width: this.virtualSize + e.spaceBetween + "px"
        }) : i.css({
          height: this.virtualSize + e.spaceBetween + "px"
        })), e.slidesPerColumn > 1 && (this.virtualSize = (S + e.spaceBetween) * E, this.virtualSize = Math.ceil(this.virtualSize / e.slidesPerColumn) - e.spaceBetween, this.isHorizontal() ? i.css({
          width: this.virtualSize + e.spaceBetween + "px"
        }) : i.css({
          height: this.virtualSize + e.spaceBetween + "px"
        }), e.centeredSlides)) {
          C = [];

          for (var Q = 0; Q < p.length; Q += 1) {
            var J = p[Q];
            e.roundLengths && (J = Math.floor(J)), p[Q] < this.virtualSize + p[0] && C.push(J);
          }

          p = C;
        }

        if (!e.centeredSlides) {
          C = [];

          for (var ee = 0; ee < p.length; ee += 1) {
            var te = p[ee];
            e.roundLengths && (te = Math.floor(te)), p[ee] <= this.virtualSize - s && C.push(te);
          }

          p = C, Math.floor(this.virtualSize - s) - Math.floor(p[p.length - 1]) > 1 && p.push(this.virtualSize - s);
        }

        if (0 === p.length && (p = [0]), 0 !== e.spaceBetween && (this.isHorizontal() ? a ? d.filter(v).css({
          marginLeft: w + "px"
        }) : d.filter(v).css({
          marginRight: w + "px"
        }) : d.filter(v).css({
          marginBottom: w + "px"
        })), e.centeredSlides && e.centeredSlidesBounds) {
          var ie = 0;
          u.forEach(function (t) {
            ie += t + (e.spaceBetween ? e.spaceBetween : 0);
          });
          var se = (ie -= e.spaceBetween) - s;
          p = p.map(function (e) {
            return e < 0 ? -f : e > se ? se + m : e;
          });
        }

        if (e.centerInsufficientSlides) {
          var ae = 0;

          if (u.forEach(function (t) {
            ae += t + (e.spaceBetween ? e.spaceBetween : 0);
          }), (ae -= e.spaceBetween) < s) {
            var re = (s - ae) / 2;
            p.forEach(function (e, t) {
              p[t] = e - re;
            }), c.forEach(function (e, t) {
              c[t] = e + re;
            });
          }
        }

        n.extend(this, {
          slides: d,
          snapGrid: p,
          slidesGrid: c,
          slidesSizesGrid: u
        }), h !== l && this.emit("slidesLengthChange"), p.length !== g && (this.params.watchOverflow && this.checkOverflow(), this.emit("snapGridLengthChange")), c.length !== b && this.emit("slidesGridLengthChange"), (e.watchSlidesProgress || e.watchSlidesVisibility) && this.updateSlidesOffset();
      }
    },
    updateAutoHeight: function updateAutoHeight(e) {
      var t,
          i = [],
          s = 0;
      if ("number" == typeof e ? this.setTransition(e) : !0 === e && this.setTransition(this.params.speed), "auto" !== this.params.slidesPerView && this.params.slidesPerView > 1) for (t = 0; t < Math.ceil(this.params.slidesPerView); t += 1) {
        var a = this.activeIndex + t;
        if (a > this.slides.length) break;
        i.push(this.slides.eq(a)[0]);
      } else i.push(this.slides.eq(this.activeIndex)[0]);

      for (t = 0; t < i.length; t += 1) {
        if (void 0 !== i[t]) {
          var r = i[t].offsetHeight;
          s = r > s ? r : s;
        }
      }

      s && this.$wrapperEl.css("height", s + "px");
    },
    updateSlidesOffset: function updateSlidesOffset() {
      for (var e = this.slides, t = 0; t < e.length; t += 1) {
        e[t].swiperSlideOffset = this.isHorizontal() ? e[t].offsetLeft : e[t].offsetTop;
      }
    },
    updateSlidesProgress: function updateSlidesProgress(e) {
      void 0 === e && (e = this && this.translate || 0);
      var t = this.params,
          i = this.slides,
          a = this.rtlTranslate;

      if (0 !== i.length) {
        void 0 === i[0].swiperSlideOffset && this.updateSlidesOffset();
        var r = -e;
        a && (r = e), i.removeClass(t.slideVisibleClass), this.visibleSlidesIndexes = [], this.visibleSlides = [];

        for (var n = 0; n < i.length; n += 1) {
          var o = i[n],
              l = (r + (t.centeredSlides ? this.minTranslate() : 0) - o.swiperSlideOffset) / (o.swiperSlideSize + t.spaceBetween);

          if (t.watchSlidesVisibility) {
            var d = -(r - o.swiperSlideOffset),
                h = d + this.slidesSizesGrid[n];
            (d >= 0 && d < this.size - 1 || h > 1 && h <= this.size || d <= 0 && h >= this.size) && (this.visibleSlides.push(o), this.visibleSlidesIndexes.push(n), i.eq(n).addClass(t.slideVisibleClass));
          }

          o.progress = a ? -l : l;
        }

        this.visibleSlides = s(this.visibleSlides);
      }
    },
    updateProgress: function updateProgress(e) {
      if (void 0 === e) {
        var t = this.rtlTranslate ? -1 : 1;
        e = this && this.translate && this.translate * t || 0;
      }

      var i = this.params,
          s = this.maxTranslate() - this.minTranslate(),
          a = this.progress,
          r = this.isBeginning,
          o = this.isEnd,
          l = r,
          d = o;
      0 === s ? (a = 0, r = !0, o = !0) : (r = (a = (e - this.minTranslate()) / s) <= 0, o = a >= 1), n.extend(this, {
        progress: a,
        isBeginning: r,
        isEnd: o
      }), (i.watchSlidesProgress || i.watchSlidesVisibility) && this.updateSlidesProgress(e), r && !l && this.emit("reachBeginning toEdge"), o && !d && this.emit("reachEnd toEdge"), (l && !r || d && !o) && this.emit("fromEdge"), this.emit("progress", a);
    },
    updateSlidesClasses: function updateSlidesClasses() {
      var e,
          t = this.slides,
          i = this.params,
          s = this.$wrapperEl,
          a = this.activeIndex,
          r = this.realIndex,
          n = this.virtual && i.virtual.enabled;
      t.removeClass(i.slideActiveClass + " " + i.slideNextClass + " " + i.slidePrevClass + " " + i.slideDuplicateActiveClass + " " + i.slideDuplicateNextClass + " " + i.slideDuplicatePrevClass), (e = n ? this.$wrapperEl.find("." + i.slideClass + '[data-swiper-slide-index="' + a + '"]') : t.eq(a)).addClass(i.slideActiveClass), i.loop && (e.hasClass(i.slideDuplicateClass) ? s.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + r + '"]').addClass(i.slideDuplicateActiveClass) : s.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + r + '"]').addClass(i.slideDuplicateActiveClass));
      var o = e.nextAll("." + i.slideClass).eq(0).addClass(i.slideNextClass);
      i.loop && 0 === o.length && (o = t.eq(0)).addClass(i.slideNextClass);
      var l = e.prevAll("." + i.slideClass).eq(0).addClass(i.slidePrevClass);
      i.loop && 0 === l.length && (l = t.eq(-1)).addClass(i.slidePrevClass), i.loop && (o.hasClass(i.slideDuplicateClass) ? s.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + o.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicateNextClass) : s.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + o.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicateNextClass), l.hasClass(i.slideDuplicateClass) ? s.children("." + i.slideClass + ":not(." + i.slideDuplicateClass + ')[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicatePrevClass) : s.children("." + i.slideClass + "." + i.slideDuplicateClass + '[data-swiper-slide-index="' + l.attr("data-swiper-slide-index") + '"]').addClass(i.slideDuplicatePrevClass));
    },
    updateActiveIndex: function updateActiveIndex(e) {
      var t,
          i = this.rtlTranslate ? this.translate : -this.translate,
          s = this.slidesGrid,
          a = this.snapGrid,
          r = this.params,
          o = this.activeIndex,
          l = this.realIndex,
          d = this.snapIndex,
          h = e;

      if (void 0 === h) {
        for (var p = 0; p < s.length; p += 1) {
          void 0 !== s[p + 1] ? i >= s[p] && i < s[p + 1] - (s[p + 1] - s[p]) / 2 ? h = p : i >= s[p] && i < s[p + 1] && (h = p + 1) : i >= s[p] && (h = p);
        }

        r.normalizeSlideIndex && (h < 0 || void 0 === h) && (h = 0);
      }

      if ((t = a.indexOf(i) >= 0 ? a.indexOf(i) : Math.floor(h / r.slidesPerGroup)) >= a.length && (t = a.length - 1), h !== o) {
        var c = parseInt(this.slides.eq(h).attr("data-swiper-slide-index") || h, 10);
        n.extend(this, {
          snapIndex: t,
          realIndex: c,
          previousIndex: o,
          activeIndex: h
        }), this.emit("activeIndexChange"), this.emit("snapIndexChange"), l !== c && this.emit("realIndexChange"), (this.initialized || this.runCallbacksOnInit) && this.emit("slideChange");
      } else t !== d && (this.snapIndex = t, this.emit("snapIndexChange"));
    },
    updateClickedSlide: function updateClickedSlide(e) {
      var t = this.params,
          i = s(e.target).closest("." + t.slideClass)[0],
          a = !1;
      if (i) for (var r = 0; r < this.slides.length; r += 1) {
        this.slides[r] === i && (a = !0);
      }
      if (!i || !a) return this.clickedSlide = void 0, void (this.clickedIndex = void 0);
      this.clickedSlide = i, this.virtual && this.params.virtual.enabled ? this.clickedIndex = parseInt(s(i).attr("data-swiper-slide-index"), 10) : this.clickedIndex = s(i).index(), t.slideToClickedSlide && void 0 !== this.clickedIndex && this.clickedIndex !== this.activeIndex && this.slideToClickedSlide();
    }
  };
  var p = {
    getTranslate: function getTranslate(e) {
      void 0 === e && (e = this.isHorizontal() ? "x" : "y");
      var t = this.params,
          i = this.rtlTranslate,
          s = this.translate,
          a = this.$wrapperEl;
      if (t.virtualTranslate) return i ? -s : s;
      if (t.cssMode) return s;
      var r = n.getTranslate(a[0], e);
      return i && (r = -r), r || 0;
    },
    setTranslate: function setTranslate(e, t) {
      var i = this.rtlTranslate,
          s = this.params,
          a = this.$wrapperEl,
          r = this.wrapperEl,
          n = this.progress,
          o = 0,
          l = 0;
      this.isHorizontal() ? o = i ? -e : e : l = e, s.roundLengths && (o = Math.floor(o), l = Math.floor(l)), s.cssMode ? r[this.isHorizontal() ? "scrollLeft" : "scrollTop"] = this.isHorizontal() ? -o : -l : s.virtualTranslate || a.transform("translate3d(" + o + "px, " + l + "px, 0px)"), this.previousTranslate = this.translate, this.translate = this.isHorizontal() ? o : l;
      var d = this.maxTranslate() - this.minTranslate();
      (0 === d ? 0 : (e - this.minTranslate()) / d) !== n && this.updateProgress(e), this.emit("setTranslate", this.translate, t);
    },
    minTranslate: function minTranslate() {
      return -this.snapGrid[0];
    },
    maxTranslate: function maxTranslate() {
      return -this.snapGrid[this.snapGrid.length - 1];
    },
    translateTo: function translateTo(e, t, i, s, a) {
      var r;
      void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0), void 0 === s && (s = !0);
      var n = this,
          o = n.params,
          l = n.wrapperEl;
      if (n.animating && o.preventInteractionOnTransition) return !1;
      var d,
          h = n.minTranslate(),
          p = n.maxTranslate();

      if (d = s && e > h ? h : s && e < p ? p : e, n.updateProgress(d), o.cssMode) {
        var c = n.isHorizontal();
        return 0 === t ? l[c ? "scrollLeft" : "scrollTop"] = -d : l.scrollTo ? l.scrollTo(((r = {})[c ? "left" : "top"] = -d, r.behavior = "smooth", r)) : l[c ? "scrollLeft" : "scrollTop"] = -d, !0;
      }

      return 0 === t ? (n.setTransition(0), n.setTranslate(d), i && (n.emit("beforeTransitionStart", t, a), n.emit("transitionEnd"))) : (n.setTransition(t), n.setTranslate(d), i && (n.emit("beforeTransitionStart", t, a), n.emit("transitionStart")), n.animating || (n.animating = !0, n.onTranslateToWrapperTransitionEnd || (n.onTranslateToWrapperTransitionEnd = function (e) {
        n && !n.destroyed && e.target === this && (n.$wrapperEl[0].removeEventListener("transitionend", n.onTranslateToWrapperTransitionEnd), n.$wrapperEl[0].removeEventListener("webkitTransitionEnd", n.onTranslateToWrapperTransitionEnd), n.onTranslateToWrapperTransitionEnd = null, delete n.onTranslateToWrapperTransitionEnd, i && n.emit("transitionEnd"));
      }), n.$wrapperEl[0].addEventListener("transitionend", n.onTranslateToWrapperTransitionEnd), n.$wrapperEl[0].addEventListener("webkitTransitionEnd", n.onTranslateToWrapperTransitionEnd))), !0;
    }
  };
  var c = {
    setTransition: function setTransition(e, t) {
      this.params.cssMode || this.$wrapperEl.transition(e), this.emit("setTransition", e, t);
    },
    transitionStart: function transitionStart(e, t) {
      void 0 === e && (e = !0);
      var i = this.activeIndex,
          s = this.params,
          a = this.previousIndex;

      if (!s.cssMode) {
        s.autoHeight && this.updateAutoHeight();
        var r = t;

        if (r || (r = i > a ? "next" : i < a ? "prev" : "reset"), this.emit("transitionStart"), e && i !== a) {
          if ("reset" === r) return void this.emit("slideResetTransitionStart");
          this.emit("slideChangeTransitionStart"), "next" === r ? this.emit("slideNextTransitionStart") : this.emit("slidePrevTransitionStart");
        }
      }
    },
    transitionEnd: function transitionEnd(e, t) {
      void 0 === e && (e = !0);
      var i = this.activeIndex,
          s = this.previousIndex,
          a = this.params;

      if (this.animating = !1, !a.cssMode) {
        this.setTransition(0);
        var r = t;

        if (r || (r = i > s ? "next" : i < s ? "prev" : "reset"), this.emit("transitionEnd"), e && i !== s) {
          if ("reset" === r) return void this.emit("slideResetTransitionEnd");
          this.emit("slideChangeTransitionEnd"), "next" === r ? this.emit("slideNextTransitionEnd") : this.emit("slidePrevTransitionEnd");
        }
      }
    }
  };
  var u = {
    slideTo: function slideTo(e, t, i, s) {
      var a;
      void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0);
      var r = this,
          n = e;
      n < 0 && (n = 0);
      var o = r.params,
          l = r.snapGrid,
          d = r.slidesGrid,
          h = r.previousIndex,
          p = r.activeIndex,
          c = r.rtlTranslate,
          u = r.wrapperEl;
      if (r.animating && o.preventInteractionOnTransition) return !1;
      var v = Math.floor(n / o.slidesPerGroup);
      v >= l.length && (v = l.length - 1), (p || o.initialSlide || 0) === (h || 0) && i && r.emit("beforeSlideChangeStart");
      var f,
          m = -l[v];
      if (r.updateProgress(m), o.normalizeSlideIndex) for (var g = 0; g < d.length; g += 1) {
        -Math.floor(100 * m) >= Math.floor(100 * d[g]) && (n = g);
      }

      if (r.initialized && n !== p) {
        if (!r.allowSlideNext && m < r.translate && m < r.minTranslate()) return !1;
        if (!r.allowSlidePrev && m > r.translate && m > r.maxTranslate() && (p || 0) !== n) return !1;
      }

      if (f = n > p ? "next" : n < p ? "prev" : "reset", c && -m === r.translate || !c && m === r.translate) return r.updateActiveIndex(n), o.autoHeight && r.updateAutoHeight(), r.updateSlidesClasses(), "slide" !== o.effect && r.setTranslate(m), "reset" !== f && (r.transitionStart(i, f), r.transitionEnd(i, f)), !1;

      if (o.cssMode) {
        var b = r.isHorizontal();
        return 0 === t ? u[b ? "scrollLeft" : "scrollTop"] = -m : u.scrollTo ? u.scrollTo(((a = {})[b ? "left" : "top"] = -m, a.behavior = "smooth", a)) : u[b ? "scrollLeft" : "scrollTop"] = -m, !0;
      }

      return 0 === t ? (r.setTransition(0), r.setTranslate(m), r.updateActiveIndex(n), r.updateSlidesClasses(), r.emit("beforeTransitionStart", t, s), r.transitionStart(i, f), r.transitionEnd(i, f)) : (r.setTransition(t), r.setTranslate(m), r.updateActiveIndex(n), r.updateSlidesClasses(), r.emit("beforeTransitionStart", t, s), r.transitionStart(i, f), r.animating || (r.animating = !0, r.onSlideToWrapperTransitionEnd || (r.onSlideToWrapperTransitionEnd = function (e) {
        r && !r.destroyed && e.target === this && (r.$wrapperEl[0].removeEventListener("transitionend", r.onSlideToWrapperTransitionEnd), r.$wrapperEl[0].removeEventListener("webkitTransitionEnd", r.onSlideToWrapperTransitionEnd), r.onSlideToWrapperTransitionEnd = null, delete r.onSlideToWrapperTransitionEnd, r.transitionEnd(i, f));
      }), r.$wrapperEl[0].addEventListener("transitionend", r.onSlideToWrapperTransitionEnd), r.$wrapperEl[0].addEventListener("webkitTransitionEnd", r.onSlideToWrapperTransitionEnd))), !0;
    },
    slideToLoop: function slideToLoop(e, t, i, s) {
      void 0 === e && (e = 0), void 0 === t && (t = this.params.speed), void 0 === i && (i = !0);
      var a = e;
      return this.params.loop && (a += this.loopedSlides), this.slideTo(a, t, i, s);
    },
    slideNext: function slideNext(e, t, i) {
      void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
      var s = this.params,
          a = this.animating;
      return s.loop ? !a && (this.loopFix(), this._clientLeft = this.$wrapperEl[0].clientLeft, this.slideTo(this.activeIndex + s.slidesPerGroup, e, t, i)) : this.slideTo(this.activeIndex + s.slidesPerGroup, e, t, i);
    },
    slidePrev: function slidePrev(e, t, i) {
      void 0 === e && (e = this.params.speed), void 0 === t && (t = !0);
      var s = this.params,
          a = this.animating,
          r = this.snapGrid,
          n = this.slidesGrid,
          o = this.rtlTranslate;

      if (s.loop) {
        if (a) return !1;
        this.loopFix(), this._clientLeft = this.$wrapperEl[0].clientLeft;
      }

      function l(e) {
        return e < 0 ? -Math.floor(Math.abs(e)) : Math.floor(e);
      }

      var d,
          h = l(o ? this.translate : -this.translate),
          p = r.map(function (e) {
        return l(e);
      }),
          c = (n.map(function (e) {
        return l(e);
      }), r[p.indexOf(h)], r[p.indexOf(h) - 1]);
      return void 0 === c && s.cssMode && r.forEach(function (e) {
        !c && h >= e && (c = e);
      }), void 0 !== c && (d = n.indexOf(c)) < 0 && (d = this.activeIndex - 1), this.slideTo(d, e, t, i);
    },
    slideReset: function slideReset(e, t, i) {
      return void 0 === e && (e = this.params.speed), void 0 === t && (t = !0), this.slideTo(this.activeIndex, e, t, i);
    },
    slideToClosest: function slideToClosest(e, t, i, s) {
      void 0 === e && (e = this.params.speed), void 0 === t && (t = !0), void 0 === s && (s = .5);
      var a = this.activeIndex,
          r = Math.floor(a / this.params.slidesPerGroup),
          n = this.rtlTranslate ? this.translate : -this.translate;

      if (n >= this.snapGrid[r]) {
        var o = this.snapGrid[r];
        n - o > (this.snapGrid[r + 1] - o) * s && (a += this.params.slidesPerGroup);
      } else {
        var l = this.snapGrid[r - 1];
        n - l <= (this.snapGrid[r] - l) * s && (a -= this.params.slidesPerGroup);
      }

      return a = Math.max(a, 0), a = Math.min(a, this.snapGrid.length - 1), this.slideTo(a, e, t, i);
    },
    slideToClickedSlide: function slideToClickedSlide() {
      var e,
          t = this,
          i = t.params,
          a = t.$wrapperEl,
          r = "auto" === i.slidesPerView ? t.slidesPerViewDynamic() : i.slidesPerView,
          o = t.clickedIndex;

      if (i.loop) {
        if (t.animating) return;
        e = parseInt(s(t.clickedSlide).attr("data-swiper-slide-index"), 10), i.centeredSlides ? o < t.loopedSlides - r / 2 || o > t.slides.length - t.loopedSlides + r / 2 ? (t.loopFix(), o = a.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + i.slideDuplicateClass + ")").eq(0).index(), n.nextTick(function () {
          t.slideTo(o);
        })) : t.slideTo(o) : o > t.slides.length - r ? (t.loopFix(), o = a.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]:not(.' + i.slideDuplicateClass + ")").eq(0).index(), n.nextTick(function () {
          t.slideTo(o);
        })) : t.slideTo(o);
      } else t.slideTo(o);
    }
  };
  var v = {
    loopCreate: function loopCreate() {
      var t = this,
          i = t.params,
          a = t.$wrapperEl;
      a.children("." + i.slideClass + "." + i.slideDuplicateClass).remove();
      var r = a.children("." + i.slideClass);

      if (i.loopFillGroupWithBlank) {
        var n = i.slidesPerGroup - r.length % i.slidesPerGroup;

        if (n !== i.slidesPerGroup) {
          for (var o = 0; o < n; o += 1) {
            var l = s(e.createElement("div")).addClass(i.slideClass + " " + i.slideBlankClass);
            a.append(l);
          }

          r = a.children("." + i.slideClass);
        }
      }

      "auto" !== i.slidesPerView || i.loopedSlides || (i.loopedSlides = r.length), t.loopedSlides = Math.ceil(parseFloat(i.loopedSlides || i.slidesPerView, 10)), t.loopedSlides += i.loopAdditionalSlides, t.loopedSlides > r.length && (t.loopedSlides = r.length);
      var d = [],
          h = [];
      r.each(function (e, i) {
        var a = s(i);
        e < t.loopedSlides && h.push(i), e < r.length && e >= r.length - t.loopedSlides && d.push(i), a.attr("data-swiper-slide-index", e);
      });

      for (var p = 0; p < h.length; p += 1) {
        a.append(s(h[p].cloneNode(!0)).addClass(i.slideDuplicateClass));
      }

      for (var c = d.length - 1; c >= 0; c -= 1) {
        a.prepend(s(d[c].cloneNode(!0)).addClass(i.slideDuplicateClass));
      }
    },
    loopFix: function loopFix() {
      this.emit("beforeLoopFix");
      var e,
          t = this.activeIndex,
          i = this.slides,
          s = this.loopedSlides,
          a = this.allowSlidePrev,
          r = this.allowSlideNext,
          n = this.snapGrid,
          o = this.rtlTranslate;
      this.allowSlidePrev = !0, this.allowSlideNext = !0;
      var l = -n[t] - this.getTranslate();
      if (t < s) e = i.length - 3 * s + t, e += s, this.slideTo(e, 0, !1, !0) && 0 !== l && this.setTranslate((o ? -this.translate : this.translate) - l);else if (t >= i.length - s) {
        e = -i.length + t + s, e += s, this.slideTo(e, 0, !1, !0) && 0 !== l && this.setTranslate((o ? -this.translate : this.translate) - l);
      }
      this.allowSlidePrev = a, this.allowSlideNext = r, this.emit("loopFix");
    },
    loopDestroy: function loopDestroy() {
      var e = this.$wrapperEl,
          t = this.params,
          i = this.slides;
      e.children("." + t.slideClass + "." + t.slideDuplicateClass + ",." + t.slideClass + "." + t.slideBlankClass).remove(), i.removeAttr("data-swiper-slide-index");
    }
  };
  var f = {
    setGrabCursor: function setGrabCursor(e) {
      if (!(o.touch || !this.params.simulateTouch || this.params.watchOverflow && this.isLocked || this.params.cssMode)) {
        var t = this.el;
        t.style.cursor = "move", t.style.cursor = e ? "-webkit-grabbing" : "-webkit-grab", t.style.cursor = e ? "-moz-grabbin" : "-moz-grab", t.style.cursor = e ? "grabbing" : "grab";
      }
    },
    unsetGrabCursor: function unsetGrabCursor() {
      o.touch || this.params.watchOverflow && this.isLocked || this.params.cssMode || (this.el.style.cursor = "");
    }
  };
  var m,
      g,
      b,
      w,
      y,
      x,
      T,
      E,
      S,
      C,
      M,
      P,
      z,
      k,
      $,
      L = {
    appendSlide: function appendSlide(e) {
      var t = this.$wrapperEl,
          i = this.params;
      if (i.loop && this.loopDestroy(), "object" == _typeof(e) && "length" in e) for (var s = 0; s < e.length; s += 1) {
        e[s] && t.append(e[s]);
      } else t.append(e);
      i.loop && this.loopCreate(), i.observer && o.observer || this.update();
    },
    prependSlide: function prependSlide(e) {
      var t = this.params,
          i = this.$wrapperEl,
          s = this.activeIndex;
      t.loop && this.loopDestroy();
      var a = s + 1;

      if ("object" == _typeof(e) && "length" in e) {
        for (var r = 0; r < e.length; r += 1) {
          e[r] && i.prepend(e[r]);
        }

        a = s + e.length;
      } else i.prepend(e);

      t.loop && this.loopCreate(), t.observer && o.observer || this.update(), this.slideTo(a, 0, !1);
    },
    addSlide: function addSlide(e, t) {
      var i = this.$wrapperEl,
          s = this.params,
          a = this.activeIndex;
      s.loop && (a -= this.loopedSlides, this.loopDestroy(), this.slides = i.children("." + s.slideClass));
      var r = this.slides.length;
      if (e <= 0) this.prependSlide(t);else if (e >= r) this.appendSlide(t);else {
        for (var n = a > e ? a + 1 : a, l = [], d = r - 1; d >= e; d -= 1) {
          var h = this.slides.eq(d);
          h.remove(), l.unshift(h);
        }

        if ("object" == _typeof(t) && "length" in t) {
          for (var p = 0; p < t.length; p += 1) {
            t[p] && i.append(t[p]);
          }

          n = a > e ? a + t.length : a;
        } else i.append(t);

        for (var c = 0; c < l.length; c += 1) {
          i.append(l[c]);
        }

        s.loop && this.loopCreate(), s.observer && o.observer || this.update(), s.loop ? this.slideTo(n + this.loopedSlides, 0, !1) : this.slideTo(n, 0, !1);
      }
    },
    removeSlide: function removeSlide(e) {
      var t = this.params,
          i = this.$wrapperEl,
          s = this.activeIndex;
      t.loop && (s -= this.loopedSlides, this.loopDestroy(), this.slides = i.children("." + t.slideClass));
      var a,
          r = s;

      if ("object" == _typeof(e) && "length" in e) {
        for (var n = 0; n < e.length; n += 1) {
          a = e[n], this.slides[a] && this.slides.eq(a).remove(), a < r && (r -= 1);
        }

        r = Math.max(r, 0);
      } else a = e, this.slides[a] && this.slides.eq(a).remove(), a < r && (r -= 1), r = Math.max(r, 0);

      t.loop && this.loopCreate(), t.observer && o.observer || this.update(), t.loop ? this.slideTo(r + this.loopedSlides, 0, !1) : this.slideTo(r, 0, !1);
    },
    removeAllSlides: function removeAllSlides() {
      for (var e = [], t = 0; t < this.slides.length; t += 1) {
        e.push(t);
      }

      this.removeSlide(e);
    }
  },
      I = (m = t.navigator.platform, g = t.navigator.userAgent, b = {
    ios: !1,
    android: !1,
    androidChrome: !1,
    desktop: !1,
    iphone: !1,
    ipod: !1,
    ipad: !1,
    edge: !1,
    ie: !1,
    firefox: !1,
    macos: !1,
    windows: !1,
    cordova: !(!t.cordova && !t.phonegap),
    phonegap: !(!t.cordova && !t.phonegap),
    electron: !1
  }, w = t.screen.width, y = t.screen.height, x = g.match(/(Android);?[\s\/]+([\d.]+)?/), T = g.match(/(iPad).*OS\s([\d_]+)/), E = g.match(/(iPod)(.*OS\s([\d_]+))?/), S = !T && g.match(/(iPhone\sOS|iOS)\s([\d_]+)/), C = g.indexOf("MSIE ") >= 0 || g.indexOf("Trident/") >= 0, M = g.indexOf("Edge/") >= 0, P = g.indexOf("Gecko/") >= 0 && g.indexOf("Firefox/") >= 0, z = "Win32" === m, k = g.toLowerCase().indexOf("electron") >= 0, $ = "MacIntel" === m, !T && $ && o.touch && (1024 === w && 1366 === y || 834 === w && 1194 === y || 834 === w && 1112 === y || 768 === w && 1024 === y) && (T = g.match(/(Version)\/([\d.]+)/), $ = !1), b.ie = C, b.edge = M, b.firefox = P, x && !z && (b.os = "android", b.osVersion = x[2], b.android = !0, b.androidChrome = g.toLowerCase().indexOf("chrome") >= 0), (T || S || E) && (b.os = "ios", b.ios = !0), S && !E && (b.osVersion = S[2].replace(/_/g, "."), b.iphone = !0), T && (b.osVersion = T[2].replace(/_/g, "."), b.ipad = !0), E && (b.osVersion = E[3] ? E[3].replace(/_/g, ".") : null, b.ipod = !0), b.ios && b.osVersion && g.indexOf("Version/") >= 0 && "10" === b.osVersion.split(".")[0] && (b.osVersion = g.toLowerCase().split("version/")[1].split(" ")[0]), b.webView = !(!(S || T || E) || !g.match(/.*AppleWebKit(?!.*Safari)/i) && !t.navigator.standalone) || t.matchMedia && t.matchMedia("(display-mode: standalone)").matches, b.webview = b.webView, b.standalone = b.webView, b.desktop = !(b.ios || b.android) || k, b.desktop && (b.electron = k, b.macos = $, b.windows = z, b.macos && (b.os = "macos"), b.windows && (b.os = "windows")), b.pixelRatio = t.devicePixelRatio || 1, b);

  function D(i) {
    var a = this.touchEventsData,
        r = this.params,
        o = this.touches;

    if (!this.animating || !r.preventInteractionOnTransition) {
      var l = i;
      l.originalEvent && (l = l.originalEvent);
      var d = s(l.target);
      if (("wrapper" !== r.touchEventsTarget || d.closest(this.wrapperEl).length) && (a.isTouchEvent = "touchstart" === l.type, (a.isTouchEvent || !("which" in l) || 3 !== l.which) && !(!a.isTouchEvent && "button" in l && l.button > 0 || a.isTouched && a.isMoved))) if (r.noSwiping && d.closest(r.noSwipingSelector ? r.noSwipingSelector : "." + r.noSwipingClass)[0]) this.allowClick = !0;else if (!r.swipeHandler || d.closest(r.swipeHandler)[0]) {
        o.currentX = "touchstart" === l.type ? l.targetTouches[0].pageX : l.pageX, o.currentY = "touchstart" === l.type ? l.targetTouches[0].pageY : l.pageY;
        var h = o.currentX,
            p = o.currentY,
            c = r.edgeSwipeDetection || r.iOSEdgeSwipeDetection,
            u = r.edgeSwipeThreshold || r.iOSEdgeSwipeThreshold;

        if (!c || !(h <= u || h >= t.screen.width - u)) {
          if (n.extend(a, {
            isTouched: !0,
            isMoved: !1,
            allowTouchCallbacks: !0,
            isScrolling: void 0,
            startMoving: void 0
          }), o.startX = h, o.startY = p, a.touchStartTime = n.now(), this.allowClick = !0, this.updateSize(), this.swipeDirection = void 0, r.threshold > 0 && (a.allowThresholdMove = !1), "touchstart" !== l.type) {
            var v = !0;
            d.is(a.formElements) && (v = !1), e.activeElement && s(e.activeElement).is(a.formElements) && e.activeElement !== d[0] && e.activeElement.blur();
            var f = v && this.allowTouchMove && r.touchStartPreventDefault;
            (r.touchStartForcePreventDefault || f) && l.preventDefault();
          }

          this.emit("touchStart", l);
        }
      }
    }
  }

  function O(t) {
    var i = this.touchEventsData,
        a = this.params,
        r = this.touches,
        o = this.rtlTranslate,
        l = t;

    if (l.originalEvent && (l = l.originalEvent), i.isTouched) {
      if (!i.isTouchEvent || "mousemove" !== l.type) {
        var d = "touchmove" === l.type && l.targetTouches && (l.targetTouches[0] || l.changedTouches[0]),
            h = "touchmove" === l.type ? d.pageX : l.pageX,
            p = "touchmove" === l.type ? d.pageY : l.pageY;
        if (l.preventedByNestedSwiper) return r.startX = h, void (r.startY = p);
        if (!this.allowTouchMove) return this.allowClick = !1, void (i.isTouched && (n.extend(r, {
          startX: h,
          startY: p,
          currentX: h,
          currentY: p
        }), i.touchStartTime = n.now()));
        if (i.isTouchEvent && a.touchReleaseOnEdges && !a.loop) if (this.isVertical()) {
          if (p < r.startY && this.translate <= this.maxTranslate() || p > r.startY && this.translate >= this.minTranslate()) return i.isTouched = !1, void (i.isMoved = !1);
        } else if (h < r.startX && this.translate <= this.maxTranslate() || h > r.startX && this.translate >= this.minTranslate()) return;
        if (i.isTouchEvent && e.activeElement && l.target === e.activeElement && s(l.target).is(i.formElements)) return i.isMoved = !0, void (this.allowClick = !1);

        if (i.allowTouchCallbacks && this.emit("touchMove", l), !(l.targetTouches && l.targetTouches.length > 1)) {
          r.currentX = h, r.currentY = p;
          var c = r.currentX - r.startX,
              u = r.currentY - r.startY;

          if (!(this.params.threshold && Math.sqrt(Math.pow(c, 2) + Math.pow(u, 2)) < this.params.threshold)) {
            var v;
            if (void 0 === i.isScrolling) this.isHorizontal() && r.currentY === r.startY || this.isVertical() && r.currentX === r.startX ? i.isScrolling = !1 : c * c + u * u >= 25 && (v = 180 * Math.atan2(Math.abs(u), Math.abs(c)) / Math.PI, i.isScrolling = this.isHorizontal() ? v > a.touchAngle : 90 - v > a.touchAngle);
            if (i.isScrolling && this.emit("touchMoveOpposite", l), void 0 === i.startMoving && (r.currentX === r.startX && r.currentY === r.startY || (i.startMoving = !0)), i.isScrolling) i.isTouched = !1;else if (i.startMoving) {
              this.allowClick = !1, a.cssMode || l.preventDefault(), a.touchMoveStopPropagation && !a.nested && l.stopPropagation(), i.isMoved || (a.loop && this.loopFix(), i.startTranslate = this.getTranslate(), this.setTransition(0), this.animating && this.$wrapperEl.trigger("webkitTransitionEnd transitionend"), i.allowMomentumBounce = !1, !a.grabCursor || !0 !== this.allowSlideNext && !0 !== this.allowSlidePrev || this.setGrabCursor(!0), this.emit("sliderFirstMove", l)), this.emit("sliderMove", l), i.isMoved = !0;
              var f = this.isHorizontal() ? c : u;
              r.diff = f, f *= a.touchRatio, o && (f = -f), this.swipeDirection = f > 0 ? "prev" : "next", i.currentTranslate = f + i.startTranslate;
              var m = !0,
                  g = a.resistanceRatio;

              if (a.touchReleaseOnEdges && (g = 0), f > 0 && i.currentTranslate > this.minTranslate() ? (m = !1, a.resistance && (i.currentTranslate = this.minTranslate() - 1 + Math.pow(-this.minTranslate() + i.startTranslate + f, g))) : f < 0 && i.currentTranslate < this.maxTranslate() && (m = !1, a.resistance && (i.currentTranslate = this.maxTranslate() + 1 - Math.pow(this.maxTranslate() - i.startTranslate - f, g))), m && (l.preventedByNestedSwiper = !0), !this.allowSlideNext && "next" === this.swipeDirection && i.currentTranslate < i.startTranslate && (i.currentTranslate = i.startTranslate), !this.allowSlidePrev && "prev" === this.swipeDirection && i.currentTranslate > i.startTranslate && (i.currentTranslate = i.startTranslate), a.threshold > 0) {
                if (!(Math.abs(f) > a.threshold || i.allowThresholdMove)) return void (i.currentTranslate = i.startTranslate);
                if (!i.allowThresholdMove) return i.allowThresholdMove = !0, r.startX = r.currentX, r.startY = r.currentY, i.currentTranslate = i.startTranslate, void (r.diff = this.isHorizontal() ? r.currentX - r.startX : r.currentY - r.startY);
              }

              a.followFinger && !a.cssMode && ((a.freeMode || a.watchSlidesProgress || a.watchSlidesVisibility) && (this.updateActiveIndex(), this.updateSlidesClasses()), a.freeMode && (0 === i.velocities.length && i.velocities.push({
                position: r[this.isHorizontal() ? "startX" : "startY"],
                time: i.touchStartTime
              }), i.velocities.push({
                position: r[this.isHorizontal() ? "currentX" : "currentY"],
                time: n.now()
              })), this.updateProgress(i.currentTranslate), this.setTranslate(i.currentTranslate));
            }
          }
        }
      }
    } else i.startMoving && i.isScrolling && this.emit("touchMoveOpposite", l);
  }

  function A(e) {
    var t = this,
        i = t.touchEventsData,
        s = t.params,
        a = t.touches,
        r = t.rtlTranslate,
        o = t.$wrapperEl,
        l = t.slidesGrid,
        d = t.snapGrid,
        h = e;
    if (h.originalEvent && (h = h.originalEvent), i.allowTouchCallbacks && t.emit("touchEnd", h), i.allowTouchCallbacks = !1, !i.isTouched) return i.isMoved && s.grabCursor && t.setGrabCursor(!1), i.isMoved = !1, void (i.startMoving = !1);
    s.grabCursor && i.isMoved && i.isTouched && (!0 === t.allowSlideNext || !0 === t.allowSlidePrev) && t.setGrabCursor(!1);
    var p,
        c = n.now(),
        u = c - i.touchStartTime;
    if (t.allowClick && (t.updateClickedSlide(h), t.emit("tap click", h), u < 300 && c - i.lastClickTime < 300 && t.emit("doubleTap doubleClick", h)), i.lastClickTime = n.now(), n.nextTick(function () {
      t.destroyed || (t.allowClick = !0);
    }), !i.isTouched || !i.isMoved || !t.swipeDirection || 0 === a.diff || i.currentTranslate === i.startTranslate) return i.isTouched = !1, i.isMoved = !1, void (i.startMoving = !1);
    if (i.isTouched = !1, i.isMoved = !1, i.startMoving = !1, p = s.followFinger ? r ? t.translate : -t.translate : -i.currentTranslate, !s.cssMode) if (s.freeMode) {
      if (p < -t.minTranslate()) return void t.slideTo(t.activeIndex);
      if (p > -t.maxTranslate()) return void (t.slides.length < d.length ? t.slideTo(d.length - 1) : t.slideTo(t.slides.length - 1));

      if (s.freeModeMomentum) {
        if (i.velocities.length > 1) {
          var v = i.velocities.pop(),
              f = i.velocities.pop(),
              m = v.position - f.position,
              g = v.time - f.time;
          t.velocity = m / g, t.velocity /= 2, Math.abs(t.velocity) < s.freeModeMinimumVelocity && (t.velocity = 0), (g > 150 || n.now() - v.time > 300) && (t.velocity = 0);
        } else t.velocity = 0;

        t.velocity *= s.freeModeMomentumVelocityRatio, i.velocities.length = 0;
        var b = 1e3 * s.freeModeMomentumRatio,
            w = t.velocity * b,
            y = t.translate + w;
        r && (y = -y);
        var x,
            T,
            E = !1,
            S = 20 * Math.abs(t.velocity) * s.freeModeMomentumBounceRatio;
        if (y < t.maxTranslate()) s.freeModeMomentumBounce ? (y + t.maxTranslate() < -S && (y = t.maxTranslate() - S), x = t.maxTranslate(), E = !0, i.allowMomentumBounce = !0) : y = t.maxTranslate(), s.loop && s.centeredSlides && (T = !0);else if (y > t.minTranslate()) s.freeModeMomentumBounce ? (y - t.minTranslate() > S && (y = t.minTranslate() + S), x = t.minTranslate(), E = !0, i.allowMomentumBounce = !0) : y = t.minTranslate(), s.loop && s.centeredSlides && (T = !0);else if (s.freeModeSticky) {
          for (var C, M = 0; M < d.length; M += 1) {
            if (d[M] > -y) {
              C = M;
              break;
            }
          }

          y = -(y = Math.abs(d[C] - y) < Math.abs(d[C - 1] - y) || "next" === t.swipeDirection ? d[C] : d[C - 1]);
        }

        if (T && t.once("transitionEnd", function () {
          t.loopFix();
        }), 0 !== t.velocity) {
          if (b = r ? Math.abs((-y - t.translate) / t.velocity) : Math.abs((y - t.translate) / t.velocity), s.freeModeSticky) {
            var P = Math.abs((r ? -y : y) - t.translate),
                z = t.slidesSizesGrid[t.activeIndex];
            b = P < z ? s.speed : P < 2 * z ? 1.5 * s.speed : 2.5 * s.speed;
          }
        } else if (s.freeModeSticky) return void t.slideToClosest();

        s.freeModeMomentumBounce && E ? (t.updateProgress(x), t.setTransition(b), t.setTranslate(y), t.transitionStart(!0, t.swipeDirection), t.animating = !0, o.transitionEnd(function () {
          t && !t.destroyed && i.allowMomentumBounce && (t.emit("momentumBounce"), t.setTransition(s.speed), t.setTranslate(x), o.transitionEnd(function () {
            t && !t.destroyed && t.transitionEnd();
          }));
        })) : t.velocity ? (t.updateProgress(y), t.setTransition(b), t.setTranslate(y), t.transitionStart(!0, t.swipeDirection), t.animating || (t.animating = !0, o.transitionEnd(function () {
          t && !t.destroyed && t.transitionEnd();
        }))) : t.updateProgress(y), t.updateActiveIndex(), t.updateSlidesClasses();
      } else if (s.freeModeSticky) return void t.slideToClosest();

      (!s.freeModeMomentum || u >= s.longSwipesMs) && (t.updateProgress(), t.updateActiveIndex(), t.updateSlidesClasses());
    } else {
      for (var k = 0, $ = t.slidesSizesGrid[0], L = 0; L < l.length; L += s.slidesPerGroup) {
        void 0 !== l[L + s.slidesPerGroup] ? p >= l[L] && p < l[L + s.slidesPerGroup] && (k = L, $ = l[L + s.slidesPerGroup] - l[L]) : p >= l[L] && (k = L, $ = l[l.length - 1] - l[l.length - 2]);
      }

      var I = (p - l[k]) / $;

      if (u > s.longSwipesMs) {
        if (!s.longSwipes) return void t.slideTo(t.activeIndex);
        "next" === t.swipeDirection && (I >= s.longSwipesRatio ? t.slideTo(k + s.slidesPerGroup) : t.slideTo(k)), "prev" === t.swipeDirection && (I > 1 - s.longSwipesRatio ? t.slideTo(k + s.slidesPerGroup) : t.slideTo(k));
      } else {
        if (!s.shortSwipes) return void t.slideTo(t.activeIndex);
        t.navigation && (h.target === t.navigation.nextEl || h.target === t.navigation.prevEl) ? h.target === t.navigation.nextEl ? t.slideTo(k + s.slidesPerGroup) : t.slideTo(k) : ("next" === t.swipeDirection && t.slideTo(k + s.slidesPerGroup), "prev" === t.swipeDirection && t.slideTo(k));
      }
    }
  }

  function G() {
    var e = this.params,
        t = this.el;

    if (!t || 0 !== t.offsetWidth) {
      e.breakpoints && this.setBreakpoint();
      var i = this.allowSlideNext,
          s = this.allowSlidePrev,
          a = this.snapGrid;
      this.allowSlideNext = !0, this.allowSlidePrev = !0, this.updateSize(), this.updateSlides(), this.updateSlidesClasses(), ("auto" === e.slidesPerView || e.slidesPerView > 1) && this.isEnd && !this.params.centeredSlides ? this.slideTo(this.slides.length - 1, 0, !1, !0) : this.slideTo(this.activeIndex, 0, !1, !0), this.autoplay && this.autoplay.running && this.autoplay.paused && this.autoplay.run(), this.allowSlidePrev = s, this.allowSlideNext = i, this.params.watchOverflow && a !== this.snapGrid && this.checkOverflow();
    }
  }

  function B(e) {
    this.allowClick || (this.params.preventClicks && e.preventDefault(), this.params.preventClicksPropagation && this.animating && (e.stopPropagation(), e.stopImmediatePropagation()));
  }

  function H() {
    var e = this.wrapperEl;
    this.previousTranslate = this.translate, this.translate = this.isHorizontal() ? -e.scrollLeft : -e.scrollTop, -0 === this.translate && (this.translate = 0), this.updateActiveIndex(), this.updateSlidesClasses();
    var t = this.maxTranslate() - this.minTranslate();
    (0 === t ? 0 : (this.translate - this.minTranslate()) / t) !== this.progress && this.updateProgress(this.translate), this.emit("setTranslate", this.translate, !1);
  }

  var N = !1;

  function X() {}

  var V = {
    init: !0,
    direction: "horizontal",
    touchEventsTarget: "container",
    initialSlide: 0,
    speed: 300,
    cssMode: !1,
    updateOnWindowResize: !0,
    preventInteractionOnTransition: !1,
    edgeSwipeDetection: !1,
    edgeSwipeThreshold: 20,
    freeMode: !1,
    freeModeMomentum: !0,
    freeModeMomentumRatio: 1,
    freeModeMomentumBounce: !0,
    freeModeMomentumBounceRatio: 1,
    freeModeMomentumVelocityRatio: 1,
    freeModeSticky: !1,
    freeModeMinimumVelocity: .02,
    autoHeight: !1,
    setWrapperSize: !1,
    virtualTranslate: !1,
    effect: "slide",
    breakpoints: void 0,
    spaceBetween: 0,
    slidesPerView: 1,
    slidesPerColumn: 1,
    slidesPerColumnFill: "column",
    slidesPerGroup: 1,
    centeredSlides: !1,
    centeredSlidesBounds: !1,
    slidesOffsetBefore: 0,
    slidesOffsetAfter: 0,
    normalizeSlideIndex: !0,
    centerInsufficientSlides: !1,
    watchOverflow: !1,
    roundLengths: !1,
    touchRatio: 1,
    touchAngle: 45,
    simulateTouch: !0,
    shortSwipes: !0,
    longSwipes: !0,
    longSwipesRatio: .5,
    longSwipesMs: 300,
    followFinger: !0,
    allowTouchMove: !0,
    threshold: 0,
    touchMoveStopPropagation: !1,
    touchStartPreventDefault: !0,
    touchStartForcePreventDefault: !1,
    touchReleaseOnEdges: !1,
    uniqueNavElements: !0,
    resistance: !0,
    resistanceRatio: .85,
    watchSlidesProgress: !1,
    watchSlidesVisibility: !1,
    grabCursor: !1,
    preventClicks: !0,
    preventClicksPropagation: !0,
    slideToClickedSlide: !1,
    preloadImages: !0,
    updateOnImagesReady: !0,
    loop: !1,
    loopAdditionalSlides: 0,
    loopedSlides: null,
    loopFillGroupWithBlank: !1,
    allowSlidePrev: !0,
    allowSlideNext: !0,
    swipeHandler: null,
    noSwiping: !0,
    noSwipingClass: "swiper-no-swiping",
    noSwipingSelector: null,
    passiveListeners: !0,
    containerModifierClass: "swiper-container-",
    slideClass: "swiper-slide",
    slideBlankClass: "swiper-slide-invisible-blank",
    slideActiveClass: "swiper-slide-active",
    slideDuplicateActiveClass: "swiper-slide-duplicate-active",
    slideVisibleClass: "swiper-slide-visible",
    slideDuplicateClass: "swiper-slide-duplicate",
    slideNextClass: "swiper-slide-next",
    slideDuplicateNextClass: "swiper-slide-duplicate-next",
    slidePrevClass: "swiper-slide-prev",
    slideDuplicatePrevClass: "swiper-slide-duplicate-prev",
    wrapperClass: "swiper-wrapper",
    runCallbacksOnInit: !0
  },
      Y = {
    update: h,
    translate: p,
    transition: c,
    slide: u,
    loop: v,
    grabCursor: f,
    manipulation: L,
    events: {
      attachEvents: function attachEvents() {
        var t = this.params,
            i = this.touchEvents,
            s = this.el,
            a = this.wrapperEl;
        this.onTouchStart = D.bind(this), this.onTouchMove = O.bind(this), this.onTouchEnd = A.bind(this), t.cssMode && (this.onScroll = H.bind(this)), this.onClick = B.bind(this);
        var r = !!t.nested;
        if (!o.touch && o.pointerEvents) s.addEventListener(i.start, this.onTouchStart, !1), e.addEventListener(i.move, this.onTouchMove, r), e.addEventListener(i.end, this.onTouchEnd, !1);else {
          if (o.touch) {
            var n = !("touchstart" !== i.start || !o.passiveListener || !t.passiveListeners) && {
              passive: !0,
              capture: !1
            };
            s.addEventListener(i.start, this.onTouchStart, n), s.addEventListener(i.move, this.onTouchMove, o.passiveListener ? {
              passive: !1,
              capture: r
            } : r), s.addEventListener(i.end, this.onTouchEnd, n), i.cancel && s.addEventListener(i.cancel, this.onTouchEnd, n), N || (e.addEventListener("touchstart", X), N = !0);
          }

          (t.simulateTouch && !I.ios && !I.android || t.simulateTouch && !o.touch && I.ios) && (s.addEventListener("mousedown", this.onTouchStart, !1), e.addEventListener("mousemove", this.onTouchMove, r), e.addEventListener("mouseup", this.onTouchEnd, !1));
        }
        (t.preventClicks || t.preventClicksPropagation) && s.addEventListener("click", this.onClick, !0), t.cssMode && a.addEventListener("scroll", this.onScroll), t.updateOnWindowResize ? this.on(I.ios || I.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", G, !0) : this.on("observerUpdate", G, !0);
      },
      detachEvents: function detachEvents() {
        var t = this.params,
            i = this.touchEvents,
            s = this.el,
            a = this.wrapperEl,
            r = !!t.nested;
        if (!o.touch && o.pointerEvents) s.removeEventListener(i.start, this.onTouchStart, !1), e.removeEventListener(i.move, this.onTouchMove, r), e.removeEventListener(i.end, this.onTouchEnd, !1);else {
          if (o.touch) {
            var n = !("onTouchStart" !== i.start || !o.passiveListener || !t.passiveListeners) && {
              passive: !0,
              capture: !1
            };
            s.removeEventListener(i.start, this.onTouchStart, n), s.removeEventListener(i.move, this.onTouchMove, r), s.removeEventListener(i.end, this.onTouchEnd, n), i.cancel && s.removeEventListener(i.cancel, this.onTouchEnd, n);
          }

          (t.simulateTouch && !I.ios && !I.android || t.simulateTouch && !o.touch && I.ios) && (s.removeEventListener("mousedown", this.onTouchStart, !1), e.removeEventListener("mousemove", this.onTouchMove, r), e.removeEventListener("mouseup", this.onTouchEnd, !1));
        }
        (t.preventClicks || t.preventClicksPropagation) && s.removeEventListener("click", this.onClick, !0), t.cssMode && a.removeEventListener("scroll", this.onScroll), this.off(I.ios || I.android ? "resize orientationchange observerUpdate" : "resize observerUpdate", G);
      }
    },
    breakpoints: {
      setBreakpoint: function setBreakpoint() {
        var e = this.activeIndex,
            t = this.initialized,
            i = this.loopedSlides;
        void 0 === i && (i = 0);
        var s = this.params,
            a = this.$el,
            r = s.breakpoints;

        if (r && (!r || 0 !== Object.keys(r).length)) {
          var o = this.getBreakpoint(r);

          if (o && this.currentBreakpoint !== o) {
            var l = o in r ? r[o] : void 0;
            l && ["slidesPerView", "spaceBetween", "slidesPerGroup", "slidesPerColumn"].forEach(function (e) {
              var t = l[e];
              void 0 !== t && (l[e] = "slidesPerView" !== e || "AUTO" !== t && "auto" !== t ? "slidesPerView" === e ? parseFloat(t) : parseInt(t, 10) : "auto");
            });
            var d = l || this.originalParams,
                h = s.slidesPerColumn > 1,
                p = d.slidesPerColumn > 1;
            h && !p ? a.removeClass(s.containerModifierClass + "multirow " + s.containerModifierClass + "multirow-column") : !h && p && (a.addClass(s.containerModifierClass + "multirow"), "column" === d.slidesPerColumnFill && a.addClass(s.containerModifierClass + "multirow-column"));
            var c = d.direction && d.direction !== s.direction,
                u = s.loop && (d.slidesPerView !== s.slidesPerView || c);
            c && t && this.changeDirection(), n.extend(this.params, d), n.extend(this, {
              allowTouchMove: this.params.allowTouchMove,
              allowSlideNext: this.params.allowSlideNext,
              allowSlidePrev: this.params.allowSlidePrev
            }), this.currentBreakpoint = o, u && t && (this.loopDestroy(), this.loopCreate(), this.updateSlides(), this.slideTo(e - i + this.loopedSlides, 0, !1)), this.emit("breakpoint", d);
          }
        }
      },
      getBreakpoint: function getBreakpoint(e) {
        if (e) {
          var i = !1,
              s = [];
          Object.keys(e).forEach(function (e) {
            s.push(e);
          }), s.sort(function (e, t) {
            return parseInt(e, 10) - parseInt(t, 10);
          });

          for (var a = 0; a < s.length; a += 1) {
            var r = s[a];
            r <= t.innerWidth && (i = r);
          }

          return i || "max";
        }
      }
    },
    checkOverflow: {
      checkOverflow: function checkOverflow() {
        var e = this.params,
            t = this.isLocked,
            i = this.slides.length > 0 && e.slidesOffsetBefore + e.spaceBetween * (this.slides.length - 1) + this.slides[0].offsetWidth * this.slides.length;
        e.slidesOffsetBefore && e.slidesOffsetAfter && i ? this.isLocked = i <= this.size : this.isLocked = 1 === this.snapGrid.length, this.allowSlideNext = !this.isLocked, this.allowSlidePrev = !this.isLocked, t !== this.isLocked && this.emit(this.isLocked ? "lock" : "unlock"), t && t !== this.isLocked && (this.isEnd = !1, this.navigation.update());
      }
    },
    classes: {
      addClasses: function addClasses() {
        var e = this.classNames,
            t = this.params,
            i = this.rtl,
            s = this.$el,
            a = [];
        a.push("initialized"), a.push(t.direction), t.freeMode && a.push("free-mode"), t.autoHeight && a.push("autoheight"), i && a.push("rtl"), t.slidesPerColumn > 1 && (a.push("multirow"), "column" === t.slidesPerColumnFill && a.push("multirow-column")), I.android && a.push("android"), I.ios && a.push("ios"), t.cssMode && a.push("css-mode"), a.forEach(function (i) {
          e.push(t.containerModifierClass + i);
        }), s.addClass(e.join(" "));
      },
      removeClasses: function removeClasses() {
        var e = this.$el,
            t = this.classNames;
        e.removeClass(t.join(" "));
      }
    },
    images: {
      loadImage: function loadImage(e, i, s, a, r, n) {
        var o;

        function l() {
          n && n();
        }

        e.complete && r ? l() : i ? ((o = new t.Image()).onload = l, o.onerror = l, a && (o.sizes = a), s && (o.srcset = s), i && (o.src = i)) : l();
      },
      preloadImages: function preloadImages() {
        var e = this;

        function t() {
          null != e && e && !e.destroyed && (void 0 !== e.imagesLoaded && (e.imagesLoaded += 1), e.imagesLoaded === e.imagesToLoad.length && (e.params.updateOnImagesReady && e.update(), e.emit("imagesReady")));
        }

        e.imagesToLoad = e.$el.find("img");

        for (var i = 0; i < e.imagesToLoad.length; i += 1) {
          var s = e.imagesToLoad[i];
          e.loadImage(s, s.currentSrc || s.getAttribute("src"), s.srcset || s.getAttribute("srcset"), s.sizes || s.getAttribute("sizes"), !0, t);
        }
      }
    }
  },
      F = {},
      W = function (e) {
    function t() {
      for (var i, a, r, l = [], d = arguments.length; d--;) {
        l[d] = arguments[d];
      }

      1 === l.length && l[0].constructor && l[0].constructor === Object ? r = l[0] : (a = (i = l)[0], r = i[1]), r || (r = {}), r = n.extend({}, r), a && !r.el && (r.el = a), e.call(this, r), Object.keys(Y).forEach(function (e) {
        Object.keys(Y[e]).forEach(function (i) {
          t.prototype[i] || (t.prototype[i] = Y[e][i]);
        });
      });
      var h = this;
      void 0 === h.modules && (h.modules = {}), Object.keys(h.modules).forEach(function (e) {
        var t = h.modules[e];

        if (t.params) {
          var i = Object.keys(t.params)[0],
              s = t.params[i];
          if ("object" != _typeof(s) || null === s) return;
          if (!(i in r && "enabled" in s)) return;
          !0 === r[i] && (r[i] = {
            enabled: !0
          }), "object" != _typeof(r[i]) || "enabled" in r[i] || (r[i].enabled = !0), r[i] || (r[i] = {
            enabled: !1
          });
        }
      });
      var p = n.extend({}, V);
      h.useModulesParams(p), h.params = n.extend({}, p, F, r), h.originalParams = n.extend({}, h.params), h.passedParams = n.extend({}, r), h.$ = s;
      var c = s(h.params.el);

      if (a = c[0]) {
        if (c.length > 1) {
          var u = [];
          return c.each(function (e, i) {
            var s = n.extend({}, r, {
              el: i
            });
            u.push(new t(s));
          }), u;
        }

        var v, f, m;
        return a.swiper = h, c.data("swiper", h), a && a.shadowRoot && a.shadowRoot.querySelector ? (v = s(a.shadowRoot.querySelector("." + h.params.wrapperClass))).children = function (e) {
          return c.children(e);
        } : v = c.children("." + h.params.wrapperClass), n.extend(h, {
          $el: c,
          el: a,
          $wrapperEl: v,
          wrapperEl: v[0],
          classNames: [],
          slides: s(),
          slidesGrid: [],
          snapGrid: [],
          slidesSizesGrid: [],
          isHorizontal: function isHorizontal() {
            return "horizontal" === h.params.direction;
          },
          isVertical: function isVertical() {
            return "vertical" === h.params.direction;
          },
          rtl: "rtl" === a.dir.toLowerCase() || "rtl" === c.css("direction"),
          rtlTranslate: "horizontal" === h.params.direction && ("rtl" === a.dir.toLowerCase() || "rtl" === c.css("direction")),
          wrongRTL: "-webkit-box" === v.css("display"),
          activeIndex: 0,
          realIndex: 0,
          isBeginning: !0,
          isEnd: !1,
          translate: 0,
          previousTranslate: 0,
          progress: 0,
          velocity: 0,
          animating: !1,
          allowSlideNext: h.params.allowSlideNext,
          allowSlidePrev: h.params.allowSlidePrev,
          touchEvents: (f = ["touchstart", "touchmove", "touchend", "touchcancel"], m = ["mousedown", "mousemove", "mouseup"], o.pointerEvents && (m = ["pointerdown", "pointermove", "pointerup"]), h.touchEventsTouch = {
            start: f[0],
            move: f[1],
            end: f[2],
            cancel: f[3]
          }, h.touchEventsDesktop = {
            start: m[0],
            move: m[1],
            end: m[2]
          }, o.touch || !h.params.simulateTouch ? h.touchEventsTouch : h.touchEventsDesktop),
          touchEventsData: {
            isTouched: void 0,
            isMoved: void 0,
            allowTouchCallbacks: void 0,
            touchStartTime: void 0,
            isScrolling: void 0,
            currentTranslate: void 0,
            startTranslate: void 0,
            allowThresholdMove: void 0,
            formElements: "input, select, option, textarea, button, video",
            lastClickTime: n.now(),
            clickTimeout: void 0,
            velocities: [],
            allowMomentumBounce: void 0,
            isTouchEvent: void 0,
            startMoving: void 0
          },
          allowClick: !0,
          allowTouchMove: h.params.allowTouchMove,
          touches: {
            startX: 0,
            startY: 0,
            currentX: 0,
            currentY: 0,
            diff: 0
          },
          imagesToLoad: [],
          imagesLoaded: 0
        }), h.useModules(), h.params.init && h.init(), h;
      }
    }

    e && (t.__proto__ = e), t.prototype = Object.create(e && e.prototype), t.prototype.constructor = t;
    var i = {
      extendedDefaults: {
        configurable: !0
      },
      defaults: {
        configurable: !0
      },
      Class: {
        configurable: !0
      },
      $: {
        configurable: !0
      }
    };
    return t.prototype.slidesPerViewDynamic = function () {
      var e = this.params,
          t = this.slides,
          i = this.slidesGrid,
          s = this.size,
          a = this.activeIndex,
          r = 1;

      if (e.centeredSlides) {
        for (var n, o = t[a].swiperSlideSize, l = a + 1; l < t.length; l += 1) {
          t[l] && !n && (r += 1, (o += t[l].swiperSlideSize) > s && (n = !0));
        }

        for (var d = a - 1; d >= 0; d -= 1) {
          t[d] && !n && (r += 1, (o += t[d].swiperSlideSize) > s && (n = !0));
        }
      } else for (var h = a + 1; h < t.length; h += 1) {
        i[h] - i[a] < s && (r += 1);
      }

      return r;
    }, t.prototype.update = function () {
      var e = this;

      if (e && !e.destroyed) {
        var t = e.snapGrid,
            i = e.params;
        i.breakpoints && e.setBreakpoint(), e.updateSize(), e.updateSlides(), e.updateProgress(), e.updateSlidesClasses(), e.params.freeMode ? (s(), e.params.autoHeight && e.updateAutoHeight()) : (("auto" === e.params.slidesPerView || e.params.slidesPerView > 1) && e.isEnd && !e.params.centeredSlides ? e.slideTo(e.slides.length - 1, 0, !1, !0) : e.slideTo(e.activeIndex, 0, !1, !0)) || s(), i.watchOverflow && t !== e.snapGrid && e.checkOverflow(), e.emit("update");
      }

      function s() {
        var t = e.rtlTranslate ? -1 * e.translate : e.translate,
            i = Math.min(Math.max(t, e.maxTranslate()), e.minTranslate());
        e.setTranslate(i), e.updateActiveIndex(), e.updateSlidesClasses();
      }
    }, t.prototype.changeDirection = function (e, t) {
      void 0 === t && (t = !0);
      var i = this.params.direction;
      return e || (e = "horizontal" === i ? "vertical" : "horizontal"), e === i || "horizontal" !== e && "vertical" !== e ? this : (this.$el.removeClass("" + this.params.containerModifierClass + i).addClass("" + this.params.containerModifierClass + e), this.params.direction = e, this.slides.each(function (t, i) {
        "vertical" === e ? i.style.width = "" : i.style.height = "";
      }), this.emit("changeDirection"), t && this.update(), this);
    }, t.prototype.init = function () {
      this.initialized || (this.emit("beforeInit"), this.params.breakpoints && this.setBreakpoint(), this.addClasses(), this.params.loop && this.loopCreate(), this.updateSize(), this.updateSlides(), this.params.watchOverflow && this.checkOverflow(), this.params.grabCursor && this.setGrabCursor(), this.params.preloadImages && this.preloadImages(), this.params.loop ? this.slideTo(this.params.initialSlide + this.loopedSlides, 0, this.params.runCallbacksOnInit) : this.slideTo(this.params.initialSlide, 0, this.params.runCallbacksOnInit), this.attachEvents(), this.initialized = !0, this.emit("init"));
    }, t.prototype.destroy = function (e, t) {
      void 0 === e && (e = !0), void 0 === t && (t = !0);
      var i = this,
          s = i.params,
          a = i.$el,
          r = i.$wrapperEl,
          o = i.slides;
      return void 0 === i.params || i.destroyed ? null : (i.emit("beforeDestroy"), i.initialized = !1, i.detachEvents(), s.loop && i.loopDestroy(), t && (i.removeClasses(), a.removeAttr("style"), r.removeAttr("style"), o && o.length && o.removeClass([s.slideVisibleClass, s.slideActiveClass, s.slideNextClass, s.slidePrevClass].join(" ")).removeAttr("style").removeAttr("data-swiper-slide-index")), i.emit("destroy"), Object.keys(i.eventsListeners).forEach(function (e) {
        i.off(e);
      }), !1 !== e && (i.$el[0].swiper = null, i.$el.data("swiper", null), n.deleteProps(i)), i.destroyed = !0, null);
    }, t.extendDefaults = function (e) {
      n.extend(F, e);
    }, i.extendedDefaults.get = function () {
      return F;
    }, i.defaults.get = function () {
      return V;
    }, i.Class.get = function () {
      return e;
    }, i.$.get = function () {
      return s;
    }, Object.defineProperties(t, i), t;
  }(l),
      R = {
    name: "device",
    proto: {
      device: I
    },
    static: {
      device: I
    }
  },
      q = {
    name: "support",
    proto: {
      support: o
    },
    static: {
      support: o
    }
  },
      j = {
    isEdge: !!t.navigator.userAgent.match(/Edge/g),
    isSafari: function () {
      var e = t.navigator.userAgent.toLowerCase();
      return e.indexOf("safari") >= 0 && e.indexOf("chrome") < 0 && e.indexOf("android") < 0;
    }(),
    isUiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(t.navigator.userAgent)
  },
      K = {
    name: "browser",
    proto: {
      browser: j
    },
    static: {
      browser: j
    }
  },
      U = {
    name: "resize",
    create: function create() {
      var e = this;
      n.extend(e, {
        resize: {
          resizeHandler: function resizeHandler() {
            e && !e.destroyed && e.initialized && (e.emit("beforeResize"), e.emit("resize"));
          },
          orientationChangeHandler: function orientationChangeHandler() {
            e && !e.destroyed && e.initialized && e.emit("orientationchange");
          }
        }
      });
    },
    on: {
      init: function init() {
        t.addEventListener("resize", this.resize.resizeHandler), t.addEventListener("orientationchange", this.resize.orientationChangeHandler);
      },
      destroy: function destroy() {
        t.removeEventListener("resize", this.resize.resizeHandler), t.removeEventListener("orientationchange", this.resize.orientationChangeHandler);
      }
    }
  },
      _ = {
    func: t.MutationObserver || t.WebkitMutationObserver,
    attach: function attach(e, i) {
      void 0 === i && (i = {});
      var s = this,
          a = new (0, _.func)(function (e) {
        if (1 !== e.length) {
          var i = function i() {
            s.emit("observerUpdate", e[0]);
          };

          t.requestAnimationFrame ? t.requestAnimationFrame(i) : t.setTimeout(i, 0);
        } else s.emit("observerUpdate", e[0]);
      });
      a.observe(e, {
        attributes: void 0 === i.attributes || i.attributes,
        childList: void 0 === i.childList || i.childList,
        characterData: void 0 === i.characterData || i.characterData
      }), s.observer.observers.push(a);
    },
    init: function init() {
      if (o.observer && this.params.observer) {
        if (this.params.observeParents) for (var e = this.$el.parents(), t = 0; t < e.length; t += 1) {
          this.observer.attach(e[t]);
        }
        this.observer.attach(this.$el[0], {
          childList: this.params.observeSlideChildren
        }), this.observer.attach(this.$wrapperEl[0], {
          attributes: !1
        });
      }
    },
    destroy: function destroy() {
      this.observer.observers.forEach(function (e) {
        e.disconnect();
      }), this.observer.observers = [];
    }
  },
      Z = {
    name: "observer",
    params: {
      observer: !1,
      observeParents: !1,
      observeSlideChildren: !1
    },
    create: function create() {
      n.extend(this, {
        observer: {
          init: _.init.bind(this),
          attach: _.attach.bind(this),
          destroy: _.destroy.bind(this),
          observers: []
        }
      });
    },
    on: {
      init: function init() {
        this.observer.init();
      },
      destroy: function destroy() {
        this.observer.destroy();
      }
    }
  },
      Q = {
    update: function update(e) {
      var t = this,
          i = t.params,
          s = i.slidesPerView,
          a = i.slidesPerGroup,
          r = i.centeredSlides,
          o = t.params.virtual,
          l = o.addSlidesBefore,
          d = o.addSlidesAfter,
          h = t.virtual,
          p = h.from,
          c = h.to,
          u = h.slides,
          v = h.slidesGrid,
          f = h.renderSlide,
          m = h.offset;
      t.updateActiveIndex();
      var g,
          b,
          w,
          y = t.activeIndex || 0;
      g = t.rtlTranslate ? "right" : t.isHorizontal() ? "left" : "top", r ? (b = Math.floor(s / 2) + a + l, w = Math.floor(s / 2) + a + d) : (b = s + (a - 1) + l, w = a + d);
      var x = Math.max((y || 0) - w, 0),
          T = Math.min((y || 0) + b, u.length - 1),
          E = (t.slidesGrid[x] || 0) - (t.slidesGrid[0] || 0);

      function S() {
        t.updateSlides(), t.updateProgress(), t.updateSlidesClasses(), t.lazy && t.params.lazy.enabled && t.lazy.load();
      }

      if (n.extend(t.virtual, {
        from: x,
        to: T,
        offset: E,
        slidesGrid: t.slidesGrid
      }), p === x && c === T && !e) return t.slidesGrid !== v && E !== m && t.slides.css(g, E + "px"), void t.updateProgress();
      if (t.params.virtual.renderExternal) return t.params.virtual.renderExternal.call(t, {
        offset: E,
        from: x,
        to: T,
        slides: function () {
          for (var e = [], t = x; t <= T; t += 1) {
            e.push(u[t]);
          }

          return e;
        }()
      }), void S();
      var C = [],
          M = [];
      if (e) t.$wrapperEl.find("." + t.params.slideClass).remove();else for (var P = p; P <= c; P += 1) {
        (P < x || P > T) && t.$wrapperEl.find("." + t.params.slideClass + '[data-swiper-slide-index="' + P + '"]').remove();
      }

      for (var z = 0; z < u.length; z += 1) {
        z >= x && z <= T && (void 0 === c || e ? M.push(z) : (z > c && M.push(z), z < p && C.push(z)));
      }

      M.forEach(function (e) {
        t.$wrapperEl.append(f(u[e], e));
      }), C.sort(function (e, t) {
        return t - e;
      }).forEach(function (e) {
        t.$wrapperEl.prepend(f(u[e], e));
      }), t.$wrapperEl.children(".swiper-slide").css(g, E + "px"), S();
    },
    renderSlide: function renderSlide(e, t) {
      var i = this.params.virtual;
      if (i.cache && this.virtual.cache[t]) return this.virtual.cache[t];
      var a = i.renderSlide ? s(i.renderSlide.call(this, e, t)) : s('<div class="' + this.params.slideClass + '" data-swiper-slide-index="' + t + '">' + e + "</div>");
      return a.attr("data-swiper-slide-index") || a.attr("data-swiper-slide-index", t), i.cache && (this.virtual.cache[t] = a), a;
    },
    appendSlide: function appendSlide(e) {
      if ("object" == _typeof(e) && "length" in e) for (var t = 0; t < e.length; t += 1) {
        e[t] && this.virtual.slides.push(e[t]);
      } else this.virtual.slides.push(e);
      this.virtual.update(!0);
    },
    prependSlide: function prependSlide(e) {
      var t = this.activeIndex,
          i = t + 1,
          s = 1;

      if (Array.isArray(e)) {
        for (var a = 0; a < e.length; a += 1) {
          e[a] && this.virtual.slides.unshift(e[a]);
        }

        i = t + e.length, s = e.length;
      } else this.virtual.slides.unshift(e);

      if (this.params.virtual.cache) {
        var r = this.virtual.cache,
            n = {};
        Object.keys(r).forEach(function (e) {
          var t = r[e],
              i = t.attr("data-swiper-slide-index");
          i && t.attr("data-swiper-slide-index", parseInt(i, 10) + 1), n[parseInt(e, 10) + s] = t;
        }), this.virtual.cache = n;
      }

      this.virtual.update(!0), this.slideTo(i, 0);
    },
    removeSlide: function removeSlide(e) {
      if (null != e) {
        var t = this.activeIndex;
        if (Array.isArray(e)) for (var i = e.length - 1; i >= 0; i -= 1) {
          this.virtual.slides.splice(e[i], 1), this.params.virtual.cache && delete this.virtual.cache[e[i]], e[i] < t && (t -= 1), t = Math.max(t, 0);
        } else this.virtual.slides.splice(e, 1), this.params.virtual.cache && delete this.virtual.cache[e], e < t && (t -= 1), t = Math.max(t, 0);
        this.virtual.update(!0), this.slideTo(t, 0);
      }
    },
    removeAllSlides: function removeAllSlides() {
      this.virtual.slides = [], this.params.virtual.cache && (this.virtual.cache = {}), this.virtual.update(!0), this.slideTo(0, 0);
    }
  },
      J = {
    name: "virtual",
    params: {
      virtual: {
        enabled: !1,
        slides: [],
        cache: !0,
        renderSlide: null,
        renderExternal: null,
        addSlidesBefore: 0,
        addSlidesAfter: 0
      }
    },
    create: function create() {
      n.extend(this, {
        virtual: {
          update: Q.update.bind(this),
          appendSlide: Q.appendSlide.bind(this),
          prependSlide: Q.prependSlide.bind(this),
          removeSlide: Q.removeSlide.bind(this),
          removeAllSlides: Q.removeAllSlides.bind(this),
          renderSlide: Q.renderSlide.bind(this),
          slides: this.params.virtual.slides,
          cache: {}
        }
      });
    },
    on: {
      beforeInit: function beforeInit() {
        if (this.params.virtual.enabled) {
          this.classNames.push(this.params.containerModifierClass + "virtual");
          var e = {
            watchSlidesProgress: !0
          };
          n.extend(this.params, e), n.extend(this.originalParams, e), this.params.initialSlide || this.virtual.update();
        }
      },
      setTranslate: function setTranslate() {
        this.params.virtual.enabled && this.virtual.update();
      }
    }
  },
      ee = {
    handle: function handle(i) {
      var s = this.rtlTranslate,
          a = i;
      a.originalEvent && (a = a.originalEvent);
      var r = a.keyCode || a.charCode;
      if (!this.allowSlideNext && (this.isHorizontal() && 39 === r || this.isVertical() && 40 === r || 34 === r)) return !1;
      if (!this.allowSlidePrev && (this.isHorizontal() && 37 === r || this.isVertical() && 38 === r || 33 === r)) return !1;

      if (!(a.shiftKey || a.altKey || a.ctrlKey || a.metaKey || e.activeElement && e.activeElement.nodeName && ("input" === e.activeElement.nodeName.toLowerCase() || "textarea" === e.activeElement.nodeName.toLowerCase()))) {
        if (this.params.keyboard.onlyInViewport && (33 === r || 34 === r || 37 === r || 39 === r || 38 === r || 40 === r)) {
          var n = !1;
          if (this.$el.parents("." + this.params.slideClass).length > 0 && 0 === this.$el.parents("." + this.params.slideActiveClass).length) return;
          var o = t.innerWidth,
              l = t.innerHeight,
              d = this.$el.offset();
          s && (d.left -= this.$el[0].scrollLeft);

          for (var h = [[d.left, d.top], [d.left + this.width, d.top], [d.left, d.top + this.height], [d.left + this.width, d.top + this.height]], p = 0; p < h.length; p += 1) {
            var c = h[p];
            c[0] >= 0 && c[0] <= o && c[1] >= 0 && c[1] <= l && (n = !0);
          }

          if (!n) return;
        }

        this.isHorizontal() ? (33 !== r && 34 !== r && 37 !== r && 39 !== r || (a.preventDefault ? a.preventDefault() : a.returnValue = !1), (34 !== r && 39 !== r || s) && (33 !== r && 37 !== r || !s) || this.slideNext(), (33 !== r && 37 !== r || s) && (34 !== r && 39 !== r || !s) || this.slidePrev()) : (33 !== r && 34 !== r && 38 !== r && 40 !== r || (a.preventDefault ? a.preventDefault() : a.returnValue = !1), 34 !== r && 40 !== r || this.slideNext(), 33 !== r && 38 !== r || this.slidePrev()), this.emit("keyPress", r);
      }
    },
    enable: function enable() {
      this.keyboard.enabled || (s(e).on("keydown", this.keyboard.handle), this.keyboard.enabled = !0);
    },
    disable: function disable() {
      this.keyboard.enabled && (s(e).off("keydown", this.keyboard.handle), this.keyboard.enabled = !1);
    }
  },
      te = {
    name: "keyboard",
    params: {
      keyboard: {
        enabled: !1,
        onlyInViewport: !0
      }
    },
    create: function create() {
      n.extend(this, {
        keyboard: {
          enabled: !1,
          enable: ee.enable.bind(this),
          disable: ee.disable.bind(this),
          handle: ee.handle.bind(this)
        }
      });
    },
    on: {
      init: function init() {
        this.params.keyboard.enabled && this.keyboard.enable();
      },
      destroy: function destroy() {
        this.keyboard.enabled && this.keyboard.disable();
      }
    }
  };

  var ie = {
    lastScrollTime: n.now(),
    lastEventBeforeSnap: void 0,
    recentWheelEvents: [],
    event: function event() {
      return t.navigator.userAgent.indexOf("firefox") > -1 ? "DOMMouseScroll" : function () {
        var t = "onwheel" in e;

        if (!t) {
          var i = e.createElement("div");
          i.setAttribute("onwheel", "return;"), t = "function" == typeof i.onwheel;
        }

        return !t && e.implementation && e.implementation.hasFeature && !0 !== e.implementation.hasFeature("", "") && (t = e.implementation.hasFeature("Events.wheel", "3.0")), t;
      }() ? "wheel" : "mousewheel";
    },
    normalize: function normalize(e) {
      var t = 0,
          i = 0,
          s = 0,
          a = 0;
      return "detail" in e && (i = e.detail), "wheelDelta" in e && (i = -e.wheelDelta / 120), "wheelDeltaY" in e && (i = -e.wheelDeltaY / 120), "wheelDeltaX" in e && (t = -e.wheelDeltaX / 120), "axis" in e && e.axis === e.HORIZONTAL_AXIS && (t = i, i = 0), s = 10 * t, a = 10 * i, "deltaY" in e && (a = e.deltaY), "deltaX" in e && (s = e.deltaX), e.shiftKey && !s && (s = a, a = 0), (s || a) && e.deltaMode && (1 === e.deltaMode ? (s *= 40, a *= 40) : (s *= 800, a *= 800)), s && !t && (t = s < 1 ? -1 : 1), a && !i && (i = a < 1 ? -1 : 1), {
        spinX: t,
        spinY: i,
        pixelX: s,
        pixelY: a
      };
    },
    handleMouseEnter: function handleMouseEnter() {
      this.mouseEntered = !0;
    },
    handleMouseLeave: function handleMouseLeave() {
      this.mouseEntered = !1;
    },
    handle: function handle(e) {
      var t = e,
          i = this,
          s = i.params.mousewheel;
      if (i.params.cssMode && t.preventDefault(), !i.mouseEntered && !s.releaseOnEdges) return !0;
      t.originalEvent && (t = t.originalEvent);
      var a = 0,
          r = i.rtlTranslate ? -1 : 1,
          o = ie.normalize(t);
      if (s.forceToAxis) {
        if (i.isHorizontal()) {
          if (!(Math.abs(o.pixelX) > Math.abs(o.pixelY))) return !0;
          a = o.pixelX * r;
        } else {
          if (!(Math.abs(o.pixelY) > Math.abs(o.pixelX))) return !0;
          a = o.pixelY;
        }
      } else a = Math.abs(o.pixelX) > Math.abs(o.pixelY) ? -o.pixelX * r : -o.pixelY;
      if (0 === a) return !0;

      if (s.invert && (a = -a), i.params.freeMode) {
        var l = {
          time: n.now(),
          delta: Math.abs(a),
          direction: Math.sign(a)
        },
            d = i.mousewheel.lastEventBeforeSnap,
            h = d && l.time < d.time + 500 && l.delta <= d.delta && l.direction === d.direction;

        if (!h) {
          i.mousewheel.lastEventBeforeSnap = void 0, i.params.loop && i.loopFix();
          var p = i.getTranslate() + a * s.sensitivity,
              c = i.isBeginning,
              u = i.isEnd;

          if (p >= i.minTranslate() && (p = i.minTranslate()), p <= i.maxTranslate() && (p = i.maxTranslate()), i.setTransition(0), i.setTranslate(p), i.updateProgress(), i.updateActiveIndex(), i.updateSlidesClasses(), (!c && i.isBeginning || !u && i.isEnd) && i.updateSlidesClasses(), i.params.freeModeSticky) {
            clearTimeout(i.mousewheel.timeout), i.mousewheel.timeout = void 0;
            var v = i.mousewheel.recentWheelEvents;
            v.length >= 15 && v.shift();
            var f = v.length ? v[v.length - 1] : void 0,
                m = v[0];
            if (v.push(l), f && (l.delta > f.delta || l.direction !== f.direction)) v.splice(0);else if (v.length >= 15 && l.time - m.time < 500 && m.delta - l.delta >= 1 && l.delta <= 6) {
              var g = a > 0 ? .8 : .2;
              i.mousewheel.lastEventBeforeSnap = l, v.splice(0), i.mousewheel.timeout = n.nextTick(function () {
                i.slideToClosest(i.params.speed, !0, void 0, g);
              }, 0);
            }
            i.mousewheel.timeout || (i.mousewheel.timeout = n.nextTick(function () {
              i.mousewheel.lastEventBeforeSnap = l, v.splice(0), i.slideToClosest(i.params.speed, !0, void 0, .5);
            }, 500));
          }

          if (h || i.emit("scroll", t), i.params.autoplay && i.params.autoplayDisableOnInteraction && i.autoplay.stop(), p === i.minTranslate() || p === i.maxTranslate()) return !0;
        }
      } else {
        var b = {
          time: n.now(),
          delta: Math.abs(a),
          direction: Math.sign(a),
          raw: e
        },
            w = i.mousewheel.recentWheelEvents;
        w.length >= 2 && w.shift();
        var y = w.length ? w[w.length - 1] : void 0;
        if (w.push(b), y ? (b.direction !== y.direction || b.delta > y.delta) && i.mousewheel.animateSlider(b) : i.mousewheel.animateSlider(b), i.mousewheel.releaseScroll(b)) return !0;
      }

      return t.preventDefault ? t.preventDefault() : t.returnValue = !1, !1;
    },
    animateSlider: function animateSlider(e) {
      return e.delta >= 6 && n.now() - this.mousewheel.lastScrollTime < 60 || (e.direction < 0 ? this.isEnd && !this.params.loop || this.animating || (this.slideNext(), this.emit("scroll", e.raw)) : this.isBeginning && !this.params.loop || this.animating || (this.slidePrev(), this.emit("scroll", e.raw)), this.mousewheel.lastScrollTime = new t.Date().getTime(), !1);
    },
    releaseScroll: function releaseScroll(e) {
      var t = this.params.mousewheel;

      if (e.direction < 0) {
        if (this.isEnd && !this.params.loop && t.releaseOnEdges) return !0;
      } else if (this.isBeginning && !this.params.loop && t.releaseOnEdges) return !0;

      return !1;
    },
    enable: function enable() {
      var e = ie.event();
      if (this.params.cssMode) return this.wrapperEl.removeEventListener(e, this.mousewheel.handle), !0;
      if (!e) return !1;
      if (this.mousewheel.enabled) return !1;
      var t = this.$el;
      return "container" !== this.params.mousewheel.eventsTarged && (t = s(this.params.mousewheel.eventsTarged)), t.on("mouseenter", this.mousewheel.handleMouseEnter), t.on("mouseleave", this.mousewheel.handleMouseLeave), t.on(e, this.mousewheel.handle), this.mousewheel.enabled = !0, !0;
    },
    disable: function disable() {
      var e = ie.event();
      if (this.params.cssMode) return this.wrapperEl.addEventListener(e, this.mousewheel.handle), !0;
      if (!e) return !1;
      if (!this.mousewheel.enabled) return !1;
      var t = this.$el;
      return "container" !== this.params.mousewheel.eventsTarged && (t = s(this.params.mousewheel.eventsTarged)), t.off(e, this.mousewheel.handle), this.mousewheel.enabled = !1, !0;
    }
  },
      se = {
    update: function update() {
      var e = this.params.navigation;

      if (!this.params.loop) {
        var t = this.navigation,
            i = t.$nextEl,
            s = t.$prevEl;
        s && s.length > 0 && (this.isBeginning ? s.addClass(e.disabledClass) : s.removeClass(e.disabledClass), s[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](e.lockClass)), i && i.length > 0 && (this.isEnd ? i.addClass(e.disabledClass) : i.removeClass(e.disabledClass), i[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](e.lockClass));
      }
    },
    onPrevClick: function onPrevClick(e) {
      e.preventDefault(), this.isBeginning && !this.params.loop || this.slidePrev();
    },
    onNextClick: function onNextClick(e) {
      e.preventDefault(), this.isEnd && !this.params.loop || this.slideNext();
    },
    init: function init() {
      var e,
          t,
          i = this.params.navigation;
      (i.nextEl || i.prevEl) && (i.nextEl && (e = s(i.nextEl), this.params.uniqueNavElements && "string" == typeof i.nextEl && e.length > 1 && 1 === this.$el.find(i.nextEl).length && (e = this.$el.find(i.nextEl))), i.prevEl && (t = s(i.prevEl), this.params.uniqueNavElements && "string" == typeof i.prevEl && t.length > 1 && 1 === this.$el.find(i.prevEl).length && (t = this.$el.find(i.prevEl))), e && e.length > 0 && e.on("click", this.navigation.onNextClick), t && t.length > 0 && t.on("click", this.navigation.onPrevClick), n.extend(this.navigation, {
        $nextEl: e,
        nextEl: e && e[0],
        $prevEl: t,
        prevEl: t && t[0]
      }));
    },
    destroy: function destroy() {
      var e = this.navigation,
          t = e.$nextEl,
          i = e.$prevEl;
      t && t.length && (t.off("click", this.navigation.onNextClick), t.removeClass(this.params.navigation.disabledClass)), i && i.length && (i.off("click", this.navigation.onPrevClick), i.removeClass(this.params.navigation.disabledClass));
    }
  },
      ae = {
    update: function update() {
      var e = this.rtl,
          t = this.params.pagination;

      if (t.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
        var i,
            a = this.virtual && this.params.virtual.enabled ? this.virtual.slides.length : this.slides.length,
            r = this.pagination.$el,
            n = this.params.loop ? Math.ceil((a - 2 * this.loopedSlides) / this.params.slidesPerGroup) : this.snapGrid.length;

        if (this.params.loop ? ((i = Math.ceil((this.activeIndex - this.loopedSlides) / this.params.slidesPerGroup)) > a - 1 - 2 * this.loopedSlides && (i -= a - 2 * this.loopedSlides), i > n - 1 && (i -= n), i < 0 && "bullets" !== this.params.paginationType && (i = n + i)) : i = void 0 !== this.snapIndex ? this.snapIndex : this.activeIndex || 0, "bullets" === t.type && this.pagination.bullets && this.pagination.bullets.length > 0) {
          var o,
              l,
              d,
              h = this.pagination.bullets;
          if (t.dynamicBullets && (this.pagination.bulletSize = h.eq(0)[this.isHorizontal() ? "outerWidth" : "outerHeight"](!0), r.css(this.isHorizontal() ? "width" : "height", this.pagination.bulletSize * (t.dynamicMainBullets + 4) + "px"), t.dynamicMainBullets > 1 && void 0 !== this.previousIndex && (this.pagination.dynamicBulletIndex += i - this.previousIndex, this.pagination.dynamicBulletIndex > t.dynamicMainBullets - 1 ? this.pagination.dynamicBulletIndex = t.dynamicMainBullets - 1 : this.pagination.dynamicBulletIndex < 0 && (this.pagination.dynamicBulletIndex = 0)), o = i - this.pagination.dynamicBulletIndex, d = ((l = o + (Math.min(h.length, t.dynamicMainBullets) - 1)) + o) / 2), h.removeClass(t.bulletActiveClass + " " + t.bulletActiveClass + "-next " + t.bulletActiveClass + "-next-next " + t.bulletActiveClass + "-prev " + t.bulletActiveClass + "-prev-prev " + t.bulletActiveClass + "-main"), r.length > 1) h.each(function (e, a) {
            var r = s(a),
                n = r.index();
            n === i && r.addClass(t.bulletActiveClass), t.dynamicBullets && (n >= o && n <= l && r.addClass(t.bulletActiveClass + "-main"), n === o && r.prev().addClass(t.bulletActiveClass + "-prev").prev().addClass(t.bulletActiveClass + "-prev-prev"), n === l && r.next().addClass(t.bulletActiveClass + "-next").next().addClass(t.bulletActiveClass + "-next-next"));
          });else {
            var p = h.eq(i),
                c = p.index();

            if (p.addClass(t.bulletActiveClass), t.dynamicBullets) {
              for (var u = h.eq(o), v = h.eq(l), f = o; f <= l; f += 1) {
                h.eq(f).addClass(t.bulletActiveClass + "-main");
              }

              if (this.params.loop) {
                if (c >= h.length - t.dynamicMainBullets) {
                  for (var m = t.dynamicMainBullets; m >= 0; m -= 1) {
                    h.eq(h.length - m).addClass(t.bulletActiveClass + "-main");
                  }

                  h.eq(h.length - t.dynamicMainBullets - 1).addClass(t.bulletActiveClass + "-prev");
                } else u.prev().addClass(t.bulletActiveClass + "-prev").prev().addClass(t.bulletActiveClass + "-prev-prev"), v.next().addClass(t.bulletActiveClass + "-next").next().addClass(t.bulletActiveClass + "-next-next");
              } else u.prev().addClass(t.bulletActiveClass + "-prev").prev().addClass(t.bulletActiveClass + "-prev-prev"), v.next().addClass(t.bulletActiveClass + "-next").next().addClass(t.bulletActiveClass + "-next-next");
            }
          }

          if (t.dynamicBullets) {
            var g = Math.min(h.length, t.dynamicMainBullets + 4),
                b = (this.pagination.bulletSize * g - this.pagination.bulletSize) / 2 - d * this.pagination.bulletSize,
                w = e ? "right" : "left";
            h.css(this.isHorizontal() ? w : "top", b + "px");
          }
        }

        if ("fraction" === t.type && (r.find("." + t.currentClass).text(t.formatFractionCurrent(i + 1)), r.find("." + t.totalClass).text(t.formatFractionTotal(n))), "progressbar" === t.type) {
          var y;
          y = t.progressbarOpposite ? this.isHorizontal() ? "vertical" : "horizontal" : this.isHorizontal() ? "horizontal" : "vertical";
          var x = (i + 1) / n,
              T = 1,
              E = 1;
          "horizontal" === y ? T = x : E = x, r.find("." + t.progressbarFillClass).transform("translate3d(0,0,0) scaleX(" + T + ") scaleY(" + E + ")").transition(this.params.speed);
        }

        "custom" === t.type && t.renderCustom ? (r.html(t.renderCustom(this, i + 1, n)), this.emit("paginationRender", this, r[0])) : this.emit("paginationUpdate", this, r[0]), r[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](t.lockClass);
      }
    },
    render: function render() {
      var e = this.params.pagination;

      if (e.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
        var t = this.virtual && this.params.virtual.enabled ? this.virtual.slides.length : this.slides.length,
            i = this.pagination.$el,
            s = "";

        if ("bullets" === e.type) {
          for (var a = this.params.loop ? Math.ceil((t - 2 * this.loopedSlides) / this.params.slidesPerGroup) : this.snapGrid.length, r = 0; r < a; r += 1) {
            e.renderBullet ? s += e.renderBullet.call(this, r, e.bulletClass) : s += "<" + e.bulletElement + ' class="' + e.bulletClass + '"></' + e.bulletElement + ">";
          }

          i.html(s), this.pagination.bullets = i.find("." + e.bulletClass);
        }

        "fraction" === e.type && (s = e.renderFraction ? e.renderFraction.call(this, e.currentClass, e.totalClass) : '<span class="' + e.currentClass + '"></span> / <span class="' + e.totalClass + '"></span>', i.html(s)), "progressbar" === e.type && (s = e.renderProgressbar ? e.renderProgressbar.call(this, e.progressbarFillClass) : '<span class="' + e.progressbarFillClass + '"></span>', i.html(s)), "custom" !== e.type && this.emit("paginationRender", this.pagination.$el[0]);
      }
    },
    init: function init() {
      var e = this,
          t = e.params.pagination;

      if (t.el) {
        var i = s(t.el);
        0 !== i.length && (e.params.uniqueNavElements && "string" == typeof t.el && i.length > 1 && 1 === e.$el.find(t.el).length && (i = e.$el.find(t.el)), "bullets" === t.type && t.clickable && i.addClass(t.clickableClass), i.addClass(t.modifierClass + t.type), "bullets" === t.type && t.dynamicBullets && (i.addClass("" + t.modifierClass + t.type + "-dynamic"), e.pagination.dynamicBulletIndex = 0, t.dynamicMainBullets < 1 && (t.dynamicMainBullets = 1)), "progressbar" === t.type && t.progressbarOpposite && i.addClass(t.progressbarOppositeClass), t.clickable && i.on("click", "." + t.bulletClass, function (t) {
          t.preventDefault();
          var i = s(this).index() * e.params.slidesPerGroup;
          e.params.loop && (i += e.loopedSlides), e.slideTo(i);
        }), n.extend(e.pagination, {
          $el: i,
          el: i[0]
        }));
      }
    },
    destroy: function destroy() {
      var e = this.params.pagination;

      if (e.el && this.pagination.el && this.pagination.$el && 0 !== this.pagination.$el.length) {
        var t = this.pagination.$el;
        t.removeClass(e.hiddenClass), t.removeClass(e.modifierClass + e.type), this.pagination.bullets && this.pagination.bullets.removeClass(e.bulletActiveClass), e.clickable && t.off("click", "." + e.bulletClass);
      }
    }
  },
      re = {
    setTranslate: function setTranslate() {
      if (this.params.scrollbar.el && this.scrollbar.el) {
        var e = this.scrollbar,
            t = this.rtlTranslate,
            i = this.progress,
            s = e.dragSize,
            a = e.trackSize,
            r = e.$dragEl,
            n = e.$el,
            o = this.params.scrollbar,
            l = s,
            d = (a - s) * i;
        t ? (d = -d) > 0 ? (l = s - d, d = 0) : -d + s > a && (l = a + d) : d < 0 ? (l = s + d, d = 0) : d + s > a && (l = a - d), this.isHorizontal() ? (r.transform("translate3d(" + d + "px, 0, 0)"), r[0].style.width = l + "px") : (r.transform("translate3d(0px, " + d + "px, 0)"), r[0].style.height = l + "px"), o.hide && (clearTimeout(this.scrollbar.timeout), n[0].style.opacity = 1, this.scrollbar.timeout = setTimeout(function () {
          n[0].style.opacity = 0, n.transition(400);
        }, 1e3));
      }
    },
    setTransition: function setTransition(e) {
      this.params.scrollbar.el && this.scrollbar.el && this.scrollbar.$dragEl.transition(e);
    },
    updateSize: function updateSize() {
      if (this.params.scrollbar.el && this.scrollbar.el) {
        var e = this.scrollbar,
            t = e.$dragEl,
            i = e.$el;
        t[0].style.width = "", t[0].style.height = "";
        var s,
            a = this.isHorizontal() ? i[0].offsetWidth : i[0].offsetHeight,
            r = this.size / this.virtualSize,
            o = r * (a / this.size);
        s = "auto" === this.params.scrollbar.dragSize ? a * r : parseInt(this.params.scrollbar.dragSize, 10), this.isHorizontal() ? t[0].style.width = s + "px" : t[0].style.height = s + "px", i[0].style.display = r >= 1 ? "none" : "", this.params.scrollbar.hide && (i[0].style.opacity = 0), n.extend(e, {
          trackSize: a,
          divider: r,
          moveDivider: o,
          dragSize: s
        }), e.$el[this.params.watchOverflow && this.isLocked ? "addClass" : "removeClass"](this.params.scrollbar.lockClass);
      }
    },
    getPointerPosition: function getPointerPosition(e) {
      return this.isHorizontal() ? "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].clientX : e.clientX : "touchstart" === e.type || "touchmove" === e.type ? e.targetTouches[0].clientY : e.clientY;
    },
    setDragPosition: function setDragPosition(e) {
      var t,
          i = this.scrollbar,
          s = this.rtlTranslate,
          a = i.$el,
          r = i.dragSize,
          n = i.trackSize,
          o = i.dragStartPos;
      t = (i.getPointerPosition(e) - a.offset()[this.isHorizontal() ? "left" : "top"] - (null !== o ? o : r / 2)) / (n - r), t = Math.max(Math.min(t, 1), 0), s && (t = 1 - t);
      var l = this.minTranslate() + (this.maxTranslate() - this.minTranslate()) * t;
      this.updateProgress(l), this.setTranslate(l), this.updateActiveIndex(), this.updateSlidesClasses();
    },
    onDragStart: function onDragStart(e) {
      var t = this.params.scrollbar,
          i = this.scrollbar,
          s = this.$wrapperEl,
          a = i.$el,
          r = i.$dragEl;
      this.scrollbar.isTouched = !0, this.scrollbar.dragStartPos = e.target === r[0] || e.target === r ? i.getPointerPosition(e) - e.target.getBoundingClientRect()[this.isHorizontal() ? "left" : "top"] : null, e.preventDefault(), e.stopPropagation(), s.transition(100), r.transition(100), i.setDragPosition(e), clearTimeout(this.scrollbar.dragTimeout), a.transition(0), t.hide && a.css("opacity", 1), this.params.cssMode && this.$wrapperEl.css("scroll-snap-type", "none"), this.emit("scrollbarDragStart", e);
    },
    onDragMove: function onDragMove(e) {
      var t = this.scrollbar,
          i = this.$wrapperEl,
          s = t.$el,
          a = t.$dragEl;
      this.scrollbar.isTouched && (e.preventDefault ? e.preventDefault() : e.returnValue = !1, t.setDragPosition(e), i.transition(0), s.transition(0), a.transition(0), this.emit("scrollbarDragMove", e));
    },
    onDragEnd: function onDragEnd(e) {
      var t = this.params.scrollbar,
          i = this.scrollbar,
          s = this.$wrapperEl,
          a = i.$el;
      this.scrollbar.isTouched && (this.scrollbar.isTouched = !1, this.params.cssMode && (this.$wrapperEl.css("scroll-snap-type", ""), s.transition("")), t.hide && (clearTimeout(this.scrollbar.dragTimeout), this.scrollbar.dragTimeout = n.nextTick(function () {
        a.css("opacity", 0), a.transition(400);
      }, 1e3)), this.emit("scrollbarDragEnd", e), t.snapOnRelease && this.slideToClosest());
    },
    enableDraggable: function enableDraggable() {
      if (this.params.scrollbar.el) {
        var t = this.scrollbar,
            i = this.touchEventsTouch,
            s = this.touchEventsDesktop,
            a = this.params,
            r = t.$el[0],
            n = !(!o.passiveListener || !a.passiveListeners) && {
          passive: !1,
          capture: !1
        },
            l = !(!o.passiveListener || !a.passiveListeners) && {
          passive: !0,
          capture: !1
        };
        o.touch ? (r.addEventListener(i.start, this.scrollbar.onDragStart, n), r.addEventListener(i.move, this.scrollbar.onDragMove, n), r.addEventListener(i.end, this.scrollbar.onDragEnd, l)) : (r.addEventListener(s.start, this.scrollbar.onDragStart, n), e.addEventListener(s.move, this.scrollbar.onDragMove, n), e.addEventListener(s.end, this.scrollbar.onDragEnd, l));
      }
    },
    disableDraggable: function disableDraggable() {
      if (this.params.scrollbar.el) {
        var t = this.scrollbar,
            i = this.touchEventsTouch,
            s = this.touchEventsDesktop,
            a = this.params,
            r = t.$el[0],
            n = !(!o.passiveListener || !a.passiveListeners) && {
          passive: !1,
          capture: !1
        },
            l = !(!o.passiveListener || !a.passiveListeners) && {
          passive: !0,
          capture: !1
        };
        o.touch ? (r.removeEventListener(i.start, this.scrollbar.onDragStart, n), r.removeEventListener(i.move, this.scrollbar.onDragMove, n), r.removeEventListener(i.end, this.scrollbar.onDragEnd, l)) : (r.removeEventListener(s.start, this.scrollbar.onDragStart, n), e.removeEventListener(s.move, this.scrollbar.onDragMove, n), e.removeEventListener(s.end, this.scrollbar.onDragEnd, l));
      }
    },
    init: function init() {
      if (this.params.scrollbar.el) {
        var e = this.scrollbar,
            t = this.$el,
            i = this.params.scrollbar,
            a = s(i.el);
        this.params.uniqueNavElements && "string" == typeof i.el && a.length > 1 && 1 === t.find(i.el).length && (a = t.find(i.el));
        var r = a.find("." + this.params.scrollbar.dragClass);
        0 === r.length && (r = s('<div class="' + this.params.scrollbar.dragClass + '"></div>'), a.append(r)), n.extend(e, {
          $el: a,
          el: a[0],
          $dragEl: r,
          dragEl: r[0]
        }), i.draggable && e.enableDraggable();
      }
    },
    destroy: function destroy() {
      this.scrollbar.disableDraggable();
    }
  },
      ne = {
    setTransform: function setTransform(e, t) {
      var i = this.rtl,
          a = s(e),
          r = i ? -1 : 1,
          n = a.attr("data-swiper-parallax") || "0",
          o = a.attr("data-swiper-parallax-x"),
          l = a.attr("data-swiper-parallax-y"),
          d = a.attr("data-swiper-parallax-scale"),
          h = a.attr("data-swiper-parallax-opacity");

      if (o || l ? (o = o || "0", l = l || "0") : this.isHorizontal() ? (o = n, l = "0") : (l = n, o = "0"), o = o.indexOf("%") >= 0 ? parseInt(o, 10) * t * r + "%" : o * t * r + "px", l = l.indexOf("%") >= 0 ? parseInt(l, 10) * t + "%" : l * t + "px", null != h) {
        var p = h - (h - 1) * (1 - Math.abs(t));
        a[0].style.opacity = p;
      }

      if (null == d) a.transform("translate3d(" + o + ", " + l + ", 0px)");else {
        var c = d - (d - 1) * (1 - Math.abs(t));
        a.transform("translate3d(" + o + ", " + l + ", 0px) scale(" + c + ")");
      }
    },
    setTranslate: function setTranslate() {
      var e = this,
          t = e.$el,
          i = e.slides,
          a = e.progress,
          r = e.snapGrid;
      t.children("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (t, i) {
        e.parallax.setTransform(i, a);
      }), i.each(function (t, i) {
        var n = i.progress;
        e.params.slidesPerGroup > 1 && "auto" !== e.params.slidesPerView && (n += Math.ceil(t / 2) - a * (r.length - 1)), n = Math.min(Math.max(n, -1), 1), s(i).find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (t, i) {
          e.parallax.setTransform(i, n);
        });
      });
    },
    setTransition: function setTransition(e) {
      void 0 === e && (e = this.params.speed);
      this.$el.find("[data-swiper-parallax], [data-swiper-parallax-x], [data-swiper-parallax-y], [data-swiper-parallax-opacity], [data-swiper-parallax-scale]").each(function (t, i) {
        var a = s(i),
            r = parseInt(a.attr("data-swiper-parallax-duration"), 10) || e;
        0 === e && (r = 0), a.transition(r);
      });
    }
  },
      oe = {
    getDistanceBetweenTouches: function getDistanceBetweenTouches(e) {
      if (e.targetTouches.length < 2) return 1;
      var t = e.targetTouches[0].pageX,
          i = e.targetTouches[0].pageY,
          s = e.targetTouches[1].pageX,
          a = e.targetTouches[1].pageY;
      return Math.sqrt(Math.pow(s - t, 2) + Math.pow(a - i, 2));
    },
    onGestureStart: function onGestureStart(e) {
      var t = this.params.zoom,
          i = this.zoom,
          a = i.gesture;

      if (i.fakeGestureTouched = !1, i.fakeGestureMoved = !1, !o.gestures) {
        if ("touchstart" !== e.type || "touchstart" === e.type && e.targetTouches.length < 2) return;
        i.fakeGestureTouched = !0, a.scaleStart = oe.getDistanceBetweenTouches(e);
      }

      a.$slideEl && a.$slideEl.length || (a.$slideEl = s(e.target).closest(".swiper-slide"), 0 === a.$slideEl.length && (a.$slideEl = this.slides.eq(this.activeIndex)), a.$imageEl = a.$slideEl.find("img, svg, canvas"), a.$imageWrapEl = a.$imageEl.parent("." + t.containerClass), a.maxRatio = a.$imageWrapEl.attr("data-swiper-zoom") || t.maxRatio, 0 !== a.$imageWrapEl.length) ? (a.$imageEl.transition(0), this.zoom.isScaling = !0) : a.$imageEl = void 0;
    },
    onGestureChange: function onGestureChange(e) {
      var t = this.params.zoom,
          i = this.zoom,
          s = i.gesture;

      if (!o.gestures) {
        if ("touchmove" !== e.type || "touchmove" === e.type && e.targetTouches.length < 2) return;
        i.fakeGestureMoved = !0, s.scaleMove = oe.getDistanceBetweenTouches(e);
      }

      s.$imageEl && 0 !== s.$imageEl.length && (o.gestures ? i.scale = e.scale * i.currentScale : i.scale = s.scaleMove / s.scaleStart * i.currentScale, i.scale > s.maxRatio && (i.scale = s.maxRatio - 1 + Math.pow(i.scale - s.maxRatio + 1, .5)), i.scale < t.minRatio && (i.scale = t.minRatio + 1 - Math.pow(t.minRatio - i.scale + 1, .5)), s.$imageEl.transform("translate3d(0,0,0) scale(" + i.scale + ")"));
    },
    onGestureEnd: function onGestureEnd(e) {
      var t = this.params.zoom,
          i = this.zoom,
          s = i.gesture;

      if (!o.gestures) {
        if (!i.fakeGestureTouched || !i.fakeGestureMoved) return;
        if ("touchend" !== e.type || "touchend" === e.type && e.changedTouches.length < 2 && !I.android) return;
        i.fakeGestureTouched = !1, i.fakeGestureMoved = !1;
      }

      s.$imageEl && 0 !== s.$imageEl.length && (i.scale = Math.max(Math.min(i.scale, s.maxRatio), t.minRatio), s.$imageEl.transition(this.params.speed).transform("translate3d(0,0,0) scale(" + i.scale + ")"), i.currentScale = i.scale, i.isScaling = !1, 1 === i.scale && (s.$slideEl = void 0));
    },
    onTouchStart: function onTouchStart(e) {
      var t = this.zoom,
          i = t.gesture,
          s = t.image;
      i.$imageEl && 0 !== i.$imageEl.length && (s.isTouched || (I.android && e.preventDefault(), s.isTouched = !0, s.touchesStart.x = "touchstart" === e.type ? e.targetTouches[0].pageX : e.pageX, s.touchesStart.y = "touchstart" === e.type ? e.targetTouches[0].pageY : e.pageY));
    },
    onTouchMove: function onTouchMove(e) {
      var t = this.zoom,
          i = t.gesture,
          s = t.image,
          a = t.velocity;

      if (i.$imageEl && 0 !== i.$imageEl.length && (this.allowClick = !1, s.isTouched && i.$slideEl)) {
        s.isMoved || (s.width = i.$imageEl[0].offsetWidth, s.height = i.$imageEl[0].offsetHeight, s.startX = n.getTranslate(i.$imageWrapEl[0], "x") || 0, s.startY = n.getTranslate(i.$imageWrapEl[0], "y") || 0, i.slideWidth = i.$slideEl[0].offsetWidth, i.slideHeight = i.$slideEl[0].offsetHeight, i.$imageWrapEl.transition(0), this.rtl && (s.startX = -s.startX, s.startY = -s.startY));
        var r = s.width * t.scale,
            o = s.height * t.scale;

        if (!(r < i.slideWidth && o < i.slideHeight)) {
          if (s.minX = Math.min(i.slideWidth / 2 - r / 2, 0), s.maxX = -s.minX, s.minY = Math.min(i.slideHeight / 2 - o / 2, 0), s.maxY = -s.minY, s.touchesCurrent.x = "touchmove" === e.type ? e.targetTouches[0].pageX : e.pageX, s.touchesCurrent.y = "touchmove" === e.type ? e.targetTouches[0].pageY : e.pageY, !s.isMoved && !t.isScaling) {
            if (this.isHorizontal() && (Math.floor(s.minX) === Math.floor(s.startX) && s.touchesCurrent.x < s.touchesStart.x || Math.floor(s.maxX) === Math.floor(s.startX) && s.touchesCurrent.x > s.touchesStart.x)) return void (s.isTouched = !1);
            if (!this.isHorizontal() && (Math.floor(s.minY) === Math.floor(s.startY) && s.touchesCurrent.y < s.touchesStart.y || Math.floor(s.maxY) === Math.floor(s.startY) && s.touchesCurrent.y > s.touchesStart.y)) return void (s.isTouched = !1);
          }

          e.preventDefault(), e.stopPropagation(), s.isMoved = !0, s.currentX = s.touchesCurrent.x - s.touchesStart.x + s.startX, s.currentY = s.touchesCurrent.y - s.touchesStart.y + s.startY, s.currentX < s.minX && (s.currentX = s.minX + 1 - Math.pow(s.minX - s.currentX + 1, .8)), s.currentX > s.maxX && (s.currentX = s.maxX - 1 + Math.pow(s.currentX - s.maxX + 1, .8)), s.currentY < s.minY && (s.currentY = s.minY + 1 - Math.pow(s.minY - s.currentY + 1, .8)), s.currentY > s.maxY && (s.currentY = s.maxY - 1 + Math.pow(s.currentY - s.maxY + 1, .8)), a.prevPositionX || (a.prevPositionX = s.touchesCurrent.x), a.prevPositionY || (a.prevPositionY = s.touchesCurrent.y), a.prevTime || (a.prevTime = Date.now()), a.x = (s.touchesCurrent.x - a.prevPositionX) / (Date.now() - a.prevTime) / 2, a.y = (s.touchesCurrent.y - a.prevPositionY) / (Date.now() - a.prevTime) / 2, Math.abs(s.touchesCurrent.x - a.prevPositionX) < 2 && (a.x = 0), Math.abs(s.touchesCurrent.y - a.prevPositionY) < 2 && (a.y = 0), a.prevPositionX = s.touchesCurrent.x, a.prevPositionY = s.touchesCurrent.y, a.prevTime = Date.now(), i.$imageWrapEl.transform("translate3d(" + s.currentX + "px, " + s.currentY + "px,0)");
        }
      }
    },
    onTouchEnd: function onTouchEnd() {
      var e = this.zoom,
          t = e.gesture,
          i = e.image,
          s = e.velocity;

      if (t.$imageEl && 0 !== t.$imageEl.length) {
        if (!i.isTouched || !i.isMoved) return i.isTouched = !1, void (i.isMoved = !1);
        i.isTouched = !1, i.isMoved = !1;
        var a = 300,
            r = 300,
            n = s.x * a,
            o = i.currentX + n,
            l = s.y * r,
            d = i.currentY + l;
        0 !== s.x && (a = Math.abs((o - i.currentX) / s.x)), 0 !== s.y && (r = Math.abs((d - i.currentY) / s.y));
        var h = Math.max(a, r);
        i.currentX = o, i.currentY = d;
        var p = i.width * e.scale,
            c = i.height * e.scale;
        i.minX = Math.min(t.slideWidth / 2 - p / 2, 0), i.maxX = -i.minX, i.minY = Math.min(t.slideHeight / 2 - c / 2, 0), i.maxY = -i.minY, i.currentX = Math.max(Math.min(i.currentX, i.maxX), i.minX), i.currentY = Math.max(Math.min(i.currentY, i.maxY), i.minY), t.$imageWrapEl.transition(h).transform("translate3d(" + i.currentX + "px, " + i.currentY + "px,0)");
      }
    },
    onTransitionEnd: function onTransitionEnd() {
      var e = this.zoom,
          t = e.gesture;
      t.$slideEl && this.previousIndex !== this.activeIndex && (t.$imageEl.transform("translate3d(0,0,0) scale(1)"), t.$imageWrapEl.transform("translate3d(0,0,0)"), e.scale = 1, e.currentScale = 1, t.$slideEl = void 0, t.$imageEl = void 0, t.$imageWrapEl = void 0);
    },
    toggle: function toggle(e) {
      var t = this.zoom;
      t.scale && 1 !== t.scale ? t.out() : t.in(e);
    },
    in: function _in(e) {
      var t,
          i,
          a,
          r,
          n,
          o,
          l,
          d,
          h,
          p,
          c,
          u,
          v,
          f,
          m,
          g,
          b = this.zoom,
          w = this.params.zoom,
          y = b.gesture,
          x = b.image;
      (y.$slideEl || (y.$slideEl = this.clickedSlide ? s(this.clickedSlide) : this.slides.eq(this.activeIndex), y.$imageEl = y.$slideEl.find("img, svg, canvas"), y.$imageWrapEl = y.$imageEl.parent("." + w.containerClass)), y.$imageEl && 0 !== y.$imageEl.length) && (y.$slideEl.addClass("" + w.zoomedSlideClass), void 0 === x.touchesStart.x && e ? (t = "touchend" === e.type ? e.changedTouches[0].pageX : e.pageX, i = "touchend" === e.type ? e.changedTouches[0].pageY : e.pageY) : (t = x.touchesStart.x, i = x.touchesStart.y), b.scale = y.$imageWrapEl.attr("data-swiper-zoom") || w.maxRatio, b.currentScale = y.$imageWrapEl.attr("data-swiper-zoom") || w.maxRatio, e ? (m = y.$slideEl[0].offsetWidth, g = y.$slideEl[0].offsetHeight, a = y.$slideEl.offset().left + m / 2 - t, r = y.$slideEl.offset().top + g / 2 - i, l = y.$imageEl[0].offsetWidth, d = y.$imageEl[0].offsetHeight, h = l * b.scale, p = d * b.scale, v = -(c = Math.min(m / 2 - h / 2, 0)), f = -(u = Math.min(g / 2 - p / 2, 0)), (n = a * b.scale) < c && (n = c), n > v && (n = v), (o = r * b.scale) < u && (o = u), o > f && (o = f)) : (n = 0, o = 0), y.$imageWrapEl.transition(300).transform("translate3d(" + n + "px, " + o + "px,0)"), y.$imageEl.transition(300).transform("translate3d(0,0,0) scale(" + b.scale + ")"));
    },
    out: function out() {
      var e = this.zoom,
          t = this.params.zoom,
          i = e.gesture;
      i.$slideEl || (i.$slideEl = this.clickedSlide ? s(this.clickedSlide) : this.slides.eq(this.activeIndex), i.$imageEl = i.$slideEl.find("img, svg, canvas"), i.$imageWrapEl = i.$imageEl.parent("." + t.containerClass)), i.$imageEl && 0 !== i.$imageEl.length && (e.scale = 1, e.currentScale = 1, i.$imageWrapEl.transition(300).transform("translate3d(0,0,0)"), i.$imageEl.transition(300).transform("translate3d(0,0,0) scale(1)"), i.$slideEl.removeClass("" + t.zoomedSlideClass), i.$slideEl = void 0);
    },
    enable: function enable() {
      var e = this.zoom;

      if (!e.enabled) {
        e.enabled = !0;
        var t = !("touchstart" !== this.touchEvents.start || !o.passiveListener || !this.params.passiveListeners) && {
          passive: !0,
          capture: !1
        },
            i = !o.passiveListener || {
          passive: !1,
          capture: !0
        };
        o.gestures ? (this.$wrapperEl.on("gesturestart", ".swiper-slide", e.onGestureStart, t), this.$wrapperEl.on("gesturechange", ".swiper-slide", e.onGestureChange, t), this.$wrapperEl.on("gestureend", ".swiper-slide", e.onGestureEnd, t)) : "touchstart" === this.touchEvents.start && (this.$wrapperEl.on(this.touchEvents.start, ".swiper-slide", e.onGestureStart, t), this.$wrapperEl.on(this.touchEvents.move, ".swiper-slide", e.onGestureChange, i), this.$wrapperEl.on(this.touchEvents.end, ".swiper-slide", e.onGestureEnd, t), this.touchEvents.cancel && this.$wrapperEl.on(this.touchEvents.cancel, ".swiper-slide", e.onGestureEnd, t)), this.$wrapperEl.on(this.touchEvents.move, "." + this.params.zoom.containerClass, e.onTouchMove, i);
      }
    },
    disable: function disable() {
      var e = this.zoom;

      if (e.enabled) {
        this.zoom.enabled = !1;
        var t = !("touchstart" !== this.touchEvents.start || !o.passiveListener || !this.params.passiveListeners) && {
          passive: !0,
          capture: !1
        },
            i = !o.passiveListener || {
          passive: !1,
          capture: !0
        };
        o.gestures ? (this.$wrapperEl.off("gesturestart", ".swiper-slide", e.onGestureStart, t), this.$wrapperEl.off("gesturechange", ".swiper-slide", e.onGestureChange, t), this.$wrapperEl.off("gestureend", ".swiper-slide", e.onGestureEnd, t)) : "touchstart" === this.touchEvents.start && (this.$wrapperEl.off(this.touchEvents.start, ".swiper-slide", e.onGestureStart, t), this.$wrapperEl.off(this.touchEvents.move, ".swiper-slide", e.onGestureChange, i), this.$wrapperEl.off(this.touchEvents.end, ".swiper-slide", e.onGestureEnd, t), this.touchEvents.cancel && this.$wrapperEl.off(this.touchEvents.cancel, ".swiper-slide", e.onGestureEnd, t)), this.$wrapperEl.off(this.touchEvents.move, "." + this.params.zoom.containerClass, e.onTouchMove, i);
      }
    }
  },
      le = {
    loadInSlide: function loadInSlide(e, t) {
      void 0 === t && (t = !0);
      var i = this,
          a = i.params.lazy;

      if (void 0 !== e && 0 !== i.slides.length) {
        var r = i.virtual && i.params.virtual.enabled ? i.$wrapperEl.children("." + i.params.slideClass + '[data-swiper-slide-index="' + e + '"]') : i.slides.eq(e),
            n = r.find("." + a.elementClass + ":not(." + a.loadedClass + "):not(." + a.loadingClass + ")");
        !r.hasClass(a.elementClass) || r.hasClass(a.loadedClass) || r.hasClass(a.loadingClass) || (n = n.add(r[0])), 0 !== n.length && n.each(function (e, n) {
          var o = s(n);
          o.addClass(a.loadingClass);
          var l = o.attr("data-background"),
              d = o.attr("data-src"),
              h = o.attr("data-srcset"),
              p = o.attr("data-sizes");
          i.loadImage(o[0], d || l, h, p, !1, function () {
            if (null != i && i && (!i || i.params) && !i.destroyed) {
              if (l ? (o.css("background-image", 'url("' + l + '")'), o.removeAttr("data-background")) : (h && (o.attr("srcset", h), o.removeAttr("data-srcset")), p && (o.attr("sizes", p), o.removeAttr("data-sizes")), d && (o.attr("src", d), o.removeAttr("data-src"))), o.addClass(a.loadedClass).removeClass(a.loadingClass), r.find("." + a.preloaderClass).remove(), i.params.loop && t) {
                var e = r.attr("data-swiper-slide-index");

                if (r.hasClass(i.params.slideDuplicateClass)) {
                  var s = i.$wrapperEl.children('[data-swiper-slide-index="' + e + '"]:not(.' + i.params.slideDuplicateClass + ")");
                  i.lazy.loadInSlide(s.index(), !1);
                } else {
                  var n = i.$wrapperEl.children("." + i.params.slideDuplicateClass + '[data-swiper-slide-index="' + e + '"]');
                  i.lazy.loadInSlide(n.index(), !1);
                }
              }

              i.emit("lazyImageReady", r[0], o[0]);
            }
          }), i.emit("lazyImageLoad", r[0], o[0]);
        });
      }
    },
    load: function load() {
      var e = this,
          t = e.$wrapperEl,
          i = e.params,
          a = e.slides,
          r = e.activeIndex,
          n = e.virtual && i.virtual.enabled,
          o = i.lazy,
          l = i.slidesPerView;

      function d(e) {
        if (n) {
          if (t.children("." + i.slideClass + '[data-swiper-slide-index="' + e + '"]').length) return !0;
        } else if (a[e]) return !0;

        return !1;
      }

      function h(e) {
        return n ? s(e).attr("data-swiper-slide-index") : s(e).index();
      }

      if ("auto" === l && (l = 0), e.lazy.initialImageLoaded || (e.lazy.initialImageLoaded = !0), e.params.watchSlidesVisibility) t.children("." + i.slideVisibleClass).each(function (t, i) {
        var a = n ? s(i).attr("data-swiper-slide-index") : s(i).index();
        e.lazy.loadInSlide(a);
      });else if (l > 1) for (var p = r; p < r + l; p += 1) {
        d(p) && e.lazy.loadInSlide(p);
      } else e.lazy.loadInSlide(r);
      if (o.loadPrevNext) if (l > 1 || o.loadPrevNextAmount && o.loadPrevNextAmount > 1) {
        for (var c = o.loadPrevNextAmount, u = l, v = Math.min(r + u + Math.max(c, u), a.length), f = Math.max(r - Math.max(u, c), 0), m = r + l; m < v; m += 1) {
          d(m) && e.lazy.loadInSlide(m);
        }

        for (var g = f; g < r; g += 1) {
          d(g) && e.lazy.loadInSlide(g);
        }
      } else {
        var b = t.children("." + i.slideNextClass);
        b.length > 0 && e.lazy.loadInSlide(h(b));
        var w = t.children("." + i.slidePrevClass);
        w.length > 0 && e.lazy.loadInSlide(h(w));
      }
    }
  },
      de = {
    LinearSpline: function LinearSpline(e, t) {
      var i,
          s,
          a,
          r,
          n,
          o = function o(e, t) {
        for (s = -1, i = e.length; i - s > 1;) {
          e[a = i + s >> 1] <= t ? s = a : i = a;
        }

        return i;
      };

      return this.x = e, this.y = t, this.lastIndex = e.length - 1, this.interpolate = function (e) {
        return e ? (n = o(this.x, e), r = n - 1, (e - this.x[r]) * (this.y[n] - this.y[r]) / (this.x[n] - this.x[r]) + this.y[r]) : 0;
      }, this;
    },
    getInterpolateFunction: function getInterpolateFunction(e) {
      this.controller.spline || (this.controller.spline = this.params.loop ? new de.LinearSpline(this.slidesGrid, e.slidesGrid) : new de.LinearSpline(this.snapGrid, e.snapGrid));
    },
    setTranslate: function setTranslate(e, t) {
      var i,
          s,
          a = this,
          r = a.controller.control;

      function n(e) {
        var t = a.rtlTranslate ? -a.translate : a.translate;
        "slide" === a.params.controller.by && (a.controller.getInterpolateFunction(e), s = -a.controller.spline.interpolate(-t)), s && "container" !== a.params.controller.by || (i = (e.maxTranslate() - e.minTranslate()) / (a.maxTranslate() - a.minTranslate()), s = (t - a.minTranslate()) * i + e.minTranslate()), a.params.controller.inverse && (s = e.maxTranslate() - s), e.updateProgress(s), e.setTranslate(s, a), e.updateActiveIndex(), e.updateSlidesClasses();
      }

      if (Array.isArray(r)) for (var o = 0; o < r.length; o += 1) {
        r[o] !== t && r[o] instanceof W && n(r[o]);
      } else r instanceof W && t !== r && n(r);
    },
    setTransition: function setTransition(e, t) {
      var i,
          s = this,
          a = s.controller.control;

      function r(t) {
        t.setTransition(e, s), 0 !== e && (t.transitionStart(), t.params.autoHeight && n.nextTick(function () {
          t.updateAutoHeight();
        }), t.$wrapperEl.transitionEnd(function () {
          a && (t.params.loop && "slide" === s.params.controller.by && t.loopFix(), t.transitionEnd());
        }));
      }

      if (Array.isArray(a)) for (i = 0; i < a.length; i += 1) {
        a[i] !== t && a[i] instanceof W && r(a[i]);
      } else a instanceof W && t !== a && r(a);
    }
  },
      he = {
    makeElFocusable: function makeElFocusable(e) {
      return e.attr("tabIndex", "0"), e;
    },
    addElRole: function addElRole(e, t) {
      return e.attr("role", t), e;
    },
    addElLabel: function addElLabel(e, t) {
      return e.attr("aria-label", t), e;
    },
    disableEl: function disableEl(e) {
      return e.attr("aria-disabled", !0), e;
    },
    enableEl: function enableEl(e) {
      return e.attr("aria-disabled", !1), e;
    },
    onEnterKey: function onEnterKey(e) {
      var t = this.params.a11y;

      if (13 === e.keyCode) {
        var i = s(e.target);
        this.navigation && this.navigation.$nextEl && i.is(this.navigation.$nextEl) && (this.isEnd && !this.params.loop || this.slideNext(), this.isEnd ? this.a11y.notify(t.lastSlideMessage) : this.a11y.notify(t.nextSlideMessage)), this.navigation && this.navigation.$prevEl && i.is(this.navigation.$prevEl) && (this.isBeginning && !this.params.loop || this.slidePrev(), this.isBeginning ? this.a11y.notify(t.firstSlideMessage) : this.a11y.notify(t.prevSlideMessage)), this.pagination && i.is("." + this.params.pagination.bulletClass) && i[0].click();
      }
    },
    notify: function notify(e) {
      var t = this.a11y.liveRegion;
      0 !== t.length && (t.html(""), t.html(e));
    },
    updateNavigation: function updateNavigation() {
      if (!this.params.loop && this.navigation) {
        var e = this.navigation,
            t = e.$nextEl,
            i = e.$prevEl;
        i && i.length > 0 && (this.isBeginning ? this.a11y.disableEl(i) : this.a11y.enableEl(i)), t && t.length > 0 && (this.isEnd ? this.a11y.disableEl(t) : this.a11y.enableEl(t));
      }
    },
    updatePagination: function updatePagination() {
      var e = this,
          t = e.params.a11y;
      e.pagination && e.params.pagination.clickable && e.pagination.bullets && e.pagination.bullets.length && e.pagination.bullets.each(function (i, a) {
        var r = s(a);
        e.a11y.makeElFocusable(r), e.a11y.addElRole(r, "button"), e.a11y.addElLabel(r, t.paginationBulletMessage.replace(/{{index}}/, r.index() + 1));
      });
    },
    init: function init() {
      this.$el.append(this.a11y.liveRegion);
      var e,
          t,
          i = this.params.a11y;
      this.navigation && this.navigation.$nextEl && (e = this.navigation.$nextEl), this.navigation && this.navigation.$prevEl && (t = this.navigation.$prevEl), e && (this.a11y.makeElFocusable(e), this.a11y.addElRole(e, "button"), this.a11y.addElLabel(e, i.nextSlideMessage), e.on("keydown", this.a11y.onEnterKey)), t && (this.a11y.makeElFocusable(t), this.a11y.addElRole(t, "button"), this.a11y.addElLabel(t, i.prevSlideMessage), t.on("keydown", this.a11y.onEnterKey)), this.pagination && this.params.pagination.clickable && this.pagination.bullets && this.pagination.bullets.length && this.pagination.$el.on("keydown", "." + this.params.pagination.bulletClass, this.a11y.onEnterKey);
    },
    destroy: function destroy() {
      var e, t;
      this.a11y.liveRegion && this.a11y.liveRegion.length > 0 && this.a11y.liveRegion.remove(), this.navigation && this.navigation.$nextEl && (e = this.navigation.$nextEl), this.navigation && this.navigation.$prevEl && (t = this.navigation.$prevEl), e && e.off("keydown", this.a11y.onEnterKey), t && t.off("keydown", this.a11y.onEnterKey), this.pagination && this.params.pagination.clickable && this.pagination.bullets && this.pagination.bullets.length && this.pagination.$el.off("keydown", "." + this.params.pagination.bulletClass, this.a11y.onEnterKey);
    }
  },
      pe = {
    init: function init() {
      if (this.params.history) {
        if (!t.history || !t.history.pushState) return this.params.history.enabled = !1, void (this.params.hashNavigation.enabled = !0);
        var e = this.history;
        e.initialized = !0, e.paths = pe.getPathValues(), (e.paths.key || e.paths.value) && (e.scrollToSlide(0, e.paths.value, this.params.runCallbacksOnInit), this.params.history.replaceState || t.addEventListener("popstate", this.history.setHistoryPopState));
      }
    },
    destroy: function destroy() {
      this.params.history.replaceState || t.removeEventListener("popstate", this.history.setHistoryPopState);
    },
    setHistoryPopState: function setHistoryPopState() {
      this.history.paths = pe.getPathValues(), this.history.scrollToSlide(this.params.speed, this.history.paths.value, !1);
    },
    getPathValues: function getPathValues() {
      var e = t.location.pathname.slice(1).split("/").filter(function (e) {
        return "" !== e;
      }),
          i = e.length;
      return {
        key: e[i - 2],
        value: e[i - 1]
      };
    },
    setHistory: function setHistory(e, i) {
      if (this.history.initialized && this.params.history.enabled) {
        var s = this.slides.eq(i),
            a = pe.slugify(s.attr("data-history"));
        t.location.pathname.includes(e) || (a = e + "/" + a);
        var r = t.history.state;
        r && r.value === a || (this.params.history.replaceState ? t.history.replaceState({
          value: a
        }, null, a) : t.history.pushState({
          value: a
        }, null, a));
      }
    },
    slugify: function slugify(e) {
      return e.toString().replace(/\s+/g, "-").replace(/[^\w-]+/g, "").replace(/--+/g, "-").replace(/^-+/, "").replace(/-+$/, "");
    },
    scrollToSlide: function scrollToSlide(e, t, i) {
      if (t) for (var s = 0, a = this.slides.length; s < a; s += 1) {
        var r = this.slides.eq(s);

        if (pe.slugify(r.attr("data-history")) === t && !r.hasClass(this.params.slideDuplicateClass)) {
          var n = r.index();
          this.slideTo(n, e, i);
        }
      } else this.slideTo(0, e, i);
    }
  },
      ce = {
    onHashCange: function onHashCange() {
      var t = e.location.hash.replace("#", "");

      if (t !== this.slides.eq(this.activeIndex).attr("data-hash")) {
        var i = this.$wrapperEl.children("." + this.params.slideClass + '[data-hash="' + t + '"]').index();
        if (void 0 === i) return;
        this.slideTo(i);
      }
    },
    setHash: function setHash() {
      if (this.hashNavigation.initialized && this.params.hashNavigation.enabled) if (this.params.hashNavigation.replaceState && t.history && t.history.replaceState) t.history.replaceState(null, null, "#" + this.slides.eq(this.activeIndex).attr("data-hash") || "");else {
        var i = this.slides.eq(this.activeIndex),
            s = i.attr("data-hash") || i.attr("data-history");
        e.location.hash = s || "";
      }
    },
    init: function init() {
      if (!(!this.params.hashNavigation.enabled || this.params.history && this.params.history.enabled)) {
        this.hashNavigation.initialized = !0;
        var i = e.location.hash.replace("#", "");
        if (i) for (var a = 0, r = this.slides.length; a < r; a += 1) {
          var n = this.slides.eq(a);

          if ((n.attr("data-hash") || n.attr("data-history")) === i && !n.hasClass(this.params.slideDuplicateClass)) {
            var o = n.index();
            this.slideTo(o, 0, this.params.runCallbacksOnInit, !0);
          }
        }
        this.params.hashNavigation.watchState && s(t).on("hashchange", this.hashNavigation.onHashCange);
      }
    },
    destroy: function destroy() {
      this.params.hashNavigation.watchState && s(t).off("hashchange", this.hashNavigation.onHashCange);
    }
  },
      ue = {
    run: function run() {
      var e = this,
          t = e.slides.eq(e.activeIndex),
          i = e.params.autoplay.delay;
      t.attr("data-swiper-autoplay") && (i = t.attr("data-swiper-autoplay") || e.params.autoplay.delay), clearTimeout(e.autoplay.timeout), e.autoplay.timeout = n.nextTick(function () {
        e.params.autoplay.reverseDirection ? e.params.loop ? (e.loopFix(), e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.isBeginning ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(e.slides.length - 1, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slidePrev(e.params.speed, !0, !0), e.emit("autoplay")) : e.params.loop ? (e.loopFix(), e.slideNext(e.params.speed, !0, !0), e.emit("autoplay")) : e.isEnd ? e.params.autoplay.stopOnLastSlide ? e.autoplay.stop() : (e.slideTo(0, e.params.speed, !0, !0), e.emit("autoplay")) : (e.slideNext(e.params.speed, !0, !0), e.emit("autoplay")), e.params.cssMode && e.autoplay.running && e.autoplay.run();
      }, i);
    },
    start: function start() {
      return void 0 === this.autoplay.timeout && !this.autoplay.running && (this.autoplay.running = !0, this.emit("autoplayStart"), this.autoplay.run(), !0);
    },
    stop: function stop() {
      return !!this.autoplay.running && void 0 !== this.autoplay.timeout && (this.autoplay.timeout && (clearTimeout(this.autoplay.timeout), this.autoplay.timeout = void 0), this.autoplay.running = !1, this.emit("autoplayStop"), !0);
    },
    pause: function pause(e) {
      this.autoplay.running && (this.autoplay.paused || (this.autoplay.timeout && clearTimeout(this.autoplay.timeout), this.autoplay.paused = !0, 0 !== e && this.params.autoplay.waitForTransition ? (this.$wrapperEl[0].addEventListener("transitionend", this.autoplay.onTransitionEnd), this.$wrapperEl[0].addEventListener("webkitTransitionEnd", this.autoplay.onTransitionEnd)) : (this.autoplay.paused = !1, this.autoplay.run())));
    }
  },
      ve = {
    setTranslate: function setTranslate() {
      for (var e = this.slides, t = 0; t < e.length; t += 1) {
        var i = this.slides.eq(t),
            s = -i[0].swiperSlideOffset;
        this.params.virtualTranslate || (s -= this.translate);
        var a = 0;
        this.isHorizontal() || (a = s, s = 0);
        var r = this.params.fadeEffect.crossFade ? Math.max(1 - Math.abs(i[0].progress), 0) : 1 + Math.min(Math.max(i[0].progress, -1), 0);
        i.css({
          opacity: r
        }).transform("translate3d(" + s + "px, " + a + "px, 0px)");
      }
    },
    setTransition: function setTransition(e) {
      var t = this,
          i = t.slides,
          s = t.$wrapperEl;

      if (i.transition(e), t.params.virtualTranslate && 0 !== e) {
        var a = !1;
        i.transitionEnd(function () {
          if (!a && t && !t.destroyed) {
            a = !0, t.animating = !1;

            for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1) {
              s.trigger(e[i]);
            }
          }
        });
      }
    }
  },
      fe = {
    setTranslate: function setTranslate() {
      var e,
          t = this.$el,
          i = this.$wrapperEl,
          a = this.slides,
          r = this.width,
          n = this.height,
          o = this.rtlTranslate,
          l = this.size,
          d = this.params.cubeEffect,
          h = this.isHorizontal(),
          p = this.virtual && this.params.virtual.enabled,
          c = 0;
      d.shadow && (h ? (0 === (e = i.find(".swiper-cube-shadow")).length && (e = s('<div class="swiper-cube-shadow"></div>'), i.append(e)), e.css({
        height: r + "px"
      })) : 0 === (e = t.find(".swiper-cube-shadow")).length && (e = s('<div class="swiper-cube-shadow"></div>'), t.append(e)));

      for (var u = 0; u < a.length; u += 1) {
        var v = a.eq(u),
            f = u;
        p && (f = parseInt(v.attr("data-swiper-slide-index"), 10));
        var m = 90 * f,
            g = Math.floor(m / 360);
        o && (m = -m, g = Math.floor(-m / 360));
        var b = Math.max(Math.min(v[0].progress, 1), -1),
            w = 0,
            y = 0,
            x = 0;
        f % 4 == 0 ? (w = 4 * -g * l, x = 0) : (f - 1) % 4 == 0 ? (w = 0, x = 4 * -g * l) : (f - 2) % 4 == 0 ? (w = l + 4 * g * l, x = l) : (f - 3) % 4 == 0 && (w = -l, x = 3 * l + 4 * l * g), o && (w = -w), h || (y = w, w = 0);
        var T = "rotateX(" + (h ? 0 : -m) + "deg) rotateY(" + (h ? m : 0) + "deg) translate3d(" + w + "px, " + y + "px, " + x + "px)";

        if (b <= 1 && b > -1 && (c = 90 * f + 90 * b, o && (c = 90 * -f - 90 * b)), v.transform(T), d.slideShadows) {
          var E = h ? v.find(".swiper-slide-shadow-left") : v.find(".swiper-slide-shadow-top"),
              S = h ? v.find(".swiper-slide-shadow-right") : v.find(".swiper-slide-shadow-bottom");
          0 === E.length && (E = s('<div class="swiper-slide-shadow-' + (h ? "left" : "top") + '"></div>'), v.append(E)), 0 === S.length && (S = s('<div class="swiper-slide-shadow-' + (h ? "right" : "bottom") + '"></div>'), v.append(S)), E.length && (E[0].style.opacity = Math.max(-b, 0)), S.length && (S[0].style.opacity = Math.max(b, 0));
        }
      }

      if (i.css({
        "-webkit-transform-origin": "50% 50% -" + l / 2 + "px",
        "-moz-transform-origin": "50% 50% -" + l / 2 + "px",
        "-ms-transform-origin": "50% 50% -" + l / 2 + "px",
        "transform-origin": "50% 50% -" + l / 2 + "px"
      }), d.shadow) if (h) e.transform("translate3d(0px, " + (r / 2 + d.shadowOffset) + "px, " + -r / 2 + "px) rotateX(90deg) rotateZ(0deg) scale(" + d.shadowScale + ")");else {
        var C = Math.abs(c) - 90 * Math.floor(Math.abs(c) / 90),
            M = 1.5 - (Math.sin(2 * C * Math.PI / 360) / 2 + Math.cos(2 * C * Math.PI / 360) / 2),
            P = d.shadowScale,
            z = d.shadowScale / M,
            k = d.shadowOffset;
        e.transform("scale3d(" + P + ", 1, " + z + ") translate3d(0px, " + (n / 2 + k) + "px, " + -n / 2 / z + "px) rotateX(-90deg)");
      }
      var $ = j.isSafari || j.isUiWebView ? -l / 2 : 0;
      i.transform("translate3d(0px,0," + $ + "px) rotateX(" + (this.isHorizontal() ? 0 : c) + "deg) rotateY(" + (this.isHorizontal() ? -c : 0) + "deg)");
    },
    setTransition: function setTransition(e) {
      var t = this.$el;
      this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), this.params.cubeEffect.shadow && !this.isHorizontal() && t.find(".swiper-cube-shadow").transition(e);
    }
  },
      me = {
    setTranslate: function setTranslate() {
      for (var e = this.slides, t = this.rtlTranslate, i = 0; i < e.length; i += 1) {
        var a = e.eq(i),
            r = a[0].progress;
        this.params.flipEffect.limitRotation && (r = Math.max(Math.min(a[0].progress, 1), -1));
        var n = -180 * r,
            o = 0,
            l = -a[0].swiperSlideOffset,
            d = 0;

        if (this.isHorizontal() ? t && (n = -n) : (d = l, l = 0, o = -n, n = 0), a[0].style.zIndex = -Math.abs(Math.round(r)) + e.length, this.params.flipEffect.slideShadows) {
          var h = this.isHorizontal() ? a.find(".swiper-slide-shadow-left") : a.find(".swiper-slide-shadow-top"),
              p = this.isHorizontal() ? a.find(".swiper-slide-shadow-right") : a.find(".swiper-slide-shadow-bottom");
          0 === h.length && (h = s('<div class="swiper-slide-shadow-' + (this.isHorizontal() ? "left" : "top") + '"></div>'), a.append(h)), 0 === p.length && (p = s('<div class="swiper-slide-shadow-' + (this.isHorizontal() ? "right" : "bottom") + '"></div>'), a.append(p)), h.length && (h[0].style.opacity = Math.max(-r, 0)), p.length && (p[0].style.opacity = Math.max(r, 0));
        }

        a.transform("translate3d(" + l + "px, " + d + "px, 0px) rotateX(" + o + "deg) rotateY(" + n + "deg)");
      }
    },
    setTransition: function setTransition(e) {
      var t = this,
          i = t.slides,
          s = t.activeIndex,
          a = t.$wrapperEl;

      if (i.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e), t.params.virtualTranslate && 0 !== e) {
        var r = !1;
        i.eq(s).transitionEnd(function () {
          if (!r && t && !t.destroyed) {
            r = !0, t.animating = !1;

            for (var e = ["webkitTransitionEnd", "transitionend"], i = 0; i < e.length; i += 1) {
              a.trigger(e[i]);
            }
          }
        });
      }
    }
  },
      ge = {
    setTranslate: function setTranslate() {
      for (var e = this.width, t = this.height, i = this.slides, a = this.$wrapperEl, r = this.slidesSizesGrid, n = this.params.coverflowEffect, l = this.isHorizontal(), d = this.translate, h = l ? e / 2 - d : t / 2 - d, p = l ? n.rotate : -n.rotate, c = n.depth, u = 0, v = i.length; u < v; u += 1) {
        var f = i.eq(u),
            m = r[u],
            g = (h - f[0].swiperSlideOffset - m / 2) / m * n.modifier,
            b = l ? p * g : 0,
            w = l ? 0 : p * g,
            y = -c * Math.abs(g),
            x = l ? 0 : n.stretch * g,
            T = l ? n.stretch * g : 0;
        Math.abs(T) < .001 && (T = 0), Math.abs(x) < .001 && (x = 0), Math.abs(y) < .001 && (y = 0), Math.abs(b) < .001 && (b = 0), Math.abs(w) < .001 && (w = 0);
        var E = "translate3d(" + T + "px," + x + "px," + y + "px)  rotateX(" + w + "deg) rotateY(" + b + "deg)";

        if (f.transform(E), f[0].style.zIndex = 1 - Math.abs(Math.round(g)), n.slideShadows) {
          var S = l ? f.find(".swiper-slide-shadow-left") : f.find(".swiper-slide-shadow-top"),
              C = l ? f.find(".swiper-slide-shadow-right") : f.find(".swiper-slide-shadow-bottom");
          0 === S.length && (S = s('<div class="swiper-slide-shadow-' + (l ? "left" : "top") + '"></div>'), f.append(S)), 0 === C.length && (C = s('<div class="swiper-slide-shadow-' + (l ? "right" : "bottom") + '"></div>'), f.append(C)), S.length && (S[0].style.opacity = g > 0 ? g : 0), C.length && (C[0].style.opacity = -g > 0 ? -g : 0);
        }
      }

      (o.pointerEvents || o.prefixedPointerEvents) && (a[0].style.perspectiveOrigin = h + "px 50%");
    },
    setTransition: function setTransition(e) {
      this.slides.transition(e).find(".swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left").transition(e);
    }
  },
      be = {
    init: function init() {
      var e = this.params.thumbs,
          t = this.constructor;
      e.swiper instanceof t ? (this.thumbs.swiper = e.swiper, n.extend(this.thumbs.swiper.originalParams, {
        watchSlidesProgress: !0,
        slideToClickedSlide: !1
      }), n.extend(this.thumbs.swiper.params, {
        watchSlidesProgress: !0,
        slideToClickedSlide: !1
      })) : n.isObject(e.swiper) && (this.thumbs.swiper = new t(n.extend({}, e.swiper, {
        watchSlidesVisibility: !0,
        watchSlidesProgress: !0,
        slideToClickedSlide: !1
      })), this.thumbs.swiperCreated = !0), this.thumbs.swiper.$el.addClass(this.params.thumbs.thumbsContainerClass), this.thumbs.swiper.on("tap", this.thumbs.onThumbClick);
    },
    onThumbClick: function onThumbClick() {
      var e = this.thumbs.swiper;

      if (e) {
        var t = e.clickedIndex,
            i = e.clickedSlide;

        if (!(i && s(i).hasClass(this.params.thumbs.slideThumbActiveClass) || null == t)) {
          var a;

          if (a = e.params.loop ? parseInt(s(e.clickedSlide).attr("data-swiper-slide-index"), 10) : t, this.params.loop) {
            var r = this.activeIndex;
            this.slides.eq(r).hasClass(this.params.slideDuplicateClass) && (this.loopFix(), this._clientLeft = this.$wrapperEl[0].clientLeft, r = this.activeIndex);
            var n = this.slides.eq(r).prevAll('[data-swiper-slide-index="' + a + '"]').eq(0).index(),
                o = this.slides.eq(r).nextAll('[data-swiper-slide-index="' + a + '"]').eq(0).index();
            a = void 0 === n ? o : void 0 === o ? n : o - r < r - n ? o : n;
          }

          this.slideTo(a);
        }
      }
    },
    update: function update(e) {
      var t = this.thumbs.swiper;

      if (t) {
        var i = "auto" === t.params.slidesPerView ? t.slidesPerViewDynamic() : t.params.slidesPerView;

        if (this.realIndex !== t.realIndex) {
          var s,
              a = t.activeIndex;

          if (t.params.loop) {
            t.slides.eq(a).hasClass(t.params.slideDuplicateClass) && (t.loopFix(), t._clientLeft = t.$wrapperEl[0].clientLeft, a = t.activeIndex);
            var r = t.slides.eq(a).prevAll('[data-swiper-slide-index="' + this.realIndex + '"]').eq(0).index(),
                n = t.slides.eq(a).nextAll('[data-swiper-slide-index="' + this.realIndex + '"]').eq(0).index();
            s = void 0 === r ? n : void 0 === n ? r : n - a == a - r ? a : n - a < a - r ? n : r;
          } else s = this.realIndex;

          t.visibleSlidesIndexes && t.visibleSlidesIndexes.indexOf(s) < 0 && (t.params.centeredSlides ? s = s > a ? s - Math.floor(i / 2) + 1 : s + Math.floor(i / 2) - 1 : s > a && (s = s - i + 1), t.slideTo(s, e ? 0 : void 0));
        }

        var o = 1,
            l = this.params.thumbs.slideThumbActiveClass;
        if (this.params.slidesPerView > 1 && !this.params.centeredSlides && (o = this.params.slidesPerView), this.params.thumbs.multipleActiveThumbs || (o = 1), o = Math.floor(o), t.slides.removeClass(l), t.params.loop || t.params.virtual && t.params.virtual.enabled) for (var d = 0; d < o; d += 1) {
          t.$wrapperEl.children('[data-swiper-slide-index="' + (this.realIndex + d) + '"]').addClass(l);
        } else for (var h = 0; h < o; h += 1) {
          t.slides.eq(this.realIndex + h).addClass(l);
        }
      }
    }
  },
      we = [R, q, K, U, Z, J, te, {
    name: "mousewheel",
    params: {
      mousewheel: {
        enabled: !1,
        releaseOnEdges: !1,
        invert: !1,
        forceToAxis: !1,
        sensitivity: 1,
        eventsTarged: "container"
      }
    },
    create: function create() {
      n.extend(this, {
        mousewheel: {
          enabled: !1,
          enable: ie.enable.bind(this),
          disable: ie.disable.bind(this),
          handle: ie.handle.bind(this),
          handleMouseEnter: ie.handleMouseEnter.bind(this),
          handleMouseLeave: ie.handleMouseLeave.bind(this),
          animateSlider: ie.animateSlider.bind(this),
          releaseScroll: ie.releaseScroll.bind(this),
          lastScrollTime: n.now(),
          lastEventBeforeSnap: void 0,
          recentWheelEvents: []
        }
      });
    },
    on: {
      init: function init() {
        !this.params.mousewheel.enabled && this.params.cssMode && this.mousewheel.disable(), this.params.mousewheel.enabled && this.mousewheel.enable();
      },
      destroy: function destroy() {
        this.params.cssMode && this.mousewheel.enable(), this.mousewheel.enabled && this.mousewheel.disable();
      }
    }
  }, {
    name: "navigation",
    params: {
      navigation: {
        nextEl: null,
        prevEl: null,
        hideOnClick: !1,
        disabledClass: "swiper-button-disabled",
        hiddenClass: "swiper-button-hidden",
        lockClass: "swiper-button-lock"
      }
    },
    create: function create() {
      n.extend(this, {
        navigation: {
          init: se.init.bind(this),
          update: se.update.bind(this),
          destroy: se.destroy.bind(this),
          onNextClick: se.onNextClick.bind(this),
          onPrevClick: se.onPrevClick.bind(this)
        }
      });
    },
    on: {
      init: function init() {
        this.navigation.init(), this.navigation.update();
      },
      toEdge: function toEdge() {
        this.navigation.update();
      },
      fromEdge: function fromEdge() {
        this.navigation.update();
      },
      destroy: function destroy() {
        this.navigation.destroy();
      },
      click: function click(e) {
        var t,
            i = this.navigation,
            a = i.$nextEl,
            r = i.$prevEl;
        !this.params.navigation.hideOnClick || s(e.target).is(r) || s(e.target).is(a) || (a ? t = a.hasClass(this.params.navigation.hiddenClass) : r && (t = r.hasClass(this.params.navigation.hiddenClass)), !0 === t ? this.emit("navigationShow", this) : this.emit("navigationHide", this), a && a.toggleClass(this.params.navigation.hiddenClass), r && r.toggleClass(this.params.navigation.hiddenClass));
      }
    }
  }, {
    name: "pagination",
    params: {
      pagination: {
        el: null,
        bulletElement: "span",
        clickable: !1,
        hideOnClick: !1,
        renderBullet: null,
        renderProgressbar: null,
        renderFraction: null,
        renderCustom: null,
        progressbarOpposite: !1,
        type: "bullets",
        dynamicBullets: !1,
        dynamicMainBullets: 1,
        formatFractionCurrent: function formatFractionCurrent(e) {
          return e;
        },
        formatFractionTotal: function formatFractionTotal(e) {
          return e;
        },
        bulletClass: "swiper-pagination-bullet",
        bulletActiveClass: "swiper-pagination-bullet-active",
        modifierClass: "swiper-pagination-",
        currentClass: "swiper-pagination-current",
        totalClass: "swiper-pagination-total",
        hiddenClass: "swiper-pagination-hidden",
        progressbarFillClass: "swiper-pagination-progressbar-fill",
        progressbarOppositeClass: "swiper-pagination-progressbar-opposite",
        clickableClass: "swiper-pagination-clickable",
        lockClass: "swiper-pagination-lock"
      }
    },
    create: function create() {
      n.extend(this, {
        pagination: {
          init: ae.init.bind(this),
          render: ae.render.bind(this),
          update: ae.update.bind(this),
          destroy: ae.destroy.bind(this),
          dynamicBulletIndex: 0
        }
      });
    },
    on: {
      init: function init() {
        this.pagination.init(), this.pagination.render(), this.pagination.update();
      },
      activeIndexChange: function activeIndexChange() {
        this.params.loop ? this.pagination.update() : void 0 === this.snapIndex && this.pagination.update();
      },
      snapIndexChange: function snapIndexChange() {
        this.params.loop || this.pagination.update();
      },
      slidesLengthChange: function slidesLengthChange() {
        this.params.loop && (this.pagination.render(), this.pagination.update());
      },
      snapGridLengthChange: function snapGridLengthChange() {
        this.params.loop || (this.pagination.render(), this.pagination.update());
      },
      destroy: function destroy() {
        this.pagination.destroy();
      },
      click: function click(e) {
        this.params.pagination.el && this.params.pagination.hideOnClick && this.pagination.$el.length > 0 && !s(e.target).hasClass(this.params.pagination.bulletClass) && (!0 === this.pagination.$el.hasClass(this.params.pagination.hiddenClass) ? this.emit("paginationShow", this) : this.emit("paginationHide", this), this.pagination.$el.toggleClass(this.params.pagination.hiddenClass));
      }
    }
  }, {
    name: "scrollbar",
    params: {
      scrollbar: {
        el: null,
        dragSize: "auto",
        hide: !1,
        draggable: !1,
        snapOnRelease: !0,
        lockClass: "swiper-scrollbar-lock",
        dragClass: "swiper-scrollbar-drag"
      }
    },
    create: function create() {
      n.extend(this, {
        scrollbar: {
          init: re.init.bind(this),
          destroy: re.destroy.bind(this),
          updateSize: re.updateSize.bind(this),
          setTranslate: re.setTranslate.bind(this),
          setTransition: re.setTransition.bind(this),
          enableDraggable: re.enableDraggable.bind(this),
          disableDraggable: re.disableDraggable.bind(this),
          setDragPosition: re.setDragPosition.bind(this),
          getPointerPosition: re.getPointerPosition.bind(this),
          onDragStart: re.onDragStart.bind(this),
          onDragMove: re.onDragMove.bind(this),
          onDragEnd: re.onDragEnd.bind(this),
          isTouched: !1,
          timeout: null,
          dragTimeout: null
        }
      });
    },
    on: {
      init: function init() {
        this.scrollbar.init(), this.scrollbar.updateSize(), this.scrollbar.setTranslate();
      },
      update: function update() {
        this.scrollbar.updateSize();
      },
      resize: function resize() {
        this.scrollbar.updateSize();
      },
      observerUpdate: function observerUpdate() {
        this.scrollbar.updateSize();
      },
      setTranslate: function setTranslate() {
        this.scrollbar.setTranslate();
      },
      setTransition: function setTransition(e) {
        this.scrollbar.setTransition(e);
      },
      destroy: function destroy() {
        this.scrollbar.destroy();
      }
    }
  }, {
    name: "parallax",
    params: {
      parallax: {
        enabled: !1
      }
    },
    create: function create() {
      n.extend(this, {
        parallax: {
          setTransform: ne.setTransform.bind(this),
          setTranslate: ne.setTranslate.bind(this),
          setTransition: ne.setTransition.bind(this)
        }
      });
    },
    on: {
      beforeInit: function beforeInit() {
        this.params.parallax.enabled && (this.params.watchSlidesProgress = !0, this.originalParams.watchSlidesProgress = !0);
      },
      init: function init() {
        this.params.parallax.enabled && this.parallax.setTranslate();
      },
      setTranslate: function setTranslate() {
        this.params.parallax.enabled && this.parallax.setTranslate();
      },
      setTransition: function setTransition(e) {
        this.params.parallax.enabled && this.parallax.setTransition(e);
      }
    }
  }, {
    name: "zoom",
    params: {
      zoom: {
        enabled: !1,
        maxRatio: 3,
        minRatio: 1,
        toggle: !0,
        containerClass: "swiper-zoom-container",
        zoomedSlideClass: "swiper-slide-zoomed"
      }
    },
    create: function create() {
      var e = this,
          t = {
        enabled: !1,
        scale: 1,
        currentScale: 1,
        isScaling: !1,
        gesture: {
          $slideEl: void 0,
          slideWidth: void 0,
          slideHeight: void 0,
          $imageEl: void 0,
          $imageWrapEl: void 0,
          maxRatio: 3
        },
        image: {
          isTouched: void 0,
          isMoved: void 0,
          currentX: void 0,
          currentY: void 0,
          minX: void 0,
          minY: void 0,
          maxX: void 0,
          maxY: void 0,
          width: void 0,
          height: void 0,
          startX: void 0,
          startY: void 0,
          touchesStart: {},
          touchesCurrent: {}
        },
        velocity: {
          x: void 0,
          y: void 0,
          prevPositionX: void 0,
          prevPositionY: void 0,
          prevTime: void 0
        }
      };
      "onGestureStart onGestureChange onGestureEnd onTouchStart onTouchMove onTouchEnd onTransitionEnd toggle enable disable in out".split(" ").forEach(function (i) {
        t[i] = oe[i].bind(e);
      }), n.extend(e, {
        zoom: t
      });
      var i = 1;
      Object.defineProperty(e.zoom, "scale", {
        get: function get() {
          return i;
        },
        set: function set(t) {
          if (i !== t) {
            var s = e.zoom.gesture.$imageEl ? e.zoom.gesture.$imageEl[0] : void 0,
                a = e.zoom.gesture.$slideEl ? e.zoom.gesture.$slideEl[0] : void 0;
            e.emit("zoomChange", t, s, a);
          }

          i = t;
        }
      });
    },
    on: {
      init: function init() {
        this.params.zoom.enabled && this.zoom.enable();
      },
      destroy: function destroy() {
        this.zoom.disable();
      },
      touchStart: function touchStart(e) {
        this.zoom.enabled && this.zoom.onTouchStart(e);
      },
      touchEnd: function touchEnd(e) {
        this.zoom.enabled && this.zoom.onTouchEnd(e);
      },
      doubleTap: function doubleTap(e) {
        this.params.zoom.enabled && this.zoom.enabled && this.params.zoom.toggle && this.zoom.toggle(e);
      },
      transitionEnd: function transitionEnd() {
        this.zoom.enabled && this.params.zoom.enabled && this.zoom.onTransitionEnd();
      },
      slideChange: function slideChange() {
        this.zoom.enabled && this.params.zoom.enabled && this.params.cssMode && this.zoom.onTransitionEnd();
      }
    }
  }, {
    name: "lazy",
    params: {
      lazy: {
        enabled: !1,
        loadPrevNext: !1,
        loadPrevNextAmount: 1,
        loadOnTransitionStart: !1,
        elementClass: "swiper-lazy",
        loadingClass: "swiper-lazy-loading",
        loadedClass: "swiper-lazy-loaded",
        preloaderClass: "swiper-lazy-preloader"
      }
    },
    create: function create() {
      n.extend(this, {
        lazy: {
          initialImageLoaded: !1,
          load: le.load.bind(this),
          loadInSlide: le.loadInSlide.bind(this)
        }
      });
    },
    on: {
      beforeInit: function beforeInit() {
        this.params.lazy.enabled && this.params.preloadImages && (this.params.preloadImages = !1);
      },
      init: function init() {
        this.params.lazy.enabled && !this.params.loop && 0 === this.params.initialSlide && this.lazy.load();
      },
      scroll: function scroll() {
        this.params.freeMode && !this.params.freeModeSticky && this.lazy.load();
      },
      resize: function resize() {
        this.params.lazy.enabled && this.lazy.load();
      },
      scrollbarDragMove: function scrollbarDragMove() {
        this.params.lazy.enabled && this.lazy.load();
      },
      transitionStart: function transitionStart() {
        this.params.lazy.enabled && (this.params.lazy.loadOnTransitionStart || !this.params.lazy.loadOnTransitionStart && !this.lazy.initialImageLoaded) && this.lazy.load();
      },
      transitionEnd: function transitionEnd() {
        this.params.lazy.enabled && !this.params.lazy.loadOnTransitionStart && this.lazy.load();
      },
      slideChange: function slideChange() {
        this.params.lazy.enabled && this.params.cssMode && this.lazy.load();
      }
    }
  }, {
    name: "controller",
    params: {
      controller: {
        control: void 0,
        inverse: !1,
        by: "slide"
      }
    },
    create: function create() {
      n.extend(this, {
        controller: {
          control: this.params.controller.control,
          getInterpolateFunction: de.getInterpolateFunction.bind(this),
          setTranslate: de.setTranslate.bind(this),
          setTransition: de.setTransition.bind(this)
        }
      });
    },
    on: {
      update: function update() {
        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline);
      },
      resize: function resize() {
        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline);
      },
      observerUpdate: function observerUpdate() {
        this.controller.control && this.controller.spline && (this.controller.spline = void 0, delete this.controller.spline);
      },
      setTranslate: function setTranslate(e, t) {
        this.controller.control && this.controller.setTranslate(e, t);
      },
      setTransition: function setTransition(e, t) {
        this.controller.control && this.controller.setTransition(e, t);
      }
    }
  }, {
    name: "a11y",
    params: {
      a11y: {
        enabled: !0,
        notificationClass: "swiper-notification",
        prevSlideMessage: "Previous slide",
        nextSlideMessage: "Next slide",
        firstSlideMessage: "This is the first slide",
        lastSlideMessage: "This is the last slide",
        paginationBulletMessage: "Go to slide {{index}}"
      }
    },
    create: function create() {
      var e = this;
      n.extend(e, {
        a11y: {
          liveRegion: s('<span class="' + e.params.a11y.notificationClass + '" aria-live="assertive" aria-atomic="true"></span>')
        }
      }), Object.keys(he).forEach(function (t) {
        e.a11y[t] = he[t].bind(e);
      });
    },
    on: {
      init: function init() {
        this.params.a11y.enabled && (this.a11y.init(), this.a11y.updateNavigation());
      },
      toEdge: function toEdge() {
        this.params.a11y.enabled && this.a11y.updateNavigation();
      },
      fromEdge: function fromEdge() {
        this.params.a11y.enabled && this.a11y.updateNavigation();
      },
      paginationUpdate: function paginationUpdate() {
        this.params.a11y.enabled && this.a11y.updatePagination();
      },
      destroy: function destroy() {
        this.params.a11y.enabled && this.a11y.destroy();
      }
    }
  }, {
    name: "history",
    params: {
      history: {
        enabled: !1,
        replaceState: !1,
        key: "slides"
      }
    },
    create: function create() {
      n.extend(this, {
        history: {
          init: pe.init.bind(this),
          setHistory: pe.setHistory.bind(this),
          setHistoryPopState: pe.setHistoryPopState.bind(this),
          scrollToSlide: pe.scrollToSlide.bind(this),
          destroy: pe.destroy.bind(this)
        }
      });
    },
    on: {
      init: function init() {
        this.params.history.enabled && this.history.init();
      },
      destroy: function destroy() {
        this.params.history.enabled && this.history.destroy();
      },
      transitionEnd: function transitionEnd() {
        this.history.initialized && this.history.setHistory(this.params.history.key, this.activeIndex);
      },
      slideChange: function slideChange() {
        this.history.initialized && this.params.cssMode && this.history.setHistory(this.params.history.key, this.activeIndex);
      }
    }
  }, {
    name: "hash-navigation",
    params: {
      hashNavigation: {
        enabled: !1,
        replaceState: !1,
        watchState: !1
      }
    },
    create: function create() {
      n.extend(this, {
        hashNavigation: {
          initialized: !1,
          init: ce.init.bind(this),
          destroy: ce.destroy.bind(this),
          setHash: ce.setHash.bind(this),
          onHashCange: ce.onHashCange.bind(this)
        }
      });
    },
    on: {
      init: function init() {
        this.params.hashNavigation.enabled && this.hashNavigation.init();
      },
      destroy: function destroy() {
        this.params.hashNavigation.enabled && this.hashNavigation.destroy();
      },
      transitionEnd: function transitionEnd() {
        this.hashNavigation.initialized && this.hashNavigation.setHash();
      },
      slideChange: function slideChange() {
        this.hashNavigation.initialized && this.params.cssMode && this.hashNavigation.setHash();
      }
    }
  }, {
    name: "autoplay",
    params: {
      autoplay: {
        enabled: !1,
        delay: 3e3,
        waitForTransition: !0,
        disableOnInteraction: !0,
        stopOnLastSlide: !1,
        reverseDirection: !1
      }
    },
    create: function create() {
      var e = this;
      n.extend(e, {
        autoplay: {
          running: !1,
          paused: !1,
          run: ue.run.bind(e),
          start: ue.start.bind(e),
          stop: ue.stop.bind(e),
          pause: ue.pause.bind(e),
          onVisibilityChange: function onVisibilityChange() {
            "hidden" === document.visibilityState && e.autoplay.running && e.autoplay.pause(), "visible" === document.visibilityState && e.autoplay.paused && (e.autoplay.run(), e.autoplay.paused = !1);
          },
          onTransitionEnd: function onTransitionEnd(t) {
            e && !e.destroyed && e.$wrapperEl && t.target === this && (e.$wrapperEl[0].removeEventListener("transitionend", e.autoplay.onTransitionEnd), e.$wrapperEl[0].removeEventListener("webkitTransitionEnd", e.autoplay.onTransitionEnd), e.autoplay.paused = !1, e.autoplay.running ? e.autoplay.run() : e.autoplay.stop());
          }
        }
      });
    },
    on: {
      init: function init() {
        this.params.autoplay.enabled && (this.autoplay.start(), document.addEventListener("visibilitychange", this.autoplay.onVisibilityChange));
      },
      beforeTransitionStart: function beforeTransitionStart(e, t) {
        this.autoplay.running && (t || !this.params.autoplay.disableOnInteraction ? this.autoplay.pause(e) : this.autoplay.stop());
      },
      sliderFirstMove: function sliderFirstMove() {
        this.autoplay.running && (this.params.autoplay.disableOnInteraction ? this.autoplay.stop() : this.autoplay.pause());
      },
      touchEnd: function touchEnd() {
        this.params.cssMode && this.autoplay.paused && !this.params.autoplay.disableOnInteraction && this.autoplay.run();
      },
      destroy: function destroy() {
        this.autoplay.running && this.autoplay.stop(), document.removeEventListener("visibilitychange", this.autoplay.onVisibilityChange);
      }
    }
  }, {
    name: "effect-fade",
    params: {
      fadeEffect: {
        crossFade: !1
      }
    },
    create: function create() {
      n.extend(this, {
        fadeEffect: {
          setTranslate: ve.setTranslate.bind(this),
          setTransition: ve.setTransition.bind(this)
        }
      });
    },
    on: {
      beforeInit: function beforeInit() {
        if ("fade" === this.params.effect) {
          this.classNames.push(this.params.containerModifierClass + "fade");
          var e = {
            slidesPerView: 1,
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: !0,
            spaceBetween: 0,
            virtualTranslate: !0
          };
          n.extend(this.params, e), n.extend(this.originalParams, e);
        }
      },
      setTranslate: function setTranslate() {
        "fade" === this.params.effect && this.fadeEffect.setTranslate();
      },
      setTransition: function setTransition(e) {
        "fade" === this.params.effect && this.fadeEffect.setTransition(e);
      }
    }
  }, {
    name: "effect-cube",
    params: {
      cubeEffect: {
        slideShadows: !0,
        shadow: !0,
        shadowOffset: 20,
        shadowScale: .94
      }
    },
    create: function create() {
      n.extend(this, {
        cubeEffect: {
          setTranslate: fe.setTranslate.bind(this),
          setTransition: fe.setTransition.bind(this)
        }
      });
    },
    on: {
      beforeInit: function beforeInit() {
        if ("cube" === this.params.effect) {
          this.classNames.push(this.params.containerModifierClass + "cube"), this.classNames.push(this.params.containerModifierClass + "3d");
          var e = {
            slidesPerView: 1,
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: !0,
            resistanceRatio: 0,
            spaceBetween: 0,
            centeredSlides: !1,
            virtualTranslate: !0
          };
          n.extend(this.params, e), n.extend(this.originalParams, e);
        }
      },
      setTranslate: function setTranslate() {
        "cube" === this.params.effect && this.cubeEffect.setTranslate();
      },
      setTransition: function setTransition(e) {
        "cube" === this.params.effect && this.cubeEffect.setTransition(e);
      }
    }
  }, {
    name: "effect-flip",
    params: {
      flipEffect: {
        slideShadows: !0,
        limitRotation: !0
      }
    },
    create: function create() {
      n.extend(this, {
        flipEffect: {
          setTranslate: me.setTranslate.bind(this),
          setTransition: me.setTransition.bind(this)
        }
      });
    },
    on: {
      beforeInit: function beforeInit() {
        if ("flip" === this.params.effect) {
          this.classNames.push(this.params.containerModifierClass + "flip"), this.classNames.push(this.params.containerModifierClass + "3d");
          var e = {
            slidesPerView: 1,
            slidesPerColumn: 1,
            slidesPerGroup: 1,
            watchSlidesProgress: !0,
            spaceBetween: 0,
            virtualTranslate: !0
          };
          n.extend(this.params, e), n.extend(this.originalParams, e);
        }
      },
      setTranslate: function setTranslate() {
        "flip" === this.params.effect && this.flipEffect.setTranslate();
      },
      setTransition: function setTransition(e) {
        "flip" === this.params.effect && this.flipEffect.setTransition(e);
      }
    }
  }, {
    name: "effect-coverflow",
    params: {
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows: !0
      }
    },
    create: function create() {
      n.extend(this, {
        coverflowEffect: {
          setTranslate: ge.setTranslate.bind(this),
          setTransition: ge.setTransition.bind(this)
        }
      });
    },
    on: {
      beforeInit: function beforeInit() {
        "coverflow" === this.params.effect && (this.classNames.push(this.params.containerModifierClass + "coverflow"), this.classNames.push(this.params.containerModifierClass + "3d"), this.params.watchSlidesProgress = !0, this.originalParams.watchSlidesProgress = !0);
      },
      setTranslate: function setTranslate() {
        "coverflow" === this.params.effect && this.coverflowEffect.setTranslate();
      },
      setTransition: function setTransition(e) {
        "coverflow" === this.params.effect && this.coverflowEffect.setTransition(e);
      }
    }
  }, {
    name: "thumbs",
    params: {
      thumbs: {
        multipleActiveThumbs: !0,
        swiper: null,
        slideThumbActiveClass: "swiper-slide-thumb-active",
        thumbsContainerClass: "swiper-container-thumbs"
      }
    },
    create: function create() {
      n.extend(this, {
        thumbs: {
          swiper: null,
          init: be.init.bind(this),
          update: be.update.bind(this),
          onThumbClick: be.onThumbClick.bind(this)
        }
      });
    },
    on: {
      beforeInit: function beforeInit() {
        var e = this.params.thumbs;
        e && e.swiper && (this.thumbs.init(), this.thumbs.update(!0));
      },
      slideChange: function slideChange() {
        this.thumbs.swiper && this.thumbs.update();
      },
      update: function update() {
        this.thumbs.swiper && this.thumbs.update();
      },
      resize: function resize() {
        this.thumbs.swiper && this.thumbs.update();
      },
      observerUpdate: function observerUpdate() {
        this.thumbs.swiper && this.thumbs.update();
      },
      setTransition: function setTransition(e) {
        var t = this.thumbs.swiper;
        t && t.setTransition(e);
      },
      beforeDestroy: function beforeDestroy() {
        var e = this.thumbs.swiper;
        e && this.thumbs.swiperCreated && e && e.destroy();
      }
    }
  }];
  return void 0 === W.use && (W.use = W.Class.use, W.installModule = W.Class.installModule), W.use(we), W;
});
"use strict";

$(document).ready(function () {
  initSelects();
  initSliders();
  openLangSwitcher();
  tabs();
  openProfileSettings();
  selectAllCheckbox();
  actionsAfterCheck();
  openActions();
  loadFile();
  getDatesToGraph();
  filterGraph();
  modals();
  openTableActions();
  openMobileMenu();
  initMaps();
  scrollToNumbers();
  addSchool(); // const defaultDatepicker = $('.datepicker-here').datepicker({
  // 	autoClose: true,
  // });

  $('.datepicker-here').each(function () {
    var defaultDatepicker = $(this).datepicker({
      autoClose: true
    });
    $(this).on('input', function () {
      defaultDatepicker.setViewDate($(this).val());
    });
  });
  $('.monthpicker').datepicker({
    autoClose: true,
    view: 'months',
    minView: 'months',
    dateFormat: 'MM yyyy'
  });
  $('#not-patronymic').change(function () {
    var checked = $(this).prop('checked');
    $(this).parent('.radio').siblings('.input').find('input').attr('disabled', checked);
  });
  $('.article-play').fancybox({
    openEffect: 'none',
    closeEffect: 'none',
    helpers: {
      media: {}
    }
  });
});

function openLangSwitcher() {
  $('body').on('click', '.lang-current', function () {
    $(this).addClass('active');
    $(this).siblings('.lang-container').slideToggle();
  });
}

function initSliders() {
  var mainSlider = new Swiper('.main-container', {
    slidesPerView: 1,
    spaceBetween: 20,
    navigation: {
      prevEl: '.main-left',
      nextEl: '.main-right'
    }
  });
  var totalSlide = $('.main-container .swiper-slide').length;
  $('.main-container .swiper-slide').each(function (index) {
    $(this).find('.slider-current').text(index + 1);
    $(this).find('.slider-total').text(totalSlide);
  });
  var directionSlider = new Swiper('.direction .swiper-container', {
    slidesPerView: 'auto'
  });
  var partnersSlider = new Swiper('.partners .swiper-container', {
    slidesPerView: 'auto',
    navigation: {
      prevEl: '.partners-left',
      nextEl: '.partners-right'
    }
  });
  var volontSlider = new Swiper('.volont-slider .swiper-container', {
    slidesPerView: 'auto',
    pagination: {
      el: '.volont-pagination'
    },
    navigation: {
      nextEl: '.volont-next'
    }
  });
  volontSlider.on('slideChange', function () {
    $('.volont-next').removeClass("slide-".concat(volontSlider.activeIndex));
    $('.volont-next').removeClass("slide-".concat(volontSlider.activeIndex + 2));
    $('.volont-next').addClass("slide-".concat(volontSlider.activeIndex + 1));
  });
}

;

function initSelects() {
  $('.nice-select').each(function () {
    $(this).niceSelect();
  });
  $('.sl2').each(function () {
    $(this).select2({
      minimumResultsForSearch: Infinity,
      placeholder: $(this).data('placeholder')
    });
  });
  $('.school-select').each(function () {
    $(this).select2({
      placeholder: $(this).data('placeholder'),
      dropdownCssClass: "school-dropdown"
    });
  });
}

;

function tabs() {
  $('.auth-list').on('click', 'li:not(.active)', function (e) {
    $(this).addClass('active').siblings().removeClass('active').closest('.auth').find('.auth-body').removeClass('active').eq($(this).index()).addClass('active');
  });
  $('.admin-list').on('click', 'li:not(.active)', function (e) {
    $(this).addClass('active').siblings().removeClass('active').closest('.admin').find('.admin-tab').removeClass('active').eq($(this).index()).addClass('active');
  });
  $('.contacts-list').on('click', 'li:not(.active)', function (e) {
    $(this).addClass('active').siblings().removeClass('active').closest('.contacts').find('.contacts-tab').removeClass('active').eq($(this).index()).addClass('active');
  });
  $('.data-list').on('click', 'li:not(.active)', function (e) {
    $(this).addClass('active').siblings().removeClass('active').closest('.admin').find('.data-tab').removeClass('active').eq($(this).index()).addClass('active');
  });
}

function openProfileSettings() {
  $('.profile').on('click', function () {
    $('.profile-settings').slideToggle(400);
    $(this).toggleClass('active');
  });
}

function selectAllCheckbox() {
  $('.table th .check input').change(function () {
    var checked = $(this).prop('checked');
    $('.table td .check input').prop('checked', checked);
    $('.table td .check input').trigger('change');
  });
}

function actionsAfterCheck() {
  $('.table td .check input').change(function () {
    var allInputLength = $('.table td .check input').length;
    var checkedLength = $('.table td .check input:checked').length;
    $(this).parents('.table').find('.actions').addClass('active');
    $('#current').text(checkedLength);
    $('#all').text(allInputLength);

    if (checkedLength === 0) {
      $(this).parents('.table').find('.actions').removeClass('active');
    }

    if (checkedLength === allInputLength) {
      $('.table th .check input').prop('checked', true);
    } else {
      $('.table th .check input').prop('checked', false);
    }
  });
}

function openActions() {
  $('.actions-btn').on('click', function () {
    $(this).toggleClass('active');
    $('.actions-drop').toggleClass('active');
  });
}

function loadFile() {
  $('#file').change(function (e) {
    var file = e.target.files[0];
    $(this).next('.btn-text').text(file.name);
  });
}

function getDatesToGraph() {
  var lng = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 14;
  var startDate = new Date();
  var endDate = startDate * 1 + lng * 24 * 3600 * 1000;
  var template = '';
  var days = ['Вc', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
  var months = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
  var values = ['4', '2', '5'];

  function getDates(d1, d2) {
    var oneDay = 24 * 3600 * 1000;

    for (var ms = d1 * 1, last = d2 * 1; ms < last; ms += oneDay) {
      template += "\n\t\t\t<div class=\"graph-day\" style=\"z-index: ".concat(new Date(ms).getDate(), "\">\n\t\t\t<div class=\"graph-wrap\" style=\"height: ").concat(values[new Date(ms).getDay()] * 45, "px\">\n\t\t\t<div class=\"graph-value\"></div>\n\t\t\t<div class=\"help\">\n\t\t\t<div class=\"help-text\">\n\t\t\t<div class=\"help-date\">").concat(new Date(ms).getDate(), " ").concat(months[new Date(ms).getMonth()], "</div>\n\t\t\t<ul class=\"help-list\">\n\t\t\t<li>10:00 / 1-4 \u043A\u043B\u0430\u0441\u0441</li>\n\t\t\t<li>12:00 / 9 \u0411 \u043A\u043B\u0430\u0441\u0441</li>\n\t\t\t<li>14:00 / 5-11 \u043A\u043B\u0430\u0441\u0441</li>\n\t\t\t<li>16:00 / 1-4 \u043A\u043B\u0430\u0441\u0441</li>\n\t\t\t</ul>\n\t\t\t</div>\n\t\t\t</div>\n\t\t\t</div>\n\t\t\t<div class=\"graph-weekday\">").concat(days[new Date(ms).getDay()], "</div>\n\t\t\t<div class=\"graph-date\">").concat(new Date(ms).getDate(), "</div>\n\t\t\t</div>\n\t\t\t");
    }
  }

  getDates(startDate, endDate);
  $('.graph-bottom').html(template);
}

function filterGraph() {
  $('.graph-setting').each(function () {
    $(this).on('click', function () {
      $('.graph-setting').removeClass('active');
      $(this).addClass('active');
      getDatesToGraph($(this).data('lng'));
    });
  });
}

var modal = $('.modal');
var overlay = $('.overlay');
var close = $('.modal-close, .modal-cancel');
var width = $(window).width();

function modals() {
  $(document).on('click', '.modal-close, .modal-cancel', function (event) {
    event.preventDefault();
    modalClose();
  }); // close.on('click', function(event){
  // 	event.preventDefault();
  // 	modalClose();
  // });

  overlay.on('mousedown', function (event) {
    if (event.target !== modal && modal.has(event.target).length === 0) {
      modalClose();
    }
  });
  $('body').on('click', '[data-open]', function (e) {
    e.preventDefault();
    var modalName = $(this).data('open');
    modalOpen(modalName);
  });
}

function modalOpen(modalName) {
  $('body').css('width', width);
  $('body').addClass('no-scroll');
  overlay.addClass('active');
  modal.removeClass('active');
  $('.' + modalName).addClass('active');
}

function modalClose() {
  modal.removeClass('active');
  overlay.removeClass('active');
  $('body').removeClass('no-scroll');
}

;

function openTableActions() {
  $('.table-more').each(function () {
    $(this).on('click', function () {
      if ($(this).next('.table-drop').hasClass('active')) {
        $('.table-drop').removeClass('active');
      } else {
        $('.table-drop').removeClass('active');
        $(this).next('.table-drop').addClass('active');
      }
    });
  }); // $('body').on('click', function(e) {
  // 	if (e.target !== $('.table-more') && $('.table-more').has(e.target).length === 0) {
  // 		$('.table-drop').removeClass('active')
  // 	}
  // })
}

function openMobileMenu() {
  $('.burger').on('click', function () {
    $('.mobile-menu').addClass('active');
  });
  $('.mobile-close').on('click', function () {
    $('.mobile-menu').removeClass('active');
  });
}

function initMaps() {
  if ($('#map-1').is('#map-1')) {
    var init = function init() {
      var map = new ymaps.Map('map-1', {
        center: [lng, lat],
        zoom: zoom,
        controls: [],
        behaviors: ['drag']
      });
      var placemark = new ymaps.Placemark([lng, lat], {
        hintContent: ''
      }, {
        iconLayout: 'default#image'
      });
      map.geoObjects.add(placemark);
    };

    ymaps.ready(init);
    var lat = $('#map-1').data('lat');
    var lng = $('#map-1').data('lng');
    var zoom = $('#map-1').data('zoom');
  }

  if ($('#map-2').is('#map-2')) {
    var _init = function _init() {
      var map = new ymaps.Map('map-2', {
        center: [_lng, _lat],
        zoom: _zoom,
        controls: [],
        behaviors: ['drag']
      });
      var placemark = new ymaps.Placemark([_lng, _lat], {
        hintContent: ''
      }, {
        iconLayout: 'default#image'
      });
      map.geoObjects.add(placemark);
    };

    ymaps.ready(_init);

    var _lat = $('#map-2').data('lat');

    var _lng = $('#map-2').data('lng');

    var _zoom = $('#map-2').data('zoom');
  }
}

function animateNumbers() {
  $('.num').each(function () {
    var num = +$(this).data('num');
    $(this).animateNumber({
      number: num,
      numberStep: function numberStep(now, tween) {
        var floored_number = Math.floor(now),
            target = $(tween.elem);
        target.find('span').text(floored_number);
      }
    }, {
      easing: 'swing',
      duration: 1000
    });
  });
}

function scrollToNumbers() {
  var scrollDone = false;
  $(window).on('scroll load resize', function (e) {
    var h = $(window).height();
    var top = $(window).scrollTop();
    var numbers = $('.numbers');

    if (numbers.is('.numbers')) {
      if (top > numbers.offset().top - h) {
        if (!scrollDone) {
          animateNumbers();
          scrollDone = true;
        }
      }
    }
  });
}

function addSchool() {
  var btn = $('#add-school');
  var template = $('.school-item').html();
  btn.on('click', function () {
    $(this).parents('.admin-tab').append("<form class=\"school-item\">\n\t\t\t\t".concat(template, "\n\t\t\t</form>"));
  });
}